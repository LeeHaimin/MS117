function enter(pi) {
    if (pi.getMap().getAllMonstersThreadsafe().size() == 0) {
        cm.warp(100000000,0);
	cm.setEventCount("神木村");
	cm.worldSpouseMessage(0x20,"[系列-神木村] 玩家 "+ cm.getChar().getName() +" 通关 "+ cm.getEventCount("神木村") +" 次 系统随机给予通关奖励。");
    } else {
        pi.playerMessage(-1, "必须消灭区域内的所有怪物才能移动到下一回合。");
        pi.playerMessage(5, "必须消灭区域内的所有怪物才能移动到下一回合。");
    }
}