/* Joyce
	Event NPC
*/

var status = -1;
var eff = "#fUI/UIWindow/Quest/icon6/7#";
var maps = Array(
    500000000,
    740000000,//林中之城
    749050400, //水下世界 - 水下世界
    219000000, //火焰之路 - 阿里安特
    600000000, //魔法密林 - 魔法密林
    551000000, //神秘岛 - 冰峰雪域
    740000000, //女皇之路 - 圣地岔路
    802001000, //射手村 - 射手村
    802001026, //废弃都市 - 废弃都市
    701100000, //神木村 - 神木村
    701000000, //明珠港 - 明珠港
    865000000, //玩具城 - 玩具城
    701100000, //逆奥之城 - 卡姆那 （内部）
    800040000, //诺特勒斯 - 诺特勒斯码头
    801000000, //昭和村 - 昭和村
    540000000, //新加坡 - 中心商务区
    541000000
);//上海外滩


var monstermaps = Array(
    Array(690000078, 0, "射手村东部森林(猪猪) "),
    Array(690000116, 0, "火独眼巢穴 IV							"),
    Array(690000090, 0, "森林打猎场 I"),
    Array(690000060, 0, "猴子森林"),
    Array(690000038, 0, "蘑菇巢穴（刺蘑菇僵尸蘑菇)）"),
    Array(690000035, 0, "堕落主义"),
    Array(690000032, 0, "绿蘑菇猎场 III"),
    Array(690000054, 0, "明珠港郊外(蜗牛)"),
    Array(690000071, 0, "冰冷的神殿（牛魔王蝙蝠怪）"),
    Array(690000058, 0, "废弃地铁1号线(蝙蝠小幽灵)"),
    Array(690000063, 0, "铁甲猪的领地"),
    Array(690000064, 0, "巨人之森"),
    Array(280030200, 0, "扎昆的祭坛"),
    Array(280030001, 0, "进阶扎昆的祭坛"),
    Array(690000065, 0, "土龙洞")
);

var tiaotiaomaps = Array(
    Array(50000, 0, "大蘑菇								[1-10级]"),
    Array(101030500, 0, "森林尽头							[20-30级]"),
    Array(102030400, 0, "灰烬之地							[50-60级]"),
    Array(200010100, 0, "三色庭院通道						[60-70级]"),

    Array(310060300, 0, "深坑道								[80-85级]"),
    Array(261010102, 0, "研究所202号							[85-90级]"),
    Array(300010000, 0, "苔藓树丛路口						[90-95级]"),
    Array(211040300, 0, "尖锐的绝壁1							[95-100级]"),
    Array(223010000, 0, "滑稽车站1							[100-110级]"),
    Array(251010403, 0, "红鼻子海盗团老巢3					[110-120级]"),
    Array(252020700, 0, "苦难者之屋							[120-125级]"),
    Array(250010304, 0, "流浪熊的地盘						[125-128级]"),
    Array(211042200, 0, "艰苦洞穴3							[128-135级]"),
    Array(240040500, 0, "龙之巢穴入口						[135-140级]"),
    Array(240040520, 0, "遭破坏的龙之巢穴					[140-145级]"),
    Array(240040511, 0, "被遗忘的龙之巢穴					[145-150级]"),
    Array(240040600, 0, "主巢穴山峰							[150-155级]"),
    Array(270030500, 0, "忘却之路5							[150-155级]"),
    Array(271000100, 0, "变形的提诺之林						[155-160级]"),
    Array(271030100, 0, "骑士团第1区域						[160-165级]"),
    Array(271030400, 0, "骑士团第4区域						[165-170级]"),
    Array(271030540, 0, "骑士团殿堂5							[170-180级]"),
    Array(273030100, 0, "火岩山丘							[180-190级]"),
    Array(273040300, 0, "荒废的发掘地区4						[190-195级]"),
    Array(273060300, 0, "战士决战处							[195-200级]")
);

var bossmaps = Array(
    910001000,//僵尸蘑菇的巢穴
    749050400,//蓝蘑菇王的巢穴
    701000210);//藏经主场


var selectedMap = -1;
var selectedArea = -1;

function start() {
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == 1) {
        status++;
    } else {
        if (status >= 2 || status == 0) {
            cm.dispose();
            return;
        }
        status--;
    }

    if (status == 0) {
        cm.sendSimple("#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#您好 #r#h ##k 请选择您要传送的项目:\r\n#L0##b" + eff + "特色旅游地图#b#l\r\n#L1##b" + eff + "老版冒险岛地图#b#l\r\n#L8#" + eff + "推荐练级地图传送#b#l\r\n#L5#" + eff + "美洲豹栖息地#b#l\r\n#L18#" + eff + "进入家族中心(创建家族)#b#l");
    } else if (status == 1) {
        var selStr = "请选择您的目的地: #b";
        if (selection == 0) {
            for (var i = 0; i < maps.length; i++) {
                selStr += "\r\n#L" + i + "##m" + maps[i] + "# #l";
            }
        } else if (selection == 2) {
            cm.dispose();
            cm.openNpc(9010022);
            return;
        } else if (selection == 3) {
            cm.dispose();
            cm.openNpc(9900002, 1);
            return;
        } else if (selection == 18) {
            cm.warp(200000301, 0);
            cm.dispose();
            return;
        } else if (selection == 4) {
            for (var i = 0; i < bossmaps.length; i++) {
                selStr += "\r\n#L" + i + "##m" + bossmaps[i] + "# #l";
            }
        } else if (selection == 8) {
            for (var i = 0; i < tiaotiaomaps.length; i++) {
                selStr += "\r\n#L" + i + "#" + tiaotiaomaps[i][2] + "";
            }
        } else if (selection == 5) {
            cm.warp(931000500, 0);
            cm.dispose();
            return;
        } else {
            for (var i = 0; i < monstermaps.length; i++) {
                selStr += "\r\n#L" + i + "#" + monstermaps[i][2] + "";
            }
        }
        selectedArea = selection;
        cm.sendSimple(selStr);
    } else if (status == 2) {
        //cm.sendYesNo("看来这里的事情都已经处理完了啊。您真的要移动到 #m" + (selectedArea == 0 ? maps[selection] : monstermaps[selection]) + "# 吗？");
        cm.sendYesNo("看来这里的事情都已经处理完了啊。您真的要移动吗？");
        selectedMap = selection;
    } else if (status == 3) {
        if (selectedMap >= 0) {
            if (selectedArea == 0) {
                cm.warp(maps[selectedMap], 0);
            } else if (selectedArea == 4) {
                cm.warp(bossmaps[selectedMap], 0);
            } else if (selectedArea == 8) {
                cm.warp(tiaotiaomaps[selectedMap][0], 0);
            } else {
                cm.warp(monstermaps[selectedMap][0], 0);
            }
            //cm.warp(selectedArea == 0 ? maps[selectedMap] : monstermaps[selectedMap], 0);
        }
        cm.dispose();
    } else if (status == 6) {

    }
}