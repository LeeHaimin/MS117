/* 150装备 */

var status = -1;
var itemList = Array(
Array(1212063, 4500000), 
Array(1222058, 4500000), 
Array(1232057, 4500000), 
Array(1242060, 4500000), 
Array(1242061, 4500000), 
Array(1302275, 4500000),
Array(1312153, 4500000),
Array(1322203, 4500000),
Array(1332225, 4500000),
Array(1342082, 4500000),
Array(1362090, 4500000),
Array(1372177, 4500000),
Array(1382208, 4500000),
Array(1402196, 4500000),
Array(1412135, 4500000),
Array(1422140, 4500000),
Array(1432167, 4500000),
Array(1442223, 4500000),
Array(1452205, 4500000),
Array(1462193, 4500000),
Array(1472214, 4500000),
Array(1482168, 4500000),
Array(1492179, 4500000),
Array(1522094, 4500000),
Array(1532098, 4500000),
Array(1252015, 4500000),
Array(1003797, 1500000),
Array(1042254, 1500000),
Array(1062165, 1500000),
Array(1003798, 1500000),
Array(1042255, 1500000),
Array(1062166, 1500000),
Array(1003799, 1500000),
Array(1042256, 1500000),
Array(1062167, 1500000),
Array(1003800, 1500000),
Array(1042257, 1500000),
Array(1062168, 1500000),
Array(1003801, 1500000),
Array(1042258, 1500000),
Array(1062169, 1500000)
);
var selectedItem = -1;
var selectedCost = -1;

function start() {
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == 1) {
        status++;
    } else {
        if (status >= 0) {
            cm.dispose();
            return;
        }
        status--;
    }
    if (status == 0) {
        var selStr = "#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#亲爱的#r#h ##k您好，请选择您希望购买的装备：";
        for (var i = 0; i < itemList.length; i++) {
            selStr += "\r\n#L" + i + "##i" + itemList[i][0] + ":# #b#t" + itemList[i][0] + "##k   #r" + itemList[i][1] / 5 + "#k点卷#l";
        }
        cm.sendSimple(selStr);
    } else if (status == 1) {
        var item = itemList[selection];
        if (item != null) {
            selectedItem = item[0];
            selectedCost = item[1] / 5;
            cm.sendYesNo("您是否购买#i" + selectedItem + ":# #b#t" + selectedItem + "##k 需要 #r" + selectedCost + "#k 点卷？");
        } else {
            cm.sendOk("出现错误...");
            cm.dispose();
        }
    } else if (status == 2) {
        if (selectedCost <= 0 || selectedItem <= 0) {
            cm.sendOk("购买道具出现错误...");
            cm.dispose();
            return;
        }
        if (cm.getPlayer().getCSPoints(1) >= selectedCost) {
            var gachaponItem = cm.gainGachaponItem(selectedItem, 1, "点卷商店", 3, true);
            if (gachaponItem != -1) {
                cm.gainNX( - selectedCost);
                cm.sendOk("恭喜您成功购买#i" + selectedItem + ":# #b#t" + selectedItem + "##k。");
            } else {
                cm.sendOk("购买失败，请您确认在背包所有栏目窗口中是否有一格以上的空间。");
            }
        } else {
            cm.sendOk("您没有那么多点卷。\r\n\r\n购买#i" + selectedItem + ":# #b#t" + selectedItem + "##k 需要 #r" + selectedCost + "#k 点卷。");
        }
        cm.dispose();
    }
}