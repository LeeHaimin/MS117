var status = 0;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status == 1 && mode == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        status--;
        cm.dispose();
    }
    if (status == 0) {
if((cm.getJob() == 0 || cm.getJob() == 2004)&& cm.getPlayer().getLevel() >= 10){
var selStr = "你确定要转职？\r\n请慎重选择...\r\n#b#L0#我要选择光明夜光#l\r\n#b#L1#我要选择黑暗夜光#l";
}else{
var selStr = "你确定要转职？\r\n请慎重选择...\r\n#b#L2#我要进行转职(2/3/4转)#l";
}
        cm.sendSimple(selStr);
    } else if (status == 1) {
        switch (selection) {
        case 0:
            if ((cm.getJob() == 0 || cm.getJob() == 2004) && cm.getPlayer().getLevel() >= 10) {
                cm.getPlayer().changeJob(2700);
		cm.teachSkill(27000106,0,5);
		cm.teachSkill(27001100,0,20);
		cm.gainItem(1212000,1);
		cm.gainItem(1352400,1);
		cm.sendOk("系统已经为您转职为光明夜光法师.\r\n.");
            } else {
                cm.sendOk("你不是新手职业 或你的等级没有达到10.");
            }
            cm.dispose();
            break;
        case 1:
            if ((cm.getJob() == 0 || cm.getJob() == 2004) && cm.getPlayer().getLevel() >= 10) {
                cm.getPlayer().changeJob(2700);
		cm.teachSkill(27000107,0,5);
		cm.teachSkill(27001200,0,20);
		cm.gainItem(1212000,1);
		cm.gainItem(1352400,1);
		cm.sendOk("系统已经为您转职为黑暗夜光法师.\r\n.");
            } else {
                cm.sendOk("你不是新手职业 或你的等级没有达到10.");
            }
            cm.dispose();
            break;
        case 2:
            if (cm.getJob()==2700 && cm.getPlayer().getLevel()>=30) {
                cm.getPlayer().changeJob(2710);
		cm.gainItem(1352401,1);
		cm.gainItem(1212001,1);
		cm.sendOk("系统已经为您转职为夜光法师(二转).\r\n.");
	    } else if (cm.getJob()==2710 && cm.getPlayer().getLevel()>=60){
                cm.getPlayer().changeJob(2711);
		cm.gainItem(1212005,1);
		cm.gainItem(1352402,1);
		cm.sendOk("系统已经为您转职为夜光法师(三转).\r\n.");
	    } else if (cm.getJob()==2711 && cm.getPlayer().getLevel()>=100){
                cm.getPlayer().changeJob(2712);
		cm.gainItem(1212010,1);
		cm.gainItem(1352403,1);
		cm.sendOk("系统已经为您转职为夜光法师(四转).\r\n.");
            } else {
                cm.sendOk("你不是新手职业 或你的等级没有达到10.");
            }
            cm.dispose();
            break;
        case 7:
            if ((cm.getJob() == 0 || cm.getJob() == 2004)&& cm.getPlayer().getLevel() >= 10) {
                cm.openNpc(9300011,1);
            } else {
                cm.sendOk("你不是新手职业 或你的等级没有达到10.");
            }
            cm.dispose();
            break;
        }
    }
}