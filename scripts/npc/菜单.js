var status = 0;
var random = java.lang.Math.floor(Math.random() * 4);
var eff = "#fUI/UIWindow.img/PvP/Scroll/enabled/next2#";
function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status == 0 && mode == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        status--;
    }
    if (cm.getMapId() == 180000001) {
            cm.sendOk("很遗憾，您因为违反用户守则被禁止游戏活动，如有异议请联系管理员.")
            cm.dispose();
        } else if (status == 0) {
        var selStr = "#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#亲爱的#r#h ##k您好，请您选择您需要的功能:\r\n您当前的 #fUI/UIWindow.img/Shop/meso#积分：#r"+cm.getPlayerPoints()+"#k 点  #fUI/UIWindow.img/Shop/meso#活力值：#r"+cm.getPlayerEnergy()+"#k 点\r\n";
        selStr += "#L0#" + eff + "#r打开万能NPC[常用功能集合处]#l\r\n";
        selStr += "#L1#" + eff + "#b传送自由市场#l";
        selStr += "  #L2#" + eff + "#b免费领取经验[10级前]#l\r\n";
        selStr += "#L3#" + eff + "#b查看怪物掉物#l";
        selStr += "  #L4#" + eff + "#b管理雇佣商店#l\r\n";
        selStr += "#L5#" + eff + "#b累计充值兑换#l";
        selStr += "  #L6#" + eff + "#b冒险交流论坛[#r点击弹出#b]#l\r\n";
        selStr += "#L7#" + eff + "#b在线点数兑换#l";
        selStr += "  #L8#" + eff + "#b游戏内容指导#l\r\n";
		selStr += "#L13#" + eff + "#b活动功能简绍#l";
		selStr += "  #L10#" + eff + "#r进入PVP地图#l\r\n";
		selStr += "#L14#" + eff + "#b本服BOSS掉物  #l";
		selStr += "#L15#" + eff + "#b点卷中介#l\r\n";
		selStr += "#L16#" + eff + "#b点卷换金币    #l";
		selStr += "#L17#" + eff + "#b金币换点卷#l";
        cm.sendSimple(selStr);
    } else if (status == 1) {
        switch (selection) {
        case 0:
            cm.dispose();
            cm.openNpc(9900002);
            break;
        case 1:
            if (cm.getPlayer().getMapId() >= 910000000 && cm.getPlayer().getMapId() <= 910000022) {
                cm.sendOk("您已经在市场了，还想做什么？");
            } else {
                cm.saveReturnLocation("FREE_MARKET");
                cm.warp(910000000, "st00");
            }
            cm.dispose();
            break;
        case 2:
	if(cm.getPlayer().getLevel() <= 250){
	cm.gainExp( + 500000000);
	cm.worldMessage("恭喜新玩家"+ cm.getChar().getName() +"在拍卖NPC处领取5W经验");
	cm.sendOk("恭喜您领取成功,30级下都能在我这里领取经验");
}else{
	cm.sendOk("你的等级大于30");
}
		cm.dispose();
            break;
        case 3://怪物暴物品命令
            cm.dispose();
            cm.openNpc(9010000, 1);
            break;
        case 4://管理雇佣商店
            cm.dispose();
	    cm.openNpc(9030000);
            break;
        case 5://累积充值
            cm.dispose();
	    cm.openNpc(9900002,7);
            break;
        case 6://充值网站
            cm.dispose();
            cm.openWeb("http://27.79.112.168");
	    cm.sendOk("已经为您打开官方交流论坛！");
            break;
        case 7://在线时间换
            cm.dispose();
	    cm.openNpc(9000030,1);
            break;
        case 8://游戏指导
            cm.dispose();
	    cm.openNpc(9900005,1000);
            break;
        case 9:
            cm.dispose();
	    cm.openNpc(9900005,2000);
            break;
		 case 10:
		if (cm.getPlayer().getLevel() > 100 ) {
			cm.warp(701000210);
			cm.sendOk("青龙之门为大混战PK.赤龙之门为组队PK战.黄龙之门为家族PK战.祝您好运");
			} else {
				cm.sendOk("对不起.PK太危险了.100级以下的玩家不能进入噢.");
			}
			cm.dispose();
            break;
		 case 11:
		if (cm.getPlayer().getLevel() > 30 ) {
			cm.warp(746000003);
			cm.sendOk("您已经进入组队PK 祝你好运");
			} else {
				cm.sendOk("对不起.PK太危险了.只允许等级不小于30级的玩家进入");
			}
			cm.dispose();
            break;
            break;
		case 12:
		if (cm.getPlayer().getLevel() > 30 ) {
			cm.warp(746000004);
			cm.sendOk("您已经进入家族PK 祝你好运");
			} else {
				cm.sendOk("对不起.PK太危险了.只允许等级不小于30级的玩家进入");
			}
			cm.dispose();
            break;
		case 13:
            cm.dispose();
			cm.openNpc(9900001,3000);
            break;
		case 14:
            cm.dispose();
			cm.openNpc(9900002,36);
            break;
		case 15:
            cm.dispose();
			cm.openNpc(9900002,81);
            break;
		case 16:
            cm.dispose();
			cm.openNpc(9900002,83);
            break;
		case 17:
            cm.dispose();
			cm.openNpc(9900002,82);
            break;
	}
    }
}