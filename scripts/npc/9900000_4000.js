/** Author: nejevoli
	NPC Name: 		NimaKin
	Map(s): 		Victoria Road : 隐秘之路 - 工作场所 (180000000)
	Description: 		Maxes out your stats and able to modify your equipment stats
*/
importPackage(java.lang);

var status = 0;
var slot = Array();
var stats = Array("固定属性");
var selected;
var statsSel;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status >= 0 && mode == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        status--;
    }
    if (status == 0) {
        if (cm.getPlayerStat("GM") == 1) {
            cm.sendSimple("亲爱的管理员#r#h ##k您好，您有什么事情需要我帮忙的吗?#b\r\n#L2#修改装备属性#l");
        } else if (cm.getPlayerStat("GM") == 1) {
            cm.sendSimple("亲爱的管理员#r#h ##k您好，您有什么事情需要我帮忙的吗?#b\r\n#L0#加满属性#l\r\n#L1#加满所有技能#l\r\n#L4#初始化AP/SP#l\r\n#L7#初始化属性#k");
        } else {
            cm.dispose();
        }
        } else if (selection == 2 && cm.getPlayerStat("GM") == 1) {
            var avail = "";
            for (var i = -1; i > -199; i--) {
                if (cm.getInventory( - 1).getItem(i) != null) {
                    avail += "#L" + Math.abs(i) + "##t" + cm.getInventory( - 1).getItem(i).getItemId() + "##l\r\n";
                }
                slot.push(i);
            }
            cm.sendSimple("请选择您需要修改属性的装备:\r\n#b" + avail);
    } else if (status == 2 && cm.getPlayerStat("GM") == 1) {
        selected = selection - 1;
        var text = "";
        for (var i = 0; i < stats.length; i++) {
            text += "#L" + i + "#" + stats[i] + "#l\r\n";
        }
        cm.sendSimple("你选择了修改 #b#t" + cm.getInventory( - 1).getItem(slot[selected]).getItemId() + "##k 装备的属性\r\n请选择修改这个装备的具体属性\r\n#b" + text);
    } else if (status == 3 && cm.getPlayerStat("GM") == 1) {
        statsSel = selection;
        cm.playerMessage("当前选择 " + selection+"  "+slot[selected]);
        if (selection == 20) {
            cm.sendGetText("这个属性确定吗 #b#t" + cm.getInventory( - 1).getItem(slot[selected]).getItemId() + "##k's " + stats[statsSel] + " to?");
} else {
           //cm.sendGetNumber("这个属性确定吗1 #b#t" + cm.getInventory( - 1).getItem(slot[selected]).getItemId() + "##k's " + stats[statsSel] + " to?", 0, 0, 32767);
	   cm.sendYesNo("请确认是否调整！");
        }
    } else if (status == 4 && cm.getPlayerStat("GM") == 1) {
        cm.changeStat(slot[selected], 0, 90); //力量
        cm.changeStat(slot[selected], 1, 90); //敏捷
        cm.changeStat(slot[selected], 2, 90); //智力
        cm.changeStat(slot[selected], 3, 90); //运气
        cm.changeStat(slot[selected], 6, 66); //物理攻击
        cm.changeStat(slot[selected], 7, 66); //魔法攻击
        cm.changeStat(slot[selected], 15, 0); //可升级次数
        cm.changeStat(slot[selected], 16, 2); //金锤子次数
        cm.changeStat(slot[selected], 17, 9); //已升级次数
        cm.changeStat(slot[selected], 19, 12); //装备星级
        //cm.playerMessage("当前选择 " + selection +"  "+slot[selected]+" "+ stats[statsSel]);
        //cm.sendOk("已经调整完毕！");
        cm.dispose();
    } else if (status == 10 && cm.getPlayerStat("GM") == 1) {
        if (selection == 0) {
            cm.sendGetText("What would you like to search for? (e.g. STR %)");
            return;
        }
        cm.sendSimple("#L3#" + cm.getPotentialInfo(selection) + "#l");
        status = 0;
    } else if (status == 11 && cm.getPlayerStat("GM") == 1) {
        var eek = cm.getAllPotentialInfoSearch(cm.getText());
        for (var ii = 0; ii < eek.size(); ii++) {
            avail += "#L" + eek.get(ii) + "#Potential ID " + eek.get(ii) + "#l\r\n";
        }
        cm.sendSimple("What would you like to learn about?\r\n#b" + avail);
        status = 9;
    } else {
        cm.dispose();
    }
}