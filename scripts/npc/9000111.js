var status = 0;
var typed=0;
var random = 0;

function start() {
	status = -1;
	action(1, 0, 0);
}

function action(mode, type, selection) {
	if (mode == -1) {
		cm.dispose();
	} else {
		if (mode == 0 && status == 0) {
			cm.dispose();
			return;
		}
		if (mode == 1)
			status++;
		else
			status--;
		if (status == 0) { 
			cm.sendSimple("#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#亲爱的#r#h ##k您好，购买月卡的朋友除了享受以下福利外还赠送专属宠物#z5000228#、VIP勋章、取宠物名道具。.月卡用户每日会获得:\r\n#r#L2##fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#办理月卡(68元)#l #b\r\n\r\n#L4##fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#[月卡]双倍经验卡x1天权#l\r\n#L5##fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#[月卡]双倍爆率卡x1天权#l\r\n#L6##fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#[月卡]重置日常活动任务#l\r\n#L7##fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#[月卡]每日返还点卷魔方 (普通10个高级10个点券2000)#b)#l\r\n#L8##fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#[月卡]每日赠送道具卷轴 (#v2049400##v2049300#各3个#v4310088#50个#b)#l\r\n#L11##fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#[月卡]每日免费抽奖 (#k目前状态： #r推荐内容#b)#l");
		} else if (status == 1) {
			if (selection == 1) {
			if(cm.haveItem(2430865,1)){
			cm.sendOk("您已经办理了月卡,无需重复办理,谢谢!!!");
			cm.dispose();
			} else {
                	cm.sendYesNo("您是否要花费#r 100 #k元的充值金额办理#b 周卡理财 #k。");
			typed = 1;
			}
			} else if (selection == 2) {
			if(cm.haveItem(2430865,1)){
			cm.sendOk("您已经办理了月卡,无需重复办理,谢谢!!!");
			cm.dispose();
			} else {
                	cm.sendYesNo("您是否要花费#r 68 #k元的充值金额办理#b 月卡 #k。");
			typed = 2;
			}
			} else if (selection == 3) {
			if(cm.haveItem(2430865,1)){
			cm.sendOk("您已经办理了月卡,无需重复办理,谢谢!!!");
			cm.dispose();
			} else {
                	cm.sendYesNo("您是否要花费#r 48 #k元的充值金额办理#b 年卡理财 #k。");
			typed = 3;
			}
			} else if (selection == 4) {
			if(cm.haveItem(2430865,1) && cm.getSpace(5) > 2 && cm.getBossLog("理财双倍") == 0){
			cm.gainItem(5210004,1,1); //双倍经验卡 晚 1天权
			cm.gainItem(5210002,1,1); //双倍经验卡 白 1天权
			cm.setBossLog("理财双倍");
			cm.sendOk("请注意查收！");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 领取了每日双倍经验卡。");
			cm.dispose();
			} else {
                	cm.sendOk("请确认您今天是否已经领取。\r\n请确认您是否办理了月卡。\r\n请确认您的特殊背包栏是否空余2个位置。");
			cm.dispose();
			}
			} else if (selection == 5) {
			if(cm.haveItem(2430865,1) && cm.getSpace(5) > 2 && cm.getBossLog("理财双爆") == 0){
			cm.gainItem(5360015,1,1); //双倍爆率卡 1天权
			cm.setBossLog("理财双爆");
			cm.sendOk("请注意查收！");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 领取了每日双倍爆率卡。");
			cm.dispose();
			} else {
                	cm.sendOk("请确认您今天是否已经领取。\r\n请确认您是否办理了月卡。\r\n请确认您的特殊背包栏是否空余2个位置。");
			cm.dispose();
			}
			} else if (selection == 6) {
			if(cm.haveItem(4001753,1) && cm.haveItem(2430865,1)){
			cm.resetEventCount("抽奖");
			cm.resetEventCount("历练");
			cm.resetEventCount("养成");
			cm.resetEventCount("皇陵");
			cm.resetEventCount("罗朱");
			cm.resetEventCount("海盗");
			cm.resetEventCount("鬼节");
			cm.gainItem(4001753,-1);
			cm.sendOk("日常活动任务已经全部重置");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 重置了全部日常活动任务。");
			cm.dispose();
			}else{
			cm.sendOk("请确认您是否办理了月卡。\r\n请确认是否拥有#v4001753#日常任务重置物品。");
			cm.dispose();
			}
			} else if (selection == 7) {
			if(cm.haveItem(2430865,1) && cm.getSpace(5) > 2 && cm.getBossLog("理财魔方") == 0){
			cm.gainItem(5062002,10); //高级神奇魔方 x3
			cm.gainItem(5062000,10); //高级神奇魔方 x6
			cm.gainNX(+2000); //点卷
			cm.setBossLog("理财魔方");
			cm.sendOk("请注意查收！");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 领取了每日月卡福利。");
			cm.dispose();
			}else{
			cm.sendOk("请确认您今天是否已经领取。\r\n请确认您是否办理了月卡。\r\n请确认您的特殊背包栏是否空余2个位置。");
			cm.dispose();
			}
			} else if (selection == 8) {
			if(cm.haveItem(2430865,1) && cm.getSpace(2) > 2 && cm.getBossLog("理财卷轴") == 0){
			random = java.lang.Math.floor(Math.random() * 5);
			cm.gainItem(2049400,3);
			cm.gainItem(2049300,3); //强化混沌卷走 x2
cm.gainItem(4310088,50); 
			if(random > 3){
			cm.gainItem(2049122,1); //正向混沌卷轴 x1
			}
			cm.setBossLog("理财卷轴");
			cm.sendOk("请注意查收！");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 领取了卷轴支援。");
			cm.dispose();
			}else{
			cm.sendOk("请确认您今天是否已经领取。\r\n请确认您是否办理了月卡。\r\n请确认您的消耗背包栏是否空余2个位置。");
			cm.dispose();
			}
			} else if (selection == 9) {
			if(cm.haveItem(2430865,1) && cm.getBossLog("理财积分") == 0){
			cm.gainPlayerPoints(1800); //积分值
			cm.setBossLog("理财积分");
			cm.sendOk("请注意查收！");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 在月卡领取了每日大量积分值。");
			cm.dispose();
			}else{
			cm.sendOk("请确认您今天是否已经领取。\r\n请确认您是否办理了月卡。");
			cm.dispose();
			}
			} else if (selection == 10) {
			if(cm.haveItem(2430865,1) && cm.getBossLog("理财活力") == 0){
			cm.gainPlayerEnergy(500); //活力值
			cm.setBossLog("理财活力");
			cm.sendOk("请注意查收！");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 在月卡领取了每日大量活力值。");
			cm.dispose();
			}else{
			cm.sendOk("请确认您今天是否已经领取。\r\n请确认您是否办理了月卡。");
			cm.dispose();
			}
			} else if (selection == 11) {
			if(cm.haveItem(2430865,1) && cm.getBossLog("理财抽奖") == 0){
			cm.gainItem(2430069,1); //理财抽奖礼包
			cm.setBossLog("理财抽奖");
			cm.sendOk("请注意查收！");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 领取了每日抽奖礼包。");
			cm.dispose();
			}else{
			cm.sendOk("请确认您今天是否已经领取。\r\n请确认您是否办理了月卡。");
			cm.dispose();
			}
			}
	    	} else if (status == 2) {
			if(typed == 1){
                	if (cm.getHyPay(1) < 100) {
                	cm.sendNext("您充值金额不足 100 元。");
            		} else if (cm.addHyPay(100) > 0) {
			cm.gainNX(90000);
			cm.gainNX(-90000);
			cm.gainItem(4001753,2); //日常重置物品
			cm.gainItem(2430865,1,7); //理财凭证
			cm.gainItem(1142145,1,7); //VIP勋章 10G
			cm.sendOk("成功办理了周卡理财。\r\n#v2430865##b理财凭证 #v1142145#理财勋章 #v4001753#日常重置物品。\r\n#r注：请保留好以上物品...");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 在月卡办理了月卡周卡系列。");
			} else {
			cm.sendOk("办理月卡出现错误，请反馈给管理员！");
			}
                    	cm.dispose();
			}
			if(typed == 2){
                	if (cm.getHyPay(1) < 68) {
                	cm.sendNext("您充值金额不足 68 元。");
            		} else if (cm.addHyPay(68) > 0) {
			cm.gainItem(4001753,30); //日常重置物品
			cm.gainItem(2430865,1,30); //理财凭证
			cm.gainItem(1142145,1,30); //VIP勋章 10G
cm.gainPet(5000228,0,1,1,100,30,1); 
cm.gainItem(5170000,1,30);
			cm.sendOk("成功办理了月卡理财。\r\n#v2430865##b月卡权限(丢失不补) #v1142145#VIP勋章 #v4001753#日常重置物品以及一些小礼物。\r\n#r注：请保留好以上物品...");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 办理了月卡。");
			} else {
			cm.sendOk("办理月卡出现错误，请反馈给管理员！");
			}
                    	cm.dispose();
			}
			if(typed == 3){
                	if (cm.getHyPay(1) < 1500) {
                	cm.sendNext("您充值金额不足 1500 元。");
            		} else if (cm.addHyPay(1500) > 0) {
			cm.gainNX(3600000);
			cm.gainNX(-3600000);
			cm.gainItem(4001753,96); //日常重置物品
			cm.gainItem(2430865,1,365); //理财凭证
			cm.gainItem(1142145,1,365); //VIP勋章 10G
			cm.sendOk("成功办理了年卡理财。\r\n#v2430865##b理财凭证 #v1142145#理财勋章 #v4001753#日常重置物品。\r\n#r注：请保留好以上物品...");
			cm.worldSpouseMessage(0x20,"[月卡] 玩家 "+ cm.getChar().getName() +" 在月卡办理了月卡年卡系列。");
			} else {
			cm.sendOk("办理月卡出现错误，请反馈给管理员！");
			}
                    	cm.dispose();
			}
	  }
     }
}
