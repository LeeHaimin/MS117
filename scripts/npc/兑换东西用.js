/* 物品兑换 */

var status = -1;
//需要的道具 需要的数量 给玩家的道具 给玩家的数量
var itemList = Array(
Array(2210122, 1, 4310088, 1000),
//Array(2210122, 1, 4310034, 1000),
//Array(2210122, 1, 2046913, 20),
//Array(2210122, 1, 2046914, 20),
//Array(2210122, 1, 2046173, 20),
//Array(2210122, 1, 2046577, 20),
//Array(2210122, 1, 2046578, 20),
//Array(2210122, 1, 2046579, 20),
//Array(2210122, 1, 2046580, 20),
//Array(2210122, 1, 2046763, 20),
//Array(2210122, 1, 2046764, 20),
//Array(2210122, 1, 2046765, 20),
//Array(2210122, 1, 2046766, 20),
Array(2210122, 1, 1102484, 1),
Array(2210122, 1, 1082546, 1),
Array(2210122, 1, 1072746, 1),
Array(2210122, 1, 1132178, 1),
Array(2210122, 1, 1102485, 1),
Array(2210122, 1, 1082547, 1),
Array(2210122, 1, 1072747, 1),
Array(2210122, 1, 3010806, 1),
Array(4000659, 10, 4000038, 1) //10个希纳斯的头环兑换1个金杯
);
var itemId = -1; //需要的道具
var amount = -1; //需要的数量
var itemIdX = -1; //给玩家的道具
var amountX = -1; //给玩家的数量

function start() {
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == 0 && status == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        if (status > 0) {
            cm.dispose();
            return;
        }
        status--;
    }
    if (status == 0) {
        var selStr = "亲爱的#b#h0##k您好，这里是（铁胆无畏丶）家族物品兑换处(温馨提示：此脚本只给宣传的玩家进行兑换，别无他用，宣传20个论坛，送一套暴君，加油哈！！):\r\n\r\n#b";
        for (var i = 0; i < itemList.length; i++) {
            selStr += "\r\n#L" + i + "#使用" + itemList[i][1] + "个#v" + itemList[i][0] + "#兑换" + itemList[i][3] + "个#v" + itemList[i][2] + "##l";
        }
        cm.sendSimple(selStr);
    } else if (status == 1) {
        var item = itemList[selection];
        if (item != null) {
            itemId = item[0];
            amount = item[1];
            itemIdX = item[2];
            amountX = item[3];
            if (cm.getItemQuantity(itemId) >= amount) {
                cm.sendYesNo("您是否要使用#b" + amount + "#k个#v" + itemId + "# #b#t" + itemId + "##k兑换#b" + amountX + "#k个#v" + itemIdX + "# #b#t" + itemIdX + "##k吗？");
            } else {
                cm.sendOk("兑换#b" + amountX + "#k个#v" + itemIdX + "# #b#t" + itemIdX + "##k需要#b" + amount + "#k个#v" + itemId + "# #b#t" + itemId + "##k\r\n您还需收集#r" + (amount - cm.getItemQuantity(itemId)) + "#k个我才能为您兑换。");
                cm.dispose();
            }
        } else {
            cm.sendOk("出现错误...");
            cm.dispose();
        }
    } else if (status == 2) {
        if (itemId <= 0 || amount <= 0 || itemIdX <= 0 || amountX <= 0) {
            cm.sendOk("兑换道具出现错误...");
            cm.dispose();
            return;
        }
        if (cm.haveItem(itemId, amount) && cm.canHold(itemIdX, amountX)) {
            cm.gainItem(itemId, -amount);
            cm.gainItem(itemIdX, amountX);
            cm.worldMessage("『物品兑换』恭喜玩家 " + cm.getChar().getName() + " 使用 " + amount + "个 " + cm.getItemName(itemId) + " 成功兑换 " + amountX + "个 " + cm.getItemName(itemIdX));
            cm.sendOk("恭喜您使用#b " + amount + "#k个#v " + itemId + "##b#t " + itemId + "##k兑换#b " + amountX + "#k个#v " + itemIdX + "##b#t " + itemIdX + "##k。");
            status = -1;
        } else {
            cm.sendOk("您的背包空间不足，或者没有足够的道具来兑换。");
            cm.dispose();
        }
    }
}