status = -1;
var itemList = Array(

// ------ 特殊奖品 --------------
Array(3994417, 100, 1, 1), //红色蜡笔
Array(3994418, 100, 1, 1), //橙色蜡笔
Array(3994419, 100, 1, 1), //黄色蜡笔
Array(3994417, 100, 1, 1), //绿色蜡笔
Array(3994418, 100, 1, 1), //青色蜡笔
Array(3994419, 100, 1, 1) //蓝色蜡笔
);

function start() {
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == 1) {
        status++;
    } else {
        if (status == 0) {
            cm.sendOk("不想使用吗？…我的肚子里有各类#b奇特座椅或卷轴、装备、新奇道具#k哦！");
            cm.dispose();
        }
        status--;
    }
    if (status == 0) {
        if (cm.haveItem(4000463,50)) {
            cm.sendYesNo("冒险岛特殊物品抽奖中有各类#b蜡笔#k噢！使用50个“#b#t4000463##k”就可以交换. 在旁边的裂空之鹰那里就有哦噢。 假如不买国庆纪念币的话，是不可以使用我的。现在要玩转蛋机么? 悄悄的告诉你，点击后要不么中，一中就必是蜡笔，有惊喜！！");
        } else {
            cm.sendOk("你背包里有50个#b#t4000463##k吗?");
            cm.safeDispose();
        }
    } else if (status == 1) {
        var chance = Math.floor(Math.random() * 1000);
        var finalitem = Array();
        for (var i = 0; i < itemList.length; i++) {
            if (itemList[i][1] >= chance) {
                finalitem.push(itemList[i]);
            }
        }
        if (finalitem.length != 0) {
            var item;
            var random = new java.util.Random();
            var finalchance = random.nextInt(finalitem.length);
            var itemId = finalitem[finalchance][0];
            var quantity = finalitem[finalchance][2];
            var notice = finalitem[finalchance][3];
            item = cm.gainGachaponItem(itemId, quantity, "市场彪鲁特殊物品抽奖中", notice);
            if (item != -1) {
                cm.gainItem(4000463, -50);
                cm.sendOk("你获得了 #b#t" + item + "##k " + quantity + "个。");
            } else {
                cm.sendOk("你确实有#b#t4000463##k吗？如果是，请你确认在背包的装备，消耗，其他窗口中是否有一格以上的空间。");
            }
            cm.safeDispose();
        } else {
            cm.sendOk("今天的运气可真差，什么都没有拿到。\r\n(获得了安慰奖：爱心宝石 一个。)");
            cm.gainItem(4000463, -50);
            cm.gainItem(4001465, 1);
            cm.safeDispose();
        }
    }
}