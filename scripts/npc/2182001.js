/*
 *	阿斯旺 - 财务大臣伍德万
 */

var status = 0;
var beauty = 0;
var next = true;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == -1) {
        cm.dispose();
    } else {
        if (status >= 0 && mode == 0) {
            cm.dispose();
            return;
        }
        if (mode == 1) {
            status++;
        } else {
            status--;
        }
        if (status == 0) {
            cm.sendSimple("干嘛……有什么事吗?#b\r\n\r\n#L0#我是来领取这一季结束前的解放战通关奖励的。#l\r\n#b#L4#我想再次了解一下在阿斯旺可以获得什么东西。#l#k");
        } else if (status == 1) {
            if (selection == 0) {
                cm.sendOk("嗯……你没有未领取的纪念币。现在去消灭阿斯旺的余党可以获得纪念币，你去努力消灭余党吧。");
                cm.dispose();
            } else {
                cm.sendNext("你应该听宰相说过了，不过看样子好像不记得了。你最好整理一下思绪。");
            }
        } else if (status == 2) {
            cm.sendSimple("说说你想知道什么吧。\r\n\r\n#b#L0#我想知道个人奖励有哪些。#k#l");
        } else if (status == 3) {
            cm.sendSimple("有以下个人奖励。说说你想知道什么。\r\n\r\n\r\n#b#L0#请介绍一下内在能力。#k#l\r\n#b#L1#怎样可以获得购买物品时所需的纪念币？#k#l");
        } else if (status == 4) {
            if (selection == 0) {
                beauty = 1;
                cm.sendNext("#b内在能力#k是指隐藏在体内的力量。记住，#b每天最多可进行5次#k，只要帮助消灭阿斯旺里剩余的希拉的余党，你就能获得名声值。");
            } else if (selection == 1) {
                beauty = 2;
                cm.sendNext("#b每日可进行5次#k，在消灭阿斯旺里残余的希拉的余党的过程中，消灭血牙或猫头鹰塔、守护塔后，有一定概率可获得纪念币。");
            }
        } else if (status == 5) {
            if (beauty == 1) {
                cm.sendNextPrev("通过积累名声值，提升荣誉等级后，每达到#b2级、30级、70级#k时各可以获得一种内在能力。#b用于更改内在能力的还原器#k可在每次荣誉等级提升时获得，也可使用消灭余党后获得的纪念币进行购买。");
            } else if (beauty == 2) {
                cm.sendNextPrev("还有，每次完成消灭余党时，扎比埃尔那里会有新的物品出现。可能是阿斯旺的珍奇卷轴，也可能是可以改变内在力量的各种等级的还原器。此外，还有药水或阿斯旺装备，以及可以制作装备的配方等各种物品。所以#r收到扎比埃尔的悄悄话时#k，一定要去看一看。");
            }
            if (!next) {
                cm.dispose();
                return;
            }
        } else if (status == 6) {
            var selStr = "有关个人奖励的说明还没有结束，想知道什么的话，就跟我说。\r\n\r\n\r\n#b"
            if (beauty == 1) {
                selStr += "#L1#怎样可以获得购买物品时所需的纪念币？#k#l";
            } else if (beauty == 2) {
                selStr += "#L0#请介绍一下内在能力。#k#l";
            }
            status = 3;
            next = false;
            cm.sendSimple(selStr);
        }
    }
}