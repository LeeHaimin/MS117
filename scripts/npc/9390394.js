var status = 0;
var random = java.lang.Math.floor(Math.random() * 4);
var eff = "#fUI/UIWindow.img/PvP/Scroll/enabled/next2#";

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status == 0 && mode == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        status--;
    }
    if (cm.getMapId() == 180000001) {
        cm.sendOk("很遗憾，您因为违反用户守则被禁止游戏活动，如有异议请联系管理员.")
        cm.dispose();
    } else if (status == 0) {
        var selStr = "#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#亲爱的#r#h ##k您好，请您选择您需要的功能:\r\n您当前的 #fUI/UIWindow.img/Shop/meso#点券:#r" + 
        cm.getPlayer().getCSPoints(1) + "#k  #fUI/UIWindow.img/Shop/meso#活力值：#r" + cm.getPlayerEnergy() + "#k 点\r\n"+"当前地图ID:"+cm.getPlayer().getMap().getId()+"\r\n";
        selStr += "#L20#" + eff + "#=============BOSS#=============#l\r\n\n";
        selStr += "#L0#" + eff + "#r传送不夜城[主城]#l\r\n";
        selStr += "#L1#" + eff + "#b传送自由市场#l";
        selStr += "  #L2#" + eff + "#b万能传送#l\r\n";
        selStr += "#L3#" + eff + "#b快速转职#l";
        selStr += "  #L4#" + eff + "#b杂货商店#l\r\n";
        selStr += "#L5#" + eff + "#b清理背包#l";
        selStr += "  #L6#" + eff + "#b学习骑宠、群宠技能#l\r\n";
        selStr += "#L7#" + eff + "#b欢乐集市（新手装备商店+点券商店）#l\r\n";
        selStr += "  #L8#" + eff + "#b转蛋屋(抽奖区)#l\r\n";
        selStr += "#L9#" + eff + "#b匠人街(学习专业/制作装备)#l\r\n";
        selStr += "  #L10#" + eff + "#r进入PVP地图#l\r\n";
        selStr += "#L14#" + eff + "#b组队副本中心#l";
        selStr += "#L15#" + eff + "#b点卷兑换#l\r\n";
        selStr += "#L16#" + eff + "#b专属抽奖    #l";
        selStr += "#L17#" + eff + "#b专属货币#l\r\n";
        selStr += "#L19#" + eff + "#=============满技能#=============#l\r\n";
        //selStr += "#L18#" + eff + "#r极品兑换#l";
        cm.sendSimple(selStr);
    } else if (status == 1) {
        switch (selection) {
            case 0:
                cm.warp(741000000, 0);
                cm.dispose();
                break;
            case 1:
                if (cm.getPlayer().getMapId() >= 910000000 && cm.getPlayer().getMapId() <= 910000022) {
                    cm.sendOk("您已经在市场了，还想做什么？");
                } else {
                    cm.saveReturnLocation("FREE_MARKET");
                    cm.warp(910000000, "st00");
                }
                cm.dispose();
                break;
            case 2:
                cm.dispose();
                cm.openNpc(9270035);
                break;
            case 18:
                cm.dispose();
                cm.openNpc(9209000);
                break;
            case 3://怪物暴物品命令
                cm.dispose();
                cm.openNpc(9300011, 0);
                break;
            case 4://管理雇佣商店
                cm.dispose();
                cm.openShop(9310030);
                break;
            case 5://累积充值
                cm.dispose();
                cm.openNpc(9310071, 0);
                break;
            case 6://充值网站
                cm.dispose();
                cm.openNpc(9209005, 0);
                break;
            case 7://在线时间换
                cm.dispose();
                cm.openNpc(9209001, 0);
                break;
            case 8://游戏指导
                cm.dispose();
                cm.warp(749050400, 0);
                break;
            case 9:
                cm.dispose();
                cm.warp(910001000, 0);
                break;
            case 10:
                if (cm.getPlayer().getLevel() > 100) {
                    cm.warp(701000210);
                    cm.sendOk("青龙之门为大混战PK.赤龙之门为组队PK战.黄龙之门为家族PK战.祝您好运");
                } else {
                    cm.sendOk("对不起.PK太危险了.100级以下的玩家不能进入噢.");
                }
                cm.dispose();
                break;
            case 11:
                if (cm.getPlayer().getLevel() > 30) {
                    cm.warp(746000003);
                    cm.sendOk("您已经进入组队PK 祝你好运");
                } else {
                    cm.sendOk("对不起.PK太危险了.只允许等级不小于30级的玩家进入");
                }
                cm.dispose();
                break;
                break;
            case 12:
                if (cm.getPlayer().getLevel() > 30) {
                    cm.warp(746000004);
                    cm.sendOk("您已经进入家族PK 祝你好运");
                } else {
                    cm.sendOk("对不起.PK太危险了.只允许等级不小于30级的玩家进入");
                }
                cm.dispose();
                break;
            case 13:
                cm.dispose();
                cm.openNpc(9900001, 3000);
                break;
            case 14:
                cm.dispose();
                cm.warp(910002000);
                break;
            case 15:
                cm.dispose();
                cm.openNpc(9330019, 0);
                break;
            case 16:
                cm.dispose();
                cm.openNpc(9330029, 0);
                break;
            case 17:
                cm.dispose();
                cm.openNpc(9310072, 0);
                break;
            case 19:
                cm.dispose();
                cm.openNpc(9900004, 999);
                break;
            case 20:
                // cm.getPlayer().showMonster(8800002,994, 184,100);
                cm.getPlayer().getMap().spawnChaosZakum(0, 0);//getMonsterById(8800002);
                cm.dispose();
                // new java.awt.Point(994, 184);
                // cm.getPlayer().getMap().spawnMonsterOnGroundBelow(monster,new java.awt.Point(994, 184));
                // cm.dispose();
                // cm.openNpc(9999999);
                break;
        }
    }
}