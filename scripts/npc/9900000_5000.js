/** Author: nejevoli
	NPC Name: 		NimaKin
	Map(s): 		Victoria Road : 隐秘之路 - 工作场所 (180000000)
	Description: 		Maxes out your stats and able to modify your equipment stats
*/
importPackage(java.lang);

var status = 0;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status >= 0 && mode == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        status--;
    }
    if (status == 0) {
        if (cm.getPlayerStat("GM") == 1) {
            cm.sendSimple("亲爱的管理员#r#h ##k您好，您有什么事情需要我帮忙的吗?#b\r\n#L0#战士套装#l\r\n#L1#法师套装#l\r\n#L2#飞侠套装#l\r\n#L3#弓手套装#l\r\n#L4#海盗套装#l#k");
        } else if (cm.getPlayerStat("GM") == 1) {
            cm.sendSimple("亲爱的管理员#r#h ##k您好，您有什么事情需要我帮忙的吗?#b\r\n#L0#加满属性#l\r\n#L1#加满所有技能#l\r\n#L4#初始化AP/SP#l\r\n#L7#初始化属性#k");
        } else {
            cm.dispose();
        }
    } else if (status == 1) {
        if (selection == 0) {
	cm.gainItem(1003797,1);
	cm.gainItem(1042254,1);
	cm.gainItem(1062165,1);
	cm.gainItem(1102481,1);
	cm.gainItem(1082543,1);
	cm.gainItem(1072743,1);
	cm.gainItem(1032223,1);
        } else if (selection == 1) {
	cm.gainItem(1003798,1);
	cm.gainItem(1042255,1);
	cm.gainItem(1062166,1);
	cm.gainItem(1102482,1);
	cm.gainItem(1082544,1);
	cm.gainItem(1072744,1);
	cm.gainItem(1032223,1);
        } else if (selection == 2) {
	cm.gainItem(1003800,1);
	cm.gainItem(1042257,1);
	cm.gainItem(1062168,1);
	cm.gainItem(1102484,1);
	cm.gainItem(1082546,1);
	cm.gainItem(1072746,1);
	cm.gainItem(1032223,1);
        } else if (selection == 3) {
	cm.gainItem(1003799,1);
	cm.gainItem(1042256,1);
	cm.gainItem(1062167,1);
	cm.gainItem(1102483,1);
	cm.gainItem(1082545,1);
	cm.gainItem(1072745,1);
	cm.gainItem(1032223,1);
        } else if (selection == 4) {
	cm.gainItem(1003801,1);
	cm.gainItem(1042258,1);
	cm.gainItem(1062169,1);
	cm.gainItem(1102485,1);
	cm.gainItem(1082547,1);
	cm.gainItem(1072747,1);
	cm.gainItem(1032223,1);
        }
	cm.sendOk("领取成功");
	cm.dispose();
	}
}