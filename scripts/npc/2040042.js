/*
	NPC Name: 		Sgt. Anderson
	Map(s): 		Ludibrium PQ Maps
	Description: 		Warps you out from Ludi PQ
*/

function start() {
    if (cm.getMapId() != 922010000) {
        cm.sendYesNo("你确定要过关吗?");
    } else {
        if (cm.haveItem(4001022)) {
            cm.removeAll(922010800);
        }
        if (cm.haveItem(4001023)) {
            cm.removeAll(922010800);
        }
        cm.warp(922010800, 0);
        cm.dispose();
    }
}

function action(mode, type, selection) {
    if (mode == 1) {
        cm.warp(922010800, 0);
    }
    cm.dispose();
}