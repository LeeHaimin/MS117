var status = 0;
var eff ="#fUI/UIWindow/Quest/icon6/7#";

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status == 0 && mode == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        status--;
    }
    if (status == 0) {
        var selStr = "#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#亲爱的#r#h ##k您好，请您选择您需要的功能(神之子不能领取100级前的等级奖励):\r\n#b#L0#" + eff + "领取10级等级奖励#l\r\n#L1#" + eff + "领取60级等级奖励#l\r\n#L2#" + eff + "领取100级等级奖励#l\r\n#L3#" + eff + "领取180级等级奖励#l\r\n#L4#" + eff + "领取250级等级奖励#l";
        cm.sendSimple(selStr);
    } else if (status == 1) {
        switch (selection) {
        case 0:
if(cm.getBossLog("10级奖励",1) < 1 && (cm.getPlayer().getLevel() > 9 && cm.getPlayer().getLevel() < 100) && (cm.getSpace(5)>1 && cm.getSpace(1)>9)){
cm.gainItem(1012057,1,7);//透明面具
cm.gainItem(1002186,1,7);//透明头盔
cm.gainItem(1102039,1,7);//
cm.gainItem(1082102,1,7);//
cm.gainItem(1092064,1,7);//
cm.gainItem(1072153,1,7);//
cm.gainItem(1702224,1,7);//
cm.gainItem(1022048,1,7);//
cm.gainItem(1032024,1,7);
cm.gainMeso(200000);
cm.gainItem(2431305,1);
cm.setBossLog("10级奖励",1);
cm.sendOk("你成功领取奖励");
cm.worldMessage(cm.getChar().getName() + "玩家成功领取10级奖励.");
cm.dispose();
} else {
cm.sendOk("你的等级不满足条件，或没有足够的背包空间\r\n#b(装备栏9个空位以上,特殊栏1个空位以上).\r\n注:等级奖励只能领取一次.");
cm.dispose();
}
            break;
        case 1:
if(cm.getBossLog("50级奖励",1) < 1 && (cm.getPlayer().getLevel() > 59 && cm.getPlayer().getLevel() < 100) && cm.getSpace(5)>2){
cm.gainMeso(+1000000);
cm.gainItem(2431305,1);
cm.gainItem(5062000,6);
cm.gainItem(4310088,100);
cm.setBossLog("50级奖励",1);
cm.sendOk("你成功领取奖励");
cm.worldMessage(cm.getChar().getName() + "玩家成功领取60级奖励.");
cm.dispose();
} else {
cm.sendOk("你的等级不满足条件，或没有足够的背包空间\r\n#b(特殊栏3个空位以上).\r\n注:等级奖励只能领取一次.");
cm.dispose();
}
            break;
        case 2:
if(cm.getBossLog("100级奖励",1) < 1 && (cm.getPlayer().getLevel() > 99 && cm.getPlayer().getLevel() < 250) && cm.getSpace(5)>1){
cm.gainItem(5390010,5);
cm.gainItem(2431305,1);
cm.gainItem(5390000,20);
cm.setBossLog("100级奖励",1);
cm.sendOk("你成功领取奖励");
cm.worldMessage(cm.getChar().getName() + "玩家成功领取100级奖励.");
cm.dispose();
} else {
cm.sendOk("你的等级不满足条件，或没有足够的背包空间\r\n#b(特殊栏2个空位以上).\r\n注:等级奖励只能领取一次.");
cm.dispose();
}
            break;
        case 3:
if(cm.getBossLog("150级奖励",1) < 1 && (cm.getPlayer().getLevel() > 179 && cm.getPlayer().getLevel() < 250) && (cm.getSpace(5)>2 && cm.getSpace(3)>1)){
cm.gainItem(5062000,10);
cm.gainItem(5064000,5);
cm.gainItem(3010155,1);
cm.gainItem(5390002,5);
cm.gainItem(4310088,300);
cm.setBossLog("150级奖励",1);
cm.sendOk("你成功领取奖励");
cm.worldMessage(cm.getChar().getName() + "玩家成功领取180级奖励.");
cm.dispose();
} else {
cm.sendOk("你的等级不满足条件，或没有足够的背包空间\r\n#b(特殊栏3个空位以上,设置栏1个空位以上).\r\n注:等级奖励只能领取一次.");
cm.dispose();
}
            break;
        case 4:
if(cm.getBossLog("200级奖励",1) < 1 && cm.getPlayer().getLevel() == 250 && cm.getSpace(5)>2){
cm.gainItem(5062002,15);
cm.gainItem(5064000,10);
cm.gainItem(2028013,10);
cm.gainItem(2028011,10);
cm.gainItem(4310088,2000);
cm.setBossLog("200级奖励",1);
cm.sendOk("你成功领取奖励");
cm.worldMessage(cm.getChar().getName() + "玩家成功领取250级奖励.");
cm.dispose();
} else {
cm.sendOk("你的等级不满足条件，或没有足够的背包空间\r\n#b(特殊栏3个空位以上).#b\r\n注:等级奖励只能领取一次.");
cm.dispose();
}
            break;
        }
    }
}