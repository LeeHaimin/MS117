var status = 0;
var eff ="#fUI/UIWindow/Quest/icon6/7#";

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (status == 0 && mode == 0) {
        cm.dispose();
        return;
    }
    if (mode == 1) {
        status++;
    } else {
        status--;
    }
    if (status == 0) {
      //  var selStr = "#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#亲爱的#r#h ##k您好，请您选择您需要的功能:\r\n#b#L0#" + eff + "打开装备商店[#r装备/箭失/手仗/卡片/宝盒等#b]#l\r\n#L2#" + eff + "打开杂货店[#r药水/挑战物品/宠物食品/等#b]#l\r\n#L4#" + eff + "打开火炮手装备店#l\r\n#L5#" + eff + "打开双刀装备店#l\r\n#L7#" + eff + "打开飞标专卖店#l\r\n#L11#" + eff + "打开30级副手专卖店#l\r\n#L12#" + eff + "打开60级副手专卖店#l\r\n#L13#" + eff + "打开100级副手专卖店#l\r\n#L10#" + eff + "我要购买外星人装备#l\r\n#L6#" + eff + "我要购买其它职业装备#l"; //#L9#狮子王道具兑换#r[New]#k#l\r\n
        var selStr = "#fUI/UIWindow2.img/Quest/quest_info/summary_icon/summary#\r\n#fUI/UIWindow2.img/QuestAlarm/BtQ/normal/0#亲爱的#r#h ##k您好，请您选择您需要的功能:\r\n";
		selStr += "#L0#" + eff + "#b全职业副手武器商店(#r100级以下#b)#l\r\n";
		//selStr += "#L1#" + eff + "#b杂货店(#r药水/飞镖/BOSS挑战物品/宠物食品/.#b)#l\r\n";	
		//selStr += "#L2#" + eff + "#b飞镖箭子弹店#l\r\n\r\n";		
		//selStr += "#L3#" + eff + "#b我要购买外星人装备#l\r\n\r\n";		
		selStr += "#L4#" + eff + "#b战士装备(#r新手装备#b)#l";
		selStr += "#L5#" + eff + "#b法师装备(#r新手装备#b)#l\r\n";
		selStr += "#L6#" + eff + "#b箭手装备(#r新手装备#b)#l";
		selStr += "#L7#" + eff + "#b飞侠装备(#r新手装备#b)#l\r\n";
		selStr += "#L8#" + eff + "#b海盗装备(#r新手装备#b)#l";
		//selStr += "#L9#" + eff + "#b幻影装备(#r100级以#b)#l\r\n";
		//selStr += "#L10#" + eff + "#b双刀装备(#r100级以下#b)#l";
		//selStr += "#L11#" + eff + "#b尖兵装备(#r100级以下#b)#l\r\n";	
		//selStr += "#L12#" + eff + "#b萝莉装备(#r100级以下#b)#l";
		//selStr += "#L13#" + eff + "#b夜光装备(#r100级以下#b)#l\r\n";			
		//selStr += "#L14#" + eff + "#b火炮装备(#r100级以下#b)#l";
		//selStr += "#L15#" + eff + "#b双弩装备(#r100级以下#b)#l\r\n";	
		selStr += "#L16#" + eff + "#b恶魔复仇者装备(#r100级以下#b)#l\r\n\r\n\r\n";		
		selStr += "#b温馨提示:特殊飞镖请到废弃药店冲镖哦!#l";
		cm.sendSimple(selStr);
    } else if (status == 1) {
        switch (selection) {
        case 0:
			cm.dispose();
			cm.openShop(1033001);
			break;
        case 1:
			cm.dispose();
			cm.openShop(9090000);
			break;			
        case 2:
            cm.dispose();
            cm.openShop(1012123);
            break;
        case 3://外星人装备
           /* cm.dispose();
            cm.openShop(9090000);*/
			cm.dispose();
            cm.openShop(9310117);
            break;
        case 4://战士
            cm.dispose();
            cm.openShop(1021000);
            /*cm.dispose();
            cm.openShop(1012124);*/
            break;
        case 5://法师
            /*cm.dispose();
            cm.openShop(1012125);*/		
            cm.dispose();
            cm.openShop(1031000);
            break;
        case 6://弓手装备
           /* cm.dispose();
            cm.sendOk("其它职业的装备请到各个城市的装备商店购买.\r\n高级装备可以靠刷怪/BOSS/抽奖/活动/获得...\r\n祝你游戏愉快!如果有好的建议请联系管理员.");*/
            cm.dispose();
            cm.openShop(1011000);
			break;
        case 7://飞侠
		    cm.dispose();
            cm.openShop(1051000);
			break;
        case 8://海盗
            cm.dispose();
            cm.openShop(1091000);
            break;
        case 9://幻影
            /*cm.dispose();
            cm.openShop(2161010);*/		
            cm.dispose();
            cm.openShop(1001000);
            break;
        case 10://双刀
           /* cm.dispose();
            cm.openShop(9310117);*/
            cm.dispose();
            cm.openShop(9120000);			
            break;
        case 11://尖兵
            /*cm.dispose();
            cm.openShop(1011101);*/
            cm.dispose();
            cm.openShop(2150001);
            break;
        case 12://萝莉
            /*cm.dispose();
            cm.openShop(9310111);*/
            cm.dispose();
            cm.openShop(2142912);
            break;
        case 13://夜光
            /*cm.dispose();
            cm.openShop(9000188);*/
            cm.dispose();
            cm.openShop(1400001);
            break;
        case 14://火炮
            cm.dispose();
            cm.openShop(1200001);
            break;
        case 15://双弩
            cm.dispose();
            cm.openShop(1100001);
            break;
        case 16://恶魔复仇者
			cm.dispose();
			cm.openShop(1033001);
            break;			
        }
    }
}