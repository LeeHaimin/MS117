var status = 0;
var fstype = 0;

function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == -1) {
        cm.dispose();
    } else {
        if (mode == 0) {
            cm.dispose();
            return;
        }
        if (mode == 1)
            status++;
        if (status == 0) {
            if (cm.getBossLog("开始奖励", 1) < 1) {
                var text = "";
                //text = "  #fUI/UIWindow2.img/StagedGachapon/Creature/0/normal/2#   #fUI/UIWindow2.img/Megaphone/0##fUI/UIWindow2.img/Megaphone/0# 欢迎来到#r#e冒险岛online#fUI/UIWindow2.img/Megaphone/0##fUI/UIWindow2.img/Megaphone/0#   #fUI/UIWindow2.img/StagedGachapon/Creature/0/normal/2#\r\n";
                text = "#k亲爱的 #r#h # #k为您准备了一些礼物\r\n怎么样~您决定现在就领取这些礼物吗?\r\n";
                text += "#L1##b嗯，是的，我现在就领取。#k#l\r\n";
                text += "#L2##b算了，我不需要。#k#l\r\n";
                cm.sendSimple(text);
            } else {
                cm.sendOk("您现在可以从右边传送点出去开始您的冒险之旅。背包里的箱子一定要打开哦记得等级达到10级时候点击拍卖开始第一次转职哦!");
                cm.dispose();
            }
        } else if (selection == 1) {
               cm.gainItem(2430241, 1);	// 新手礼包
            cm.setBossLog("开始奖励", 1);

            cm.sendOk("领取成功,祝您游戏愉快。");
            cm.dispose();
        } else if (selection == 2) {
            cm.sendOk("好的,那你想领取了在来找我吧~~~");
            cm.dispose();
        }
    }
}