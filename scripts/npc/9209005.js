/* 
	Joyce
	Event NPC
	学习技能
*/

var status = -1;

function start() {
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == 1) {
        status++;
    } else {
        if (status == 0) {
            cm.dispose();
            return;
        }
        status--;
    }
    if (status == 0) {
        cm.sendSimple("您好 #r#h ##k 有什么需要我帮忙的吗？\r\n#b#L1#群宠技能#l  #L2#骑宠技能#l#k    #r#L4#飞行骑乘#l#k");
    } else if (status == 1) {
        if (selection == 1) { //群宠技能
            if (cm.getPlayer().getSkillLevel(8) > 0 || cm.getPlayer().getSkillLevel(10000018) > 0 || cm.getPlayer().getSkillLevel(20000024) > 0 || cm.getPlayer().getSkillLevel(20011024) > 0) {
                cm.sendOk("您已经学习过群宠技能。");
            } else {
                if (cm.getJob() == 2001 || (cm.getJob() >= 2200 && cm.getJob() <= 2218)) {
                    cm.teachSkill(20011024, 1, 0); // 龙神 - 群宠
                } else if (cm.getJob() == 2000 || (cm.getJob() >= 2100 && cm.getJob() <= 2112)) {
                    cm.teachSkill(20000024, 1, 0); // 战神 - 群宠
                } else if (cm.getJob() >= 1000 && cm.getJob() <= 1512) {
                    cm.teachSkill(10000018, 1, 0); // 骑士团 - 群宠
                } else {
                    cm.teachSkill(8, 1, 0); // 冒险家 - 群宠
                }
                cm.sendOk("恭喜您学习群宠技能成功。");
            }
            cm.dispose();
        } else if (selection == 2) { //骑兽技能
            if (cm.getPlayer().getSkillLevel(80001000) > 0) {
                cm.sendOk("您已经学习过骑兽技能。");
            } else {
                cm.teachSkill(80001000, 1, 0);
                cm.sendOk("恭喜您学习骑兽技能成功。");
            }
            cm.dispose();
        } else if (selection == 3) { //骑兽商店
            cm.openNpc(9270035,2)
            cm.dispose();
        } else if (selection == 4) { //飞行骑乘
            if (cm.getPlayer().getSkillLevel(80001089) > 0) {
                cm.sendOk("您已经学习过飞行骑乘这个技能。");
            } else {
                cm.teachSkill(80001089, 1, 0);
                cm.sendOk("恭喜您学习飞行骑乘技能成功。");
            }
            cm.dispose();
        }
    }
}