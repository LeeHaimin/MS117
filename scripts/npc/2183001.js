/*
 *	阿斯旺 - 阿斯旺解放战
 */

var status = -1;
var minLevel = 40;
var maxCount = 5;
var minPartySize = 1;
var maxPartySize = 4;

function start() {
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == -1) {
        cm.dispose();
    } else {
        if (status >= 0 && mode == 0) {
            cm.dispose();
            return;
        }
        if (mode == 1) {
            status++;
        } else {
            status--;
        }
        if (status == 0) {
            cm.sendSimple("#e<阿斯旺解放战>#n\r\n你愿意去消灭依然徘徊在阿斯旺地区的希拉的余党吗？#b\r\n\r\n\r\n#L1#消灭希拉的余党。(40级以上。剩余入场次数: " + (maxCount - cm.getBossLog("阿斯旺")) + "次)#l\r\n#L0#直接迎战希拉(120级以上)#l");
        } else if (status == 1) {
            if (selection == 0) {
                if (cm.getLevel() >= 120) {
                    cm.sendNext("现在你将到达希拉之塔入口，请务必消灭希拉吧。");
                } else {
                    cm.sendOk("以你现在的实力，对战希拉有些勉强。必须达到120级以上才能进行挑战。");
                    cm.dispose();
                }
            } else {
                if (cm.getPlayer().getParty() == null) {
                    cm.sendOk("必须组队入场。");
                } else if (!cm.isLeader()) {
                    cm.sendOk("你不是队长啊？让队长来和我说话。");
                } else {
                    var party = cm.getPlayer().getParty().getMembers();
                    var mapId = cm.getPlayer().getMapId();
                    var next = 0;
                    var levelValid = 0;
                    var inMap = 0;
                    var it = party.iterator();
                    while (it.hasNext()) {
                        var cPlayer = it.next();
                        var ccPlayer = cm.getPlayer().getMap().getCharacterById(cPlayer.getId());
                        if (ccPlayer == null) {
                            next = 1;
                        }
                        if (ccPlayer != null && ccPlayer.getLevel() >= minLevel) {
                            levelValid += 1;
                        } else {
                            next = 2;
                        }
                        if (ccPlayer != null && ccPlayer.getMapId() == mapId) {
                            inMap += 1;
                        }
                        if (ccPlayer != null && ccPlayer.getBossLog("阿斯旺") >= maxCount) {
                            next = 4;
                        }
                    }
                    if (party.size() > maxPartySize || inMap < minPartySize) {
                        next = 3;
                    }
                    if (next == 1) {
                        cm.sendOk("队伍中有玩家不在此地图。");
                    } else if (next == 2) {
                        cm.sendOk("队伍中有玩家的等级不符合。必须 " + minLevel + "级以上的队员，才能进去。");
                    } else if (next == 3) {
                        cm.sendOk("队员不够" + minPartySize + "人。至少必须有" + minPartySize + "个" + minLevel + "级以上的队员，才能进去。");
                    } else if (next == 4) {
                        cm.sendOk("队伍中有玩家的入场次数已经用完。");
                    } else if (next == 0) {
                        var em = cm.getEventManager("Aswan");
                        if (em == null) {
                            cm.sendOk("当前服务器未开启此功能，请稍后在试...");
                        } else {
                            var prop = em.getProperty("state");
                            if (prop.equals("0") || prop == null) {
                                cm.setPartyBossLog("阿斯旺");
                                em.startInstance(cm.getPlayer().getParty(), cm.getPlayer().getMap(), 200);
                            } else {
                                cm.sendOk("当前频道已有玩家在进行任务中，请稍后在试。");
                            }
                        }
                    } else {
                        cm.sendOk("队员不够" + minPartySize + "人。这里非常危险。至少必须有" + minPartySize + "个" + minLevel + "级以上的队员，才能进去。");
                    }
                }
                cm.dispose();
            }
        } else if (status == 2) {
            cm.warp(262030000, 0); //希拉之塔 - 希拉之塔入口
            cm.dispose();
        }
    }
}