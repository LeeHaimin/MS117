/* Kedrick
	Fishking King NPC
*/

var status = -1;
var sel;

function action(mode, type, selection) {
    if (mode == 1) {
        status++;
    } else {
        status--;
    }
    if (status == 0) {
        cm.sendSimple("你好！我是渔场管理员.\n\r如果你想要进行钓鱼，请到[商城]购买[钓鱼竿]，钓鱼竿分#v5340001:#/#v5340000:#两种，高级10秒钓一次鱼，普通的是20秒一次，好了介绍这么多，多多搜集兑换丰厚奖励吧！\n\r #b#L0#进入钓鱼场#l\n\r #L2#购买钓鱼用的椅子(5万金币)#l \n\r #L3#兑换高级鱼饵#l \n\r #L5##i1142146:#交易 500 条黄金鱼[期限 : 30 天])#l");
    } else if (status == 1) {
        sel = selection;
        if (sel == 0) {
            if (cm.haveItem(5340000) || cm.haveItem(5340001)) {
                if (cm.haveItem(3011000)) {
                    cm.saveLocation("FISHING");
                    cm.warp(741000200);
                    cm.dispose();
                } else {
                    cm.sendNext("你必须先购买钓鱼用的椅子和钓鱼竿!");
                    cm.safeDispose();
                }
            } else {
                cm.sendNext("你需要要有钓鱼椅和鱼竿、鱼饵!");
                cm.safeDispose();
            }
        } else if (sel == 1) {
            cm.sendYesNo("It requires 3000 meso for 120 baits. Do you want to purchase?");
        } else if (sel == 2) {
            if (cm.haveItem(3011000)) {
                cm.sendNext("You already have a fishing chair. Each character can only have 1 fishing chair.");
            } else {
                if (cm.canHold(3011000) && cm.getMeso() >= 50000) {
                    cm.gainMeso( - 50000);
                    cm.gainItem(3011000, 1);
                    cm.sendNext("Happy Fishing~");
                } else {
                    cm.sendOk("Please check if you have the required meso or sufficient inventory slot.");
                }
            }
            cm.safeDispose();
        } else if (sel == 3) {
            if (cm.canHold(2300001, 120) && cm.haveItem(5350000, 1)) {
                if (!cm.haveItem(2300009)) {
                    cm.gainItem(2300001, 100);
                    cm.gainItem(5350000, -1);
                    cm.sendNext("Happy Fishing~");
                } else {
                    cm.sendNext("你没有可以兑换的高级鱼饵.");
                }
            } else {
                cm.sendOk("你没有可以兑换的高级鱼饵.请检查你的背包是否有空余.");
            }
            cm.safeDispose();
        } else if (sel == 4) {
            cm.sendOk("You need to be above level 10, with a fishing rod, fishing baits and a fishing chair in order to enter the Fishing Lagoon. You will reel in a catch every 1 minute. Talk to lagoon's NPC Madrick to check out your catch record!");
            cm.safeDispose();
        } else if (sel == 5) {
            if (cm.haveItem(4000518, 500)) {
                if (cm.canHold(1142146)) {
                    cm.gainItem(4000518, -500);
                    cm.gainItemPeriod(1142146, 1, 30);
                    cm.sendOk("Woah, I guess you must have spend quite a lot of effort in the Fishing Lagoon fishing for these eggs. Here, take it. The #bFishing King Medal#k!")
                } else {
                    cm.sendOk("Please check if you have sufficient inventory slot for it.");
                }
            } else {
                cm.sendOk("Please get me 500 #i4000518:# Golden Fish Egg in exchange for a Fishing King medal!")
            }
            cm.safeDispose();
        }
    } else if (status == 2) {
        if (sel == 1) {
            if (cm.canHold(2300000, 120) && cm.getMeso() >= 3000) {
                if (!cm.haveItem(2300000)) {
                    cm.gainMeso( - 3000);
                    cm.gainItem(2300000, 120);
                    cm.sendNext("Happy Fishing~");
                } else {
                    cm.sendNext("You already have fishing bait.");
                }
            } else {
                cm.sendOk("Please check if you have the required meso or sufficient inventory slot.");
            }
            cm.safeDispose();
        }
    }
}