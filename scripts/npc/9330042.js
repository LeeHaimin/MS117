var status = 0;
var choice;
var scrolls = Array(2028048,2430462,2431738);
var scrolls1 = Array(50,50,100);
/*
* 魔力冒险岛
*/
function start() {
    status = -1;
    action(1, 0, 0);
}

function action(mode, type, selection) {
    if (mode == -1)
        cm.dispose();
    else {
        if (status == 0 && mode == 0) {
            cm.dispose();
            return;
        } else if (status >= 1 && mode == 0) {
            cm.sendOk("好吧，欢迎下次继续光临！.");
            cm.dispose();
            return;
        }
        if (mode == 1)
            status++;
        else
            status--;

        if (status == 0) {
            var choices = "";
            for (var i = 0; i < scrolls.length; i++) {
                choices += "\r\n#L" + i + "##v" + scrolls[i] + "##z" + scrolls[i] + "#　#d需要#r" + scrolls1[i] + "个#d#z4000463##k#l";
            }
            cm.sendSimpleS(choices,2);
        } else if (status == 1) {
            cm.sendYesNo("#b你确定需要购买这个物品么？这将花费你" + scrolls1[selection] + "个#z4000463#！！#k" +"\r\n#v" + scrolls[selection] + "##t" + scrolls[selection] + "#");
            choice = selection;
        } else if (status == 2) {
            if(cm.haveItem(4000463,scrolls1[choice])) {
                cm.gainItem(4000463,-scrolls1[choice]);//木妖
                cm.gainItem(scrolls[choice], 1);
                cm.sendOk("谢谢你的光顾，你购买的物品已经放入你的背包！.");
                cm.dispose();
            } else {
                cm.sendOk("抱歉，你没足够的#z4000463#.");
                cm.dispose();
            }
        }
    }
}
