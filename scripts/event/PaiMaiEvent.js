/*
 明珠港冒险岛所有
 脚本功能：拍卖系统后台控制
 */

importPackage(java.sql);
importPackage(java.lang);

var time = new Date();
var hour = time.getHours();
var min = time.getMinutes();
var sec = time.getSeconds();
var setupTask;
var PaiMaiIdOnly;
var PaiMaiIdOnly1;
var itemName;


var MaxPrice;
var MaxPlayer;



function init() {
    scheduleNew();
}

function scheduleNew() {


    var cal = java.util.Calendar.getInstance();
    cal.set(java.util.Calendar.HOUR, 0);
    cal.set(java.util.Calendar.MINUTE, 0);
    cal.set(java.util.Calendar.SECOND, 0);
    var nextTime = cal.getTimeInMillis();
    while (nextTime <= java.lang.System.currentTimeMillis()) {
        nextTime += 1000 * 60 * 1;//5分钟检查一次时间
    }
    setupTask = em.scheduleAtTimestamp("startEvent", nextTime);
}

function cancelSchedule() {
    setupTask.cancel(true);
}

function startEvent() {
    scheduleNew()
   // if (hour == 0) {//每日0点执行刷新
        var ItemDataBase = em.getDataSelectFromDB("SELECT * FROM PaiMaiNpc WHERE bought = 0 LIMIT 1") //查询拍卖数据库
        var PaiMaiDataBase = em.getDataSelectFromDB("SELECT * FROM paimainpcrecord")//得到记录数据库
        var UpDateData = em.getDataInsertFromDB("update PaiMaiNpc set bought=? where bought=0")
        var UpDateMax = em.getDataInsertFromDB("update paimainpcrecord set status=? where status=0")
        var RecordMax = em.getDataSelectFromDB("select PaiMaiId,CharName,Price,Status from PaiMaiNpcRecord where Price=(select max(Price) from PaiMaiNpcRecord where status = 0)") //取最大
        var i = 0;
        while (PaiMaiDataBase.next()) {
            PaiMaiIdOnly = PaiMaiDataBase.getString("PaiMaiId");
        }//每次拍卖都只能1个 所以PaiMaiIdOnly不设置为数组

        while (ItemDataBase.next()) {
            itemName = ItemDataBase.getString("itemName");
            if (ItemDataBase.getString("PaiMaiId") == PaiMaiIdOnly) {//拍卖ID必须一致
                UpDateData.setString(1, 1)
                UpDateData.executeUpdate();//更新;
                //完成兑换后，直接在NPC将这个字段改成角色ID
            }
            i++;
        }//将拍卖为0状态的改成为1状态

        while (RecordMax.next()) {//获取最多角色讯息
            if (RecordMax.getString("PaiMaiId") == PaiMaiIdOnly) {
                MaxPlayer = RecordMax.getString("CharName");
                MaxPrice = RecordMax.getString("Price");
                UpDateMax.setString(1, PaiMaiIdOnly)
                UpDateMax.executeUpdate();//更新;
            }
        }
        var time = parseInt(PaiMaiIdOnly) + 1;
        if (i != 0) {
            em.broadcastServerMsg(5120025, "[拍卖系统] 第 （" + PaiMaiIdOnly + "） 期拍卖已经结束。 \r\n第 （" + time + "） 期现在开始！", true);
            em.broadcastServerMsg(4, "[拍卖小快报] 第" + PaiMaiIdOnly + "期拍卖已经结束，本期拍卖的道具为：" + itemName + "。在本期拍卖中，玩家（" + MaxPlayer + "）以全服务器最高的价格（" + MaxPrice + "）成功竞拍了本期的道具。大家一起来祝贺（他/她）吧！", false);
            em.broadcastServerMsg(6, "[拍卖小快报] 第" + PaiMaiIdOnly + "期拍卖已经结束，本期拍卖的道具为：" + itemName + "。在本期拍卖中，玩家（" + MaxPlayer + "）以全服务器最高的价格（" + MaxPrice + "）成功竞拍了本期的道具。大家一起来祝贺（他/她）吧！", false);
        }
        // - 0 正在拍卖
        // - 1 拍卖结束，清算结算
        // - CharId 角色ID归属已经拍卖成功
    //}
}
