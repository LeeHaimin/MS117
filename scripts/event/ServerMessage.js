var Message = new Array(
"欢迎来到欢乐岛,有任何问题联系管理员小心。", 
"使用 @help 命令，可以查看你当前能使用的命令列表。", 
"如果无法和NPC进行对话，请使用 @ea 命令。", 
"在一些常去的城市地图可以点击左边的快捷移动，来快速移动到自由市场，匠人街等等地图。", 
"玩家可以到专业技术地图学习各种生活技能。",
"本服主城在不夜城可以使用命令@回城",
"有任何问题可以在交流群中交流.");

var setupTask;

function init() {
    scheduleNew();
}

function scheduleNew() {
    setupTask = em.schedule("start", 900000);
}

function cancelSchedule() {
    setupTask.cancel(false);
}

function start() {
    scheduleNew();
    em.broadcastServerMsg("[公告事项] " + Message[Math.floor(Math.random() * Message.length)]);
}