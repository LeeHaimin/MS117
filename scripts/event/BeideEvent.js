/*
 863000100- 入场
 863010100 - 绯红 - 通往贝勒德的路  分左边 和右边（判断坐标，身体的左边和右边）， 中间是离开。
 
 左边：                                                    			右边：
 863010220 - 绯红 - 贝勒德左腿下层		863010200 - 绯红 - 贝勒德右腿下层			      两边只要打一边就可以了。
 863010230 - 绯红 - 贝勒德左腿上层		863010210 - 绯红 - 贝勒德右腿上层   						
 两边都打完后：
 863010240 - 绯红 - 贝勒德的肚脐          9390612    -11,87
 
 左边：										右边：
 863010400 - 绯红 - 贝勒德东部悬崖下方					863010300 - 绯红 - 贝勒德西部悬崖下方																		
 863010410 - 绯红 - 贝勒德东部悬崖上方					863010310 - 绯红 - 贝勒德西部悬崖上方
 863010420 - 绯红 - 贝勒德的左手上臂					863010320 - 绯红 - 贝勒德的右手上臂
 863010430 - 绯红 - 贝勒德的左手肩膀   如果掉了，会掉在863010220		863010330 - 绯红 - 贝勒德的右手肩膀    如果掉了，会掉在863010200    两边打一边就可以了。  9390610,0,69
 9390611, 1,86,70
 
 
 863010600 - 绯红 - 贝勒德的头  BOSS关
 9390600, 1,0,0
 
 设定：如果从左边地图上的话，道具设定A数组，如果在右边地图上的话，道具设定B数组
 --- 》 中间可以加一个动画效果
 BOSS打完后，传送到863000920，之后全图自动暴东西！
 */
var MapList = Array(
        863010100, //绯红 - 通往贝勒德的路  分左边 和右边（判断坐标，身体的左边和右边）， 中间是离开。
        863010220, //绯红 - 贝勒德左腿下层
        863010230, //绯红 - 贝勒德左腿上层	
        863010200, //绯红 - 贝勒德右腿下层
        863010210, //绯红 - 贝勒德右腿上层   	
        863010240, //绯红 - 贝勒德的肚脐 ->可以选择不打，但是物品的暴率会减少，头的血量会多
        863010400, //绯红 - 贝勒德东部悬崖下方	
        863010410, //绯红 - 贝勒德东部悬崖上方		
        863010420, //绯红 - 贝勒德的左手上臂	
        863010430, //绯红 - 贝勒德的左手肩膀  如果掉了，会掉在863010220	
        863010300, //绯红 - 贝勒德西部悬崖下方		
        863010310, //绯红 - 贝勒德西部悬崖上方
        863010320, //绯红 - 贝勒德的右手上臂
        863010330, //绯红 - 贝勒德的右手肩膀 如果掉了，会掉在863010200    两边打一边就可以了。
        863000900, //BOSS
        863000920 //奖励关卡
        );

var mobid, mob, modified;
var 宿主花 = 1000000000;
var 肚脐血量 = 80000000000;
var 肩膀血量 = 6000000000;
var 总BOSS头血量 = 110000000000000;
var dropItems = new Array(
		new Array(9390600, 4034151, 99999, 1, 1),//贝勒德的头
		new Array(9390600, 3010936, 200, 1, 1)
        //怪物ID，物品ID，爆率，最小值，最大值
        );


function init() {
    em.setProperty("position", "empty"); //判断这个组队由哪个方向进入。
}

function monsterValue(eim, mobId) {
    if (mobId == 9390612) {
        for (var i = 0; i < eim.getPlayerCount(); i++) {
            eim.getPlayers().get(i).dropMessage(6, "[决战贝勒德] 现在有60秒的时间捡取奖励。");
            eim.getPlayers().get(i).getClient().getSession().write(Packages.tools.MaplePacketCreator.getPVPClock(3, 60));//60秒的时间自动出去
        }
        //eim.getMapInstance(350060160).spawnNpc(9390125, new java.awt.Point(0, 0));
        setupTask = em.schedule("clearPQ", 1000 * 60 * 1, eim);//10分钟的任务时间
    }
    return 1;
}


function monsterDrop(eim, player, mob) {
    var mobid = mob.getId();
    var toDrop = new Array();
    for (var i = 0; i < dropItems.length; i++) {
        if (mobid != dropItems[i][0])
            continue;
        var chance = Math.floor(Math.random() * 999999);
        if (chance < dropItems[i][2]) {
            var minQuantity = dropItems[i][3];
            var maxQuantity = dropItems[i][4];
            var quantity = Math.floor(Math.random() * (maxQuantity - minQuantity + 1) + minQuantity);
            toDrop.push(new Array(dropItems[i][1], quantity));//载入暴率数组
        }
    }
    for (var i = 0; i < toDrop.length; i++) {
        if (player.getMap() != null)
            player.getMap().spawnMobDrop(em.newItem(toDrop[i][0], 0, toDrop[i][1]), new java.awt.Point(mob.getTruePosition().getX() + 25 * i, mob.getTruePosition().getY()), mob, player, 0, 0);
    }
}

function setup(level, leaderid) {
    var eim = em.newInstance("BeideEvent");
    for (var i = 0; i < MapList.length; i++) {
        var map = eim.setInstanceMap(MapList[i]);
        map.resetPQ(level);
        map.resetFully();
    }
    mobid = 9390612;//贝勒德的肚脐
    mob = em.getMonster(mobid);
    modified = em.newMonsterStats();
    modified.setOHp(肚脐血量);
    modified.setOMp(mob.getMobMaxMp() * 2);
    mob.setOverrideStats(modified);
    eim.registerMonster(mob);
    var mapForMob1 = eim.getMapInstance(863010600);
    mapForMob1.spawnMonsterOnGroundBelow(mob, new java.awt.Point(0, 0));

    mobid = 9390610;//贝勒德的右手肩膀
    mob = em.getMonster(mobid);
    modified = em.newMonsterStats();
    modified.setOHp(肩膀血量);
    modified.setOMp(mob.getMobMaxMp() * 2);
    mob.setOverrideStats(modified);
    eim.registerMonster(mob);
    var mapForMob1 = eim.getMapInstance(863010330);
    mapForMob1.spawnMonsterOnGroundBelow(mob, new java.awt.Point(0, 69));


    mobid = 9390611;//贝勒德的左手肩膀
    mob = em.getMonster(mobid);
    modified = em.newMonsterStats();
    modified.setOHp(肩膀血量);
    modified.setOMp(mob.getMobMaxMp() * 2);
    mob.setOverrideStats(modified);
    eim.registerMonster(mob);
    var mapForMob1 = eim.getMapInstance(863010430);
    mapForMob1.spawnMonsterOnGroundBelow(mob, new java.awt.Point(86, 70));

    mobid = 9390637;//贝勒德的宿主花
    mob = em.getMonster(mobid);
    modified = em.newMonsterStats();
    modified.setOHp(宿主花);
    modified.setOMp(mob.getMobMaxMp() * 2);
    mob.setOverrideStats(modified);
    eim.registerMonster(mob);
    var mapForMob1 = eim.getMapInstance(863010240);
    mapForMob1.spawnMonsterOnGroundBelow(mob, new java.awt.Point(0, 0));
    eim.startEventTimer(1000 * 60 * 45); //45 min
    em.setProperty("position", "empty"); ////判断这个组队由哪个方向进入。
    return eim;
}

function playerEntry(eim, player) {
    var map = eim.getMapInstance(0);
    player.dropMessage(6, "[巨大的贝勒德] 进入到了挑战地图，请小心行事。");
    player.changeMap(map, map.getPortal(0));
}

function playerDead(eim, player) {
}

function playerRevive(eim, player) {
}

function scheduledTimeout(eim) {
    em.setProperty("position", "empty");
    eim.disposeIfPlayerBelow(100, 910000000);
}

function changedMap(eim, player, mapid) {
    switch (mapid) {
        case 863010100:
            var map = eim.getMapInstance(863010100);
            map.startMapEffect("勇士们快来解救我吧！你可以从右边或者左边进入我的脚步爬上来……。", 5120117);
            break;
        case 863010220 :
            var map = eim.getMapInstance(863010220);
            map.startMapEffect("无法忍受的怪异声响，脚下总是藏着一些东西让我发痒！", 5120117);
            break;
        case 863010230 :
            var map = eim.getMapInstance(863010230);
            map.startMapEffect("无法忍受的怪异声响，脚下总是藏着一些东西让我发痒！", 5120117);
            break;
        case 863010200 :
            var map = eim.getMapInstance(863010200);
            map.startMapEffect("无法忍受的怪异声响，脚下总是藏着一些东西让我发痒！", 5120117);
            break;
        case 863010210 :
            var map = eim.getMapInstance(863010210);
            map.startMapEffect("无法忍受的怪异声响，脚下总是藏着一些东西让我发痒！", 5120117);
            break;
        case 863010240 :
            var map = eim.getMapInstance(863010240);
            map.startMapEffect("无法忍受的怪异声响，脚下总是藏着一些东西让我发痒！", 5120117);
            break;
        case 863010400 :
            var map = eim.getMapInstance(863010400);
            map.startMapEffect("消灭我吧！总是感觉黑暗力量在我的肚子里酝酿。。", 5120117);
            break;
        case 863010410 :
            var map = eim.getMapInstance(863010400);
            map.startMapEffect("消灭我吧！总是感觉黑暗力量在我的肚子里酝酿。。", 5120117);
            break;
        case 863010420 :
            var map = eim.getMapInstance(863010420);
            map.startMapEffect("感谢你勇士！谢谢你来救我！请加把劲！现在你就要看到我了！", 5120117);
            break;
        case 863010430 :
            var map = eim.getMapInstance(863010430);
            map.startMapEffect("感谢你勇士！谢谢你来救我！请加把劲！现在你就要看到我了！", 5120117);
            break;
        case 863010300 :
            var map = eim.getMapInstance(863010300);
            map.startMapEffect("感谢你勇士！谢谢你来救我！请加把劲！现在你就要看到我了！", 5120117);
            break;
        case 863010310 :
            var map = eim.getMapInstance(863010310);
            map.startMapEffect("感谢你勇士！谢谢你来救我！请加把劲！现在你就要看到我了！", 5120117);
            break;
        case 863010320 :
            var map = eim.getMapInstance(863010320);
            map.startMapEffect("感谢你勇士！谢谢你来救我！请加把劲！现在你就要看到我了！", 5120117);
            break;
        case 863010330 :
            var map = eim.getMapInstance(863010320);
            map.startMapEffect("感谢你勇士！谢谢你来救我！请加把劲！现在你就要看到我了！", 5120117);
            break;
        case 863010600 :
            var map = eim.getMapInstance(863010600);
            map.startMapEffect("快点来阻止我！！我的勇士。。", 5120117);
            break;
        case 863000920:
            break;
    }
    switch (mapid) {
        case 863010100:
        case 863010100:
        case 863010220 :
        case 863010230 :
        case 863010200 :
        case 863010210 :
        case 863010240 :
        case 863010400 :
        case 863010410 :
        case 863010420 :
        case 863010430 :
        case 863010300 :
        case 863010310 :
        case 863010320 :
        case 863010330 :
        case 863010600 :
        case 863000920:
            return;
    }
    player.dropMessage(6, "[巨大的贝勒德] 已退出挑战。");
    eim.unregisterPlayer(player);
    if (eim.getPlayerCount() < 1) {
        eim.disposeIfPlayerBelow(100, 910000000);
    }
}

function playerDisconnected(eim, player) {
    eim.unregisterPlayer(player);
    return 0;
}

function leftParty(eim, player) {
    playerExit(eim, player);
}

function disbandParty(eim) {
    eim.disposeIfPlayerBelow(100, 910000000);
}

function playerExit(eim, player) {
    eim.unregisterPlayer(player);
    var map = eim.getMapFactory().getMap(910000000);
    player.changeMap(map, map.getPortal(0));
}

function clearPQ(eim) {
    eim.disposeIfPlayerBelow(100, 910000000);
    em.broadcastServerMsg("[决战贝勒德] 解脱泰坦级贝勒德的伟大任务完成了，你们才是真正的勇士！奖励发放完毕");
}

function allMonstersDead(eim) {
}

function cancelSchedule() {
    em.setProperty("started", "false");
}