var mapId = 746000012;
var item = Array(1702379,1702388,1003520,1003122,1003714,1102555,1072820,1052626,1003163); //极品稀有点装
var index = 0;

function init() {
    em.setProperty("state", "0");
    em.setProperty("leader", "true");
}

function setup(eim, leaderid) {
    em.setProperty("state", "1");
    em.setProperty("leader", "true");
    var eim = em.newInstance("Vergamot" + leaderid);

    eim.setProperty("vergamotSummoned", "0");

    var map = eim.setInstanceMap(mapId);
    map.resetFully();

    var mob = em.getMonster(8920100); //诱惑之血腥女王
    var overrideStats = Packages.server.life.OverrideMonsterStats();
    var hprand = 21000000000;
    overrideStats.setOHp(hprand);
    overrideStats.setOExp(1500000);
    overrideStats.setOMp(200000);
    mob.setOverrideStats(overrideStats);
    mob.setHp(hprand);
    eim.registerMonster(mob);
    map.spawnMonsterOnGroundBelow(mob, new java.awt.Point(187, 27)); //刷出这个怪物
    eim.startEventTimer(1000 * 300);
    return eim;
}

function playerEntry(eim, player) {
    var map = eim.getMapInstance(0);
    player.changeMap(map, map.getPortal(0));
}

function playerRevive(eim, player) {
    return false;
}

function scheduledTimeout(eim) {
    end(eim);
}

function changedMap(eim, player, mapid) {
    if (mapid != 746000012) {
        eim.unregisterPlayer(player);

        if (eim.disposeIfPlayerBelow(0, 0)) {
            em.setProperty("state", "0");
            em.setProperty("leader", "true");
        }
    }
}

function playerDisconnected(eim, player) {
    return 0;
}

function monsterValue(eim, mobId) {
    return 1;
}

function playerExit(eim, player) {
    eim.unregisterPlayer(player);

    if (eim.disposeIfPlayerBelow(0, 0)) {
        em.setProperty("state", "0");
        em.setProperty("leader", "true");
    }
}

function end(eim) {
    var map = eim.setInstanceMap(mapId);
    if (map.getNumMonsters() != 0) {
        xw(eim);
        map.broadcastMessage(Packages.tools.MaplePacketCreator.serverNotice(1, "任务失败。扣除100修炼点。。"));
    }
    eim.disposeIfPlayerBelow(100, 910000000);
    em.setProperty("state", "0");
    em.setProperty("leader", "true");
}

function clearPQ(eim) {
    end(eim);
}

function allMonstersDead(eim) {
    var map = eim.setInstanceMap(mapId);
    var mob;
    var overrideStats = Packages.server.life.OverrideMonsterStats();
    var hprand;
    var time;
       if(em.getProperty("state").equals("1")==true){
        em.setProperty("state", "2");
        mob = em.getMonster(8920102); //愤怒之血腥女王
        hprand = 42000000000;
        time = 1000 * 240;
	overrideStats.setOHp(hprand);
        mob.setOverrideStats(overrideStats);
        mob.setHp(hprand);
        eim.registerMonster(mob);
        map.spawnMonsterOnGroundBelow(mob, new java.awt.Point(187, 27)); //刷出这个怪物
        em.getMapFactory().getMap(746000012).startSimpleMapEffect("你不爱我，难道是我变丑了？不可能。。这绝对不可能。。我要烧掉这一切", 5120001);
    }else if(em.getProperty("state").equals("2")==true){
        em.setProperty("state", "3");
        mob = em.getMonster(8920101); //白雪人与企鹅王
        hprand = 84000000000;
        time = 1000 * 180;
	overrideStats.setOHp(hprand);
        mob.setOverrideStats(overrideStats);
        mob.setHp(hprand);
        eim.registerMonster(mob);
        map.spawnMonsterOnGroundBelow(mob, new java.awt.Point(187, 27)); //刷出这个怪物
        em.getMapFactory().getMap(746000012).startSimpleMapEffect("不疯魔，不成活，和我合为一体吧。", 5120001);
    }else if(em.getProperty("state").equals("3")==true){
        em.setProperty("state", "4");
        mob = em.getMonster(8930000); //贝伦
        hprand = 168000000000;
        time = 1000 * 180;
	overrideStats.setOHp(hprand);
        mob.setOverrideStats(overrideStats);
        mob.setHp(hprand);
        eim.registerMonster(mob);
        map.spawnMonsterOnGroundBelow(mob, new java.awt.Point(187, 27)); //刷出这个怪物
        em.getMapFactory().getMap(746000012).startSimpleMapEffect("既然这样就让你看看我的真面目！", 5120001);
    }else if(em.getProperty("state").equals("4")==true){
        var iter = em.getInstances().iterator();
        var eim = iter.next();
        var pIter = eim.getPlayers().iterator();
        var chr = pIter.next();
        var map = eim.getMapFactory().getMap(mapId);
        var randitem = Math.floor(Math.random() * item.length);
	map.spawnAutoDrop(item[randitem],chr.getPosition());
        em.getMapFactory().getMap(746000012).startSimpleMapEffect("这不可能，我怎么可能死~", 5120001);
        time = 1000 * 60 * 1;
    }

    eim.startEventTimer(time);
}

function xw(eim) {
    var iter = em.getInstances().iterator();
    while (iter.hasNext()) {
        var eim = iter.next();
        var pIter = eim.getPlayers().iterator();
        while (pIter.hasNext()) {
            var chr = pIter.next();
            if (chr.getXw() >= 100) {
                chr.setXw(chr.getXw() - 100);
            }

        }
    }
}

function leftParty(eim, player) {}

function disbandParty(eim) {}

function playerDead(eim, player) {}

function cancelSchedule() {}

function monsterDrop(eim, player, mob) {}