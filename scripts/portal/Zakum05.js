/*
    Zakum Entrance
*/

function enter(pi) {
    //if (pi.getQuestStatus(100200) == 2) {
       // pi.playerMessage(5, "没有完成任务不能进去。");
       // return false;

    //} else 
    var channel = pi.getPlayer().getClient().getChannel();
    var mapid;
    switch (channel) {
        case 1:
        case 2:
        case 3:
            mapid = 211042400;
            break;
        case 4:
        case 5:
            mapid = 211042401;
            break;
    }
    if (!pi.haveItem(4001017)) {
        pi.playerMessage(5, "没有火焰的眼不能进去。");
        return false;
    }
    pi.playPortalSE();
    pi.warp(mapid, "west00");
    return true;
}