function enter(pi) {
    if (pi.getMapId() == 350060707) {
        pi.playerMessage(-1, "恭喜你 - 你已通关！请领奖品 然后退出斯乌副本");
        pi.playerMessage(5, "恭喜你 - 你已通关！请领奖品 然后退出斯乌副本");

    } else {
        if (pi.getMap().getAllMonstersThreadsafe().size() == 0) {
            pi.warpParty(pi.getMapId() + 1, 0);
        } else {
            pi.playerMessage(-1, "※ 斯乌 ※ ：不杀死我就想进入下一关？太异想天开了吧");
            pi.playerMessage(5, "※ 斯乌 ※ ：不杀死我就想进入下一关？太异想天开了吧");
        }
    }
}