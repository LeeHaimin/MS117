@echo off
@title Server
color b
cls
::set path=jre7\bin;%SystemRoot%\system32;%SystemRoot%;%SystemRoot%
::set JRE_HOME=jre7
::set JAVA_HOME=jre7\bin
set CLASSPATH=.;out\*
::java -server -Xms512m -Xmx512m -Xss256k -XX:ReservedCodeCacheSize=256m -Dwzpath=wz\ -Djavax.net.ssl.keyStore=filename.keystore -Djavax.net.ssl.keyStorePassword=passwd-Djavax.net.ssl.trustStore=filename.keystore -Djavax.net.ssl.trustStorePassword=passwd server.Start
java -server -Xms512m -Xmx512m -Xss256k -XX:ReservedCodeCacheSize=256m -Dwzpath=wz\ -Djavax.net.ssl.keyStore=filename.keystore -Djavax.net.ssl.keyStorePassword=passwd-Djavax.net.ssl.trustStore=filename.keystore -Djavax.net.ssl.trustStorePassword=passwd -Dfile.encoding=utf-8 server.Start
pause