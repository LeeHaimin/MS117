package dumpTools;

import java.io.FileOutputStream;
import java.io.IOException;

import provider.MapleDataDirectoryEntry;
import provider.MapleDataFileEntry;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;

/*
 * Author: Xerdox
 */
public class HairAndEyeCreator
{

    public static void main(String[] args) throws IOException
    {
        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");//公司
        MapleDataProvider hairSource = MapleDataProviderFactory.getDataProvider(MapleDataProviderFactory.fileInWZPath("Character.wz/Hair"));
        MapleDataProvider faceSource = MapleDataProviderFactory.getDataProvider(MapleDataProviderFactory.fileInWZPath("Character.wz/Face"));
        MapleDataDirectoryEntry root = hairSource.getRoot();
        StringBuilder sb = new StringBuilder();
        FileOutputStream out = new FileOutputStream("hairAndFacesID.txt", true);
        System.out.println("加载 : 男角色发型数据.");
        sb.append("男角色发型:\r\n");
        for (MapleDataFileEntry topDir : root.getFiles())
        {
            if (topDir.getName().startsWith("000"))
            {
                int id = Integer.parseInt(topDir.getName().substring(0, 8));
                if ((id / 1000 == 30 || id / 1000 == 33) && id % 10 == 0)
                {
                    sb.append(id).append(", ");
                }
            }
        }
        System.out.println("加载 : 女角色发型数据.");
        sb.append("\r\n\r\n");
        sb.append("女角色发型:\r\n");
        for (MapleDataFileEntry topDir : root.getFiles())
        {
            if (topDir.getName().startsWith("000"))
            {
                int id = Integer.parseInt(topDir.getName().substring(0, 8));
                if ((id / 1000 == 31 || id / 1000 == 34) && id % 10 == 0)
                {
                    sb.append(id).append(", ");
                }
            }
        }
        System.out.println("加载 : 男角色脸型数据.");
        sb.append("\r\n\r\n");
        sb.append("男角色脸型:\r\n");
        final MapleDataDirectoryEntry root2 = faceSource.getRoot();
        for (MapleDataFileEntry topDir2 : root2.getFiles())
        {
            if (topDir2.getName().startsWith("000"))
            {
                int id = Integer.parseInt(topDir2.getName().substring(0, 8));
                if ((id / 1000 == 20) && id % 1000 < 100)
                {
                    sb.append(id).append(", ");
                }
            }
        }
        System.out.println("加载 : 女角色脸型数据.");
        sb.append("\r\n\r\n");
        sb.append("女角色脸型:\r\n");
        for (MapleDataFileEntry topDir2 : root2.getFiles())
        {
            if (topDir2.getName().startsWith("000"))
            {
                int id = Integer.parseInt(topDir2.getName().substring(0, 8));
                if ((id / 1000 == 21) && id % 1000 < 100)
                {
                    sb.append(id).append(", ");
                }
            }
        }
        out.write(sb.toString().getBytes());
    }
}
