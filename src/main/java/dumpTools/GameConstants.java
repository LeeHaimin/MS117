/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dumpTools;

/**
 * @author PlayDK
 */
public class GameConstants
{

    public static final String[] stats = {"tuc", "reqLevel", "reqJob", "reqSTR", "reqDEX", "reqINT", "reqLUK", "reqPOP", "cash", "cursed", "success", "setItemID", "equipTradeBlock", "durability",
            "randOption", "randStat", "masterLevel", "reqSkillLevel", "elemDefault", "incRMAS", "incRMAF", "incRMAI", "incRMAL", "canLevel", "skill", "charmEXP", "limitBreak", "imdR", "bdR"};

    public static MapleInventoryType getInventoryType(int itemId)
    {
        byte type = (byte) (itemId / 1000000);
        if (type < 1 || type > 5)
        {
            return MapleInventoryType.UNDEFINED;
        }
        return MapleInventoryType.getByType(type);
    }
}
