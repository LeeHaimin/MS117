package dumpTools;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import database.DatabaseConnectionWZ;
import provider.MapleData;
import provider.MapleDataDirectoryEntry;
import provider.MapleDataFileEntry;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;
import tools.Pair;

public class DumpItems
{

    protected final Set<Integer> doneIds = new LinkedHashSet<>();
    private final MapleDataProvider item;
    private final MapleDataProvider string = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/String.wz"));
    protected final MapleData cashStringData = string.getData("Cash.img");
    protected final MapleData consumeStringData = string.getData("Consume.img");
    protected final MapleData eqpStringData = string.getData("Eqp.img");
    protected final MapleData etcStringData = string.getData("Etc.img");
    protected final MapleData insStringData = string.getData("Ins.img");
    protected final MapleData petStringData = string.getData("Pet.img");
    private final MapleDataProvider character;
    private final Connection con = DatabaseConnectionWZ.getConnection();
    private final List<String> subCon = new LinkedList<>();
    private final List<String> subMain = new LinkedList<>();
    protected boolean hadError = false;
    protected boolean update;
    protected int id = 0;

    public DumpItems(boolean update)
    {
        this.update = update;
        this.item = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/Item.wz"));
        this.character = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/Character.wz"));
        if (string == null)
        {
            hadError = true;
        }
    }

    public static void main(String[] args)
    {
//        System.setProperty("wzpath", "D:\\MapleStory\\MS117\\WZ");//家里
        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");//办公室
        boolean hadError = false;
        boolean update = false;
        long startTime = System.currentTimeMillis();
        for (String file : args)
        {
            if (file.equalsIgnoreCase("-update"))
            {
                update = true;
                break;
            }
        }
        int currentQuest = 0;
        try
        {
            final DumpItems dq = new DumpItems(true);
            System.out.println("Dumping Items");
            dq.dumpItems();
            hadError |= dq.isHadError();
            currentQuest = dq.currentId();
        }
        catch (Exception e)
        {
            hadError = true;
            System.out.println(currentQuest + " 道具.");
            System.out.println("出现错误 - 2 " + e);
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        double elapsedSeconds = (endTime - startTime) / 1000.0;
        int elapsedSecs = (((int) elapsedSeconds) % 60);
        int elapsedMinutes = (int) (elapsedSeconds / 60.0);
        String withErrors = "";
        if (hadError)
        {
            withErrors = " with errors";
        }
        System.out.println("Finished" + withErrors + " in " + elapsedMinutes + " 分 " + elapsedSecs + " 秒");
    }

    public void dumpItems() throws Exception
    {
        if (!hadError)
        {
            System.out.println(id + " 道具.");
            PreparedStatement ps_adddata = con.prepareStatement("INSERT INTO wz_itemadddata(itemid, `key`, `subKey`, `value`) VALUES (?, ?, ?, ?)");
            PreparedStatement ps_rewarddata = con.prepareStatement("INSERT INTO wz_itemrewarddata(itemid, item, prob, quantity, period, worldMsg, effect) VALUES (?, ?, ?, ?, ?, ?, ?)");
            PreparedStatement ps_itemdata = con.prepareStatement("INSERT INTO wz_itemdata(itemid, name, msg, `desc`, slotMax, price, wholePrice, stateChange, flags, karma, meso, monsterBook, " +
                    "itemMakeLevel, questId, scrollReqs, consumeItem, totalprob, incSkill, replaceId, replaceMsg, `create`, afterImage) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                    + " ?, ?, ?, ?)");
            PreparedStatement ps_equipdata = con.prepareStatement("INSERT INTO wz_itemequipdata(itemid, itemLevel, `key`, `value`) VALUES (?, ?, ?, ?)");
            try
            {
                dumpItems(ps_adddata, ps_rewarddata, ps_itemdata, ps_equipdata);
            }
            catch (Exception e)
            {
                System.out.println(id + " 道具.");
                System.out.println("出现错误 - 1 " + e);
                hadError = true;
            }
            finally
            {
                ps_rewarddata.executeBatch();
                ps_rewarddata.close();
                ps_adddata.executeBatch();
                ps_adddata.close();
                ps_equipdata.executeBatch();
                ps_equipdata.close();
                ps_itemdata.executeBatch();
                ps_itemdata.close();
            }
        }
    }

    public boolean isHadError()
    {
        return hadError;
    }

    public int currentId()
    {
        return id;
    }

    public void dumpItems(PreparedStatement ps_adddata, PreparedStatement ps_rewarddata, PreparedStatement ps_itemdata, PreparedStatement ps_equipdata) throws Exception
    {
        if (!update)
        {
            delete("DELETE FROM wz_itemdata");
            delete("DELETE FROM wz_itemequipdata");
            delete("DELETE FROM wz_itemadddata");
            delete("DELETE FROM wz_itemrewarddata");
            System.out.println("删除 wz_itemdata 道具信息完成.");
        }
        System.out.println("开始读取道具信息.....");
        dumpItems(item, ps_adddata, ps_rewarddata, ps_itemdata, ps_equipdata, false);
        dumpItems(character, ps_adddata, ps_rewarddata, ps_itemdata, ps_equipdata, true);
        System.out.println("开始添加道具信息...");
        System.out.println("请不要关闭正在导入SQL信息...");
        //System.out.println(subMain.toString());
        //System.out.println(subCon.toString());
    }

    public void delete(String sql) throws Exception
    {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.executeUpdate();
        ps.close();
    }

    public void dumpItems(MapleDataProvider d, PreparedStatement ps_adddata, PreparedStatement ps_rewarddata, PreparedStatement ps_itemdata, PreparedStatement ps_equipdata, boolean charz) throws Exception
    {
        for (MapleDataDirectoryEntry topDir : d.getRoot().getSubdirectories())
        { //requirements first
            if (!topDir.getName().equalsIgnoreCase("Special") && !topDir.getName().equalsIgnoreCase("Hair") && !topDir.getName().equalsIgnoreCase("Face") && !topDir.getName().equalsIgnoreCase(
                    "Afterimage"))
            {
                for (MapleDataFileEntry ifile : topDir.getFiles())
                {
                    MapleData iz = d.getData(topDir.getName() + "/" + ifile.getName());
                    if (charz || topDir.getName().equalsIgnoreCase("Pet"))
                    {
                        dumpItem(ps_adddata, ps_rewarddata, ps_itemdata, ps_equipdata, iz);
                    }
                    else
                    {
                        for (MapleData itemData : iz)
                        {
                            dumpItem(ps_adddata, ps_rewarddata, ps_itemdata, ps_equipdata, itemData);
                        }
                    }
                }
            }
        }
    }

    public void dumpItem(PreparedStatement ps_adddata, PreparedStatement ps_rewarddata, PreparedStatement ps_itemdata, PreparedStatement ps_equipdata, MapleData iz) throws Exception
    {
        try
        {
            if (iz.getName().endsWith(".img"))
            {
                this.id = Integer.parseInt(iz.getName().substring(0, iz.getName().length() - 4));
            }
            else
            {
                this.id = Integer.parseInt(iz.getName());
            }
        }
        catch (NumberFormatException nfe)
        { //not we need
            return;
        }
        if (doneIds.contains(id) || GameConstants.getInventoryType(id) == MapleInventoryType.UNDEFINED)
        {
            return;
        }
        doneIds.add(id);
        if (update && doesExist("SELECT * FROM wz_itemdata WHERE itemid = " + id))
        {
//            System.out.println("插入:" + id + "数据库中已存在");
            return;
        }
//        System.out.println("插入:" + id + "数据库不存在，开始添加");
        /*
         * itemdata
         */
        ps_itemdata.setInt(1, id);
        MapleData stringData = getStringData(id);
        if (stringData == null)
        {
            ps_itemdata.setString(2, "");
            ps_itemdata.setString(3, "");
            ps_itemdata.setString(4, "");
            //System.out.println("未知名称道具 : " + id);
        }
        else
        {
            ps_itemdata.setString(2, MapleDataTool.getString("name", stringData, ""));
            ps_itemdata.setString(3, MapleDataTool.getString("msg", stringData, ""));
            ps_itemdata.setString(4, MapleDataTool.getString("desc", stringData, ""));
        }
        short ret;
        MapleData smEntry = iz.getChildByPath("info/slotMax");
        if (smEntry == null)
        {
            if (GameConstants.getInventoryType(id) == MapleInventoryType.EQUIP)
            {
                ret = 1;
            }
            else
            {
                ret = 100;
            }
        }
        else
        {
            ret = (short) MapleDataTool.getIntConvert(smEntry, -1);
        }
        ps_itemdata.setInt(5, ret);
        double pEntry;
        MapleData pData = iz.getChildByPath("info/unitPrice");
        if (pData != null)
        {
            try
            {
                pEntry = MapleDataTool.getDouble(pData);
            }
            catch (Exception e)
            {
                pEntry = MapleDataTool.getIntConvert(pData, -1);
            }
        }
        else
        {
            pData = iz.getChildByPath("info/price");
            if (pData == null)
            {
                pEntry = -1.0;
            }
            else
            {
                pEntry = MapleDataTool.getIntConvert(pData, -1);
            }
        }
        if (id == 2070019 || id == 2330007)
        {
            pEntry = 1.0;
        }
        ps_itemdata.setString(6, String.valueOf(pEntry));
        ps_itemdata.setInt(7, MapleDataTool.getIntConvert("info/price", iz, -1));
        ps_itemdata.setInt(8, MapleDataTool.getIntConvert("info/stateChangeItem", iz, 0));
        int flags = MapleDataTool.getIntConvert("info/bagType", iz, 0);
        if (MapleDataTool.getIntConvert("info/notSale", iz, 0) > 0)
        {
            flags |= 0x10;
        }
        if (MapleDataTool.getIntConvert("info/expireOnLogout", iz, 0) > 0)
        {
            flags |= 0x20;
        }
        if (MapleDataTool.getIntConvert("info/pickUpBlock", iz, 0) > 0)
        {
            flags |= 0x40;
        }
        if (MapleDataTool.getIntConvert("info/only", iz, 0) > 0)
        {
            flags |= 0x80;
        }
        if (MapleDataTool.getIntConvert("info/accountSharable", iz, 0) > 0)
        {
            flags |= 0x100;
        }
        if (MapleDataTool.getIntConvert("info/quest", iz, 0) > 0)
        {
            flags |= 0x200;
        }
        if (MapleDataTool.getIntConvert("info/tradeBlock", iz, 0) > 0)
        {
            flags |= 0x400;
        }
        if (MapleDataTool.getIntConvert("info/accountShareTag", iz, 0) > 0)
        {
            flags |= 0x800;
        }
        if (MapleDataTool.getIntConvert("info/mobHP", iz, 0) > 0 && MapleDataTool.getIntConvert("info/mobHP", iz, 0) < 100)
        {
            flags |= 0x1000;
        }
        if (MapleDataTool.getIntConvert("info/nActivatedSocket", iz, 0) > 0)
        {
            flags |= 0x2000;
        }
        if (MapleDataTool.getIntConvert("info/superiorEqp", iz, 0) > 0)
        {
            flags |= 0x4000;
        }
        ps_itemdata.setInt(9, flags);
        ps_itemdata.setInt(10, MapleDataTool.getIntConvert("info/tradeAvailable", iz, 0));
        ps_itemdata.setInt(11, MapleDataTool.getIntConvert("info/meso", iz, 0));
        ps_itemdata.setInt(12, MapleDataTool.getIntConvert("info/mob", iz, 0));
        ps_itemdata.setInt(13, MapleDataTool.getIntConvert("info/lv", iz, 0));
        ps_itemdata.setInt(14, MapleDataTool.getIntConvert("info/questId", iz, 0));
        int totalprob = 0;
        StringBuilder scrollReqs = new StringBuilder(), consumeItem = new StringBuilder(), incSkill = new StringBuilder();
        MapleData dat = iz.getChildByPath("req");
        if (dat != null)
        {
            for (MapleData req : dat)
            {
                if (scrollReqs.length() > 0)
                {
                    scrollReqs.append(",");
                }
                scrollReqs.append(MapleDataTool.getIntConvert(req, 0));
            }
        }
        dat = iz.getChildByPath("consumeItem");
        if (dat != null)
        {
            for (MapleData req : dat)
            {
                if (consumeItem.length() > 0)
                {
                    consumeItem.append(",");
                }
                consumeItem.append(MapleDataTool.getIntConvert(req, 0));
            }
        }
        ps_itemdata.setString(15, scrollReqs.toString());
        ps_itemdata.setString(16, consumeItem.toString());
        Map<Integer, Map<String, Integer>> equipStats = new HashMap<>();
        equipStats.put(-1, new HashMap<>());
        dat = iz.getChildByPath("mob");
        if (dat != null)
        {
            for (MapleData child : dat)
            {
                equipStats.get(-1).put("mob" + MapleDataTool.getIntConvert("id", child, 0), MapleDataTool.getIntConvert("prob", child, 0));
            }
        }
        dat = iz.getChildByPath("info/level/case");
        if (dat != null)
        {
            for (MapleData info : dat)
            {
                for (MapleData data : info)
                {
                    if (data.getName().length() == 1 && data.getChildByPath("Skill") != null)
                    {
                        for (MapleData skil : data.getChildByPath("Skill"))
                        {
                            int incSkillz = MapleDataTool.getIntConvert("id", skil, 0);
                            if (incSkillz != 0)
                            {
                                if (incSkill.length() > 0)
                                {
                                    incSkill.append(",");
                                }
                                incSkill.append(incSkillz);
                            }
                        }
                    }
                }
            }
        }
        dat = iz.getChildByPath("info/level/info");
        if (dat != null)
        {
            for (MapleData info : dat)
            {
                if (MapleDataTool.getIntConvert("exp", info, 0) == 0)
                {
                    continue;
                }
                int lv = Integer.parseInt(info.getName());
                if (equipStats.get(lv) == null)
                {
                    equipStats.put(lv, new HashMap<>());
                }
                for (MapleData data : info)
                {
                    if (data.getName().length() > 3)
                    {
                        equipStats.get(lv).put(data.getName().substring(3), MapleDataTool.getIntConvert(data, 0));
                    }
                }
            }
        }
        dat = iz.getChildByPath("info");
        if (dat != null)
        {
            ps_itemdata.setString(22, MapleDataTool.getString("afterImage", dat, ""));
            Map<String, Integer> rett = equipStats.get(-1);
            for (MapleData data : dat.getChildren())
            {
                if (data.getName().startsWith("inc"))
                {
                    int gg = MapleDataTool.getIntConvert(data, 0);
                    if (gg != 0)
                    {
                        rett.put(data.getName().substring(3), gg);
                    }
                }
            }
            //save sql, only do the ones that exist
            for (String stat : GameConstants.stats)
            {
                MapleData d = dat.getChildByPath(stat);
                if (stat.equals("canLevel"))
                {
                    if (dat.getChildByPath("level") != null)
                    {
                        rett.put(stat, 1);
                    }
                }
                else if (d != null)
                {
                    if (stat.equals("skill"))
                    {
                        for (int i = 0; i < d.getChildren().size(); i++)
                        { // List of allowed skillIds
                            rett.put("skillid" + i, MapleDataTool.getIntConvert(Integer.toString(i), d, 0));
                        }
                    }
                    else
                    {
                        int dd = MapleDataTool.getIntConvert(d, 0);
                        if (dd != 0)
                        {
                            rett.put(stat, dd);
                        }
                    }
                }
            }
        }
        else
        {
            ps_itemdata.setString(22, "");
        }
        /*
         * equipdata
         */
        ps_equipdata.setInt(1, id);
        for (Entry<Integer, Map<String, Integer>> stats : equipStats.entrySet())
        {
            ps_equipdata.setInt(2, stats.getKey());
            for (Entry<String, Integer> stat : stats.getValue().entrySet())
            {
                ps_equipdata.setString(3, stat.getKey());
                ps_equipdata.setInt(4, stat.getValue());
                ps_equipdata.addBatch();
            }
        }
        /*
         * adddata
         */
        dat = iz.getChildByPath("info/addition");
        if (dat != null)
        {
            ps_adddata.setInt(1, id);
            for (MapleData d : dat.getChildren())
            {
                Pair<String, Integer> incs = null;
                if (d.getName().equalsIgnoreCase("statinc") || d.getName().equalsIgnoreCase("critical") || d.getName().equalsIgnoreCase("skill") || d.getName().equalsIgnoreCase("mobdie") || d.getName().equalsIgnoreCase("hpmpchange") || d.getName().equalsIgnoreCase("elemboost") || d.getName().equalsIgnoreCase("elemBoost") || d.getName().equalsIgnoreCase("mobcategory") || d.getName().equalsIgnoreCase("boss"))
                {
                    for (MapleData subKey : d.getChildren())
                    {
                        if (subKey.getName().equals("con"))
                        {
                            for (MapleData conK : subKey.getChildren())
                            {
                                if (conK.getName().equals("job"))
                                {
                                    StringBuilder sbbb = new StringBuilder();
                                    if (conK.getData() == null)
                                    { // a loop
                                        for (MapleData ids : conK.getChildren())
                                        {
                                            sbbb.append(ids.getData().toString());
                                            sbbb.append(",");
                                        }
                                        sbbb.deleteCharAt(sbbb.length() - 1);
                                    }
                                    else
                                    {
                                        sbbb.append(conK.getData().toString());
                                    }
                                    ps_adddata.setString(2, d.getName().equals("elemBoost") ? "elemboost" : d.getName());
                                    ps_adddata.setString(3, "con:job");
                                    ps_adddata.setString(4, sbbb.toString());
                                    ps_adddata.addBatch();
                                }
                                else if (!conK.getName().equals("weekDay"))
                                { // 01142367
                                    ps_adddata.setString(2, d.getName().equals("elemBoost") ? "elemboost" : d.getName());
                                    ps_adddata.setString(3, "con:" + conK.getName());
                                    ps_adddata.setString(4, conK.getData().toString());
                                    ps_adddata.addBatch();
                                }
                            }
                        }
                        else
                        {
                            ps_adddata.setString(2, d.getName().equals("elemBoost") ? "elemboost" : d.getName());
                            ps_adddata.setString(3, subKey.getName());
                            ps_adddata.setString(4, subKey.getData().toString());
                            ps_adddata.addBatch();
                        }
                    }
                }
                else
                {
                    System.out.println("UNKNOWN EQ ADDITION : " + d.getName() + " from " + id);
                }
            }
        }
        /*
         * rewarddata
         */
        dat = iz.getChildByPath("reward");
        if (dat != null)
        {
            ps_rewarddata.setInt(1, id);
            for (MapleData reward : dat)
            {
                ps_rewarddata.setInt(2, MapleDataTool.getIntConvert("item", reward, 0));
                ps_rewarddata.setInt(3, MapleDataTool.getIntConvert("prob", reward, 0));
                ps_rewarddata.setInt(4, MapleDataTool.getIntConvert("count", reward, 0));
                ps_rewarddata.setInt(5, MapleDataTool.getIntConvert("period", reward, 0));
                ps_rewarddata.setString(6, MapleDataTool.getString("worldMsg", reward, ""));
                ps_rewarddata.setString(7, MapleDataTool.getString("Effect", reward, ""));
                ps_rewarddata.addBatch();
                totalprob += MapleDataTool.getIntConvert("prob", reward, 0);
            }
        }
        /*
         * itemdata
         */
        ps_itemdata.setInt(17, totalprob);
        ps_itemdata.setString(18, incSkill.toString());
        dat = iz.getChildByPath("replace");
        if (dat != null)
        {
            ps_itemdata.setInt(19, MapleDataTool.getInt("itemid", dat, 0));
            ps_itemdata.setString(20, MapleDataTool.getString("msg", dat, ""));
        }
        else
        {
            ps_itemdata.setInt(19, 0);
            ps_itemdata.setString(20, "");
        }
        ps_itemdata.setInt(21, MapleDataTool.getInt("info/create", iz, 0));
        ps_itemdata.addBatch();
    }

    public boolean doesExist(String sql) throws Exception
    {
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        boolean ret = rs.next();
        rs.close();
        ps.close();
        return ret;
    }

    protected MapleData getStringData(int itemId)
    {
        String cat = null;
        MapleData theData;
        if (itemId >= 5010000)
        {
            theData = cashStringData;
        }
        else if (itemId >= 2000000 && itemId < 3000000)
        {
            theData = consumeStringData;
        }
        else if (itemId == 1112757 //克劳的指环 - 可以在其他指环上装备的指环. 克劳之灵魂将提升攻击力.
                || itemId == 1003055 //小鱼发夹
                || (itemId / 10000 == 101) //OK 面饰
                || (itemId / 10000 == 102) //OK 眼饰
                || (itemId / 10000 == 103) //OK 耳环
                || (itemId >= 1112138 && itemId <= 1112140) //OK
                || (itemId >= 1112245 && itemId <= 1112247) //OK
                || (itemId >= 1112400 && itemId <= 1112403) //OK
                || (itemId >= 1112401 && itemId <= 1112403) //OK
                || (itemId >= 1112686 && itemId <= 1112688) //OK
                || (itemId >= 1112785 && itemId <= 1112787) //OK
                || (itemId / 10000 == 112 && itemId != 1122000 && itemId != 1122007) //OK 项链
                || (itemId / 10000 == 113) //OK 腰带
                || (itemId / 10000 == 114) //OK 勋章
                || (itemId / 10000 == 115) //OK 护肩
                || (itemId / 10000 == 116) //OK 口袋
                || (itemId / 10000 == 118) //OK 徽章
                || (itemId / 10000 == 119) //OK 纹章
                || (itemId / 10000 == 120) //OK 图腾
        )
        {
            theData = eqpStringData;
            cat = "Accessory";
        }
        else if (itemId >= 1662000 && itemId < 1680000)
        { //OK 安卓
            theData = eqpStringData;
            cat = "Android";
        }
        else if (itemId >= 1172000 && itemId < 1173000)
        { //OK 怪物书
            theData = eqpStringData;
            cat = "MonsterBook";
        }
        else if (itemId >= 1000000 && itemId < 1010000 || itemId == 1072448)
        { //OK 帽子
            theData = eqpStringData;
            cat = "Cap";
        }
        else if (itemId >= 1102000 && itemId < 1103000 || itemId == 1100000 || itemId == 1100001)
        { //OK 披风
            theData = eqpStringData;
            cat = "Cape";
        }
        else if (itemId >= 1040000 && itemId < 1050000)
        { //OK 上衣
            theData = eqpStringData;
            cat = "Coat";
        }
        else if (itemId >= 20000 && itemId < 22000)
        { //OK 脸型
            theData = eqpStringData;
            cat = "Face";
        }
        else if (itemId >= 1080000 && itemId < 1090000)
        { //OK 手套
            theData = eqpStringData;
            cat = "Glove";
        }
        else if (itemId >= 30000 && itemId < 38000)
        { //OK 发型
            theData = eqpStringData;
            cat = "Hair";
        }
        else if (itemId >= 1050000 && itemId < 1060000)
        { //OK 套服
            theData = eqpStringData;
            cat = "Longcoat";
        }
        else if (itemId >= 1060000 && itemId < 1070000)
        { //OK 裤子
            theData = eqpStringData;
            cat = "Pants";
        }
        else if (itemId >= 1610000 && itemId < 1660000)
        { //OK 机械装备
            theData = eqpStringData;
            cat = "Mechanic";
        }
        else if (itemId >= 1802000 && itemId < 1820000)
        { //OK 宠物装备
            theData = eqpStringData;
            cat = "PetEquip";
        }
        else if ((itemId / 1000 == 1942) || (itemId / 1000 == 1952) || (itemId / 1000 == 1962) || (itemId / 1000 == 1972))
        { //OK 龙龙装备
            theData = eqpStringData;
            cat = "Dragon";
        }
        else if (itemId >= 1112000 && itemId < 1120000 || itemId == 1122000 || itemId == 1122007)
        { //OK 戒指
            theData = eqpStringData;
            cat = "Ring";
        }
        else if (itemId >= 1092000 && itemId < 1100000 || itemId == 1090000 || itemId == 1091000)
        { //盾牌  OK
            theData = eqpStringData;
            cat = "Shield";
        }
        else if (itemId >= 1070000 && itemId < 1080000)
        { //鞋子  OK
            theData = eqpStringData;
            cat = "Shoes";
        }
        else if (itemId >= 1680000 && itemId < 1690000)
        { //拼图  OK
            theData = eqpStringData;
            cat = "Bits";
        }
        else if (itemId >= 1842000 && itemId < 1900000)
        { //MonsterBattle  OK
            theData = eqpStringData;
            cat = "MonsterBattle";
        }
        else if (itemId >= 1900000 && itemId < 2000000)
        { //骑宠 OK
            theData = eqpStringData;
            cat = "Taming";
        }
        else if (itemId >= 1300000 && itemId < 1800000 || itemId / 1000 == 1212 || itemId / 1000 == 1222 || itemId / 1000 == 1232 || itemId / 1000 == 1242 || itemId / 1000 == 1252)
        {
            theData = eqpStringData;
            cat = "Weapon";
        }
        else if (itemId >= 4000000 && itemId < 5000000)
        {
            theData = etcStringData;
            cat = "Etc";
        }
        else if (itemId >= 3000000 && itemId < 4000000)
        {
            theData = insStringData;
        }
        else if (itemId >= 5000000 && itemId < 5010000)
        {
            theData = petStringData;
        }
        else
        {
            return null;
        }
        if (cat == null)
        {
            if (theData != etcStringData)
            {
                return theData.getChildByPath(String.valueOf(itemId));
            }
            else
            {
                return theData.getChildByPath("Etc/" + itemId);
            }
        }
        else
        {
            if (theData == eqpStringData)
            {
                return theData.getChildByPath("Eqp/" + cat + "/" + itemId);
            }
            else
            {
                return theData.getChildByPath(cat + "/" + itemId);
            }
        }
    }
}