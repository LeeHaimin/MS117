/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dumpTools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import tools.FileoutputUtil;

/**
 * @author PlayDK
 */
public class WZMapNameDumper
{

    public static void main(String[] args) throws IOException
    {
        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");//公司
        File stringFile = MapleDataProviderFactory.fileInWZPath("string.wz");
        MapleDataProvider stringProvider = MapleDataProviderFactory.getDataProvider(stringFile);
        MapleData map = stringProvider.getData("Map.img");

        String output = args[0];
        File outputDir = new File(output);
        File mapTxt = new File(output + "\\Map.txt");
        outputDir.mkdir();
        mapTxt.createNewFile();
        System.out.println("提取 Map.img 数据...");
        PrintWriter writer = new PrintWriter(new FileOutputStream(mapTxt));
        for (MapleData child : map.getChildren())
        {
            writer.println(child.getName());
            writer.println();
            List<Integer> mapIds = new ArrayList<Integer>();
            for (MapleData child2 : child.getChildren())
            {
                int mapId = Integer.parseInt(child2.getName());
                MapleData streetData = child2.getChildByPath("streetName");
                MapleData mapData = child2.getChildByPath("mapName");
                String streetName = "(无数据名)";
                String mapName = "(无地图名)";
                if (streetData != null)
                {
                    streetName = (String) streetData.getData();
                }
                if (mapData != null)
                {
                    mapName = (String) mapData.getData();
                }
                mapIds.add(mapId);
                writer.println(child2.getName() + " - " + streetName + " - " + mapName);
            }
            writer.println();
            Collections.sort(mapIds);
            for (Integer mapid : mapIds)
            {
                FileoutputUtil.log(child.getName() + ".txt", mapid + "", true);
            }
        }
        writer.flush();
        writer.close();
        System.out.println("Map.img 提取完成.");
    }
}
