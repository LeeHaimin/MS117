package dumpTools;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class FileoutputUtil
{

    // Logging output file
    public static final String Acc_Stuck = "log\\AccountStuck.log", Login_Error = "log\\Login_Error.log", PacketLog = "log\\PacketLog.log", SkillsLog = "log\\SkillsLog.log", SkillBuff = "log" +
            "\\SkillBuffLog.log", AttackLog = "log\\AttackLog.log", ClientError = "log\\ClientError.log", PlayerSkills = "log\\PlayerSkills.log", Zakum_Log = "log\\Log_Zakum.log", Horntail_Log =
            "log\\Log_Horntail.log", Pinkbean_Log = "log\\Pinkbean.log", ScriptEx_Log = "log\\Script_Except.log", PacketEx_Log = "log\\Packet_Except.log", // I cba looking for every error, adding
    // this back in.
    Donator_Log = "log\\Donator.log", Hacker_Log = "log\\Hacker.log", Movement_Log = "log\\Movement.log", 掉血错误 = "log\\掉血错误.log", 攻击出错 = "log\\攻击出错.log", 封包出错 = "log\\封包出错.log", 数据异常 = "log" +
            "\\数据异常.log", 在线统计 = "在线统计.txt", CommandEx_Log = "log\\Command_Except.log" //PQ_Log = "Log_PQ.rtf"
            ;
    // End
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat sdfGMT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat sdf_ = new SimpleDateFormat("yyyy-MM-dd");

    static
    {
        sdfGMT.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    public static void packetLog(String file, String msg)
    {
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(file, true);
            out.write(msg.getBytes());
            out.write(("\r\n\r\n").getBytes());
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignore)
            {
            }
        }
    }

    public static void log(String file, String msg)
    {
        log(file, msg, false);
    }

    public static void log(String file, String msg, boolean A)
    {
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(file, true);
            out.write(msg.getBytes());
            String rn = A ? "\r\n" : "\r\n------------------------ " + CurrentReadable_Time() + " ------------------------\r\n";
            out.write((rn).getBytes());
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignore)
            {
            }
        }
    }

    public static String CurrentReadable_Time()
    {
        return sdf.format(Calendar.getInstance().getTime());
    }

    public static void hiredMerchLog(String file, String msg)
    {
        String newfile = "log\\HiredMerch\\" + file + ".txt";
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(newfile, true);
            out.write(("[" + CurrentReadable_Time() + "] ").getBytes());
            out.write(msg.getBytes());
            out.write(("\r\n").getBytes());
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignore)
            {
            }
        }
    }

    public static void outputFileError(final String file, final Throwable t)
    {
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(file, true);
            out.write(getString(t).getBytes());
            out.write(("\r\n------------------------ " + CurrentReadable_Time() + " ------------------------\r\n").getBytes());
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignore)
            {
            }
        }
    }

    public static String getString(final Throwable e)
    {
        String retValue = null;
        StringWriter sw = null;
        PrintWriter pw = null;
        try
        {
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            retValue = sw.toString();
        }
        finally
        {
            try
            {
                if (pw != null)
                {
                    pw.close();
                }
                if (sw != null)
                {
                    sw.close();
                }
            }
            catch (IOException ignore)
            {
            }
        }
        return retValue;
    }

    public static String CurrentReadable_Date()
    {
        return sdf_.format(Calendar.getInstance().getTime());
    }

    public static String CurrentReadable_TimeGMT()
    {
        return sdfGMT.format(new Date());
    }
}
