package dumpTools;

import java.awt.Point;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import database.DatabaseConnectionWZ;
import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;

public class DumpMobSkills
{

    private final MapleDataProvider skill;
    private final Connection con = DatabaseConnectionWZ.getConnection();
    protected boolean hadError = false;
    protected boolean update = false;
    protected int id = 0;

    public DumpMobSkills(boolean update) throws Exception
    {
        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");//公司
        this.update = update;
        this.skill = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/Skill.wz"));
        if (skill == null)
        {
            hadError = true;
        }
    }

    public static void main(String[] args)
    {
        boolean hadError = false;
        boolean update = false;
        long startTime = System.currentTimeMillis();
        for (String file : args)
        {
            if (file.equalsIgnoreCase("-update"))
            {
                update = true;
            }
        }
        int currentQuest = 0;
        try
        {
            DumpMobSkills dq = new DumpMobSkills(update);
            System.out.println("Dumping mobskills");
            dq.dumpMobSkills();
            hadError |= dq.isHadError();
            currentQuest = dq.currentId();
        }
        catch (Exception e)
        {
            hadError = true;
            System.out.println(currentQuest + " 技能.");
        }
        long endTime = System.currentTimeMillis();
        double elapsedSeconds = (endTime - startTime) / 1000.0;
        int elapsedSecs = (((int) elapsedSeconds) % 60);
        int elapsedMinutes = (int) (elapsedSeconds / 60.0);

        String withErrors = "";
        if (hadError)
        {
            withErrors = " with errors";
        }
        System.out.println("Finished" + withErrors + " in " + elapsedMinutes + " 分 " + elapsedSecs + " 秒");
    }

    public void dumpMobSkills() throws Exception
    {
        if (!hadError)
        {
            PreparedStatement ps =
                    con.prepareStatement("INSERT INTO wz_mobskilldata(skillid, `level`, hp, mpcon, x, y, time, prop, `limit`, spawneffect,`interval`, summons, ltx, lty, rbx, rby, " + "once) VALUES "
                            + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            try
            {
                dumpMobSkills(ps);
            }
            catch (Exception e)
            {
                System.out.println(id + " 技能.");
                hadError = true;
            }
            finally
            {
                ps.executeBatch();
                ps.close();
            }
        }
    }

    public boolean isHadError()
    {
        return hadError;
    }

    public int currentId()
    {
        return id;
    }

    //kinda inefficient
    public void dumpMobSkills(PreparedStatement ps) throws Exception
    {
        if (!update)
        {
            delete("DELETE FROM wz_mobskilldata");
            System.out.println("删除 wz_mobskilldata 技能信息完成.");
        }
        final MapleData skillz = skill.getData("MobSkill.img");
        System.out.println("开始添加 wz_mobskilldata 技能信息.....");

        for (MapleData ids : skillz.getChildren())
        {
            for (MapleData lvlz : ids.getChildByPath("level").getChildren())
            {
                this.id = Integer.parseInt(ids.getName());
                int lvl = Integer.parseInt(lvlz.getName());
                if (update && doesExist("SELECT * FROM wz_mobskilldata WHERE skillid = " + id + " AND level = " + lvl))
                {
                    continue;
                }
                ps.setInt(1, id);
                ps.setInt(2, lvl);
                ps.setInt(3, MapleDataTool.getInt("hp", lvlz, 100));
                ps.setInt(4, MapleDataTool.getInt("mpCon", lvlz, 0));
                ps.setInt(5, MapleDataTool.getInt("x", lvlz, 1));
                ps.setInt(6, MapleDataTool.getInt("y", lvlz, 1));
                ps.setInt(7, MapleDataTool.getInt("time", lvlz, 0)); // * 1000
                ps.setInt(8, MapleDataTool.getInt("prop", lvlz, 100)); // / 100.0
                ps.setInt(9, MapleDataTool.getInt("limit", lvlz, 0));
                ps.setInt(10, MapleDataTool.getInt("summonEffect", lvlz, 0));
                ps.setInt(11, MapleDataTool.getInt("interval", lvlz, 0)); // * 1000

                StringBuilder summ = new StringBuilder();
                List<Integer> toSummon = new ArrayList<Integer>();
                for (int i = 0; i > -1; i++)
                {
                    if (lvlz.getChildByPath(String.valueOf(i)) == null)
                    {
                        break;
                    }
                    toSummon.add(MapleDataTool.getInt(lvlz.getChildByPath(String.valueOf(i)), 0));
                }
                for (Integer summon : toSummon)
                {
                    if (summ.length() > 0)
                    {
                        summ.append(", ");
                    }
                    summ.append(summon);
                }
                ps.setString(12, summ.toString());
                if (lvlz.getChildByPath("lt") != null)
                {
                    Point lt = (Point) lvlz.getChildByPath("lt").getData();
                    ps.setInt(13, lt.x);
                    ps.setInt(14, lt.y);
                }
                else
                {
                    ps.setInt(13, 0);
                    ps.setInt(14, 0);
                }
                if (lvlz.getChildByPath("rb") != null)
                {
                    Point rb = (Point) lvlz.getChildByPath("rb").getData();
                    ps.setInt(15, rb.x);
                    ps.setInt(16, rb.y);
                }
                else
                {
                    ps.setInt(15, 0);
                    ps.setInt(16, 0);
                }
                ps.setByte(17, (byte) (MapleDataTool.getInt("summonOnce", lvlz, 0) > 0 ? 1 : 0));
                System.out.println("添加技能: " + id + " 等级 " + lvl);
                ps.addBatch();
            }
        }
        System.out.println("添加 wz_mobskilldata 技能信息完成...");
    }

    public void delete(String sql) throws Exception
    {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.executeUpdate();
        ps.close();
    }

    public boolean doesExist(String sql) throws Exception
    {
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        boolean ret = rs.next();
        rs.close();
        ps.close();
        return ret;
    }
}