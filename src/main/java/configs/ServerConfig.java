package configs;


public class ServerConfig
{
    public static final short MAPLE_VERSION = 117;


    public static final String MAPLE_PATCH = "1";


    public static final byte MAPLE_TYPE = 4;


    public static final int BEGINNER_SPAWN_MAP = 10000;


    public static final boolean AUTO_BAN_ENABLE = false;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\configs\ServerConfig.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */