package constants;


public class SkillConstants
{
    public static final int[] defaultskills = {1101013, 1301013, 4100011, 3101009, 5100015};


    public static boolean isDefaultSkill(int skillId)
    {
        for (int i : defaultskills)
        {
            if (i == skillId)
            {
                return true;
            }
        }
        return false;
    }

    public static int getLuminousSkillMode(int skillId)
    {
        switch (skillId)
        {
            case 27001100:
            case 27101100:
            case 27101101:
            case 27111100:
            case 27111101:
            case 27121100:
                return 20040216;
            case 27001201:
            case 27101202:
            case 27111202:
            case 27120211:
            case 27121201:
            case 27121202:
                return 20040217;
            case 27111303:
            case 27121303:
                return 20040219;
        }
        return -1;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\SkillConstants.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */