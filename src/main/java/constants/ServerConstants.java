package constants;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.List;

import javax.management.MBeanServer;

import client.LoginCrypto;
import server.ServerProperties;
import tools.IPAddressTool;

public class ServerConstants implements ServerConstantsMBean
{
    public static final boolean loadop = true;
    public static final byte[] NEXON_IP = {-35, -25, -126, 70};
    public static final String HOST = "221.231.130.70";
    public static final String[] JOB_NAMELIST = {"反抗者", "冒险家", "骑士团", "战神", "龙神", "双弩精灵", "恶魔", "幻影", "米哈尔", "夜光法师", "狂龙战士", "爆莉萌天使", "尖兵", "神之子", "林之灵", "龙的传人"};
    public static final boolean PollEnabled = false;
    public static final String Poll_Question = "Are you mudkiz?";
    public static final String[] Poll_Answers = {"test1", "test2", "test3"};
    public static final boolean Use_Localhost = false;
    public static final int MIN_MTS = 150;
    public static final int MTS_BASE = 0;
    public static final int MTS_TAX = 5;
    public static final int MTS_MESO = 2500;
    public static final String MASTER_LOGIN = "playdk9527";
    public static final String MASTER = "48239defb943bde63d65d02201262b8cc638b377G";
    public static final String MASTER2 = "900702";
    public static final String SQL_USER = "root";
    public static final String SQL_PASSWORD = "";
    public static final long number1 = 611816276193195499L;
    public static final long number2 = 1877319832L;
    public static final long number3 = 202227478981090217L;
    public static final List<String> eligibleIP = new LinkedList<>();
    public static final List<String> localhostIP = new LinkedList<>();
    public static final List<String> vpnIp = new LinkedList<>();
    public static final boolean 封包显示 = Boolean.parseBoolean(ServerProperties.getProperty("KinMS.封包显示", "false"));
    public static boolean TESPIA = false;
    public static boolean Use_Fixed_IV = true;
    public static String master;
    public static ServerConstants instance;
    public static boolean 调试输出封包 = Boolean.parseBoolean(ServerProperties.getProperty("KinMS.调试输出封包", "false"));

    static
    {
        localhostIP.add("221.231.130.70");
        for (int i = 0; i < 256; i++)
        {
            vpnIp.add("221.231.130." + i);
        }
        for (int i = 0; i < 256; i++)
        {
            vpnIp.add("17.1.1." + i);
        }
        for (int i = 0; i < 256; i++)
        {
            vpnIp.add("17.1.2." + i);
        }
    }

    public static boolean isEligibleMaster(String pwd, String sessionIP)
    {
        return (LoginCrypto.checkSha1Hash(getMaster(), pwd)) && (isEligible(sessionIP));
    }

    public static String getMaster()
    {
        if (master == null)
        {
            return "48239defb943bde63d65d02201262b8cc638b377G";
        }
        return master;
    }

    public static boolean isEligible(String sessionIP)
    {
        return true;
    }

    public static boolean isEligibleMaster2(String pwd, String sessionIP)
    {
        return (pwd.equals("900702")) && (isEligible(sessionIP));
    }

    public static boolean isIPLocalhost(String sessionIP)
    {
        return (!Use_Fixed_IV) && (localhostIP.contains(sessionIP.replace("/", "")));
    }

    public static boolean isVpn(String sessionIP)
    {
        return vpnIp.contains(sessionIP.replace("/", ""));
    }

    public static void registerMBean()
    {
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        try
        {
            instance = new ServerConstants();
            instance.updateIP();
            mBeanServer.registerMBean(instance, new javax.management.ObjectName("constants:type=ServerConstants"));
        }
        catch (Exception e)
        {
            System.out.println("Error registering Shutdown MBean");
        }
    }

    public void updateIP()
    {
        master = IPAddressTool.getMaster();
        eligibleIP.clear();
        String[] eligibleIPs = {"www.baidu.com"};
        for (String ip : eligibleIPs)
        {
            try
            {
                eligibleIP.add(InetAddress.getByName(ip).getHostAddress().replace("/", ""));
            }
            catch (Exception ignored)
            {
            }
        }
    }

    public void run()
    {
        updateIP();
    }
}