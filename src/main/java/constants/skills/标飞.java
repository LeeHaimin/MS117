package constants.skills;

public class 标飞
{
    public static final int 精准暗器 = 4100000;
    public static final int 强力投掷 = 4100001;
    public static final int 物理训练 = 4100007;
    public static final int 刺客标记 = 4100011;
    public static final int 刺客标记_飞镖 = 4100012;
    public static final int 快速暗器 = 4101003;
    public static final int 爆裂飞镖 = 4101008;
    public static final int 风之护符 = 4101010;
    public static final int 永恒黑暗 = 4110008;
    public static final int 武器用毒液 = 4110011;
    public static final int 娴熟飞镖术 = 4110012;
    public static final int 药品吸收 = 4110014;
    public static final int 影分身 = 4111002;
    public static final int 影网术 = 4111003;
    public static final int 黑暗杂耍 = 4111007;
    public static final int 暗器伤人 = 4111009;
    public static final int 三连环光击破 = 4111010;
    public static final int 影子分裂 = 4111015;
    public static final int 假动作 = 4120002;
    public static final int 致命毒液 = 4120011;
    public static final int 暗器专家 = 4120012;
    public static final int 隐士标记 = 4120018;
    public static final int 隐士标记_飞镖 = 4120019;
    public static final int 冒险岛勇士 = 4121000;
    public static final int 勇士的意志 = 4121009;
    public static final int 四连镖 = 4121013;
    public static final int 黑暗祝福 = 4121014;
    public static final int 模糊领域 = 4121015;
    public static final int 突然袭击 = 4121016;
    public static final int 决战之巅 = 4121017;
    public static final int 神圣之力 = 4120030;
    public static final int 神圣敏捷 = 4120031;
    public static final int 神圣智力 = 4120032;
    public static final int 神圣幸运 = 4120033;
    public static final int 神圣爆击 = 4120034;
    public static final int 神圣精准 = 4120035;
    public static final int 神圣最大生命 = 4120036;
    public static final int 神圣最大魔法 = 4120037;
    public static final int 神圣最大恶魔精气 = 4120038;
    public static final int 神圣物理防御 = 4120039;
    public static final int 神圣魔法防御 = 4120040;
    public static final int 神圣移动 = 4120041;
    public static final int 神圣跳跃 = 4120042;
    public static final int 影子分裂_强化 = 4120043;
    public static final int 影子分裂_额外目标 = 4120044;
    public static final int 影子分裂_额外攻击 = 4120045;
    public static final int 模糊领域_增强 = 4120046;
    public static final int 模糊领域_减速 = 4120047;
    public static final int 模糊领域_缩短冷却时间 = 4120048;
    public static final int 四连镖_强化 = 4120049;
    public static final int 四连镖_BOSS杀手 = 4120050;
    public static final int 四连镖_额外攻击 = 4120051;
    public static final int 四季飞镖 = 4121052;
    public static final int 传说冒险家 = 4121053;
    public static final int 流血剧毒 = 4121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\标飞.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */