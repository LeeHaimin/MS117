package constants.skills;

public class 英雄
{
    public static final int 精准武器 = 1100000;
    public static final int 终极剑斧 = 1100002;
    public static final int 物理训练 = 1100009;
    public static final int 快速武器 = 1101004;
    public static final int 愤怒之火 = 1101006;
    public static final int 轻舞飞扬 = 1101011;
    public static final int 狂澜之力 = 1101012;
    public static final int 斗气集中 = 1101013;
    public static final int 自我恢复 = 1110000;
    public static final int 乘胜追击 = 1110009;
    public static final int 抵抗力 = 1110011;
    public static final int 斗气协合 = 1110013;
    public static final int 恐慌 = 1111003;
    public static final int 虎咆哮 = 1111008;
    public static final int 勇猛劈砍 = 1111010;
    public static final int 突进 = 1111012;
    public static final int 进阶斗气 = 1120003;
    public static final int 战斗精通 = 1120012;
    public static final int 进阶终极攻击 = 1120013;
    public static final int 稳如泰山 = 1120014;
    public static final int 终极打击_爆击 = 1120017;
    public static final int 冒险岛勇士 = 1121000;
    public static final int 终极打击 = 1121008;
    public static final int 葵花宝典 = 1121010;
    public static final int 勇士的意志 = 1121011;
    public static final int 烈焰冲斩 = 1121015;
    public static final int 魔击无效 = 1121016;
    public static final int 神圣之力 = 1120030;
    public static final int 神圣敏捷 = 1120031;
    public static final int 神圣智力 = 1120032;
    public static final int 神圣幸运 = 1120033;
    public static final int 神圣爆击 = 1120034;
    public static final int 神圣精准 = 1120035;
    public static final int 神圣最大生命 = 1120036;
    public static final int 神圣最大魔法 = 1120037;
    public static final int 神圣最大恶魔精气 = 1120038;
    public static final int 神圣物理防御 = 1120039;
    public static final int 神圣魔法防御 = 1120040;
    public static final int 神圣移动 = 1120041;
    public static final int 神圣跳跃 = 1120042;
    public static final int 进阶斗气_强化 = 1120043;
    public static final int 进阶斗气_额外机会 = 1120044;
    public static final int 进阶斗气_BOSS杀手 = 1120045;
    public static final int 进阶终极攻击_命中 = 1120046;
    public static final int 进阶终极攻击_额外伤害 = 1120047;
    public static final int 进阶终极攻击_额外机会 = 1120048;
    public static final int 终极打击_强化 = 1120049;
    public static final int 终极打击_额外目标 = 1120050;
    public static final int 终极打击_额外攻击 = 1120051;
    public static final int 狂怒连爆 = 1121052;
    public static final int 传说冒险家 = 1121053;
    public static final int 战灵附体 = 1121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\英雄.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */