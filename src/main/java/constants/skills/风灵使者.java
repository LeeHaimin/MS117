package constants.skills;

public class 风灵使者
{
    public static final int 风之私语 = 13000023;
    public static final int 清风箭 = 13001020;
    public static final int 风影漫步_新 = 13001021;
    public static final int 元素_风 = 13001022;
    public static final int 狂风肆虐Ⅰ = 13100022;
    public static final int 精准弓_新 = 13100025;
    public static final int 物理训练_新 = 13100026;
    public static final int 狂风肆虐Ⅰ_强化 = 13100027;
    public static final int 仙灵回旋 = 13101020;
    public static final int 疾风箭 = 13101021;
    public static final int 快速箭_新 = 13101023;
    public static final int 风精灵相助 = 13101024;
    public static final int 狂风肆虐Ⅱ = 13110022;
    public static final int 轻如鸿毛 = 13110025;
    public static final int 重振精神 = 13110026;
    public static final int 狂风肆虐Ⅱ_强化 = 13110027;
    public static final int 风霜雪舞 = 13111020;
    public static final int 精准箭 = 13111021;
    public static final int 信天翁_新 = 13111023;
    public static final int 绿水晶花 = 13111024;
    public static final int 狂风肆虐Ⅲ = 13120003;
    public static final int 神箭手_新 = 13120006;
    public static final int 钻石星尘 = 13120007;
    public static final int 极限信天翁 = 13120008;
    public static final int 狂风肆虐Ⅲ_强化 = 13120010;
    public static final int 希纳斯的骑士 = 13121000;
    public static final int 天空之歌 = 13121001;
    public static final int 旋风箭 = 13121002;
    public static final int 风之祝福 = 13121004;
    public static final int 火眼晶晶 = 13121005;
    public static final int 旋风箭_溅射 = 13121009;
    public static final int 神圣之力 = 13120030;
    public static final int 神圣敏捷 = 13120031;
    public static final int 神圣智力 = 13120032;
    public static final int 神圣幸运 = 13120033;
    public static final int 神圣爆击 = 13120034;
    public static final int 神圣精准 = 13120035;
    public static final int 神圣最大生命 = 13120036;
    public static final int 神圣最大魔法 = 13120037;
    public static final int 神圣最大恶魔精气 = 13120038;
    public static final int 神圣物理防御 = 13120039;
    public static final int 神圣魔法防御 = 13120040;
    public static final int 神圣移动 = 13120041;
    public static final int 神圣跳跃 = 13120042;
    public static final int 狂风肆虐_强化 = 13120043;
    public static final int 狂风肆虐_增强 = 13120044;
    public static final int 狂风肆虐_二次机会 = 13120045;
    public static final int 旋风箭_强化 = 13120046;
    public static final int 旋风箭_额外目标 = 13120047;
    public static final int 旋风箭_额外攻击 = 13120048;
    public static final int 天空之歌_强化 = 13120049;
    public static final int 天空之歌_无视防御 = 13120050;
    public static final int 天空之歌_BOSS杀手 = 13120051;
    public static final int 季候风 = 13121052;
    public static final int 守护者之荣誉 = 13121053;
    public static final int 暴风灭世 = 13121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\风灵使者.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */