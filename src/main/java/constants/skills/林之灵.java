package constants.skills;

public class 林之灵
{
    public static final int 精灵集中 = 110000800;
    public static final int 模式解除 = 110001500;
    public static final int 巨熊模式 = 110001501;
    public static final int 雪豹模式 = 110001502;
    public static final int 猛鹰模式 = 110001503;
    public static final int 猫咪模式 = 110001504;
    public static final int 守护者的身手 = 110001506;
    public static final int 守护模式变更 = 110001510;
    public static final int 驯兽魔法棒练习 = 110000515;
    public static final int 林之灵之修养 = 110000513;
    public static final int 冒险岛守护勇士 = 110001511;
    public static final int 林之灵之意志 = 110001512;
    public static final int 前爪挥击 = 112000000;
    public static final int 火焰屁 = 112001006;
    public static final int 波波之粮食储备 = 112000011;
    public static final int 波波之耐心 = 112000010;
    public static final int 吸收 = 112001004;
    public static final int 波波之额外攻击 = 112000015;
    public static final int 波波之坚韧 = 112000012;
    public static final int 怒之乱击 = 112000003;
    public static final int 波波之致命一击 = 112000014;
    public static final int 强力吸收 = 112001005;
    public static final int 小波波 = 112001007;
    public static final int 波波之勇猛 = 112000013;
    public static final int 集中打击 = 112001009;
    public static final int 生鲜龙卷风 = 112001008;
    public static final int 波波之重生 = 112000016;
    public static final int 火焰屁_强化 = 112000020;
    public static final int 掀桌 = 112001018;
    public static final int 前爪挥击2 = 112000001;
    public static final int 前爪挥击3 = 112000002;
    public static final int 巨熊35击 = 112000019;
    public static final int 雪豹重斩 = 112100000;
    public static final int 迅雷冲刺 = 112101007;
    public static final int 拉伊之皮_强化 = 112100011;
    public static final int 雪豹强袭 = 112100002;
    public static final int 拉伊之力_强化 = 112100013;
    public static final int 男子汉之舞 = 112101004;
    public static final int 男子汉步伐 = 112101005;
    public static final int 嗨_兄弟 = 112101016;
    public static final int 雪豹咆哮 = 112100003;
    public static final int 闪电掠影 = 112100008;
    public static final int 男子汉姿态 = 112100006;
    public static final int 致命三角 = 112101009;
    public static final int 进阶迅雷冲刺 = 112100012;
    public static final int 拉伊之牙_强化 = 112100010;
    public static final int 拉伊之爪_强化 = 112100014;
    public static final int 拉伊之心_强化 = 112100015;
    public static final int 尽情狂欢 = 112101017;
    public static final int 雪豹_未知 = 112100001;
    public static final int 编队攻击 = 112110003;
    public static final int 伊卡跳跃 = 112111017;
    public static final int 伊卡跳跃_高度 = 112111002;
    public static final int 设置伊卡驿站 = 112111010;
    public static final int 伊卡飞翔 = 112111000;
    public static final int 伊卡之翼_强化 = 112111007;
    public static final int 伊卡的灵敏身手 = 112110014;
    public static final int 编队掩护飞行 = 112110005;
    public static final int 伊卡滑降 = 112111001;
    public static final int 伊卡羽毛披风 = 112110012;
    public static final int 呼叫好友 = 112111011;
    public static final int 伊卡之爪_强化 = 112111006;
    public static final int 编队轰炸 = 112111004;
    public static final int 伊卡之眼_强化 = 112111009;
    public static final int 编队_强化 = 112110015;
    public static final int 伊卡之喙_强化 = 112111008;
    public static final int 伊卡羽毛鞋 = 112110013;
    public static final int 旋风飞行 = 112111016;
    public static final int 伙伴发射 = 112120000;
    public static final int 阿尔之萌 = 112120015;
    public static final int 阿尔之魅力_强化 = 112120021;
    public static final int 阿尔之饱腹感 = 112120023;
    public static final int 阿尔之意志 = 112120014;
    public static final int 阿尔之窃取 = 112120017;
    public static final int 喵喵治愈 = 112121013;
    public static final int 阿尔之爪 = 112120018;
    public static final int 喵喵消失 = 112121010;
    public static final int 喵喵空间 = 112121005;
    public static final int 喵喵卡片 = 112121006;
    public static final int 伙伴发射_强化 = 112120058;
    public static final int 阿尔之弱点把握 = 112120022;
    public static final int 猫咪发射_ = 112121004;
    public static final int 喵喵复活 = 112121011;
    public static final int 喵喵金卡 = 112120019;
    public static final int 阿尔之好伙伴 = 112120016;
    public static final int 绚烂电击_ = 112121057;
    public static final int 伙伴发射2 = 112120001;
    public static final int 伙伴发射3 = 112120002;
    public static final int 伙伴发射4 = 112120003;
    public static final int 红色卡片 = 112121007;
    public static final int 蓝色卡片 = 112121008;
    public static final int 绿色卡片 = 112121009;
    public static final int 金卡 = 112121020;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\林之灵.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */