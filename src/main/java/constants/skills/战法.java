package constants.skills;

public class 战法
{
    public static final int 精灵弱化 = 32000012;
    public static final int 惩戒 = 32000013;
    public static final int 长杖艺术 = 32000015;
    public static final int 三段冲击 = 32001000;
    public static final int 快速移动 = 32001002;
    public static final int 黑暗灵气 = 32001003;
    public static final int 精准长杖 = 32100006;
    public static final int 智慧激发 = 32100007;
    public static final int 普通转化 = 32100008;
    public static final int 四段冲击 = 32101000;
    public static final int 超级黑暗锁链 = 32101001;
    public static final int 黄色灵气 = 32101003;
    public static final int 伤害吸收 = 32101004;
    public static final int 快速长杖 = 32101005;
    public static final int 进阶蓝色灵气 = 32110000;
    public static final int 战斗精通 = 32110001;
    public static final int 霸体1 = 32110007;
    public static final int 霸体2 = 32110008;
    public static final int 霸体3 = 32110009;
    public static final int 死亡冲击 = 32111002;
    public static final int 黑暗闪电 = 32111003;
    public static final int 霸体 = 32111005;
    public static final int 重生 = 32111006;
    public static final int 快速移动精通 = 32111010;
    public static final int 蓝色灵气 = 32111012;
    public static final int 稳如泰山 = 32111014;
    public static final int 斗战突击 = 32111015;
    public static final int 进阶黑暗灵气 = 32120000;
    public static final int 进阶黄色灵气 = 32120001;
    public static final int 进阶黑暗灵气_融合 = 32120013;
    public static final int 进阶黄色灵气_融合 = 32120014;
    public static final int 进阶蓝色灵气_融合 = 32120015;
    public static final int 长杖专家 = 32120016;
    public static final int 致命冲击 = 32121002;
    public static final int 飓风 = 32121003;
    public static final int 黑暗创世 = 32121004;
    public static final int 灵气大融合 = 32121005;
    public static final int 避难所 = 32121006;
    public static final int 冒险岛勇士 = 32121007;
    public static final int 勇士的意志 = 32121008;
    public static final int 暴怒对战 = 32121010;
    public static final int 未知技能 = 32121011;
    public static final int 神圣之力 = 32120030;
    public static final int 神圣敏捷 = 32120031;
    public static final int 神圣智力 = 32120032;
    public static final int 神圣幸运 = 32120033;
    public static final int 神圣爆击 = 32120034;
    public static final int 神圣精准 = 32120035;
    public static final int 神圣最大生命 = 32120036;
    public static final int 神圣最大魔法 = 32120037;
    public static final int 神圣最大恶魔精气 = 32120038;
    public static final int 神圣物理防御 = 32120039;
    public static final int 神圣魔法防御 = 32120040;
    public static final int 神圣移动 = 32120041;
    public static final int 神圣跳跃 = 32120042;
    public static final int 快速移动精通_强化 = 32120043;
    public static final int 快速移动精通_范围提升 = 32120044;
    public static final int 快速移动精通_增强 = 32120045;
    public static final int 黑暗闪电_强化 = 32120046;
    public static final int 黑暗闪电_分裂伤害 = 32120047;
    public static final int 黑暗闪电_缩短冷却时间 = 32120048;
    public static final int 飓风_强化 = 32120049;
    public static final int 飓风_额外目标 = 32120050;
    public static final int 飓风_缩短冷却时间 = 32120051;
    public static final int 如意棒 = 32120052;
    public static final int 如意棒_第2击 = 32120055;
    public static final int 自由之墙 = 32121053;
    public static final int 结合灵气 = 32121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\战法.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */