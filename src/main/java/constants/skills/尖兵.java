package constants.skills;

public class 尖兵
{
    public static final int 急速支援 = 30020232;
    public static final int 混合逻辑 = 30020233;
    public static final int 多线程Ⅰ = 30020234;
    public static final int 机械战车冲刺 = 30021235;
    public static final int 多模式链接 = 30021236;
    public static final int 自由飞行 = 30021237;
    public static final int 伪装 = 30020240;
    public static final int 神经系统改造 = 36000003;
    public static final int 多线程Ⅱ = 36000004;
    public static final int 能量曲线 = 36001000;
    public static final int 突然加速 = 36001001;
    public static final int 超能力量 = 36001002;
    public static final int 精准火箭 = 36001005;
    public static final int 精英支援 = 36100005;
    public static final int 尖兵精通 = 36100006;
    public static final int 多线程Ⅲ = 36100007;
    public static final int 精准火箭1次强化 = 36100010;
    public static final int 银色快剑_闪光 = 36101000;
    public static final int 原子推进器 = 36101001;
    public static final int 直线透视 = 36101002;
    public static final int 高效输能 = 36101003;
    public static final int 尖兵加速 = 36101004;
    public static final int 银色快剑_集中 = 36101008;
    public static final int 银色快剑_跳跃 = 36101009;
    public static final int 宙斯盾系统 = 36110004;
    public static final int 三角进攻 = 36110005;
    public static final int 多线程Ⅳ = 36110007;
    public static final int 精准火箭2次强化 = 36110012;
    public static final int 战斗切换_爆炸 = 36111000;
    public static final int 斜线追击 = 36111001;
    public static final int 反重力力场 = 36111002;
    public static final int 双重防御 = 36111003;
    public static final int 全息投影 = 36111006;
    public static final int 额外供给 = 36111008;
    public static final int 战斗切换_击落 = 36111009;
    public static final int 战斗切换_分裂 = 36111010;
    public static final int 瞬间闪避 = 36120005;
    public static final int 尖兵专家 = 36120006;
    public static final int 多线程Ⅴ = 36120010;
    public static final int 精准火箭最终强化 = 36120015;
    public static final int 多线程Ⅵ = 36120016;
    public static final int 刀锋之舞 = 36121000;
    public static final int 聚能脉冲炮_狙击 = 36121001;
    public static final int 全息力场_穿透 = 36121002;
    public static final int 神秘代码 = 36121003;
    public static final int 攻击矩阵 = 36121004;
    public static final int 时间胶囊 = 36121007;
    public static final int 冒险岛勇士 = 36121008;
    public static final int 勇士的意志 = 36121009;
    public static final int 聚能脉冲炮_炮击 = 36121011;
    public static final int 聚能脉冲炮_暴击 = 36121012;
    public static final int 全息力场_力场 = 36121013;
    public static final int 全息力场_支援 = 36121014;
    public static final int 神圣之力 = 36120030;
    public static final int 神圣敏捷 = 36120031;
    public static final int 神圣智力 = 36120032;
    public static final int 神圣幸运 = 36120033;
    public static final int 神圣爆击 = 36120034;
    public static final int 神圣精准 = 36120035;
    public static final int 神圣最大生命 = 36120036;
    public static final int 神圣最大魔法 = 36120037;
    public static final int 神圣最大恶魔精气 = 36120038;
    public static final int 神圣物理防御 = 36120039;
    public static final int 神圣魔法防御 = 36120040;
    public static final int 神圣移动 = 36120041;
    public static final int 神圣跳跃 = 36120042;
    public static final int 刀锋之舞_快速移动 = 36120043;
    public static final int 刀锋之舞_强化 = 36120044;
    public static final int 刀锋之舞_额外目标 = 36120045;
    public static final int 聚能脉冲炮_强化 = 36120046;
    public static final int 聚能脉冲炮_无视防御 = 36120047;
    public static final int 聚能脉冲炮_额外目标 = 36120048;
    public static final int 全息力场_加速 = 36120049;
    public static final int 全息力场_强化 = 36120050;
    public static final int 全息力场_坚持 = 36120051;
    public static final int 毁灭死光 = 36121052;
    public static final int 禁锢束缚 = 36121053;
    public static final int 永动引擎 = 36121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\尖兵.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */