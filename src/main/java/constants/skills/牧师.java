package constants.skills;

public class 牧师
{
    public static final int 魔力吸收 = 2300000;
    public static final int 咒语精通 = 2300006;
    public static final int 智慧激发 = 2300007;
    public static final int 祈祷众生 = 2300009;
    public static final int 群体治愈 = 2301002;
    public static final int 神之保护 = 2301003;
    public static final int 祝福 = 2301004;
    public static final int 圣箭术 = 2301005;
    public static final int 魔法狂暴 = 2301008;
    public static final int 神圣集中 = 2310008;
    public static final int 魔法爆击 = 2310010;
    public static final int 净化 = 2311001;
    public static final int 时空门 = 2311002;
    public static final int 神圣祈祷 = 2311003;
    public static final int 圣光 = 2311004;
    public static final int 快速移动精通 = 2311007;
    public static final int 神圣魔法盾 = 2311009;
    public static final int 神圣源泉 = 2311011;
    public static final int 神圣保护 = 2311012;
    public static final int 神秘瞄准术 = 2320011;
    public static final int 魔力精通 = 2320012;
    public static final int 祈祷和音 = 2320013;
    public static final int 冒险岛勇士 = 2321000;
    public static final int 创世之破 = 2321001;
    public static final int 强化圣龙 = 2321003;
    public static final int 终极无限 = 2321004;
    public static final int 进阶祝福 = 2321005;
    public static final int 复活术 = 2321006;
    public static final int 光芒飞箭 = 2321007;
    public static final int 圣光普照 = 2321008;
    public static final int 勇士的意志 = 2321009;
    public static final int 神圣之力 = 2320030;
    public static final int 神圣敏捷 = 2320031;
    public static final int 神圣智力 = 2320032;
    public static final int 神圣幸运 = 2320033;
    public static final int 神圣爆击 = 2320034;
    public static final int 神圣精准 = 2320035;
    public static final int 神圣最大生命 = 2320036;
    public static final int 神圣最大魔法 = 2320037;
    public static final int 神圣最大恶魔精气 = 2320038;
    public static final int 神圣物理防御 = 2320039;
    public static final int 神圣魔法防御 = 2320040;
    public static final int 神圣移动 = 2320041;
    public static final int 神圣跳跃 = 2320042;
    public static final int 神圣魔法盾_额外格挡 = 2320043;
    public static final int 神圣魔法盾_坚持 = 2320044;
    public static final int 神圣魔法盾_缩短冷却时间 = 2320045;
    public static final int 神圣祈祷_经验值 = 2320046;
    public static final int 神圣祈祷_身体抗性 = 2320047;
    public static final int 神圣祈祷_额外掉落 = 2320048;
    public static final int 进阶祝福_额外伤害 = 2320049;
    public static final int 进阶祝福_增强 = 2320050;
    public static final int 进阶祝福_额外点数 = 2320051;
    public static final int 天堂之门 = 2321052;
    public static final int 传说冒险家 = 2321053;
    public static final int 天使复仇 = 2321054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\牧师.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */