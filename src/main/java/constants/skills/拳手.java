package constants.skills;

public class 拳手
{
    public static final int 精准拳 = 5100001;
    public static final int HP增加 = 5100009;
    public static final int 物理训练 = 5100010;
    public static final int 忍耐 = 5100013;
    public static final int 能量获得 = 5100015;
    public static final int 贯骨击 = 5101004;
    public static final int 急速拳 = 5101006;
    public static final int 静心 = 5101011;
    public static final int 龙卷风拳 = 5101012;
    public static final int 能量旋风 = 5101014;
    public static final int 迷惑攻击 = 5110000;
    public static final int 致命狂热 = 5110011;
    public static final int 超级冲击 = 5110014;
    public static final int 能量爆破 = 5111002;
    public static final int 幸运骰子 = 5111007;
    public static final int 双龙飓风 = 5111009;
    public static final int 树木防御 = 5111010;
    public static final int 碎石乱击 = 5111012;
    public static final int 爆能破袭 = 5111013;
    public static final int 碎石乱击_1 = 5111015;
    public static final int 反制攻击 = 5120011;
    public static final int 双幸运骰子 = 5120012;
    public static final int 重装碾压 = 5120014;
    public static final int 终极冲击 = 5120018;
    public static final int 冒险岛勇士 = 5121000;
    public static final int 潜龙出渊 = 5121001;
    public static final int 激怒拳 = 5121007;
    public static final int 勇士的意志 = 5121008;
    public static final int 极速领域 = 5121009;
    public static final int 伺机待发 = 5121010;
    public static final int 诺特勒斯战舰 = 5121013;
    public static final int 蛇拳 = 5121015;
    public static final int 能量爆炸 = 5121016;
    public static final int 双重爆炸 = 5121017;
    public static final int 潜龙出渊_1 = 5121019;
    public static final int 暴怒拳 = 5121020;
    public static final int 神圣之力 = 5120030;
    public static final int 神圣敏捷 = 5120031;
    public static final int 神圣智力 = 5120032;
    public static final int 神圣幸运 = 5120033;
    public static final int 神圣爆击 = 5120034;
    public static final int 神圣精准 = 5120035;
    public static final int 神圣最大生命 = 5120036;
    public static final int 神圣最大魔法 = 5120037;
    public static final int 神圣最大恶魔精气 = 5120038;
    public static final int 神圣物理防御 = 5120039;
    public static final int 神圣魔法防御 = 5120040;
    public static final int 神圣移动 = 5120041;
    public static final int 神圣跳跃 = 5120042;
    public static final int 双幸运骰子_再一次机会 = 5120043;
    public static final int 双幸运骰子_额外数字 = 5120044;
    public static final int 双幸运骰子_增强 = 5120045;
    public static final int 激怒拳_强化 = 5120046;
    public static final int 激怒拳_BOSS杀手 = 5120047;
    public static final int 激怒拳_额外攻击 = 5120048;
    public static final int 能量爆炸_强化 = 5120049;
    public static final int 能量爆炸_额外目标 = 5120050;
    public static final int 能量爆炸_额外攻击 = 5120051;
    public static final int 混元归一 = 5121052;
    public static final int 传说冒险家 = 5121053;
    public static final int 能量激发 = 5121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\拳手.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */