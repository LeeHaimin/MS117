package constants.skills;

public class 双弩
{
    public static final int 水晶投掷 = 20021000;
    public static final int 潜入 = 20021001;
    public static final int 效率提升 = 20020002;
    public static final int 精灵恢复 = 20020109;
    public static final int 精灵的祝福 = 20021110;
    public static final int 优雅移动 = 20020111;
    public static final int 王者资格 = 20020112;
    public static final int 匠人之魂 = 20021003;
    public static final int 英雄之回声 = 20021005;
    public static final int 锻造 = 20021007;
    public static final int 流星竹雨 = 20021009;
    public static final int 金刚霸体 = 20021010;
    public static final int 狂暴战魂 = 20021011;
    public static final int 潜力激发 = 23000001;
    public static final int 敏锐瞄准 = 23000003;
    public static final int 自然吸收 = 23000004;
    public static final int 快速二连射 = 23001000;
    public static final int 杂耍跳 = 23001002;
    public static final int 终结箭 = 23100004;
    public static final int 精准双弩枪 = 23100005;
    public static final int 终极双弩枪 = 23100006;
    public static final int 物理训练 = 23100008;
    public static final int 十字穿刺 = 23101000;
    public static final int 冲锋拳 = 23101001;
    public static final int 快速双弩枪 = 23101002;
    public static final int 精神注入 = 23101003;
    public static final int 冲锋拳1 = 23101007;
    public static final int 爆裂飞腿 = 23110006;
    public static final int 急袭双杀 = 23111000;
    public static final int 飞叶龙卷风 = 23111001;
    public static final int 独角兽之角 = 23111002;
    public static final int 狂风俯冲 = 23111003;
    public static final int 火焰咆哮 = 23111004;
    public static final int 水盾 = 23111005;
    public static final int 精灵骑士 = 23111008;
    public static final int 精灵骑士1 = 23111009;
    public static final int 精灵骑士2 = 23111010;
    public static final int 双弩枪专家 = 23120009;
    public static final int 防御突破 = 23120010;
    public static final int 旋转月瀑坠击 = 23120011;
    public static final int 进阶终极攻击 = 23120012;
    public static final int 伊师塔之环 = 23121000;
    public static final int 传说之矛 = 23121002;
    public static final int 闪电刀刃 = 23121003;
    public static final int 古老意志 = 23121004;
    public static final int 冒险岛勇士 = 23121005;
    public static final int 勇士的意志 = 23121008;
    public static final int 进阶急袭双杀 = 23120013;
    public static final int 神圣之力 = 23120030;
    public static final int 神圣敏捷 = 23120031;
    public static final int 神圣智力 = 23120032;
    public static final int 神圣幸运 = 23120033;
    public static final int 神圣爆击 = 23120034;
    public static final int 神圣精准 = 23120035;
    public static final int 神圣最大生命 = 23120036;
    public static final int 神圣最大魔法 = 23120037;
    public static final int 神圣最大恶魔精气 = 23120038;
    public static final int 神圣物理防御 = 23120039;
    public static final int 神圣魔法防御 = 23120040;
    public static final int 神圣移动 = 23120041;
    public static final int 神圣跳跃 = 23120042;
    public static final int 伊师塔之环_强化 = 23120043;
    public static final int 伊师塔之环_范围提升 = 23120044;
    public static final int 伊师塔之环_链接强化 = 23120045;
    public static final int 水盾_强化 = 23120046;
    public static final int 水盾_抗性提升1 = 23120047;
    public static final int 水盾_抗性提升2 = 23120048;
    public static final int 传说之矛_强化 = 23120049;
    public static final int 传说之矛_减少防御 = 23120050;
    public static final int 传说之矛_链接强化 = 23120051;
    public static final int 艾琳之怒 = 23121052;
    public static final int 英雄奥斯 = 23121053;
    public static final int 小精灵祝福 = 23121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\双弩.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */