package constants.skills;

public class 枪手
{
    public static final int 精准枪 = 5200000;
    public static final int 物理训练 = 5200009;
    public static final int 三连射杀 = 5200010;
    public static final int 快枪手 = 5201001;
    public static final int 速射 = 5201003;
    public static final int 轻羽鞋_下落 = 5201005;
    public static final int 激退射杀 = 5201006;
    public static final int 无限子弹 = 5201008;
    public static final int 轻羽鞋 = 5201011;
    public static final int 召唤船员 = 5201012;
    public static final int 召唤船员2 = 5201013;
    public static final int 召唤船员3 = 5201014;
    public static final int 坚忍不拔 = 5210012;
    public static final int 合金子弹 = 5210013;
    public static final int 集合船员 = 5210015;
    public static final int 集合船员2 = 5210016;
    public static final int 集合船员3 = 5210017;
    public static final int 集合船员4 = 5210018;
    public static final int 幸运骰子 = 5211007;
    public static final int 双管枪射击 = 5211008;
    public static final int 空尖弹 = 5211009;
    public static final int 冲撞弹 = 5211010;
    public static final int 八轮重机枪 = 5211014;
    public static final int 反制攻击 = 5220012;
    public static final int 双幸运骰子 = 5220014;
    public static final int 船员统帅 = 5220019;
    public static final int 船长的威严 = 5220020;
    public static final int 战船轰炸机_爆炸 = 5220023;
    public static final int 冒险岛勇士 = 5221000;
    public static final int 金属风暴 = 5221004;
    public static final int 勇士的意志 = 5221010;
    public static final int 诺特勒斯战舰 = 5221013;
    public static final int 无尽追击 = 5221015;
    public static final int 爆头 = 5221016;
    public static final int 齐射 = 5221017;
    public static final int 海盗气魄 = 5221018;
    public static final int 神速衔接 = 5221021;
    public static final int 战船轰炸机 = 5221022;
    public static final int 神圣之力 = 5220030;
    public static final int 神圣敏捷 = 5220031;
    public static final int 神圣智力 = 5220032;
    public static final int 神圣幸运 = 5220033;
    public static final int 神圣爆击 = 5220034;
    public static final int 神圣精准 = 5220035;
    public static final int 神圣最大生命 = 5220036;
    public static final int 神圣最大魔法 = 5220037;
    public static final int 神圣最大恶魔精气 = 5220038;
    public static final int 神圣物理防御 = 5220039;
    public static final int 神圣魔法防御 = 5220040;
    public static final int 神圣移动 = 5220041;
    public static final int 神圣跳跃 = 5220042;
    public static final int 双管枪射击_强化 = 5220043;
    public static final int 双管枪射击_额外目标 = 5220044;
    public static final int 双管枪射击_额外攻击 = 5220045;
    public static final int 齐射_强化 = 5220046;
    public static final int 齐射_额外目标 = 5220047;
    public static final int 齐射_额外攻击 = 5220048;
    public static final int 金属风暴_强化 = 5220049;
    public static final int 金属风暴_范围提升 = 5220050;
    public static final int 金属风暴_BOSS杀手 = 5220051;
    public static final int 奇异炸弹 = 5221052;
    public static final int 传说冒险家 = 5221053;
    public static final int 不倦神酒 = 5221054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\枪手.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */