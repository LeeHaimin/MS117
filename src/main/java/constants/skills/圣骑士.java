package constants.skills;

public class 圣骑士
{
    public static final int 精准武器 = 1200000;
    public static final int 终极剑钝器 = 1200002;
    public static final int 物理训练 = 1200009;
    public static final int 元素冲击 = 1200014;
    public static final int 快速武器 = 1201004;
    public static final int 火焰冲击 = 1201011;
    public static final int 寒冰冲击 = 1201012;
    public static final int 准骑士密令 = 1201013;
    public static final int 盾防精通 = 1210001;
    public static final int 阿基里斯 = 1210015;
    public static final int 祝福护甲 = 1210016;
    public static final int 雷鸣冲击 = 1211008;
    public static final int 元气恢复 = 1211010;
    public static final int 战斗命令 = 1211011;
    public static final int 突进 = 1211012;
    public static final int 压制术 = 1211013;
    public static final int 抗震防御 = 1211014;
    public static final int 万佛归一破 = 1220010;
    public static final int 稳如泰山 = 1220017;
    public static final int 圣骑士专家 = 1220018;
    public static final int 冒险岛勇士 = 1221000;
    public static final int 神圣冲击 = 1221004;
    public static final int 连环环破 = 1221009;
    public static final int 圣域 = 1221011;
    public static final int 勇士的意志 = 1221012;
    public static final int 魔击无效 = 1221014;
    public static final int 虚空元素 = 1221015;
    public static final int 守护之神 = 1221016;
    public static final int 神圣之力 = 1220030;
    public static final int 神圣敏捷 = 1220031;
    public static final int 神圣智力 = 1220032;
    public static final int 神圣幸运 = 1220033;
    public static final int 神圣爆击 = 1220034;
    public static final int 神圣精准 = 1220035;
    public static final int 神圣最大生命 = 1220036;
    public static final int 神圣最大魔法 = 1220037;
    public static final int 神圣最大恶魔精气 = 1220038;
    public static final int 神圣物理防御 = 1220039;
    public static final int 神圣魔法防御 = 1220040;
    public static final int 神圣移动 = 1220041;
    public static final int 神圣跳跃 = 1220042;
    public static final int 压制术_坚持 = 1220043;
    public static final int 压制术_额外机会 = 1220044;
    public static final int 压制术_增强 = 1220045;
    public static final int 连环环破_强化 = 1220046;
    public static final int 连环环破_爆击率 = 1220047;
    public static final int 连环环破_额外攻击 = 1220048;
    public static final int 圣域_强化 = 1220049;
    public static final int 圣域_额外攻击 = 1220050;
    public static final int 圣域_缩短冷却时间 = 1220051;
    public static final int 毁灭 = 1221052;
    public static final int 传说冒险家 = 1221053;
    public static final int 至圣领域 = 1221054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\圣骑士.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */