package constants.skills;

public class 炮手
{
    public static final int 升级火炮 = 5010003;
    public static final int 幸运一击 = 5010004;
    public static final int 溅射炮 = 5011000;
    public static final int 炮击拳 = 5011001;
    public static final int 紧急后撤 = 5011002;
    public static final int 致命炮火 = 5300004;
    public static final int 精准炮 = 5300005;
    public static final int 猴子炸药桶_爆炸 = 5300007;
    public static final int 海盗训练 = 5300008;
    public static final int 强击炮 = 5301000;
    public static final int 猴子炸药桶 = 5301001;
    public static final int 大炮加速 = 5301002;
    public static final int 猴子魔法 = 5301003;
    public static final int 火炮强化 = 5310006;
    public static final int 生命强化 = 5310007;
    public static final int 猴子冲击波_1 = 5310008;
    public static final int 反击炮 = 5310009;
    public static final int 穿甲炮 = 5311000;
    public static final int 猴子超级炸弹 = 5311001;
    public static final int 猴子冲击波 = 5311002;
    public static final int 炮击跳跃 = 5311003;
    public static final int 随机橡木桶 = 5311004;
    public static final int 幸运骰子 = 5311005;
    public static final int 双幸运骰子 = 5320007;
    public static final int 超级猴子魔法 = 5320008;
    public static final int 极限燃烧弹 = 5320009;
    public static final int 双胞胎猴子支援_1 = 5320011;
    public static final int 加农火箭炮 = 5321000;
    public static final int 诺特勒斯战舰 = 5321001;
    public static final int 磁性船锚 = 5321003;
    public static final int 双胞胎猴子支援 = 5321004;
    public static final int 冒险岛勇士 = 5321005;
    public static final int 勇士的意志 = 5321006;
    public static final int 海盗精神 = 5321010;
    public static final int 集中炮击 = 5321012;
    public static final int 神圣之力 = 5320030;
    public static final int 神圣敏捷 = 5320031;
    public static final int 神圣智力 = 5320032;
    public static final int 神圣幸运 = 5320033;
    public static final int 神圣爆击 = 5320034;
    public static final int 神圣精准 = 5320035;
    public static final int 神圣最大生命 = 5320036;
    public static final int 神圣最大魔法 = 5320037;
    public static final int 神圣最大恶魔精气 = 5320038;
    public static final int 神圣物理防御 = 5320039;
    public static final int 神圣魔法防御 = 5320040;
    public static final int 神圣移动 = 5320041;
    public static final int 神圣跳跃 = 5320042;
    public static final int 双胞胎猴子支援_增加 = 5320043;
    public static final int 双胞胎猴子支援_坚持 = 5320044;
    public static final int 双胞胎猴子支援_增强 = 5320045;
    public static final int 加农火箭炮_强化 = 5320046;
    public static final int 加农火箭炮_额外目标 = 5320047;
    public static final int 加农火箭炮_额外攻击 = 5320048;
    public static final int 集中炮击_强化 = 5320049;
    public static final int 集中炮击_爆击率 = 5320050;
    public static final int 集中炮击_额外攻击 = 5320051;
    public static final int 旋转彩虹炮 = 5321052;
    public static final int 传说冒险家 = 5321053;
    public static final int 霰弹炮 = 5321054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\炮手.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */