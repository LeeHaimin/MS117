package constants.skills;

public class 恶魔
{
    public static final int 恶魔之翼 = 30011109;
    public static final int 恶魔跳跃 = 30010110;
    public static final int 死亡诅咒 = 30010111;
    public static final int 恶魔之怒 = 30010112;
    public static final int 恶魔之翼1 = 30011159;
    public static final int 恶魔跳跃1 = 30010183;
    public static final int 恶魔跳跃2 = 30010184;
    public static final int 恶魔之血 = 30010185;
    public static final int 恶魔跳跃3 = 30010186;
    public static final int 黑暗敏捷 = 31000002;
    public static final int HP增加 = 31000003;
    public static final int 恶魔血月斩 = 31000004;
    public static final int 守护者之甲 = 31000005;
    public static final int 恶魔镰刀 = 31001000;
    public static final int 恶魔加速 = 31001001;
    public static final int 恶魔血月斩1 = 31001006;
    public static final int 恶魔血月斩2 = 31001007;
    public static final int 恶魔血月斩3 = 31001008;
    public static final int 精准武器 = 31100004;
    public static final int 物理训练 = 31100005;
    public static final int 愤怒 = 31100006;
    public static final int 恶魔血月斩1次强化 = 31100007;
    public static final int 灵魂吞噬 = 31101000;
    public static final int 黑暗猛攻 = 31101001;
    public static final int 恶魔追踪 = 31101002;
    public static final int 黑暗复仇 = 31101003;
    public static final int 邪恶拷问 = 31110006;
    public static final int 精神集中 = 31110007;
    public static final int 精气防护 = 31110008;
    public static final int 极限精气吸收 = 31110009;
    public static final int 恶魔血月斩2次强化 = 31110010;
    public static final int 黑暗审判 = 31111000;
    public static final int 死亡牵引 = 31111001;
    public static final int 血腥渡鸦 = 31111003;
    public static final int 恶魔呼吸 = 31111005;
    public static final int 黑暗忍耐 = 31111004;
    public static final int 进阶精准武器 = 31120008;
    public static final int 皮肤硬化 = 31120009;
    public static final int 恶魔血月斩最终强化 = 31120011;
    public static final int 恶魔爆炸 = 31121000;
    public static final int 恶魔冲击波 = 31121001;
    public static final int 吸血鬼之触 = 31121002;
    public static final int 鬼泣 = 31121003;
    public static final int 冒险岛勇士 = 31121004;
    public static final int 黑暗束缚 = 31121006;
    public static final int 黑暗变形 = 31121005;
    public static final int 无限精气 = 31121007;
    public static final int 恶魔爆炸1 = 31121010;
    public static final int 神圣之力 = 31120030;
    public static final int 神圣敏捷 = 31120031;
    public static final int 神圣智力 = 31120032;
    public static final int 神圣幸运 = 31120033;
    public static final int 神圣爆击 = 31120034;
    public static final int 神圣精准 = 31120035;
    public static final int 神圣最大生命 = 31120036;
    public static final int 神圣最大魔法 = 31120037;
    public static final int 神圣最大恶魔精气 = 31120038;
    public static final int 神圣物理防御 = 31120039;
    public static final int 神圣魔法防御 = 31120040;
    public static final int 神圣移动 = 31120041;
    public static final int 神圣跳跃 = 31120042;
    public static final int 恶魔呼吸_强化 = 31120043;
    public static final int 恶魔呼吸_额外攻击 = 31120044;
    public static final int 恶魔呼吸_减少精气 = 31120045;
    public static final int 黑暗变形_增强 = 31120046;
    public static final int 黑暗变形_强化 = 31120047;
    public static final int 黑暗变形_减少精气 = 31120048;
    public static final int 恶魔冲击波_强化 = 31120049;
    public static final int 恶魔冲击波_额外攻击 = 31120050;
    public static final int 恶魔冲击波_减少精气 = 31120051;
    public static final int 地狱夺魂 = 31121052;
    public static final int 自由之墙 = 31121053;
    public static final int 蓝血 = 31121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\恶魔.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */