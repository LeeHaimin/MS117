package constants.skills;

public class 弓手
{
    public static final int 精准弓 = 3100000;
    public static final int 终极弓 = 3100001;
    public static final int 物理训练 = 3100006;
    public static final int 三彩箭矢_绿 = 3100010;
    public static final int 快速箭 = 3101002;
    public static final int 无形箭 = 3101004;
    public static final int 爆炸箭 = 3101005;
    public static final int 后撤跳疾射 = 3101008;
    public static final int 三彩箭矢 = 3101009;
    public static final int 贯穿箭 = 3110001;
    public static final int 闪避 = 3110007;
    public static final int 精神集中 = 3110012;
    public static final int 射术精修 = 3110014;
    public static final int 烈火箭 = 3111003;
    public static final int 火凤凰 = 3111005;
    public static final int 暴风箭雨_三转 = 3111009;
    public static final int 寒冰爪钩 = 3111010;
    public static final int 极限射箭 = 3111011;
    public static final int 神箭手 = 3120005;
    public static final int 进阶终极攻击 = 3120008;
    public static final int 进阶箭筒_吸血 = 3120017;
    public static final int 防甲穿透 = 3120018;
    public static final int 暴风箭雨_四转 = 3120019;
    public static final int 冒险岛勇士 = 3121000;
    public static final int 火眼晶晶 = 3121002;
    public static final int 幻影步 = 3121007;
    public static final int 勇士的意志 = 3121009;
    public static final int 箭矢炮盘 = 3121013;
    public static final int 创伤之射 = 3121014;
    public static final int 骤雨箭矢 = 3121015;
    public static final int 进阶箭筒 = 3121016;
    public static final int 神圣之力 = 3120030;
    public static final int 神圣敏捷 = 3120031;
    public static final int 神圣智力 = 3120032;
    public static final int 神圣幸运 = 3120033;
    public static final int 神圣爆击 = 3120034;
    public static final int 神圣精准 = 3120035;
    public static final int 神圣最大生命 = 3120036;
    public static final int 神圣最大魔法 = 3120037;
    public static final int 神圣最大恶魔精气 = 3120038;
    public static final int 神圣物理防御 = 3120039;
    public static final int 神圣魔法防御 = 3120040;
    public static final int 神圣移动 = 3120041;
    public static final int 神圣跳跃 = 3120042;
    public static final int 火眼晶晶_坚持 = 3120043;
    public static final int 火眼晶晶_无视防御 = 3120044;
    public static final int 火眼晶晶_神圣暴击 = 3120045;
    public static final int 骤雨箭矢_强化 = 3120046;
    public static final int 骤雨箭矢_额外目标 = 3120047;
    public static final int 骤雨箭矢_额外攻击 = 3120048;
    public static final int 箭矢炮盘_强化 = 3120049;
    public static final int 箭矢炮盘_BOSS杀手 = 3120050;
    public static final int 箭矢炮盘_额外目标 = 3120051;
    public static final int 掠夺之风 = 3121052;
    public static final int 传说冒险家 = 3121053;
    public static final int 战斗准备 = 3121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\弓手.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */