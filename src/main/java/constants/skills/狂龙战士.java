package constants.skills;

public class 狂龙战士
{
    public static final int 防御模式 = 60001216;
    public static final int 攻击模式 = 60001217;
    public static final int 垂直连接 = 60001218;
    public static final int 钢铁之墙 = 60000222;
    public static final int 皮肤保护 = 61000003;
    public static final int 飞龙斩 = 61001000;
    public static final int 双重跳跃 = 61001002;
    public static final int 飞龙斩_1 = 61001004;
    public static final int 飞龙斩_2 = 61001005;
    public static final int 烈火箭 = 61001101;
    public static final int 防御模式1次强化 = 61100005;
    public static final int 精准剑 = 61100006;
    public static final int 内心火焰 = 61100007;
    public static final int 攻击模式1次强化 = 61100008;
    public static final int 飞龙斩1次强化 = 61100009;
    public static final int 剑刃之壁 = 61101002;
    public static final int 熊熊烈火升级 = 61101004;
    public static final int 冲击波 = 61101100;
    public static final int 穿刺冲击 = 61101101;
    public static final int 防御模式2次强化 = 61110005;
    public static final int 自我恢复 = 61110006;
    public static final int 进阶内心火焰 = 61110007;
    public static final int 重拾力量_额外攻击 = 61110009;
    public static final int 攻击模式2次强化 = 61110010;
    public static final int 飞龙斩2次强化 = 61110015;
    public static final int 剑刃之壁_变身 = 61110211;
    public static final int 飞龙斩_变身_3转 = 61110212;
    public static final int 石化 = 61111002;
    public static final int 重拾力量 = 61111003;
    public static final int 催化 = 61111004;
    public static final int 终极变形_3转 = 61111008;
    public static final int 扇击 = 61111100;
    public static final int 牵引锁链 = 61111101;
    public static final int 扇击_变身 = 61111111;
    public static final int 扇击_1 = 61111113;
    public static final int 变身_传送 = 61111114;
    public static final int 进阶剑刃之壁 = 61120007;
    public static final int 终极变形_4转 = 61120008;
    public static final int 防御模式3次强化 = 61120010;
    public static final int 无敌之勇 = 61120011;
    public static final int 进阶精准剑 = 61120012;
    public static final int 攻击模式3次强化 = 61120013;
    public static final int 恶魔之息_暴怒 = 61120018;
    public static final int 飞龙斩3次强化 = 61120020;
    public static final int 飞龙斩_变身_4转 = 61120219;
    public static final int 强健护甲 = 61121009;
    public static final int 诺巴的勇士 = 61121014;
    public static final int 诺巴勇士的意志 = 61121015;
    public static final int 怒雷屠龙斩 = 61121100;
    public static final int 天空剑影 = 61121102;
    public static final int 剑气突袭 = 61121104;
    public static final int 恶魔之息 = 61121105;
    public static final int 剑气突袭_爆发 = 61121116;
    public static final int 怒雷屠龙斩_变身 = 61121201;
    public static final int 天空剑影_变身 = 61121203;
    public static final int 进阶剑刃之壁_变身 = 61121217;
    public static final int 神圣之力 = 61120030;
    public static final int 神圣敏捷 = 61120031;
    public static final int 神圣智力 = 61120032;
    public static final int 神圣幸运 = 61120033;
    public static final int 神圣爆击 = 61120034;
    public static final int 神圣精准 = 61120035;
    public static final int 神圣最大生命 = 61120036;
    public static final int 神圣最大魔法 = 61120037;
    public static final int 神圣最大恶魔精气 = 61120038;
    public static final int 神圣物理防御 = 61120039;
    public static final int 神圣魔法防御 = 61120040;
    public static final int 神圣移动 = 61120041;
    public static final int 神圣跳跃 = 61120042;
    public static final int 怒雷屠龙斩_强化 = 61120043;
    public static final int 怒雷屠龙斩_坚持 = 61120044;
    public static final int 怒雷屠龙斩_额外攻击 = 61120045;
    public static final int 恶魔之息_强化 = 61120046;
    public static final int 恶魔之息_暴怒坚持 = 61120047;
    public static final int 恶魔之息_暴怒强化 = 61120048;
    public static final int 扇击_强化 = 61120049;
    public static final int 扇击_坚持 = 61120050;
    public static final int 扇击_额外攻击 = 61120051;
    public static final int 日珥 = 61121052;
    public static final int 终极变形_超级 = 61121053;
    public static final int 狂龙战士的威严 = 61121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\狂龙战士.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */