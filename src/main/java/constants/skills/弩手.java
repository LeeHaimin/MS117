package constants.skills;

public class 弩手
{
    public static final int 精准弩 = 3200000;
    public static final int 终极弩 = 3200001;
    public static final int 物理训练 = 3200006;
    public static final int 远近效应 = 3200009;
    public static final int 快速弩 = 3201002;
    public static final int 无形箭 = 3201004;
    public static final int 穿透箭_二转 = 3201005;
    public static final int 星云撒网 = 3201008;
    public static final int 贯穿箭 = 3210001;
    public static final int 闪避 = 3210007;
    public static final int 伤害置换 = 3210013;
    public static final int 射术精修 = 3210015;
    public static final int 冰凤凰 = 3211005;
    public static final int 飞龙冲击波 = 3211008;
    public static final int 破裂雷电 = 3211009;
    public static final int 寒冰爪钩 = 3211010;
    public static final int 治愈长杖 = 3211011;
    public static final int 极限射箭 = 3211012;
    public static final int 神弩手 = 3220004;
    public static final int 天赐神箭 = 3220015;
    public static final int 剑指一人 = 3220016;
    public static final int 弱点捕捉 = 3220018;
    public static final int 冒险岛勇士 = 3221000;
    public static final int 火眼晶晶 = 3221002;
    public static final int 幻影步 = 3221006;
    public static final int 一击要害箭 = 3221007;
    public static final int 勇士的意志 = 3221008;
    public static final int 神箭幻影 = 3221014;
    public static final int 穿透箭_四转 = 3221017;
    public static final int 神圣之力 = 3220030;
    public static final int 神圣敏捷 = 3220031;
    public static final int 神圣智力 = 3220032;
    public static final int 神圣幸运 = 3220033;
    public static final int 神圣爆击 = 3220034;
    public static final int 神圣精准 = 3220035;
    public static final int 神圣最大生命 = 3220036;
    public static final int 神圣最大魔法 = 3220037;
    public static final int 神圣最大恶魔精气 = 3220038;
    public static final int 神圣物理防御 = 3220039;
    public static final int 神圣魔法防御 = 3220040;
    public static final int 神圣移动 = 3220041;
    public static final int 神圣跳跃 = 3220042;
    public static final int 火眼晶晶_坚持 = 3220043;
    public static final int 火眼晶晶_无视防御 = 3220044;
    public static final int 火眼晶晶_神圣暴击 = 3220045;
    public static final int 穿透箭_强化 = 3220046;
    public static final int 穿透箭_额外目标 = 3220047;
    public static final int 穿透箭_额外攻击 = 3220048;
    public static final int 一击要害箭_强化 = 3220049;
    public static final int 一击要害箭_最大值提高 = 3220050;
    public static final int 一击要害箭_缩短冷却时间 = 3220051;
    public static final int 远程强击 = 3221052;
    public static final int 传说冒险家 = 3221053;
    public static final int 鹰眼 = 3221054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\弩手.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */