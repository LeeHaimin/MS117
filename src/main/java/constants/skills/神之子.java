package constants.skills;

public class 神之子
{
    public static final int 重归神殿 = 100001262;
    public static final int 双重打击 = 100000282;
    public static final int 圣洁之力 = 100001263;
    public static final int 神圣迅捷 = 100001264;
    public static final int 暴风一跃 = 100001265;
    public static final int 暴风步 = 100001266;
    public static final int 暴风闪 = 100001269;
    public static final int 伦娜之庇护 = 100001268;
    public static final int 狂蛮撞击 = 101000100;
    public static final int 进阶狂蛮撞击 = 101000101;
    public static final int 进阶狂蛮撞击_冲击波 = 101000102;
    public static final int 精准大剑 = 101000103;
    public static final int 穿透之波 = 101000200;
    public static final int 影子突击 = 101000201;
    public static final int 影子突击_剑气 = 101000202;
    public static final int 精准太刀 = 101000203;
    public static final int 上旋斩 = 101001100;
    public static final int 月袭斩 = 101001200;
    public static final int 武器投掷 = 101100100;
    public static final int 进阶神剑陨落 = 101100101;
    public static final int 固态身体 = 101100102;
    public static final int 回旋切割者 = 101100200;
    public static final int 进阶旋卷切割 = 101100201;
    public static final int 进阶旋卷切割_剑气 = 101100202;
    public static final int 强化之躯 = 101100203;
    public static final int 前方挥斩 = 101101100;
    public static final int 闪光斩 = 101101200;
    public static final int 圆月旋风 = 101110101;
    public static final int 进阶圆月旋风 = 101110102;
    public static final int 防甲瓦解 = 101110103;
    public static final int 进阶圆月旋风_吸收 = 101110104;
    public static final int 进阶狂转回旋 = 101110200;
    public static final int 进阶狂转回旋_剑气 = 101110201;
    public static final int 旋跃斩 = 101110202;
    public static final int 进阶旋跃斩 = 101110203;
    public static final int 进阶旋跃斩_剑气 = 101110204;
    public static final int 战斗恢复 = 101110205;
    public static final int 回旋重击 = 101111100;
    public static final int 狂转回旋 = 101111200;
    public static final int 跳跃坠袭 = 101120100;
    public static final int 跳跃坠袭_冲击波 = 101120101;
    public static final int 地裂山崩 = 101120102;
    public static final int 地裂山崩_冲击波 = 101120103;
    public static final int 进阶地裂山崩 = 101120104;
    public static final int 进阶地裂山崩_冲击波 = 101120105;
    public static final int 进阶地裂山崩_雷电区域 = 101120106;
    public static final int 防御之盾 = 101120109;
    public static final int 暴击束缚 = 101120110;
    public static final int 极速切割_漩涡 = 101120200;
    public static final int 极速突击 = 101120201;
    public static final int 暴风制动 = 101120202;
    public static final int 暴风制动_旋风 = 101120203;
    public static final int 进阶暴风旋涡 = 101120204;
    public static final int 进阶暴风旋涡_旋涡 = 101120205;
    public static final int 进阶暴风旋涡_雷电区域 = 101120206;
    public static final int 圣光照耀 = 101120207;
    public static final int 巨能崩袭 = 101121100;
    public static final int 极速切割 = 101121200;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\神之子.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */