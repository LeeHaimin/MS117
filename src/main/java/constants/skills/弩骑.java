package constants.skills;

public class 弩骑
{
    public static final int 自然吸收 = 33000004;
    public static final int 快速弩_1转 = 33001003;
    public static final int 美洲豹骑士 = 33001001;
    public static final int 自动射击装置 = 33000005;
    public static final int 连环三箭 = 33001000;
    public static final int 三段跳 = 33001002;
    public static final int 精准弩 = 33100000;
    public static final int 物理训练 = 33100010;
    public static final int 终极弓弩 = 33100009;
    public static final int 快速弩 = 33101012;
    public static final int 炸裂箭 = 33101001;
    public static final int 美洲豹怒吼 = 33101002;
    public static final int 无形箭弩 = 33101003;
    public static final int 呼啸 = 33101005;
    public static final int 地雷 = 33101004;
    public static final int 地雷_自曝 = 33101008;
    public static final int 银鹰召唤 = 33101011;
    public static final int 骑宠精通 = 33110000;
    public static final int 十字攻击 = 33111002;
    public static final int 暴走形态 = 33111007;
    public static final int 疾风 = 33110008;
    public static final int 狂野射击 = 33111001;
    public static final int 白炽冲撞 = 33111010;
    public static final int 利爪狂风 = 33111006;
    public static final int 撤步退身 = 33111011;
    public static final int 野性陷阱 = 33111003;
    public static final int 致盲 = 33111004;
    public static final int 音速震波_咆哮 = 33121001;
    public static final int 音速震波 = 33121002;
    public static final int 神弩手 = 33120000;
    public static final int 火眼晶晶 = 33121004;
    public static final int 冒险岛勇士 = 33121007;
    public static final int 勇士的意志 = 33121008;
    public static final int 野性本能 = 33120010;
    public static final int 进阶终极攻击 = 33120011;
    public static final int 辅助打猎单元 = 33121012;
    public static final int 弹仓扩展 = 33121013;
    public static final int 奥义箭乱舞 = 33121009;
    public static final int 神经毒气 = 33121005;
    public static final int 神圣之力 = 33120030;
    public static final int 神圣敏捷 = 33120031;
    public static final int 神圣智力 = 33120032;
    public static final int 神圣幸运 = 33120033;
    public static final int 神圣爆击 = 33120034;
    public static final int 神圣精准 = 33120035;
    public static final int 神圣最大生命 = 33120036;
    public static final int 神圣最大魔法 = 33120037;
    public static final int 神圣最大恶魔精气 = 33120038;
    public static final int 神圣物理防御 = 33120039;
    public static final int 神圣魔法防御 = 33120040;
    public static final int 神圣移动 = 33120041;
    public static final int 神圣跳跃 = 33120042;
    public static final int 暴走形态_强化 = 33120043;
    public static final int 暴走形态_额外体力点数 = 33120044;
    public static final int 暴走形态_快速攻击 = 33120045;
    public static final int 音速震波_强化 = 33120046;
    public static final int 音速震波_额外目标 = 33120047;
    public static final int 音速震波_额外攻击 = 33120048;
    public static final int 奥义箭乱舞_强化 = 33120049;
    public static final int 奥义箭乱舞_无视防御 = 33120050;
    public static final int 奥义箭乱舞_BOSS杀手 = 33120051;
    public static final int 狂暴合一 = 33121052;
    public static final int 自由之墙 = 33121053;
    public static final int 沉默之怒 = 33121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\弩骑.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */