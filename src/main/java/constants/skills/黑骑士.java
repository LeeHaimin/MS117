package constants.skills;

public class 黑骑士
{
    public static final int 精准武器 = 1300000;
    public static final int 终极枪矛 = 1300002;
    public static final int 物理训练 = 1300009;
    public static final int 快速武器 = 1301004;
    public static final int 极限防御 = 1301006;
    public static final int 神圣之火 = 1301007;
    public static final int 贯穿刺透 = 1301011;
    public static final int 神矛天引 = 1301012;
    public static final int 灵魂助力 = 1301013;
    public static final int 黑暗至尊 = 1310009;
    public static final int 抵抗力 = 1310010;
    public static final int 灵魂祝福 = 1310016;
    public static final int 拉曼查之枪 = 1311011;
    public static final int 突进 = 1311012;
    public static final int 灵魂助力统治 = 1311013;
    public static final int 灵魂助力震惊 = 1311014;
    public static final int 交叉锁链 = 1311015;
    public static final int 灵魂复仇 = 1320011;
    public static final int 重生契约 = 1320016;
    public static final int 稳如泰山 = 1320017;
    public static final int 进阶精准武器 = 1320018;
    public static final int 重生契约_状态 = 1320019;
    public static final int 冒险岛勇士 = 1321000;
    public static final int 勇士的意志 = 1321010;
    public static final int 黑暗穿刺 = 1321012;
    public static final int 神枪降临 = 1321013;
    public static final int 魔击无效 = 1321014;
    public static final int 龙之献祭 = 1321015;
    public static final int 神圣之力 = 1320030;
    public static final int 神圣敏捷 = 1320031;
    public static final int 神圣智力 = 1320032;
    public static final int 神圣幸运 = 1320033;
    public static final int 神圣爆击 = 1320034;
    public static final int 神圣精准 = 1320035;
    public static final int 神圣最大生命 = 1320036;
    public static final int 神圣最大魔法 = 1320037;
    public static final int 神圣最大恶魔精气 = 1320038;
    public static final int 神圣物理防御 = 1320039;
    public static final int 神圣魔法防御 = 1320040;
    public static final int 神圣移动 = 1320041;
    public static final int 神圣跳跃 = 1320042;
    public static final int 神圣之火_坚持 = 1320043;
    public static final int 神圣之火_额外魔法点数 = 1320044;
    public static final int 神圣之火_额外体力点数 = 1320045;
    public static final int 黑暗力量_强化 = 1320046;
    public static final int 黑暗力量_爆击伤害 = 1320047;
    public static final int 黑暗力量_爆击率 = 1320048;
    public static final int 黑暗穿刺_强化 = 1320049;
    public static final int 黑暗穿刺_无视防御 = 1320050;
    public static final int 黑暗穿刺_额外攻击 = 1320051;
    public static final int 黑暗融合 = 1321052;
    public static final int 传说冒险家 = 1321053;
    public static final int 黑暗饥渴 = 1321054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\黑骑士.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */