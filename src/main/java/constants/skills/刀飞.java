package constants.skills;

public class 刀飞
{
    public static final int 精准短刀 = 4200000;
    public static final int 物理训练 = 4200007;
    public static final int 盾防精通 = 4200010;
    public static final int 暴击蓄能 = 4200013;
    public static final int 快速短刀 = 4201002;
    public static final int 神通术 = 4201004;
    public static final int 命运 = 4201009;
    public static final int 金钱护盾 = 4201011;
    public static final int 回旋斩 = 4201012;
    public static final int 武器用毒液 = 4210010;
    public static final int 贪婪 = 4210012;
    public static final int 永恒黑暗 = 4210013;
    public static final int 金钱炸弹_1 = 4210014;
    public static final int 炼狱 = 4211002;
    public static final int 敛财术 = 4211003;
    public static final int 金钱炸弹 = 4211006;
    public static final int 黑暗杂耍 = 4211007;
    public static final int 影分身 = 4211008;
    public static final int 刀刃之舞 = 4211011;
    public static final int 假动作 = 4220002;
    public static final int 致命毒液 = 4220011;
    public static final int 短刀专家 = 4220012;
    public static final int 名流爆击 = 4220015;
    public static final int 冒险岛勇士 = 4221000;
    public static final int 烟幕弹 = 4221006;
    public static final int 一出双击 = 4221007;
    public static final int 勇士的意志 = 4221008;
    public static final int 突然袭击 = 4221010;
    public static final int 侠盗本能 = 4221013;
    public static final int 暗杀 = 4221014;
    public static final int 暗杀_1 = 4221016;
    public static final int 神圣之力 = 4220030;
    public static final int 神圣敏捷 = 4220031;
    public static final int 神圣智力 = 4220032;
    public static final int 神圣幸运 = 4220033;
    public static final int 神圣爆击 = 4220034;
    public static final int 神圣精准 = 4220035;
    public static final int 神圣最大生命 = 4220036;
    public static final int 神圣最大魔法 = 4220037;
    public static final int 神圣最大恶魔精气 = 4220038;
    public static final int 神圣物理防御 = 4220039;
    public static final int 神圣魔法防御 = 4220040;
    public static final int 神圣移动 = 4220041;
    public static final int 神圣跳跃 = 4220042;
    public static final int 金钱炸弹_强化 = 4220043;
    public static final int 金钱炸弹_额外目标 = 4220044;
    public static final int 金钱炸弹_增强 = 4220045;
    public static final int 一出双击_强化 = 4220046;
    public static final int 一出双击_额外目标 = 4220047;
    public static final int 一出双击_额外攻击 = 4220048;
    public static final int 暗杀_强化 = 4220049;
    public static final int 暗杀_额外攻击 = 4220050;
    public static final int 暗杀_最大值提高 = 4220051;
    public static final int 潜影杀机 = 4221052;
    public static final int 传说冒险家 = 4221053;
    public static final int 幸运钱 = 4221054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\刀飞.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */