package constants.skills;

public class 爆莉萌天使
{
    public static final int 释世书 = 60011216;
    public static final int 真释世书 = 60010217;
    public static final int 魔法裂隙 = 60011218;
    public static final int 灵魂契约 = 60011219;
    public static final int 白日梦 = 60011220;
    public static final int 协调 = 60011221;
    public static final int 装扮 = 60011222;
    public static final int 亲和Ⅰ = 65000003;
    public static final int 魔法转移 = 65001001;
    public static final int 火眼金睛 = 65001002;
    public static final int 泡沫之星 = 65001100;
    public static final int 精准灵魂手铳 = 65100003;
    public static final int 内心之火 = 65100004;
    public static final int 亲和Ⅱ = 65100005;
    public static final int 粉粉旋风 = 65101001;
    public static final int 力量转移 = 65101002;
    public static final int 穿刺爆炸_爆炸 = 65101006;
    public static final int 穿刺爆炸 = 65101100;
    public static final int 内心平和 = 65110005;
    public static final int 亲和Ⅲ = 65110006;
    public static final int 力量召唤 = 65111002;
    public static final int 远古召唤 = 65111003;
    public static final int 钢铁莲花 = 65111004;
    public static final int 灵魂吸取 = 65111007;
    public static final int 灵魂吸取_1 = 65111100;
    public static final int 坠落群星 = 65111101;
    public static final int 灵魂手铳手 = 65120005;
    public static final int 亲和Ⅳ = 65120006;
    public static final int 大地冲击波 = 65121002;
    public static final int 灵魂共鸣 = 65121003;
    public static final int 灵魂凝视 = 65121004;
    public static final int 三位一体_2击 = 65121007;
    public static final int 三位一体_3击 = 65121008;
    public static final int 诺巴的勇士 = 65121009;
    public static final int 诺巴勇士的意志 = 65121010;
    public static final int 原始之吼 = 65121100;
    public static final int 三位一体 = 65121101;
    public static final int 神圣之力 = 65120030;
    public static final int 神圣敏捷 = 65120031;
    public static final int 神圣智力 = 65120032;
    public static final int 神圣幸运 = 65120033;
    public static final int 神圣爆击 = 65120034;
    public static final int 神圣精准 = 65120035;
    public static final int 神圣最大生命 = 65120036;
    public static final int 神圣最大魔法 = 65120037;
    public static final int 神圣最大恶魔精气 = 65120038;
    public static final int 神圣物理防御 = 65120039;
    public static final int 神圣魔法防御 = 65120040;
    public static final int 神圣移动 = 65120041;
    public static final int 神圣跳跃 = 65120042;
    public static final int 灵魂吸取_强化 = 65120043;
    public static final int 灵魂吸取_装扮 = 65120044;
    public static final int 灵魂吸取_重生 = 65120045;
    public static final int 大地冲击波_强化 = 65120046;
    public static final int 大地冲击波_最大强化 = 65120047;
    public static final int 大地冲击波_缩短冷却时间 = 65120048;
    public static final int 灵魂共鸣_强化 = 65120049;
    public static final int 灵魂共鸣_缩短冷却时间 = 65120050;
    public static final int 灵魂共鸣_坚持 = 65120051;
    public static final int 超级诺巴 = 65121052;
    public static final int 终极契约 = 65121053;
    public static final int 灵魂鼓舞 = 65121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\爆莉萌天使.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */