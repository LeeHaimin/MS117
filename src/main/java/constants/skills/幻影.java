package constants.skills;

public class 幻影
{
    public static final int 幻影回归 = 20031203;
    public static final int 致命本能 = 20030204;
    public static final int 幻影屏障 = 20031205;
    public static final int 灵敏身手 = 20030206;
    public static final int 印技之瞳 = 20031207;
    public static final int 印技树 = 20031208;
    public static final int 卡牌审判 = 20031209;
    public static final int 卡牌审判_高级 = 20031210;
    public static final int 快速逃避 = 24000003;
    public static final int 二连刺 = 24001000;
    public static final int 幻影印技天赋Ⅰ = 24001001;
    public static final int 迅捷幻影 = 24001002;
    public static final int 卡片雪舞 = 24100003;
    public static final int 精准手杖 = 24100004;
    public static final int 超级幸运星 = 24100006;
    public static final int 命运召唤 = 24101000;
    public static final int 幻影印技天赋Ⅱ = 24101001;
    public static final int 和风卡浪 = 24101002;
    public static final int 快速手杖 = 24101005;
    public static final int 电光回避 = 24110004;
    public static final int 敏锐直觉 = 24110007;
    public static final int 卡牌斩碎 = 24111000;
    public static final int 幻影印技天赋Ⅲ = 24111001;
    public static final int 神秘的运气 = 24111002;
    public static final int 幸运保护 = 24111003;
    public static final int 月光祝福 = 24111005;
    public static final int 幻影突击 = 24111006;
    public static final int 幻影突击1 = 24111008;
    public static final int 黑色秘卡 = 24120002;
    public static final int 手杖专家 = 24120006;
    public static final int 蓝光连击 = 24121000;
    public static final int 幻影印技天赋Ⅳ = 24121001;
    public static final int 暮光祝福 = 24121003;
    public static final int 圣歌祈祷 = 24121004;
    public static final int 卡片风暴 = 24121005;
    public static final int 灵魂偷取 = 24121007;
    public static final int 冒险岛勇士 = 24121008;
    public static final int 勇士的意志 = 24121009;
    public static final int 暮光祝福1 = 24121010;
    public static final int 神圣之力 = 24120030;
    public static final int 神圣敏捷 = 24120031;
    public static final int 神圣智力 = 24120032;
    public static final int 神圣幸运 = 24120033;
    public static final int 神圣爆击 = 24120034;
    public static final int 神圣精准 = 24120035;
    public static final int 神圣最大生命 = 24120036;
    public static final int 神圣最大魔法 = 24120037;
    public static final int 神圣最大恶魔精气 = 24120038;
    public static final int 神圣物理防御 = 24120039;
    public static final int 神圣魔法防御 = 24120040;
    public static final int 神圣移动 = 24120041;
    public static final int 神圣跳跃 = 24120042;
    public static final int 卡片风暴_强化 = 24120043;
    public static final int 卡片风暴_缩短冷却时间 = 24120044;
    public static final int 卡片风暴_额外目标 = 24120045;
    public static final int 蓝光连击_强化 = 24120046;
    public static final int 蓝光连击_额外目标 = 24120047;
    public static final int 蓝光连击_最快移动 = 24120048;
    public static final int 幸运保护_抗性提升 = 24120049;
    public static final int 幸运保护_最大生命值 = 24120050;
    public static final int 幸运保护_最大魔力值 = 24120051;
    public static final int 玫瑰卡片终结_1 = 24120055;
    public static final int 玫瑰卡片终结 = 24121052;
    public static final int 英雄奥斯 = 24121053;
    public static final int 最终审判 = 24121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\幻影.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */