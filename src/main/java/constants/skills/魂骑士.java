package constants.skills;

public class 魂骑士
{
    public static final int 三连斩 = 11001020;
    public static final int 灵魂鸣响 = 11000023;
    public static final int 光之刃 = 11001021;
    public static final int 元素_灵魂 = 11001022;
    public static final int 精准剑_新 = 11100025;
    public static final int 人神一体 = 11100026;
    public static final int 月光洒落 = 11101022;
    public static final int 坚定意志 = 11101023;
    public static final int 机警灵活 = 11101024;
    public static final int 瞬步 = 11101120;
    public static final int 摄魂斩 = 11101121;
    public static final int 猛袭 = 11101220;
    public static final int 灼影之焰 = 11101221;
    public static final int 钢铁之轮 = 11110025;
    public static final int 心灵呐喊 = 11110026;
    public static final int 旭日 = 11111022;
    public static final int 灵魂之眼 = 11111023;
    public static final int 灵魂守卫 = 11111024;
    public static final int 月影斩 = 11111120;
    public static final int 月光十字斩 = 11111121;
    public static final int 光速突击 = 11111220;
    public static final int 日光十字斩 = 11111221;
    public static final int 长剑专家 = 11120007;
    public static final int 幻千之刃 = 11120008;
    public static final int 人剑合一 = 11120009;
    public static final int 人剑合一_旭日 = 11120010;
    public static final int 希纳斯的骑士 = 11121000;
    public static final int 落魂剑 = 11121004;
    public static final int 日月轮转 = 11121005;
    public static final int 灵魂誓约 = 11121006;
    public static final int 日月轮转_月光洒落 = 11121011;
    public static final int 日月轮转_旭日 = 11121012;
    public static final int 落魂剑_傀儡 = 11121013;
    public static final int 月光之舞 = 11121101;
    public static final int 月光之舞_空中 = 11121102;
    public static final int 新月斩 = 11121103;
    public static final int 极速霞光 = 11121201;
    public static final int 极速霞光_空中 = 11121202;
    public static final int 烈日之刺 = 11121203;
    public static final int 神圣之力 = 11120030;
    public static final int 神圣敏捷 = 11120031;
    public static final int 神圣智力 = 11120032;
    public static final int 神圣幸运 = 11120033;
    public static final int 神圣爆击 = 11120034;
    public static final int 神圣精准 = 11120035;
    public static final int 神圣最大生命 = 11120036;
    public static final int 神圣最大魔法 = 11120037;
    public static final int 神圣最大恶魔精气 = 11120038;
    public static final int 神圣物理防御 = 11120039;
    public static final int 神圣魔法防御 = 11120040;
    public static final int 神圣移动 = 11120041;
    public static final int 神圣跳跃 = 11120042;
    public static final int 灵魂之眼_坚持 = 11120043;
    public static final int 灵魂之眼_增强 = 11120044;
    public static final int 灵魂之眼_无视防御 = 11120045;
    public static final int 斩刺_强化 = 11120046;
    public static final int 斩刺_额外目标 = 11120047;
    public static final int 斩刺_额外攻击 = 11120048;
    public static final int 极速之舞_强化 = 11120049;
    public static final int 极速之舞_无视防御 = 11120050;
    public static final int 极速之舞_BOSS杀手 = 11120051;
    public static final int 冥河破 = 11121052;
    public static final int 守护者之荣誉 = 11121053;
    public static final int 灵魂之剑 = 11121054;
    public static final int 冥河破_爆破 = 11121055;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\魂骑士.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */