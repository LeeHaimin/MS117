package constants.skills;

public class 战神
{
    public static final int 蜗牛投掷术 = 20001000;
    public static final int 团队治疗 = 20001001;
    public static final int 疾风步 = 20001002;
    public static final int 匠人之魂 = 20001003;
    public static final int 英雄之回声 = 20001005;
    public static final int 向下跳跃 = 20001006;
    public static final int 锻造 = 20001007;
    public static final int 流星竹雨 = 20001009;
    public static final int 金刚霸体 = 20001010;
    public static final int 狂暴战魂 = 20001011;
    public static final int 绿水灵的弱点 = 20009002;
    public static final int 猪猪的弱点 = 20009000;
    public static final int 木妖的弱点 = 20009001;
    public static final int 群宠 = 20000024;
    public static final int 教程技能1 = 20000015;
    public static final int 教程技能2 = 20000014;
    public static final int 教程技能3 = 20000016;
    public static final int 教程技能4 = 20000017;
    public static final int 教程技能5 = 20000018;
    public static final int 矛连击强化 = 21000000;
    public static final int 双重重击 = 21000002;
    public static final int 斗气爆裂 = 21000004;
    public static final int 守护者之甲 = 21000005;
    public static final int 双重重击_未知 = 21000006;
    public static final int 战斗步伐 = 21001001;
    public static final int 快速矛 = 21001003;
    public static final int 精准矛 = 21100000;
    public static final int 三重重击 = 21100001;
    public static final int 战神突进_老技能 = 21100002;
    public static final int 幻影狼牙 = 21100007;
    public static final int 物理训练 = 21100008;
    public static final int 终极矛 = 21100010;
    public static final int 抗压 = 21101003;
    public static final int 连环吸血 = 21101005;
    public static final int 冰雪矛 = 21101006;
    public static final int 战神突进 = 21101011;
    public static final int 进阶矛连击强化 = 21110000;
    public static final int 全力挥击 = 21110002;
    public static final int 终极投掷_老技能 = 21110003;
    public static final int 旋风_老技能 = 21110006;
    public static final int 全力挥击_双重重击 = 21110007;
    public static final int 全力挥击_三重重击 = 21110008;
    public static final int 分裂攻击 = 21110010;
    public static final int 战神之审判 = 21110011;
    public static final int 全力挥击_未知效果 = 21110015;
    public static final int 威势 = 21111001;
    public static final int 斗气重生 = 21111009;
    public static final int 摩诃祝福 = 21111012;
    public static final int 终极投掷 = 21111013;
    public static final int 旋风 = 21111014;
    public static final int 攻击策略 = 21120001;
    public static final int 战神之舞 = 21120002;
    public static final int 防守策略 = 21120004;
    public static final int 巨熊咆哮_老技能 = 21120005;
    public static final int 钻石星辰 = 21120006;
    public static final int 战神之舞_双重重击 = 21120009;
    public static final int 战神之舞_三重重击 = 21120010;
    public static final int 迅捷移动 = 21120011;
    public static final int 进阶终极攻击 = 21120012;
    public static final int BOSS斗气重生 = 21120014;
    public static final int 战神之舞_未知效果 = 21120015;
    public static final int 冒险岛勇士 = 21121000;
    public static final int 战神之盾 = 21121007;
    public static final int 勇士的意志 = 21121008;
    public static final int 巨熊咆哮 = 21121013;
    public static final int 神圣之力 = 21120030;
    public static final int 神圣敏捷 = 21120031;
    public static final int 神圣智力 = 21120032;
    public static final int 神圣幸运 = 21120033;
    public static final int 神圣爆击 = 21120034;
    public static final int 神圣精准 = 21120035;
    public static final int 神圣最大生命 = 21120036;
    public static final int 神圣最大魔法 = 21120037;
    public static final int 神圣最大恶魔精气 = 21120038;
    public static final int 神圣物理防御 = 21120039;
    public static final int 神圣魔法防御 = 21120040;
    public static final int 神圣移动 = 21120041;
    public static final int 神圣跳跃 = 21120042;
    public static final int 斗气重生_丽丽兹 = 21120043;
    public static final int 斗气重生_缩短冷却时间 = 21120044;
    public static final int 斗气重生_额外连击 = 21120045;
    public static final int 巨熊咆哮_强化 = 21120046;
    public static final int 巨熊咆哮_额外攻击 = 21120047;
    public static final int 巨熊咆哮_无视防御 = 21120048;
    public static final int 钻石星辰_额外攻击 = 21120049;
    public static final int 钻石星辰_链接强化 = 21120050;
    public static final int 钻石星辰_丽丽兹 = 21120051;
    public static final int 比昂德 = 21120052;
    public static final int 英雄奥斯 = 21121053;
    public static final int 连击无限制 = 21121054;
    public static final int 比昂德_2击 = 21121055;
    public static final int 比昂德_3击 = 21121056;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\战神.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */