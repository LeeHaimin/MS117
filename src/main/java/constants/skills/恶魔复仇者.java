package constants.skills;

public class 恶魔复仇者
{
    public static final int 恶魔之翼 = 30011109;
    public static final int 恶魔跳跃 = 30010110;
    public static final int 恶魔之血 = 30010185;
    public static final int 野性狂怒 = 30010241;
    public static final int 血之契约 = 30010242;
    public static final int 超越 = 30010230;
    public static final int 高效吸收 = 30010231;
    public static final int 生命吸收 = 31010002;
    public static final int 恶魔之力 = 31010003;
    public static final int 超越十字斩 = 31011000;
    public static final int 负荷释放 = 31011001;
    public static final int 超越十字斩_1 = 31011004;
    public static final int 超越十字斩_2 = 31011005;
    public static final int 超越十字斩_3 = 31011006;
    public static final int 超越十字斩_4 = 31011007;
    public static final int 铜墙铁壁 = 31200004;
    public static final int 亡命剑精通 = 31200005;
    public static final int 心灵之力 = 31200006;
    public static final int 超越恶魔突袭 = 31201000;
    public static final int 暗影蝙蝠 = 31201001;
    public static final int 恶魔加速 = 31201002;
    public static final int 深渊之怒 = 31201003;
    public static final int 超越恶魔突袭_1 = 31201007;
    public static final int 超越恶魔突袭_2 = 31201008;
    public static final int 超越恶魔突袭_3 = 31201009;
    public static final int 超越恶魔突袭_4 = 31201010;
    public static final int 负荷缓解 = 31210005;
    public static final int 进阶生命吸收 = 31210006;
    public static final int 超越月光斩 = 31211000;
    public static final int 活力吞噬 = 31211001;
    public static final int 持盾突击 = 31211002;
    public static final int 驱邪 = 31211003;
    public static final int 恶魔恢复 = 31211004;
    public static final int 超越月光斩_1 = 31211007;
    public static final int 超越月光斩_2 = 31211008;
    public static final int 超越月光斩_3 = 31211009;
    public static final int 超越月光斩_4 = 31211010;
    public static final int 持盾突击_1 = 31211011;
    public static final int 防御专精 = 31220005;
    public static final int 进阶亡命剑精通 = 31220006;
    public static final int 强化超越 = 31220007;
    public static final int 超越处决 = 31221000;
    public static final int 追击盾 = 31221001;
    public static final int 破甲 = 31221002;
    public static final int 血腥禁锢 = 31221003;
    public static final int 惊天之力 = 31221004;
    public static final int 冒险岛勇士 = 31221008;
    public static final int 超越处决_1 = 31221009;
    public static final int 超越处决_2 = 31221010;
    public static final int 超越处决_3 = 31221011;
    public static final int 超越处决_4 = 31221012;
    public static final int 追击盾_1 = 31221014;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\恶魔复仇者.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */