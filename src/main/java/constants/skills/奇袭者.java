package constants.skills;

public class 奇袭者
{
    public static final int 光速 = 15000023;
    public static final int 闪电冲击 = 15001020;
    public static final int 疾电 = 15001021;
    public static final int 元素_闪电 = 15001022;
    public static final int 精准拳_新 = 15100023;
    public static final int 体力锻炼 = 15100024;
    public static final int 雷魄 = 15100025;
    public static final int 转轮踢 = 15101020;
    public static final int 波涛汹涌 = 15101021;
    public static final int 急速拳_新 = 15101022;
    public static final int 连锁达人 = 15110025;
    public static final int 雷帝 = 15110026;
    public static final int 冲天 = 15111020;
    public static final int 雷鸣 = 15111021;
    public static final int 疾风 = 15111022;
    public static final int 漩涡 = 15111023;
    public static final int 极限铠甲 = 15111024;
    public static final int 台风 = 15120003;
    public static final int 拳甲专家 = 15120006;
    public static final int 刺激 = 15120007;
    public static final int 雷神 = 15120008;
    public static final int 希纳斯的骑士 = 15121000;
    public static final int 毁灭 = 15121001;
    public static final int 霹雳 = 15121002;
    public static final int 聚雷 = 15121004;
    public static final int 极速领域_新 = 15121005;
    public static final int 神圣之力 = 15120030;
    public static final int 神圣敏捷 = 15120031;
    public static final int 神圣智力 = 15120032;
    public static final int 神圣幸运 = 15120033;
    public static final int 神圣爆击 = 15120034;
    public static final int 神圣精准 = 15120035;
    public static final int 神圣最大生命 = 15120036;
    public static final int 神圣最大魔法 = 15120037;
    public static final int 神圣最大恶魔精气 = 15120038;
    public static final int 神圣物理防御 = 15120039;
    public static final int 神圣魔法防御 = 15120040;
    public static final int 神圣移动 = 15120041;
    public static final int 神圣跳跃 = 15120042;
    public static final int 疾风_强化 = 15120043;
    public static final int 疾风_额外目标 = 15120044;
    public static final int 疾风_额外攻击 = 15120045;
    public static final int 霹雳_强化 = 15120046;
    public static final int 霹雳_额外目标 = 15120047;
    public static final int 霹雳_额外攻击 = 15120048;
    public static final int 毁灭_强化 = 15120049;
    public static final int 毁灭_无视防御 = 15120050;
    public static final int 毁灭_BOSS杀手 = 15120051;
    public static final int 海神降临 = 15121052;
    public static final int 守护者之荣誉 = 15121053;
    public static final int 开天辟地 = 15121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\奇袭者.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */