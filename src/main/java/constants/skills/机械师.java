package constants.skills;

public class 机械师
{
    public static final int 幸运一击 = 35000005;
    public static final int 火焰喷射器 = 35001001;
    public static final int 金属机甲_原型 = 35001002;
    public static final int 机枪扫射 = 35001004;
    public static final int 钻臂冲击 = 35001003;
    public static final int 机械精通 = 35100000;
    public static final int 精准重武器 = 35100008;
    public static final int 物理训练 = 35100011;
    public static final int 原子锤 = 35101003;
    public static final int 火箭推进器 = 35101004;
    public static final int 传送门_GX9 = 35101005;
    public static final int 机械加速 = 35101006;
    public static final int 完美机甲 = 35101007;
    public static final int 强化火焰喷射器 = 35101009;
    public static final int 强化机枪扫射 = 35101010;
    public static final int 精准机械拳 = 35110014;
    public static final int 人造卫星 = 35111001;
    public static final int 磁场 = 35111002;
    public static final int 金属机甲_重机枪 = 35111004;
    public static final int 加速器_EX7 = 35111005;
    public static final int 人造卫星1 = 35111009;
    public static final int 人造卫星2 = 35111010;
    public static final int 治疗机器人_HLX = 35111011;
    public static final int 幸运骰子 = 35111013;
    public static final int 火箭拳 = 35111015;
    public static final int 状态调试 = 35111016;
    public static final int 终极机甲 = 35120000;
    public static final int 机器人精通 = 35120001;
    public static final int 双幸运骰子 = 35120014;
    public static final int 战争机器_泰坦 = 35121003;
    public static final int 金属机甲_导弹战车 = 35121005;
    public static final int 卫星防护 = 35121006;
    public static final int 冒险岛勇士 = 35121007;
    public static final int 勇士的意志 = 35121008;
    public static final int 机器人工厂_RM1 = 35121009;
    public static final int 放大器_AF11 = 35121010;
    public static final int 机器人工厂_机器人 = 35121011;
    public static final int 激光爆破 = 35121012;
    public static final int 金属机甲_重机枪_4转 = 35121013;
    public static final int 神圣之力 = 35120030;
    public static final int 神圣敏捷 = 35120031;
    public static final int 神圣智力 = 35120032;
    public static final int 神圣幸运 = 35120033;
    public static final int 神圣爆击 = 35120034;
    public static final int 神圣精准 = 35120035;
    public static final int 神圣最大生命 = 35120036;
    public static final int 神圣最大魔法 = 35120037;
    public static final int 神圣最大恶魔精气 = 35120038;
    public static final int 神圣物理防御 = 35120039;
    public static final int 神圣魔法防御 = 35120040;
    public static final int 神圣移动 = 35120041;
    public static final int 神圣跳跃 = 35120042;
    public static final int 磁场_强化 = 35120043;
    public static final int 磁场_坚持 = 35120044;
    public static final int 磁场_缩短冷却时间 = 35120045;
    public static final int 放大器_AF_11_强化 = 35120046;
    public static final int 放大器_AF_11_组队强化 = 35120047;
    public static final int 放大器_AF_11_坚持 = 35120048;
    public static final int 激光爆破_强化 = 35120049;
    public static final int 激光爆破_额外目标 = 35120050;
    public static final int 激光爆破_额外攻击 = 35120051;
    public static final int 扭曲空间 = 35121052;
    public static final int 自由之墙 = 35121053;
    public static final int 反应钢甲 = 35121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\机械师.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */