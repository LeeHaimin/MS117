package constants.skills;

public class 火毒
{
    public static final int 魔力吸收 = 2100000;
    public static final int 咒语精通 = 2100006;
    public static final int 智慧激发 = 2100007;
    public static final int 元素吸收 = 2100009;
    public static final int 燎原之火 = 2100010;
    public static final int 精神力 = 2101001;
    public static final int 火焰宝珠 = 2101004;
    public static final int 毒雾术 = 2101005;
    public static final int 魔法狂暴 = 2101008;
    public static final int 极限魔力 = 2110000;
    public static final int 魔力激化 = 2110001;
    public static final int 魔法爆击 = 2110009;
    public static final int 魔力纵燃 = 2110012;
    public static final int 末日烈焰 = 2111002;
    public static final int 致命毒雾 = 2111003;
    public static final int 快速移动精通 = 2111007;
    public static final int 自然力重置 = 2111008;
    public static final int 绿水灵病毒 = 2111010;
    public static final int 元素配合 = 2111011;
    public static final int 神秘瞄准术 = 2120010;
    public static final int 魔力精通 = 2120012;
    public static final int 天降落星_坠落 = 2120013;
    public static final int 炽热吸收 = 2120014;
    public static final int 冒险岛勇士 = 2121000;
    public static final int 迷雾爆发 = 2121003;
    public static final int 终极无限 = 2121004;
    public static final int 火魔兽 = 2121005;
    public static final int 美杜莎之眼 = 2121006;
    public static final int 天降落星 = 2121007;
    public static final int 勇士的意志 = 2121008;
    public static final int 炙焰笼罩 = 2121011;
    public static final int 神圣之力 = 2120030;
    public static final int 神圣敏捷 = 2120031;
    public static final int 神圣智力 = 2120032;
    public static final int 神圣幸运 = 2120033;
    public static final int 神圣爆击 = 2120034;
    public static final int 神圣精准 = 2120035;
    public static final int 神圣最大生命 = 2120036;
    public static final int 神圣最大魔法 = 2120037;
    public static final int 最大精气值提高50点 = 2120038;
    public static final int 神圣物理防御 = 2120039;
    public static final int 神圣魔法防御 = 2120040;
    public static final int 神圣移动 = 2120041;
    public static final int 神圣跳跃 = 2120042;
    public static final int 致命毒雾_强化 = 2120043;
    public static final int 致命毒雾_伤害持续 = 2120044;
    public static final int 致命毒雾_持续伤害强化 = 2120045;
    public static final int 美杜莎之眼_强化 = 2120046;
    public static final int 美杜莎之眼_持续伤害强化 = 2120047;
    public static final int 美杜莎之眼_额外攻击 = 2120048;
    public static final int 迷雾爆发_额外攻击 = 2120049;
    public static final int 迷雾爆发_无视防御 = 2120050;
    public static final int 迷雾爆发_缩短冷却时间 = 2120051;
    public static final int 审判之焰 = 2121052;
    public static final int 传说冒险家 = 2121053;
    public static final int 火焰灵气 = 2121054;
    public static final int 审判之焰_人偶 = 2121055;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\火毒.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */