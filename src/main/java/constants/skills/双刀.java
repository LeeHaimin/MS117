package constants.skills;

public class 双刀
{
    public static final int 精准双刀 = 4300000;
    public static final int 暗影轻功 = 4301003;
    public static final int 龙卷风 = 4301004;
    public static final int 物理训练 = 4310006;
    public static final int 流云斩 = 4311002;
    public static final int 双刀风暴 = 4311003;
    public static final int 命运 = 4311005;
    public static final int 快速双刀 = 4311009;
    public static final int 武器用毒液 = 4320005;
    public static final int 闪光弹 = 4321002;
    public static final int 悬浮地刺 = 4321004;
    public static final int 暗影飞跃斩 = 4321006;
    public static final int 进阶隐身术 = 4330001;
    public static final int 生命偷取 = 4330007;
    public static final int 永恒黑暗 = 4330008;
    public static final int 影子闪避 = 4330009;
    public static final int 血雨腥风 = 4331000;
    public static final int 镜像分身 = 4331002;
    public static final int 地狱锁链 = 4331006;
    public static final int 锋利 = 4340010;
    public static final int 致命毒液 = 4340012;
    public static final int 双刀专家 = 4340013;
    public static final int 冒险岛勇士 = 4341000;
    public static final int 终极斩 = 4341002;
    public static final int 暴怒刀阵 = 4341004;
    public static final int 傀儡召唤 = 4341006;
    public static final int 荆棘 = 4341007;
    public static final int 勇士的意志 = 4341008;
    public static final int 幽灵一击 = 4341009;
    public static final int 突然袭击 = 4341011;
    public static final int 神圣之力 = 4340030;
    public static final int 神圣敏捷 = 4340031;
    public static final int 神圣智力 = 4340032;
    public static final int 神圣幸运 = 4340033;
    public static final int 神圣爆击 = 4340034;
    public static final int 神圣精准 = 4340035;
    public static final int 神圣最大生命 = 4340036;
    public static final int 神圣最大魔法 = 4340037;
    public static final int 神圣最大恶魔精气 = 4340038;
    public static final int 神圣物理防御 = 4340039;
    public static final int 神圣魔法防御 = 4340040;
    public static final int 神圣移动 = 4340041;
    public static final int 神圣跳跃 = 4340042;
    public static final int 血雨腥风_强化 = 4340043;
    public static final int 血雨腥风_额外目标 = 4340044;
    public static final int 血雨腥风_额外攻击 = 4340045;
    public static final int 幽灵一击_强化 = 4340046;
    public static final int 幽灵一击_无视防御 = 4340047;
    public static final int 幽灵一击_额外攻击 = 4340048;
    public static final int 突然袭击_强化 = 4340049;
    public static final int 突然袭击_持续伤害强化 = 4340050;
    public static final int 突然袭击_缩短冷却时间 = 4340051;
    public static final int 阿修罗 = 4341052;
    public static final int 传说冒险家 = 4341053;
    public static final int 隐形剑 = 4341054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\双刀.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */