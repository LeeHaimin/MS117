package constants.skills;

public class 龙神
{
    public static final int 蜗牛投掷术 = 20011000;
    public static final int 团队治疗 = 20011001;
    public static final int 疾风步 = 20011002;
    public static final int 匠人之魂 = 20011003;
    public static final int 英雄之回声 = 20011005;
    public static final int 锻造 = 20011007;
    public static final int 流星竹雨 = 20011009;
    public static final int 金刚霸体 = 20011010;
    public static final int 狂暴战魂 = 20011011;
    public static final int 绿水灵的弱点 = 20019002;
    public static final int 木妖的弱点 = 20019001;
    public static final int 猪猪的弱点 = 20019000;
    public static final int 龙飞行 = 20010022;
    public static final int 龙魂 = 22000000;
    public static final int 精灵弱化 = 22000002;
    public static final int 魔法导弹 = 22001001;
    public static final int 火焰环 = 22101000;
    public static final int 快速移动 = 22101001;
    public static final int 闪电箭 = 22111000;
    public static final int 魔法盾 = 22111001;
    public static final int 智慧激发 = 22120001;
    public static final int 咒语精通 = 22120002;
    public static final int 冰点寒气 = 22121000;
    public static final int 魔力闪爆 = 22131000;
    public static final int 魔法屏障 = 22131001;
    public static final int 自然力重置 = 22131002;
    public static final int 魔法爆击 = 22140000;
    public static final int 龙推力 = 22141001;
    public static final int 魔法狂暴 = 22141002;
    public static final int 飞龙传动 = 22141004;
    public static final int 魔力激化 = 22150000;
    public static final int 飞龙闪 = 22150004;
    public static final int 火焰喷射 = 22151001;
    public static final int 魔翼斩杀 = 22151002;
    public static final int 抗魔领域 = 22151003;
    public static final int 龙的愤怒 = 22160000;
    public static final int 地震 = 22161001;
    public static final int 鬼刻符 = 22161002;
    public static final int 极光恢复 = 22161003;
    public static final int 玛瑙的保佑 = 22161004;
    public static final int 快速移动精通 = 22161005;
    public static final int 魔法精通 = 22170001;
    public static final int 冒险岛勇士 = 22171000;
    public static final int 魔龙分身 = 22171002;
    public static final int 火焰轮 = 22171003;
    public static final int 勇士的意志 = 22171004;
    public static final int 玛瑙的祝福 = 22181000;
    public static final int 熊熊烈火 = 22181001;
    public static final int 黑暗迷雾 = 22181002;
    public static final int 灵魂之石 = 22181003;
    public static final int 玛瑙的意志 = 22181004;
    public static final int 神圣之力 = 22170030;
    public static final int 神圣敏捷 = 22170031;
    public static final int 神圣智力 = 22170032;
    public static final int 神圣幸运 = 22170033;
    public static final int 神圣爆击 = 22170034;
    public static final int 神圣精准 = 22170035;
    public static final int 神圣最大生命 = 22170036;
    public static final int 神圣最大魔法 = 22170037;
    public static final int 神圣最大恶魔精气 = 22170038;
    public static final int 神圣物理防御 = 22170039;
    public static final int 神圣魔法防御 = 22170040;
    public static final int 神圣移动 = 22170041;
    public static final int 神圣跳跃 = 22170042;
    public static final int 魔龙分身_强化 = 22170043;
    public static final int 魔龙分身_范围提升 = 22170044;
    public static final int 魔龙分身_无视防御 = 22170045;
    public static final int 黑暗迷雾_强化 = 22170046;
    public static final int 黑暗迷雾_缩短冷却时间 = 22170047;
    public static final int 黑暗迷雾_分裂伤害 = 22170048;
    public static final int 熊熊烈火_强化 = 22170049;
    public static final int 熊熊烈火_无视防御 = 22170050;
    public static final int 熊熊烈火_坚持 = 22170051;
    public static final int 召唤玛瑙龙 = 22171052;
    public static final int 英雄奥斯 = 22171053;
    public static final int 狂怒灵魂 = 22171054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\龙神.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */