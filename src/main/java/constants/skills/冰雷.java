package constants.skills;

public class 冰雷
{
    public static final int 魔力吸收 = 2200000;
    public static final int 咒语精通 = 2200006;
    public static final int 智慧激发 = 2200007;
    public static final int 极冻效果 = 2200011;
    public static final int 精神力 = 2201001;
    public static final int 雷电术 = 2201005;
    public static final int 冰冻术 = 2201008;
    public static final int 寒冰步 = 2201009;
    public static final int 魔法狂暴 = 2201010;
    public static final int 极限魔力 = 2210000;
    public static final int 魔力激化 = 2210001;
    public static final int 魔法爆击 = 2210009;
    public static final int 寒霜破裂 = 2210013;
    public static final int 冰咆哮 = 2211002;
    public static final int 快速移动精通 = 2211007;
    public static final int 自然力重置 = 2211008;
    public static final int 冰河锁链 = 2211010;
    public static final int 闪电风暴 = 2211011;
    public static final int 元素配合 = 2211012;
    public static final int 神秘瞄准术 = 2220010;
    public static final int 魔力精通 = 2220013;
    public static final int 落霜冰破_冰之枪 = 2220014;
    public static final int 冰冻效果 = 2220015;
    public static final int 冒险岛勇士 = 2221000;
    public static final int 终极无限 = 2221004;
    public static final int 冰破魔兽 = 2221005;
    public static final int 链环闪电 = 2221006;
    public static final int 落霜冰破 = 2221007;
    public static final int 勇士的意志 = 2221008;
    public static final int 极冻吐息 = 2221011;
    public static final int 寒霜爆晶 = 2221012;
    public static final int 神圣之力 = 2220030;
    public static final int 神圣敏捷 = 2220031;
    public static final int 神圣智力 = 2220032;
    public static final int 神圣幸运 = 2220033;
    public static final int 神圣爆击 = 2220034;
    public static final int 神圣精准 = 2220035;
    public static final int 神圣最大生命 = 2220036;
    public static final int 神圣最大魔法 = 2220037;
    public static final int 神圣最大恶魔精气 = 2220038;
    public static final int 神圣物理防御 = 2220039;
    public static final int 神圣魔法防御 = 2220040;
    public static final int 神圣移动 = 2220041;
    public static final int 神圣跳跃 = 2220042;
    public static final int 快速移动精通_强化 = 2220043;
    public static final int 快速移动精通_额外目标 = 2220044;
    public static final int 快速移动精通_范围提升 = 2220045;
    public static final int 链环闪电_强化 = 2220046;
    public static final int 链环闪电_额外目标 = 2220047;
    public static final int 链环闪电_额外攻击 = 2220048;
    public static final int 冰河锁链_额外目标 = 2220049;
    public static final int 冰河锁链_额外攻击 = 2220050;
    public static final int 冰河锁链_缩短冷却时间 = 2220051;
    public static final int 闪电矛 = 2221052;
    public static final int 传说冒险家 = 2221053;
    public static final int 寒冰灵气 = 2221054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\冰雷.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */