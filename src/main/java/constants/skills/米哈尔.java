package constants.skills;

public class 米哈尔
{
    public static final int 光之守护 = 50001214;
    public static final int 增加HP = 51000000;
    public static final int 灵魂盾 = 51000001;
    public static final int 灵魂敏捷 = 51000002;
    public static final int 灵魂跳跃 = 51001003;
    public static final int 灵魂之刃 = 51001004;
    public static final int 物理训练 = 51100000;
    public static final int 精准剑 = 51100001;
    public static final int 终结攻击 = 51100002;
    public static final int 快速剑 = 51101003;
    public static final int 愤怒 = 51101004;
    public static final int 灵魂突刺 = 51101005;
    public static final int 闪耀追逐 = 51101006;
    public static final int 自我恢复 = 51110000;
    public static final int 专注 = 51110001;
    public static final int 灵魂突击 = 51110002;
    public static final int 闪耀之光 = 51111003;
    public static final int 灵魂恢复术 = 51111004;
    public static final int 魔击无效 = 51111005;
    public static final int 灵魂挥剑 = 51111006;
    public static final int 闪耀爆破 = 51111007;
    public static final int 战斗精通 = 51120000;
    public static final int 进阶精准剑 = 51120001;
    public static final int 进阶终结攻击 = 51120002;
    public static final int 进阶灵魂盾 = 51120003;
    public static final int 稳如泰山 = 51121004;
    public static final int 冒险岛勇士 = 51121005;
    public static final int 灵魂之怒 = 51121006;
    public static final int 灵魂抨击 = 51121007;
    public static final int 闪耀爆炸 = 51121008;
    public static final int 神圣之力 = 51120030;
    public static final int 神圣敏捷 = 51120031;
    public static final int 神圣智力 = 51120032;
    public static final int 神圣幸运 = 51120033;
    public static final int 神圣爆击 = 51120034;
    public static final int 神圣精准 = 51120035;
    public static final int 神圣最大生命 = 51120036;
    public static final int 神圣最大魔法 = 51120037;
    public static final int 神圣最大恶魔精气 = 51120038;
    public static final int 神圣物理防御 = 51120039;
    public static final int 神圣魔法防御 = 51120040;
    public static final int 神圣移动 = 51120041;
    public static final int 神圣跳跃 = 51120042;
    public static final int 灵魂恢复术_坚持 = 51120043;
    public static final int 灵魂恢复术_钢铁皮肤 = 51120044;
    public static final int 灵魂恢复术_身体抗性 = 51120045;
    public static final int 闪耀爆炸_强化 = 51120046;
    public static final int 闪耀爆炸_额外目标 = 51120047;
    public static final int 闪耀爆炸_额外攻击 = 51120048;
    public static final int 灵魂抨击_强化 = 51120049;
    public static final int 灵魂抨击_额外机会 = 51120050;
    public static final int 灵魂抨击_额外攻击 = 51120051;
    public static final int 致命突击 = 51121052;
    public static final int 明日祝福 = 51121053;
    public static final int 神圣方块 = 51121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\米哈尔.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */