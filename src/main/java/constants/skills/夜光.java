package constants.skills;

public class 夜光
{
    public static final int 太阳火焰 = 20040216;
    public static final int 月蚀 = 20040217;
    public static final int 穿透 = 20040218;
    public static final int 平衡 = 20040219;
    public static final int 光明黑暗模式转换 = 20041239;
    public static final int 平衡_未知 = 20040220;
    public static final int 光之力量 = 20040221;
    public static final int 光之传送 = 20041222;
    public static final int 普通魔法防护 = 27000003;
    public static final int 强化光之魔法 = 27000106;
    public static final int 光明黑暗魔法强化 = 27000207;
    public static final int 光束瞬移 = 27001002;
    public static final int 魔力延伸 = 27001004;
    public static final int 魔法偷窃 = 27001005;
    public static final int 耀眼光球 = 27001100;
    public static final int 黑暗降临 = 27001201;
    public static final int 黑暗祝福 = 27100003;
    public static final int 咒语精通 = 27100005;
    public static final int 智慧激发 = 27100006;
    public static final int 魔法狂暴 = 27101004;
    public static final int 仙女发射 = 27101100;
    public static final int 闪爆光柱 = 27101101;
    public static final int 虚空重压 = 27101202;
    public static final int 生命潮汐 = 27110007;
    public static final int 抵抗之魔法盾 = 27111004;
    public static final int 光影防护 = 27111005;
    public static final int 光照精神力 = 27111006;
    public static final int 超级光谱 = 27111100;
    public static final int 闪耀救赎 = 27111101;
    public static final int 暗锁冲击 = 27111202;
    public static final int 死亡之刃 = 27111303;
    public static final int 魔法精通 = 27120007;
    public static final int 暗光精通 = 27120008;
    public static final int 晨星坠落_爆炸 = 27120211;
    public static final int 黑暗高潮 = 27121005;
    public static final int 黑暗巫术 = 27121006;
    public static final int 冒险岛勇士 = 27121009;
    public static final int 勇士的意志 = 27121010;
    public static final int 闪电反击 = 27121100;
    public static final int 晨星坠落 = 27121201;
    public static final int 启示录 = 27121202;
    public static final int 绝对死亡 = 27121303;
    public static final int 神圣之力 = 27120030;
    public static final int 神圣敏捷 = 27120031;
    public static final int 神圣智力 = 27120032;
    public static final int 神圣幸运 = 27120033;
    public static final int 神圣爆击 = 27120034;
    public static final int 神圣精准 = 27120035;
    public static final int 神圣最大生命 = 27120036;
    public static final int 神圣最大魔法 = 27120037;
    public static final int 神圣最大恶魔精气 = 27120038;
    public static final int 神圣物理防御 = 27120039;
    public static final int 神圣魔法防御 = 27120040;
    public static final int 神圣移动 = 27120041;
    public static final int 神圣跳跃 = 27120042;
    public static final int 闪电反击_强化 = 27120043;
    public static final int 闪电反击_削减 = 27120044;
    public static final int 闪电反击_范围提升 = 27120045;
    public static final int 启示录_强化 = 27120046;
    public static final int 启示录_重生 = 27120047;
    public static final int 启示录_额外目标 = 27120048;
    public static final int 绝对死亡_强化 = 27120049;
    public static final int 绝对死亡_额外目标 = 27120050;
    public static final int 绝对死亡_范围提升 = 27120051;
    public static final int 末日审判 = 27121052;
    public static final int 英雄奥斯 = 27121053;
    public static final int 记录 = 27121054;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\constants\skills\夜光.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */