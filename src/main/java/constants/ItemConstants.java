package constants;

import client.inventory.MapleInventoryType;
import client.inventory.MapleWeaponType;


public class ItemConstants
{
    public static final int[] rankC = {70000000, 70000001, 70000002, 70000003, 70000004, 70000005, 70000006, 70000007, 70000008, 70000009, 70000010, 70000011, 70000012, 70000013};
    public static final int[] rankB = {70000014, 70000015, 70000017, 70000018, 70000021, 70000022, 70000023, 70000024, 70000025, 70000026};
    public static final int[] rankA = {70000027, 70000028, 70000029, 70000030, 70000031, 70000032, 70000033, 70000034, 70000035, 70000036};
    public static final int[] rankS = {70000048, 70000049, 70000050, 70000051, 70000052, 70000053, 70000054, 70000055, 70000056, 70000057, 70000058, 70000059, 70000060, 70000061, 70000062};
    public static final int[] circulators = {2700000, 2700100, 2700200, 2700300, 2700400, 2700500, 2700600, 2700700, 2700800, 2700900, 2701000};
    public static final int[] rankBlock = {70000016, 70000037, 70000038, 70000039, 70000040, 70000041, 70000042, 70000043, 70000044, 70000045, 70000046, 70000047};

    public static boolean isHarvesting(int itemId)
    {
        return (itemId >= 1500000) && (itemId < 1520000);
    }

    public static boolean isRechargable(int itemId)
    {
        return (is飞镖道具(itemId)) || (is子弹道具(itemId));
    }

    public static boolean is飞镖道具(int itemId)
    {
        return itemId / 10000 == 207;
    }

    public static boolean is子弹道具(int itemId)
    {
        return itemId / 10000 == 233;
    }

    public static boolean isOverall(int itemId)
    {
        return itemId / 10000 == 105;
    }

    public static boolean isPet(int itemId)
    {
        return itemId / 10000 == 500;
    }

    public static boolean is弩矢道具(int itemId)
    {
        return (itemId >= 2061000) && (itemId < 2062000);
    }

    public static boolean is弓矢道具(int itemId)
    {
        return (itemId >= 2060000) && (itemId < 2061000);
    }

    public static boolean isMagicWeapon(int itemId)
    {
        int type = itemId / 10000;
        return (type == 137) || (type == 138) || (type == 121);
    }

    public static boolean isWeapon(int itemId)
    {
        if (itemId == 1342069)
        {
            return false;
        }
        return ((itemId >= 1300000) && (itemId < 1540000)) || (itemId / 1000 == 1212) || (itemId / 1000 == 1222) || (itemId / 1000 == 1232) || (itemId / 1000 == 1242) || (itemId / 1000 == 1252);
    }

    public static MapleInventoryType getInventoryType(int itemId)
    {
        byte type = (byte) (itemId / 1000000);
        if ((type < 1) || (type > 5))
        {
            return MapleInventoryType.UNDEFINED;
        }
        return MapleInventoryType.getByType(type);
    }

    public static boolean isShield(int itemId)
    {
        int cat = itemId / 10000;
        cat %= 100;
        return cat == 9;
    }

    public static boolean isEquip(int itemId)
    {
        return itemId / 1000000 == 1;
    }

    public static boolean isCleanSlate(int itemId)
    {
        return itemId / 100 == 20490;
    }

    public static boolean isAccessoryScroll(int itemId)
    {
        return itemId / 100 == 20492;
    }

    public static boolean isLimitBreakScroll(int itemId)
    {
        return itemId / 100 == 26140;
    }

    public static int getChaosNumber(int itemId)
    {
        switch (itemId)
        {
            case 2049116:
                return 10;
            case 2049119:
            case 2049132:
            case 2049133:
            case 2049134:
                return 8;
            case 2049135:
            case 2049136:
            case 2049137:
                return 7;
        }
        return 5;
    }

    public static boolean isChaosForGoodness(int itemId)
    {
        if (!isChaosScroll(itemId))
        {
            return false;
        }
        switch (itemId)
        {
            case 2049122:
            case 2049124:
            case 2049127:
            case 2049129:
            case 2049130:
            case 2049131:
            case 2049135:
            case 2049136:
            case 2049137:
            case 2049140:
            case 2049155:
                return true;
        }
        return false;
    }

    public static boolean isChaosScroll(int itemId)
    {
        if ((itemId >= 2049105) && (itemId <= 2049110))
        {
            return false;
        }
        return itemId / 100 == 20491;
    }

    public static boolean isEquipScroll(int scrollId)
    {
        return scrollId / 100 == 20493;
    }

    public static boolean isResetScroll(int scrollId)
    {
        return scrollId / 100 == 20496;
    }

    public static boolean isPotentialScroll(int scrollId)
    {
        return (scrollId / 100 == 20494) || (scrollId / 100 == 20497) || (scrollId == 5534000);
    }

    public static boolean isPotentialAddScroll(int scrollId)
    {
        switch (scrollId)
        {
            case 2048305:
            case 2048306:
            case 2048307:
            case 2048308:
            case 2048309:
            case 2048310:
                return true;
        }
        return false;
    }

    public static boolean is真觉醒冒险之心(int itemId)
    {
        switch (itemId)
        {
            case 1122122:
            case 1122123:
            case 1122124:
            case 1122125:
            case 1122126:
                return true;
        }
        return false;
    }

    public static boolean isSpecialScroll(int scrollId)
    {
        switch (scrollId)
        {
            case 2040727:
            case 2041058:
            case 2530000:
            case 2530001:
            case 2531000:
            case 5063100:
            case 5064000:
            case 5064003:
            case 5064100:
            case 5064200:
            case 5064300:
            case 5068100:
            case 5068200:
                return true;
        }
        return false;
    }

    public static boolean isAzwanScroll(int scrollId)
    {
        switch (scrollId)
        {


            case 2046060:
            case 2046061:
            case 2046062:
            case 2046063:
            case 2046064:
            case 2046065:
            case 2046066:
            case 2046067:
            case 2046068:
            case 2046069:
            case 2046141:
            case 2046142:
            case 2046143:
            case 2046144:
            case 2046145:
            case 2046519:
            case 2046520:
            case 2046521:
            case 2046522:
            case 2046523:
            case 2046524:
            case 2046525:
            case 2046526:
            case 2046527:
            case 2046528:
            case 2046529:
            case 2046530:
            case 2046701:
            case 2046702:
            case 2046703:
            case 2046704:
            case 2046705:
            case 2046706:
            case 2046707:
            case 2046708:
            case 2046709:
            case 2046710:
            case 2046711:
            case 2046712:
                return true;
        }

        return false;
    }

    public static boolean is回城卷轴(int id)
    {
        return (id >= 2030000) && (id < 2040000);
    }

    public static boolean is升级卷轴(int id)
    {
        return (id >= 2040000) && (id < 2050000);
    }

    public static boolean is短枪道具(int id)
    {
        return (id >= 1492000) && (id < 1500000);
    }

    public static boolean isUse(int id)
    {
        return (id >= 2000000) && (id < 3000000);
    }

    public static boolean is怪物召唤包(int id)
    {
        return id / 10000 == 210;
    }

    public static boolean is怪物卡片(int id)
    {
        return id / 10000 == 238;
    }

    public static boolean isBoss怪物卡(int id)
    {
        return id / 1000 >= 2388;
    }

    public static int getCardShortId(int id)
    {
        return id % 10000;
    }

    public static boolean is强化宝石(int id)
    {
        return (id >= 4250000) && (id <= 4251402);
    }

    public static boolean isOtherGem(int id)
    {
        switch (id)
        {
            case 1032062:
            case 1142156:
            case 1142157:
            case 2040727:
            case 2041058:
            case 4001174:
            case 4001175:
            case 4001176:
            case 4001177:
            case 4001178:
            case 4001179:
            case 4001180:
            case 4001181:
            case 4001182:
            case 4001183:
            case 4001184:
            case 4001185:
            case 4001186:
            case 4031980:
            case 4032312:
            case 4032334:
                return true;
        }
        return false;
    }

    public static boolean isNoticeItem(int itemId)
    {
        switch (itemId)
        {
            case 2028061:
            case 2028062:
            case 2290285:
            case 2430112:
            case 4020013:
            case 4021011:
            case 4021012:
            case 4021019:
            case 4021020:
            case 4021021:
            case 4021022:
            case 4310015:
                return true;
        }
        return false;
    }

    public static int getAdditionalSuccess(int itemId)
    {
        if (itemId == 2048200) return 5;
        if ((itemId == 2048201) || (itemId == 2048202)) return 10;
        if (itemId == 2048203) return 100;
        if (itemId == 2048204) return 20;
        if ((itemId == 2048300) || (itemId == 2048303)) return 60;
        if ((itemId == 2048301) || (itemId == 2048302)) return 80;
        if (itemId == 2048304)
        {
            return 100;
        }
        return 0;
    }

    public static int getNebuliteGrade(int id)
    {
        if (id / 10000 != 306)
        {
            return -1;
        }
        if ((id >= 3060000) && (id < 3061000)) return 0;
        if ((id >= 3061000) && (id < 3062000)) return 1;
        if ((id >= 3062000) && (id < 3063000)) return 2;
        if ((id >= 3063000) && (id < 3064000))
        {
            return 3;
        }
        return 4;
    }

    public static boolean is机甲装备(int itemId)
    {
        return (itemId >= 1610000) && (itemId < 1660000);
    }

    public static boolean is龙龙装备(int itemId)
    {
        return (itemId >= 1940000) && (itemId < 1980000);
    }

    public static boolean canHammer(int itemId)
    {
        switch (itemId)
        {
            case 1122000:
            case 1122076:
                return false;
        }
        return canScroll(itemId);
    }

    public static boolean canScroll(int itemId)
    {
        return ((itemId / 100000 != 19) && (itemId / 100000 != 16)) || ((itemId / 1000 == 1672) && (itemId != 1672030) && (itemId != 1672031) && (itemId != 1672032));
    }

    public static int getLowestPrice(int itemId)
    {
        switch (itemId)
        {
            case 2340000:
            case 2530000:
            case 2531000:
                return 50000000;
        }
        return -1;
    }

    public static int getModifier(int itemId, int up)
    {
        if (up <= 0)
        {
            return 0;
        }
        switch (itemId)
        {
            case 2022459:
            case 2860179:
            case 2860193:
            case 2860207:
                return 130;
            case 2022460:
            case 2022462:
            case 2022730:
                return 150;
            case 2860181:
            case 2860195:
            case 2860209:
                return 200;
        }
        if (itemId / 10000 == 286)
        {
            return 150;
        }
        return 200;
    }

    public static short getSlotMax(int itemId)
    {
        switch (itemId)
        {
            case 4030003:
            case 4030004:
            case 4030005:
                return 1;
            case 3993000:
            case 3993002:
            case 3993003:
            case 4001168:
            case 4031306:
            case 4031307:
                return 100;
            case 5220010:
            case 5220013:
                return 1000;
            case 5220020:
                return 2000;
        }
        return 0;
    }

    public static boolean isDropRestricted(int itemId)
    {
        return (itemId == 3012000) || (itemId == 4030004) || (itemId == 1052098) || (itemId == 1052202);
    }

    public static boolean isPickupRestricted(int itemId)
    {
        return (itemId == 4030003) || (itemId == 4030004);
    }

    public static short getStat(int itemId, int def)
    {
        switch (itemId)
        {
            case 1002419:
                return 5;
            case 1002959:
                return 25;
            case 1142002:
                return 10;
            case 1122121:
                return 7;
        }
        return (short) def;
    }

    public static short getHpMp(int itemId, int def)
    {
        switch (itemId)
        {
            case 1122121:
                return 500;
            case 1002959:
            case 1142002:
                return 1000;
        }
        return (short) def;
    }

    public static short getATK(int itemId, int def)
    {
        switch (itemId)
        {
            case 1122121:
                return 3;
            case 1002959:
                return 4;
            case 1142002:
                return 9;
        }
        return (short) def;
    }

    public static short getDEF(int itemId, int def)
    {
        switch (itemId)
        {
            case 1122121:
                return 250;
            case 1002959:
                return 500;
        }
        return (short) def;
    }

    public static int getRewardPot(int itemid, int closeness)
    {
        switch (itemid)
        {
            case 2440000:
                switch (closeness / 10)
                {
                    case 0:
                    case 1:
                    case 2:
                        return 2028041 + closeness / 10;
                    case 3:
                    case 4:
                    case 5:
                        return 2028046 + closeness / 10;
                    case 6:
                    case 7:
                    case 8:
                        return 2028049 + closeness / 10;
                }
                return 2028057;
            case 2440001:
                switch (closeness / 10)
                {
                    case 0:
                    case 1:
                    case 2:
                        return 2028044 + closeness / 10;
                    case 3:
                    case 4:
                    case 5:
                        return 2028049 + closeness / 10;
                    case 6:
                    case 7:
                    case 8:
                        return 2028052 + closeness / 10;
                }
                return 2028060;
            case 2440002:
                return 2028069;
            case 2440003:
                return 2430278;
            case 2440004:
                return 2430381;
            case 2440005:
                return 2430393;
        }
        return 0;
    }

    public static boolean isLogItem(int itemId)
    {
        switch (itemId)
        {
            case 2040006:
            case 2040007:
            case 2040303:
            case 2040403:
            case 2040506:
            case 2040507:
            case 2040603:
            case 2040709:
            case 2040710:
            case 2040711:
            case 2040806:
            case 2040903:
            case 2041024:
            case 2041025:
            case 2043003:
            case 2043103:
            case 2043203:
            case 2043303:
            case 2043703:
            case 2043803:
            case 2044003:
            case 2044019:
            case 2044103:
            case 2044203:
            case 2044303:
            case 2044403:
            case 2044503:
            case 2044603:
            case 2044703:
            case 2044815:
            case 2044908:
            case 2049000:
            case 2049001:
            case 2049002:
            case 2340000:
            case 4000463:
                return true;
        }
        return false;
    }

    public static int[] getInnerSkillbyRank(int rank)
    {
        if (rank == 0) return rankC;
        if (rank == 1) return rankB;
        if (rank == 2) return rankA;
        if (rank == 3)
        {
            return rankS;
        }
        return null;
    }

    public static boolean isTablet(int itemId)
    {
        return itemId / 1000 == 2047;
    }

    public static boolean isGeneralScroll(int itemId)
    {
        return itemId / 1000 == 2046;
    }

    public static int getSuccessTablet(int scrollId, int level)
    {
        if (scrollId % 1000 / 100 == 2)
        {
            switch (level)
            {
                case 0:
                    return 70;
                case 1:
                    return 55;
                case 2:
                    return 43;
                case 3:
                    return 33;
                case 4:
                    return 26;
                case 5:
                    return 20;
                case 6:
                    return 16;
                case 7:
                    return 12;
                case 8:
                    return 10;
            }
            return 7;
        }
        if (scrollId % 1000 / 100 == 3)
        {
            switch (level)
            {
                case 0:
                    return 70;
                case 1:
                    return 35;
                case 2:
                    return 18;
                case 3:
                    return 12;
            }
            return 7;
        }

        switch (level)
        {
            case 0:
                return 70;
            case 1:
                return 50;
            case 2:
                return 36;
            case 3:
                return 26;
            case 4:
                return 19;
            case 5:
                return 14;
            case 6:
                return 10;
        }
        return 7;
    }

    public static int getCurseTablet(int scrollId, int level)
    {
        if (scrollId % 1000 / 100 == 2)
        {
            switch (level)
            {
                case 0:
                    return 10;
                case 1:
                    return 12;
                case 2:
                    return 16;
                case 3:
                    return 20;
                case 4:
                    return 26;
                case 5:
                    return 33;
                case 6:
                    return 43;
                case 7:
                    return 55;
                case 8:
                    return 70;
            }
            return 100;
        }
        if (scrollId % 1000 / 100 == 3)
        {
            switch (level)
            {
                case 0:
                    return 12;
                case 1:
                    return 18;
                case 2:
                    return 35;
                case 3:
                    return 70;
            }
            return 100;
        }

        switch (level)
        {
            case 0:
                return 10;
            case 1:
                return 14;
            case 2:
                return 19;
            case 3:
                return 26;
            case 4:
                return 36;
            case 5:
                return 50;
            case 6:
                return 70;
        }
        return 100;
    }

    public static boolean isAccessory(int itemId)
    {
        return ((itemId >= 1010000) && (itemId < 1040000)) || ((itemId >= 1122000) && (itemId < 1153000)) || ((itemId >= 1112000) && (itemId < 1113000));
    }

    public static boolean isRing(int itemId)
    {
        return (itemId >= 1112000) && (itemId < 1113000);
    }

    public static boolean isEffectRing(int itemid)
    {
        return (is好友戒指(itemid)) || (is恋人戒指(itemid)) || (is结婚戒指(itemid));
    }

    public static boolean is好友戒指(int itemId)
    {
        switch (itemId)
        {
            case 1049000:
            case 1112800:
            case 1112801:
            case 1112802:
            case 1112810:
            case 1112811:
            case 1112812:
            case 1112817:
                return true;
        }
        return false;
    }

    public static boolean is恋人戒指(int itemId)
    {
        switch (itemId)
        {
            case 1048000:
            case 1048001:
            case 1048002:
            case 1112001:
            case 1112002:
            case 1112003:
            case 1112005:
            case 1112006:
            case 1112007:
            case 1112012:
            case 1112013:
            case 1112014:
            case 1112015:
            case 1112816:
            case 1112820:
                return true;
        }
        return false;
    }

    public static boolean is结婚戒指(int itemId)
    {
        switch (itemId)
        {
            case 1112300:
            case 1112301:
            case 1112302:
            case 1112303:
            case 1112304:
            case 1112305:
            case 1112306:
            case 1112307:
            case 1112308:
            case 1112309:
            case 1112310:
            case 1112311:
            case 1112312:
            case 1112315:
            case 1112316:
            case 1112317:
            case 1112318:
            case 1112319:
            case 1112320:
            case 1112804:
                return true;
        }
        return false;
    }

    public static boolean isSubWeapon(int itemId)
    {
        switch (itemId / 10000)
        {
        }
        return true;
    }

    public static boolean is双刀主手(int itemId)
    {
        return itemId / 10000 == 133;
    }

    public static boolean is双刀副手(int itemId)
    {
        return (itemId / 10000 == 134) && (itemId != 1342069);
    }

    public static boolean is幻影卡片(int itemId)
    {
        return (itemId >= 1352100) && (itemId <= 1352107);
    }

    public static boolean is龙传宝盒(int itemId)
    {
        return (itemId >= 1352300) && (itemId <= 1352304);
    }

    public static boolean is夜光宝珠(int itemId)
    {
        return (itemId >= 1352400) && (itemId <= 1352404);
    }

    public static boolean is狂龙副手(int itemId)
    {
        return (itemId >= 1352500) && (itemId <= 1352504);
    }

    public static boolean is萝莉副手(int itemId)
    {
        return (itemId >= 1352600) && (itemId <= 1352604);
    }

    public static boolean is特殊副手(int itemId)
    {
        return is双弩箭矢(itemId);
    }

    public static boolean is双弩箭矢(int itemId)
    {
        return (itemId >= 1352000) && (itemId <= 1352007);
    }

    public static boolean isTwoHanded(int itemId)
    {
        return isTwoHanded(itemId, 0);
    }

    public static boolean isTwoHanded(int itemId, int job)
    {
        switch (getWeaponType(itemId))
        {
            case 短枪:
                return (job < 570) || (job > 572);
            case 双手剑:
                return (job < 6100) || (job > 6112);
            case 双手斧:
            case 指节:
            case 双手钝器:
            case 弓:
            case 拳套:
            case 弩:
            case 矛:
            case 枪:
            case 手持火炮:
                return true;
        }
        return false;
    }

    public static MapleWeaponType getWeaponType(int itemId)
    {
        int cat = itemId / 10000;
        cat %= 100;
        switch (cat)
        {
            case 21:
                return MapleWeaponType.双头杖;
            case 22:
                return MapleWeaponType.灵魂手铳;
            case 23:
                return MapleWeaponType.亡命剑;
            case 24:
                return MapleWeaponType.能量剑;
            case 25:
                return MapleWeaponType.驯兽魔法棒;
            case 30:
                return MapleWeaponType.单手剑;
            case 31:
                return MapleWeaponType.单手斧;
            case 32:
                return MapleWeaponType.单手钝器;
            case 33:
                return MapleWeaponType.短刀;
            case 34:
                return MapleWeaponType.双刀副手;
            case 35:
                return MapleWeaponType.特殊副手;
            case 36:
                return MapleWeaponType.手杖;
            case 37:
                return MapleWeaponType.短杖;
            case 38:
                return MapleWeaponType.长杖;
            case 40:
                return MapleWeaponType.双手剑;
            case 41:
                return MapleWeaponType.双手斧;
            case 42:
                return MapleWeaponType.双手钝器;
            case 43:
                return MapleWeaponType.枪;
            case 44:
                return MapleWeaponType.矛;
            case 45:
                return MapleWeaponType.弓;
            case 46:
                return MapleWeaponType.弩;
            case 47:
                return MapleWeaponType.拳套;
            case 48:
                return MapleWeaponType.指节;
            case 49:
                return MapleWeaponType.短枪;
            case 52:
                return MapleWeaponType.双弩枪;
            case 53:
                return MapleWeaponType.手持火炮;
            case 56:
                return MapleWeaponType.大剑;
            case 57:
                return MapleWeaponType.太刀;
        }

        return MapleWeaponType.没有武器;
    }
}