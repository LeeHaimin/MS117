package handling.login;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;
import tools.Triple;

public class LoginInformationProvider
{
    private static final LoginInformationProvider instance = new LoginInformationProvider();
    protected final List<String> ForbiddenName = new ArrayList<>();
    protected final List<String> Curse = new ArrayList<>();
    protected final List<Integer> makeCharInfoItemIds = new ArrayList<>();

    protected final Map<Triple<Integer, Integer, Integer>, List<Integer>> makeCharInfo = new HashMap<>();

    protected LoginInformationProvider()
    {
        String WZpath = System.getProperty("wzpath");
        MapleDataProvider prov = MapleDataProviderFactory.getDataProvider(new java.io.File(WZpath + "/Etc.wz"));
        MapleData nameData = prov.getData("ForbiddenName.img");
        for (MapleData data : nameData.getChildren())
        {
            this.ForbiddenName.add(MapleDataTool.getString(data));
        }
        this.ForbiddenName.add("落叶无痕");
        this.ForbiddenName.add("HiredMerch");
        nameData = prov.getData("Curse.img");
        for (MapleData data : nameData.getChildren())
        {
            this.Curse.add(MapleDataTool.getString(data).split(",")[0]);
            this.ForbiddenName.add(MapleDataTool.getString(data).split(",")[0]);
        }
        MapleData infoData = prov.getData("MakeCharInfo.img");

        for (MapleData dat : infoData)
        {
            if ((!dat.getName().endsWith("Male")) && (!dat.getName().endsWith("Female")) && (!dat.getName().endsWith("Adventurer")) && (!dat.getName().equals("10112_Dummy")))
            {
                int type;

                if (dat.getName().equals("000_1"))
                {
                    type = JobType.getById(1).type;
                }
                else
                {
                    if (dat.getName().equals("3001_Dummy"))
                    {
                        type = JobType.getById(6).type;
                    }
                    else
                    {
                        type = JobType.getById(Integer.parseInt(dat.getName())).type;
                    }
                }
                for (MapleData d : dat)
                {
                    int gender;
                    if ((d.getName().equals("male")) || (d.getName().startsWith("male")))
                    {
                        gender = 0;
                    }
                    else
                    {
                        if ((!d.getName().equals("female")) && (!d.getName().startsWith("female"))) continue;
                        gender = 1;
                    }


                    for (MapleData da : d)
                    {
                        Triple<Integer, Integer, Integer> key = new Triple(gender, Integer.parseInt(da.getName()), type);
                        List<Integer> our = this.makeCharInfo.get(key);
                        if (our == null)
                        {
                            our = new ArrayList<>();
                            this.makeCharInfo.put(key, our);
                        }
                        for (MapleData dd : da)
                            if (!dd.getName().equals("name")) our.add(MapleDataTool.getInt(dd, -1));
                    }
                }
            }
        }
        MapleData dat;
        int type;
        int gender;
        List<Integer> our;
        MapleData uA = infoData.getChildByPath("UltimateAdventurer");
        for (MapleData it : uA)
        {
            Triple key = new Triple(-1, Integer.parseInt(it.getName()), JobType.终极冒险家.type);
            our = this.makeCharInfo.get(key);
            if (our == null)
            {
                our = new ArrayList<>();
                this.makeCharInfo.put(key, our);
            }
            for (MapleData d : it)
            {
                our.add(MapleDataTool.getInt(d, -1));
            }
        }
        Object key;
        for (MapleData data : infoData)
        {
            if (!data.getName().equalsIgnoreCase("UltimateAdventurer"))
            {

                if ((data.getName().endsWith("Male")) || (data.getName().endsWith("Female")))
                {
                    for (key = data.iterator(); ((Iterator) key).hasNext(); )
                    {
                        dat = (MapleData) ((Iterator) key).next();
                        for (MapleData da : dat)
                        {
                            int itemId = MapleDataTool.getInt(da, -1);
                            if ((itemId > 1000000) && (!this.makeCharInfoItemIds.contains(itemId)))
                            {
                                this.makeCharInfoItemIds.add(itemId);
                            }
                        }
                    }
                }
                else
                {
                    for (key = data.iterator(); ((Iterator) key).hasNext(); )
                    {
                        dat = (MapleData) ((Iterator) key).next();
                        if ((dat.getName().startsWith("male")) || (dat.getName().startsWith("female")))
                        {
                            for (MapleData da : dat)
                            {
                                for (MapleData dd : da)
                                {
                                    if (!dd.getName().equals("name"))
                                    {
                                        int itemId = MapleDataTool.getInt(dd, -1);
                                        if ((itemId > 1000000) && (!this.makeCharInfoItemIds.contains(itemId)))
                                        {
                                            this.makeCharInfoItemIds.add(itemId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static LoginInformationProvider getInstance()
    {
        return instance;
    }

    public boolean isForbiddenName(String in)
    {
        for (String name : this.ForbiddenName)
        {
            if (in.toLowerCase().contains(name.toLowerCase()))
            {
                return true;
            }
        }
        return false;
    }


    public boolean isCurseMsg(String in)
    {
        for (String name : this.Curse)
        {
            if (in.toLowerCase().contains(name.toLowerCase()))
            {
                return true;
            }
        }
        return false;
    }

    public boolean isEligibleItem(int gender, int val, int job, int item)
    {
        if (item < 0)
        {
            return false;
        }
        Triple<Integer, Integer, Integer> key = new Triple(gender, val, job);
        List<Integer> our = this.makeCharInfo.get(key);
        if (our == null)
        {
            return false;
        }
        return our.contains(item);
    }


    public boolean isEligibleItem(int itemId)
    {
        if (itemId < 0)
        {
            return false;
        }
        return (itemId == 0) || (this.makeCharInfoItemIds.contains(itemId));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\LoginInformationProvider.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */