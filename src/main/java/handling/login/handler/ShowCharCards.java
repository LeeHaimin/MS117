package handling.login.handler;

import client.MapleClient;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class ShowCharCards
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int accId = slea.readInt();
        if ((!c.isLoggedIn()) || (c.getAccID() != accId))
        {
            c.getSession().close(true);
            return;
        }
        c.getSession().write(LoginPacket.showCharCards(c.getAccCardSlots()));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\ShowCharCards.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */