package handling.login.handler;

import java.util.Calendar;

import client.MapleClient;
import handling.login.LoginServer;
import handling.login.LoginWorker;
import server.AutoRegister;
import tools.DateUtil;
import tools.MaplePacketCreator;
import tools.StringUtil;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class LoginPasswordHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int[] bytes = new int[6];
        for (int i = 0; i < bytes.length; i++)
        {
            bytes[i] = slea.readByteAsInt();
        }
        StringBuilder sps = new StringBuilder();
        for (int aByte : bytes)
        {
            sps.append(StringUtil.getLeftPaddedStr(Integer.toHexString(aByte).toUpperCase(), '0', 2));
            sps.append("-");
        }
        String macData = sps.toString();
        macData = macData.substring(0, macData.length() - 1);
        c.setMac(macData);
        slea.skip(15);
        int loginok = 0;
        String login = slea.readMapleAsciiString();
        String pwd = slea.readMapleAsciiString();
        boolean isIpBan = c.hasBannedIP();
        boolean isMacBan = c.hasBannedMac();
        boolean isBanned = (isIpBan) || (isMacBan);
        if (isBanned)
        {
            c.clearInformation();
            c.getSession().write(MaplePacketCreator.serverNotice(1, "无法进入游戏."));
            c.getSession().write(LoginPacket.getLoginFailed(16));
            return;
        }
        if ((LoginServer.isAutoReg()) && (!AutoRegister.getAccountExists(login)))
        {
            if (AutoRegister.createAccount(login, pwd, c.getSession().getRemoteAddress().toString()))
            {
                c.getSession().write(MaplePacketCreator.serverNotice(1, "注册帐号成功.\r\n请重新输入帐号密码进入游戏."));
            }
            else
            {
                c.getSession().write(MaplePacketCreator.serverNotice(1, "注册帐号失败."));
            }
            c.clearInformation();
            c.getSession().write(LoginPacket.getLoginFailed(16));
            return;
        }
        loginok = c.login(login, pwd, isBanned);

        Calendar tempbannedTill = c.getTempBanCalendar();
        if ((tempbannedTill != null) && (tempbannedTill.getTimeInMillis() > System.currentTimeMillis()))
        {
            c.clearInformation();
            long tempban = DateUtil.getTempBanTimestamp(tempbannedTill.getTimeInMillis());
            c.getSession().write(LoginPacket.getTempBan(tempban, c.getBanReason()));
            return;
        }

        if ((loginok == 3) && (!isBanned))
        {
            c.clearInformation();
            c.getSession().write(LoginPacket.getTempBan(2147483647L, c.getBanReason()));
        }
        else if ((loginok == 0) && (isBanned) && (!c.isGm()))
        {
            c.getSession().write(LoginPacket.getPermBan((byte) 1));
        }
        else if (loginok != 0)
        {
            c.clearInformation();
            c.getSession().write(LoginPacket.getLoginFailed(loginok));
        }
        else if (c.getGender() == 10)
        {
            c.updateLoginState(4);
            c.getSession().write(LoginPacket.genderNeeded(c));
        }
        else if ((LoginServer.isCheckMacs()) && (!c.hasCheckMac(macData)))
        {
            c.clearInformation();
            c.getSession().write(MaplePacketCreator.serverNotice(1, "登陆标识符不正确."));
            c.getSession().write(LoginPacket.getLoginFailed(16));
        }
        else
        {
            c.loginAttempt = 0;
            c.updateMacs();
            LoginWorker.registerClient(c);
        }
    }
}