package handling.login.handler;

import client.MapleClient;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class DeleteCharHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (!c.isGm())
        {
            return;
        }
        String Secondpw_Client = slea.readMapleAsciiString();
        int charId = slea.readInt();
        if ((!c.login_Auth(charId)) || (!c.isLoggedIn()))
        {
            c.getSession().close(true);
            return;
        }
        byte state = 0;
        if (c.getSecondPassword() != null)
        {
            if (Secondpw_Client == null)
            {
                c.getSession().close(true);
                return;
            }
            if (!c.CheckSecondPassword(Secondpw_Client))
            {
                state = 12;
            }
        }

        if (state == 0)
        {
            state = (byte) c.deleteCharacter(charId);
        }
        c.getSession().write(LoginPacket.deleteCharResponse(charId, state));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\DeleteCharHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */