package handling.login.handler;

import client.MapleCharacterUtil;
import client.MapleClient;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class ShowAccCash
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int accId = slea.readInt();
        if (c.getAccID() == accId)
        {
            if (c.getPlayer() != null)
            {
                c.getSession().write(MaplePacketCreator.showPlayerCash(c.getPlayer()));
            }
            else
            {
                Pair<Integer, Integer> cashInfo = MapleCharacterUtil.getCashByAccId(accId);
                if (cashInfo == null)
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                c.getSession().write(LoginPacket.ShowAccCash(cashInfo.getLeft(), cashInfo.getRight()));
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\ShowAccCash.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */