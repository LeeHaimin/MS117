package handling.login.handler;

import java.util.List;

import client.MapleCharacter;
import client.MapleClient;
import handling.channel.ChannelServer;
import handling.world.World;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class CharlistRequestHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (!c.isLoggedIn())
        {
            c.getSession().close(true);
            return;
        }
        int server = slea.readShort();
        int channel = slea.readByte() + 1;
        if ((!World.isChannelAvailable(channel)) || (server != 0))
        {
            c.getSession().write(LoginPacket.getLoginFailed(10));
            return;
        }

        System.out.println("客户地址: " + c.getSession().getRemoteAddress().toString().split(":")[0] + " 连接到世界服务器: " + server + " 频道: " + channel);

        List<MapleCharacter> chars = c.loadCharacters(server);
        if ((chars != null) && (ChannelServer.getInstance(channel) != null))
        {
            c.setWorld(server);
            c.setChannel(channel);

            c.getSession().write(LoginPacket.getCharList(c.getSecondPassword(), chars, c.getAccCharSlots()));
        }
        else
        {
            c.getSession().close(true);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\CharlistRequestHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */