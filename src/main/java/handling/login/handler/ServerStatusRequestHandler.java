package handling.login.handler;

import client.MapleClient;
import handling.login.LoginServer;
import tools.packet.LoginPacket;


public class ServerStatusRequestHandler
{
    public static void handlePacket(MapleClient c)
    {
        int numPlayer = LoginServer.getUsersOn();
        int userLimit = LoginServer.getUserLimit();
        if (numPlayer >= userLimit)
        {
            c.getSession().write(LoginPacket.getServerStatus(2));
        }
        else if (numPlayer >= userLimit * 0.8D)
        {
            c.getSession().write(LoginPacket.getServerStatus(1));
        }
        else
        {
            c.getSession().write(LoginPacket.getServerStatus(0));
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\ServerStatusRequestHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */