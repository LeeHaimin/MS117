package handling.login.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class ViewCharHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        Map<Byte, ArrayList<MapleCharacter>> worlds = new HashMap<>();
        List<MapleCharacter> chars = c.loadCharacters(0);
        c.getSession().write(LoginPacket.showAllCharacter(chars.size()));
        for (MapleCharacter chr : chars)
        {
            if (chr != null)
            {
                ArrayList<MapleCharacter> chrr;
                if (!worlds.containsKey(chr.getWorld()))
                {
                    chrr = new ArrayList<>();
                    worlds.put(chr.getWorld(), chrr);
                }
                else
                {
                    chrr = worlds.get(chr.getWorld());
                }
                chrr.add(chr);
            }
        }
        for (Map.Entry<Byte, ArrayList<MapleCharacter>> w : worlds.entrySet())
        {
            c.getSession().write(LoginPacket.showAllCharacterInfo(w.getKey(), w.getValue(), c.getSecondPassword()));
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\ViewCharHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */