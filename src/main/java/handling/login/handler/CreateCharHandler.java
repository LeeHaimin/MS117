package handling.login.handler;

import org.apache.log4j.Logger;

import client.MapleCharacter;
import client.MapleCharacterUtil;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import handling.login.JobType;
import handling.login.LoginInformationProvider;
import server.MapleItemInformationProvider;
import server.ServerProperties;
import server.quest.MapleQuest;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class CreateCharHandler
{
    private static final Logger log = Logger.getLogger(CreateCharHandler.class);

    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (!c.isLoggedIn())
        {
            c.getSession().close(true);
            return;
        }


        int faceMark = 0;

        int cape = 0;
        int bottom = 0;


        int shield = 0;


        String name = slea.readMapleAsciiString();
        int keymode = slea.readInt();
        if ((keymode != 0) && (keymode != 1))
        {
            System.out.println("创建角色错误 键盘模式错误 当前模式: " + keymode);
            c.getSession().write(LoginPacket.charNameResponse(name, (byte) 3));
            return;
        }
        slea.readInt();
        int job_type = slea.readInt();
        JobType job = JobType.getByType(job_type);
        if (job == null)
        {
            System.out.println("创建角色错误 没有找到该职业类型的数据 当前职业类型: " + job_type);
            return;
        }
        short subcategory = slea.readShort();
        JobType jobType = JobType.getByType(job_type);
        byte gender = slea.readByte();
        byte skin = slea.readByte();
        byte hairColor = slea.readByte();
        int face = slea.readInt();
        int hair = slea.readInt();
        if (job.faceMark)
        {
            faceMark = slea.readInt();
        }
        if (job == JobType.林之灵)
        {
            slea.readInt();
            slea.readInt();
        }
        int top = slea.readInt();
        if (job.cape)
        {
            cape = slea.readInt();
        }

        if ((top / 10000 == 104) || (job.bottom))
        {
            bottom = slea.readInt();
        }
        int shoes = slea.readInt();
        int weapon = slea.readInt();
        if (slea.available() >= 4L)
        {
            shield = slea.readInt();
        }
        if (ServerProperties.ShowPacket())
        {
            log.info("\r\n名字: " + name + "\r\n职业: " + job_type + "\r\n性别: " + gender + "\r\n皮肤: " + skin + "\r\n头发: " + hairColor + "\r\n脸型: " + face + "\r\n发型: " + hair + "\r\n脸饰: " + faceMark +
                    "\r\n上衣: " + top + "\r\n裤子: " + bottom + "\r\n鞋子: " + shoes + "\r\n武器: " + weapon + "\r\n盾牌: " + shield + "\r\n");
        }


        MapleItemInformationProvider li = MapleItemInformationProvider.getInstance();
        int[] items = {top, bottom, cape, shoes, weapon, shield};
        for (int i : items)
        {
            if (!LoginInformationProvider.getInstance().isEligibleItem(i))
            {
                log.info("[作弊] 新建角色装备检测失败 名字: " + name + " 职业: " + job_type + " 道具ID: " + i + " - " + li.getName(i));
                c.getSession().write(LoginPacket.charNameResponse(name, (byte) 3));
                return;
            }
        }

        MapleCharacter newchar = MapleCharacter.getDefault(c, jobType);
        newchar.setWorld((byte) c.getWorld());
        newchar.setFace(face);
        newchar.setHair(hair);
        newchar.setGender(gender);
        newchar.setName(name);
        newchar.setSkinColor(skin);
        newchar.setDecorate(faceMark);
        if (job == JobType.神之子)
        {
            newchar.setLevel((short) 100);
            newchar.getStat().str = 518;
            newchar.getStat().dex = 4;
            newchar.getStat().luk = 4;
            newchar.getStat().int_ = 4;
            newchar.getStat().maxhp = 6485;
            newchar.getStat().hp = 6485;
            newchar.getStat().maxmp = 100;
            newchar.getStat().mp = 100;
            newchar.setRemainingSp(3, 0);
            newchar.setRemainingSp(3, 1);
        }
        if (job == JobType.林之灵)
        {
            newchar.setLevel((short) 10);
            newchar.getStat().maxhp = 570;
            newchar.getStat().hp = 570;
            newchar.getStat().maxmp = 270;
            newchar.getStat().mp = 270;
            newchar.setRemainingAp((short) 45);
            newchar.setRemainingSp(3, 0);
            newchar.updateInfoQuest(59300, "bTail=1;bEar=1;TailID=5010119;EarID=5010116", false);
        }

        MapleInventory equipedIv = newchar.getInventory(MapleInventoryType.EQUIPPED);

        int[][] equips = {{top, -5}, {bottom, -6}, {shoes, -7}, {cape, -9}, {weapon, -11}, {shield, -10}};


        for (int[] i : equips)
        {
            if (i[0] > 0)
            {
                Item item = li.getEquipById(i[0]);
                item.setPosition((byte) i[1]);
                item.setGMLog("角色创建");
                equipedIv.addFromDB(item);
            }
        }

        newchar.getInventory(MapleInventoryType.USE).addItem(new Item(2000013, (short) 0, (short) 100, (short) 0));
        newchar.getInventory(MapleInventoryType.USE).addItem(new Item(2000014, (short) 0, (short) 100, (short) 0));

        int[][] guidebooks = {{4161001, 0}, {4161047, 1}, {4161048, 2000}, {4161052, 2001}, {4161054, 3}, {4161079, 2002}};
        int guidebook = 0;
        int[][] arrayOfInt3 = guidebooks;
        for (int[] array : arrayOfInt3)
        {
            if (newchar.getJob() == array[1])
            {
                guidebook = array[0];
            }
            else if (newchar.getJob() / 1000 == array[1])
            {
                guidebook = array[0];
            }
        }
        if (guidebook > 0)
        {
            newchar.getInventory(MapleInventoryType.ETC).addItem(new Item(guidebook, (short) 0, (short) 1, (short) 0));
        }

        if (job == JobType.骑士团)
        {
            newchar.setQuestAdd(MapleQuest.getInstance(20022), (byte) 1, "1");
            newchar.setQuestAdd(MapleQuest.getInstance(20010), (byte) 1, null);
        }

        if ((MapleCharacterUtil.canCreateChar(name, c.isGm())) && ((!LoginInformationProvider.getInstance().isForbiddenName(name)) || (c.isGm())) && ((c.isGm()) || (c.canMakeCharacter(c.getWorld()))))
        {
            MapleCharacter.saveNewCharToDB(newchar, jobType, subcategory, keymode == 0);
            c.getSession().write(LoginPacket.addNewCharEntry(newchar, true));
            c.createdChar(newchar.getId());
        }
        else
        {
            c.getSession().write(LoginPacket.addNewCharEntry(newchar, false));
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\CreateCharHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */