package handling.login.handler;

import client.MapleClient;
import handling.login.LoginServer;
import tools.packet.LoginPacket;


public class ServerListRequestHandler
{
    public static void handlePacket(MapleClient c, boolean packet)
    {
        if (packet)
        {
            return;
        }
        c.getSession().write(LoginPacket.getLoginWelcome());
        c.getSession().write(LoginPacket.getServerList(0, LoginServer.getLoad()));
        c.getSession().write(LoginPacket.getEndOfServerList());
        c.getSession().write(LoginPacket.enableRecommended());
    }
}