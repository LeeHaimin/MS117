package handling.login.handler;

import client.MapleClient;
import configs.ServerConfig;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class MapLoginHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        byte mapleType = slea.readByte();
        short mapleVersion = slea.readShort();
        String maplePatch = String.valueOf(slea.readShort());
        if ((mapleType != ServerConfig.MAPLE_TYPE) || (mapleVersion != ServerConfig.MAPLE_VERSION) ||

                (!maplePatch.equals(ServerConfig.MAPLE_PATCH)))
        {
            c.getSession().close(true);
        }
        else
        {
            c.getSession().write(LoginPacket.getLoginAUTH());
        }
    }
}
