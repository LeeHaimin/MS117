package handling.login.handler;

import java.util.LinkedHashMap;
import java.util.Map;

import client.MapleClient;
import handling.channel.ChannelServer;
import tools.data.input.SeekableLittleEndianAccessor;


public class UpdateCharCards
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (!c.isLoggedIn())
        {
            c.getSession().close(true);
            return;
        }
        Map<Integer, Integer> cids = new LinkedHashMap<>();
        for (int i = 1; i <= 9; i++)
        {
            int charId = slea.readInt();
            if (((!c.login_Auth(charId)) && (charId != 0)) || (ChannelServer.getInstance(c.getChannel()) == null) || (c.getWorld() != 0))
            {
                c.getSession().close(true);
                return;
            }
            cids.put(i, charId);
        }
        c.updateCharacterCards(cids);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\UpdateCharCards.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */