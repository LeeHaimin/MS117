package handling.login.handler;

import client.MapleClient;
import handling.channel.ChannelServer;
import handling.login.LoginServer;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;


public class CharSelectedHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int charId = slea.readInt();
        if ((!c.isLoggedIn()) || (loginFailCount(c)) || (!c.login_Auth(charId)))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((ChannelServer.getInstance(c.getChannel()) == null) || (c.getWorld() != 0))
        {
            c.getSession().close(true);
            return;
        }
        if (c.getIdleTask() != null)
        {
            c.getIdleTask().cancel(true);
        }

        String ip = c.getSessionIPAddress();
        LoginServer.putLoginAuth(charId, ip.substring(ip.indexOf('/') + 1), c.getTempIP(), c.getChannel());
        c.updateLoginState(1, ip);
        c.getSession().write(MaplePacketCreator.getServerIP(c, Integer.parseInt(ChannelServer.getInstance(c.getChannel()).getIP().split(":")[1]), charId));
    }

    private static boolean loginFailCount(MapleClient c)
    {
        c.loginAttempt = ((short) (c.loginAttempt + 1));
        return c.loginAttempt > 5;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\CharSelectedHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */