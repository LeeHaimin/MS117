package handling.login.handler;

import client.MapleClient;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;


public class LicenseRequestHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (slea.readByte() == 1)
        {
            c.getSession().write(LoginPacket.licenseResult());
            c.updateLoginState(0);
        }
        else
        {
            c.getSession().close(true);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\handler\LicenseRequestHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */