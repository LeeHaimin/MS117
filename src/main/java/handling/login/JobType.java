package handling.login;


public enum JobType
{
    反抗者(0, 3000, 931000000, false, false, false), 冒险家(1, 0, 0, false, false, false), 骑士团(2, 1000, 913040000, false, false, false), 战神(3, 2000, 914000000, false, false, true), 龙神(4, 2001, 900090000,
        false, false, true), 双弩精灵(5, 2002, 910150000, false, false, false), 恶魔猎手(6, 3001, 931050310, true, false, false), 幻影(7, 2003, 910000000, false, true, false), 暗影双刀(8, 0, 910000000, false,
        false, false), 米哈尔(9, 5000, 910000000, false, false, false), 夜光(10, 2004, 910000000, false, true, false), 狂龙(11, 6000, 910000000, false, false, false), 萝莉(12, 6001, 910000000, false, false,
        false), 龙的传人(17, 0, 910000000, false, false, false), 尖兵(14, 3002, 910000000, true, false, false), 神之子(15, 10112, 910000000, false, true, false), 林之灵(16, 11212, 910000000, true, false, false
        , false), 终极冒险家(-1, 0, 130000000, false, false, false);

    public final int type;
    public final int jobId;
    public final int mapId;
    public final boolean faceMark;
    public final boolean cape;
    public final boolean bottom;
    public final boolean cap;
    private final boolean 自由市场 = true;

    JobType(int type, int id, int map, boolean faceMark, boolean cape, boolean bottom)
    {
        this.type = type;
        this.jobId = id;
        this.mapId = (this.自由市场 ? 910000000 : map);
        this.faceMark = faceMark;
        this.cape = cape;
        this.bottom = bottom;
        this.cap = false;
    }

    JobType(int type, int id, int map, boolean faceMark, boolean cape, boolean bottom, boolean cap)
    {
        this.type = type;
        this.jobId = id;
        this.mapId = (this.自由市场 ? 910000000 : map);
        this.faceMark = faceMark;
        this.cape = cape;
        this.bottom = bottom;
        this.cap = cap;
    }

    public static JobType getByType(int g)
    {
        for (JobType e : JobType.values())
        {
            if (e.type == g)
            {
                return e;
            }
        }
        return null;
    }

    public static JobType getById(int wzNmae)
    {
        for (JobType e : JobType.values())
        {
            if ((e.jobId == wzNmae) || ((wzNmae == 508) && (e.type == 13)) || ((wzNmae == 1) && (e.type == 8)))
            {
                return e;
            }
        }
        return 冒险家;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\login\JobType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */