package handling.mina;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import client.MapleClient;
import handling.RecvPacketOpcode;
import server.ServerProperties;
import tools.FileoutputUtil;
import tools.HexTool;
import tools.MapleAESOFB;
import tools.MapleLog;
import tools.StringUtil;
import tools.data.input.ByteArrayByteStream;
import tools.data.input.GenericLittleEndianAccessor;

public class MaplePacketDecoder extends CumulativeProtocolDecoder
{
    public static final String DECODER_STATE_KEY = MaplePacketDecoder.class.getName() + ".STATE";
    private static final Logger log = Logger.getLogger(MaplePacketDecoder.class);

    protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception
    {
        DecoderState decoderState = (DecoderState) session.getAttribute(DECODER_STATE_KEY);


        MapleClient client = (MapleClient) session.getAttribute("CLIENT");
        if (decoderState.packetlength == -1)
        {
            if (in.remaining() >= 4)
            {
                int packetHeader = in.getInt();
                if (!client.getReceiveCrypto().checkPacket(packetHeader))
                {
                    session.close(true);
                    return false;
                }
                decoderState.packetlength = MapleAESOFB.getPacketLength(packetHeader);
            }
            else
            {
                log.trace("decode... not enough data");
                return false;
            }
        }
        if (in.remaining() >= decoderState.packetlength)
        {
            byte[] decryptedPacket = new byte[decoderState.packetlength];
            in.get(decryptedPacket, 0, decoderState.packetlength);
            decoderState.packetlength = -1;
            client.getReceiveCrypto().crypt(decryptedPacket);

            out.write(decryptedPacket);
            if (ServerProperties.ShowPacket())
            {
                int packetLen = decryptedPacket.length;
                int pHeader = readFirstShort(decryptedPacket);
                String pHeaderStr = Integer.toHexString(pHeader).toUpperCase();
                pHeaderStr = StringUtil.getLeftPaddedStr(pHeaderStr, '0', 4);
                String op = lookupSend(pHeader);
                String Send = "Send " + op + " [" + pHeaderStr + "] (" + packetLen + ")";
                Send = Send + (client.getPlayer() != null ? " From : " + client.getPlayer().getName() + "\r\n" : "\r\n");
                if (packetLen <= 6000)
                {
                    String SendTo = Send + HexTool.toString(decryptedPacket) + "\r\n" + HexTool.toStringFromAscii(decryptedPacket);
                    if (!ServerProperties.SendPacket(op, pHeaderStr))
                    {
                        MapleLog.getInstance().logWrite(13, SendTo + "\r\n");
                        FileoutputUtil.packetLog("log\\PacketLog.log", SendTo);
                        if ((op.equals("CLOSE_RANGE_ATTACK")) || (op.equals("RANGED_ATTACK")) || (op.equals("MAGIC_ATTACK")))
                        {
                            FileoutputUtil.packetLog("log\\AttackLog.log", SendTo);
                        }
                        else if (op.equals("SPECIAL_MOVE"))
                        {
                            FileoutputUtil.packetLog("log\\SkillBuffLog.log", SendTo);
                        }
                    }
                }
                else
                {
                    System.out.println(Send + HexTool.toString(new byte[]{decryptedPacket[0], decryptedPacket[1]}) + "...\r\n");
                }
            }
            return true;
        }
        log.trace("decode... not enough data to decode (need +" + decoderState.packetlength + ")");
        return false;
    }

    private int readFirstShort(byte[] arr)
    {
        return new GenericLittleEndianAccessor(new ByteArrayByteStream(arr)).readShort();
    }

    private String lookupSend(int val)
    {
        for (RecvPacketOpcode op : RecvPacketOpcode.values())
        {
            if (op.getValue() == val)
            {
                return op.name();
            }
        }
        return "UNKNOWN";
    }

    public static class DecoderState
    {
        public int packetlength = -1;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\mina\MaplePacketDecoder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */