package handling.mina;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import java.util.concurrent.locks.Lock;

import client.MapleClient;
import handling.SendPacketOpcode;
import server.ServerProperties;
import tools.FileoutputUtil;
import tools.HexTool;
import tools.MapleAESOFB;
import tools.MapleLog;
import tools.StringUtil;
import tools.data.input.GenericLittleEndianAccessor;

public class MaplePacketEncoder implements ProtocolEncoder
{
    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception
    {
        MapleClient client = (MapleClient) session.getAttribute("CLIENT");

        if (client != null)
        {
            MapleAESOFB send_crypto = client.getSendCrypto();
            byte[] input = (byte[]) message;
            if (ServerProperties.ShowPacket())
            {
                int packetLen = input.length;
                int pHeader = readFirstShort(input);
                String pHeaderStr = Integer.toHexString(pHeader).toUpperCase();
                pHeaderStr = StringUtil.getLeftPaddedStr(pHeaderStr, '0', 4);
                String op = lookupRecv(pHeader);
                String Recv = "Recv " + op + " [" + pHeaderStr + "] (" + packetLen + ")";
                Recv = Recv + (client.getPlayer() != null ? " Send to : " + client.getPlayer().getName() + "\r\n" : "\r\n");
                if (packetLen <= 50000)
                {
                    if (!ServerProperties.RecvPacket(op, pHeaderStr))
                    {
                        String RecvTo = Recv + HexTool.toString(input) + "\r\n" + HexTool.toStringFromAscii(input);

                        MapleLog.getInstance().logWrite(11, RecvTo + "\r\n");
                        FileoutputUtil.packetLog("log\\PacketLog.log", RecvTo);
                        if ((op.equals("GIVE_BUFF")) || (op.equals("CANCEL_BUFF")))
                        {
                            FileoutputUtil.packetLog("log\\SkillBuffLog.log", RecvTo);
                        }
                    }
                }
                else
                {
                    System.out.println(Recv + HexTool.toString(new byte[]{input[0], input[1]}) + "...\r\n");
                }
            }
            byte[] unencrypted = new byte[input.length];
            System.arraycopy(input, 0, unencrypted, 0, input.length);
            byte[] ret = new byte[unencrypted.length + 4];
            Lock mutex = client.getLock();
            mutex.lock();
            try
            {
                byte[] header = send_crypto.getPacketHeader(unencrypted.length);

                send_crypto.crypt(unencrypted);
                System.arraycopy(header, 0, ret, 0, 4);
            }
            finally
            {
                mutex.unlock();
            }
            System.arraycopy(unencrypted, 0, ret, 4, unencrypted.length);
            out.write(IoBuffer.wrap(ret));
        }
        else
        {
            out.write(IoBuffer.wrap((byte[]) message));
        }
    }

    public void dispose(IoSession session) throws Exception
    {
    }

    private int readFirstShort(byte[] arr)
    {
        return new GenericLittleEndianAccessor(new tools.data.input.ByteArrayByteStream(arr)).readShort();
    }

    private String lookupRecv(int val)
    {
        for (SendPacketOpcode op : SendPacketOpcode.values())
        {
            if (op.getValue() == val)
            {
                return op.name();
            }
        }
        return "UNKNOWN";
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\mina\MaplePacketEncoder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */