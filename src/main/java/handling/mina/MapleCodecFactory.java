package handling.mina;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class MapleCodecFactory implements org.apache.mina.filter.codec.ProtocolCodecFactory
{
    private final ProtocolEncoder encoder;
    private final ProtocolDecoder decoder;

    public MapleCodecFactory()
    {
        this.encoder = new MaplePacketEncoder();
        this.decoder = new MaplePacketDecoder();
    }

    public ProtocolEncoder getEncoder() throws Exception
    {
        return this.encoder;
    }

    public ProtocolDecoder getDecoder() throws Exception
    {
        return this.decoder;
    }

    public ProtocolEncoder getEncoder(IoSession session) throws Exception
    {
        return this.encoder;
    }

    public ProtocolDecoder getDecoder(IoSession session) throws Exception
    {
        return this.decoder;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\mina\MapleCodecFactory.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */