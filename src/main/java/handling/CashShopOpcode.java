package handling;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import constants.ServerConstants;


public enum CashShopOpcode implements WritableIntValueHolder
{
    加载道具栏, 加载礼物, 加载购物车, 更新购物车, 购买道具, 商城送礼, 错误提示, 扩充道具栏, 扩充仓库, 购买角色卡, 扩充项链, 商城到背包, 背包到商城, 删除道具, 道具到期, 换购道具, 购买礼包, 商城送礼包, 购买任务道具, 领奖卡提示, 注册商城, 打开箱子, 商城提示;

    static
    {
        reloadValues();
    }

    private short code = -2;

    CashShopOpcode()
    {
    }

    public static void reloadValues()
    {
        try
        {
            if (ServerConstants.loadop)
            {
                Properties props = new Properties();
                InputStream inputStream = new FileInputStream("config/cashops.properties");
                BufferedReader buff = new BufferedReader(new InputStreamReader(inputStream));
                props.load(buff);
                ExternalCodeTableGetter.populateValues(props, values());
                inputStream.close();
                buff.close();
            }
            else
            {
                ExternalCodeTableGetter.populateValues(getDefaultProperties(), values());
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException("加载 cashops.properties 文件出现错误", e);
        }
    }

    public static Properties getDefaultProperties() throws IOException
    {
        Properties props = new Properties();
        FileInputStream fileInputStream = new FileInputStream("config/cashops.properties");
        BufferedReader buff = new BufferedReader(new InputStreamReader(fileInputStream));
        props.load(buff);
        fileInputStream.close();
        buff.close();
        return props;
    }

    public short getValue()
    {
        return this.code;
    }

    public void setValue(short code)
    {
        this.code = code;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\CashShopOpcode.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */