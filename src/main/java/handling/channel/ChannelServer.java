package handling.channel;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.transport.socket.SocketSessionConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import handling.MapleServerHandler;
import handling.ServerType;
import handling.mina.MapleCodecFactory;
import handling.world.CheaterData;
import scripting.event.EventScriptManager;
import server.ServerProperties;
import server.events.MapleCoconut;
import server.events.MapleEvent;
import server.events.MapleEventType;
import server.events.MapleOxQuiz;
import server.events.MapleSnowball;
import server.life.PlayerNPC;
import server.maps.AramiaFireWorks;
import server.maps.MapleMap;
import server.maps.MapleMapFactory;
import server.shops.HiredMerchant;
import server.shops.HiredMerchantSave;
import server.squad.MapleSquad;
import server.squad.MapleSquadType;
import tools.MaplePacketCreator;

public class ChannelServer
{
    private static final Map<Integer, ChannelServer> instances = new HashMap<>();
    private static final Logger log = Logger.getLogger(ChannelServer.class);
    private static final short DEFAULT_PORT = 7575;
    public static long serverStartTime;
    private final AramiaFireWorks works = new AramiaFireWorks();
    private final Map<MapleSquadType, MapleSquad> mapleSquads = new tools.ConcurrentEnumMap(MapleSquadType.class);
    private final Map<Integer, HiredMerchant> merchants = new HashMap<>();
    private final List<PlayerNPC> playerNPCs = new LinkedList<>();
    private final Map<MapleEventType, MapleEvent> events = new EnumMap(MapleEventType.class);
    private final int channel;
    private final MapleMapFactory mapFactory;
    Connection shareCon;
    private int running_MerchantID = 0;
    private int flags = 0;
    private int doubleExp = 1;
    private int sharePrice = 0;
    private boolean shutdown = false;
    private boolean finishedShutdown = false;
    private boolean MegaphoneMuteState = false;
    private boolean adminOnly = false;
    private boolean canPvp = false;
    private boolean shieldWardAll = false;
    private boolean autoPoints = false;
    private boolean checkSp = false;
    private boolean checkCash = false;
    private boolean useMapScript = false;
    private ReentrantReadWriteLock merchLock = null;
    private ReentrantReadWriteLock.ReadLock mcReadLock = null;
    private ReentrantReadWriteLock.WriteLock mcWriteLock = null;
    private int eventmap = -1;
    private int expRate;
    private int mesoRate;
    private int dropRate;
    private int cashRate;
    private int traitRate;
    private int stateRate;
    private int statLimit;
    private int createGuildCost;
    private int globalRate;
    private int autoPaoDian;
    private int autoGain;
    private int autoNx;
    private int merchantTime;
    private short port;
    private String serverMessage;
    private String ip;
    private String serverName;
    private PlayerStorage players;
    private IoAcceptor acceptor;
    private EventScriptManager eventSM;
    private String ShopPack;

    private ChannelServer(int channel)
    {
        this.channel = channel;
        this.mapFactory = new MapleMapFactory(channel);

        this.merchLock = new ReentrantReadWriteLock(true);
        this.mcReadLock = this.merchLock.readLock();
        this.mcWriteLock = this.merchLock.writeLock();
    }

    public static Set<Integer> getAllInstance()
    {
        return new HashSet(instances.keySet());
    }

    public static void startChannel_Main()
    {
        serverStartTime = System.currentTimeMillis();
        int ch = Integer.parseInt(ServerProperties.getProperty("channel.count", "0"));
        if (ch > 10)
        {
            ch = 10;
        }
        for (int i = 0; i < ch; i++)
        {
            newInstance(i + 1).run_startup_configurations();
        }
    }

    public void run_startup_configurations()
    {
        setChannel(this.channel);
        try
        {
            this.expRate = Integer.parseInt(ServerProperties.getProperty("world.exp", "10"));
            this.mesoRate = Integer.parseInt(ServerProperties.getProperty("world.meso", "10"));
            this.dropRate = Integer.parseInt(ServerProperties.getProperty("world.drop", "3"));
            this.cashRate = Integer.parseInt(ServerProperties.getProperty("world.cash", "1"));
            this.globalRate = Integer.parseInt(ServerProperties.getProperty("world.globalRate", "1"));
            this.traitRate = Integer.parseInt(ServerProperties.getProperty("world.trait", "1"));
            this.stateRate = Integer.parseInt(ServerProperties.getProperty("world.state", "4"));
            this.statLimit = Integer.parseInt(ServerProperties.getProperty("world.statLimit", "999"));
            this.autoNx = Integer.parseInt(ServerProperties.getProperty("world.autoNx", "10"));
            this.autoGain = Integer.parseInt(ServerProperties.getProperty("world.autoGain", "10"));
            this.autoPaoDian = Integer.parseInt(ServerProperties.getProperty("world.autoPaoDian", "1"));
            this.createGuildCost = Integer.parseInt(ServerProperties.getProperty("world.createGuildCost", "10000000"));
            this.merchantTime = Integer.parseInt(ServerProperties.getProperty("world.merchantTime", "24"));

            this.serverMessage = "aaaaaaa";

            this.serverName = "怀旧岛V117-江浩生日  反编译源码版";
            this.flags = Integer.parseInt(ServerProperties.getProperty("world.flags", "0"));
            this.adminOnly = Boolean.parseBoolean(ServerProperties.getProperty("world.admin", "false"));
            this.canPvp = Boolean.parseBoolean(ServerProperties.getProperty("world.canPvp", "false"));
            this.shieldWardAll = Boolean.parseBoolean(ServerProperties.getProperty("world.shieldWardAll", "false"));
            this.checkSp = Boolean.parseBoolean(ServerProperties.getProperty("world.checkSp", "true"));
            this.checkCash = Boolean.parseBoolean(ServerProperties.getProperty("world.checkCash", "true"));
            this.useMapScript = Boolean.parseBoolean(ServerProperties.getProperty("world.useMapScript", "false"));
            this.autoPoints = Boolean.parseBoolean(ServerProperties.getProperty("world.autoPoints", "false"));
            this.eventSM = new EventScriptManager(this, ServerProperties.getProperty("channel.events").split(","));
            this.port = Short.parseShort(ServerProperties.getProperty("channel.port" + this.channel, String.valueOf(7575 + this.channel)));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        this.ip = (ServerProperties.getProperty("channel.interface", constants.ServerConstants.HOST) + ":" + this.port);

        IoBuffer.setUseDirectBuffer(false);
        IoBuffer.setAllocator(new org.apache.mina.core.buffer.SimpleBufferAllocator());

        this.acceptor = new org.apache.mina.transport.socket.nio.NioSocketAcceptor();
        this.acceptor.getFilterChain().addLast("codec", new org.apache.mina.filter.codec.ProtocolCodecFilter(new MapleCodecFactory()));
        this.acceptor.getSessionConfig().setIdleTime(org.apache.mina.core.session.IdleStatus.BOTH_IDLE, 30);
        this.players = new PlayerStorage(this.channel);
        getShopPack();
        loadEvents();
        loadShare();
        try
        {
            this.acceptor.setHandler(new MapleServerHandler(this.channel, ServerType.频道服务器));
            this.acceptor.bind(new InetSocketAddress(this.port));
            ((SocketSessionConfig) this.acceptor.getSessionConfig()).setTcpNoDelay(true);
            log.info("频道: " + this.channel + " 监听端口: " + this.port);
            this.eventSM.init();
        }
        catch (IOException e)
        {
            log.info("绑定端口: " + this.port + " 失败 (ch: " + getChannel() + ")" + e);
        }
    }

    public static ChannelServer newInstance(int channel)
    {
        return new ChannelServer(channel);
    }

    public String getShopPack()
    {
        if (this.ShopPack != null)
        {
            return this.ShopPack;
        }
        Properties props = new Properties();
        try
        {
            FileInputStream is = new FileInputStream("CashPack.txt");
            props.load(is);
            is.close();
        }
        catch (IOException ex)
        {
            log.error("无法加载 CashPack.txt 的商城信息数据文件.");
        }
        this.ShopPack = props.getProperty("pack");
        return this.ShopPack;
    }

    public void loadEvents()
    {
        if (!this.events.isEmpty())
        {
            return;
        }
        this.events.put(MapleEventType.CokePlay, new MapleCoconut(this.channel, MapleEventType.CokePlay));
        this.events.put(MapleEventType.Coconut, new MapleCoconut(this.channel, MapleEventType.Coconut));
        this.events.put(MapleEventType.Fitness, new server.events.MapleFitness(this.channel, MapleEventType.Fitness));
        this.events.put(MapleEventType.OlaOla, new server.events.MapleOla(this.channel, MapleEventType.OlaOla));
        this.events.put(MapleEventType.OxQuiz, new MapleOxQuiz(this.channel, MapleEventType.OxQuiz));
        this.events.put(MapleEventType.Snowball, new MapleSnowball(this.channel, MapleEventType.Snowball));
        this.events.put(MapleEventType.Survival, new server.events.MapleSurvival(this.channel, MapleEventType.Survival));
    }

    public void loadShare()
    {
        if ((this.channel != 1) || (this.finishedShutdown))
        {
            return;
        }
        this.shareCon = database.DatabaseConnection.getConnection();
        try
        {
            PreparedStatement ps = this.shareCon.prepareStatement("SELECT * FROM shares WHERE channelid = ?");
            ps.setInt(1, 1);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                this.sharePrice = rs.getInt("currentprice");
            }
            else
            {
                this.sharePrice = 35;
//                throw new RuntimeException("[EXCEPTION] 无法加载股票数据.");
            }
            log.info("目前的股票价格: " + this.sharePrice);
            rs.close();
            ps.close();
        }
        catch (SQLException e)
        {
            log.error("ERROR Load Shares", e);
        }
    }

    public int getChannel()
    {
        return this.channel;
    }

    public void setChannel(int channel)
    {
        instances.put(channel, this);
        handling.login.LoginServer.addChannel(channel);
    }

    public static Set<Integer> getChannelServer()
    {
        return new HashSet(instances.keySet());
    }

    public static int getChannelCount()
    {
        return instances.size();
    }

    public static Map<Integer, Integer> getChannelLoad()
    {
        Map<Integer, Integer> ret = new HashMap<>();
        for (ChannelServer cs : instances.values())
        {
            ret.put(cs.getChannel(), cs.getConnectedClients());
        }
        return ret;
    }

    public int getConnectedClients()
    {
        return getPlayerStorage().getConnectedClients();
    }

    public PlayerStorage getPlayerStorage()
    {
        if (this.players == null)
        {
            this.players = new PlayerStorage(this.channel);
        }
        return this.players;
    }

    public static MapleCharacter getCharacterById(int id)
    {
        for (ChannelServer cserv_ : ChannelServer.getAllInstances())
        {
            MapleCharacter ret = cserv_.getPlayerStorage().getCharacterById(id);
            if (ret != null)
            {
                return ret;
            }
        }
        return null;
    }

    public static ArrayList<ChannelServer> getAllInstances()
    {
        return new ArrayList(instances.values());
    }

    public static MapleCharacter getCharacterByName(String name)
    {
        for (ChannelServer cserv_ : ChannelServer.getAllInstances())
        {
            MapleCharacter ret = cserv_.getPlayerStorage().getCharacterByName(name);
            if (ret != null)
            {
                return ret;
            }
        }
        return null;
    }

    public void shutdown()
    {
        if (this.finishedShutdown)
        {
            return;
        }
        broadcastPacket(MaplePacketCreator.serverNotice(0, "游戏即将关闭维护..."));


        this.shutdown = true;
        System.out.println("频道 " + this.channel + " 正在清理活动脚本...");

        this.eventSM.cancel();

        System.out.println("频道 " + this.channel + " 正在保存所有角色数据...");

        getPlayerStorage().disconnectAll();

        System.out.println("频道 " + this.channel + " 解除绑定端口...");

        this.acceptor.unbind();
        this.acceptor = null;


        instances.remove(this.channel);
        setFinishShutdown();
    }

    public void broadcastPacket(byte[] data)
    {
        getPlayerStorage().broadcastPacket(data);
    }

    public void setFinishShutdown()
    {
        this.finishedShutdown = true;
        log.info("频道 " + this.channel + " 已关闭完成.");
    }

    public void unbind()
    {
        this.acceptor.unbind();
    }

    public boolean hasFinishedShutdown()
    {
        return this.finishedShutdown;
    }

    public void addPlayer(MapleCharacter chr)
    {
        getPlayerStorage().registerPlayer(chr);
    }

    public void removePlayer(MapleCharacter chr)
    {
        getPlayerStorage().deregisterPlayer(chr);
    }

    public void removePlayer(int idz, String namez)
    {
        getPlayerStorage().deregisterPlayer(idz, namez);
    }

    public String getServerMessage()
    {
        return this.serverMessage;
    }

    public void setServerMessage(String newMessage)
    {
        this.serverMessage = newMessage;
        broadcastPacket(MaplePacketCreator.serverMessage(this.serverMessage));
    }

    public String getIP()
    {
        return this.ip;
    }

    public boolean isShutdown()
    {
        return this.shutdown;
    }

    public int getLoadedMaps()
    {
        return this.mapFactory.getLoadedMaps();
    }

    public EventScriptManager getEventSM()
    {
        return this.eventSM;
    }

    public void reloadEvents()
    {
        this.eventSM.cancel();
        this.eventSM = new EventScriptManager(this, ServerProperties.getProperty("channel.events").split(","));
        this.eventSM.init();
    }

    public int getExpRate()
    {
        return this.expRate * getDoubleExp();
    }

    public void setExpRate(int expRate)
    {
        this.expRate = expRate;
    }

    public int getDoubleExp()
    {
        if ((this.doubleExp < 0) || (this.doubleExp > 2))
        {
            return 1;
        }
        return this.doubleExp;
    }

    public void setDoubleExp(int doubleExp)
    {
        if ((doubleExp < 0) || (doubleExp > 2))
        {
            this.doubleExp = 1;
        }
        else
        {
            this.doubleExp = doubleExp;
        }
    }

    public int getCashRate()
    {
        return this.cashRate;
    }

    public void setCashRate(int cashRate)
    {
        this.cashRate = cashRate;
    }

    public int getMesoRate()
    {
        return this.mesoRate * getDoubleExp();
    }

    public void setMesoRate(int mesoRate)
    {
        this.mesoRate = mesoRate;
    }

    public int getDropRate()
    {
        return this.dropRate * getDoubleExp();
    }

    public void setDropRate(int dropRate)
    {
        this.dropRate = dropRate;
    }

    public int getGlobalRate()
    {
        if (this.globalRate <= 0)
        {
            return 1;
        }
        return this.globalRate;
    }

    public void setGlobalRate(int rate)
    {
        this.globalRate = rate;
    }

    public int getStatLimit()
    {
        return this.statLimit;
    }

    public void setStatLimit(int limit)
    {
        this.statLimit = limit;
    }

    public Map<MapleSquadType, MapleSquad> getAllSquads()
    {
        return Collections.unmodifiableMap(this.mapleSquads);
    }

    public MapleSquad getMapleSquad(String type)
    {
        return getMapleSquad(MapleSquadType.valueOf(type.toLowerCase()));
    }

    public MapleSquad getMapleSquad(MapleSquadType type)
    {
        return this.mapleSquads.get(type);
    }

    public boolean addMapleSquad(MapleSquad squad, String type)
    {
        MapleSquadType types = MapleSquadType.valueOf(type.toLowerCase());
        if ((types != null) && (!this.mapleSquads.containsKey(types)))
        {
            this.mapleSquads.put(types, squad);
            squad.scheduleRemoval();
            return true;
        }
        return false;
    }

    public boolean removeMapleSquad(MapleSquadType types)
    {
        if ((types != null) && (this.mapleSquads.containsKey(types)))
        {
            this.mapleSquads.remove(types);
            return true;
        }
        return false;
    }

    public int closeAllMerchant()
    {
        int ret = 0;
        this.mcWriteLock.lock();
        HiredMerchant hm;
        try
        {
            Iterator<Map.Entry<Integer, HiredMerchant>> merchants_ = this.merchants.entrySet().iterator();
            while (merchants_.hasNext())
            {
                hm = (HiredMerchant) ((Map.Entry) merchants_.next()).getValue();
                HiredMerchantSave.QueueShopForSave(hm);
                hm.getMap().removeMapObject(hm);
                merchants_.remove();
                ret++;
            }
        }
        finally
        {
            this.mcWriteLock.unlock();
        }

        for (int i = 910000001; i <= 910000022; i++)
        {
            for (server.maps.MapleMapObject mmo : this.mapFactory.getMap(i).getAllHiredMerchantsThreadsafe())
            {
                HiredMerchantSave.QueueShopForSave((HiredMerchant) mmo);
                ret++;
            }
        }
        return ret;
    }

    public void closeAllMerchants()
    {
        int ret = 0;
        long Start = System.currentTimeMillis();
        this.mcWriteLock.lock();
        try
        {
            Iterator<Map.Entry<Integer, HiredMerchant>> hmit = this.merchants.entrySet().iterator();
            while (hmit.hasNext())
            {
                ((HiredMerchant) ((Map.Entry) hmit.next()).getValue()).closeShop(true, false);
                hmit.remove();
                ret++;
            }
        }
        catch (Exception e)
        {
            log.error("关闭雇佣商店出现错误..." + e);
        }
        finally
        {
            this.mcWriteLock.unlock();
        }
        log.info("频道 " + this.channel + " 共保存雇佣商店: " + ret + " | 耗时: " + (System.currentTimeMillis() - Start) + " 毫秒.");
    }

    public int addMerchant(HiredMerchant hMerchant)
    {
        this.mcWriteLock.lock();
        try
        {
            this.running_MerchantID += 1;
            this.merchants.put(this.running_MerchantID, hMerchant);
            return this.running_MerchantID;
        }
        finally
        {
            this.mcWriteLock.unlock();
        }
    }

    public void removeMerchant(HiredMerchant hMerchant)
    {
        merchLock.writeLock().lock();

        try
        {
            merchants.remove(hMerchant.getStoreId());
        }
        finally
        {
            merchLock.writeLock().unlock();
        }
    }

    public boolean containsMerchant(int accId)
    {
        boolean contains = false;
        this.mcReadLock.lock();
        try
        {
            for (HiredMerchant hm : this.merchants.values())
            {
                if (hm.getOwnerAccId() == accId)
                {
                    contains = true;
                    break;
                }
            }
        }
        finally
        {
            this.mcReadLock.unlock();
        }
        return contains;
    }

    public boolean containsMerchant(int accId, int chrId)
    {
        boolean contains = false;
        this.mcReadLock.lock();
        try
        {
            for (HiredMerchant hm : this.merchants.values())
            {
                if ((hm.getOwnerAccId() == accId) && (hm.getOwnerId() == chrId))
                {
                    contains = true;
                    break;
                }
            }
        }
        finally
        {
            this.mcReadLock.unlock();
        }
        return contains;
    }

    public List<HiredMerchant> searchMerchant(int itemSearch)
    {
        List<HiredMerchant> list = new LinkedList<>();
        this.mcReadLock.lock();
        try
        {
            for (HiredMerchant hm : this.merchants.values())
            {
                if (hm.searchItem(itemSearch).size() > 0)
                {
                    list.add(hm);
                }
            }
        }
        finally
        {
            this.mcReadLock.unlock();
        }
        return list;
    }

    public HiredMerchant getHiredMerchants(int accId, int chrId)
    {
        this.mcReadLock.lock();
        try
        {
            for (HiredMerchant hm : this.merchants.values())
            {
                if ((hm.getOwnerAccId() == accId) && (hm.getOwnerId() == chrId))
                {
                    return hm;
                }
            }
        }
        finally
        {
            this.mcReadLock.unlock();
        }
        return null;
    }

    public void toggleMegaphoneMuteState()
    {
        this.MegaphoneMuteState = (!this.MegaphoneMuteState);
    }

    public boolean getMegaphoneMuteState()
    {
        return this.MegaphoneMuteState;
    }

    public int getEvent()
    {
        return this.eventmap;
    }

    public void setEvent(int ze)
    {
        this.eventmap = ze;
    }

    public MapleEvent getEvent(MapleEventType t)
    {
        return this.events.get(t);
    }

    public Collection<PlayerNPC> getAllPlayerNPC()
    {
        return this.playerNPCs;
    }

    public void addPlayerNPC(PlayerNPC npc)
    {
        if (this.playerNPCs.contains(npc))
        {
            return;
        }
        this.playerNPCs.add(npc);
        getMapFactory().getMap(npc.getMapId()).addMapObject(npc);
    }

    public MapleMapFactory getMapFactory()
    {
        return this.mapFactory;
    }

    public void removePlayerNPC(PlayerNPC npc)
    {
        if (this.playerNPCs.contains(npc))
        {
            this.playerNPCs.remove(npc);
            getMapFactory().getMap(npc.getMapId()).removeMapObject(npc);
        }
    }

    public String getServerName()
    {
        return this.serverName;
    }

    public void setServerName(String sn)
    {
        this.serverName = sn;
    }

    public String getTrueServerName()
    {
        return this.serverName.substring(0, this.serverName.length() - 3);
    }

    public int getPort()
    {
        return this.port;
    }

    public void setShutdown()
    {
        this.shutdown = true;
        log.info("频道 " + this.channel + " 正在关闭和保存雇佣商店数据信息...");
    }

    public boolean isAdminOnly()
    {
        return this.adminOnly;
    }

    public int getTempFlag()
    {
        return this.flags;
    }

    public List<CheaterData> getCheaters()
    {
        List<CheaterData> cheaters = getPlayerStorage().getCheaters();
        Collections.sort(cheaters);
        return cheaters;
    }

    public List<CheaterData> getReports()
    {
        List<CheaterData> cheaters = getPlayerStorage().getReports();
        Collections.sort(cheaters);
        return cheaters;
    }

    public void broadcastMessage(byte[] message)
    {
        broadcastPacket(message);
    }

    public void broadcastSmega(byte[] message)
    {
        broadcastSmegaPacket(message);
    }

    public void broadcastSmegaPacket(byte[] data)
    {
        getPlayerStorage().broadcastSmegaPacket(data);
    }

    public void broadcastGMMessage(byte[] message)
    {
        broadcastGMPacket(message);
    }

    public void broadcastGMPacket(byte[] data)
    {
        getPlayerStorage().broadcastGMPacket(data);
    }

    public void startMapEffect(String msg, int itemId)
    {
        startMapEffect(msg, itemId, 10);
    }

    public void startMapEffect(String msg, int itemId, int time)
    {
        for (MapleMap load : getMapFactory().getAllMaps())
        {
            if (load.getCharactersSize() > 0)
            {
                load.startMapEffect(msg, itemId, time);
            }
        }
    }

    public AramiaFireWorks getFireWorks()
    {
        return this.works;
    }

    public int getTraitRate()
    {
        return this.traitRate;
    }

    public void saveAll()
    {
        int nos = 0;
        for (MapleCharacter chr : this.players.getAllCharacters())
        {
            if (chr != null)
            {
                nos++;
                chr.saveToDB(false, false);
            }
        }
        log.info("[自动保存] 已经将频道 " + this.channel + " 的 " + nos + " 个玩家的数据自动保存到数据中.");
    }

    public int getAutoGain()
    {
        return this.autoGain;
    }

    public void setAutoGain(int rate)
    {
        this.autoGain = rate;
    }

    public void AutoGain(int rate)
    {
    }

    public int getAutoNx()
    {
        return this.autoNx;
    }

    public void setAutoNx(int rate)
    {
        this.autoNx = rate;
    }

    public void AutoNx(int rate)
    {
        this.mapFactory.getMap(741000000).AutoNx(rate, isAutoPoints());
    }

    public boolean isAutoPoints()
    {
        return this.autoPoints;
    }

    public int getAutoPaoDian()
    {
        return this.autoPaoDian;
    }

    public void setAutoPaoDian(int rate)
    {
        this.autoPaoDian = rate;
    }

    public void AutoPaoDian()
    {
        AutoPaoDian(1);
    }

    public void AutoPaoDian(int rate)
    {
        for (MapleCharacter chr : this.players.getAllCharacters())
        {
            if (chr != null)
            {
                chr.gainGamePoints(rate);
            }
        }
    }

    public boolean isCanPvp()
    {
        return this.canPvp;
    }

    public boolean isShieldWardAll()
    {
        return this.shieldWardAll;
    }

    public void setShieldWardAll(boolean all)
    {
        this.shieldWardAll = all;
    }

    public int getStateRate()
    {
        return this.stateRate;
    }

    public void setStateRate(int stateRate)
    {
        this.stateRate = stateRate;
    }

    public int getCreateGuildCost()
    {
        return this.createGuildCost;
    }

    public boolean isConnected(String name)
    {
        return getPlayerStorage().getCharacterByName(name) != null;
    }

    public int getSharePrice()
    {
        return this.sharePrice;
    }

    public void increaseShare(int share)
    {
        if ((this.channel != 1) || (this.finishedShutdown))
        {
            return;
        }
        this.sharePrice += share;
        try
        {
            PreparedStatement ps = this.shareCon.prepareStatement("UPDATE shares SET currentprice = ? WHERE channelid = 1");
            ps.setInt(1, this.sharePrice);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            log.error("ERROR Increase Shares", e);
        }
    }

    public void decreaseShare(int share)
    {
        if ((this.channel != 1) || (this.finishedShutdown))
        {
            return;
        }
        this.sharePrice -= share;
        if (this.sharePrice < 0)
        {
            this.sharePrice = 0;
        }
        try
        {
            PreparedStatement ps = this.shareCon.prepareStatement("UPDATE shares SET currentprice = ? WHERE channelid = 1");
            ps.setInt(1, this.sharePrice);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            log.error("ERROR Decrease Shares", e);
        }
    }

    public void saveShares()
    {
        if ((this.channel != 1) || (this.finishedShutdown))
        {
            return;
        }
        try
        {
            PreparedStatement ps = this.shareCon.prepareStatement("UPDATE shares SET currentprice = ? WHERE channelid = 1");
            ps.setInt(1, this.sharePrice);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            log.error("ERROR Save Shares", e);
        }
    }

    public int getMerchantTime()
    {
        return this.merchantTime;
    }

    public boolean isCheckSp()
    {
        return this.checkSp;
    }

    public boolean isCheckCash()
    {
        return this.checkCash;
    }

    public boolean isUseMapScript()
    {
        return this.useMapScript;
    }

    public void createBoss(int map, String monsterID, int x, int y, int hp)
    {
        mapFactory.getMap(map).showMonster(monsterID, x, y, hp);
    }

    public void autoExp(int base)
    {
        for (MapleCharacter mc : getInstance(1).getPlayerStorage().getAllCharacters())
        {
            if (mc.getMap().getId() == 910000000)
            {
                mc.gainExp(base * 10000000, true, false, false);
            }
        }
    }

    public static ChannelServer getInstance(int channel)
    {
        return instances.get(channel);
    }
}