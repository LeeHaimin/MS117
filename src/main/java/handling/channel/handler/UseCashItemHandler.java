package handling.channel.handler;

import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.MapleStat;
import client.MonsterFamiliar;
import client.PlayerStats;
import client.Skill;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePet;
import client.inventory.PetFlag;
import constants.GameConstants;
import handling.channel.ChannelServer;
import handling.world.WorldBroadcastService;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.PredictCardFactory;
import server.Randomizer;
import server.StructFamiliar;
import server.StructItemOption;
import server.maps.FieldLimitType;
import server.maps.MapleMap;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.InventoryPacket;
import tools.packet.MTSCSPacket;
import tools.packet.PetPacket;

public class UseCashItemHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.inPVP()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.updateTick(slea.readInt());
        chr.setScrolledPosition((short) 0);
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        int itemType = itemId / 10000;
        Item toUse = chr.getInventory(MapleInventoryType.CASH).getItem(slot);
        if ((toUse == null) || (toUse.getItemId() != itemId) || (toUse.getQuantity() < 1) || (chr.hasBlockedInventory()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.isAdmin())
        {
            chr.dropMessage(5, "使用商城道具 物品ID: " + itemId + " 物品类型: " + itemType);
        }
        boolean used = false;
        boolean cc = false;
        Skill skillSPTo;
        Skill skillSPFrom;
        int theJob;
        boolean tvEar;
        List<String> tvMessages;
        int buff;
        MapleCharacter mChar;
        String nName;
        int mesars;
        String name;
        String otherName;
        int cardId;
        int commentId;
        server.PredictCardFactory.PredictCardComment Comment;
        int love;
        List<String> messages;
        boolean ear;
        long expire;
        Item item;
        short flag;
        switch (itemType)
        {
            case 504:
                if ((itemId == 5043000) || (itemId == 5043001))
                {
                    short questid = slea.readShort();
                    int npcid = slea.readInt();
                    server.quest.MapleQuest quest = server.quest.MapleQuest.getInstance(questid);
                    if ((chr.getQuest(quest).getStatus() == 1) && (quest.canComplete(chr, npcid)))
                    {
                        int mapId = server.life.MapleLifeFactory.getNPCLocation(npcid);
                        if (mapId != -1)
                        {
                            MapleMap map = c.getChannelServer().getMapFactory().getMap(mapId);
                            if ((map.containsNPC(npcid)) && (!FieldLimitType.VipRock.check(chr.getMap().getFieldLimit())) && (!FieldLimitType.VipRock.check(map.getFieldLimit())) && (!chr.isInBlockedMap()))
                            {
                                chr.changeMap(map, map.getPortal(0));
                            }
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(1, "使用道具出现未知的错误.");
                        }
                    }
                }
                else if (itemId == 5042000)
                {
                    MapleMap map = c.getChannelServer().getMapFactory().getMap(701000200);
                    chr.changeMap(map, map.getPortal(0));
                    used = true;
                }
                else if (itemId == 5040005)
                {
                    chr.dropMessage(5, "无法使用这个道具.");
                }
                else
                {
                    used = InventoryHandler.UseTeleRock(slea, c, itemId);
                }
                break;
            case 505:
                int apfrom;
                int statLimit;
                int job;
                if (itemId == 5050000)
                {
                    Map<MapleStat, Long> statupdate = new EnumMap(MapleStat.class);
                    int apto = (int) slea.readLong();
                    apfrom = (int) slea.readLong();
                    statLimit = c.getChannelServer().getStatLimit();
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(5, "洗能力点 apto: " + apto + " apfrom: " + apfrom);
                    }
                    if (apto != apfrom)
                    {

                        job = chr.getJob();
                        PlayerStats playerst = chr.getStat();
                        used = true;
                        switch (apto)
                        {
                            case 64:
                                if (playerst.getStr() >= statLimit)
                                {
                                    used = false;
                                }
                                break;
                            case 128:
                                if (playerst.getDex() >= statLimit)
                                {
                                    used = false;
                                }
                                break;
                            case 256:
                                if (playerst.getInt() >= statLimit)
                                {
                                    used = false;
                                }
                                break;
                            case 512:
                                if (playerst.getLuk() >= statLimit)
                                {
                                    used = false;
                                }
                                break;
                            case 2048:
                                if (playerst.getMaxHp() >= chr.getMaxHpForSever())
                                {
                                    used = false;
                                }
                                break;
                            case 8192:
                                if (playerst.getMaxMp() >= chr.getMaxMpForSever())
                                {
                                    used = false;
                                }
                                break;
                        }
                        switch (apfrom)
                        {
                            case 64:
                                if (playerst.getStr() <= 4)
                                {
                                    used = false;
                                }
                                break;
                            case 128:
                                if (playerst.getDex() <= 4)
                                {
                                    used = false;
                                }
                                break;
                            case 256:
                                if (playerst.getInt() <= 4)
                                {
                                    used = false;
                                }
                                break;
                            case 512:
                                if (playerst.getLuk() <= 4)
                                {
                                    used = false;
                                }
                                break;
                            case 2048:
                                if ((chr.getHpApUsed() <= 0) || (chr.getHpApUsed() >= 10000))
                                {
                                    used = false;
                                }
                                break;
                            case 8192:
                                if ((chr.getHpApUsed() <= 0) || (chr.getHpApUsed() >= 10000) || (GameConstants.isNotMpJob(job)))
                                {
                                    used = false;
                                }
                                break;
                        }
                        if (used)
                        {
                            long toSet;
                            switch (apto)
                            {
                                case 64:
                                    toSet = playerst.getStr() + 1;
                                    playerst.setStr((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.力量, toSet);
                                    break;

                                case 128:
                                    toSet = playerst.getDex() + 1;
                                    playerst.setDex((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.敏捷, toSet);
                                    break;

                                case 256:
                                    toSet = playerst.getInt() + 1;
                                    playerst.setInt((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.智力, toSet);
                                    break;

                                case 512:
                                    toSet = playerst.getLuk() + 1;
                                    playerst.setLuk((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.运气, toSet);
                                    break;

                                case 2048:
                                    int maxhp = playerst.getMaxHp();
                                    if (GameConstants.is新手职业(job))
                                    {
                                        maxhp += Randomizer.rand(4, 8);
                                    }
                                    else if (GameConstants.is恶魔复仇者(job))
                                    {
                                        maxhp += 30;
                                    }
                                    else if (((job >= 100) && (job <= 132)) || ((job >= 3200) && (job <= 3212)) || ((job >= 1100) && (job <= 1112)) || ((job >= 3100) && (job <= 3112)) || ((job >= 5100) && (job <= 5112)))
                                    {
                                        maxhp += Randomizer.rand(36, 42);
                                    }
                                    else if (((job >= 200) && (job <= 232)) || (GameConstants.is龙神(job)) || ((job >= 1200) && (job <= 1212)) || ((job >= 2700) && (job <= 2712)))
                                    {
                                        maxhp += Randomizer.rand(10, 12);
                                    }
                                    else if (((job >= 300) && (job <= 322)) || ((job >= 400) && (job <= 434)) || ((job >= 1300) && (job <= 1312)) || ((job >= 1400) && (job <= 1412)) || ((job >= 3300) && (job <= 3312)) || ((job >= 2300) && (job <= 2312)) || ((job >= 2400) && (job <= 2412)))
                                    {
                                        maxhp += Randomizer.rand(14, 18);
                                    }
                                    else if (((job >= 510) && (job <= 512)) || ((job >= 580) && (job <= 582)) || ((job >= 1510) && (job <= 1512)))
                                    {
                                        maxhp += Randomizer.rand(24, 28);
                                    }
                                    else if (((job >= 500) && (job <= 532)) || ((job >= 590) && (job <= 592)) || (GameConstants.is龙的传人(job)) || ((job >= 3500) && (job <= 3512)) || (job == 1500))
                                    {
                                        maxhp += Randomizer.rand(16, 20);
                                    }
                                    else if ((job >= 2000) && (job <= 2112))
                                    {
                                        maxhp += Randomizer.rand(34, 38);
                                    }
                                    else
                                    {
                                        maxhp += Randomizer.rand(16, 20);
                                    }
                                    maxhp = Math.min(chr.getMaxHpForSever(), Math.abs(maxhp));
                                    chr.setHpApUsed((short) (chr.getHpApUsed() + 1));
                                    playerst.setMaxHp(maxhp, chr);
                                    statupdate.put(MapleStat.MAXHP, (long) maxhp);
                                    break;
                                case 8192:
                                    int maxmp = playerst.getMaxMp();
                                    if (GameConstants.is新手职业(job))
                                    {
                                        maxmp += Randomizer.rand(6, 8);
                                    }
                                    else
                                    {
                                        if (GameConstants.isNotMpJob(job)) return;
                                        if (((job >= 100) && (job <= 132)) || ((job >= 1100) && (job <= 1112)) || ((job >= 2000) && (job <= 2112)) || ((job >= 5100) && (job <= 5112)))
                                        {
                                            maxmp += Randomizer.rand(4, 9);
                                        }
                                        else if (((job >= 200) && (job <= 232)) || (GameConstants.is龙神(job)) || ((job >= 3200) && (job <= 3212)) || ((job >= 1200) && (job <= 1212)) || ((job >= 2700) && (job <= 2712)))
                                        {
                                            maxmp += Randomizer.rand(32, 36);
                                        }
                                        else if (((job >= 300) && (job <= 322)) || ((job >= 400) && (job <= 434)) || ((job >= 500) && (job <= 592)) || ((job >= 3200) && (job <= 3212)) || ((job >= 3500) && (job <= 3512)) || ((job >= 1300) && (job <= 1312)) || ((job >= 1400) && (job <= 1412)) || ((job >= 1500) && (job <= 1512)) || ((job >= 2300) && (job <= 2312)) || ((job >= 2400) && (job <= 2412)))
                                        {
                                            maxmp += Randomizer.rand(8, 10);
                                        }
                                        else maxmp += Randomizer.rand(6, 8);
                                    }
                                    if (GameConstants.isNotMpJob(job))
                                    {
                                        chr.dropMessage(1, "该职业无法使用.");
                                    }
                                    else
                                    {
                                        maxmp = Math.min(chr.getMaxMpForSever(), Math.abs(maxmp));
                                        chr.setHpApUsed((short) (chr.getHpApUsed() + 1));
                                        playerst.setMaxMp(maxmp, chr);
                                        statupdate.put(MapleStat.MAXMP, (long) maxmp);
                                    }
                                    break;
                            }
                            switch (apfrom)
                            {
                                case 64:
                                    toSet = playerst.getStr() - 1;
                                    playerst.setStr((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.力量, toSet);
                                    break;

                                case 128:
                                    toSet = playerst.getDex() - 1;
                                    playerst.setDex((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.敏捷, toSet);
                                    break;

                                case 256:
                                    toSet = playerst.getInt() - 1;
                                    playerst.setInt((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.智力, toSet);
                                    break;

                                case 512:
                                    toSet = playerst.getLuk() - 1;
                                    playerst.setLuk((short) (int) toSet, chr);
                                    statupdate.put(MapleStat.运气, toSet);
                                    break;

                                case 2048:
                                    int maxhp = playerst.getMaxHp();
                                    if (GameConstants.is新手职业(job))
                                    {
                                        maxhp -= 12;
                                    }
                                    else if (GameConstants.is恶魔复仇者(job))
                                    {
                                        maxhp -= 30;
                                    }
                                    else if (((job >= 200) && (job <= 232)) || ((job >= 1200) && (job <= 1212)) || ((job >= 2700) && (job <= 2712)))
                                    {
                                        maxhp -= 10;
                                    }
                                    else if (((job >= 300) && (job <= 322)) || ((job >= 400) && (job <= 434)) || ((job >= 1300) && (job <= 1312)) || ((job >= 1400) && (job <= 1412)) || ((job >= 3300) && (job <= 3312)) || ((job >= 3500) && (job <= 3512)) || ((job >= 2300) && (job <= 2312)) || ((job >= 2400) && (job <= 2412)))
                                    {
                                        maxhp -= 15;
                                    }
                                    else if (((job >= 500) && (job <= 592)) || ((job >= 1500) && (job <= 1512)))
                                    {
                                        maxhp -= 22;
                                    }
                                    else if (((job >= 100) && (job <= 132)) || ((job >= 1100) && (job <= 1112)) || ((job >= 3100) && (job <= 3112)) || ((job >= 5100) && (job <= 5112)))
                                    {
                                        maxhp -= 32;
                                    }
                                    else if (((job >= 2000) && (job <= 2112)) || ((job >= 3200) && (job <= 3212)))
                                    {
                                        maxhp -= 40;
                                    }
                                    else
                                    {
                                        maxhp -= 20;
                                    }
                                    chr.setHpApUsed((short) (chr.getHpApUsed() - 1));
                                    playerst.setMaxHp(maxhp, chr);
                                    statupdate.put(MapleStat.MAXHP, (long) maxhp);
                                    break;
                                case 8192:
                                    int maxmp = playerst.getMaxMp();
                                    if (GameConstants.is新手职业(job))
                                    {
                                        maxmp -= 8;
                                    }
                                    else
                                    {
                                        if (GameConstants.isNotMpJob(job)) return;
                                        if (((job >= 100) && (job <= 132)) || ((job >= 1100) && (job <= 1112)) || ((job >= 5100) && (job <= 5112)))
                                        {
                                            maxmp -= 4;
                                        }
                                        else if (((job >= 200) && (job <= 232)) || ((job >= 1200) && (job <= 1212)) || ((job >= 2700) && (job <= 2712)))
                                        {
                                            maxmp -= 30;
                                        }
                                        else if (((job >= 500) && (job <= 592)) || ((job >= 300) && (job <= 322)) || ((job >= 400) && (job <= 434)) || ((job >= 1300) && (job <= 1312)) || ((job >= 1400) && (job <= 1412)) || ((job >= 1500) && (job <= 1512)) || ((job >= 3300) && (job <= 3312)) || ((job >= 3500) && (job <= 3512)) || ((job >= 2300) && (job <= 2312)) || ((job >= 2400) && (job <= 2412)))
                                        {
                                            maxmp -= 10;
                                        }
                                        else if ((job >= 2000) && (job <= 2112))
                                        {
                                            maxmp -= 5;
                                        }
                                        else maxmp -= 20;
                                    }
                                    if (GameConstants.isNotMpJob(job))
                                    {
                                        chr.dropMessage(1, "该职业无法使用.");
                                    }
                                    else
                                    {
                                        chr.setHpApUsed((short) (chr.getHpApUsed() - 1));
                                        playerst.setMaxMp(maxmp, chr);
                                        statupdate.put(MapleStat.MAXMP, (long) maxmp);
                                    }
                                    break;
                            }
                            c.getSession().write(MaplePacketCreator.updatePlayerStats(statupdate, true, chr));
                        }
                    }
                }
                else if ((itemId >= 5050005) && (!GameConstants.is龙神(chr.getJob())))
                {
                    chr.dropMessage(1, "只有龙神职业才能使用这个道具.");

                }
                else if ((itemId < 5050005) && (GameConstants.is龙神(chr.getJob())))
                {
                    chr.dropMessage(1, "龙神职业无法使用这个道具.");
                }
                else
                {
                    int skill1 = slea.readInt();
                    int skill2 = slea.readInt();
                    for (int i : GameConstants.blockedSkills)
                    {
                        if (skill1 == i)
                        {
                            chr.dropMessage(1, "该技能未修复，无法增加此技能.");
                            return;
                        }
                    }
                    skillSPTo = client.SkillFactory.getSkill(skill1);
                    skillSPFrom = client.SkillFactory.getSkill(skill2);
                    if ((skillSPTo.isBeginnerSkill()) || (skillSPFrom.isBeginnerSkill()))
                    {
                        chr.dropMessage(1, "无法对新手技能使用.");

                    }
                    else if (GameConstants.getSkillBookBySkill(skill1) != GameConstants.getSkillBookBySkill(skill2))
                    {
                        chr.dropMessage(1, "You may not add different job skills.");

                    }
                    else if ((chr.getSkillLevel(skillSPTo) + 1 <= skillSPTo.getMaxLevel()) && (chr.getSkillLevel(skillSPFrom) > 0) && (skillSPTo.canBeLearnedBy(chr.getJob())))
                        if ((skillSPTo.isFourthJob()) && (chr.getSkillLevel(skillSPTo) + 1 > chr.getMasterLevel(skillSPTo)))
                        {
                            chr.dropMessage(1, "You will exceed the master level.");
                        }
                        else
                        {
                            if (itemId >= 5050005)
                            {
                                if ((GameConstants.getSkillBookBySkill(skill1) != (itemId - 5050005) * 2) && (GameConstants.getSkillBookBySkill(skill1) != (itemId - 5050005) * 2 + 1))
                                {
                                    chr.dropMessage(1, "You may not add this job SP using this reset.");
                                    break;
                                }
                            }
                            else
                            {
                                theJob = GameConstants.getJobNumber(skill2 / 10000);
                                switch (skill2 / 10000)
                                {
                                    case 430:
                                        theJob = 1;
                                        break;
                                    case 431:
                                    case 432:
                                        theJob = 2;
                                        break;
                                    case 433:
                                        theJob = 3;
                                        break;
                                    case 434:
                                        theJob = 4;
                                }

                                if (theJob != itemId - 5050000)
                                {
                                    chr.dropMessage(1, "You may not subtract from this skill. Use the appropriate SP reset.");
                                    break;
                                }
                            }
                            chr.changeSingleSkillLevel(skillSPFrom, (byte) (chr.getSkillLevel(skillSPFrom) - 1), chr.getMasterLevel(skillSPFrom));
                            chr.changeSingleSkillLevel(skillSPTo, (byte) (chr.getSkillLevel(skillSPTo) + 1), chr.getMasterLevel(skillSPTo));
                            used = true;
                        }
                }
                break;

            case 506:
                if (itemId == 5060000)
                {
                    item = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(slea.readShort());
                    if ((item != null) && (item.getOwner().equals("")))
                    {
                        boolean change = true;
                        for (String z : GameConstants.RESERVED)
                        {
                            if (chr.getName().contains(z))
                            {
                                change = false;
                            }
                        }
                        if (change)
                        {
                            item.setOwner(chr.getName());
                            chr.forceUpdateItem(item);
                            used = true;
                            break;
                        }
                    }
                    else
                    {
                        chr.dropMessage(1, "请将道具直接点在你需要刻名的装备上.");
                        break;
                    }
                }
                else if ((itemId == 5060001) || (itemId == 5061000) || (itemId == 5061001) || (itemId == 5061002) || (itemId == 5061003))
                {
                    MapleInventoryType type = MapleInventoryType.getByType((byte) slea.readInt());
                    item = chr.getInventory(type).getItem((byte) slea.readInt());
                    if ((item != null) && (item.getExpiration() == -1L))
                    {
                        flag = item.getFlag();
                        flag = (short) (flag | ItemFlag.LOCK.getValue());
                        item.setFlag(flag);
                        long days = 0L;
                        if (itemId == 5061000)
                        {
                            days = 7L;
                        }
                        else if (itemId == 5061001)
                        {
                            days = 30L;
                        }
                        else if (itemId == 5061002)
                        {
                            days = 90L;
                        }
                        else if (itemId == 5061003)
                        {
                            days = 365L;
                        }
                        if (chr.isAdmin())
                        {
                            chr.dropMessage(5, "使用封印之锁 物品ID: " + itemId + " 天数: " + days);
                        }
                        if (days > 0L)
                        {
                            item.setExpiration(System.currentTimeMillis() + days * 24L * 60L * 60L * 1000L);
                        }
                        chr.forceUpdateItem(item);
                        used = true;
                    }
                    else
                    {
                        chr.dropMessage(1, "使用道具出现错误.");
                    }
                }
                else if ((itemId == 5064000) || (itemId == 5064003))
                {
                    short dst = slea.readShort();
                    if (dst < 0)
                    {
                        item = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
                    }
                    else
                    {
                        item = null;
                    }
                    if ((item != null) && (item.getType() == 1))
                    {
                        int maxEnhance = itemId == 5064003 ? 7 : 12;
                        if (((Equip) item).getEnhance() >= maxEnhance)
                        {
                            chr.dropMessage(1, "该道具已无法继续使用防爆卷轴效果.");
                        }
                        else
                        {
                            flag = item.getFlag();
                            if (!ItemFlag.装备防爆.check(flag))
                            {
                                flag = (short) (flag | ItemFlag.装备防爆.getValue());
                                item.setFlag(flag);
                                chr.forceUpdateItem(item);
                                chr.getMap().broadcastMessage(chr, InventoryPacket.getScrollEffect(chr.getId(), itemId, item.getItemId()), true);
                                used = true;
                            }
                            else
                            {
                                chr.dropMessage(1, "已经获得了相同效果。");
                                break;
                            }
                        }
                    }
                    else
                    {
                        chr.dropMessage(1, "请将卷轴点在你需要保护的装备上.");
                        break;
                    }
                }
                else if (itemId == 5064100)
                {
                    short dst = slea.readShort();
                    if (dst < 0)
                    {
                        item = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
                    }
                    else
                    {
                        item = null;
                    }
                    if ((item != null) && (item.getType() == 1))
                    {
                        flag = item.getFlag();
                        if (!ItemFlag.保护升级次数.check(flag))
                        {
                            flag = (short) (flag | ItemFlag.保护升级次数.getValue());
                            item.setFlag(flag);
                            chr.forceUpdateItem(item);
                            chr.getMap().broadcastMessage(chr, InventoryPacket.getScrollEffect(chr.getId(), itemId, item.getItemId()), true);
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(5, "已经获得了相同效果。");
                            break;
                        }
                    }
                    else
                    {
                        chr.dropMessage(5, "请将卷轴点在你需要保护的装备上.");
                        break;
                    }
                }
                else if (itemId == 5064300)
                {
                    short dst = slea.readShort();
                    if (dst < 0)
                    {
                        item = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
                    }
                    else
                    {
                        item = null;
                    }
                    if ((item != null) && (item.getType() == 1))
                    {
                        flag = item.getFlag();
                        if (!ItemFlag.卷轴防护.check(flag))
                        {
                            flag = (short) (flag | ItemFlag.卷轴防护.getValue());
                            item.setFlag(flag);
                            chr.forceUpdateItem(item);
                            chr.getMap().broadcastMessage(chr, InventoryPacket.getScrollEffect(chr.getId(), itemId, item.getItemId()), true);
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(5, "已经获得了相同效果。");
                            break;
                        }
                    }
                    else
                    {
                        chr.dropMessage(5, "请将卷轴点在你需要保护的装备上.");
                        break;
                    }
                }
                else if (itemId == 5060003)
                {
                    item = chr.getInventory(MapleInventoryType.ETC).findById(4170023);
                    if ((item == null) || (item.getQuantity() <= 0))
                    {
                        return;
                    }
                    if (getIncubatedItems(c, itemId))
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.ETC, item.getPosition(), (short) 1, false);
                        used = true;
                    }
                }
                else if (itemId == 5062000)
                {
                    item = chr.getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readInt());
                    if ((item != null) && (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1))
                    {
                        Equip eq = (Equip) item;
                        int stateRate = chr.getClient().getChannelServer().getStateRate();
                        if ((eq.getState() >= 17) && (eq.getState() != 20))
                        {
                            eq.renewPotential(0, chr.isAdmin() ? 99 : stateRate);
                            chr.forceUpdateItem(item);
                            chr.getMap().broadcastMessage(InventoryPacket.showPotentialReset(false, chr.getId(), true, itemId));
                            MapleInventoryManipulator.addById(c, 2430112, (short) 1, "Cube on " + FileoutputUtil.CurrentReadable_Date());
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(5, "请确认您要重置的道具具有潜能属性.");
                        }
                    }
                    else
                    {
                        chr.getMap().broadcastMessage(InventoryPacket.showPotentialReset(false, chr.getId(), false, itemId));
                    }

                }
                else if ((itemId == 5062001) || (itemId == 5062100))
                {
                    item = chr.getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readInt());
                    if ((item != null) && (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1))
                    {
                        Equip eq = (Equip) item;
                        int stateRate = chr.getClient().getChannelServer().getStateRate();
                        if ((eq.getState() >= 17) && (eq.getState() != 20))
                        {
                            eq.renewPotential(1, chr.isAdmin() ? 99 : stateRate);
                            chr.forceUpdateItem(item);
                            chr.getMap().broadcastMessage(InventoryPacket.showPotentialReset(false, chr.getId(), true, itemId));
                            MapleInventoryManipulator.addById(c, 2430112, (short) 1, "Cube on " + FileoutputUtil.CurrentReadable_Date());
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(5, "请确认您要重置的道具具有潜能属性.");
                        }
                    }
                    else
                    {
                        chr.getMap().broadcastMessage(InventoryPacket.showPotentialReset(false, chr.getId(), false, itemId));
                    }

                }
                else if (itemId == 5062002)
                {
                    item = chr.getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readInt());
                    if ((item != null) && (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1))
                    {
                        Equip eq = (Equip) item;
                        int stateRate = chr.getClient().getChannelServer().getStateRate();
                        if (eq.getState() >= 17)
                        {
                            eq.renewPotential(3, chr.isAdmin() ? 99 : stateRate);
                            chr.forceUpdateItem(item);
                            chr.getMap().broadcastMessage(InventoryPacket.showPotentialReset(false, chr.getId(), true, itemId));
                            MapleInventoryManipulator.addById(c, 2430481, (short) 1, "Cube on " + FileoutputUtil.CurrentReadable_Date());
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(5, "请确认您要重置的道具具有潜能属性.");
                        }
                    }
                    else
                    {
                        chr.getMap().broadcastMessage(InventoryPacket.showPotentialReset(false, chr.getId(), false, itemId));
                    }

                }
                else if ((itemId == 5062400) || (itemId == 5062402))
                {

                    byte skinSlot = (byte) slea.readInt();
                    byte itemSlot = (byte) slea.readInt();
                    Equip toItem = (Equip) chr.getInventory(MapleInventoryType.EQUIP).getItem(itemSlot);
                    Equip skin = (Equip) chr.getInventory(MapleInventoryType.EQUIP).getItem(skinSlot);
                    if ((toItem != null) && (skin != null) && (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1))
                    {
                        MapleInventoryManipulator.addById(c, 2028093, (short) 1, "使用神奇铁砧 时间: " + FileoutputUtil.CurrentReadable_Date());
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.EQUIP, skinSlot, (short) 1, false, true);
                        toItem.setItemSkin(skin.getItemSkin() > 0 ? skin.getItemSkin() : skin.getItemId());
                        chr.forceUpdateItem(toItem);
                        c.getSession().write(InventoryPacket.showSynthesizingMsg(itemId, 2028093, true));
                        chr.finishAchievement(57);
                        used = true;
                    }
                    else
                    {
                        c.getSession().write(InventoryPacket.showSynthesizingMsg(itemId, 2028093, false));
                    }
                }
                else if (itemId == 5062500)
                {
                    item = chr.getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readInt());
                    if ((item != null) && (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1))
                    {
                        Equip eq = (Equip) item;
                        int stateRate = chr.getClient().getChannelServer().getStateRate();
                        if ((eq.getAddState(4) >= 17) || (eq.getAddState(5) >= 17) || (eq.getAddState(6) >= 17))
                        {
                            if (eq.getAddState(4) >= 17)
                            {
                                eq.renewAddPotential(chr.isAdmin() ? 99 : stateRate, 4);
                            }
                            if (eq.getAddState(5) >= 17)
                            {
                                eq.renewAddPotential(chr.isAdmin() ? 99 : stateRate, 5);
                            }
                            if (eq.getAddState(6) >= 17)
                            {
                                eq.renewAddPotential(chr.isAdmin() ? 99 : stateRate, 6);
                            }
                            chr.forceUpdateItem(item);
                            chr.getMap().broadcastMessage(InventoryPacket.潜能变化效果(chr.getId(), true, itemId));
                            MapleInventoryManipulator.addById(c, 2430915, (short) 1, "Cube on " + FileoutputUtil.CurrentReadable_Date());
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(5, "请确认您要重置的道具具有扩展潜能属性.");
                        }
                    }
                    else
                    {
                        chr.getMap().broadcastMessage(InventoryPacket.潜能变化效果(chr.getId(), false, itemId));
                    }

                }
                else if (itemId == 5068100)
                {
                    short dst = slea.readShort();
                    if (dst == 0)
                    {
                        chr.dropMessage(5, "请将卷轴点在你需要保护的宠物装备上");
                    }
                    else if ((dst == -114) || (dst == -122) || (dst == -124))
                    {
                        item = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
                        if (item != null)
                        {
                            flag = item.getFlag();
                            if (!ItemFlag.保护升级次数.check(flag))
                            {
                                flag = (short) (flag | ItemFlag.保护升级次数.getValue());
                                item.setFlag(flag);

                                chr.forceUpdateItem(item);
                                chr.getMap().broadcastMessage(chr, InventoryPacket.getScrollEffect(chr.getId(), itemId, item.getItemId()), true);
                                used = true;
                            }
                            else
                            {
                                chr.dropMessage(5, "已经获得了相同效果。");
                            }
                        }
                    }
                    else
                    {
                        chr.dropMessage(5, "该卷轴只能用于宠物装备上。");
                    }
                }
                else if (itemId == 5068200)
                {
                    short dst = slea.readShort();
                    if (dst == 0)
                    {
                        chr.dropMessage(5, "请将卷轴点在你需要保护的宠物装备上");
                    }
                    else if ((dst == -114) || (dst == -122) || (dst == -124))
                    {
                        item = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
                        if (item != null)
                        {
                            flag = item.getFlag();

                            if (!ItemFlag.卷轴防护.check(flag))
                            {
                                flag = (short) (flag | ItemFlag.卷轴防护.getValue());
                                item.setFlag(flag);

                                chr.forceUpdateItem(item);
                                chr.getMap().broadcastMessage(chr, InventoryPacket.getScrollEffect(chr.getId(), itemId, item.getItemId()), true);
                                used = true;
                            }
                            else
                            {
                                chr.dropMessage(5, "已经获得了相同效果。");
                            }
                        }
                    }
                    else
                    {
                        chr.dropMessage(5, "该卷轴只能用于宠物装备上。");
                    }
                }
                else if (itemId == 5060002)
                {
                    byte inventory2 = (byte) slea.readInt();
                    byte slot2 = (byte) slea.readInt();
                    Item item2 = chr.getInventory(MapleInventoryType.getByType(inventory2)).getItem(slot2);
                    if (item2 == null)
                    {
                        return;
                    }
                    chr.dropMessage(1, "暂时无法使用这个道具.");
                }
                else if ((itemId == 5062009) || (itemId == 5062010))
                {
                    short itempos = (short) slea.readInt();
                    if (ItemPotentialAndMagnify(chr, itempos, itemId))
                    {
                        used = true;
                    }
                    else
                    {
                        chr.dropMessage(1, "使用道具失败...");
                    }
                }
                else
                {
                    chr.dropMessage(1, "暂时无法使用这个道具.");
                }
                break;

            case 507:
                if (chr.isAdmin())
                {
                    chr.dropMessage(5, "使用商场喇叭 道具类型: " + itemId / 1000 % 10);
                }
                if (chr.getLevel() < 10)
                {
                    chr.dropMessage(5, "需要等级10级才能使用这个道具.");

                }
                else if (chr.getMapId() == 180000001)
                {
                    chr.dropMessage(5, "在这个地方无法使用这个道具.");

                }
                else if ((!chr.getCheatTracker().canSmega()) && (!chr.isGM()))
                {
                    chr.dropMessage(5, "你需要等待3秒之后才能使用这个道具.");

                }
                else if (!c.getChannelServer().getMegaphoneMuteState())
                {
                    int msgType = itemId / 1000 % 10;
                    used = true;
                    switch (msgType)
                    {
                        case 0:
                            chr.getMap().broadcastMessage(MaplePacketCreator.serverNotice(2, chr.getMedalText() + chr.getName() + " : " + slea.readMapleAsciiString()));
                            break;
                        case 1:
                            c.getChannelServer().broadcastSmegaPacket(MaplePacketCreator.serverNotice(2, chr.getMedalText() + chr.getName() + " : " + slea.readMapleAsciiString()));
                            break;
                        case 2:
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.serverNotice(3, c.getChannel(),
                                    chr.getMedalText() + chr.getName() + " : " + slea.readMapleAsciiString(), slea.readByte() != 0));
                            break;
                        case 3:
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.serverNotice(26, c.getChannel(),
                                    chr.getMedalText() + chr.getName() + " : " + slea.readMapleAsciiString(), slea.readByte() != 0));
                            break;
                        case 4:
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.serverNotice(27, c.getChannel(),
                                    chr.getMedalText() + chr.getName() + " : " + slea.readMapleAsciiString(), slea.readByte() != 0));
                            break;
                        case 5:
                            int tvType = itemId % 10;
                            boolean megassenger = false;
                            tvEar = false;
                            MapleCharacter victim = null;
                            if (tvType != 1)
                            {
                                if (tvType >= 3)
                                {
                                    megassenger = true;
                                    if (tvType == 3)
                                    {
                                        slea.readByte();
                                    }
                                    tvEar = 1 == slea.readByte();
                                }
                                else if (tvType != 2)
                                {
                                    slea.readByte();
                                }
                                if (tvType != 4)
                                {
                                    victim = c.getChannelServer().getPlayerStorage().getCharacterByName(slea.readMapleAsciiString());
                                }
                            }
                            tvMessages = new LinkedList<>();
                            StringBuilder builder = new StringBuilder();
                            String message = slea.readMapleAsciiString();
                            if (megassenger)
                            {
                                builder.append(" ").append(message);
                            }
                            tvMessages.add(message);

                            if (!server.maps.MapleTVEffect.isActive())
                            {
                                if (megassenger)
                                {
                                    String text = builder.toString();
                                    if (text.length() <= 60)
                                    {
                                        WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.serverNotice(3, c.getChannel(), chr.getName() + " : " + builder.toString(), tvEar));
                                    }
                                }
                                server.maps.MapleTVEffect mapleTVEffect = new server.maps.MapleTVEffect(chr, victim, tvMessages, tvType);
                                mapleTVEffect.stratMapleTV();
                            }
                            else
                            {
                                chr.dropMessage(1, "冒险岛TV正在使用中");
                                used = false;
                            }
                            break;
                        case 6:
                            String djmsg = chr.getMedalText() + chr.getName() + " : " + slea.readMapleAsciiString();
                            boolean sjEar = slea.readByte() > 0;
                            item = null;
                            if (slea.readByte() == 1)
                            {
                                byte invType = (byte) slea.readInt();
                                byte pos = (byte) slea.readInt();
                                item = chr.getInventory(MapleInventoryType.getByType(invType)).getItem(pos);
                            }
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.itemMegaphone(djmsg, sjEar, c.getChannel(), item));
                            break;
                        case 7:
                            byte numLines = slea.readByte();
                            if ((numLines < 1) || (numLines > 3))
                            {
                                return;
                            }
                            List<String> bfMessages = new LinkedList<>();

                            for (int i = 0; i < numLines; i++)
                            {
                                String bfMsg = slea.readMapleAsciiString();
                                if (bfMsg.length() > 65)
                                {
                                    break;
                                }
                                bfMessages.add(chr.getName() + " : " + bfMsg);
                            }
                            boolean bfEar = slea.readByte() > 0;
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.tripleSmega(bfMessages, bfEar, c.getChannel()));
                            break;
                        case 9:
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.serverNotice(itemId == 5079001 ? 24 : 25, c.getChannel(), chr.getMedalText() + chr.getName() + " : "
                                    + slea.readMapleAsciiString(), slea.readByte() != 0));
                    }
                }
                else
                {
                    chr.dropMessage(5, "当前频道禁止使用道具喇叭.");
                }
                break;

            case 508:
                server.maps.MapleLove love1 = new server.maps.MapleLove(chr, chr.getPosition(), chr.getMap().getFootholds().findBelow(chr.getPosition()).getId(), slea.readMapleAsciiString(), itemId);
                chr.getMap().spawnLove(love1);
                used = true;
                break;

            case 509:
                String sendTo = slea.readMapleAsciiString();
                String msg = slea.readMapleAsciiString();
                chr.sendNote(sendTo, msg);
                used = true;
                break;


            case 510:
                chr.getMap().startJukebox(chr.getName(), itemId);
                used = true;
                break;

            case 512:
                String msg1 = ii.getMsg(itemId);
                String ourMsg = slea.readMapleAsciiString();
                if (!msg1.contains("%s"))
                {
                    msg1 = ourMsg;
                }
                else
                {
                    msg1 = msg1.replaceFirst("%s", chr.getName());
                    if (!msg1.contains("%s"))
                    {
                        msg1 = ii.getMsg(itemId).replaceFirst("%s", ourMsg);
                    }
                    else
                    {
                        try
                        {
                            msg1 = msg1.replaceFirst("%s", ourMsg);
                        }
                        catch (Exception e)
                        {
                            msg1 = ii.getMsg(itemId).replaceFirst("%s", ourMsg);
                        }
                    }
                }
                chr.getMap().startMapEffect(msg1, itemId);
                buff = ii.getStateChangeItem(itemId);
                if (buff != 0)
                {
                    for (MapleCharacter mChar1 : chr.getMap().getCharactersThreadsafe())
                        ii.getItemEffect(buff).applyTo(mChar1);
                }
                used = true;
                break;


            case 515:
                if ((itemId >= 5152100) && (itemId <= 5152107))
                {
                    int color = (itemId - 5152100) * 100;

                    if (color >= 0)
                    {
                        changeFace(chr, color);
                        used = true;
                    }
                    else
                    {
                        chr.dropMessage(1, "使用一次性隐形眼镜出现错误.");
                    }
                }
                else if (itemId == 5155000)
                {
                    chr.changeElfEar();
                    used = true;
                }
                else if (itemId == 5156000)
                {
                    if ((chr.getMarriageId() > 0) || (chr.getMarriageRing() != null))
                    {
                        chr.dropSpouseMessage(11, "已婚人士无法使用。");
                    }
                    else if ((GameConstants.is米哈尔(chr.getJob())) || (GameConstants.is爆莉萌天使(chr.getJob())))
                    {
                        chr.dropSpouseMessage(11, "该职业群无法使用的物品。");
                    }
                    else
                    {
                        tools.Pair<Integer, Integer> ret = GameConstants.getDefaultFaceAndHair(chr.getJob(), chr.getGender());
                        Map<MapleStat, Long> statup = new EnumMap(MapleStat.class);
                        chr.setGender((byte) (chr.getGender() == 0 ? 1 : 0));
                        chr.setFace(ret.getLeft());
                        chr.setHair(ret.getRight());
                        statup.put(MapleStat.脸型, (long) chr.getFace());
                        statup.put(MapleStat.发型, (long) chr.getHair());
                        statup.put(MapleStat.性别, (long) chr.getGender());
                        c.getSession().write(MaplePacketCreator.updatePlayerStats(statup, chr));
                        c.getSession().write(MaplePacketCreator.showOwnCraftingEffect("Effect/BasicEff.img/TransGender", 0, 0));
                        chr.getMap().broadcastMessage(chr, MaplePacketCreator.showCraftingEffect(chr.getId(), "Effect/BasicEff.img/TransGender", 0, 0), false);
                        chr.equipChanged();
                        used = true;
                    }
                }
                else
                {
                    chr.dropMessage(1, "暂不支持这个道具的使用.");
                }
                break;

            case 517:
                int uniqueid = (int) slea.readLong();
                MaplePet pet = null;
                for (MaplePet petx : chr.getPets())
                {
                    if ((petx != null) && (petx.getUniqueId() == uniqueid))
                    {
                        pet = petx;
                        break;
                    }
                }
                MaplePet petx;
                if (pet == null)
                {
                    chr.dropMessage(1, "宠物改名错误，找不到宠物的信息.");
                }
                else
                {
                    nName = slea.readMapleAsciiString();
                    for (String ptName : GameConstants.RESERVED)
                    {
                        if ((pet.getName().contains(ptName)) || (nName.contains(ptName)))
                        {
                            break;
                        }
                    }
                    if (client.MapleCharacterUtil.canChangePetName(nName))
                    {
                        pet.setName(nName);
                        pet.saveToDb();
                        c.getSession().write(PetPacket.updatePet(pet, chr.getInventory(MapleInventoryType.CASH).getItem((byte) pet.getInventoryPosition()), true));
                        c.getSession().write(MaplePacketCreator.enableActions());
                        chr.getMap().broadcastMessage(MTSCSPacket.changePetName(chr, nName, pet.getInventoryPosition()));
                        used = true;
                    }
                }
                break;
            case 519:
                PetFlag petFlag;
                if ((itemId >= 5190000) && (itemId <= 5190011))
                {
                    int uniqueid1 = (int) slea.readLong();
                    MaplePet pet11 = null;
                    for (MaplePet petxx : chr.getPets())
                    {
                        if ((petxx != null) && (petxx.getUniqueId() == uniqueid1))
                        {
                            pet11 = petxx;
                            break;
                        }
                    }
                    if (pet11 == null)
                    {
                        chr.dropMessage(1, "宠物改名错误，找不到宠物的信息.");
                    }
                    else
                    {
                        petFlag = PetFlag.getByAddId(itemId);
                        if ((petFlag != null) && (!petFlag.check(pet11.getFlags())))
                        {
                            pet11.setFlags(pet11.getFlags() | petFlag.getValue());
                            pet11.saveToDb();
                            c.getSession().write(PetPacket.updatePet(pet11, chr.getInventory(MapleInventoryType.CASH).getItem((byte) pet11.getInventoryPosition()), true));
                            c.getSession().write(MaplePacketCreator.enableActions());
                            c.getSession().write(MTSCSPacket.changePetFlag(uniqueid1, true, petFlag.getValue()));
                            used = true;
                        }
                    }
                }
                else if ((itemId >= 5191000) && (itemId <= 5191004))
                {
                    int uniqueid2 = (int) slea.readLong();
                    MaplePet pett = null;
                    for (MaplePet petxt : chr.getPets())
                    {
                        if ((petxt != null) && (petxt.getUniqueId() == uniqueid2))
                        {
                            pett = petxt;
                            break;
                        }
                    }
                    if (pett == null)
                    {
                        chr.dropMessage(1, "宠物改名错误，找不到宠物的信息.");
                    }
                    else
                    {
                        PetFlag petFlag1 = PetFlag.getByDelId(itemId);
                        if ((petFlag1 != null) && (petFlag1.check(pett.getFlags())))
                        {
                            pett.setFlags(pett.getFlags() - petFlag1.getValue());
                            pett.saveToDb();
                            c.getSession().write(PetPacket.updatePet(pett, chr.getInventory(MapleInventoryType.CASH).getItem((byte) pett.getInventoryPosition()), true));
                            c.getSession().write(MaplePacketCreator.enableActions());
                            c.getSession().write(MTSCSPacket.changePetFlag(uniqueid2, false, petFlag1.getValue()));
                            used = true;
                        }
                    }
                }
                break;


            case 520:
                if ((itemId >= 5200000) && (itemId <= 5200008))
                {
                    if (chr.isIntern())
                    {
                        mesars = ii.getMeso(itemId);
                        if ((mesars > 0) && (chr.getMeso() < Integer.MAX_VALUE - mesars))
                        {
                            used = true;
                            if (Math.random() > 0.1D)
                            {
                                int gainmes = Randomizer.nextInt(mesars);
                                chr.gainMeso(gainmes, false);
                                c.getSession().write(MTSCSPacket.sendMesobagSuccess(gainmes));
                            }
                            else
                            {
                                c.getSession().write(MTSCSPacket.sendMesobagFailed());
                            }
                        }
                        else
                        {
                            chr.dropMessage(1, "金币已达到上限无法使用这个道具.");
                        }
                    }
                    else
                    {
                        server.AutobanManager.getInstance().autoban(chr.getClient(), "使用非法道具.");
                    }
                }
                else
                {
                    chr.dropMessage(5, "暂时无法使用这个道具.");
                }
                break;
            case 522:
                MonsterFamiliar mf;
                if (itemId == 5220083)
                {
                    used = true;
                    for (Map.Entry<Integer, StructFamiliar> f : ii.getFamiliars().entrySet())
                    {
                        if ((f.getValue().itemid == 2870055) || (f.getValue().itemid == 2871002) || (f.getValue().itemid == 2870235) || (f.getValue().itemid == 2870019))
                        {
                            mf = chr.getFamiliars().get(f.getKey());
                            if (mf != null)
                            {
                                if (mf.getVitality() >= 3)
                                {
                                    mf.setExpiry(Math.min(System.currentTimeMillis() + 7776000000L, mf.getExpiry() + 2592000000L));
                                }
                                else
                                {
                                    mf.setVitality(mf.getVitality() + 1);
                                    mf.setExpiry(mf.getExpiry() + 2592000000L);
                                }
                            }
                            else
                            {
                                mf = new MonsterFamiliar(chr.getId(), f.getKey(), System.currentTimeMillis() + 2592000000L);
                                chr.getFamiliars().put(f.getKey(), mf);
                            }
                            c.getSession().write(MaplePacketCreator.registerFamiliar(mf));
                        }
                    }
                }
                else if (itemId == 5220084)
                {
                    if (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() < 3)
                    {
                        chr.dropMessage(5, "请确保您有足够的背包空间.");
                    }
                    else
                    {
                        used = true;
                        int[] familiars = new int[3];
                        for (; ; )
                        {
                            for (int i = 0; i < familiars.length; i++)
                            {
                                if (familiars[i] <= 0)
                                {

                                    for (Map.Entry<Integer, StructFamiliar> f : ii.getFamiliars().entrySet())
                                        if ((Randomizer.nextInt(500) == 0) && (((i < 2) && (f.getValue().grade == 0)) || ((i == 2) && (f.getValue().grade != 0))))
                                        {
                                            MapleInventoryManipulator.addById(c, f.getValue().itemid, (short) 1, "Booster Pack");
                                            c.getSession().write(MTSCSPacket.getBoosterFamiliar(chr.getId(), f.getKey(), 0));
                                            familiars[i] = f.getValue().itemid;
                                            break;
                                        }
                                }
                            }
                            if ((familiars[0] > 0) && (familiars[1] > 0) && (familiars[2] > 0))
                            {
                                break;
                            }
                        }
                        c.getSession().write(MTSCSPacket.getBoosterPack(familiars[0], familiars[1], familiars[2]));
                        c.getSession().write(MTSCSPacket.getBoosterPackClick());
                        c.getSession().write(MTSCSPacket.getBoosterPackReveal());
                    }
                }
                else
                {
                    chr.dropMessage(1, "暂时无法使用这个道具.");
                }
                break;

            case 523:
                int itemSearch = slea.readInt();
                List<server.shops.HiredMerchant> hms = c.getChannelServer().searchMerchant(itemSearch);
                if (hms.size() > 0)
                {
                    c.getSession().write(MaplePacketCreator.getOwlSearched(itemSearch, hms));
                    used = true;
                }
                else
                {
                    chr.dropMessage(1, "没有找到这个道具.");
                }
                client.MapleCharacterUtil.addToItemSearch(itemSearch);
                break;

            case 524:
                MaplePet pettt = null;
                MaplePet[] pets = chr.getSpawnPets();
                for (int i = 0; i < 3; i++)
                {
                    if ((pets[i] != null) && (pets[i].canConsume(itemId)))
                    {
                        pettt = pets[i];
                        break;
                    }
                }
                if (pettt == null)
                {
                    chr.dropMessage(1, "没有可以喂食的宠物。\r\n请重新确认。");
                }
                else
                {
                    byte petIndex = chr.getPetIndex(pettt);
                    pettt.setFullness(100);
                    if (pettt.getCloseness() < 30000)
                    {
                        if (pettt.getCloseness() + 100 * c.getChannelServer().getTraitRate() > 30000)
                        {
                            pettt.setCloseness(30000);
                        }
                        else
                        {
                            pettt.setCloseness(pettt.getCloseness() + 100 * c.getChannelServer().getTraitRate());
                        }
                        if (pettt.getCloseness() >= GameConstants.getClosenessNeededForLevel(pettt.getLevel() + 1))
                        {
                            pettt.setLevel(pettt.getLevel() + 1);
                            c.getSession().write(PetPacket.showOwnPetLevelUp(chr.getPetIndex(pettt)));
                            chr.getMap().broadcastMessage(PetPacket.showPetLevelUp(chr, petIndex));
                        }
                    }
                    c.getSession().write(PetPacket.updatePet(pettt, chr.getInventory(MapleInventoryType.CASH).getItem(pettt.getInventoryPosition()), true));
                    chr.getMap().broadcastMessage(chr, PetPacket.commandResponse(chr.getId(), (byte) 1, petIndex, true, true), true);
                    used = true;
                }
                break;

            case 528:
                java.awt.Rectangle bounds = new java.awt.Rectangle((int) chr.getPosition().getX(), (int) chr.getPosition().getY(), 1, 1);
                server.maps.MapleMist mist = new server.maps.MapleMist(bounds, chr);
                chr.getMap().spawnMist(mist, 10000, true);
                c.getSession().write(MaplePacketCreator.enableActions());
                used = true;
                break;

            case 532:
                name = slea.readMapleAsciiString();
                otherName = slea.readMapleAsciiString();
                slea.readInt();
                slea.readInt();
                cardId = slea.readByte();
                PredictCardFactory pcf = PredictCardFactory.getInstance();
                server.PredictCardFactory.PredictCard Card = pcf.getPredictCard(cardId);
                commentId = Randomizer.nextInt(pcf.getCardCommentSize());
                Comment = pcf.getPredictCardComment(commentId);
                if ((Card != null) && (Comment != null))
                {
                    chr.dropMessage(5, "占卜只是随便写的，占卜结果就当个玩笑看看。");
                    int rad = Randomizer.rand(1, Comment.score) + 5;
                    c.getSession().write(MaplePacketCreator.showPredictCard(name, otherName, rad, cardId, commentId));
                    used = true;
                }
                break;


            case 537:
                for (server.events.MapleEventType t : server.events.MapleEventType.values())
                {
                    server.events.MapleEvent e = ChannelServer.getInstance(c.getChannel()).getEvent(t);
                    if (e.isRunning())
                    {
                        for (int i : e.getType().mapids)
                        {
                            if (chr.getMapId() == i)
                            {
                                chr.dropMessage(5, "当前地图无法使用此道具.");
                                c.getSession().write(MaplePacketCreator.enableActions());
                                return;
                            }
                        }
                    }
                }
                chr.setChalkboard(slea.readMapleAsciiString());
                break;

            case 539:
                if (chr.getLevel() < 10)
                {
                    chr.dropMessage(5, "需要等级10级才能使用这个道具.");

                }
                else if (chr.getMapId() == 180000001)
                {
                    chr.dropMessage(5, "当前地图无法使用这个道具.");

                }
                else if (!chr.getCheatTracker().canAvatarSmega())
                {
                    chr.dropMessage(5, "你需要等待6秒之后才能使用这个道具.");

                }
                else if (!c.getChannelServer().getMegaphoneMuteState())
                {
                    messages = new LinkedList<>();
                    for (int i = 0; i < 4; i++)
                    {
                        messages.add(slea.readMapleAsciiString());
                    }
                    ear = slea.readByte() != 0;
                    WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.getAvatarMega(chr, c.getChannel(), itemId, messages, ear));
                    used = true;
                }
                else
                {
                    chr.dropMessage(5, "当前频道禁止使用情景喇叭.");
                }
                break;

            case 545:
                for (int i : GameConstants.blockedMaps)
                {
                    if (chr.getMapId() == i)
                    {
                        chr.dropMessage(5, "当前地图无法使用此道具.");
                        c.getSession().write(MaplePacketCreator.enableActions());
                        return;
                    }
                }
                if (chr.getLevel() < 10)
                {
                    chr.dropMessage(5, "只有等级达到10级才可以使用此道具.");
                }
                else if ((chr.hasBlockedInventory()) || (chr.getMap().getSquadByMap() != null) || (chr.getEventInstance() != null) || (chr.getMap().getEMByMap() != null) || (chr.getMapId() >= 990000000))
                {
                    chr.dropMessage(5, "当前地图无法使用此道具.");
                }
                else if (((chr.getMapId() >= 680000210) && (chr.getMapId() <= 680000502)) || ((chr.getMapId() / 1000 == 980000) && (chr.getMapId() != 980000000)) || (chr.getMapId() / 100 == 1030008) || (chr.getMapId() / 100 == 922010) || (chr.getMapId() / 10 == 13003000))
                {
                    chr.dropMessage(5, "当前地图无法使用此道具.");


                }
                else if (itemId == 5451001)
                {
                    chr.dropMessage(1, "暂时无法使用这个道具.");
                }
                else if ((itemId == 5450001) || (itemId == 5450005) || (itemId == 5450008) || (itemId == 5450009))
                {
                    chr.setConversation(4);
                    chr.getStorage().sendStorage(c, itemId == 5450001 ? 1002005 : 1022005);
                }
                else if (itemId == 5450010)
                {
                    server.shop.MapleShopFactory.getInstance().getShop(9090100).sendItemShop(c, itemId);
                    used = true;
                }
                else
                {
                    server.shop.MapleShopFactory.getInstance().getShop(9090000).sendItemShop(c, itemId);
                }

                break;

            case 550:
                if (itemId == 5500003)
                {
                    chr.dropMessage(1, "暂时无法使用这个道具.");
                }
                else if ((itemId == 5501001) || (itemId == 5501002))
                {
                    Skill skil = client.SkillFactory.getSkill(slea.readInt());
                    if ((skil != null) && (skil.getId() / 10000 == 8000) && (chr.getSkillLevel(skil) > 0) && (skil.isTimeLimited()) && (GameConstants.getMountItem(skil.getId(), chr) > 0))
                    {

                        long toAdd = (itemId == 5501001 ? 30 : 60) * 24 * 60 * 60 * 1000L;
                        expire = chr.getSkillExpiry(skil);
                        if ((expire >= System.currentTimeMillis()) && (expire + toAdd < System.currentTimeMillis() + 31536000000L))
                        {

                            chr.changeSingleSkillLevel(skil, chr.getSkillLevel(skil), chr.getMasterLevel(skil), expire + toAdd);
                            used = true;
                        }
                    }
                }
                else if ((itemId >= 5500000) && (itemId <= 5500006))
                {
                    Short slots = slea.readShort();
                    if (slots == 0)
                    {
                        chr.dropMessage(1, "请该道具点在你需要延长时间的道具上.");
                    }
                    else
                    {
                        Item itemm = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(slots);
                        long days = 0L;
                        if (itemId == 5500000)
                        {
                            days = 1L;
                        }
                        else if (itemId == 5500001)
                        {
                            days = 7L;
                        }
                        else if (itemId == 5500002)
                        {
                            days = 20L;
                        }
                        else if (itemId == 5500004)
                        {
                            days = 30L;
                        }
                        else if (itemId == 5500005)
                        {
                            days = 50L;
                        }
                        else if (itemId == 5500006)
                        {
                            days = 99L;
                        }
                        if ((itemm != null) && (!constants.ItemConstants.isAccessory(itemm.getItemId())) && (itemm.getExpiration() > -1L) && (!ii.isCash(itemm.getItemId())) && (System.currentTimeMillis() + 8640000000L > itemm.getExpiration() + days * 24L * 60L * 60L * 1000L))
                        {
                            boolean change = true;
                            for (String z : GameConstants.RESERVED)
                            {
                                if ((chr.getName().contains(z)) || (itemm.getOwner().contains(z)))
                                {
                                    change = false;
                                }
                            }
                            if ((change) && (days > 0L))
                            {
                                itemm.setExpiration(itemm.getExpiration() + days * 24L * 60L * 60L * 1000L);
                                chr.forceUpdateItem(itemm);
                                used = true;
                            }
                            else
                            {
                                chr.dropMessage(1, "无法使用在这个道具上.");
                            }
                        }
                        else
                        {
                            chr.dropMessage(1, "使用道具出现错误.");
                        }
                    }
                }
                else
                {
                    chr.dropMessage(1, "暂时无法使用这个道具.");
                }
                break;

            case 552:
                if (itemId == 5521000)
                {
                    MapleInventoryType type = MapleInventoryType.getByType((byte) slea.readInt());
                    Item itemm1 = chr.getInventory(type).getItem((byte) slea.readInt());
                    if ((itemm1 != null) && (!ItemFlag.KARMA_ACC.check(itemm1.getFlag())) && (!ItemFlag.KARMA_ACC_USE.check(itemm1.getFlag())) && (ii.isShareTagEnabled(itemm1.getItemId())))
                    {
                        short flagg1 = itemm1.getFlag();
                        if (ItemFlag.UNTRADEABLE.check(flagg1))
                        {
                            flagg1 = (short) (flagg1 - ItemFlag.UNTRADEABLE.getValue());
                        }
                        else if (type == MapleInventoryType.EQUIP)
                        {
                            flagg1 = (short) (flagg1 | ItemFlag.KARMA_ACC.getValue());
                        }
                        else
                        {
                            flagg1 = (short) (flagg1 | ItemFlag.KARMA_ACC_USE.getValue());
                        }
                        itemm1.setFlag(flagg1);
                        chr.forceUpdateItem(itemm1);
                        used = true;
                    }

                }
                else if ((itemId == 5520000) || (itemId == 5520001))
                {
                    MapleInventoryType type = MapleInventoryType.getByType((byte) slea.readInt());
                    item = chr.getInventory(type).getItem((byte) slea.readInt());
                    if ((item != null) && (!ItemFlag.KARMA_EQ.check(item.getFlag())) && (!ItemFlag.KARMA_USE.check(item.getFlag())) && (((itemId == 5520000) && (ii.isKarmaEnabled(item.getItemId()))) || ((itemId == 5520001) && (ii.isPKarmaEnabled(item.getItemId())))))
                    {
                        flag = item.getFlag();
                        if (ItemFlag.UNTRADEABLE.check(flag))
                        {
                            flag = (short) (flag - ItemFlag.UNTRADEABLE.getValue());
                        }
                        else if (type == MapleInventoryType.EQUIP)
                        {
                            flag = (short) (flag | ItemFlag.KARMA_EQ.getValue());
                        }
                        else
                        {
                            flag = (short) (flag | ItemFlag.KARMA_USE.getValue());
                        }
                        item.setFlag(flag);
                        chr.forceUpdateItem(item);
                        used = true;
                    }
                }

                break;


            case 553:
                if ((itemId == 5530268) || (itemId == 5530269) || (itemId == 5530052) || (itemId == 5530608) || (itemId == 5530124))
                {
                    InventoryHandler.UseRewardItem(slot, itemId, c, chr);
                }
                else
                {
                    chr.dropMessage(1, "该道具无法使用.道具ID: " + itemId);
                }
                break;

            case 557:
                slea.readInt();
                Equip equItem = (Equip) chr.getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readInt());
                if (equItem != null)
                {
                    if ((constants.ItemConstants.canHammer(equItem.getItemId())) && (ii.getSlots(equItem.getItemId()) > 0) && (equItem.getViciousHammer() < 2))
                    {
                        equItem.setViciousHammer((byte) (equItem.getViciousHammer() + 1));
                        equItem.setUpgradeSlots((byte) (equItem.getUpgradeSlots() + 1));
                        chr.forceUpdateItem(equItem);
                        c.getSession().write(MTSCSPacket.sendHammerData(true, equItem.getViciousHammer()));
                        used = true;
                    }
                    else
                    {
                        chr.dropMessage(1, "无法使用在这个道具上.");
                        c.getSession().write(MTSCSPacket.sendHammerData(false, 0));
                    }
                }

                break;
            case 562:
                if (InventoryHandler.UseSkillBook(slot, itemId, c, chr))
                {
                    chr.gainSP(1);
                }

                break;
            case 570:
                slea.skip(8);
                if (chr.getAndroid() != null)
                {
                    String nNamee = slea.readMapleAsciiString();
                    for (String ss : GameConstants.RESERVED)
                    {
                        if ((chr.getAndroid().getName().contains(ss)) || (nNamee.contains(ss)))
                        {
                            break;
                        }
                    }
                    if (client.MapleCharacterUtil.canChangePetName(nNamee))
                    {
                        chr.getAndroid().setName(nNamee);
                        chr.getAndroid().saveToDb();
                        chr.setAndroid(chr.getAndroid());
                        used = true;
                    }
                }
                break;
            case 575:
                if ((itemId == 5750000) || (itemId == 5750002))
                {
                    if (chr.getLevel() < 10)
                    {
                        chr.dropMessage(1, "使用这个道具需要等级达到10级.");
                    }
                    else
                    {
                        Item item12 = chr.getInventory(MapleInventoryType.SETUP).getItem((byte) slea.readInt());
                        if ((item12 != null) && (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1) && (chr.getInventory(MapleInventoryType.SETUP).getNumFreeSlot() >= 1))
                        {
                            int grade = constants.ItemConstants.getNebuliteGrade(item12.getItemId());
                            if ((grade != -1) && (grade < 4))
                            {
                                int rank = Randomizer.nextInt(100) < 7 ? grade : grade != 3 ? grade + 1 : Randomizer.nextInt(100) < 2 ? grade + 1 : grade;
                                List<StructItemOption> pots = new LinkedList(ii.getAllSocketInfo(rank).values());
                                int newId = 0;
                                while (newId == 0)
                                {
                                    StructItemOption pot = pots.get(Randomizer.nextInt(pots.size()));
                                    if (pot != null)
                                    {
                                        newId = pot.opID;
                                    }
                                }
                                int newGrade = constants.ItemConstants.getNebuliteGrade(newId);
                                if ((newGrade != -1) && (newGrade > grade))
                                {
                                    Item nItem = new Item(newId, (short) 0, (short) 1, (short) 0);
                                    WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(chr.getName(), " : 使用星岩魔方升级了星岩获得{" + ii.getName(newId) +
                                            "}！大家一起恭喜他（她）吧！！！！", nItem, 3, c.getChannel()));
                                }
                                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.SETUP, item12.getPosition(), (short) 1, false);
                                MapleInventoryManipulator.addById(c, newId, (short) 1, "Upgraded from alien cube on " + FileoutputUtil.CurrentReadable_Date());
                                MapleInventoryManipulator.addById(c, 2430760, (short) 1, "Alien Cube on " + FileoutputUtil.CurrentReadable_Date());
                                c.getSession().write(MaplePacketCreator.getShowItemGain(newId, (short) 1, true));
                                chr.getMap().broadcastMessage(InventoryPacket.showNebuliteEffect(chr.getId(), true, "成功交换了星岩。"));
                                c.getSession().write(MaplePacketCreator.craftMessage("你得到了" + ii.getName(newId)));
                                used = true;
                            }
                            else
                            {
                                chr.dropMessage(5, "重置的道具失败.");
                                break;
                            }
                        }
                        else
                        {
                            chr.dropMessage(5, "您的背包空间不足.");
                            break;
                        }
                    }
                }
                else if (itemId == 5750001) if (chr.getLevel() < 10)
                {
                    chr.dropMessage(1, "使用这个道具需要等级达到10级.");
                }
                else
                {
                    Item item3 = c.getPlayer().getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readInt());
                    if (item3 != null)
                    {
                        Equip eq = (Equip) item3;
                        int sockItem = eq.getSocket1();
                        if ((sockItem > 0) && (ii.itemExists(sockItem)))
                        {
                            eq.setSocket1(0);
                            chr.forceUpdateItem(item3);
                            MapleInventoryManipulator.addById(c, sockItem, (short) 1, "摘取星岩: " + FileoutputUtil.CurrentReadable_Date());
                            MapleInventoryManipulator.addById(c, 2430691, (short) 1, "Alien Cube on " + FileoutputUtil.CurrentReadable_Date());
                            chr.getMap().broadcastMessage(InventoryPacket.showNebuliteEffect(chr.getId(), true, "成功清空了插槽。"));
                            used = true;
                        }
                        else
                        {
                            chr.dropMessage(5, "该道具不具有星岩属性.");
                            break;
                        }
                    }
                    else
                    {
                        chr.dropMessage(5, "This item's nebulite cannot be removed.");
                        break;
                    }
                }
                break;


            case 577:
                if ((GameConstants.is龙的传人(chr.getJob())) && (chr.getCoreAura() != null))
                {
                    if (itemId == 5770000)
                    {
                        chr.getCoreAura().setExpiration(chr.getCoreAura().getExpiration() + -1702967296L);
                        used = true;
                    }
                    else if (itemId == 5771001)
                    {
                        chr.getCoreAura().randomCoreAura(1);
                        used = true;
                    }
                    else if (itemId == 5771002)
                    {
                        chr.getCoreAura().randomCoreAura(2);
                        used = true;
                    }
                    else if (itemId == 5771003)
                    {
                        chr.getCoreAura().randomCoreAura(3);
                        used = true;
                    }
                    else if (itemId == 5771004)
                    {
                        chr.getCoreAura().randomCoreAura(4);
                        used = true;
                    }
                    else
                    {
                        chr.dropMessage(1, "该道具暂时无法使用。");
                    }
                    if (used)
                    {
                        chr.updataCoreAura();
                    }
                }
                else
                {
                    chr.dropMessage(1, "只有龙的传人职业可以使用。");
                }
                break;

            case 579:
                if (itemId == 5790000)
                {
                    int slots = c.getAccCardSlots();
                    if (c.gainAccCardSlot())
                    {
                        chr.dropMessage(1, "卡牌扩充成功，当前栏位: " + (slots + 1));
                        used = true;
                    }
                    else
                    {
                        chr.dropMessage(1, "卡牌扩充失败，栏位已超过上限。");
                    }
                }
                else
                {
                    chr.dropMessage(1, "该道具无法使用.");
                }
                break;
            case 511:
            case 513:
            case 514:
            case 516:
            case 518:
            case 521:
            case 525:
            case 526:
            case 527:
            case 529:
            case 530:
            case 531:
            case 533:
            case 534:
            case 535:
            case 536:
            case 538:
            case 540:
            case 541:
            case 542:
            case 543:
            case 544:
            case 546:
            case 547:
            case 548:
            case 549:
            case 551:
            case 554:
            case 555:
            case 556:
            case 558:
            case 559:
            case 560:
            case 561:
            case 563:
            case 564:
            case 565:
            case 566:
            case 567:
            case 568:
            case 569:
            case 571:
            case 572:
            case 573:
            case 574:
            case 576:
            case 578:
            default:
                System.out.println("使用未处理的商城道具 : " + itemId);
                System.out.println(slea.toString(true));
        }

        if ((itemType != 506) ||


                (used))
        {
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.CASH, slot, (short) 1, false, true);
        }
        c.getSession().write(MaplePacketCreator.enableActions());
        if (cc)
        {
            if ((!chr.isAlive()) || (chr.getEventInstance() != null) || (FieldLimitType.ChannelSwitch.check(chr.getMap().getFieldLimit())))
            {
                chr.dropMessage(1, "刷新人物数据失败.");
                return;
            }
            chr.dropMessage(5, "正在刷新人数据.请等待...");
            chr.fakeRelog();
            if (chr.getScrolledPosition() != 0)
            {
                c.getSession().write(MaplePacketCreator.pamSongUI());
            }
        }
    }

    private static boolean getIncubatedItems(MapleClient c, int itemId)
    {
        if ((c.getPlayer().getInventory(MapleInventoryType.EQUIP).getNumFreeSlot() < 2) || (c.getPlayer().getInventory(MapleInventoryType.USE).getNumFreeSlot() < 2) || (c.getPlayer().getInventory(MapleInventoryType.SETUP).getNumFreeSlot() < 2))
        {
            c.getPlayer().dropMessage(5, "请确保你有足够的背包空间.");
            return false;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int id1 = server.RandomRewards.getPeanutReward();
        int id2 = server.RandomRewards.getPeanutReward();
        while (!ii.itemExists(id1))
        {
            id1 = server.RandomRewards.getPeanutReward();
        }
        while (!ii.itemExists(id2))
        {
            id2 = server.RandomRewards.getPeanutReward();
        }
        c.getSession().write(MaplePacketCreator.getPeanutResult(id1, (short) 1, id2, (short) 1, itemId));
        MapleInventoryManipulator.addById(c, id1, (short) 1, ii.getName(itemId) + " 在 " + FileoutputUtil.CurrentReadable_Date());
        MapleInventoryManipulator.addById(c, id2, (short) 1, ii.getName(itemId) + " 在 " + FileoutputUtil.CurrentReadable_Date());
        return true;
    }

    private static boolean ItemPotentialAndMagnify(MapleCharacter player, short itempos, int CSitemId)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        Item item = player.getInventory(MapleInventoryType.EQUIP).getItem(itempos);
        if ((item == null) || (itempos < 0))
        {
            return false;
        }
        Equip toScroll = (Equip) item;
        if (toScroll.getState() < 17)
        {
            return false;
        }
        int stateRate = 4;
        int togiveItem = 0;
        if (CSitemId == 5062009)
        {
            stateRate = 5;
            togiveItem = 2431893;
        }
        else if (CSitemId == 5062010)
        {
            stateRate = 6;
            togiveItem = 2431894;
        }

        toScroll.renewPotential(3, player.isAdmin() ? 99 : stateRate);
        player.forceUpdateItem(item);
        player.getMap().broadcastMessage(InventoryPacket.showPotentialReset(false, player.getId(), true, CSitemId));

        if ((player.getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1) && (togiveItem > 0))
        {
            MapleInventoryManipulator.addById(player.getClient(), togiveItem, (short) 1, "Cube on " + FileoutputUtil.CurrentReadable_Date());
        }

        List<List<StructItemOption>> pots = new LinkedList(ii.getAllPotentialInfo().values());
        int reqLevel = ii.getReqLevel(toScroll.getItemId()) / 10;
        int new_state = Math.abs(toScroll.getPotential1());
        if ((new_state > 20) || (new_state < 17))
        {
            new_state = 17;
        }
        int lines = toScroll.getPotential2() != 0 ? 3 : 2;
        while (toScroll.getState() != new_state)
        {
            for (int i = 0; i < lines; i++)
            {
                boolean rewarded = false;
                while (!rewarded)
                {
                    StructItemOption pot = (StructItemOption) ((List) pots.get(Randomizer.nextInt(pots.size()))).get(reqLevel);
                    if ((pot != null) && (pot.reqLevel / 10 <= reqLevel) && (GameConstants.optionTypeFits(pot.optionType, toScroll.getItemId())) && (GameConstants.optionTypeFitsX(pot.opID,
                            toScroll.getItemId())) && (GameConstants.potentialIDFits(pot.opID, new_state, i)))
                    {
                        if (i == 0)
                        {
                            toScroll.setPotential1(pot.opID);
                        }
                        else if (i == 1)
                        {
                            toScroll.setPotential2(pot.opID);
                        }
                        else if (i == 2)
                        {
                            toScroll.setPotential3(pot.opID);
                        }
                        rewarded = true;
                    }
                }
            }
        }

        if ((toScroll.getState() >= 18) && (toScroll.getStateMsg() < 3))
        {
            if ((toScroll.getState() == 18) && (toScroll.getStateMsg() == 0))
            {
                toScroll.setStateMsg(1);
                player.finishAchievement(52);
                if (!player.isAdmin())
                {
                    String msg = player.getMedalText() + player.getName() + " : 鉴定出 A 级装备，大家祝贺他(她)吧！";
                    WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.itemMegaphone(msg, true, player.getClient().getChannel(), toScroll));
                }
            }
            else if ((toScroll.getState() == 19) && (toScroll.getStateMsg() <= 1))
            {
                toScroll.setStateMsg(2);
                player.finishAchievement(53);
                if (!player.isAdmin())
                {
                    String msg = player.getMedalText() + player.getName() + " : 鉴定出 S 级装备，大家祝贺他(她)吧！";
                    WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.itemMegaphone(msg, true, player.getClient().getChannel(), toScroll));
                }
            }
            else if ((toScroll.getState() == 20) && (toScroll.getStateMsg() <= 2))
            {
                toScroll.setStateMsg(3);
                player.finishAchievement(54);
                if (!player.isAdmin())
                {
                    String msg = player.getMedalText() + player.getName() + " : 鉴定出 SS 级装备，大家祝贺他(她)吧！";
                    WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.itemMegaphone(msg, true, player.getClient().getChannel(), toScroll));
                }
            }
        }
        player.forceUpdateItem(toScroll, true);
        player.getMap().broadcastMessage(InventoryPacket.showMagnifyingEffect(player.getId(), toScroll.getPosition(), false));
        return true;
    }

    private static void changeFace(MapleCharacter player, int color)
    {
        if (player.getFace() % 1000 < 100)
        {
            player.setFace(player.getFace() + color);
        }
        else if ((player.getFace() % 1000 >= 100) && (player.getFace() % 1000 < 200))
        {
            player.setFace(player.getFace() - 100 + color);
        }
        else if ((player.getFace() % 1000 >= 200) && (player.getFace() % 1000 < 300))
        {
            player.setFace(player.getFace() - 200 + color);
        }
        else if ((player.getFace() % 1000 >= 300) && (player.getFace() % 1000 < 400))
        {
            player.setFace(player.getFace() - 300 + color);
        }
        else if ((player.getFace() % 1000 >= 400) && (player.getFace() % 1000 < 500))
        {
            player.setFace(player.getFace() - 400 + color);
        }
        else if ((player.getFace() % 1000 >= 500) && (player.getFace() % 1000 < 600))
        {
            player.setFace(player.getFace() - 500 + color);
        }
        else if ((player.getFace() % 1000 >= 600) && (player.getFace() % 1000 < 700))
        {
            player.setFace(player.getFace() - 600 + color);
        }
        else if ((player.getFace() % 1000 >= 700) && (player.getFace() % 1000 < 800))
        {
            player.setFace(player.getFace() - 700 + color);
        }
        player.updateSingleStat(MapleStat.脸型, player.getFace());
        player.equipChanged();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\UseCashItemHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */