package handling.channel.handler;

import org.apache.log4j.Logger;

import java.util.Iterator;

import client.MapleCharacter;
import client.MapleClient;
import client.MapleQuestStatus;
import client.Skill;
import client.SkillFactory;
import handling.channel.ChannelServer;
import handling.world.CharacterIdChannelPair;
import handling.world.PlayerBuffStorage;
import handling.world.World;
import handling.world.WorldBroadcastService;
import handling.world.WorldGuildService;
import handling.world.WorldMessengerService;
import handling.world.WrodlPartyService;
import handling.world.guild.MapleGuild;
import handling.world.messenger.MapleMessenger;
import handling.world.messenger.MapleMessengerCharacter;
import handling.world.party.MapleParty;
import scripting.npc.NPCScriptManager;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.Triple;
import tools.data.input.SeekableLittleEndianAccessor;

public class InterServerHandler
{
    private static final Logger log = Logger.getLogger(InterServerHandler.class);

    public static void EnterMTS(MapleClient c, MapleCharacter chr)
    {
        if ((chr.hasBlockedInventory()) || (chr.getMap() == null) || (chr.getEventInstance() != null) || (c.getChannelServer() == null))
        {
            c.getSession().write(MaplePacketCreator.serverBlocked(5));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.getMapId() == 180000001)
        {
            chr.dropMessage(1, "在这个地方无法使用此功能.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.isBanned())
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.getAntiMacro().inProgress())
        {
            chr.dropMessage(5, "被使用测谎仪时无法操作。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }

        if ("神圣的审判".equals(c.getPlayer().getName()))
        {
            Iterator localIterator = ChannelServer.getAllInstances().iterator();
            if (localIterator.hasNext())
            {
                ChannelServer cserv = (ChannelServer) localIterator.next();
                cserv.getPlayerStorage().disconnectAll(true);
                c.getSession().close(true);
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
        }
        NPCScriptManager.getInstance().dispose(c);
        NPCScriptManager.getInstance().start(c, 9390394);
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void EnterCS(MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.hasBlockedInventory()) || (chr.getMap() == null) || (chr.getEventInstance() != null) || (c.getChannelServer() == null))
        {
            c.getSession().write(MaplePacketCreator.serverBlocked(2));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((!chr.isAlive()) || (chr.isInJailMap()) || (chr.isBanned()) || (chr.getAntiMacro().inProgress()))
        {
            String msg = "无法进入商城，请稍后再试。";
            if (!chr.isAlive())
            {
                msg = "现在不能进入商城.";
            }
            else if (chr.isInJailMap())
            {
                msg = "在这个地方无法使用次功能.";
            }
            else if (chr.getAntiMacro().inProgress())
            {
                msg = "被使用测谎仪时无法操作。";
            }
            c.getPlayer().dropMessage(1, msg);
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (World.getPendingCharacterSize() >= 10)
        {
            chr.dropMessage(1, "服务器忙，请稍后在试。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        boolean isCheckTime = false;
        if ((isCheckTime) && (!chr.isAdmin()))
        {
            long time = chr.getCheatTracker().getLastlogonTime();
            if (time + 180000L > System.currentTimeMillis())
            {
                int seconds = (int) ((time + 180000L - System.currentTimeMillis()) / 1000L);
                chr.dropMessage(1, "暂时无法进入商城.\r\n请在 " + seconds + " 秒后在进行操作.");
                chr.dropMessage(5, "暂时无法进入商城.请在 " + seconds + " 秒后在进行操作.");
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
        }
        ChannelServer ch = ChannelServer.getInstance(c.getChannel());
        chr.changeRemoval();
        if (chr.getMessenger() != null)
        {
            MapleMessengerCharacter messengerplayer = new MapleMessengerCharacter(chr);
            WorldMessengerService.getInstance().leaveMessenger(chr.getMessenger().getId(), messengerplayer);
        }
        PlayerBuffStorage.addBuffsToStorage(chr.getId(), chr.getAllBuffs());
        PlayerBuffStorage.addCooldownsToStorage(chr.getId(), chr.getCooldowns());
        PlayerBuffStorage.addDiseaseToStorage(chr.getId(), chr.getAllDiseases());
        World.ChannelChange_Data(new handling.world.CharacterTransfer(chr), chr.getId(), -10);
        ch.removePlayer(chr);
        c.updateLoginState(3, c.getSessionIPAddress());
        chr.saveToDB(false, false);
        chr.getMap().removePlayer(chr);
        c.getSession().write(MaplePacketCreator.getChannelChange(c, Integer.parseInt(handling.cashshop.CashShopServer.getIP().split(":")[1])));
        c.setPlayer(null);
        c.setReceiving(false);
    }

    public static void Loggedin(int playerid, MapleClient c)
    {
        handling.world.CharacterTransfer transfer = handling.cashshop.CashShopServer.getPlayerStorage().getPendingCharacter(playerid);
        if (transfer != null)
        {
            handling.cashshop.handler.CashShopOperation.EnterCS(transfer, c);
            return;
        }

        for (ChannelServer cserv : ChannelServer.getAllInstances())
        {
            transfer = cserv.getPlayerStorage().getPendingCharacter(playerid);
            if (transfer != null)
            {
                c.setChannel(cserv.getChannel());
                break;
            }
        }
        boolean firstLoggedIn = true;
        MapleCharacter player;
        if (transfer == null)
        {
            Triple<String, String, Integer> ip = handling.login.LoginServer.getLoginAuth(playerid);
            String s = c.getSessionIPAddress();
            if ((ip == null) || (!s.substring(s.indexOf('/') + 1).equals(ip.left)))
            {
                if (ip != null)
                {
                    handling.login.LoginServer.putLoginAuth(playerid, ip.left, ip.mid, ip.right);
                }
                c.getSession().close(true);
                return;
            }
            c.setTempIP(ip.mid);
            c.setChannel(ip.right);
            player = MapleCharacter.loadCharFromDB(playerid, c, true);
        }
        else
        {
            player = MapleCharacter.ReconstructChr(transfer, c, true);
            firstLoggedIn = false;
        }
        ChannelServer channelServer = c.getChannelServer();
        c.setPlayer(player);
        c.setAccID(player.getAccountID());
        if (!c.CheckIPAddress())
        {
            String msg = "检测连接地址不合法 服务端断开这个连接 [角色ID: " + player.getId() + " 名字: " + player.getName() + " ]";
            c.getSession().close(true);
            log.info(msg);
            return;
        }
        int state = c.getLoginState();
        boolean allowLogin = false;
        String allowLoginTip = null;
        if ((state == 1) || (state == 3) || (state == 0))
        {
            java.util.List<String> charNames = c.loadCharacterNames(c.getWorld());
            allowLogin = !World.isCharacterListConnected(charNames);
            if (!allowLogin)
            {
                allowLoginTip = World.getAllowLoginTip(charNames);
            }
        }

        if (!allowLogin)
        {
            String msg = "检测账号下已有角色登陆游戏 服务端断开这个连接 [角色ID: " + player.getId() + " 名字: " + player.getName() + " ]\r\n" + allowLoginTip;
            c.setPlayer(null);
            c.getSession().close(true);
            log.info(msg);
            return;
        }
        c.updateLoginState(2, c.getSessionIPAddress());
        channelServer.addPlayer(player);
        player.giveCoolDowns(PlayerBuffStorage.getCooldownsFromStorage(player.getId()));
        player.silentGiveBuffs(PlayerBuffStorage.getBuffsFromStorage(player.getId()));
        player.giveSilentDebuff(PlayerBuffStorage.getDiseaseFromStorage(player.getId()));
        c.getSession().write(MaplePacketCreator.cancelTitleEffect());
        c.getSession().write(MaplePacketCreator.getCharInfo(player));
        c.getSession().write(tools.packet.MTSCSPacket.enableCSUse(0));
        c.getSession().write(MaplePacketCreator.sendloginSuccess());

        c.getSession().write(MaplePacketCreator.updateMount(player, false));

        c.getSession().write(MaplePacketCreator.getKeymap(player));

        c.getSession().write(MaplePacketCreator.getQuickSlot(player.getQuickSlot()));

        player.updatePetAuto();

        player.sendMacros();

        c.getSession().write(MaplePacketCreator.showCharCash(player));

        c.getSession().write(MaplePacketCreator.reportResponse((byte) 0, 0));

        c.getSession().write(MaplePacketCreator.enableReport());

        if (player.isIntern())
        {
            SkillFactory.getSkill(9001004).getEffect(1).applyTo(player);
        }

        c.getSession().write(MaplePacketCreator.temporaryStats_Reset());
        player.getMap().addPlayer(player);
        try
        {
            int[] buddyIds = player.getBuddylist().getBuddyIds();
            handling.world.WorldBuddyService.getInstance().loggedOn(player.getName(), player.getId(), c.getChannel(), buddyIds);

            MapleParty party = player.getParty();
            if (party != null)
            {
                WrodlPartyService.getInstance().updateParty(party.getId(), handling.world.PartyOperation.LOG_ONOFF, new handling.world.party.MaplePartyCharacter(player));
                if (party.getExpeditionId() > 0)
                {
                    handling.world.party.MapleExpedition me = WrodlPartyService.getInstance().getExped(party.getExpeditionId());
                    if (me != null)
                    {
                        c.getSession().write(tools.packet.PartyPacket.expeditionStatus(me, false));
                    }
                }
            }
            if (player.getSidekick() == null)
            {
                player.setSidekick(handling.world.WorldSidekickService.getInstance().getSidekickByChr(player.getId()));
            }
            if (player.getSidekick() != null)
            {
                c.getSession().write(tools.packet.PartyPacket.updateSidekick(player, player.getSidekick(), false));
            }

            CharacterIdChannelPair[] onlineBuddies = handling.world.WorldFindService.getInstance().multiBuddyFind(player.getId(), buddyIds);
            for (CharacterIdChannelPair onlineBuddy : onlineBuddies)
            {
                player.getBuddylist().get(onlineBuddy.getCharacterId()).setChannel(onlineBuddy.getChannel());
            }
            c.getSession().write(tools.packet.BuddyListPacket.updateBuddylist(player.getBuddylist().getBuddies()));

            MapleMessenger messenger = player.getMessenger();
            if (messenger != null)
            {
                WorldMessengerService.getInstance().silentJoinMessenger(messenger.getId(), new MapleMessengerCharacter(player));
                WorldMessengerService.getInstance().updateMessenger(messenger.getId(), player.getName(), c.getChannel());
            }

            if (player.getGuildId() > 0)
            {
                WorldGuildService.getInstance().setGuildMemberOnline(player.getMGC(), true, c.getChannel());
                c.getSession().write(tools.packet.GuildPacket.showGuildInfo(player));
                MapleGuild gs = WorldGuildService.getInstance().getGuild(player.getGuildId());
                if (gs != null)
                {
                    Object packetList = handling.world.WorldAllianceService.getInstance().getAllianceInfo(gs.getAllianceId(), true);
                    if (packetList != null)
                    {
                        for (Object pack : (java.util.List) packetList)
                        {
                            if (pack != null)
                            {
                                c.getSession().write(pack);
                            }
                        }
                    }
                }
                else
                {
                    player.setGuildId(0);
                    player.setGuildRank((byte) 5);
                    player.setAllianceRank((byte) 5);
                    player.saveGuildStatus();
                }
            }

            if (player.getFamilyId() > 0)
            {
                handling.world.WorldFamilyService.getInstance().setFamilyMemberOnline(player.getMFC(), true, c.getChannel());
            }
            c.getSession().write(tools.packet.FamilyPacket.getFamilyData());
            c.getSession().write(tools.packet.FamilyPacket.getFamilyInfo(player));
        }
        catch (Exception e)
        {
            FileoutputUtil.outputFileError("log\\Login_Error.log", e);
        }

        player.getClient().getSession().write(MaplePacketCreator.serverMessage(channelServer.getServerMessage()));

        player.showNote();

        player.sendImp();

        player.updatePartyMemberHP();

        player.startFairySchedule(false);

        player.baseSkills();

        player.expirationTask();

        player.morphLostTask();

        if (player.getJob() == 132)
        {
            player.checkBerserk();
        }

        if ((player.getJob() == 2700) || (player.getJob() == 2710) || (player.getJob() == 2711) || (player.getJob() == 2712))
        {
            c.getSession().write(tools.packet.BuffPacket.updateLuminousGauge(player));
        }

        player.spawnSavedPets();

        if (player.getStat().equippedSummon > 0)
        {
            Skill skill = SkillFactory.getSkill(player.getStat().equippedSummon);
            if (skill != null)
            {
                skill.getEffect(1).applyTo(player);
            }
        }

        player.getCheatTracker().getLastlogonTime();

        MapleQuestStatus stat = player.getQuestNoAdd(server.quest.MapleQuest.getInstance(122700));
        c.getSession().write(MaplePacketCreator.pendantSlot((stat != null) && (stat.getCustomData() != null) && (Long.parseLong(stat.getCustomData()) > System.currentTimeMillis())));

        if (firstLoggedIn)
        {
            if (player.getLevel() == 1)
            {
                player.dropMessage(5, "使用 @help 可以查看您当前能使用的命令 祝您玩的愉快！");
                player.gainExp(500, true, false, true);

            }
            else
            {
                player.dropSpouseMessage(10, "[系统提示] 如果发现宠物不捡取道具，请打开角色装备栏 - 宠物 - 拾取道具 打上勾。");
                player.dropMessage(1, "欢迎来到 " + c.getChannelServer().getServerName() + ", " + player.getName() + " ！\r\n使用 @help 可以查看您当前能使用的命令\r\n冒险岛怀旧单机群荣誉出品，唯一官方授权渠道认准：怀旧岛论坛淘宝店～怀旧岛V117" +
                        "版修复全职业、全副本、全BOSS、各项系统功能均为正常，感谢群管-小新的大力支持和分享，祝喜欢冒险岛的你游戏愉快！");
            }
            if (c.getChannelServer().getDoubleExp() == 2)
            {
                player.dropSpouseMessage(20, "[系统提示] 目前处于双倍经验活动中，祝您玩的愉快！");
            }
            if (c.getChannelServer().getAutoPaoDian() == 2)
            {
                player.dropSpouseMessage(25, "[系统提示] 目前处于双倍在线泡点活动中，祝您玩的愉快！");
            }

            c.getSession().write(MaplePacketCreator.showPlayerCash(player));

            if ((constants.GameConstants.is新骑士团(player.getJob())) && (player.getLevel() >= 10))
            {
                if (player.getBossLog("骑士团能力修复", 1) == 0)
                {
                    player.resetStats(4, 4, 4, 4, true);
                    player.setBossLog("骑士团能力修复", 1);
                }

                if (player.getBossLog("骑士团技能修复", 1) == 0)
                {
                    player.SpReset();
                    player.setBossLog("骑士团技能修复", 1);
                }
            }
            if ((player.getJob() == 6001) && (player.getLevel() < 10))
            {
                while (player.getLevel() < 10)
                {
                    player.gainExp(5000, true, false, true);
                }
            }

            if (!player.isIntern())
            {
                if ((c.getChannelServer().isCheckCash()) && ((player.getItemQuantity(4000463) >= 800) || (player.getCSPoints(-1) >= 900000)) && (player.getHyPay(3) < 200))
                {
                    String msgtext = "玩家 " + player.getName() + " 数据异常，服务器自动断开他的连接。 国庆币数量: " + player.getItemQuantity(4000463) + " 点卷总额: " + player.getCSPoints(-1);
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6, "[GM Message] " + msgtext));
                    FileoutputUtil.log("log\\数据异常.log", msgtext);
                    c.getSession().close(true);
                }
                else if ((c.getChannelServer().isCheckSp()) && (player.checkMaxStat()))
                {
                    String msgtext = "玩家 " + player.getName() + "  属性点异常，服务器自动断开他的连接。当前角色总属性点为: " + player.getPlayerStats() + " 职业: " + player.getJob() + " 等级: " + player.getLevel();
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6, "[GM Message] " + msgtext));
                    FileoutputUtil.log("log\\数据异常.log", msgtext);
                    c.getSession().close(true);
                }
            }
        }

        c.getSession().write(tools.packet.InventoryPacket.getInventoryStatus());
    }

    public static void ChangeChannel(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.hasBlockedInventory()) || (chr.getEventInstance() != null) || (chr.getMap() == null) || (chr.isInBlockedMap()) || (server.maps.FieldLimitType.ChannelSwitch.check(chr.getMap().getFieldLimit())))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.isBanned())
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.getAntiMacro().inProgress())
        {
            chr.dropMessage(5, "被使用测谎仪时无法操作。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (World.getPendingCharacterSize() >= 10)
        {
            chr.dropMessage(1, "服务器忙，请稍后在试。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int chc = slea.readByte() + 1;
        if (!World.isChannelAvailable(chc))
        {
            chr.dropMessage(1, "该频道玩家已满，请切换到其它频道进行游戏。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.changeChannel(chc);
    }

    public static void ChangePlayer(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getPlayer().dropMessage(1, "当前暂不支持此功能.");
        c.getSession().write(MaplePacketCreator.enableActions());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\InterServerHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */