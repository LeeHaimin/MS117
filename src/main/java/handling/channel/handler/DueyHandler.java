package handling.channel.handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.ItemLoader;
import client.inventory.MapleInventoryType;
import constants.GameConstants;
import constants.ItemConstants;
import database.DatabaseConnection;
import handling.channel.ChannelServer;
import handling.world.WorldFindService;
import server.MapleDueyActions;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;


public class DueyHandler
{
    public static void DueyOperation(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        byte operation = slea.readByte();
        int packageid;
        switch (operation)
        {
            case 1:
                String AS13Digit = slea.readMapleAsciiString();
                int conv = c.getPlayer().getConversation();
                if (conv == 2)
                {
                    c.getSession().write(MaplePacketCreator.sendDuey((byte) 10, loadItems(c.getPlayer())));
                }

                break;
            case 3:
                if (c.getPlayer().getConversation() != 2)
                {
                    return;
                }
                byte inventId = slea.readByte();
                short itemPos = slea.readShort();
                short amount = slea.readShort();
                int mesos = slea.readInt();
                String recipient = slea.readMapleAsciiString();
                boolean quickdelivery = slea.readByte() > 0;

                int finalcost = mesos + GameConstants.getTaxAmount(mesos) + (quickdelivery ? 0 : 5000);

                if ((mesos >= 0) && (mesos <= 100000000) && (c.getPlayer().getMeso() >= finalcost))
                {
                    int accid = getAccIdFromName(recipient, true);
                    if (accid != -1)
                    {
                        if (accid != c.getAccID())
                        {
                            boolean recipientOn = false;
                            int chz = WorldFindService.getInstance().findChannel(recipient);
                            if (chz > 0)
                            {
                                MapleCharacter receiver = ChannelServer.getInstance(chz).getPlayerStorage().getCharacterByName(recipient);
                                if (receiver != null)
                                {
                                    recipientOn = true;
                                }
                            }
                            if (inventId > 0)
                            {
                                MapleInventoryType inv = MapleInventoryType.getByType(inventId);
                                Item item = c.getPlayer().getInventory(inv).getItem((byte) itemPos);
                                if (item == null)
                                {
                                    c.getSession().write(MaplePacketCreator.sendDuey((byte) 17, null));
                                    return;
                                }
                                short flag = item.getFlag();
                                if ((ItemFlag.UNTRADEABLE.check(flag)) || (ItemFlag.LOCK.check(flag)))
                                {
                                    c.getSession().write(MaplePacketCreator.enableActions());
                                    return;
                                }
                                if (c.getPlayer().getItemQuantity(item.getItemId(), false) >= amount)
                                {
                                    MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
                                    if ((!ii.isDropRestricted(item.getItemId())) && (!ii.isAccountShared(item.getItemId())))
                                    {
                                        if (addItemToDB(item, amount, mesos, c.getPlayer().getName(), getAccIdFromName(recipient, false), recipientOn))
                                        {
                                            if ((ItemConstants.is飞镖道具(item.getItemId())) || (ItemConstants.is子弹道具(item.getItemId())))
                                            {
                                                MapleInventoryManipulator.removeFromSlot(c, inv, (byte) itemPos, item.getQuantity(), true);
                                            }
                                            else
                                            {
                                                MapleInventoryManipulator.removeFromSlot(c, inv, (byte) itemPos, amount, true, false);
                                            }
                                            c.getPlayer().gainMeso(-finalcost, false);
                                            c.getSession().write(MaplePacketCreator.sendDuey((byte) 19, null));
                                        }
                                        else
                                        {
                                            c.getSession().write(MaplePacketCreator.sendDuey((byte) 17, null));
                                        }
                                    }
                                    else
                                    {
                                        c.getSession().write(MaplePacketCreator.sendDuey((byte) 17, null));
                                    }
                                }
                                else
                                {
                                    c.getSession().write(MaplePacketCreator.sendDuey((byte) 17, null));
                                }
                            }
                            else if (addMesoToDB(mesos, c.getPlayer().getName(), getAccIdFromName(recipient, false), recipientOn))
                            {
                                c.getPlayer().gainMeso(-finalcost, false);
                                c.getSession().write(MaplePacketCreator.sendDuey((byte) 19, null));
                            }
                            else
                            {
                                c.getSession().write(MaplePacketCreator.sendDuey((byte) 17, null));
                            }
                        }
                        else
                        {
                            c.getSession().write(MaplePacketCreator.sendDuey((byte) 15, null));
                        }
                    }
                    else
                    {
                        c.getSession().write(MaplePacketCreator.sendDuey((byte) 14, null));
                    }
                }
                else
                {
                    c.getSession().write(MaplePacketCreator.sendDuey((byte) 12, null));
                }
                break;

            case 5:
                if (c.getPlayer().getConversation() != 2)
                {
                    return;
                }
                packageid = slea.readInt();

                MapleDueyActions dp = loadSingleItem(packageid, c.getPlayer().getId());
                if (dp == null)
                {
                    return;
                }
                if ((dp.getItem() != null) && (!MapleInventoryManipulator.checkSpace(c, dp.getItem().getItemId(), dp.getItem().getQuantity(), dp.getItem().getOwner())))
                {
                    c.getSession().write(MaplePacketCreator.sendDuey((byte) 22, null));
                    return;
                }
                if ((dp.getMesos() < 0) || (dp.getMesos() + c.getPlayer().getMeso() < 0))
                {
                    c.getSession().write(MaplePacketCreator.sendDuey((byte) 17, null));
                    return;
                }
                removeItemFromDB(packageid, c.getPlayer().getId());
                if (dp.getItem() != null)
                {
                    MapleInventoryManipulator.addFromDrop(c, dp.getItem(), false);
                }
                if (dp.getMesos() != 0)
                {
                    c.getPlayer().gainMeso(dp.getMesos(), false);
                }
                c.getSession().write(MaplePacketCreator.removeItemFromDuey(false, packageid));
                break;

            case 6:
                if (c.getPlayer().getConversation() != 2)
                {
                    return;
                }
                packageid = slea.readInt();
                removeItemFromDB(packageid, c.getPlayer().getId());
                c.getSession().write(MaplePacketCreator.removeItemFromDuey(true, packageid));
                break;

            case 8:
                c.getPlayer().setConversation(0);
                break;
            case 2:
            case 4:
            case 7:
            default:
                System.out.println("Unhandled Duey operation : " + slea.toString());
        }
    }

    public static List<MapleDueyActions> loadItems(MapleCharacter chr)
    {
        List<MapleDueyActions> packages = new LinkedList<>();
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM dueypackages WHERE RecieverId = ?");
            ps.setInt(1, chr.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                MapleDueyActions dueypack = getItemByPID(rs.getInt("packageid"));
                dueypack.setSender(rs.getString("SenderName"));
                dueypack.setMesos(rs.getInt("Mesos"));
                dueypack.setSentTime(rs.getLong("TimeStamp"));
                packages.add(dueypack);
            }
            rs.close();
            ps.close();
            return packages;
        }
        catch (SQLException se)
        {
            se.printStackTrace();
        }
        return null;
    }

    private static int getAccIdFromName(String name, boolean accountid)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM characters WHERE name = ?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                return -1;
            }
            int id_ = accountid ? rs.getInt("accountid") : rs.getInt("id");
            rs.close();
            ps.close();
            return id_;
        }
        catch (SQLException ignored)
        {
        }
        return -1;
    }

    private static boolean addItemToDB(Item item, int quantity, int mesos, String sName, int recipientID, boolean isOn)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("INSERT INTO dueypackages (RecieverId, SenderName, Mesos, TimeStamp, Checked, Type) VALUES (?, ?, ?, ?, ?, ?)"
                    , 1);
            ps.setInt(1, recipientID);
            ps.setString(2, sName);
            ps.setInt(3, mesos);
            ps.setLong(4, System.currentTimeMillis());
            ps.setInt(5, isOn ? 0 : 1);

            ps.setInt(6, item.getType());
            ps.executeUpdate();
            ps.close();
            if (item != null)
            {
                ItemLoader.送货道具.saveItems(Collections.singletonList(new Pair(item, ItemConstants.getInventoryType(item.getItemId()))), recipientID);
            }
            return true;
        }
        catch (SQLException se)
        {
            se.printStackTrace();
        }
        return false;
    }

    private static boolean addMesoToDB(int mesos, String sName, int recipientID, boolean isOn)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("INSERT INTO dueypackages (RecieverId, SenderName, Mesos, TimeStamp, Checked, Type) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setInt(1, recipientID);
            ps.setString(2, sName);
            ps.setInt(3, mesos);
            ps.setLong(4, System.currentTimeMillis());
            ps.setInt(5, isOn ? 0 : 1);
            ps.setInt(6, 3);

            ps.executeUpdate();
            ps.close();

            return true;
        }
        catch (SQLException se)
        {
            se.printStackTrace();
        }
        return false;
    }

    public static MapleDueyActions loadSingleItem(int packageid, int charid)
    {
        List<MapleDueyActions> packages = new LinkedList<>();
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM dueypackages WHERE PackageId = ? and RecieverId = ?");
            ps.setInt(1, packageid);
            ps.setInt(2, charid);
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                MapleDueyActions dueypack = getItemByPID(packageid);
                dueypack.setSender(rs.getString("SenderName"));
                dueypack.setMesos(rs.getInt("Mesos"));
                dueypack.setSentTime(rs.getLong("TimeStamp"));
                packages.add(dueypack);
                rs.close();
                ps.close();
                return dueypack;
            }
            rs.close();
            ps.close();
            return null;
        }
        catch (SQLException ignored)
        {
        }
        return null;
    }

    private static void removeItemFromDB(int packageid, int charid)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("DELETE FROM dueypackages WHERE PackageId = ? and RecieverId = ?");
            ps.setInt(1, packageid);
            ps.setInt(2, charid);
            ps.executeUpdate();
            ps.close();
            ItemLoader.送货道具.saveItems(null, packageid);
        }
        catch (SQLException se)
        {
            se.printStackTrace();
        }
    }

    private static MapleDueyActions getItemByPID(int packageid)
    {
        try
        {
            Map<Long, Pair<Item, MapleInventoryType>> iter = ItemLoader.送货道具.loadItems(false, packageid);
            if ((iter != null) && (iter.size() > 0))
            {
                Iterator localIterator = iter.values().iterator();
                if (localIterator.hasNext())
                {
                    Pair<Item, MapleInventoryType> i = (Pair) localIterator.next();
                    return new MapleDueyActions(packageid, i.getLeft());
                }
            }
        }
        catch (Exception se)
        {
            se.printStackTrace();
        }
        return new MapleDueyActions(packageid);
    }

    public static void reciveMsg(MapleClient c, int recipientId)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE dueypackages SET Checked = 0 where RecieverId = ?");
            ps.setInt(1, recipientId);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException se)
        {
            se.printStackTrace();
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\DueyHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */