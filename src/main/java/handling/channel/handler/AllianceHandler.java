package handling.channel.handler;

import client.MapleCharacter;
import client.MapleClient;
import handling.world.WorldAllianceService;
import handling.world.WorldGuildService;
import handling.world.guild.MapleGuild;
import tools.data.input.SeekableLittleEndianAccessor;

public class AllianceHandler
{
    public static void HandleAlliance(SeekableLittleEndianAccessor slea, MapleClient c, boolean denied)
    {
        if (c.getPlayer().getGuildId() <= 0)
        {
            c.getSession().write(tools.MaplePacketCreator.enableActions());
            return;
        }
        MapleGuild gs = WorldGuildService.getInstance().getGuild(c.getPlayer().getGuildId());
        if (gs == null)
        {
            c.getSession().write(tools.MaplePacketCreator.enableActions());
            return;
        }
        WorldAllianceService allianceService = WorldAllianceService.getInstance();

        byte op = slea.readByte();
        if ((c.getPlayer().getGuildRank() != 1) && (op != 1))
        {
            return;
        }
        if (op == 22)
        {
            denied = true;
        }
        int leaderid = 0;
        if (gs.getAllianceId() > 0)
        {
            leaderid = allianceService.getAllianceLeader(gs.getAllianceId());
        }

        if ((op != 4) && (!denied))
        {
            if ((gs.getAllianceId() > 0) && (leaderid > 0))
            {
            }

        }
        else if ((leaderid > 0) || (gs.getAllianceId() > 0))
        {
            return;
        }
        if (denied)
        {
            DenyInvite(c, gs);
            return;
        }


        switch (op)
        {
            case 1:
                for (byte[] pack : allianceService.getAllianceInfo(gs.getAllianceId(), false))
                {
                    if (pack != null)
                    {
                        c.getSession().write(pack);
                    }
                }
                break;
            case 3:
                int newGuild = WorldGuildService.getInstance().getGuildLeader(slea.readMapleAsciiString());
                if ((newGuild > 0) && (c.getPlayer().getAllianceRank() == 1) && (leaderid == c.getPlayer().getId()))
                {
                    MapleCharacter chr = c.getChannelServer().getPlayerStorage().getCharacterById(newGuild);
                    if ((chr != null) && (chr.getGuildId() > 0) && (allianceService.canInvite(gs.getAllianceId())))
                    {
                        chr.getClient().getSession().write(tools.packet.GuildPacket.sendAllianceInvite(allianceService.getAlliance(gs.getAllianceId()).getName(), c.getPlayer()));
                        WorldGuildService.getInstance().setInvitedId(chr.getGuildId(), gs.getAllianceId());
                    }
                    else
                    {
                        c.getPlayer().dropMessage(1, "请确认要联盟的家族族长和您是在同一频道.");
                    }
                }
                else
                {
                    c.getPlayer().dropMessage(1, "输入的家族名字不正确，当前服务器未找到该家族的信息.");
                }
                break;
            case 4:
                int inviteid = WorldGuildService.getInstance().getInvitedId(c.getPlayer().getGuildId());
                if (inviteid > 0)
                {
                    if (!allianceService.addGuildToAlliance(inviteid, c.getPlayer().getGuildId()))
                    {
                        c.getPlayer().dropMessage(5, "An error occured when adding guild.");
                    }
                    WorldGuildService.getInstance().setInvitedId(c.getPlayer().getGuildId(), 0);
                }
                break;
            case 2:
            case 6:
                int gid;
                if ((op == 6) && (slea.available() >= 4L))
                {
                    gid = slea.readInt();
                    if ((slea.available() >= 4L) && (gs.getAllianceId() != slea.readInt()))
                    {
                        break;
                    }
                }
                else
                {
                    gid = c.getPlayer().getGuildId();
                }
                if ((c.getPlayer().getAllianceRank() <= 2) && ((c.getPlayer().getAllianceRank() == 1) || (c.getPlayer().getGuildId() == gid)))
                {
                    if (!allianceService.removeGuildFromAlliance(gs.getAllianceId(), gid, c.getPlayer().getGuildId() != gid))
                    {
                        c.getPlayer().dropMessage(5, "An error occured when removing guild.");
                    }
                }
                break;
            case 7:
                if ((c.getPlayer().getAllianceRank() == 1) && (leaderid == c.getPlayer().getId()) && (!allianceService.changeAllianceLeader(gs.getAllianceId(), slea.readInt())))
                {
                    c.getPlayer().dropMessage(5, "An error occured when changing leader.");
                }

                break;
            case 8:
                if ((c.getPlayer().getAllianceRank() == 1) && (leaderid == c.getPlayer().getId()))
                {
                    String[] ranks = new String[5];
                    for (int i = 0; i < 5; i++)
                    {
                        ranks[i] = slea.readMapleAsciiString();
                    }
                    allianceService.updateAllianceRanks(gs.getAllianceId(), ranks);
                }
                break;

            case 9:
                if ((c.getPlayer().getAllianceRank() <= 2) && (!allianceService.changeAllianceRank(gs.getAllianceId(), slea.readInt(), slea.readByte())))
                {
                    c.getPlayer().dropMessage(5, "An error occured when changing rank.");
                }

                break;
            case 10:
                if (c.getPlayer().getAllianceRank() <= 2)
                {
                    String notice = slea.readMapleAsciiString();
                    if (notice.length() <= 100)
                    {

                        allianceService.updateAllianceNotice(gs.getAllianceId(), notice);
                    }
                }
                break;
            case 5:
            default:
                System.out.println("Unhandled GuildAlliance op: " + op + ", \n" + slea.toString());
        }

    }

    public static void DenyInvite(MapleClient c, MapleGuild gs)
    {
        int inviteid = WorldGuildService.getInstance().getInvitedId(c.getPlayer().getGuildId());
        if (inviteid > 0)
        {
            int newAlliance = WorldAllianceService.getInstance().getAllianceLeader(inviteid);
            if (newAlliance > 0)
            {
                MapleCharacter chr = c.getChannelServer().getPlayerStorage().getCharacterById(newAlliance);
                if (chr != null)
                {
                    chr.dropMessage(5, "[" + gs.getName() + "] 家族拒绝了联盟的邀请.");
                }
                WorldGuildService.getInstance().setInvitedId(c.getPlayer().getGuildId(), 0);
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\AllianceHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */