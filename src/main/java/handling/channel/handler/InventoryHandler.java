package handling.channel.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.MapleQuestStatus;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import constants.GameConstants;
import constants.ItemConstants;
import handling.world.WorldBroadcastService;
import scripting.item.ItemScriptManager;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.MapleStatEffect;
import server.Randomizer;
import server.StructItemOption;
import server.StructRewardItem;
import server.maps.FieldLimitType;
import server.maps.MapleMap;
import server.maps.MapleMapItem;
import server.shops.HiredMerchant;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.InventoryPacket;

public class InventoryHandler
{
    public static final int OWL_ID = 1;

    public static void ItemMove(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().hasBlockedInventory()))
        {
            return;
        }
        c.getPlayer().setScrolledPosition((short) 0);
        c.getPlayer().updateTick(slea.readInt());
        MapleInventoryType type = MapleInventoryType.getByType(slea.readByte());
        short src = slea.readShort();
        short dst = slea.readShort();
        short quantity = slea.readShort();
        if ((src < 0) && (dst > 0))
        {
            MapleInventoryManipulator.unequip(c, src, dst);
        }
        else if (dst < 0)
        {
            MapleInventoryManipulator.equip(c, src, dst);
        }
        else if (dst == 0)
        {
            MapleInventoryManipulator.drop(c, type, src, quantity);
        }
        else
        {
            MapleInventoryManipulator.move(c, type, src, dst);
        }
    }


    public static void SwitchBag(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (c.getPlayer().hasBlockedInventory())
        {
            return;
        }
        c.getPlayer().setScrolledPosition((short) 0);
        c.getPlayer().updateTick(slea.readInt());
        short src = (short) slea.readInt();
        short dst = (short) slea.readInt();
        if ((src < 100) || (dst < 100))
        {
            return;
        }
        MapleInventoryManipulator.move(c, MapleInventoryType.ETC, src, dst);
    }


    public static void MoveBag(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (c.getPlayer().hasBlockedInventory())
        {
            return;
        }
        c.getPlayer().setScrolledPosition((short) 0);
        c.getPlayer().updateTick(slea.readInt());
        boolean srcFirst = slea.readInt() > 0;
        if (slea.readByte() != 4)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        short dst = (short) slea.readInt();
        short src = slea.readShort();
        MapleInventoryManipulator.move(c, MapleInventoryType.ETC, srcFirst ? dst : src, srcFirst ? src : dst);
    }


    public static void ItemSort(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getPlayer().updateTick(slea.readInt());
        c.getPlayer().setScrolledPosition((short) 0);
        MapleInventoryType pInvType = MapleInventoryType.getByType(slea.readByte());
        if ((pInvType == MapleInventoryType.UNDEFINED) || (c.getPlayer().hasBlockedInventory()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleInventory pInv = c.getPlayer().getInventory(pInvType);
        boolean sorted = false;
        while (!sorted)
        {
            byte freeSlot = (byte) pInv.getNextFreeSlot();
            if (freeSlot != -1)
            {
                byte itemSlot = -1;
                for (byte i = (byte) (freeSlot + 1); i <= pInv.getSlotLimit(); i = (byte) (i + 1))
                {
                    if (pInv.getItem(i) != null)
                    {
                        itemSlot = i;
                        break;
                    }
                }
                if (itemSlot > 0)
                {
                    MapleInventoryManipulator.move(c, pInvType, itemSlot, freeSlot);
                }
                else
                {
                    sorted = true;
                }
            }
            else
            {
                sorted = true;
            }
        }
        c.getSession().write(MaplePacketCreator.finishedSort(pInvType.getType()));
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void ItemGather(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getPlayer().updateTick(slea.readInt());
        c.getPlayer().setScrolledPosition((short) 0);
        if (c.getPlayer().hasBlockedInventory())
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        byte mode = slea.readByte();
        if (mode == 5)
        {
            c.getPlayer().dropMessage(1, "特殊栏道具暂不开放已种类道具排列.");
            c.getSession().write(MaplePacketCreator.finishedGather(mode));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleInventoryType invType = MapleInventoryType.getByType(mode);
        MapleInventory Inv = c.getPlayer().getInventory(invType);

        LinkedList itemMap = new LinkedList<>();
        for (Item item : Inv.list())
        {
            itemMap.add(item.copy());
        }
        for (Object o : itemMap)
        {
            Item itemStats = (Item) o;
            MapleInventoryManipulator.removeFromSlot(c, invType, itemStats.getPosition(), itemStats.getQuantity(), true, false);
        }
        List<Item> sortedItems = sortItems(itemMap);
        for (Item item : sortedItems)
        {
            MapleInventoryManipulator.addFromDrop(c, item, false);
        }
        c.getSession().write(MaplePacketCreator.finishedGather(mode));
        c.getSession().write(MaplePacketCreator.enableActions());
        itemMap.clear();
        sortedItems.clear();
    }

    private static List<Item> sortItems(List<Item> passedMap)
    {
        ArrayList itemIds = new ArrayList<>();
        for (Item item : passedMap)
        {
            itemIds.add(item.getItemId());
        }
        Collections.sort(itemIds);
        Object sortedList = new LinkedList<>();
        for (Object itemId : itemIds)
        {
            Integer val = (Integer) itemId;
            for (Item item : passedMap)
                if (val == item.getItemId())
                {
                    ((List) sortedList).add(item);
                    passedMap.remove(item);
                    break;
                }
        }
        Integer val;
        return (List<Item>) sortedList;
    }

    public static boolean UseRewardItem(byte slot, int itemId, MapleClient c, MapleCharacter chr)
    {
        Item toUse = c.getPlayer().getInventory(ItemConstants.getInventoryType(itemId)).getItem(slot);
        c.getSession().write(MaplePacketCreator.enableActions());
        if ((toUse != null) && (toUse.getQuantity() >= 1) && (toUse.getItemId() == itemId) && (!chr.hasBlockedInventory()))
        {
            if ((chr.getInventory(MapleInventoryType.EQUIP).getNextFreeSlot() > -1) && (chr.getInventory(MapleInventoryType.USE).getNextFreeSlot() > -1) && (chr.getInventory(MapleInventoryType.SETUP).getNextFreeSlot() > -1) && (chr.getInventory(MapleInventoryType.ETC).getNextFreeSlot() > -1))
            {
                MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
                int gainmes;
                if (itemId == 2028048)
                {
                    int mesars = 5000000;
                    if ((mesars > 0) && (chr.getMeso() < Integer.MAX_VALUE - mesars))
                    {
                        gainmes = Randomizer.nextInt(mesars);
                        chr.gainMeso(gainmes, true, true);
                        c.getSession().write(tools.packet.MTSCSPacket.sendMesobagSuccess(gainmes));

                        MapleInventoryManipulator.removeFromSlot(c, ItemConstants.getInventoryType(itemId), slot, (short) 1, false);
                        return true;
                    }
                    chr.dropMessage(1, "金币已达到上限无法使用这个道具.");
                    return false;
                }

                tools.Pair<Integer, List<StructRewardItem>> rewards = ii.getRewardItem(itemId);

                if ((rewards != null) && (rewards.getLeft() > 0))
                {
                    for (; ; )
                    {
                        for (StructRewardItem reward : rewards.getRight())
                        {
                            if ((reward.prob > 0) && (Randomizer.nextInt(rewards.getLeft()) < reward.prob))
                            {
                                if (ItemConstants.getInventoryType(reward.itemid) == MapleInventoryType.EQUIP)
                                {
                                    Item item = ii.getEquipById(reward.itemid);
                                    if (reward.period > 0L)
                                    {
                                        item.setExpiration(System.currentTimeMillis() + reward.period * 60L * 1000L);
                                    }
                                    item.setGMLog("Reward item: " + itemId + " on " + FileoutputUtil.CurrentReadable_Date());
                                    if (chr.isAdmin())
                                    {
                                        chr.dropMessage(5, "打开道具获得: " + item.getItemId());
                                    }
                                    if (reward.itemid / 1000 == 1182)
                                    {
                                        ii.randomize休彼德蔓徽章((Equip) item);
                                    }
                                    MapleInventoryManipulator.addbyItem(c, item);
                                    c.getSession().write(MaplePacketCreator.getShowItemGain(item.getItemId(), item.getQuantity(), true));
                                }
                                else
                                {
                                    if (chr.isAdmin())
                                    {
                                        chr.dropMessage(5, "打开道具获得: " + reward.itemid + " - " + reward.quantity);
                                    }
                                    MapleInventoryManipulator.addById(c, reward.itemid, reward.quantity, "Reward item: " + itemId + " on " + FileoutputUtil.CurrentReadable_Date());
                                    c.getSession().write(MaplePacketCreator.getShowItemGain(reward.itemid, reward.quantity, true));
                                }

                                MapleInventoryManipulator.removeFromSlot(c, ItemConstants.getInventoryType(itemId), slot, (short) 1, false);
                                c.getSession().write(MaplePacketCreator.showRewardItemAnimation(reward.itemid, reward.effect));
                                chr.getMap().broadcastMessage(chr, MaplePacketCreator.showRewardItemAnimation(reward.itemid, reward.effect, chr.getId()), false);
                                return true;
                            }
                        }
                    }
                }
                chr.dropMessage(6, "出现未知错误.");
            }
            else
            {
                chr.dropMessage(6, "背包空间不足。");
            }
        }
        return false;
    }


    public static void UseItem(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (!chr.isAlive()) || (chr.getMapId() == 749040100) || (chr.getMap() == null) || (chr.hasDisease(client.MapleDisease.POTION)) || (chr.hasBlockedInventory()) || (chr.inPVP()) || (chr.getMap().isPvpMaps()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        long time = System.currentTimeMillis();
        if (chr.getNextConsume() > time)
        {
            chr.dropMessage(5, "暂时无法使用这个道具，请稍后在试。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        c.getPlayer().updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((itemId >= 2003516) && (itemId <= 2003519))
        {
            chr.dropMessage(1, "无法使用这个道具效果");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (!FieldLimitType.PotionUse.check(chr.getMap().getFieldLimit()))
        {
            if (MapleItemInformationProvider.getInstance().getItemEffect(toUse.getItemId()).applyTo(chr))
            {
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                if (chr.getMap().getConsumeItemCoolTime() > 0)
                {
                    chr.setNextConsume(time + chr.getMap().getConsumeItemCoolTime() * 1000);
                }
            }
        }
        else
        {
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void UseCosmetic(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (!chr.isAlive()) || (chr.getMap() == null) || (chr.hasBlockedInventory()) || (chr.inPVP()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId) || (itemId / 10000 != 254) || (itemId / 1000 % 10 != chr.getGender()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (MapleItemInformationProvider.getInstance().getItemEffect(toUse.getItemId()).applyTo(chr))
        {
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
        }
    }


    public static void UseReducer(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (!chr.isAlive()) || (chr.getMap() == null) || (chr.hasBlockedInventory()) || (chr.inPVP()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int itemId = slea.readInt();
        byte slot = (byte) slea.readInt();
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId) || (itemId / 10000 != 270))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int lines = chr.getLevel() >= 30 ? 1 : chr.getLevel() >= 50 ? 2 : chr.getLevel() >= 70 ? 3 : 0;
        if (lines < chr.getInnerSkillSize())
        {
            lines = chr.getInnerSkillSize() > 3 ? 3 : chr.getInnerSkillSize();
        }
        for (int i = 0; i < lines; i++)
        {
            boolean rewarded = false;

            int rank = 0;
            int position = i + 1;
            while (!rewarded)
            {
                client.InnerSkillEntry newskill = client.InnerAbillity.getInstance().renewSkill(rank, position, itemId, itemId == 2701000);
                if (newskill != null)
                {
                    chr.changeInnerSkill(newskill);
                    rewarded = true;
                }
            }
        }
        chr.equipChanged();
        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, toUse.getPosition(), (short) 1, false);
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void UseReturnScroll(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((!chr.isAlive()) || (chr.getMapId() == 749040100) || (chr.hasBlockedInventory()) || (chr.isInBlockedMap()) || (chr.inPVP()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        c.getPlayer().updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (!FieldLimitType.PotionUse.check(chr.getMap().getFieldLimit()))
        {
            if (ii.getItemEffect(toUse.getItemId()).applyReturnScroll(chr))
            {
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
            }
            else
            {
                c.getSession().write(MaplePacketCreator.enableActions());
            }
        }
        else
        {
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void UseMagnify(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        chr.updateTick(slea.readInt());
        chr.setScrolledPosition((short) 0);
        byte src = (byte) slea.readShort();
        byte dst = (byte) slea.readShort();
        boolean insight = (src == Byte.MAX_VALUE) && (chr.getTrait(client.MapleTraitType.sense).getLevel() >= 30);
        Item magnify = chr.getInventory(MapleInventoryType.USE).getItem(src);
        Equip toScroll;
        if (dst < 0)
        {
            toScroll = (Equip) chr.getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
        }
        else
        {
            toScroll = (Equip) chr.getInventory(MapleInventoryType.EQUIP).getItem(dst);
        }
        if (chr.isShowPacket())
        {
            System.out.println("鉴定装备: 放大镜: " + magnify + " insight: " + insight + " toScroll: " + toScroll + " BlockedInventory: " + c.getPlayer().hasBlockedInventory());
        }
        if (((magnify == null) && (!insight)) || (toScroll == null) || (c.getPlayer().hasBlockedInventory()))
        {
            chr.dropMessage(5, "请在杂货店购买放大镜直接鉴定!");
            c.getSession().write(InventoryPacket.getInventoryFull());

            return;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int reqLevel = ii.getReqLevel(toScroll.getItemId()) / 10;
        boolean isPotAdd = false;
        if (((toScroll.getState() == 1) || (toScroll.getPotential4() < 0) || (toScroll.getPotential5() < 0)) && ((insight) || (magnify.getItemId() == 2460005) || (magnify.getItemId() == 2460004) || (magnify.getItemId() == 2460003) || ((magnify.getItemId() == 2460002) && (reqLevel <= 12)) || ((magnify.getItemId() == 2460001) && (reqLevel <= 7)) || ((magnify.getItemId() == 2460000) && (reqLevel <= 3))))
        {
            List<List<StructItemOption>> pots = new LinkedList(ii.getAllPotentialInfo().values());
            if (toScroll.getState() == 1)
            {
                int new_state = Math.abs(toScroll.getPotential1());
                if ((new_state > 20) || (new_state < 17))
                {
                    new_state = 17;
                }
                int lines = toScroll.getPotential2() != 0 ? 3 : 2;
                while (toScroll.getState() != new_state)
                {
                    for (int i = 0; i < lines; i++)
                    {
                        boolean rewarded = false;
                        while (!rewarded)
                        {
                            StructItemOption pot = (StructItemOption) ((List) pots.get(Randomizer.nextInt(pots.size()))).get(reqLevel);
                            if ((pot != null) && (pot.reqLevel / 10 <= reqLevel) && (GameConstants.optionTypeFits(pot.optionType, toScroll.getItemId())) && (GameConstants.optionTypeFitsX(pot.opID,
                                    toScroll.getItemId())) && (GameConstants.potentialIDFits(pot.opID, new_state, i)))
                            {
                                if (i == 0)
                                {
                                    toScroll.setPotential1(pot.opID);
                                }
                                else if (i == 1)
                                {
                                    toScroll.setPotential2(pot.opID);
                                }
                                else if (i == 2)
                                {
                                    toScroll.setPotential3(pot.opID);
                                }
                                rewarded = true;
                            }
                        }
                    }
                }
                if ((toScroll.getState() >= 18) && (toScroll.getStateMsg() < 3))
                {
                    if ((toScroll.getState() == 18) && (toScroll.getStateMsg() == 0))
                    {
                        toScroll.setStateMsg(1);
                        chr.finishAchievement(52);
                        if (!chr.isAdmin())
                        {
                            String msg = chr.getMedalText() + chr.getName() + " : 鉴定出 A 级装备，大家祝贺他(她)吧！";
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.itemMegaphone(msg, true, c.getChannel(), toScroll));
                        }
                    }
                    else if ((toScroll.getState() == 19) && (toScroll.getStateMsg() <= 1))
                    {
                        toScroll.setStateMsg(2);
                        chr.finishAchievement(53);
                        if (!chr.isAdmin())
                        {
                            String msg = chr.getMedalText() + chr.getName() + " : 鉴定出 S 级装备，大家祝贺他(她)吧！";
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.itemMegaphone(msg, true, c.getChannel(), toScroll));
                        }
                    }
                    else if ((toScroll.getState() == 20) && (toScroll.getStateMsg() <= 2))
                    {
                        toScroll.setStateMsg(3);
                        chr.finishAchievement(54);
                        if (!chr.isAdmin())
                        {
                            String msg = chr.getMedalText() + chr.getName() + " : 鉴定出 SS 级装备，大家祝贺他(她)吧！";
                            WorldBroadcastService.getInstance().broadcastSmega(MaplePacketCreator.itemMegaphone(msg, true, c.getChannel(), toScroll));
                        }
                    }
                }
            }
            else
            {
                if (toScroll.getPotential4() < 0)
                {
                    int new_state = Math.abs(toScroll.getPotential4());
                    if ((new_state > 20) || (new_state < 17))
                    {
                        new_state = 17;
                    }
                    while (toScroll.getAddState(4) != new_state)
                    {
                        boolean rewarded = false;
                        while (!rewarded)
                        {
                            StructItemOption pot = (StructItemOption) ((List) pots.get(Randomizer.nextInt(pots.size()))).get(reqLevel);
                            if ((pot != null) && (!GameConstants.isBlockedPotential(pot.opID)) && (pot.reqLevel / 10 <= reqLevel) && (GameConstants.optionTypeFits(pot.optionType,
                                    toScroll.getItemId())) && (GameConstants.optionTypeFitsX(pot.opID, toScroll.getItemId())) && (GameConstants.potentialIDFits(pot.opID, new_state, 2)))
                            {
                                toScroll.setPotential4(pot.opID);
                                rewarded = true;
                            }
                        }
                    }
                }
                if (toScroll.getPotential5() < 0)
                {
                    int new_state = Math.abs(toScroll.getPotential5());
                    if ((new_state > 20) || (new_state < 17))
                    {
                        new_state = 17;
                    }
                    while (toScroll.getAddState(5) != new_state)
                    {
                        boolean rewarded = false;
                        while (!rewarded)
                        {
                            StructItemOption pot = (StructItemOption) ((List) pots.get(Randomizer.nextInt(pots.size()))).get(reqLevel);
                            if ((pot != null) && (!GameConstants.isBlockedPotential(pot.opID)) && (pot.reqLevel / 10 <= reqLevel) && (GameConstants.optionTypeFits(pot.optionType,
                                    toScroll.getItemId())) && (GameConstants.optionTypeFitsX(pot.opID, toScroll.getItemId())) && (GameConstants.potentialIDFits(pot.opID, new_state, 2)))
                            {
                                toScroll.setPotential5(pot.opID);
                                rewarded = true;
                            }
                        }
                    }
                }
                if (toScroll.getPotential6() < 0)
                {
                    int new_state = Math.abs(toScroll.getPotential6());
                    if ((new_state > 20) || (new_state < 17))
                    {
                        new_state = 17;
                    }
                    while (toScroll.getAddState(6) != new_state)
                    {
                        boolean rewarded = false;
                        while (!rewarded)
                        {
                            StructItemOption pot = (StructItemOption) ((List) pots.get(Randomizer.nextInt(pots.size()))).get(reqLevel);
                            if ((pot != null) && (!GameConstants.isBlockedPotential(pot.opID)) && (pot.reqLevel / 10 <= reqLevel) && (GameConstants.optionTypeFits(pot.optionType,
                                    toScroll.getItemId())) && (GameConstants.optionTypeFitsX(pot.opID, toScroll.getItemId())) && (GameConstants.potentialIDFits(pot.opID, new_state, 2)))
                            {
                                toScroll.setPotential6(pot.opID);
                                rewarded = true;
                            }
                        }
                    }
                }
                isPotAdd = true;
            }
            chr.getTrait(client.MapleTraitType.insight).addExp((insight ? 10 : magnify.getItemId() + 2 - 2460000) * 2, chr);
            chr.getMap().broadcastMessage(InventoryPacket.showMagnifyingEffect(chr.getId(), toScroll.getPosition(), isPotAdd));
            if (!insight)
            {
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, magnify.getPosition(), (short) 1, false);
            }
            chr.forceUpdateItem(toScroll, true);
            if (dst < 0)
            {
                chr.equipChanged();
            }
            c.getSession().write(MaplePacketCreator.enableActions());
        }
        else
        {
            c.getSession().write(InventoryPacket.getInventoryFull());
        }
    }


    public static boolean UseSkillBook(byte slot, int itemId, MapleClient c, MapleCharacter chr)
    {
        Item toUse = chr.getInventory(ItemConstants.getInventoryType(itemId)).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId) || (chr.hasBlockedInventory()))
        {
            return false;
        }
        Map<String, Integer> skilldata = MapleItemInformationProvider.getInstance().getEquipStats(toUse.getItemId());
        if (skilldata == null)
        {
            return false;
        }
        boolean canuse = false;
        boolean success = false;
        int skill = 0;
        int maxlevel = 0;

        Integer SuccessRate = skilldata.get("success");
        Integer ReqSkillLevel = skilldata.get("reqSkillLevel");
        Integer MasterLevel = skilldata.get("masterLevel");

        byte i = 0;
        for (; ; )
        {
            Integer CurrentLoopedSkillId = skilldata.get("skillid" + i);
            i = (byte) (i + 1);
            if ((CurrentLoopedSkillId == null) || (MasterLevel == null))
            {
                break;
            }
            client.Skill CurrSkillData = client.SkillFactory.getSkill(CurrentLoopedSkillId);
            if ((CurrSkillData != null) && (CurrSkillData.canBeLearnedBy(chr.getJob())) && ((ReqSkillLevel == null) || (chr.getSkillLevel(CurrSkillData) >= ReqSkillLevel)) && (chr.getMasterLevel(CurrSkillData) < MasterLevel))
            {
                canuse = true;
                if ((SuccessRate == null) || (Randomizer.nextInt(100) <= SuccessRate))
                {
                    success = true;
                    chr.changeSingleSkillLevel(CurrSkillData, chr.getSkillLevel(CurrSkillData), (byte) MasterLevel.intValue());
                }
                else
                {
                    success = false;
                }
                MapleInventoryManipulator.removeFromSlot(c, ItemConstants.getInventoryType(itemId), slot, (short) 1, false);
                break;
            }
        }
        c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.useSkillBook(chr, skill, maxlevel, canuse, success));
        c.getSession().write(MaplePacketCreator.enableActions());
        return canuse;
    }


    public static void UseSpReset(byte slot, int itemId, MapleClient c, MapleCharacter chr)
    {
        Item toUse = chr.getInventory(ItemConstants.getInventoryType(itemId)).getItem(slot);
        if ((toUse == null) || (itemId / 1000 != 2500) || (toUse.getItemId() != itemId) || (GameConstants.is新手职业(chr.getJob())))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.SpReset();
        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, true);
        c.getSession().write(MaplePacketCreator.useSPReset(chr.getId()));
    }


    public static void UseApReset(byte slot, int itemId, MapleClient c, MapleCharacter chr)
    {
        Item toUse = chr.getInventory(ItemConstants.getInventoryType(itemId)).getItem(slot);
        if ((toUse != null) && (toUse.getQuantity() > 0) && (toUse.getItemId() == itemId) && (!chr.hasBlockedInventory()) && (itemId / 10000 == 250))
        {
            chr.resetStats(4, 4, 4, 4);
            MapleInventoryManipulator.removeFromSlot(c, ItemConstants.getInventoryType(itemId), slot, (short) 1, false);
            c.getSession().write(MaplePacketCreator.useAPReset(chr.getId()));
            c.getSession().write(MaplePacketCreator.enableActions());
        }
        else
        {
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void UseCatchItem(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        c.getPlayer().updateTick(slea.readInt());
        c.getPlayer().setScrolledPosition((short) 0);
        byte slot = (byte) slea.readShort();
        int itemid = slea.readInt();
        server.life.MapleMonster mob = chr.getMap().getMonsterByOid(slea.readInt());
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        MapleMap map = chr.getMap();
        if ((toUse != null) && (toUse.getQuantity() > 0) && (toUse.getItemId() == itemid) && (mob != null) && (!chr.hasBlockedInventory()) && (itemid / 10000 == 227) && (MapleItemInformationProvider.getInstance().getCardMobId(itemid) == mob.getId()))
        {
            if ((!MapleItemInformationProvider.getInstance().isMobHP(itemid)) || (mob.getHp() <= mob.getMobMaxHp() / 2L))
            {
                map.broadcastMessage(tools.packet.MobPacket.catchMonster(mob.getObjectId(), itemid, (byte) 1));
                map.killMonster(mob, chr, true, false, (byte) 1);
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false, false);
                if (MapleItemInformationProvider.getInstance().getCreateId(itemid) > 0)
                {
                    MapleInventoryManipulator.addById(c, MapleItemInformationProvider.getInstance().getCreateId(itemid), (short) 1,
                            "Catch item " + itemid + " on " + FileoutputUtil.CurrentReadable_Date());
                }
            }
            else
            {
                map.broadcastMessage(tools.packet.MobPacket.catchMonster(mob.getObjectId(), itemid, (byte) 0));
                c.getSession().write(tools.packet.MobPacket.catchMob(mob.getId(), itemid, (byte) 0));
            }
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void UseMountFood(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        c.getPlayer().updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        int itemid = slea.readInt();
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        client.inventory.MapleMount mount = chr.getMount();
        if ((itemid / 10000 == 226) && (toUse != null) && (toUse.getQuantity() > 0) && (toUse.getItemId() == itemid) && (mount != null) && (!c.getPlayer().hasBlockedInventory()))
        {
            int fatigue = mount.getFatigue();
            boolean levelup = false;
            mount.setFatigue((byte) -30);
            if (fatigue > 0)
            {
                mount.increaseExp();
                int level = mount.getLevel();
                if ((level < 30) && (mount.getExp() >= GameConstants.getMountExpNeededForLevel(level + 1)))
                {
                    mount.setLevel((byte) (level + 1));
                    levelup = true;
                }
            }
            chr.getMap().broadcastMessage(MaplePacketCreator.updateMount(chr, levelup));
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void UseScriptedNPCItem(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        c.getPlayer().updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        Item toUse = chr.getInventory(ItemConstants.getInventoryType(itemId)).getItem(slot);
        long expiration_days = 0L;
        int mountid = 0;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        server.ScriptedItem info = ii.getScriptedItemInfo(itemId);
        if (info == null)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((toUse != null) && (toUse.getQuantity() >= 1) && (toUse.getItemId() == itemId) && (!chr.hasBlockedInventory()) && (!chr.inPVP()))
        {
            int hair;
            MapleQuestStatus marr;
            long lastTime;
            switch (toUse.getItemId())
            {
                case 2430007:
                    MapleInventory inventory = chr.getInventory(MapleInventoryType.SETUP);
                    MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                    if ((inventory.countById(3994102) >= 1) && (inventory.countById(3994103) >= 1) && (inventory.countById(3994104) >= 1) && (inventory.countById(3994105) >= 1))
                    {
                        MapleInventoryManipulator.addById(c, 2430008, (short) 1, "Scripted item: " + itemId + " on " + FileoutputUtil.CurrentReadable_Date());
                        MapleInventoryManipulator.removeById(c, MapleInventoryType.SETUP, 3994102, 1, false, false);
                        MapleInventoryManipulator.removeById(c, MapleInventoryType.SETUP, 3994103, 1, false, false);
                        MapleInventoryManipulator.removeById(c, MapleInventoryType.SETUP, 3994104, 1, false, false);
                        MapleInventoryManipulator.removeById(c, MapleInventoryType.SETUP, 3994105, 1, false, false);
                    }
                    else
                    {
                        MapleInventoryManipulator.addById(c, 2430007, (short) 1, "Scripted item: " + itemId + " on " + FileoutputUtil.CurrentReadable_Date());
                    }
                    scripting.npc.NPCScriptManager.getInstance().start(c, 2084001);
                    break;

                case 2431588:
                    scripting.npc.NPCScriptManager.getInstance().start(c, 9900005);
                    break;

                case 2432122:
                    scripting.npc.NPCScriptManager.getInstance().start(c, 1052135);
                    break;

                case 2430865:
                    scripting.npc.NPCScriptManager.getInstance().start(c, 9000111);
                    break;

                case 2430008:
                    chr.saveLocation(server.maps.SavedLocationType.RICHIE);

                    boolean warped = false;
                    for (int i = 390001000; i <= 390001004; i++)
                    {
                        MapleMap map = c.getChannelServer().getMapFactory().getMap(i);
                        if (map.getCharactersSize() == 0)
                        {
                            chr.changeMap(map, map.getPortal(0));
                            warped = true;
                            break;
                        }
                    }
                    if (warped)
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "All maps are currently in use, please try again later.");
                    }
                    break;

                case 2430112:
                    if (c.getPlayer().getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.USE).countById(2430112) >= 20)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 5062000, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 20, true, false)))
                            {
                                MapleInventoryManipulator.addById(c, 5062000, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                            }
                        }
                        else
                        {
                            ItemScriptManager.getInstance().start(c, info.getNpc(), toUse.getItemId());
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                    }
                    break;
                case 2431893:
                    if (c.getPlayer().getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.USE).countById(2431893) >= 20)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 5062009, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 20, true, false)))
                            {
                                MapleInventoryManipulator.addById(c, 5062009, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                            }
                        }
                        else
                        {
                            ItemScriptManager.getInstance().start(c, info.getNpc(), toUse.getItemId());
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                    }
                    break;
                case 2431894:
                    if (c.getPlayer().getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.USE).countById(2431894) >= 20)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 5062010, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 20, true, false)))
                            {
                                MapleInventoryManipulator.addById(c, 5062010, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                            }
                        }
                        else
                        {
                            ItemScriptManager.getInstance().start(c, info.getNpc(), toUse.getItemId());
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                    }
                    break;
                case 2430481:
                    if (c.getPlayer().getInventory(MapleInventoryType.USE).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.USE).countById(2430481) >= 20)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 5062002, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 20, true, false)))
                            {
                                MapleInventoryManipulator.addById(c, 5062002, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                            }
                        }
                        else
                        {
                            ItemScriptManager.getInstance().start(c, info.getNpc(), toUse.getItemId());
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "消耗栏空间位置不足.");
                    }
                    break;
                case 2430760:
                    if (c.getPlayer().getInventory(MapleInventoryType.CASH).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.USE).countById(2430760) >= 10)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 5750000, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 10, true, false)))
                            {
                                MapleInventoryManipulator.addById(c, 5750000, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "请检测背包空间是否足够.");
                            }
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "10个星岩魔方碎片才可以兑换1个星岩魔方.");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "请检测背包空间是否足够.");
                    }
                    break;
                case 2430691:
                    if (c.getPlayer().getInventory(MapleInventoryType.CASH).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.USE).countById(2430691) >= 10)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 5750001, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 10, true, false)))
                            {
                                MapleInventoryManipulator.addById(c, 5750001, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "请检测背包空间是否足够.");
                            }
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "10个星岩电钻机碎片才可以兑换1个星岩电钻机.");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "请检测背包空间是否足够.");
                    }
                    break;
                case 2430692:
                    if (c.getPlayer().getInventory(MapleInventoryType.SETUP).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.USE).countById(2430692) >= 1)
                        {
                            int rank = Randomizer.nextInt(100) < 30 ? 1 : Randomizer.nextInt(100) < 4 ? 2 : 0;
                            List<StructItemOption> pots = new LinkedList(ii.getAllSocketInfo(rank).values());
                            int newId = 0;
                            while (newId == 0)
                            {
                                StructItemOption pot = pots.get(Randomizer.nextInt(pots.size()));
                                if (pot != null)
                                {
                                    newId = pot.opID;
                                }
                            }
                            if ((MapleInventoryManipulator.checkSpace(c, newId, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 1, true, false)))
                            {
                                int grade = ItemConstants.getNebuliteGrade(newId);
                                if (grade == 2)
                                {
                                    Item nItem = new Item(newId, (short) 0, (short) 1, (short) 0);
                                    WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(c.getPlayer().getName(), " : 从星岩箱子中获得{" + ii.getName(newId) +
                                            "}！大家一起恭喜他（她）吧！！！！", nItem, 1, c.getChannel()));
                                }
                                else if (grade == 3)
                                {
                                    Item nItem = new Item(newId, (short) 0, (short) 1, (short) 0);
                                    WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(c.getPlayer().getName(), " : 从星岩箱子中获得{" + ii.getName(newId) +
                                            "}！大家一起恭喜他（她）吧！！！！", nItem, 2, c.getChannel()));
                                }
                                else if (grade == 4)
                                {
                                    Item nItem = new Item(newId, (short) 0, (short) 1, (short) 0);
                                    WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(c.getPlayer().getName(), " : 从星岩箱子中获得{" + ii.getName(newId) +
                                            "}！大家一起恭喜他（她）吧！！！！", nItem, 3, c.getChannel()));
                                }
                                MapleInventoryManipulator.addById(c, newId, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                                c.getSession().write(MaplePacketCreator.getShowItemGain(newId, (short) 1, true));
                                chr.getMap().broadcastMessage(InventoryPacket.showNebuliteEffect(chr.getId(), true, "成功交换了星岩。"));
                                c.getSession().write(MaplePacketCreator.craftMessage("你得到了" + ii.getName(newId)));
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "请检测背包空间是否足够.");
                            }
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "您没有星岩箱子.");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "请检测背包空间是否足够.");
                    }
                    break;

                case 5680019:
                    hair = 32150 + c.getPlayer().getHair() % 10;
                    c.getPlayer().setHair(hair);
                    c.getPlayer().updateSingleStat(client.MapleStat.发型, hair);
                    MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.CASH, slot, (short) 1, false);

                    break;


                case 5680020:
                    hair = 32160 + c.getPlayer().getHair() % 10;
                    c.getPlayer().setHair(hair);
                    c.getPlayer().updateSingleStat(client.MapleStat.发型, hair);
                    MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.CASH, slot, (short) 1, false);

                    break;

                case 3994225:
                    c.getPlayer().dropMessage(5, "Please bring this item to the NPC.");
                    break;
                case 2430212:
                    marr = c.getPlayer().getQuestNAdd(server.quest.MapleQuest.getInstance(122500));
                    if (marr.getCustomData() == null)
                    {
                        marr.setCustomData("0");
                    }
                    lastTime = Long.parseLong(marr.getCustomData());
                    if (lastTime + 600000L > System.currentTimeMillis())
                    {
                        c.getPlayer().dropMessage(5, "疲劳恢复药 10分钟内只能使用1次，请稍后在试。");
                    }
                    else if (c.getPlayer().getFatigue() > 0)
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                        c.getPlayer().setFatigue(c.getPlayer().getFatigue() - 5);
                    }
                    break;
                case 2430213:
                    marr = c.getPlayer().getQuestNAdd(server.quest.MapleQuest.getInstance(122500));
                    if (marr.getCustomData() == null)
                    {
                        marr.setCustomData("0");
                    }
                    lastTime = Long.parseLong(marr.getCustomData());
                    if (lastTime + 600000L > System.currentTimeMillis())
                    {
                        c.getPlayer().dropMessage(5, "疲劳恢复药 10分钟内只能使用1次，请稍后在试。");
                    }
                    else if (c.getPlayer().getFatigue() > 0)
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                        c.getPlayer().setFatigue(c.getPlayer().getFatigue() - 10);
                    }
                    break;
                case 2430214:
                case 2430220:
                    if (c.getPlayer().getFatigue() > 0)
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                        c.getPlayer().setFatigue(c.getPlayer().getFatigue() - 30);
                    }
                    break;
                case 2430227:
                    if (c.getPlayer().getFatigue() > 0)
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                        c.getPlayer().setFatigue(c.getPlayer().getFatigue() - 50);
                    }
                    break;
                case 2430231:
                    marr = c.getPlayer().getQuestNAdd(server.quest.MapleQuest.getInstance(122500));
                    if (marr.getCustomData() == null)
                    {
                        marr.setCustomData("0");
                    }
                    lastTime = Long.parseLong(marr.getCustomData());
                    if (lastTime + 600000L > System.currentTimeMillis())
                    {
                        c.getPlayer().dropMessage(5, "疲劳恢复药 10分钟内只能使用1次，请稍后在试。");
                    }
                    else if (c.getPlayer().getFatigue() > 0)
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                        c.getPlayer().setFatigue(c.getPlayer().getFatigue() - 40);
                    }
                    break;
                case 2430144:
                    int itemid = Randomizer.nextInt(373) + 2290000;
                    if ((MapleItemInformationProvider.getInstance().itemExists(itemid)) && (!MapleItemInformationProvider.getInstance().getName(itemid).contains("Special")) && (!MapleItemInformationProvider.getInstance().getName(itemid).contains("Event")))
                    {
                        MapleInventoryManipulator.addById(c, itemid, (short) 1, "Reward item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                    }
                    break;
                case 2430370:
                    if (MapleInventoryManipulator.checkSpace(c, 2028062, 1, ""))
                    {
                        MapleInventoryManipulator.addById(c, 2028062, (short) 1, "Reward item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                    }
                    break;
                case 2430158:
                    if (c.getPlayer().getInventory(MapleInventoryType.ETC).getNumFreeSlot() >= 1)
                    {
                        if (c.getPlayer().getInventory(MapleInventoryType.ETC).countById(4000630) >= 100)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 4310010, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 1, true, false)))
                            {
                                MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4000630, 100, true, false);
                                MapleInventoryManipulator.addById(c, 4310010, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "其他栏空间位置不足.");
                            }
                        }
                        else if (c.getPlayer().getInventory(MapleInventoryType.ETC).countById(4000630) >= 50)
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 4310009, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 1, true, false)))
                            {
                                MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4000630, 50, true, false);
                                MapleInventoryManipulator.addById(c, 4310009, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "其他栏空间位置不足.");
                            }
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "需要50个净化图腾才能兑换出狮子王的贵族勋章，100个净化图腾才能兑换狮子王的皇家勋章。");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "其他栏空间位置不足.");
                    }
                    break;
                case 2430159:
                    server.quest.MapleQuest.getInstance(3182).forceComplete(c.getPlayer(), 2161004);
                    MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                    break;
                case 2430200:
                    if (c.getPlayer().getQuestStatus(31152) != 2)
                    {
                        c.getPlayer().dropMessage(5, "You have no idea how to use it.");
                    }
                    else if (c.getPlayer().getInventory(MapleInventoryType.ETC).getNumFreeSlot() >= 1)
                    {
                        if ((c.getPlayer().getInventory(MapleInventoryType.ETC).countById(4000660) >= 1) && (c.getPlayer().getInventory(MapleInventoryType.ETC).countById(4000661) >= 1) && (c.getPlayer().getInventory(MapleInventoryType.ETC).countById(4000662) >= 1) && (c.getPlayer().getInventory(MapleInventoryType.ETC).countById(4000663) >= 1))
                        {
                            if ((MapleInventoryManipulator.checkSpace(c, 4032923, 1, "")) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, toUse.getItemId(), 1, true, false)) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4000660, 1, true, false)) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4000661, 1, true, false)) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4000662, 1, true, false)) && (MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4000663, 1, true, false)))
                            {
                                MapleInventoryManipulator.addById(c, 4032923, (short) 1, "Scripted item: " + toUse.getItemId() + " on " + FileoutputUtil.CurrentReadable_Date());
                            }
                            else
                            {
                                c.getPlayer().dropMessage(5, "其他栏空间位置不足.");
                            }
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "There needs to be 1 of each Stone for a Dream Key.");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "其他栏空间位置不足.");
                    }

                    break;
                case 2430130:
                case 2430131:
                    if (GameConstants.is反抗者(c.getPlayer().getJob()))
                    {
                        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                        c.getPlayer().gainExp(20000 + c.getPlayer().getLevel() * 50 * c.getChannelServer().getExpRate(), true, true, false);
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "您无法使用这个道具。");
                    }
                    break;
                case 2430132:
                case 2430133:
                case 2430134:
                case 2430142:
                    if (c.getPlayer().getInventory(MapleInventoryType.EQUIP).getNumFreeSlot() >= 1)
                    {
                        if ((c.getPlayer().getJob() == 3200) || (c.getPlayer().getJob() == 3210) || (c.getPlayer().getJob() == 3211) || (c.getPlayer().getJob() == 3212))
                        {
                            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                            MapleInventoryManipulator.addById(c, 1382101, (short) 1, "Scripted item: " + itemId + " on " + FileoutputUtil.CurrentReadable_Date());
                        }
                        else if ((c.getPlayer().getJob() == 3300) || (c.getPlayer().getJob() == 3310) || (c.getPlayer().getJob() == 3311) || (c.getPlayer().getJob() == 3312))
                        {
                            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                            MapleInventoryManipulator.addById(c, 1462093, (short) 1, "Scripted item: " + itemId + " on " + FileoutputUtil.CurrentReadable_Date());
                        }
                        else if ((c.getPlayer().getJob() == 3500) || (c.getPlayer().getJob() == 3510) || (c.getPlayer().getJob() == 3511) || (c.getPlayer().getJob() == 3512))
                        {
                            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                            MapleInventoryManipulator.addById(c, 1492080, (short) 1, "Scripted item: " + itemId + " on " + FileoutputUtil.CurrentReadable_Date());
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "您无法使用这个道具。");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "背包空间不足。");
                    }
                    break;
                case 2430455:
                    ItemScriptManager.getInstance().start(c, 9010000, 2430455);
                    break;
                case 2430036:
                case 2430053:
                    mountid = 1027;
                    expiration_days = 1L;
                    break;
                case 2430170:
                    mountid = 1027;
                    expiration_days = 7L;
                    break;
                case 2430037:
                    mountid = 1028;
                    expiration_days = 1L;
                    break;
                case 2430038:
                    mountid = 1029;
                    expiration_days = 1L;
                    break;
                case 2430039:
                    mountid = 1030;
                    expiration_days = 1L;
                    break;
                case 2430040:
                    mountid = 1031;
                    expiration_days = 1L;
                    break;
                case 2430223:
                    mountid = 1031;
                    expiration_days = 15L;
                    break;
                case 2430259:
                    mountid = 1031;
                    expiration_days = 3L;
                    break;
                case 2430242:
                    mountid = 80001018;
                    expiration_days = 10L;
                    break;
                case 2430243:
                    mountid = 80001019;
                    expiration_days = 10L;
                    break;
                case 2430261:
                    mountid = 80001019;
                    expiration_days = 3L;
                    break;
                case 2430249:
                    mountid = 80001027;
                    expiration_days = 3L;
                    break;
                case 2430225:
                    mountid = 1031;
                    expiration_days = 10L;
                    break;
                case 2430054:
                    mountid = 1028;
                    expiration_days = 30L;
                    break;
                case 2430055:
                    mountid = 1029;
                    expiration_days = 30L;
                    break;
                case 2430257:
                    mountid = 1029;
                    expiration_days = 7L;
                    break;
                case 2430056:
                    mountid = 1035;
                    expiration_days = 30L;
                    break;
                case 2430057:
                    mountid = 1033;
                    expiration_days = 30L;
                    break;
                case 2430072:
                    mountid = 1034;
                    expiration_days = 7L;
                    break;
                case 2430073:
                    mountid = 1036;
                    expiration_days = 15L;
                    break;
                case 2430074:
                    mountid = 1037;
                    expiration_days = 15L;
                    break;
                case 2430272:
                    mountid = 1038;
                    expiration_days = 3L;
                    break;
                case 2430275:
                    mountid = 80001033;
                    expiration_days = 7L;
                    break;
                case 2430075:
                    mountid = 1038;
                    expiration_days = 15L;
                    break;
                case 2430076:
                    mountid = 1039;
                    expiration_days = 15L;
                    break;
                case 2430077:
                    mountid = 1040;
                    expiration_days = 15L;
                    break;
                case 2430080:
                    mountid = 1042;
                    expiration_days = 20L;
                    break;
                case 2430082:
                    mountid = 1044;
                    expiration_days = 7L;
                    break;
                case 2430260:
                    mountid = 1044;
                    expiration_days = 3L;
                    break;
                case 2430091:
                    mountid = 1049;
                    expiration_days = 10L;
                    break;
                case 2430092:
                    mountid = 1050;
                    expiration_days = 10L;
                    break;
                case 2430263:
                    mountid = 1050;
                    expiration_days = 3L;
                    break;
                case 2430093:
                    mountid = 1051;
                    expiration_days = 10L;
                    break;
                case 2430101:
                    mountid = 1052;
                    expiration_days = 10L;
                    break;
                case 2430102:
                    mountid = 1053;
                    expiration_days = 10L;
                    break;
                case 2430103:
                    mountid = 1054;
                    expiration_days = 30L;
                    break;
                case 2430266:
                    mountid = 1054;
                    expiration_days = 3L;
                    break;
                case 2430265:
                    mountid = 1151;
                    expiration_days = 3L;
                    break;
                case 2430258:
                    mountid = 1115;
                    expiration_days = 365L;
                    break;
                case 2430117:
                    mountid = 1036;
                    expiration_days = 365L;
                    break;
                case 2430118:
                    mountid = 1039;
                    expiration_days = 365L;
                    break;
                case 2430119:
                    mountid = 1040;
                    expiration_days = 365L;
                    break;
                case 2430120:
                    mountid = 1037;
                    expiration_days = 365L;
                    break;
                case 2430271:
                    mountid = 1069;
                    expiration_days = 3L;
                    break;
                case 2430136:
                    mountid = 1069;
                    expiration_days = 15L;
                    break;
                case 2430137:
                    mountid = 1069;
                    expiration_days = 30L;
                    break;
                case 2430138:
                    mountid = 1069;
                    expiration_days = 365L;
                    break;
                case 2430145:
                    mountid = 1070;
                    expiration_days = 30L;
                    break;
                case 2430146:
                    mountid = 1070;
                    expiration_days = 365L;
                    break;
                case 2430147:
                    mountid = 1071;
                    expiration_days = 30L;
                    break;
                case 2430148:
                    mountid = 1071;
                    expiration_days = 365L;
                    break;
                case 2430135:
                    mountid = 1065;
                    expiration_days = 15L;
                    break;
                case 2430149:
                    mountid = 1072;
                    expiration_days = 30L;
                    break;
                case 2430262:
                    mountid = 1072;
                    expiration_days = 3L;
                    break;
                case 2430179:
                    mountid = 1081;
                    expiration_days = 15L;
                    break;
                case 2430264:
                    mountid = 1081;
                    expiration_days = 3L;
                    break;
                case 2430201:
                    mountid = 1096;
                    expiration_days = 3L;
                    break;
                case 2430228:
                    mountid = 1101;
                    expiration_days = 15L;
                    break;
                case 2430276:
                    mountid = 1101;
                    expiration_days = 15L;
                    break;
                case 2430277:
                    mountid = 1101;
                    expiration_days = 365L;
                    break;
                case 2430283:
                    mountid = 1025;
                    expiration_days = 10L;
                    break;
                case 2430291:
                    mountid = 1145;
                    expiration_days = -1L;
                    break;
                case 2430293:
                    mountid = 1146;
                    expiration_days = -1L;
                    break;
                case 2430295:
                    mountid = 1147;
                    expiration_days = -1L;
                    break;
                case 2430297:
                    mountid = 1148;
                    expiration_days = -1L;
                    break;
                case 2430299:
                    mountid = 1149;
                    expiration_days = -1L;
                    break;
                case 2430301:
                    mountid = 1150;
                    expiration_days = -1L;
                    break;
                case 2430303:
                    mountid = 1151;
                    expiration_days = -1L;
                    break;
                case 2430305:
                    mountid = 1152;
                    expiration_days = -1L;
                    break;
                case 2430307:
                    mountid = 1153;
                    expiration_days = -1L;
                    break;
                case 2430309:
                    mountid = 1154;
                    expiration_days = -1L;
                    break;
                case 2430311:
                    mountid = 1156;
                    expiration_days = -1L;
                    break;
                case 2430313:
                    mountid = 1156;
                    expiration_days = -1L;
                    break;
                case 2430315:
                    mountid = 1118;
                    expiration_days = -1L;
                    break;
                case 2430317:
                    mountid = 1121;
                    expiration_days = -1L;
                    break;
                case 2430319:
                    mountid = 1122;
                    expiration_days = -1L;
                    break;
                case 2430321:
                    mountid = 1123;
                    expiration_days = -1L;
                    break;
                case 2430323:
                    mountid = 1124;
                    expiration_days = -1L;
                    break;
                case 2430325:
                    mountid = 1129;
                    expiration_days = -1L;
                    break;
                case 2430327:
                    mountid = 1130;
                    expiration_days = -1L;
                    break;
                case 2430329:
                    mountid = 1063;
                    expiration_days = -1L;
                    break;
                case 2430331:
                    mountid = 1025;
                    expiration_days = -1L;
                    break;
                case 2430333:
                    mountid = 1034;
                    expiration_days = -1L;
                    break;
                case 2430335:
                    mountid = 1136;
                    expiration_days = -1L;
                    break;
                case 2430337:
                    mountid = 1051;
                    expiration_days = -1L;
                    break;
                case 2430339:
                    mountid = 1138;
                    expiration_days = -1L;
                    break;
                case 2430341:
                    mountid = 1139;
                    expiration_days = -1L;
                    break;
                case 2430343:
                    mountid = 1027;
                    expiration_days = -1L;
                    break;
                case 2430346:
                    mountid = 1029;
                    expiration_days = -1L;
                    break;
                case 2430348:
                    mountid = 1028;
                    expiration_days = -1L;
                    break;
                case 2430350:
                    mountid = 1033;
                    expiration_days = -1L;
                    break;
                case 2430352:
                    mountid = 1064;
                    expiration_days = -1L;
                    break;
                case 2430354:
                    mountid = 1096;
                    expiration_days = -1L;
                    break;
                case 2430356:
                    mountid = 1101;
                    expiration_days = -1L;
                    break;
                case 2430358:
                    mountid = 1102;
                    expiration_days = -1L;
                    break;
                case 2430360:
                    mountid = 1054;
                    expiration_days = -1L;
                    break;
                case 2430362:
                    mountid = 1053;
                    expiration_days = -1L;
                    break;
                case 2430292:
                    mountid = 1145;
                    expiration_days = 90L;
                    break;
                case 2430294:
                    mountid = 1146;
                    expiration_days = 90L;
                    break;
                case 2430296:
                    mountid = 1147;
                    expiration_days = 90L;
                    break;
                case 2430298:
                    mountid = 1148;
                    expiration_days = 90L;
                    break;
                case 2430300:
                    mountid = 1149;
                    expiration_days = 90L;
                    break;
                case 2430302:
                    mountid = 1150;
                    expiration_days = 90L;
                    break;
                case 2430304:
                    mountid = 1151;
                    expiration_days = 90L;
                    break;
                case 2430306:
                    mountid = 1152;
                    expiration_days = 90L;
                    break;
                case 2430308:
                    mountid = 1153;
                    expiration_days = 90L;
                    break;
                case 2430310:
                    mountid = 1154;
                    expiration_days = 90L;
                    break;
                case 2430312:
                    mountid = 1156;
                    expiration_days = 90L;
                    break;
                case 2430314:
                    mountid = 1156;
                    expiration_days = 90L;
                    break;
                case 2430316:
                    mountid = 1118;
                    expiration_days = 90L;
                    break;
                case 2430318:
                    mountid = 1121;
                    expiration_days = 90L;
                    break;
                case 2430320:
                    mountid = 1122;
                    expiration_days = 90L;
                    break;
                case 2430322:
                    mountid = 1123;
                    expiration_days = 90L;
                    break;
                case 2430326:
                    mountid = 1129;
                    expiration_days = 90L;
                    break;
                case 2430328:
                    mountid = 1130;
                    expiration_days = 90L;
                    break;
                case 2430330:
                    mountid = 1063;
                    expiration_days = 90L;
                    break;
                case 2430332:
                    mountid = 1025;
                    expiration_days = 90L;
                    break;
                case 2430334:
                    mountid = 1034;
                    expiration_days = 90L;
                    break;
                case 2430336:
                    mountid = 1136;
                    expiration_days = 90L;
                    break;
                case 2430338:
                    mountid = 1051;
                    expiration_days = 90L;
                    break;
                case 2430340:
                    mountid = 1138;
                    expiration_days = 90L;
                    break;
                case 2430342:
                    mountid = 1139;
                    expiration_days = 90L;
                    break;
                case 2430344:
                    mountid = 1027;
                    expiration_days = 90L;
                    break;
                case 2430347:
                    mountid = 1029;
                    expiration_days = 90L;
                    break;
                case 2430349:
                    mountid = 1028;
                    expiration_days = 90L;
                    break;
                case 2430351:
                    mountid = 1033;
                    expiration_days = 90L;
                    break;
                case 2430353:
                    mountid = 1064;
                    expiration_days = 90L;
                    break;
                case 2430355:
                    mountid = 1096;
                    expiration_days = 90L;
                    break;
                case 2430357:
                    mountid = 1101;
                    expiration_days = 90L;
                    break;
                case 2430359:
                    mountid = 1102;
                    expiration_days = 90L;
                    break;
                case 2430361:
                    mountid = 1054;
                    expiration_days = 90L;
                    break;
                case 2430363:
                    mountid = 1053;
                    expiration_days = 90L;
                    break;
                case 2430324:
                    mountid = 1158;
                    expiration_days = -1L;
                    break;
                case 2430345:
                    mountid = 1158;
                    expiration_days = 90L;
                    break;
                case 2430367:
                    mountid = 1115;
                    expiration_days = 3L;
                    break;
                case 2430365:
                    mountid = 1025;
                    expiration_days = 365L;
                    break;
                case 2430366:
                    mountid = 1025;
                    expiration_days = 15L;
                    break;
                case 2430369:
                    mountid = 1049;
                    expiration_days = 10L;
                    break;
                case 2430392:
                    mountid = 80001038;
                    expiration_days = 90L;
                    break;
                case 2430476:
                    mountid = 1039;
                    expiration_days = 15L;
                    break;
                case 2430477:
                    mountid = 1039;
                    expiration_days = 365L;
                    break;
                case 2430232:
                    mountid = 1106;
                    expiration_days = 10L;
                    break;
                case 2430511:
                    mountid = 80001033;
                    expiration_days = 15L;
                    break;
                case 2430512:
                    mountid = 80001033;
                    expiration_days = 365L;
                    break;
                case 2430536:
                    mountid = 80001114;
                    expiration_days = -1L;
                    break;
                case 2430537:
                    mountid = 80001114;
                    expiration_days = 90L;
                    break;
                case 2430229:
                    mountid = 1102;
                    expiration_days = 60L;
                    break;
                case 2430199:
                    mountid = 1102;
                    expiration_days = 1L;
                    break;
                case 2430206:
                    mountid = 1089;
                    expiration_days = 7L;
                    break;
                case 2430211:
                    mountid = 80001009;
                    expiration_days = 30L;
                    break;
                default:
                    ItemScriptManager.getInstance().start(c, info.getNpc(), toUse.getItemId());
            }

        }
        if (mountid > 0)
        {
            mountid = mountid > 80001000 ? mountid : client.PlayerStats.getSkillByJob(mountid, c.getPlayer().getJob());
            int fk = GameConstants.getMountItem(mountid, c.getPlayer());

            if ((fk > 0) && (mountid < 80001000))
            {
                for (int i = 80001001; i < 80001999; i++)
                {
                    client.Skill skill = client.SkillFactory.getSkill(i);
                    if ((skill != null) && (GameConstants.getMountItem(skill.getId(), c.getPlayer()) == fk))
                    {
                        mountid = i;
                        break;
                    }
                }
            }

            if (c.getPlayer().getSkillLevel(mountid) > 0)
            {
                c.getPlayer().dropMessage(1, "您已经拥有了[" + client.SkillFactory.getSkill(mountid).getName() + "]这个骑宠的技能，无法使用该道具。");
            }
            else if ((client.SkillFactory.getSkill(mountid) == null) || (GameConstants.getMountItem(mountid, c.getPlayer()) == 0))
            {
                c.getPlayer().dropMessage(1, "您无法使用这个骑宠的技能.");
            }
            else if (expiration_days > 0L)
            {
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                c.getPlayer().changeSingleSkillLevel(client.SkillFactory.getSkill(mountid), 1, (byte) 1, System.currentTimeMillis() + expiration_days * 24L * 60L * 60L * 1000L);
                c.getPlayer().dropMessage(1, "恭喜您获得[" + client.SkillFactory.getSkill(mountid).getName() + "]骑宠技能 " + expiration_days + " 权。");
            }
            else if (expiration_days == -1L)
            {
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                c.getPlayer().changeSingleSkillLevel(client.SkillFactory.getSkill(mountid), 1, (byte) 1, -1L);
                c.getPlayer().dropMessage(1, "恭喜您获得[" + client.SkillFactory.getSkill(mountid).getName() + "]骑宠技能永久权。");
            }
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void UseSummonBag(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((!chr.isAlive()) || (chr.hasBlockedInventory()) || (chr.inPVP()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        c.getPlayer().updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse != null) && (toUse.getQuantity() >= 1) && (toUse.getItemId() == itemId) && ((c.getPlayer().getMapId() < 910000000) || (c.getPlayer().getMapId() > 910000022)))
        {
            Map<String, Integer> toSpawn = MapleItemInformationProvider.getInstance().getEquipStats(itemId);
            if (toSpawn == null)
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            server.life.MapleMonster ht = null;
            int type = 0;
            for (Map.Entry<String, Integer> i : toSpawn.entrySet())
            {
                if ((i.getKey().startsWith("mob")) && (Randomizer.nextInt(99) <= i.getValue()))
                {
                    ht = server.life.MapleLifeFactory.getMonster(Integer.parseInt(i.getKey().substring(3)));
                    chr.getMap().spawnMonster_sSack(ht, chr.getPosition(), type);
                }
            }
            if (ht == null)
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void UseTreasureChest(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        short slot = slea.readShort();
        int itemid = slea.readInt();
        boolean useCash = slea.readByte() > 0;
        Item toUse = chr.getInventory(MapleInventoryType.ETC).getItem((byte) slot);
        if ((toUse == null) || (toUse.getQuantity() <= 0) || (toUse.getItemId() != itemid) || (chr.hasBlockedInventory()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((!chr.getCheatTracker().canMZD()) && (!chr.isGM()))
        {
            chr.dropMessage(5, "你需要等待5秒之后才能使用谜之蛋.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int price;
        int reward;
        int keyIDforRemoval;
        String box;
        String key;
        int amount = 1;
        switch (toUse.getItemId())
        {
            case 4280000:
                reward = server.RandomRewards.getGoldBoxReward();
                keyIDforRemoval = 5490000;
                box = "永恒的谜之蛋";
                key = "永恒的热度";
                price = 800;
                break;
            case 4280001:
                reward = server.RandomRewards.getSilverBoxReward();
                keyIDforRemoval = 5490001;
                box = "重生的谜之蛋";
                key = "重生的热度";
                price = 500;
                break;
            default:
                return;
        }
        switch (reward)
        {
            case 2000004:
                amount = 200;
                break;
            case 2000005:
                amount = 100;
        }

        if ((useCash) && (chr.getCSPoints(2) < price))
        {
            chr.dropMessage(1, "抵用券不足" + price + "点，请到商城购买“抵用券兑换包”即可充值抵用券！");
            c.getSession().write(MaplePacketCreator.enableActions());
        }
        else if (chr.getInventory(MapleInventoryType.CASH).countById(keyIDforRemoval) < 0)
        {
            chr.dropMessage(1, "孵化" + box + "需要" + key + "，请到商城购买！");
            c.getSession().write(MaplePacketCreator.enableActions());
        }
        else if ((chr.getInventory(MapleInventoryType.CASH).countById(keyIDforRemoval) > 0) || ((useCash) && (chr.getCSPoints(2) > price)))
        {
            Item item = MapleInventoryManipulator.addbyId_Gachapon(c, reward, (short) amount, "从 " + box + " 中获得时间: " + FileoutputUtil.CurrentReadable_Time());
            if (item == null)
            {
                chr.dropMessage(1, "孵化失败，请重试一次。");
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.ETC, slot, (short) 1, true);
            if (useCash)
            {
                chr.modifyCSPoints(2, -price, true);
            }
            else
            {
                MapleInventoryManipulator.removeById(c, MapleInventoryType.CASH, keyIDforRemoval, 1, true, false);
            }
            c.getSession().write(MaplePacketCreator.getShowItemGain(reward, (short) amount, true));
            byte rareness = GameConstants.gachaponRareItem(item.getItemId());
            if (rareness > 0)
            {
                WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(c.getPlayer().getName(), " : 从" + box + "中获得{" + ii.getName(item.getItemId()) +
                        "}！大家一起恭喜他（她）吧！！！！", item, rareness, c.getChannel()));
            }
        }
        else
        {
            chr.dropMessage(5, "孵化" + box + "失败，进检查是否有" + key + "或者抵用卷大于" + price + "点。");
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void Pickup_Player(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if (c.getPlayer().hasBlockedInventory())
        {
            return;
        }
        chr.updateTick(slea.readInt());
        c.getPlayer().setScrolledPosition((short) 0);
        slea.skip(1);
        java.awt.Point Client_Reportedpos = slea.readPos();
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        server.maps.MapleMapObject ob = chr.getMap().getMapObject(slea.readInt(), server.maps.MapleMapObjectType.ITEM);
        if (ob == null)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleMapItem mapitem = (MapleMapItem) ob;
        java.util.concurrent.locks.Lock lock = mapitem.getLock();
        lock.lock();
        try
        {
            if (mapitem.isPickedUp())
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if ((mapitem.getQuest() > 0) && (chr.getQuestStatus(mapitem.getQuest()) != 1))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if ((mapitem.getOwner() != chr.getId()) && (((!mapitem.isPlayerDrop()) && (mapitem.getDropType() == 0)) || ((mapitem.isPlayerDrop()) && (chr.getMap().getEverlast()))))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if ((!mapitem.isPlayerDrop()) && (mapitem.getDropType() == 1) && (mapitem.getOwner() != chr.getId()) && ((chr.getParty() == null) || (chr.getParty().getMemberById(mapitem.getOwner()) == null)))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            double Distance = Client_Reportedpos.distanceSq(mapitem.getPosition());
            if ((Distance > 5000.0D) && ((mapitem.getMeso() > 0) || (mapitem.getItemId() != 4001025)))
            {
                chr.getCheatTracker().checkPickup(20, false);

                WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                        "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") " + "全屏捡物。地图ID: " + chr.getMapId() + " 范围: " + Distance));
            }
            else if (chr.getPosition().distanceSq(mapitem.getPosition()) > 640000.0D)
            {
                chr.getCheatTracker().checkPickup(10, false);

                WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                        "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") " + "全屏捡物。地图ID: " + chr.getMapId() + " 范围: " + Distance));
            }
            if (mapitem.getMeso() > 0)
            {
                if ((chr.getParty() != null) && (mapitem.getOwner() != chr.getId()))
                {
                    List<MapleCharacter> toGive = new LinkedList<>();
                    int splitMeso = mapitem.getMeso() * 40 / 100;
                    for (handling.world.party.MaplePartyCharacter z : chr.getParty().getMembers())
                    {
                        MapleCharacter m = chr.getMap().getCharacterById(z.getId());
                        if ((m != null) && (m.getId() != chr.getId()))
                        {
                            toGive.add(m);
                        }
                    }
                    for (MapleCharacter m : toGive)
                    {
                        m.gainMeso(splitMeso / toGive.size() + (m.getStat().hasPartyBonus ? (int) (mapitem.getMeso() / 20.0D) : 0), true);
                    }
                    chr.gainMeso(mapitem.getMeso() - splitMeso, true);
                }
                else
                {
                    chr.gainMeso(mapitem.getMeso(), true);
                }
                removeItem(chr, mapitem, ob);
            }
            else if (MapleItemInformationProvider.getInstance().isPickupBlocked(mapitem.getItemId()))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                chr.dropMessage(5, "这个道具无法捡取.");
            }
            else if ((chr.inPVP()) && (Integer.parseInt(chr.getEventInstance().getProperty("ice")) == chr.getId()))
            {
                c.getSession().write(InventoryPacket.getInventoryFull());
                c.getSession().write(InventoryPacket.getShowInventoryFull());
                c.getSession().write(MaplePacketCreator.enableActions());
            }
            else if (useItem(c, mapitem.getItemId()))
            {
                removeItem(c.getPlayer(), mapitem, ob);

                if (mapitem.getItemId() / 10000 == 291)
                {
                    c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.getCapturePosition(c.getPlayer().getMap()));
                    c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.resetCapture());
                }
            }
            else if ((mapitem.getItemId() / 10000 != 291) && (MapleInventoryManipulator.checkSpace(c, mapitem.getItemId(), mapitem.getItem().getQuantity(), mapitem.getItem().getOwner())))
            {
                if ((mapitem.getItem().getQuantity() >= 50) && (mapitem.getItemId() == 2340000))
                {
                    c.setMonitored(true);
                }
                MapleInventoryManipulator.addFromDrop(c, mapitem.getItem(), true, mapitem.getDropper() instanceof server.life.MapleMonster);
                removeItem(chr, mapitem, ob);
            }
            else
            {
                c.getSession().write(InventoryPacket.getInventoryFull());
                c.getSession().write(InventoryPacket.getShowInventoryFull());
                c.getSession().write(MaplePacketCreator.enableActions());
            }
        }
        finally
        {
            lock.unlock();
        }
    }

    private static void removeItem(MapleCharacter chr, MapleMapItem mapitem, server.maps.MapleMapObject ob)
    {
        mapitem.setPickedUp(true);
        chr.getMap().broadcastMessage(InventoryPacket.removeItemFromMap(mapitem.getObjectId(), 2, chr.getId()), mapitem.getPosition());
        chr.getMap().removeMapObject(ob);
        if (mapitem.isRandDrop())
        {
            chr.getMap().spawnRandDrop();
        }
    }

    public static boolean useItem(MapleClient c, int id)
    {
        if (ItemConstants.isUse(id))
        {
            MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
            MapleStatEffect eff = ii.getItemEffect(id);
            if (eff == null)
            {
                return false;
            }

            if (id / 10000 == 291)
            {
                boolean area = false;
                for (java.awt.Rectangle rect : c.getPlayer().getMap().getAreas())
                {
                    if (rect.contains(c.getPlayer().getTruePosition()))
                    {
                        area = true;
                        break;
                    }
                }
                if ((!c.getPlayer().inPVP()) || ((c.getPlayer().getTeam() == id - 2910000) && (area)))
                {
                    return false;
                }
            }
            int consumeval = eff.getConsume();
            if (consumeval > 0)
            {
                consumeItem(c, eff);
                consumeItem(c, ii.getItemEffectEX(id));
                c.getSession().write(MaplePacketCreator.getShowItemGain(id, (short) 1));
                return true;
            }
        }
        return false;
    }


    public static void consumeItem(MapleClient c, MapleStatEffect eff)
    {
        if (eff == null)
        {
            return;
        }
        if (eff.getConsume() == 2)
        {
            if ((c.getPlayer().getParty() != null) && (c.getPlayer().isAlive()))
            {
                for (handling.world.party.MaplePartyCharacter pc : c.getPlayer().getParty().getMembers())
                {
                    MapleCharacter chr = c.getPlayer().getMap().getCharacterById(pc.getId());
                    if ((chr != null) && (chr.isAlive()))
                    {
                        eff.applyTo(chr);
                    }
                }
            }
            else
            {
                eff.applyTo(c.getPlayer());
            }
        }
        else if (c.getPlayer().isAlive())
        {
            eff.applyTo(c.getPlayer());
        }
    }

    public static void Pickup_Pet(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        if ((c.getPlayer().hasBlockedInventory()) || (c.getPlayer().inPVP()))
        {
            return;
        }
        c.getPlayer().setScrolledPosition((short) 0);
        byte petz = (byte) slea.readInt();
        client.inventory.MaplePet pet = chr.getSpawnPet(petz);
        slea.skip(1);
        chr.updateTick(slea.readInt());
        java.awt.Point Client_Reportedpos = slea.readPos();
        server.maps.MapleMapObject ob = chr.getMap().getMapObject(slea.readInt(), server.maps.MapleMapObjectType.ITEM);
        if ((ob == null) || (pet == null))
        {
            return;
        }
        MapleMapItem mapitem = (MapleMapItem) ob;
        java.util.concurrent.locks.Lock lock = mapitem.getLock();
        lock.lock();
        try
        {
            if (mapitem.isPickedUp())
            {
                c.getSession().write(InventoryPacket.getInventoryFull());
                return;
            }
            if ((mapitem.getOwner() != chr.getId()) && (mapitem.isPlayerDrop()))
            {
                return;
            }
            if ((mapitem.getOwner() != chr.getId()) && (((!mapitem.isPlayerDrop()) && (mapitem.getDropType() == 0)) || ((mapitem.isPlayerDrop()) && (chr.getMap().getEverlast()))))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if ((!mapitem.isPlayerDrop()) && (mapitem.getDropType() == 1) && (mapitem.getOwner() != chr.getId()) && ((chr.getParty() == null) || (chr.getParty().getMemberById(mapitem.getOwner()) == null)))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            double Distance = Client_Reportedpos.distanceSq(mapitem.getPosition());
            if ((Distance > 10000.0D) && ((mapitem.getMeso() > 0) || (mapitem.getItemId() != 4001025)))
            {
                chr.getCheatTracker().checkPickup(12, true);

                WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                        "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") " + "全屏宠吸。地图ID: " + chr.getMapId() + " 范围: " + Distance));
            }
            else if (pet.getPos().distanceSq(mapitem.getPosition()) > 640000.0D)
            {
                chr.getCheatTracker().checkPickup(6, true);

                WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                        "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") " + "全屏宠吸。地图ID: " + chr.getMapId() + " 范围: " + Distance));
            }
            if (mapitem.getMeso() > 0)
            {
                if ((chr.getParty() != null) && (mapitem.getOwner() != chr.getId()))
                {
                    List<MapleCharacter> toGive = new LinkedList<>();
                    int splitMeso = mapitem.getMeso() * 40 / 100;
                    for (handling.world.party.MaplePartyCharacter z : chr.getParty().getMembers())
                    {
                        MapleCharacter m = chr.getMap().getCharacterById(z.getId());
                        if ((m != null) && (m.getId() != chr.getId()))
                        {
                            toGive.add(m);
                        }
                    }
                    for (MapleCharacter m : toGive)
                    {
                        m.gainMeso(splitMeso / toGive.size() + (m.getStat().hasPartyBonus ? (int) (mapitem.getMeso() / 20.0D) : 0), true);
                    }
                    chr.gainMeso(mapitem.getMeso() - splitMeso, true);
                }
                else
                {
                    chr.gainMeso(mapitem.getMeso(), true);
                }
                removeItem_Pet(chr, mapitem, petz);
            }
            else if ((MapleItemInformationProvider.getInstance().isPickupBlocked(mapitem.getItemId())) || (mapitem.getItemId() / 10000 == 291))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
            }
            else if (useItem(c, mapitem.getItemId()))
            {
                removeItem_Pet(chr, mapitem, petz);
            }
            else if (MapleInventoryManipulator.checkSpace(c, mapitem.getItemId(), mapitem.getItem().getQuantity(), mapitem.getItem().getOwner()))
            {
                if ((mapitem.getItem().getQuantity() >= 50) && (mapitem.getItemId() == 2340000))
                {
                    c.setMonitored(true);
                }
                MapleInventoryManipulator.addFromDrop(c, mapitem.getItem(), true, mapitem.getDropper() instanceof server.life.MapleMonster);
                removeItem_Pet(chr, mapitem, petz);
            }
        }
        finally
        {
            lock.unlock();
        }
    }

    public static void removeItem_Pet(MapleCharacter chr, MapleMapItem mapitem, int pet)
    {
        mapitem.setPickedUp(true);
        chr.getMap().broadcastMessage(InventoryPacket.removeItemFromMap(mapitem.getObjectId(), 5, chr.getId(), pet));
        chr.getMap().removeMapObject(mapitem);
        if (mapitem.isRandDrop())
        {
            chr.getMap().spawnRandDrop();
        }
    }

    public static void OwlMinerva(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        byte slot = (byte) slea.readShort();
        int itemid = slea.readInt();
        Item toUse = c.getPlayer().getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse != null) && (toUse.getQuantity() > 0) && (toUse.getItemId() == itemid) && (itemid == 2310000) && (!c.getPlayer().hasBlockedInventory()))
        {
            int itemSearch = slea.readInt();
            List<HiredMerchant> hms = c.getChannelServer().searchMerchant(itemSearch);
            if (hms.size() > 0)
            {
                c.getSession().write(MaplePacketCreator.getOwlSearched(itemSearch, hms));
                MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, itemid, 1, true, false);
            }
            else
            {
                c.getPlayer().dropMessage(1, "没有找到这个道具.");
            }
            client.MapleCharacterUtil.addToItemSearch(itemSearch);
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void Owl(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer().getMapId() >= 910000001) && (c.getPlayer().getMapId() <= 910000022))
        {
            c.getSession().write(MaplePacketCreator.getOwlOpen());
        }
        else
        {
            c.getPlayer().dropMessage(5, "商店搜索器只能在钓鱼场使用.");
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void OwlWarp(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.enableActions());
        if ((c.getPlayer().getMapId() >= 910000001) && (c.getPlayer().getMapId() <= 910000022) && (!c.getPlayer().hasBlockedInventory()))
        {
            int id = slea.readInt();
            int type = slea.readByte();
            int map = slea.readInt();
            if ((map >= 910000001) && (map <= 910000022))
            {
                MapleMap mapp = c.getChannelServer().getMapFactory().getMap(map);
                c.getPlayer().changeMap(mapp, mapp.getPortal(0));
                HiredMerchant merchant = null;
                List<server.maps.MapleMapObject> objects;

                switch (1)
                {
                    case 0:
                        objects = mapp.getAllHiredMerchantsThreadsafe();
                        for (server.maps.MapleMapObject ob : objects)
                        {
                            if ((ob instanceof server.shops.IMaplePlayerShop))
                            {
                                server.shops.IMaplePlayerShop ips = (server.shops.IMaplePlayerShop) ob;
                                if ((ips instanceof HiredMerchant))
                                {
                                    HiredMerchant merch = (HiredMerchant) ips;
                                    if (merch.getOwnerId() == id)
                                    {
                                        merchant = merch;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case 1:
                        objects = mapp.getAllHiredMerchantsThreadsafe();
                        for (server.maps.MapleMapObject ob : objects)
                        {
                            if ((ob instanceof server.shops.IMaplePlayerShop))
                            {
                                server.shops.IMaplePlayerShop ips = (server.shops.IMaplePlayerShop) ob;
                                if ((ips instanceof HiredMerchant))
                                {
                                    HiredMerchant merch = (HiredMerchant) ips;
                                    if (merch.getStoreId() == id)
                                    {
                                        merchant = merch;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        server.maps.MapleMapObject ob = mapp.getMapObject(id, server.maps.MapleMapObjectType.HIRED_MERCHANT);
                        if ((ob instanceof server.shops.IMaplePlayerShop))
                        {
                            server.shops.IMaplePlayerShop ips = (server.shops.IMaplePlayerShop) ob;
                            if ((ips instanceof HiredMerchant))
                            {
                                merchant = (HiredMerchant) ips;
                            }
                        }
                        break;
                }
                if (merchant != null)
                {
                    if (merchant.isOwner(c.getPlayer()))
                    {
                        merchant.setOpen(false);
                        merchant.removeAllVisitors(18, 1);
                        c.getPlayer().setPlayerShop(merchant);
                        c.getSession().write(tools.packet.PlayerShopPacket.getHiredMerch(c.getPlayer(), merchant, false));
                    }
                    else if ((!merchant.isOpen()) || (!merchant.isAvailable()))
                    {
                        c.getPlayer().dropMessage(1, "主人正在整理商店物品\r\n请稍后再度光临！");
                    }
                    else if (merchant.getFreeSlot() == -1)
                    {
                        c.getPlayer().dropMessage(1, "店铺已达到最大人数\r\n请稍后再度光临！");
                    }
                    else if (merchant.isInBlackList(c.getPlayer().getName()))
                    {
                        c.getPlayer().dropMessage(1, "你被禁止进入该店铺.");
                    }
                    else
                    {
                        c.getPlayer().setPlayerShop(merchant);
                        merchant.addVisitor(c.getPlayer());
                        c.getSession().write(tools.packet.PlayerShopPacket.getHiredMerch(c.getPlayer(), merchant, false));
                    }

                }
                else
                {
                    c.getPlayer().dropMessage(1, "主人正在整理商店物品\r\n请稍后再度光临！");
                }
            }
        }
    }


    public static void PamSong(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        Item pam = c.getPlayer().getInventory(MapleInventoryType.CASH).findById(5640000);
        if ((slea.readByte() > 0) && (c.getPlayer().getScrolledPosition() != 0) && (pam != null) && (pam.getQuantity() > 0))
        {
            MapleInventoryType inv = c.getPlayer().getScrolledPosition() < 0 ? MapleInventoryType.EQUIPPED : MapleInventoryType.EQUIP;
            Item item = c.getPlayer().getInventory(inv).getItem(c.getPlayer().getScrolledPosition());
            c.getPlayer().setScrolledPosition((short) 0);
            if (item != null)
            {
                Equip eq = (Equip) item;
                eq.setUpgradeSlots((byte) (eq.getUpgradeSlots() + 1));
                c.getPlayer().forceUpdateItem(eq);
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.CASH, pam.getPosition(), (short) 1, true, false);
                c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.pamsSongEffect(c.getPlayer().getId()));
            }
        }
        else
        {
            c.getPlayer().setScrolledPosition((short) 0);
        }
    }


    public static void TeleRock(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        Item toUse = c.getPlayer().getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId) || (itemId / 10000 != 232) || (c.getPlayer().hasBlockedInventory()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        boolean used = UseTeleRock(slea, c, itemId);
        if (used)
        {
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static boolean UseTeleRock(SeekableLittleEndianAccessor slea, MapleClient c, int itemId)
    {
        boolean used = false;
        if (slea.readByte() == 0)
        {
            MapleMap target = c.getChannelServer().getMapFactory().getMap(slea.readInt());
            if (((itemId == 5041000) && (c.getPlayer().isRockMap(target.getId()))) || ((itemId != 5041000) && (c.getPlayer().isRegRockMap(target.getId()))) || (((itemId == 5040004) || (itemId == 5041001)) && ((c.getPlayer().isHyperRockMap(target.getId())) || (GameConstants.isHyperTeleMap(target.getId()))) && (!FieldLimitType.VipRock.check(c.getPlayer().getMap().getFieldLimit())) && (!FieldLimitType.VipRock.check(target.getFieldLimit())) && (!c.getPlayer().isInBlockedMap())))
            {
                c.getPlayer().changeMap(target, target.getPortal(0));
                used = true;
            }
        }
        else
        {
            MapleCharacter victim = c.getChannelServer().getPlayerStorage().getCharacterByName(slea.readMapleAsciiString());
            if ((victim != null) && (!victim.isIntern()) && (c.getPlayer().getEventInstance() == null) && (victim.getEventInstance() == null))
            {
                if ((!FieldLimitType.VipRock.check(c.getPlayer().getMap().getFieldLimit())) && (!FieldLimitType.VipRock.check(c.getChannelServer().getMapFactory().getMap(victim.getMapId()).getFieldLimit())) && (!victim.isInBlockedMap()) && (!c.getPlayer().isInBlockedMap()) && ((itemId == 5041000) || (itemId == 5040004) || (itemId == 5041001) || (victim.getMapId() / 100000000 == c.getPlayer().getMapId() / 100000000)))
                {
                    c.getPlayer().changeMap(victim.getMap(), victim.getMap().findClosestPortal(victim.getTruePosition()));
                    used = true;
                }
            }
            else
            {
                c.getPlayer().dropMessage(1, "在此频道未找到该玩家.");
            }
        }
        return used;
    }


    public static void UseNebulite(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        chr.updateTick(slea.readInt());
        chr.setScrolledPosition((short) 0);
        Item nebulite = chr.getInventory(MapleInventoryType.SETUP).getItem((byte) slea.readShort());
        int nebuliteId = slea.readInt();
        Item toMount = chr.getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readShort());
        if ((nebulite == null) || (nebuliteId != nebulite.getItemId()) || (toMount == null) || (chr.hasBlockedInventory()))
        {
            c.getSession().write(InventoryPacket.getInventoryFull());
            return;
        }
        Equip eqq = (Equip) toMount;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        boolean success = false;
        if (eqq.getSocket1() == 0)
        {

            StructItemOption pot = ii.getSocketInfo(nebuliteId);
            if ((pot != null) && (GameConstants.optionTypeFits(pot.optionType, eqq.getItemId())))
            {
                eqq.setSocket1(pot.opID);
            }


            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.SETUP, nebulite.getPosition(), (short) 1, false);
            chr.forceUpdateItem(toMount);
            success = true;
        }
        chr.getMap().broadcastMessage(InventoryPacket.showNebuliteEffect(c.getPlayer().getId(), success, success ? "成功嵌入星岩。" : "嵌入星岩失败。"));
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void UseAlienSocket(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getPlayer().updateTick(slea.readInt());
        c.getPlayer().setScrolledPosition((short) 0);
        Item alienSocket = c.getPlayer().getInventory(MapleInventoryType.USE).getItem((byte) slea.readShort());
        int alienSocketId = slea.readInt();
        Item toMount = c.getPlayer().getInventory(MapleInventoryType.EQUIP).getItem((byte) slea.readShort());
        if ((alienSocket == null) || (alienSocketId != alienSocket.getItemId()) || (toMount == null) || (c.getPlayer().hasBlockedInventory()))
        {
            c.getSession().write(InventoryPacket.getInventoryFull());
            return;
        }

        Equip eqq = (Equip) toMount;
        if (eqq.getSocketState() != 0)
        {
            c.getPlayer().dropMessage(1, "This item already has a socket.");
        }
        else
        {
            eqq.setSocket1(0);
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, alienSocket.getPosition(), (short) 1, false);
            c.getPlayer().forceUpdateItem(toMount);
        }
        c.getSession().write(tools.packet.MTSCSPacket.useAlienSocket(true));
    }

    public static void UseNebuliteFusion(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getPlayer().updateTick(slea.readInt());
        c.getPlayer().setScrolledPosition((short) 0);
        int nebuliteId1 = slea.readInt();
        Item nebulite1 = c.getPlayer().getInventory(MapleInventoryType.SETUP).getItem((byte) slea.readShort());
        int nebuliteId2 = slea.readInt();
        Item nebulite2 = c.getPlayer().getInventory(MapleInventoryType.SETUP).getItem((byte) slea.readShort());
        int mesos = slea.readInt();
        int premiumQuantity = slea.readInt();
        if ((nebulite1 == null) || (nebulite2 == null) || (nebuliteId1 != nebulite1.getItemId()) || (nebuliteId2 != nebulite2.getItemId()) || ((mesos == 0) && (premiumQuantity == 0)) || ((mesos != 0) && (premiumQuantity != 0)) || (mesos < 0) || (premiumQuantity < 0) || (c.getPlayer().hasBlockedInventory()))
        {
            c.getPlayer().dropMessage(1, "Failed to fuse Nebulite.");
            c.getSession().write(InventoryPacket.getInventoryFull());
            return;
        }
        int grade1 = ItemConstants.getNebuliteGrade(nebuliteId1);
        int grade2 = ItemConstants.getNebuliteGrade(nebuliteId2);
        int highestRank = grade1 > grade2 ? grade1 : grade2;
        if ((grade1 == -1) || (grade2 == -1) || ((highestRank == 3) && (premiumQuantity != 2)) || ((highestRank == 2) && (premiumQuantity != 1)) || ((highestRank == 1) && (mesos != 5000)) || ((highestRank == 0) && (mesos != 3000)) || ((mesos > 0) && (c.getPlayer().getMeso() < mesos)) || ((premiumQuantity > 0) && (c.getPlayer().getItemQuantity(4420000, false) < premiumQuantity)) || (grade1 >= 4) || (grade2 >= 4) || (c.getPlayer().getInventory(MapleInventoryType.SETUP).getNumFreeSlot() < 1))
        {
            c.getSession().write(InventoryPacket.useNebuliteFusion(c.getPlayer().getId(), 0, false));
            return;
        }
        int avg = (grade1 + grade2) / 2;
        int rank = Randomizer.nextInt(100) < 4 ? 0 : avg != 0 ? avg - 1 : Randomizer.nextInt(100) < 70 ? avg : avg != 3 ? avg + 1 : avg;

        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        List<StructItemOption> pots = new LinkedList(ii.getAllSocketInfo(rank).values());
        int newId = 0;
        while (newId == 0)
        {
            StructItemOption pot = pots.get(Randomizer.nextInt(pots.size()));
            if (pot != null)
            {
                newId = pot.opID;
            }
        }
        if (mesos > 0)
        {
            c.getPlayer().gainMeso(-mesos, true);
        }
        else if (premiumQuantity > 0)
        {
            MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4420000, premiumQuantity, false, false);
        }
        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.SETUP, nebulite1.getPosition(), (short) 1, false);
        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.SETUP, nebulite2.getPosition(), (short) 1, false);
        MapleInventoryManipulator.addById(c, newId, (short) 1, "Fused from " + nebuliteId1 + " and " + nebuliteId2 + " on " + FileoutputUtil.CurrentReadable_Date());
        c.getSession().write(InventoryPacket.useNebuliteFusion(c.getPlayer().getId(), newId, true));
    }


    public static void UseAdditionalItem(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (!chr.isAlive()) || (chr.getMap() == null) || (chr.hasBlockedInventory()) || (chr.inPVP()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        byte toSlot = (byte) slea.readShort();
        Item scroll = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        Equip toScroll = (Equip) chr.getInventory(MapleInventoryType.EQUIP).getItem(toSlot);
        if ((scroll == null) || (scroll.getQuantity() < 0) || (toScroll == null) || (toScroll.getQuantity() != 1))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int successRate = ii.getScrollSuccess(scroll.getItemId());
        boolean noCursed = ii.isNoCursedScroll(scroll.getItemId());
        if (successRate <= 0)
        {
            c.getPlayer().dropMessage(1, "卷轴道具: " + scroll.getItemId() + " - " + ii.getName(scroll.getItemId()) + " 成功几率为: " + successRate + " 该卷轴可能还未修复.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.isAdmin())
        {
            chr.dropSpouseMessage(11, "卷轴道具: " + scroll.getItemId() + " - " + ii.getName(scroll.getItemId()) + " 成功几率为: " + successRate + "% 卷轴是否失败不消失装备: " + noCursed);
        }
        if ((toScroll.getPotential4() == 0) || (toScroll.getPotential5() == 0) || (toScroll.getPotential6() == 0))
        {
            boolean success = false;
            int lines = toScroll.getPotential5() == 0 ? 5 : toScroll.getPotential4() == 0 ? 4 : 6;
            int reqLevel = ii.getReqLevel(toScroll.getItemId()) / 10;
            if (Randomizer.nextInt(100) <= successRate)
            {
                List<List<StructItemOption>> pots = new LinkedList(ii.getAllPotentialInfo().values());
                boolean rewarded = false;
                while (!rewarded)
                {
                    StructItemOption pot = (StructItemOption) ((List) pots.get(Randomizer.nextInt(pots.size()))).get(reqLevel);
                    if ((pot != null) && (!GameConstants.isBlockedPotential(pot.opID)) && (pot.reqLevel / 10 <= reqLevel) && (GameConstants.optionTypeFits(pot.optionType, toScroll.getItemId())) && (GameConstants.optionTypeFitsX(pot.opID, toScroll.getItemId())) && (GameConstants.potentialIDFits(pot.opID, 17, 1)))
                    {
                        if (lines == 4)
                        {
                            toScroll.setPotential4(pot.opID);
                            if (chr.isAdmin())
                            {
                                chr.dropMessage(5, "附加潜能4 获得ID： " + pot.opID);
                            }
                        }
                        else if (lines == 5)
                        {
                            toScroll.setPotential5(pot.opID);
                            if (chr.isAdmin())
                            {
                                chr.dropMessage(5, "附加潜能5 获得ID： " + pot.opID);
                            }
                        }
                        else
                        {
                            toScroll.setPotential6(pot.opID);
                            if (chr.isAdmin())
                            {
                                chr.dropMessage(6, "附加潜能6 获得ID： " + pot.opID);
                            }
                        }
                        rewarded = true;
                    }
                }
                success = true;
            }


            List<client.inventory.ModifyInventory> mods = new ArrayList<>();
            boolean removeItem = false;
            if ((!success) && (!noCursed))
            {
                mods.add(new client.inventory.ModifyInventory(3, toScroll));
                removeItem = true;
                chr.getInventory(MapleInventoryType.EQUIP).removeItem(toScroll.getPosition());
            }
            else
            {
                mods.add(new client.inventory.ModifyInventory(3, toScroll));
                mods.add(new client.inventory.ModifyInventory(0, toScroll));
            }
            c.getSession().write(InventoryPacket.modifyInventory(true, mods, chr));

            chr.getMap().broadcastMessage(InventoryPacket.潜能扩展效果(chr.getId(), success, scroll.getItemId(), removeItem));
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, scroll.getPosition(), (short) 1, false);
        }
        else
        {
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void BuyCrossHunterItem(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (!chr.isAlive()) || (chr.getMap() == null) || (chr.hasBlockedInventory()) || (chr.inPVP()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int key = slea.readShort();
        int itemId = slea.readInt();
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        server.StructCrossHunterShop shop = ii.getCrossHunterShop(key);
        if ((shop != null) && (itemId == shop.getItemId()) && (shop.getTokenPrice() > 0))
        {
            if (chr.getInventory(MapleInventoryType.ETC).countById(4310029) >= shop.getTokenPrice())
            {
                if (MapleInventoryManipulator.checkSpace(c, shop.getItemId(), 1, ""))
                {
                    MapleInventoryManipulator.removeById(c, MapleInventoryType.ETC, 4310029, shop.getTokenPrice(), true, false);
                    MapleInventoryManipulator.addById(c, shop.getItemId(), (short) 1, shop.getPotentialGrade(), "十字商店购买: " + FileoutputUtil.CurrentReadable_Date());
                    c.getSession().write(MaplePacketCreator.confirmCrossHunter((byte) 0));
                }
                else
                {
                    c.getSession().write(MaplePacketCreator.confirmCrossHunter((byte) 2));
                }
            }
            else
            {
                c.getSession().write(MaplePacketCreator.confirmCrossHunter((byte) 1));
            }
        }
        else
        {
            c.getSession().write(MaplePacketCreator.confirmCrossHunter((byte) 4));
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\InventoryHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */