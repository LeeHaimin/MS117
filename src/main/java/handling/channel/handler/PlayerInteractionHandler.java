package handling.channel.handler;

import org.apache.log4j.Logger;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.MapleInventoryType;
import constants.ItemConstants;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.MapleTrade;
import server.maps.MapleMapObjectType;
import server.shops.HiredMerchant;
import server.shops.IMaplePlayerShop;
import server.shops.MapleMiniGame;
import server.shops.MaplePlayerShop;
import server.shops.MaplePlayerShopItem;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.PlayerShopPacket;

public class PlayerInteractionHandler
{
    private static final Logger log = Logger.getLogger(PlayerInteractionHandler.class);

    public static void PlayerInteraction(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        byte mode = slea.readByte();
        handling.InteractionOpcode action = handling.InteractionOpcode.getByAction(mode);
        if ((chr == null) || (action == null))
        {
            if (server.ServerProperties.ShowPacket())
            {
                System.out.println("玩家互动未知的操作类型: 0x" + tools.StringUtil.getLeftPaddedStr(Integer.toHexString(mode).toUpperCase(), '0', 2) + " " + slea.toString());
            }
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.getAntiMacro().inProgress())
        {
            chr.dropMessage(5, "被使用测谎仪时无法操作。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.setScrolledPosition((short) 0);
        if (chr.isAdmin())
        {
            chr.dropMessage(5, "玩家互动操作类型: " + action);
        }
        switch (action)
        {
            case 创建:
                if ((chr.getPlayerShop() != null) || (c.getChannelServer().isShutdown()) || (chr.hasBlockedInventory()))
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                byte createType = slea.readByte();
                if (createType == 4)
                {
                    MapleTrade.startTrade(chr);
                }
                else if ((createType == 1) || (createType == 2) || (createType == 5) || (createType == 6))
                {
                    if ((!chr.getMap().getMapObjectsInRange(chr.getTruePosition(), 20000.0D, java.util.Arrays.asList(MapleMapObjectType.SHOP, MapleMapObjectType.HIRED_MERCHANT)).isEmpty()) || (!chr.getMap().getPortalsInRange(chr.getTruePosition(), 20000.0D).isEmpty()))
                    {
                        chr.dropMessage(1, "只能在钓鱼场摆摊.请从不夜城渔夫那里进入.");
                        c.getSession().write(MaplePacketCreator.enableActions());
                        return;
                    }
                    if (((createType == 1) || (createType == 2)) && ((server.maps.FieldLimitType.Minigames.check(chr.getMap().getFieldLimit())) || (chr.getMap().allowPersonalShop())))
                    {
                        chr.dropMessage(1, "只能在钓鱼场摆摊.请从不夜城渔夫那里进入.");
                        c.getSession().write(MaplePacketCreator.enableActions());
                        return;
                    }

                    String desc = slea.readMapleAsciiString();
                    String pass = "";
                    if (slea.readByte() > 0)
                    {
                        pass = slea.readMapleAsciiString();
                    }
                    if ((createType == 1) || (createType == 2))
                    {
                        int piece = slea.readByte();
                        int itemId = createType == 1 ? 4080000 + piece : 4080100;
                        if ((!chr.haveItem(itemId)) || ((c.getPlayer().getMapId() >= 910000001) && (c.getPlayer().getMapId() <= 910000022)))
                        {
                            return;
                        }
                        MapleMiniGame game = new MapleMiniGame(chr, itemId, desc, pass, createType);
                        game.setPieceType(piece);
                        chr.setPlayerShop(game);
                        game.setAvailable(true);
                        game.setOpen(true);
                        game.send(c);
                        chr.getMap().addMapObject(game);
                        game.update();
                    }
                    else if (chr.getMap().allowPersonalShop())
                    {
                        Item shop = c.getPlayer().getInventory(MapleInventoryType.CASH).getItem((byte) slea.readShort());
                        if ((shop == null) || (shop.getQuantity() <= 0) || (shop.getItemId() != slea.readInt()) || (c.getPlayer().getMapId() < 910000001) || (c.getPlayer().getMapId() > 910000022))
                        {
                            return;
                        }
                        if (createType == 4)
                        {
                            MaplePlayerShop mps = new MaplePlayerShop(chr, shop.getItemId(), desc);
                            chr.setPlayerShop(mps);
                            chr.getMap().addMapObject(mps);
                            c.getSession().write(PlayerShopPacket.getPlayerStore(chr, true));
                        }
                        else if (HiredMerchantHandler.UseHiredMerchant(chr.getClient(), false))
                        {
                            HiredMerchant merch = new HiredMerchant(chr, shop.getItemId(), desc);
                            chr.setPlayerShop(merch);
                            chr.getMap().addMapObject(merch);
                            c.getSession().write(PlayerShopPacket.getHiredMerch(chr, merch, true));
                        }
                    }
                }
                break;


            case 交易邀请:
                if (chr.getMap() == null)
                {
                    return;
                }
                MapleCharacter chrr = chr.getMap().getCharacterById(slea.readInt());
                if ((chrr == null) || (c.getChannelServer().isShutdown()) || (chrr.hasBlockedInventory()))
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                MapleTrade.inviteTrade(chr, chrr);
                break;

            case 拒绝邀请:
                MapleTrade.declineTrade(chr);
                break;

            case 访问:
                if (c.getChannelServer().isShutdown())
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                if ((chr.getTrade() != null) && (chr.getTrade().getPartner() != null) && (!chr.getTrade().inTrade()))
                {
                    MapleTrade.visitTrade(chr, chr.getTrade().getPartner().getChr());
                }
                else if ((chr.getMap() != null) && (chr.getTrade() == null))
                {
                    int obid = slea.readInt();
                    server.maps.MapleMapObject ob = chr.getMap().getMapObject(obid, MapleMapObjectType.HIRED_MERCHANT);
                    if (ob == null)
                    {
                        ob = chr.getMap().getMapObject(obid, MapleMapObjectType.SHOP);
                    }
                    if (((ob instanceof IMaplePlayerShop)) && (chr.getPlayerShop() == null))
                    {
                        IMaplePlayerShop ips = (IMaplePlayerShop) ob;
                        if ((ob instanceof HiredMerchant))
                        {
                            HiredMerchant merchant = (HiredMerchant) ips;
                            if ((merchant.isOwner(chr)) && (merchant.isOpen()) && (merchant.isAvailable()))
                            {
                                merchant.setOpen(false);
                                merchant.removeAllVisitors(18, 1);
                                chr.setPlayerShop(ips);
                                c.getSession().write(PlayerShopPacket.getHiredMerch(chr, merchant, false));
                            }
                            else if ((!merchant.isOpen()) || (!merchant.isAvailable()))
                            {
                                chr.dropMessage(1, "主人正在整理商店物品\r\n请稍后再度光临！");
                            }
                            else if (ips.getFreeSlot() == -1)
                            {
                                chr.dropMessage(1, "店铺已达到最大人数\r\n请稍后再度光临！");
                            }
                            else if (merchant.isInBlackList(chr.getName()))
                            {
                                chr.dropMessage(1, "你被禁止进入该店铺");
                            }
                            else
                            {
                                chr.setPlayerShop(ips);
                                merchant.addVisitor(chr);
                                c.getSession().write(PlayerShopPacket.getHiredMerch(chr, merchant, false));
                            }


                        }
                        else if (((ips instanceof MaplePlayerShop)) && (((MaplePlayerShop) ips).isBanned(chr.getName())))
                        {
                            chr.dropMessage(1, "你被禁止进入该店铺");
                        }
                        else if ((ips.getFreeSlot() < 0) || (ips.getVisitorSlot(chr) > -1) || (!ips.isOpen()) || (!ips.isAvailable()))
                        {
                            c.getSession().write(PlayerShopPacket.getMiniGameFull());
                        }
                        else
                        {
                            if ((slea.available() > 0L) && (slea.readByte() > 0))
                            {
                                String pass = slea.readMapleAsciiString();
                                if (!pass.equals(ips.getPassword()))
                                {
                                    c.getPlayer().dropMessage(1, "你输入的密码不正确.");
                                    return;
                                }
                            }
                            else if (ips.getPassword().length() > 0)
                            {
                                c.getPlayer().dropMessage(1, "你输入的密码不正确.");
                                return;
                            }
                            chr.setPlayerShop(ips);
                            ips.addVisitor(chr);
                            if ((ips instanceof MapleMiniGame))
                            {
                                ((MapleMiniGame) ips).send(c);
                            }
                            else
                            {
                                c.getSession().write(PlayerShopPacket.getPlayerStore(chr, false));
                            }
                        }
                    }
                }

                break;


            case 聊天:
                chr.updateTick(slea.readInt());
                String message = slea.readMapleAsciiString();
                if (chr.getTrade() != null)
                {
                    chr.getTrade().chat(message);
                }
                else if (chr.getPlayerShop() != null)
                {
                    IMaplePlayerShop ips = chr.getPlayerShop();
                    ips.broadcastToVisitors(PlayerShopPacket.shopChat(chr.getName() + " : " + message, ips.getVisitorSlot(chr)));
                    if (ips.getShopType() == 1)
                    {
                        ips.getMessages().add(new tools.Pair(chr.getName() + " : " + message, ips.getVisitorSlot(chr)));
                    }
                    if (chr.getClient().isMonitored())
                        handling.world.WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                                chr.getName() + " said in " + ips.getOwnerName() + " shop : " + message));
                }
                break;


            case 退出:
                if (chr.getTrade() != null)
                {
                    MapleTrade.cancelTrade(chr.getTrade(), chr.getClient(), chr);
                }
                else
                {
                    IMaplePlayerShop ips = chr.getPlayerShop();
                    if (ips == null)
                    {
                        return;
                    }
                    if ((ips.isOwner(chr)) && (ips.getShopType() != 1))
                    {
                        ips.closeShop(false, ips.isAvailable());
                    }
                    else
                    {
                        ips.removeVisitor(chr);
                    }
                    chr.setPlayerShop(null);
                }
                break;


            case 打开:
                IMaplePlayerShop shop = chr.getPlayerShop();
                if ((shop != null) && (shop.isOwner(chr)) && (shop.getShopType() < 3) && (!shop.isAvailable()))
                {
                    if (chr.getMap().allowPersonalShop())
                    {
                        if (c.getChannelServer().isShutdown())
                        {
                            chr.dropMessage(1, "服务器即将关闭维护，暂时无法进行此操作。.");
                            c.getSession().write(MaplePacketCreator.enableActions());
                            shop.closeShop(shop.getShopType() == 1, false);
                            return;
                        }
                        if ((shop.getShopType() == 1) && (HiredMerchantHandler.UseHiredMerchant(chr.getClient(), false)))
                        {
                            HiredMerchant merchant = (HiredMerchant) shop;
                            merchant.setStoreid(c.getChannelServer().addMerchant(merchant));
                            merchant.setOpen(true);
                            merchant.setAvailable(true);
                            shop.saveItems();
                            chr.getMap().broadcastMessage(PlayerShopPacket.spawnHiredMerchant(merchant));
                            chr.setPlayerShop(null);
                        }
                        else if (shop.getShopType() == 2)
                        {
                            shop.setOpen(true);
                            shop.setAvailable(true);
                            shop.update();
                        }
                    }
                    else
                    {
                        chr.getClient().disconnect(true, false);
                        c.getSession().close(true);
                    }
                }

                break;
            case 设置物品:
            case 设置物品_001:
            case 设置物品_002:
            case 设置物品_003:
                MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
                MapleInventoryType ivType = MapleInventoryType.getByType(slea.readByte());
                Item item = chr.getInventory(ivType).getItem((byte) slea.readShort());
                short quantity = slea.readShort();
                byte targetSlot = slea.readByte();
                if ((chr.getTrade() != null) && (item != null))
                {


                    boolean canTrade = true;
                    if ((item.getItemId() == 4000463) && (!canTrade))
                    {
                        chr.dropMessage(1, "该道具无法进行交易.");
                        c.getSession().write(MaplePacketCreator.enableActions());
                    }
                    else if (((quantity <= item.getQuantity()) && (quantity >= 0)) || (ItemConstants.is飞镖道具(item.getItemId())) || (ItemConstants.is子弹道具(item.getItemId())))
                    {
                        chr.getTrade().setItems(c, item, targetSlot, quantity);
                    }
                }
                break;


            case 设置金币:
            case 设置金币_005:
            case 设置金币_006:
            case 设置金币_007:
                MapleTrade trade = chr.getTrade();
                if (trade != null)
                {
                    trade.setMeso(slea.readInt());
                }

                break;
            case 确认交易:
            case 确认交易_009:
            case 确认交易_00A:
            case 确认交易_00B:
                if (chr.getTrade() != null) MapleTrade.completeTrade(chr);
                break;


            case 玩家商店_添加道具:
            case 添加物品:
            case 添加物品_0020:
            case 添加物品_0021:
            case 添加物品_0022:
                MapleInventoryType type = MapleInventoryType.getByType(slea.readByte());
                byte slot = (byte) slea.readShort();
                short bundles = slea.readShort();
                short perBundle = slea.readShort();
                int price = slea.readInt();
                if ((price <= 0) || (bundles <= 0) || (perBundle <= 0))
                {
                    chr.dropMessage(1, "添加物品出现错误(1)");
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                IMaplePlayerShop shop1 = chr.getPlayerShop();
                if ((shop1 == null) || (!shop1.isOwner(chr)) || ((shop1 instanceof MapleMiniGame)))
                {
                    return;
                }
                Item ivItem = chr.getInventory(type).getItem(slot);
                MapleItemInformationProvider ii1 = MapleItemInformationProvider.getInstance();
                if (ivItem != null)
                {
                    long check = bundles * perBundle;
                    if ((check > 32767L) || (check <= 0L))
                    {
                        return;
                    }
                    short bundles_perbundle = (short) (bundles * perBundle);
                    if (ivItem.getQuantity() >= bundles_perbundle)
                    {
                        short flag = ivItem.getFlag();
                        if ((ItemFlag.UNTRADEABLE.check(flag)) || (ItemFlag.LOCK.check(flag)))
                        {
                            c.getSession().write(MaplePacketCreator.enableActions());
                            return;
                        }
                        if (((ii1.isDropRestricted(ivItem.getItemId())) || (ii1.isAccountShared(ivItem.getItemId()))) && (!ItemFlag.KARMA_EQ.check(flag)) && (!ItemFlag.KARMA_USE.check(flag)))
                        {
                            c.getSession().write(MaplePacketCreator.enableActions());
                            return;
                        }


                        if (ivItem.getItemId() == 4000463)
                        {
                            chr.dropMessage(1, "该道具无法进行贩卖.");
                            c.getSession().write(MaplePacketCreator.enableActions());
                            return;
                        }
                        if ((bundles_perbundle >= 50) && (ivItem.getItemId() == 2340000))
                        {
                            c.setMonitored(true);
                        }
                        if (ItemConstants.getLowestPrice(ivItem.getItemId()) > price)
                        {
                            c.getPlayer().dropMessage(1, "The lowest you can sell this for is " + ItemConstants.getLowestPrice(ivItem.getItemId()));
                            c.getSession().write(MaplePacketCreator.enableActions());
                            return;
                        }
                        if ((ItemConstants.is飞镖道具(ivItem.getItemId())) || (ItemConstants.is子弹道具(ivItem.getItemId())))
                        {
                            MapleInventoryManipulator.removeFromSlot(c, type, slot, ivItem.getQuantity(), true);
                            Item sellItem = ivItem.copy();
                            shop1.addItem(new MaplePlayerShopItem(sellItem, (short) 1, price));
                        }
                        else
                        {
                            MapleInventoryManipulator.removeFromSlot(c, type, slot, bundles_perbundle, true);
                            Item sellItem = ivItem.copy();
                            sellItem.setQuantity(perBundle);
                            shop1.addItem(new MaplePlayerShopItem(sellItem, bundles, price));
                        }
                        c.getSession().write(PlayerShopPacket.shopItemUpdate(shop1));
                    }
                    else
                    {
                        chr.dropMessage(1, "添加物品的数量错误。如果是飞镖，子弹之类请充了后在进行贩卖。");
                    }
                }
                break;


            case 玩家商店_购买道具:
            case BUY_ITEM_STORE:
            case 雇佣商店_购买道具:
            case 雇佣商店_购买道具0024:
            case 雇佣商店_购买道具0025:
            case 雇佣商店_购买道具0026:
                int item1 = slea.readByte();
                short quantity1 = slea.readShort();

                IMaplePlayerShop shop2 = chr.getPlayerShop();
                if ((shop2 == null) || (shop2.isOwner(chr)) || ((shop2 instanceof MapleMiniGame)) || (item1 >= shop2.getItems().size()))
                {
                    chr.dropMessage(1, "购买道具出现错误(1)");
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                MaplePlayerShopItem tobuy = shop2.getItems().get(item1);
                if (tobuy == null)
                {
                    chr.dropMessage(1, "购买道具出现错误(2)");
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                long check = tobuy.bundles * quantity1;
                long check2 = tobuy.price * quantity1;
                long check3 = tobuy.item.getQuantity() * quantity1;
                if ((check <= 0L) || (check2 > 2147483647L) || (check2 <= 0L) || (check3 > 32767L) || (check3 < 0L))
                {
                    chr.dropMessage(1, "购买道具出现错误(3)");
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                if (chr.getMeso() - check2 < 0L)
                {
                    c.getSession().write(PlayerShopPacket.Merchant_Buy_Error((byte) 2));
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                if ((tobuy.bundles < quantity1) || ((tobuy.bundles % quantity1 != 0) && (ItemConstants.isEquip(tobuy.item.getItemId()))) || (chr.getMeso() - check2 > 2147483647L) || (shop2.getMeso() + check2 < 0L) || (shop2.getMeso() + check2 > 2147483647L))
                {
                    chr.dropMessage(1, "购买道具出现错误(4)");
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                if ((quantity1 >= 50) && (tobuy.item.getItemId() == 2340000))
                {
                    c.setMonitored(true);
                }
                shop2.buy(c, item1, quantity1);
                shop2.broadcastToVisitors(PlayerShopPacket.shopItemUpdate(shop2));
                break;


            case 雇佣商店_求购道具:
                chr.dropMessage(1, "当前服务器暂不支持求购道具.");
                break;


            case 雇佣商店_维护:
                slea.skip(1);
                byte type1 = slea.readByte();
                slea.skip(3);
                int obid = slea.readInt();
                if (type1 == 6)
                {
                    server.maps.MapleMapObject ob = chr.getMap().getMapObject(obid, MapleMapObjectType.HIRED_MERCHANT);
                    if (((ob instanceof IMaplePlayerShop)) && (chr.getPlayerShop() == null))
                    {
                        IMaplePlayerShop ips = (IMaplePlayerShop) ob;
                        if ((ob instanceof HiredMerchant))
                        {
                            HiredMerchant merchant = (HiredMerchant) ips;
                            if ((merchant.isOwner(chr)) && (merchant.isOpen()) && (merchant.isAvailable()))
                            {
                                merchant.setOpen(false);
                                merchant.removeAllVisitors(18, 1);
                                chr.setPlayerShop(ips);
                                c.getSession().write(PlayerShopPacket.getHiredMerch(chr, merchant, false));
                            }
                            else if ((!merchant.isOpen()) || (!merchant.isAvailable()))
                            {
                                chr.dropMessage(1, "主人正在整理商店物品\r\n请稍后再度光临！");
                            }
                            else if (ips.getFreeSlot() == -1)
                            {
                                chr.dropMessage(1, "店铺已达到最大人数\r\n请稍后再度光临！");
                            }
                            else if (merchant.isInBlackList(chr.getName()))
                            {
                                chr.dropMessage(1, "你被禁止进入该店铺");
                            }
                        }
                    }
                }
                else
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                }
                break;

            case 移除物品:
                slea.skip(1);
                int slot1 = slea.readShort();
                IMaplePlayerShop shop3 = chr.getPlayerShop();
                if (chr.isAdmin())
                {
                    chr.dropMessage(5, "移除商店道具: 道具数量 " + shop3.getItems().size() + " slot " + slot1);
                }
                if ((shop3 == null) || (!shop3.isOwner(chr)) || ((shop3 instanceof MapleMiniGame)) || (shop3.getItems().size() <= 0) || (shop3.getItems().size() <= slot1) || (slot1 < 0))
                {
                    return;
                }
                MaplePlayerShopItem item2 = shop3.getItems().get(slot1);
                if ((item2 != null) && (item2.bundles > 0))
                {
                    Item item_get = item2.item.copy();
                    long check4 = item2.bundles * item2.item.getQuantity();
                    if ((check4 < 0L) || (check4 > 32767L))
                    {
                        if (chr.isAdmin())
                        {
                            chr.dropMessage(5, "移除商店道具出错: check " + check4);
                        }
                        return;
                    }
                    item_get.setQuantity((short) (int) check4);
                    if ((item_get.getQuantity() >= 50) && (item2.item.getItemId() == 2340000))
                    {
                        c.setMonitored(true);
                    }
                    if (MapleInventoryManipulator.checkSpace(c, item_get.getItemId(), item_get.getQuantity(), item_get.getOwner()))
                    {
                        MapleInventoryManipulator.addFromDrop(c, item_get, false);
                        item2.bundles = 0;
                        shop3.removeFromSlot(slot1);
                    }
                }

                c.getSession().write(PlayerShopPacket.shopItemUpdate(shop3));
                break;

            case 雇佣商店_开启:
            case 雇佣商店_维护开启:
                IMaplePlayerShop shop5 = chr.getPlayerShop();
                if (((shop5 instanceof HiredMerchant)) && (shop5.isOwner(chr)) && (shop5.isAvailable()))
                {
                    shop5.setOpen(true);
                    shop5.saveItems();
                    shop5.getMessages().clear();
                    shop5.removeAllVisitors(-1, -1);
                }
                c.getSession().write(MaplePacketCreator.enableActions());
                break;

            case 雇佣商店_整理:
                IMaplePlayerShop imps = chr.getPlayerShop();
                if ((imps != null) && (imps.isOwner(chr)) && (!(imps instanceof MapleMiniGame)))
                {
                    for (int i = 0; i < imps.getItems().size(); i++)
                    {
                        if (imps.getItems().get(i).bundles == 0)
                        {
                            imps.getItems().remove(i);
                        }
                    }
                    if (chr.getMeso() + imps.getMeso() > 0)
                    {
                        chr.gainMeso(imps.getMeso(), false);
                        log.info("[雇佣] " + chr.getName() + " 雇佣整理获得金币: " + imps.getMeso() + " 时间: " + tools.FileoutputUtil.CurrentReadable_Date());
                        tools.FileoutputUtil.hiredMerchLog(chr.getName(), "雇佣整理获得金币: " + imps.getMeso());
                        imps.setMeso(0);
                    }
                    c.getSession().write(PlayerShopPacket.shopItemUpdate(imps));
                }

                break;
            case 雇佣商店_关闭:
                IMaplePlayerShop merchant = chr.getPlayerShop();
                if ((merchant != null) && (merchant.getShopType() == 1) && (merchant.isOwner(chr)))
                {
                    c.getSession().write(PlayerShopPacket.hiredMerchantOwnerLeave());
                    merchant.removeAllVisitors(-1, -1);
                    chr.setPlayerShop(null);
                    merchant.closeShop(true, true);
                }
                else
                {
                    chr.dropMessage(1, "关闭商店出现未知错误.");
                    c.getSession().write(MaplePacketCreator.enableActions());
                }
                break;


            case 管理员修改雇佣商店名称:
                chr.dropMessage(1, "暂不支持管理员修改雇佣商店的名字.");
                c.getSession().write(MaplePacketCreator.enableActions());
                break;

            case 雇佣商店_查看访问名单:
                IMaplePlayerShop merchant1 = chr.getPlayerShop();
                if ((merchant1 != null) && (merchant1.getShopType() == 1) && (merchant1.isOwner(chr)))
                {
                    ((HiredMerchant) merchant1).sendVisitor(c);
                }

                break;
            case 雇佣商店_查看黑名单:
                IMaplePlayerShop merchant2 = chr.getPlayerShop();
                if ((merchant2 != null) && (merchant2.getShopType() == 1) && (merchant2.isOwner(chr)))
                {
                    ((HiredMerchant) merchant2).sendBlackList(c);
                }

                break;
            case 雇佣商店_添加黑名单:
                IMaplePlayerShop merchant3 = chr.getPlayerShop();
                if ((merchant3 != null) && (merchant3.getShopType() == 1) && (merchant3.isOwner(chr)))
                {
                    ((HiredMerchant) merchant3).addBlackList(slea.readMapleAsciiString());
                }

                break;
            case 雇佣商店_移除黑名单:
                IMaplePlayerShop merchant4 = chr.getPlayerShop();
                if ((merchant4 != null) && (merchant4.getShopType() == 1) && (merchant4.isOwner(chr)))
                {
                    ((HiredMerchant) merchant4).removeBlackList(slea.readMapleAsciiString());
                }

                break;
            case 雇佣商店_修改商店名称:
                IMaplePlayerShop merchant5 = chr.getPlayerShop();
                if ((merchant5 != null) && (merchant5.getShopType() == 1) && (merchant5.isOwner(chr)))
                {
                    String desc = slea.readMapleAsciiString();
                    if (((HiredMerchant) merchant5).canChangeName())
                    {
                        merchant5.setDescription(desc);
                    }
                    else c.getSession().write(MaplePacketCreator.craftMessage("还不能变更名称，还需要等待" + ((HiredMerchant) merchant5).getChangeNameTimeLeft() + "秒。"));
                }
                break;


            case GIVE_UP:
                IMaplePlayerShop ips = chr.getPlayerShop();
                if (((ips instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips;
                    if (!game.isOpen())
                    {

                        game.broadcastToVisitors(PlayerShopPacket.getMiniGameResult(game, 0, game.getVisitorSlot(chr)));
                        game.nextLoser();
                        game.setOpen(true);
                        game.update();
                        game.checkExitAfterGame();
                    }
                }
                break;


            case EXPEL:
                IMaplePlayerShop ips1 = chr.getPlayerShop();
                if (((ips1 instanceof MapleMiniGame)) && (ips1.isOpen()))
                {

                    ips1.removeAllVisitors(3, 1);
                }

                break;
            case READY:
            case UN_READY:
                IMaplePlayerShop ips2 = chr.getPlayerShop();
                if (((ips2 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips2;
                    if ((!game.isOwner(chr)) && (game.isOpen()))
                    {
                        game.setReady(game.getVisitorSlot(chr));
                        game.broadcastToVisitors(PlayerShopPacket.getMiniGameReady(game.isReady(game.getVisitorSlot(chr))));
                    }
                }
                break;


            case START:
                IMaplePlayerShop ips3 = chr.getPlayerShop();
                if (((ips3 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips3;
                    if ((game.isOwner(chr)) && (game.isOpen()))
                    {
                        for (int i = 1; i < ips3.getSize(); i++)
                        {
                            if (!game.isReady(i))
                            {
                                return;
                            }
                        }
                        game.setGameType();
                        game.shuffleList();
                        if (game.getGameType() == 1)
                        {
                            game.broadcastToVisitors(PlayerShopPacket.getMiniGameStart(game.getLoser()));
                        }
                        else
                        {
                            game.broadcastToVisitors(PlayerShopPacket.getMatchCardStart(game, game.getLoser()));
                        }
                        game.setOpen(false);
                        game.update();
                    }
                }
                break;


            case REQUEST_TIE:
                IMaplePlayerShop ips4 = chr.getPlayerShop();
                if (((ips4 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips4;
                    if (!game.isOpen())
                    {

                        if (game.isOwner(chr))
                        {
                            game.broadcastToVisitors(PlayerShopPacket.getMiniGameRequestTie(), false);
                        }
                        else
                        {
                            game.getMCOwner().getClient().getSession().write(PlayerShopPacket.getMiniGameRequestTie());
                        }
                        game.setRequestedTie(game.getVisitorSlot(chr));
                    }
                }
                break;


            case ANSWER_TIE:
                IMaplePlayerShop ips5 = chr.getPlayerShop();
                if (((ips5 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips5;
                    if (!game.isOpen())
                    {

                        if ((game.getRequestedTie() > -1) && (game.getRequestedTie() != game.getVisitorSlot(chr)))
                        {
                            if (slea.readByte() > 0)
                            {
                                game.broadcastToVisitors(PlayerShopPacket.getMiniGameResult(game, 1, game.getRequestedTie()));
                                game.nextLoser();
                                game.setOpen(true);
                                game.update();
                                game.checkExitAfterGame();
                            }
                            else
                            {
                                game.broadcastToVisitors(PlayerShopPacket.getMiniGameDenyTie());
                            }
                            game.setRequestedTie(-1);
                        }
                    }
                }
                break;


            case SKIP:
                IMaplePlayerShop ips6 = chr.getPlayerShop();
                if (((ips6 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips6;
                    if (!game.isOpen())
                    {

                        if (game.getLoser() != ips6.getVisitorSlot(chr))
                        {
                            ips6.broadcastToVisitors(PlayerShopPacket.shopChat("Turn could not be skipped by " + chr.getName() + ". Loser: " + game.getLoser() + " Visitor: " + ips6.getVisitorSlot(chr), ips6.getVisitorSlot(chr)));
                            return;
                        }
                        ips6.broadcastToVisitors(PlayerShopPacket.getMiniGameSkip(ips6.getVisitorSlot(chr)));
                        game.nextLoser();
                    }
                }
                break;


            case MOVE_OMOK:
                IMaplePlayerShop ips7 = chr.getPlayerShop();
                if (((ips7 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips7;
                    if (!game.isOpen())
                    {

                        if (game.getLoser() != game.getVisitorSlot(chr))
                        {
                            game.broadcastToVisitors(PlayerShopPacket.shopChat("Omok could not be placed by " + chr.getName() + ". Loser: " + game.getLoser() + " Visitor: " + game.getVisitorSlot(chr), game.getVisitorSlot(chr)));
                            return;
                        }
                        game.setPiece(slea.readInt(), slea.readInt(), slea.readByte(), chr);
                    }
                }
                break;


            case SELECT_CARD:
                IMaplePlayerShop ips8 = chr.getPlayerShop();
                if (((ips8 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips8;
                    if (!game.isOpen())
                    {

                        if (game.getLoser() != game.getVisitorSlot(chr))
                        {
                            game.broadcastToVisitors(PlayerShopPacket.shopChat("Card could not be placed by " + chr.getName() + ". Loser: " + game.getLoser() + " Visitor: " + game.getVisitorSlot(chr), game.getVisitorSlot(chr)));
                            return;
                        }
                        if (slea.readByte() != game.getTurn())
                        {
                            game.broadcastToVisitors(PlayerShopPacket.shopChat("Omok could not be placed by " + chr.getName() + ". Loser: " + game.getLoser() + " Visitor: " + game.getVisitorSlot(chr) + " Turn: " + game.getTurn(), game.getVisitorSlot(chr)));
                            return;
                        }
                        int slot6 = slea.readByte();
                        int turn = game.getTurn();
                        int fs = game.getFirstSlot();
                        if (turn == 1)
                        {
                            game.setFirstSlot(slot6);
                            if (game.isOwner(chr))
                            {
                                game.broadcastToVisitors(PlayerShopPacket.getMatchCardSelect(turn, slot6, fs, turn), false);
                            }
                            else
                            {
                                game.getMCOwner().getClient().getSession().write(PlayerShopPacket.getMatchCardSelect(turn, slot6, fs, turn));
                            }
                            game.setTurn(0);
                            return;
                        }
                        if ((fs > 0) && (game.getCardId(fs + 1) == game.getCardId(slot6 + 1)))
                        {
                            game.broadcastToVisitors(PlayerShopPacket.getMatchCardSelect(turn, slot6, fs, game.isOwner(chr) ? 2 : 3));
                            game.setPoints(game.getVisitorSlot(chr));
                        }
                        else
                        {
                            game.broadcastToVisitors(PlayerShopPacket.getMatchCardSelect(turn, slot6, fs, game.isOwner(chr) ? 0 : 1));
                            game.nextLoser();
                        }
                        game.setTurn(1);
                        game.setFirstSlot(0);
                    }
                }
                break;
            case EXIT_AFTER_GAME:
            case CANCEL_EXIT:
                IMaplePlayerShop ips9 = chr.getPlayerShop();
                if (((ips9 instanceof MapleMiniGame)))
                {
                    MapleMiniGame game = (MapleMiniGame) ips9;
                    if (!game.isOpen())
                    {

                        game.setExitAfter(chr);
                        game.broadcastToVisitors(PlayerShopPacket.getMiniGameExitAfter(game.isExitAfter(chr)));
                    }
                }
                break;


            default:
                if (server.ServerProperties.ShowPacket())
                {
                    System.out.println("玩家互动未知的操作类型: 0x" + tools.StringUtil.getLeftPaddedStr(Integer.toHexString(mode).toUpperCase(), '0', 2) + " " + slea.toString());
                }
                c.getSession().write(MaplePacketCreator.enableActions());
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\PlayerInteractionHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */