package handling.channel.handler;

import java.util.ArrayList;
import java.util.List;

import client.MapleCharacter;
import client.MapleClient;
import client.PlayerStats;
import client.SkillFactory;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.MapleInventoryType;
import client.inventory.ModifyInventory;
import configs.ServerConfig;
import constants.ItemConstants;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.InventoryPacket;


public class ItemScrollHandler
{
    public static void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr, boolean cash)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        chr.updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        byte dst = (byte) slea.readShort();
        byte ws = 0;
        if (slea.available() >= 3L)
        {
            ws = (byte) slea.readShort();
        }


        UseUpgradeScroll(slot, dst, ws, c, chr, 0, cash);
    }

    public static boolean UseUpgradeScroll(short slot, short dst, short ws, MapleClient c, MapleCharacter chr, int vegas, boolean cash)
    {
        boolean whiteScroll = false;
        boolean legendarySpirit = false;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        chr.setScrolledPosition((short) 0);
        if ((ws & 0x2) == 2) whiteScroll = true;
        Equip toScroll;
        if (dst < 0)
        {
            toScroll = (Equip) chr.getInventory(MapleInventoryType.EQUIPPED).getItem(dst);
        }
        else
        {
            legendarySpirit = true;
            toScroll = (Equip) chr.getInventory(MapleInventoryType.EQUIP).getItem(dst);
        }
        if ((toScroll == null) || (c.getPlayer().hasBlockedInventory()))
        {
            return false;
        }
        byte oldLevel = toScroll.getLevel();
        byte oldEnhance = toScroll.getEnhance();
        byte oldState = toScroll.getState();
        byte oldAddState = toScroll.getAddState();
        short oldFlag = toScroll.getFlag();
        byte oldSlots = toScroll.getUpgradeSlots();
        int oldLimitBreak = toScroll.getLimitBreak();
        Item scroll;
        if (cash)
        {
            scroll = chr.getInventory(MapleInventoryType.CASH).getItem(slot);
        }
        else
        {
            scroll = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        }
        if (scroll == null)
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: 卷轴道具为空");
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if (chr.isAdmin())
        {
            chr.dropSpouseMessage(10, "砸卷信息: 卷轴ID " + scroll.getItemId() + " 卷轴名字 " + ii.getName(scroll.getItemId()));
        }
        if ((!ItemConstants.isSpecialScroll(scroll.getItemId())) && (!ItemConstants.isCleanSlate(scroll.getItemId())) && (!ItemConstants.isEquipScroll(scroll.getItemId())) && (!ItemConstants.isPotentialScroll(scroll.getItemId())) && (!ItemConstants.isPotentialAddScroll(scroll.getItemId())) && (!ItemConstants.isLimitBreakScroll(scroll.getItemId())) && (!ItemConstants.isResetScroll(scroll.getItemId())))
        {
            int scrollSlots = ItemConstants.isAzwanScroll(scroll.getItemId()) ? ii.getSlots(scroll.getItemId()) : 1;
            if (toScroll.getUpgradeSlots() < scrollSlots)
            {
                chr.dropMessage(1, "当前装备可升级次数为: " + toScroll.getUpgradeSlots() + " 成功或失败需要减少: " + scrollSlots + " 的升级次数，请检查该装备是否符合升级条件.");
                c.getSession().write(InventoryPacket.getInventoryFull());
                return false;
            }
        }
        else if (ItemConstants.isEquipScroll(scroll.getItemId()))
        {
            if ((toScroll.getUpgradeSlots() >= 1) || (toScroll.getEnhance() >= 100) || (vegas > 0) || (ii.isCash(toScroll.getItemId())))
            {
                if (chr.isAdmin())
                {
                    chr.dropMessage(-9,
                            "砸卷错误: 强化卷轴检测 装备是否有升级次数: " + (toScroll.getUpgradeSlots() >= 1) + " 装备星级是否大于100星: " + (toScroll.getEnhance() >= 100) + " - " + (vegas > 0) + " 装备是是否为点装: " + ii.isCash(toScroll.getItemId()));
                }
                c.getSession().write(InventoryPacket.getInventoryFull());
                return false;
            }

            int forceUpgrade = ii.getForceUpgrade(scroll.getItemId());
            if ((forceUpgrade > 1) && (toScroll.getEnhance() > 0))
            {
                if (chr.isAdmin())
                {
                    chr.dropMessage(-9, "砸卷错误: 强化卷轴检测 forceUpgrade: " + forceUpgrade + " 装备星级: " + toScroll.getEnhance());
                }
                c.getSession().write(InventoryPacket.getInventoryFull());
                return false;
            }
        }
        else if (ItemConstants.isPotentialScroll(scroll.getItemId()))
        {
            boolean isSpecialScrollA = scroll.getItemId() / 100 == 20497;
            boolean isSpecialEquip = (toScroll.getItemId() / 10000 == 135) || (toScroll.getItemId() / 1000 == 1098) || (toScroll.getItemId() / 1000 == 1099) || (toScroll.getItemId() / 1000 == 1190);
            if (((!isSpecialScrollA) && (toScroll.getState() >= 1)) || ((isSpecialScrollA) && (toScroll.getState() >= 18)) || ((toScroll.getLevel() == 0) && (toScroll.getUpgradeSlots() == 0) && (!isSpecialScrollA) && (!isSpecialEquip)) || (vegas > 0) || (ii.isCash(toScroll.getItemId())))
            {
                if (chr.isAdmin())
                {
                    chr.dropMessage(-9,
                            "砸卷错误: isPotentialScroll " + (toScroll.getState() >= 1) + " " + ((toScroll.getLevel() == 0) && (toScroll.getUpgradeSlots() == 0) && (!isSpecialScrollA) && (!isSpecialEquip)) + " " + (vegas > 0) + " " + ii.isCash(toScroll.getItemId()));
                }
                c.getSession().write(InventoryPacket.getInventoryFull());
                return false;
            }
        }
        else if (ItemConstants.isPotentialAddScroll(scroll.getItemId()))
        {
            boolean isA级潜能卷轴 = scroll.getItemId() / 100 == 20497;
            boolean is特殊装备 = (toScroll.getItemId() / 10000 == 135) || (toScroll.getItemId() / 1000 == 1098) || (toScroll.getItemId() / 1000 == 1099) || (toScroll.getItemId() / 1000 == 1190);
            if (((toScroll.getLevel() == 0) && (toScroll.getUpgradeSlots() == 0) && (!isA级潜能卷轴) && (!is特殊装备)) || (vegas > 0) || (ii.isCash(toScroll.getItemId())) || (toScroll.getAddState() > 0))
            {
                if (chr.isAdmin())
                {
                    chr.dropMessage(-9,
                            "砸卷错误: isPotentialAddScroll " + (toScroll.getState() >= 1) + " " + ((toScroll.getLevel() == 0) && (toScroll.getUpgradeSlots() == 0) && (!isA级潜能卷轴) && (!is特殊装备)) + " " + (vegas > 0) + " " + ii.isCash(toScroll.getItemId()));
                }
                c.getSession().write(InventoryPacket.getInventoryFull());
                return false;
            }
        }
        else if (ItemConstants.isSpecialScroll(scroll.getItemId()))
        {
            int maxEnhance = scroll.getItemId() == 5064003 ? 7 : 12;
            if ((ii.isCash(toScroll.getItemId())) || (toScroll.getEnhance() >= maxEnhance))
            {
                if (chr.isAdmin())
                {
                    chr.dropMessage(-9, "砸卷错误: 特殊卷轴 isCash - " + ii.isCash(toScroll.getItemId()) + " getEnhance - " + (toScroll.getEnhance() >= maxEnhance) + " 保护星级: " + maxEnhance);
                }
                c.getSession().write(InventoryPacket.getInventoryFull());
                return false;
            }
        }
        else if ((ItemConstants.isLimitBreakScroll(scroll.getItemId())) && ((!ItemConstants.isWeapon(toScroll.getItemId())) || (ii.getScrollLimitBreak(scroll.getItemId()) + oldLimitBreak > 5000000)))
        {
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }

        if ((!ItemConstants.canScroll(toScroll.getItemId())) && (!ItemConstants.isChaosScroll(toScroll.getItemId())))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: 卷轴是否能对装备进行砸卷 " + (!ItemConstants.canScroll(toScroll.getItemId())) + " 是否混沌卷轴 " + (!ItemConstants.isChaosScroll(toScroll.getItemId())));
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if (((ItemConstants.isCleanSlate(scroll.getItemId())) || (ItemConstants.isTablet(scroll.getItemId())) || (ItemConstants.isGeneralScroll(scroll.getItemId())) || (ItemConstants.isChaosScroll(scroll.getItemId()))) && ((vegas > 0) || (ii.isCash(toScroll.getItemId()))))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: 卷轴是否白衣卷轴 " + ItemConstants.isCleanSlate(scroll.getItemId()) + " isTablet " + ItemConstants.isTablet(scroll.getItemId()));
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if ((ItemConstants.isTablet(scroll.getItemId())) && (toScroll.getDurability() < 0))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: isTablet " + ItemConstants.isTablet(scroll.getItemId()) + " getDurability " + (toScroll.getDurability() < 0));
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if ((!ItemConstants.isTablet(scroll.getItemId())) && (!ItemConstants.isPotentialScroll(scroll.getItemId())) && (!ItemConstants.isEquipScroll(scroll.getItemId())) && (!ItemConstants.isCleanSlate(scroll.getItemId())) && (!ItemConstants.isSpecialScroll(scroll.getItemId())) && (!ItemConstants.isPotentialAddScroll(scroll.getItemId())) && (!ItemConstants.isChaosScroll(scroll.getItemId())) && (toScroll.getDurability() >= 0))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: !isTablet ----- 1");
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if ((scroll.getItemId() == 2049405) && (!ItemConstants.is真觉醒冒险之心(toScroll.getItemId())))
        {
            chr.dropMessage(1, "该卷轴只能对真·觉醒冒险之心使用。");
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        Item wscroll = null;

        List<Integer> scrollReqs = ii.getScrollReqs(scroll.getItemId());
        if ((scrollReqs != null) && (scrollReqs.size() > 0) && (!scrollReqs.contains(toScroll.getItemId())))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: 特定卷轴只能对指定的卷轴进行砸卷.");
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if (whiteScroll)
        {
            wscroll = chr.getInventory(MapleInventoryType.USE).findById(2340000);
            if (wscroll == null)
            {
                if (chr.isAdmin())
                {
                    chr.dropMessage(-9, "砸卷错误: 使用祝福卷轴 但祝福卷轴信息为空.");
                }
                c.getSession().write(InventoryPacket.getInventoryFull());
                whiteScroll = false;
            }
        }
        if ((ItemConstants.isTablet(scroll.getItemId())) || (ItemConstants.isGeneralScroll(scroll.getItemId())))
        {
            switch (scroll.getItemId() % 1000 / 100)
            {
                case 0:
                    if ((ItemConstants.isTwoHanded(toScroll.getItemId())) || (!ItemConstants.isWeapon(toScroll.getItemId())))
                    {
                        if (chr.isAdmin())
                        {
                            chr.dropMessage(-9, "砸卷错误: 最后检测 --- 0");
                        }
                        c.getSession().write(InventoryPacket.getInventoryFull());
                        return false;
                    }
                    break;
                case 1:
                    if ((!ItemConstants.isTwoHanded(toScroll.getItemId())) || (!ItemConstants.isWeapon(toScroll.getItemId())))
                    {
                        if (chr.isAdmin())
                        {
                            chr.dropMessage(-9, "砸卷错误: 最后检测 --- 1");
                        }
                        c.getSession().write(InventoryPacket.getInventoryFull());
                        return false;
                    }
                    break;
                case 2:
                    if ((ItemConstants.isAccessory(toScroll.getItemId())) || (ItemConstants.isWeapon(toScroll.getItemId())))
                    {
                        if (chr.isAdmin())
                        {
                            chr.dropMessage(-9, "砸卷错误: 最后检测 --- 2");
                        }
                        c.getSession().write(InventoryPacket.getInventoryFull());
                        return false;
                    }
                    break;
                case 3:
                    if ((!ItemConstants.isAccessory(toScroll.getItemId())) || (ItemConstants.isWeapon(toScroll.getItemId())))
                    {
                        if (chr.isAdmin())
                        {
                            chr.dropMessage(-9, "砸卷错误: 最后检测 --- 3");
                        }
                        c.getSession().write(InventoryPacket.getInventoryFull());
                        return false;
                    }
                    break;
            }
        }
        else if ((!ItemConstants.isAccessoryScroll(scroll.getItemId())) && (!ItemConstants.isChaosScroll(scroll.getItemId())) && (!ItemConstants.isCleanSlate(scroll.getItemId())) && (!ItemConstants.isEquipScroll(scroll.getItemId())) && (!ItemConstants.isPotentialScroll(scroll.getItemId())) && (!ItemConstants.isPotentialAddScroll(scroll.getItemId())) && (!ItemConstants.isSpecialScroll(scroll.getItemId())) && (!ItemConstants.isLimitBreakScroll(scroll.getItemId())) && (!ItemConstants.isResetScroll(scroll.getItemId())) && (!ii.canScroll(scroll.getItemId(), toScroll.getItemId())))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: 砸卷的卷轴无法对装备进行砸卷");
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }

        if ((ItemConstants.isAccessoryScroll(scroll.getItemId())) && (!ItemConstants.isAccessory(toScroll.getItemId())))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: 卷轴为配置卷轴 但砸卷的装备不是配饰");
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if (scroll.getQuantity() <= 0)
        {
            chr.dropSpouseMessage(11, "砸卷错误，背包卷轴[" + ii.getName(scroll.getItemId()) + "]数量为 0 .");
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }
        if ((legendarySpirit) && (vegas == 0) && (chr.getSkillLevel(SkillFactory.getSkill(PlayerStats.getSkillByJob(1003, chr.getJob()))) <= 0) && (ServerConfig.MAPLE_VERSION < 110))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(-9, "砸卷错误: 检测是否技能砸卷 角色没有拥有技能");
            }
            c.getSession().write(InventoryPacket.getInventoryFull());
            return false;
        }

        Equip scrolled = (Equip) ii.scrollEquipWithId(toScroll, scroll, whiteScroll, chr, vegas);
        Equip.ScrollResult scrollSuccess;
        if (scrolled == null)
        {
            if (ItemFlag.装备防爆.check(oldFlag))
            {
                scrolled = toScroll;
                scrollSuccess = Equip.ScrollResult.失败;
                scrolled.removeFlag((short) ItemFlag.装备防爆.getValue());
                chr.dropSpouseMessage(11, "由于卷轴的效果，物品没有损坏。");
            }
            else
            {
                scrollSuccess = Equip.ScrollResult.消失;
            }
        }
        else
        {
            if (((scroll.getItemId() / 100 == 20497) && (scrolled.getState() == 1)) || (scrolled.getLevel() > oldLevel) || (scrolled.getEnhance() > oldEnhance) || (scrolled.getState() > oldState) || (scrolled.getFlag() > oldFlag) || (scrolled.getAddState() > oldAddState) || (scrolled.getLimitBreak() > oldLimitBreak))
            {
                scrollSuccess = Equip.ScrollResult.成功;
            }
            else
            {
                if ((ItemConstants.isCleanSlate(scroll.getItemId())) && (scrolled.getUpgradeSlots() > oldSlots))
                {
                    scrollSuccess = Equip.ScrollResult.成功;
                }
                else
                {
                    if ((ItemConstants.isResetScroll(scroll.getItemId())) && (scrolled != toScroll))
                    {
                        scrollSuccess = Equip.ScrollResult.成功;
                    }
                    else scrollSuccess = Equip.ScrollResult.失败;
                }
            }
            if ((ItemFlag.装备防爆.check(oldFlag)) && (!ItemConstants.isCleanSlate(scroll.getItemId())) && (!ItemConstants.isSpecialScroll(scroll.getItemId())))
            {
                scrolled.removeFlag((short) ItemFlag.装备防爆.getValue());
            }

            if (chr.isIntern())
            {
                scrolled.addFlag((short) ItemFlag.CRAFTED.getValue());
                scrolled.setOwner(chr.getName());
            }
        }

        if (ItemFlag.卷轴防护.check(oldFlag))
        {
            if (scrolled != null)
            {
                scrolled.removeFlag((short) ItemFlag.卷轴防护.getValue());
            }
            if (scrollSuccess == Equip.ScrollResult.成功)
            {
                chr.getInventory(ItemConstants.getInventoryType(scroll.getItemId())).removeItem(scroll.getPosition(), (short) 1, false);
            }
            else
            {
                chr.dropSpouseMessage(11, "由于卷轴的效果，卷轴" + ii.getName(scroll.getItemId()) + "没有消失。");
            }
        }
        else
        {
            chr.getInventory(ItemConstants.getInventoryType(scroll.getItemId())).removeItem(scroll.getPosition(), (short) 1, false);
        }
        if (whiteScroll)
        {
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, wscroll.getPosition(), (short) 1, false, false);
        }
        else if ((scrollSuccess == Equip.ScrollResult.失败) && (scrolled.getUpgradeSlots() < oldSlots) && (chr.getInventory(MapleInventoryType.CASH).findById(5640000) != null))
        {
            chr.setScrolledPosition(scrolled.getPosition());
            if (vegas == 0)
            {
                c.getSession().write(MaplePacketCreator.pamSongUI());
            }
        }
        List<ModifyInventory> mods = new ArrayList<>();
        mods.add(new ModifyInventory(scroll.getQuantity() > 0 ? 1 : 3, scroll));
        if (scrollSuccess == Equip.ScrollResult.消失)
        {
            mods.add(new ModifyInventory(3, toScroll));
            if (dst < 0)
            {
                chr.getInventory(MapleInventoryType.EQUIPPED).removeItem(toScroll.getPosition());
            }
            else
            {
                chr.getInventory(MapleInventoryType.EQUIP).removeItem(toScroll.getPosition());
            }
        }
        else if (vegas == 0)
        {
            mods.add(new ModifyInventory(3, scrolled));
            mods.add(new ModifyInventory(0, scrolled));
        }
        c.getSession().write(InventoryPacket.modifyInventory(true, mods, chr));
        chr.getMap().broadcastMessage(chr, InventoryPacket.getScrollEffect(chr.getId(), scrollSuccess, legendarySpirit, whiteScroll, scroll.getItemId(), toScroll.getItemId()), vegas == 0);
        if ((dst < 0) && ((scrollSuccess == Equip.ScrollResult.成功) || (scrollSuccess == Equip.ScrollResult.消失)) && (vegas == 0))
        {
            chr.equipChanged();
        }
        return scrollSuccess == Equip.ScrollResult.成功;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\ItemScrollHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */