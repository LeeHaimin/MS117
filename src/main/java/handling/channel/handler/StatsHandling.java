package handling.channel.handler;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.MapleStat;
import client.PlayerStats;
import client.Skill;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;

public class StatsHandling
{
    public static void DistributeAP(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        Map<MapleStat, Long> statupdate = new EnumMap(MapleStat.class);
        c.getSession().write(MaplePacketCreator.updatePlayerStats(statupdate, true, chr));
        chr.updateTick(slea.readInt());

        PlayerStats stat = chr.getStat();
        int job = chr.getJob();
        int statLimit = c.getChannelServer().getStatLimit();
        if (chr.getRemainingAp() > 0)
        {
            switch (slea.readInt())
            {
                case 64:
                    if (stat.getStr() >= statLimit)
                    {
                        return;
                    }
                    stat.setStr((short) (stat.getStr() + 1), chr);
                    statupdate.put(MapleStat.力量, (long) stat.getStr());
                    break;
                case 128:
                    if (stat.getDex() >= statLimit)
                    {
                        return;
                    }
                    stat.setDex((short) (stat.getDex() + 1), chr);
                    statupdate.put(MapleStat.敏捷, (long) stat.getDex());
                    break;
                case 256:
                    if (stat.getInt() >= statLimit)
                    {
                        return;
                    }
                    stat.setInt((short) (stat.getInt() + 1), chr);
                    statupdate.put(MapleStat.智力, (long) stat.getInt());
                    break;
                case 512:
                    if (stat.getLuk() >= statLimit)
                    {
                        return;
                    }
                    stat.setLuk((short) (stat.getLuk() + 1), chr);
                    statupdate.put(MapleStat.运气, (long) stat.getLuk());
                    break;
                case 2048:
                    int maxhp = stat.getMaxHp();
                    if ((chr.getHpApUsed() >= 10000) || (maxhp >= chr.getMaxHpForSever()))
                    {
                        return;
                    }
                    if (constants.GameConstants.is新手职业(job))
                    {
                        maxhp += server.Randomizer.rand(8, 12);
                    }
                    else if (constants.GameConstants.is恶魔复仇者(job))
                    {
                        maxhp += 30;
                    }
                    else if (((job >= 100) && (job <= 132)) || ((job >= 3200) && (job <= 3212)) || ((job >= 1100) && (job <= 1112)) || ((job >= 3100) && (job <= 3112)) || ((job >= 5100) && (job <= 5112)))
                    {
                        maxhp += server.Randomizer.rand(36, 42);
                    }
                    else if (((job >= 200) && (job <= 232)) || (constants.GameConstants.is龙神(job)) || ((job >= 2700) && (job <= 2712)))
                    {
                        maxhp += server.Randomizer.rand(10, 20);
                    }
                    else if (((job >= 300) && (job <= 322)) || ((job >= 400) && (job <= 434)) || ((job >= 1300) && (job <= 1312)) || ((job >= 1400) && (job <= 1412)) || ((job >= 3300) && (job <= 3312)) || ((job >= 2300) && (job <= 2312)) || ((job >= 2400) && (job <= 2412)))
                    {
                        maxhp += server.Randomizer.rand(16, 20);
                    }
                    else if (((job >= 510) && (job <= 512)) || ((job >= 1510) && (job <= 1512)))
                    {
                        maxhp += server.Randomizer.rand(28, 32);
                    }
                    else if (((job >= 500) && (job <= 532)) || (constants.GameConstants.is龙的传人(job)) || ((job >= 3500) && (job <= 3512)) || (job == 1500))
                    {
                        maxhp += server.Randomizer.rand(18, 22);
                    }
                    else if ((job >= 1200) && (job <= 1212))
                    {
                        maxhp += server.Randomizer.rand(15, 21);
                    }
                    else if (((job >= 2000) && (job <= 2112)) || ((job >= 11200) && (job <= 11212)))
                    {
                        maxhp += server.Randomizer.rand(38, 42);
                    }
                    else if ((job >= 10100) && (job <= 10112))
                    {
                        maxhp += server.Randomizer.rand(48, 52);
                    }
                    else
                    {
                        maxhp += server.Randomizer.rand(18, 26);
                    }
                    maxhp = Math.min(chr.getMaxHpForSever(), Math.abs(maxhp));
                    chr.setHpApUsed((short) (chr.getHpApUsed() + 1));
                    stat.setMaxHp(maxhp, chr);
                    statupdate.put(MapleStat.MAXHP, (long) maxhp);
                    break;
                case 8192:
                    int maxmp = stat.getMaxMp();
                    if ((chr.getHpApUsed() >= 10000) || (stat.getMaxMp() >= chr.getMaxMpForSever()))
                    {
                        return;
                    }
                    if (constants.GameConstants.is新手职业(job))
                    {
                        maxmp += server.Randomizer.rand(6, 8);
                    }
                    else
                    {
                        if (constants.GameConstants.isNotMpJob(job)) return;
                        if (((job >= 200) && (job <= 232)) || (constants.GameConstants.is龙神(job)) || ((job >= 3200) && (job <= 3212)) || ((job >= 1200) && (job <= 1212)) || ((job >= 2700) && (job <= 2712)))
                        {
                            maxmp += server.Randomizer.rand(38, 40);
                        }
                        else if (((job >= 300) && (job <= 322)) || ((job >= 400) && (job <= 434)) || ((job >= 500) && (job <= 532)) || (constants.GameConstants.is龙的传人(job)) || ((job >= 3200) && (job <= 3212)) || ((job >= 3500) && (job <= 3512)) || ((job >= 1300) && (job <= 1312)) || ((job >= 1400) && (job <= 1412)) || ((job >= 1500) && (job <= 1512)) || ((job >= 2300) && (job <= 2312)) || ((job >= 2400) && (job <= 2412)))
                        {
                            maxmp += server.Randomizer.rand(10, 12);
                        }
                        else if (((job >= 100) && (job <= 132)) || ((job >= 1100) && (job <= 1112)) || ((job >= 2000) && (job <= 2112)) || ((job >= 5100) && (job <= 5112)))
                        {
                            maxmp += server.Randomizer.rand(6, 9);
                        }
                        else maxmp += server.Randomizer.rand(6, 12);
                    }
                    maxmp = Math.min(chr.getMaxMpForSever(), Math.abs(maxmp));
                    chr.setHpApUsed((short) (chr.getHpApUsed() + 1));
                    stat.setMaxMp(maxmp, chr);
                    statupdate.put(MapleStat.MAXMP, (long) maxmp);
                    break;
                default:
                    c.getSession().write(MaplePacketCreator.updatePlayerStats(new EnumMap(MapleStat.class), true, chr));
                    return;
            }
            chr.setRemainingAp((short) (chr.getRemainingAp() - 1));
            statupdate.put(MapleStat.AVAILABLEAP, (long) chr.getRemainingAp());
            c.getSession().write(MaplePacketCreator.updatePlayerStats(statupdate, true, chr));
        }
    }

    public static void DistributeSP(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        c.getPlayer().updateTick(slea.readInt());
        int skillid = slea.readInt();
        byte amount = slea.available() > 0L ? slea.readByte() : 1;
        if (chr.isAdmin())
        {
            chr.dropMessage(5, "开始加技能点 - 技能ID: " + skillid + " 等级: " + amount);
        }
        boolean isBeginnerSkill = false;
        int snailsLevel;
        int remainingSp;
        if ((constants.GameConstants.is新手职业(skillid / 10000)) && ((skillid % 10000 == 1000) || (skillid % 10000 == 1001) || (skillid % 10000 == 1002) || (skillid % 10000 == 2)))
        {
            boolean resistance = (skillid / 10000 == 3000) || (skillid / 10000 == 3001);
            snailsLevel = chr.getSkillLevel(client.SkillFactory.getSkill(skillid / 10000 * 10000 + 1000));
            int recoveryLevel = chr.getSkillLevel(client.SkillFactory.getSkill(skillid / 10000 * 10000 + 1001));
            int nimbleFeetLevel = chr.getSkillLevel(client.SkillFactory.getSkill(skillid / 10000 * 10000 + (resistance ? 2 : 1002)));
            remainingSp = Math.min(chr.getLevel() - 1, resistance ? 9 : 6) - snailsLevel - recoveryLevel - nimbleFeetLevel;
            isBeginnerSkill = true;
        }
        else
        {
            if (constants.GameConstants.is新手职业(skillid / 10000))
            {
                if (chr.isAdmin())
                {
                    chr.dropMessage(5, "加技能点错误 - 1");
                }
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            remainingSp = chr.getRemainingSp(constants.GameConstants.getSkillBookBySkill(skillid));
        }
        Skill skill = client.SkillFactory.getSkill(skillid);
        if (skill == null)
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(5, "加技能点错误 - 技能为空 当前技能ID: " + skillid);
            }
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        for (tools.Pair<String, Byte> ski : skill.getRequiredSkills())
        {
            if (ski.left.equalsIgnoreCase("level"))
            {
                if (chr.getLevel() < ski.right)
                {
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(5, "加技能点错误 - 技能要求等级: " + ski.right + " 当前角色等级: " + chr.getLevel());
                    }
                    c.getSession().write(MaplePacketCreator.enableActions());
                }
            }
            else if (ski.left.equalsIgnoreCase("reqAmount"))
            {
                int reqAmount = chr.getBeastTamerSkillLevels(skillid);
                if (reqAmount < ski.right)
                {
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(5, "加技能点错误 - 技能要求投入点数: " + ski.right + " 当前投入点数: " + reqAmount);
                    }
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
            }
            else
            {
                int left = Integer.parseInt(ski.left);
                if (chr.getSkillLevel(client.SkillFactory.getSkill(left)) < ski.right)
                {
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(5, "加技能点错误 - 前置技能: " + left + " - " + client.SkillFactory.getSkillName(left) + " 的技能等级不足.");
                    }
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
            }
        }
        int left;
        int maxlevel = skill.isFourthJob() ? chr.getMasterLevel(skill) : skill.getMaxLevel();
        int curLevel = chr.getSkillLevel(skill);

        if ((skill.isInvisible()) && (chr.getSkillLevel(skill) == 0) && (((skill.isFourthJob()) && (chr.getMasterLevel(skill) == 0)) || ((!skill.isFourthJob()) && (maxlevel < 10) && (!isBeginnerSkill) && (chr.getMasterLevel(skill) <= 0) && (!constants.GameConstants.is暗影双刀(chr.getJob())))))
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(5, "加技能点错误 - 3 检测 -> isFourthJob : " + skill.isFourthJob() + " getMasterLevel: " + chr.getMasterLevel(skill) + " 当前技能最大等级: " + maxlevel);
            }
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }

        for (int i : constants.GameConstants.blockedSkills)
        {
            if (skill.getId() == i)
            {
                chr.dropMessage(1, "这个技能未修复，暂时无法加点.");
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
        }
        if (chr.isAdmin())
        {
            chr.dropMessage(5, "开始加技能点 - 当前Sp: " + remainingSp + " 当前技能等级: " + curLevel + " 该技能最大等级: " + maxlevel + " 所加的等级: " + amount + " 是否为该职业技能: " + skill.canBeLearnedBy(chr.getJob()));
        }
        if ((remainingSp >= amount) && (curLevel + amount <= maxlevel) && (skill.canBeLearnedBy(chr.getJob())))
        {
            if (!isBeginnerSkill)
            {
                int skillbook = constants.GameConstants.getSkillBookBySkill(skillid);
                chr.setRemainingSp(chr.getRemainingSp(skillbook) - amount, skillbook);
            }
            chr.updateSingleStat(MapleStat.AVAILABLESP, 0L);
            chr.changeSingleSkillLevel(skill, (byte) (curLevel + amount), chr.getMasterLevel(skill));
        }
        else
        {
            if (chr.isAdmin())
            {
                chr.dropMessage(5, "加技能点错误 - SP点数不足够或者技能不是该角色的技能.");
            }
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void AutoAssignAP(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        chr.updateTick(slea.readInt());
        int autoSpSize = slea.readInt();
        if (slea.available() < autoSpSize * 12)
        {
            return;
        }
        int PrimaryStat = (int) slea.readLong();
        int amount = slea.readInt();
        int SecondaryStat = autoSpSize > 1 ? (int) slea.readLong() : 0;
        int amount2 = autoSpSize > 1 ? slea.readInt() : 0;
        if ((amount < 0) || (amount2 < 0))
        {
            return;
        }
        PlayerStats playerst = chr.getStat();
        boolean usedAp1 = true;
        boolean usedAp2 = true;
        Map<MapleStat, Long> statupdate = new EnumMap(MapleStat.class);
        c.getSession().write(MaplePacketCreator.updatePlayerStats(statupdate, true, chr));
        int statLimit = c.getChannelServer().getStatLimit();
        if (chr.getRemainingAp() >= amount + amount2)
        {
            switch (PrimaryStat)
            {
                case 64:
                    if (playerst.getStr() + amount > statLimit)
                    {
                        return;
                    }
                    playerst.setStr((short) (playerst.getStr() + amount), chr);
                    statupdate.put(MapleStat.力量, (long) playerst.getStr());
                    break;
                case 128:
                    if (playerst.getDex() + amount > statLimit)
                    {
                        return;
                    }
                    playerst.setDex((short) (playerst.getDex() + amount), chr);
                    statupdate.put(MapleStat.敏捷, (long) playerst.getDex());
                    break;
                case 256:
                    if (playerst.getInt() + amount > statLimit)
                    {
                        return;
                    }
                    playerst.setInt((short) (playerst.getInt() + amount), chr);
                    statupdate.put(MapleStat.智力, (long) playerst.getInt());
                    break;
                case 512:
                    if (playerst.getLuk() + amount > statLimit)
                    {
                        return;
                    }
                    playerst.setLuk((short) (playerst.getLuk() + amount), chr);
                    statupdate.put(MapleStat.运气, (long) playerst.getLuk());
                    break;
                case 2048:
                    int maxhp = playerst.getMaxHp();
                    if ((chr.getHpApUsed() >= 10000) || (maxhp >= chr.getMaxHpForSever()) || (!constants.GameConstants.is恶魔复仇者(chr.getJob())))
                    {
                        return;
                    }
                    maxhp += 30 * amount;
                    maxhp = Math.min(chr.getMaxHpForSever(), Math.abs(maxhp));
                    chr.setHpApUsed((short) (chr.getHpApUsed() + amount));
                    playerst.setMaxHp(maxhp, chr);
                    statupdate.put(MapleStat.MAXHP, (long) maxhp);
                    break;
                default:
                    usedAp1 = false;
            }

            switch (SecondaryStat)
            {
                case 64:
                    if (playerst.getStr() + amount2 > statLimit)
                    {
                        return;
                    }
                    playerst.setStr((short) (playerst.getStr() + amount2), chr);
                    statupdate.put(MapleStat.力量, (long) playerst.getStr());
                    break;
                case 128:
                    if (playerst.getDex() + amount2 > statLimit)
                    {
                        return;
                    }
                    playerst.setDex((short) (playerst.getDex() + amount2), chr);
                    statupdate.put(MapleStat.敏捷, (long) playerst.getDex());
                    break;
                case 256:
                    if (playerst.getInt() + amount2 > statLimit)
                    {
                        return;
                    }
                    playerst.setInt((short) (playerst.getInt() + amount2), chr);
                    statupdate.put(MapleStat.智力, (long) playerst.getInt());
                    break;
                case 512:
                    if (playerst.getLuk() + amount2 > statLimit)
                    {
                        return;
                    }
                    playerst.setLuk((short) (playerst.getLuk() + amount2), chr);
                    statupdate.put(MapleStat.运气, (long) playerst.getLuk());
                    break;
                default:
                    usedAp2 = false;
            }

            if (((!usedAp1) || (!usedAp2)) && (chr.isAdmin()))
            {
                chr.dropMessage(5, "自动分配能力点 - 主要: " + usedAp1 + " 次要: " + usedAp2);
            }
            chr.setRemainingAp((short) (chr.getRemainingAp() - ((usedAp1 ? amount : 0) + (usedAp2 ? amount2 : 0))));
            statupdate.put(MapleStat.AVAILABLEAP, (long) chr.getRemainingAp());
            c.getSession().write(MaplePacketCreator.updatePlayerStats(statupdate, true, chr));
        }
    }

    public static void DistributeHyperSP(int skillid, MapleClient c, MapleCharacter chr)
    {
        Skill skill = client.SkillFactory.getSkill(skillid);
        if ((skill == null) || (!skill.isHyperSkill()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        boolean isFixed = client.SkillFactory.isFixHyperSkill(skillid);
        if (chr.isAdmin())
        {
            chr.dropMessage(5, "开始加超级技能 - 技能ID: " + skillid + " - " + skill.getName() + " 是否修复: " + isFixed);
        }
        if (!isFixed)
        {
            chr.dropMessage(5, "对不起，您所加的技能 " + skill.getName() + " 还未修复，无法进行加点。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((chr.getLevel() >= skill.getReqLevel()) && (skill.canBeLearnedBy(chr.getJob())) && (chr.getSkillLevel(skill) == 0))
        {
            chr.changeSingleSkillLevel(skill, 1, (byte) skill.getMaxLevel());
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void ResetHyperSP(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        chr.updateTick(slea.readInt());
        int amount = slea.readShort();
        if (amount > 0)
        {
            Map<Skill, client.SkillEntry> oldList = new HashMap(chr.getSkills());
            Map<Skill, client.SkillEntry> newList = new HashMap<>();
            for (Map.Entry<Skill, client.SkillEntry> toRemove : oldList.entrySet())
            {
                if ((toRemove.getKey().isHyperSkill()) && (chr.getSkillLevel(toRemove.getKey()) == 1))
                {
                    if (toRemove.getKey().canBeLearnedBy(chr.getJob()))
                    {
                        newList.put(toRemove.getKey(), new client.SkillEntry(0, toRemove.getValue().masterlevel, toRemove.getValue().expiration));
                    }
                    else
                    {
                        newList.put(toRemove.getKey(), new client.SkillEntry(0, (byte) 0, -1L));
                    }
                }
            }
            if ((!newList.isEmpty()) && (chr.getMeso() >= amount * 1000000))
            {
                chr.gainMeso(-amount * 1000000, true, true);
                chr.changeSkillsLevel(newList);
                chr.dropMessage(1, "超级技能初始化完成\r\n本次消费金币: " + amount * 1000000);
            }
            else
            {
                chr.dropMessage(1, "超级技能初始化失败，您的金币不足。本次需要金币: " + amount * 1000000);
            }
            oldList.clear();
            newList.clear();
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\StatsHandling.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */