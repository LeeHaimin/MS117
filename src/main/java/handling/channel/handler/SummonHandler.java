package handling.channel.handler;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.PlayerStats;
import client.Skill;
import client.SkillFactory;
import client.SummonSkillEntry;
import server.MapleItemInformationProvider;
import server.MapleStatEffect;
import server.life.MapleMonster;
import server.maps.MapleMap;
import server.maps.MapleMapObject;
import server.maps.MapleMapObjectType;
import server.maps.MapleSummon;
import tools.AttackPair;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.SummonPacket;

public class SummonHandler
{
    public static void DragonFly(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.getDragon() == null))
        {
            return;
        }


        int type = slea.readInt();
        int mountId = type == 0 ? slea.readInt() : 0;
        chr.getMap().broadcastMessage(chr, MaplePacketCreator.showDragonFly(chr.getId(), type, mountId), chr.getTruePosition());
    }

    public static void MoveSummon(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        MapleMapObject obj = chr.getMap().getMapObject(slea.readInt(), MapleMapObjectType.SUMMON);
        if (obj == null)
        {
            return;
        }
        if ((obj instanceof server.maps.MapleDragon))
        {
            MoveDragon(slea, chr);
            return;
        }
        MapleSummon sum = (MapleSummon) obj;
        if ((sum.getOwnerId() != chr.getId()) || (sum.getSkillLevel() <= 0) || (sum.getMovementType() == server.maps.SummonMovementType.不会移动))
        {
            return;
        }
        slea.skip(4);
        slea.skip(4);
        slea.skip(4);
        List<server.movement.LifeMovementFragment> res = MovementParse.parseMovement(slea, 4);
        Point pos = sum.getPosition();
        MovementParse.updatePosition(res, sum, 0);
        if (res.size() > 0)
        {
            if (slea.available() != 8L)
            {
                System.out.println("slea.available() != 8 (召唤兽移动错误) 剩余封包长度: " + slea.available());
                tools.FileoutputUtil.log("log\\Movement.log", "slea.available() != 8 (召唤兽移动错误) 封包: " + slea.toString(true));
                return;
            }
            chr.getMap().broadcastMessage(chr, SummonPacket.moveSummon(chr.getId(), sum.getObjectId(), pos, res), sum.getTruePosition());
        }
    }

    public static void MoveDragon(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        slea.skip(4);
        slea.skip(4);
        slea.skip(4);
        List<server.movement.LifeMovementFragment> res = MovementParse.parseMovement(slea, 5);
        if ((chr != null) && (chr.getDragon() != null) && (res.size() > 0))
        {
            if (slea.available() != 8L)
            {
                System.out.println("slea.available() != 8 (龙龙移动错误) 剩余封包长度: " + slea.available());
                tools.FileoutputUtil.log("log\\Movement.log", "slea.available() != 8 (龙龙移动错误) 封包: " + slea.toString(true));
                return;
            }
            Point pos = chr.getDragon().getPosition();
            MovementParse.updatePosition(res, chr.getDragon(), 0);
            if (!chr.isHidden())
            {
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.moveDragon(chr.getDragon(), pos, res), chr.getTruePosition());
            }
        }
    }

    public static void DamageSummon(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        if ((chr == null) || (!chr.isAlive()) || (chr.getMap() == null))
        {
            return;
        }
        int sumoid = slea.readInt();
        MapleSummon summon = chr.getMap().getSummonByOid(sumoid);
        if ((summon == null) || (summon.getOwnerId() != chr.getId()))
        {
            return;
        }
        int type = slea.readByte();
        int damage = slea.readInt();
        int monsterIdFrom = slea.readInt();
        slea.skip(1);
        int moboid = slea.readInt();
        MapleMonster monster = chr.getMap().getMonsterByOid(moboid);
        if (monster == null)
        {
            return;
        }
        boolean remove = false;
        if ((summon.is替身术()) && (damage > 0))
        {
            summon.addSummonHp(-damage);
            if (summon.getSummonHp() <= 0)
            {
                remove = true;
            }
            else if (summon.is神箭幻影())
            {
                List<Pair<Integer, Boolean>> allDamageNumbers = new ArrayList<>();
                List<AttackPair> allDamage = new ArrayList<>();
                int theDmg = (int) (SkillFactory.getSkill(summon.getSkillId()).getEffect(summon.getSkillLevel()).getY() * damage / 100.0D);
                allDamageNumbers.add(new Pair(theDmg, Boolean.FALSE));
                allDamage.add(new AttackPair(monster.getObjectId(), allDamageNumbers));
                chr.getMap().broadcastMessage(SummonPacket.summonAttack(summon.getOwnerId(), summon.getObjectId(), (byte) -124, (byte) 17, allDamage, chr.getLevel(), true));
                monster.damage(chr, theDmg, true);
                chr.checkMonsterAggro(monster);
                if (!monster.isAlive())
                {
                    chr.getClient().getSession().write(tools.packet.MobPacket.killMonster(monster.getObjectId(), 1));
                }
            }
            chr.getMap().broadcastMessage(chr, SummonPacket.damageSummon(chr.getId(), summon.getSkillId(), damage, type, monsterIdFrom), summon.getTruePosition());
        }
        if (remove)
        {
            chr.dispelSkill(summon.getSkillId());
        }
    }

    public static void SummonAttack(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (!chr.isAlive()) || (chr.getMap() == null))
        {
            return;
        }
        MapleMap map = chr.getMap();
        MapleMapObject obj = map.getMapObject(slea.readInt(), MapleMapObjectType.SUMMON);
        if ((!(obj instanceof MapleSummon)))
        {
            chr.dropMessage(5, "召唤兽已经消失。");
            return;
        }
        MapleSummon summon = (MapleSummon) obj;
        if ((summon.getOwnerId() != chr.getId()) || (summon.getSkillLevel() <= 0))
        {
            chr.dropMessage(5, "出现错误.");
            return;
        }
        SummonSkillEntry sse = SkillFactory.getSummonData(summon.getSkillId());
        if ((summon.getSkillId() / 1000000 != 35) && (summon.getSkillId() != 33101008) && (sse == null))
        {
            chr.dropMessage(5, "召唤兽攻击处理出错。");
            return;
        }
        int tick = slea.readInt();
        if ((sse != null) && (sse.delay > 0) && (!is不检测技能(summon.getSkillId())))
        {
            chr.updateTick(tick);
            summon.CheckSummonAttackFrequency(chr, tick);
            chr.getCheatTracker().checkSummonAttack();
        }
        byte animation = slea.readByte();
        byte numAttackedAndDamage = slea.readByte();
        byte numAttacked = (byte) (numAttackedAndDamage >>> 4 & 0xF);
        byte numDamage = (byte) (numAttackedAndDamage & 0xF);
        if (sse != null)
        {
            if (numAttacked > sse.mobCount)
            {
                if (chr.isShowPacket())
                {
                    chr.dropMessage(-5, "召唤兽攻击次数错误 (Skillid : " + summon.getSkillId() + " 怪物数量 : " + numAttacked + " 默认数量: " + sse.mobCount + ")");
                }
                chr.dropMessage(5, "[警告] 请不要使用非法程序。召唤兽攻击怪物数量错误.");
                chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.SUMMON_HACK_MOBS);
                return;
            }
            int numAttackCount = chr.getStat().getAttackCount(summon.getSkillId()) + sse.attackCount;
            if (numDamage > numAttackCount)
            {
                if (chr.isShowPacket())
                {
                    chr.dropMessage(-5,
                            "召唤兽攻击次数错误 (Skillid : " + summon.getSkillId() + " 打怪次数 : " + numDamage + " 默认次数: " + sse.attackCount + " 超级技能增加次数: " + chr.getStat().getAttackCount(summon.getSkillId()) + ")");
                }
                chr.dropMessage(5, "[警告] 请不要使用非法程序。召唤兽攻击怪物次数错误.");
                chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.SUMMON_HACK_MOBS);
                return;
            }
        }
        slea.skip(summon.getSkillId() == 35111002 ? 24 : 12);

        List<AttackPair> allDamage = new ArrayList<>();
        int j;
        for (int i = 0; i < numAttacked; i++)
        {
            MapleMonster mob = map.getMonsterByOid(slea.readInt());
            if (mob != null)
            {

                slea.skip(4);
                slea.skip(20);
                List<Pair<Integer, Boolean>> allDamageNumbers = new ArrayList<>();
                for (j = 0; j < numDamage; j++)
                {
                    int damge = slea.readInt();
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(-5, "召唤兽攻击 打怪数量: " + numAttacked + " 打怪次数: " + numDamage + " 打怪伤害: " + damge + " 怪物ID: " + mob.getObjectId());
                    }
                    if ((damge > chr.getMaxDamageOver(0)) && (!chr.isGM()))
                    {
                        handling.world.WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                                "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") 召唤兽攻击伤害异常。打怪伤害: " + damge + " 地图ID: " + chr.getMapId()));
                    }
                    allDamageNumbers.add(new Pair(damge, Boolean.FALSE));
                }
                slea.skip(4);
                slea.skip(4);
                allDamage.add(new AttackPair(Integer.valueOf(mob.getObjectId()), allDamageNumbers));
            }
        }
        Skill summonSkill = SkillFactory.getSkill(summon.getSkillId());
        MapleStatEffect summonEffect = summonSkill.getEffect(summon.getSkillLevel());
        if (summonEffect == null)
        {
            chr.dropMessage(5, "召唤兽攻击出现错误 => 攻击效果为空.");
            return;
        }

        if (allDamage.isEmpty())
        {
            return;
        }
        map.broadcastMessage(chr, SummonPacket.summonAttack(summon.getOwnerId(), summon.getObjectId(), animation, numAttackedAndDamage, allDamage, chr.getLevel(), false), summon.getTruePosition());
        for (AttackPair attackEntry : allDamage)
        {
            MapleMonster targetMob = map.getMonsterByOid(attackEntry.objectid);
            if (targetMob != null)
            {

                int totDamageToOneMonster = 0;
                for (Pair<Integer, Boolean> eachde : attackEntry.attack)
                {
                    int toDamage = eachde.left;
                    if ((chr.isGM()) || (toDamage < chr.getStat().getCurrentMaxBaseDamage() * 5.0D * (summonEffect.getSelfDestruction() + summonEffect.getDamage() + chr.getStat().getDamageIncrease(summonEffect.getSourceId())) / 100.0D))
                    {
                        totDamageToOneMonster += toDamage;
                    }
                    else
                    {
                        totDamageToOneMonster += toDamage;
                    }
                }
                if ((sse != null) && (sse.delay > 0) && (summon.getMovementType() != server.maps.SummonMovementType.不会移动) && (summon.getMovementType() != server.maps.SummonMovementType.CIRCLE_STATIONARY) && (summon.getMovementType() != server.maps.SummonMovementType.自由移动) && (chr.getTruePosition().distanceSq(targetMob.getTruePosition()) > 400000.0D) && (!chr.getMap().isBossMap()))
                {
                    chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.ATTACK_FARAWAY_MONSTER_SUMMON);
                }

                if (totDamageToOneMonster > 0)
                {
                    if ((summonEffect.getMonsterStati().size() > 0) && (summonEffect.makeChanceResult()))
                    {
                        for (Map.Entry<client.status.MonsterStatus, Integer> z : summonEffect.getMonsterStati().entrySet())
                        {
                            targetMob.applyStatus(chr, new client.status.MonsterStatusEffect(z.getKey(), z.getValue(), summonSkill.getId(), null, false), summonEffect.isPoison(), 4000L, true,
                                    summonEffect);
                        }
                    }

                    if (chr.isShowPacket())
                    {
                        chr.dropMessage(5, "召唤兽打怪最终伤害 : " + totDamageToOneMonster);
                    }
                    targetMob.damage(chr, totDamageToOneMonster, true);
                    chr.checkMonsterAggro(targetMob);
                    if (!targetMob.isAlive())
                    {
                        chr.getClient().getSession().write(tools.packet.MobPacket.killMonster(targetMob.getObjectId(), 1));
                    }
                }
            }
        }
        if (!summon.isMultiAttack())
        {
            chr.getMap().broadcastMessage(SummonPacket.removeSummon(summon, true));
            chr.getMap().removeMapObject(summon);
            chr.removeVisibleMapObject(summon);
            chr.removeSummon(summon);
            if (summon.getSkillId() != 35121011)
            {
                chr.dispelSkill(summon.getSkillId());
            }
        }
    }

    public static boolean is不检测技能(int skillId)
    {
        switch (skillId)
        {
            case 5711001:
            case 23111008:
            case 23111009:
            case 23111010:
                return true;
        }
        return false;
    }

    public static void RemoveSummon(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MapleMapObject obj = c.getPlayer().getMap().getMapObject(slea.readInt(), MapleMapObjectType.SUMMON);
        if ((!(obj instanceof MapleSummon)))
        {
            return;
        }
        MapleSummon summon = (MapleSummon) obj;
        if ((summon.getOwnerId() != c.getPlayer().getId()) || (summon.getSkillLevel() <= 0))
        {
            c.getPlayer().dropMessage(5, "移除召唤兽出现错误.");
            return;
        }
        if (c.getPlayer().isShowPacket())
        {
            c.getPlayer().dropSpouseMessage(10, "收到移除召唤兽信息 - 召唤兽技能ID: " + summon.getSkillId() + " 技能名字 " + SkillFactory.getSkillName(summon.getSkillId()));
        }
        if ((summon.getSkillId() == 35111002) || (summon.getSkillId() == 35121010))
        {
            return;
        }
        c.getPlayer().getMap().broadcastMessage(SummonPacket.removeSummon(summon, false));
        c.getPlayer().getMap().removeMapObject(summon);
        c.getPlayer().removeVisibleMapObject(summon);
        c.getPlayer().removeSummon(summon);
        if (summon.getSkillId() != 35121011)
        {
            c.getPlayer().dispelSkill(summon.getSkillId());
            if (summon.is天使召唤兽())
            {
                int buffId = summon.getSkillId() % 10000 == 1179 ? 2022823 : summon.getSkillId() % 10000 == 1087 ? 2022747 : 2022746;
                c.getPlayer().dispelBuff(buffId);
            }
        }
    }

    public static void SubSummon(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        MapleMapObject obj = chr.getMap().getMapObject(slea.readInt(), MapleMapObjectType.SUMMON);
        if ((!(obj instanceof MapleSummon)))
        {
            return;
        }
        MapleSummon sum = (MapleSummon) obj;
        if ((sum.getOwnerId() != chr.getId()) || (sum.getSkillLevel() <= 0) || (!chr.isAlive()))
        {
            return;
        }
        switch (sum.getSkillId())
        {
            case 35121009:
                if (!chr.canSummon(2000))
                {
                    return;
                }
                for (int i = 0; i < 3; i++)
                {
                    MapleSummon tosummon = new MapleSummon(chr, SkillFactory.getSkill(35121011).getEffect(sum.getSkillLevel()), new Point(sum.getTruePosition().x, sum.getTruePosition().y - 5),
                            server.maps.SummonMovementType.自由移动);
                    chr.getMap().spawnSummon(tosummon);
                    chr.addSummon(tosummon);
                }
                break;
            case 35111011:
                if (!chr.canSummon(1000))
                {
                    return;
                }
                chr.addHP((int) (chr.getStat().getCurrentMaxHp() * SkillFactory.getSkill(sum.getSkillId()).getEffect(sum.getSkillLevel()).getHp() / 100.0D));
                chr.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(sum.getSkillId(), 2, chr.getLevel(), sum.getSkillLevel()));
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.showBuffeffect(chr.getId(), sum.getSkillId(), 2, chr.getLevel(), sum.getSkillLevel()), false);
                break;
            case 1301013:
                Skill bHealing = SkillFactory.getSkill(slea.readInt());
                int bHealingLvl = chr.getTotalSkillLevel(bHealing);
                if ((bHealingLvl <= 0) || (bHealing == null))
                {
                    return;
                }
                MapleStatEffect healEffect = bHealing.getEffect(bHealingLvl);
                if (bHealing.getId() == 1310016)
                {
                    healEffect.applyTo(chr);
                }
                else if (bHealing.getId() == 1301013)
                {
                    if (!chr.canSummon(healEffect.getX() * 1000))
                    {
                        return;
                    }
                    int healHp = Math.min(1000, healEffect.getHp() * chr.getLevel());
                    chr.addHP(healHp);
                }
                chr.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(sum.getSkillId(), 2, chr.getLevel(), bHealingLvl));
                chr.getMap().broadcastMessage(SummonPacket.summonSkill(chr.getId(), sum.getSkillId(), bHealing.getId() == 1301013 ? 5 : server.Randomizer.nextInt(3) + 6));
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.showBuffeffect(chr.getId(), sum.getSkillId(), 2, chr.getLevel(), bHealingLvl), false);
        }

        if (constants.GameConstants.is天使技能(sum.getSkillId()))
        {
            if (sum.getSkillId() % 10000 == 1087)
            {
                MapleItemInformationProvider.getInstance().getItemEffect(2022747).applyTo(chr);
            }
            else if (sum.getSkillId() % 10000 == 1179)
            {
                MapleItemInformationProvider.getInstance().getItemEffect(2022823).applyTo(chr);
            }
            else
            {
                MapleItemInformationProvider.getInstance().getItemEffect(2022746).applyTo(chr);
            }
            chr.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(sum.getSkillId(), 2, 2, 1));
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.showBuffeffect(chr.getId(), sum.getSkillId(), 2, 2, 1), false);
        }
    }

    public static void SummonPVP(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MapleCharacter chr = c.getPlayer();
        if ((chr == null) || (chr.isHidden()) || (!chr.isAlive()) || (chr.hasBlockedInventory()) || (chr.getMap() == null) || (!chr.inPVP()) || (!chr.getEventInstance().getProperty("started").equals("1")))
        {
            return;
        }
        MapleMap map = chr.getMap();
        MapleMapObject obj = map.getMapObject(slea.readInt(), MapleMapObjectType.SUMMON);
        if ((!(obj instanceof MapleSummon)))
        {
            chr.dropMessage(5, "召唤兽已经消失。");
            return;
        }
        int tick = -1;
        if (slea.available() == 27L)
        {
            slea.skip(23);
            tick = slea.readInt();
        }
        MapleSummon summon = (MapleSummon) obj;
        if ((summon.getOwnerId() != chr.getId()) || (summon.getSkillLevel() <= 0))
        {
            chr.dropMessage(5, "出现错误");
            return;
        }
        Skill skil = SkillFactory.getSkill(summon.getSkillId());
        MapleStatEffect effect = skil.getEffect(summon.getSkillLevel());
        int lvl = Integer.parseInt(chr.getEventInstance().getProperty("lvl"));
        int type = Integer.parseInt(chr.getEventInstance().getProperty("type"));
        int ourScore = Integer.parseInt(chr.getEventInstance().getProperty(String.valueOf(chr.getId())));
        int addedScore = 0;
        boolean magic = skil.isMagic();
        boolean killed = false;
        boolean didAttack = false;
        double maxdamage = lvl == 3 ? chr.getStat().getCurrentMaxBasePVPDamageL() : chr.getStat().getCurrentMaxBasePVPDamage();
        maxdamage *= (effect.getDamage() + chr.getStat().getDamageIncrease(summon.getSkillId())) / 100.0D;
        int mobCount = 1;
        int attackCount = 1;
        int ignoreDEF = chr.getStat().percent_ignore_mob_def_rate;

        SummonSkillEntry sse = SkillFactory.getSummonData(summon.getSkillId());
        if ((summon.getSkillId() / 1000000 != 35) && (summon.getSkillId() != 33101008) && (sse == null))
        {
            chr.dropMessage(5, "召唤兽攻击处理出错。");
            return;
        }
        Point rb;
        Point lt;
        if (sse != null)
        {
            if (sse.delay > 0)
            {
                if (tick != -1)
                {
                    summon.CheckSummonAttackFrequency(chr, tick);
                    chr.updateTick(tick);
                }
                else
                {
                    summon.CheckPVPSummonAttackFrequency(chr);
                }
                chr.getCheatTracker().checkSummonAttack();
            }
            mobCount = sse.mobCount;
            attackCount = sse.attackCount;
            lt = sse.lt;
            rb = sse.rb;
        }
        else
        {
            lt = new Point(-100, -100);
            rb = new Point(100, 100);
        }
        java.awt.Rectangle box = MapleStatEffect.calculateBoundingBox(chr.getTruePosition(), chr.isFacingLeft(), lt, rb, 0);
        List<AttackPair> ourAttacks = new ArrayList<>();

        maxdamage *= chr.getStat().getDamageRate() / 100.0D;
        for (MapleMapObject mo : chr.getMap().getCharactersIntersect(box))
        {
            MapleCharacter attacked = (MapleCharacter) mo;
            if ((attacked.getId() != chr.getId()) && (attacked.isAlive()) && (!attacked.isHidden()) && ((type == 0) || (attacked.getTeam() != chr.getTeam())))
            {
                double rawDamage = maxdamage / Math.max(0.0D, (magic ? attacked.getStat().mdef : attacked.getStat().wdef) * Math.max(1.0D, 100.0D - ignoreDEF) / 100.0D * (type == 3 ? 0.1D : 0.25D));
                if ((attacked.getBuffedValue(client.MapleBuffStat.INVINCIBILITY) != null) || (PlayersHandler.inArea(attacked)))
                {
                    rawDamage = 0.0D;
                }
                rawDamage += rawDamage * chr.getDamageIncrease(attacked.getId()) / 100.0D;
                rawDamage *= attacked.getStat().mesoGuard / 100.0D;
                rawDamage = attacked.modifyDamageTaken(rawDamage, attacked).left;
                double min = rawDamage * chr.getStat().trueMastery / 100.0D;
                List<Pair<Integer, Boolean>> attacks = new ArrayList(attackCount);
                int totalMPLoss = 0;
                int totalHPLoss = 0;
                for (int i = 0; i < attackCount; i++)
                {
                    int mploss = 0;
                    double ourDamage = server.Randomizer.nextInt((int) Math.abs(Math.round(rawDamage - min)) + 1) + min;
                    if ((attacked.getStat().dodgeChance > 0) && (server.Randomizer.nextInt(100) < attacked.getStat().dodgeChance))
                    {
                        ourDamage = 0.0D;
                    }


                    if (attacked.getBuffedValue(client.MapleBuffStat.魔法盾) != null)
                    {
                        mploss = (int) Math.min(attacked.getStat().getMp(), ourDamage * attacked.getBuffedValue(client.MapleBuffStat.魔法盾).doubleValue() / 100.0D);
                    }
                    ourDamage -= mploss;
                    if (attacked.getBuffedValue(client.MapleBuffStat.终极无限) != null)
                    {
                        mploss = 0;
                    }
                    attacks.add(new Pair((int) Math.floor(ourDamage), Boolean.FALSE));

                    totalHPLoss = (int) (totalHPLoss + Math.floor(ourDamage));
                    totalMPLoss += mploss;
                }
                attacked.addMPHP(-totalHPLoss, -totalMPLoss);
                ourAttacks.add(new AttackPair(attacked.getId(), attacked.getPosition(), attacks));
                attacked.getCheatTracker().setAttacksWithoutHit(false);
                if (totalHPLoss > 0)
                {
                    didAttack = true;
                }
                if (attacked.getStat().getHPPercent() <= 20)
                {
                    SkillFactory.getSkill(PlayerStats.getSkillByJob(93, attacked.getJob())).getEffect(1).applyTo(attacked);
                }
                if (effect != null)
                {
                    if ((effect.getMonsterStati().size() > 0) && (effect.makeChanceResult()))
                    {
                        for (Map.Entry<client.status.MonsterStatus, Integer> z : effect.getMonsterStati().entrySet())
                        {
                            client.MapleDisease d = client.status.MonsterStatus.getLinkedDisease(z.getKey());
                            if (d != null)
                            {
                                attacked.giveDebuff(d, z.getValue(), effect.getDuration(), d.getDisease(), 1);
                            }
                        }
                    }
                    effect.handleExtraPVP(chr, attacked);
                }
                chr.getClient().getSession().write(MaplePacketCreator.getPVPHPBar(attacked.getId(), attacked.getStat().getHp(), attacked.getStat().getCurrentMaxHp()));
                addedScore += totalHPLoss / 100 + totalMPLoss / 100;
                if (!attacked.isAlive())
                {
                    killed = true;
                }

                if (ourAttacks.size() >= mobCount)
                {
                    break;
                }
            }
        }
        if ((killed) || (addedScore > 0))
        {
            chr.getEventInstance().addPVPScore(chr, addedScore);
            chr.getClient().getSession().write(MaplePacketCreator.getPVPScore(ourScore + addedScore, killed));
        }
        if (didAttack)
        {
            chr.getMap().broadcastMessage(SummonPacket.pvpSummonAttack(chr.getId(), chr.getLevel(), summon.getObjectId(), summon.isFacingLeft() ? 4 : 132, summon.getTruePosition(), ourAttacks));
            if (!summon.isMultiAttack())
            {
                chr.getMap().broadcastMessage(SummonPacket.removeSummon(summon, true));
                chr.getMap().removeMapObject(summon);
                chr.removeVisibleMapObject(summon);
                chr.removeSummon(summon);
                if (summon.getSkillId() != 35121011)
                {
                    chr.dispelSkill(summon.getSkillId());
                }
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\SummonHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */