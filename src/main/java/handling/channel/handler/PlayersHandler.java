package handling.channel.handler;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleClient;
import client.MapleDisease;
import client.MapleLieDetector;
import client.PlayerStats;
import client.Skill;
import client.SkillFactory;
import client.anticheat.CheatTracker;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.status.MonsterStatus;
import constants.GameConstants;
import scripting.event.EventInstanceManager;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.MapleStatEffect;
import server.Randomizer;
import server.events.MapleCoconut;
import server.maps.MapleMap;
import server.maps.MapleMist;
import server.maps.MapleReactor;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.Triple;
import tools.data.input.SeekableLittleEndianAccessor;

public class PlayersHandler
{
    public static void Note(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        byte type = slea.readByte();
        switch (type)
        {
            case 0:
                String name = slea.readMapleAsciiString();
                String msg = slea.readMapleAsciiString();
                boolean fame = slea.readByte() > 0;
                slea.readInt();
                Item itemz = chr.getCashInventory().findByCashId((int) slea.readLong());
                if ((itemz == null) || (!itemz.getGiftFrom().equalsIgnoreCase(name)) || (!chr.getCashInventory().canSendNote(itemz.getUniqueId())))
                {
                    return;
                }
                try
                {
                    chr.sendNote(name, msg, fame ? 1 : 0);
                    chr.getCashInventory().sendedNote(itemz.getUniqueId());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            case 1:
                int num = slea.readShort();
                slea.readByte();
                for (int i = 0; i < num; i++)
                {
                    int id = slea.readInt();
                    int giveFame = slea.readByte();
                    chr.deleteNote(id, giveFame);
                }
                break;
            default:
                System.out.println("Unhandled note action, " + type + "");
        }
    }

    public static void GiveFame(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        int who = slea.readInt();
        int mode = slea.readByte();
        int famechange = mode == 0 ? -1 : 1;
        MapleCharacter target = chr.getMap().getCharacterById(who);
        if ((target == null) || (target == chr))
        {
            chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.FAMING_SELF, "不能对自身操作.");
            return;
        }
        if (chr.getLevel() < 15)
        {
            chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.FAMING_UNDER_15, "等级小于15级.");
            return;
        }
        switch (chr.canGiveFame(target))
        {
            case OK:
                if (Math.abs(target.getFame() + famechange) <= 99999)
                {
                    target.addFame(famechange);
                    target.updateSingleStat(client.MapleStat.人气, target.getFame());
                }
                if (!chr.isGM())
                {
                    chr.hasGivenFame(target);
                }
                c.getSession().write(MaplePacketCreator.giveFameResponse(mode, target.getName(), target.getFame()));
                target.getClient().getSession().write(MaplePacketCreator.receiveFame(mode, chr.getName()));
                break;
            case NOT_TODAY:
                c.getSession().write(MaplePacketCreator.giveFameErrorResponse(3));
                break;
            case NOT_THIS_MONTH:
                c.getSession().write(MaplePacketCreator.giveFameErrorResponse(4));
        }
    }

    public static void UseDoor(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        int oid = slea.readInt();
        boolean mode = slea.readByte() == 0;
        for (server.maps.MapleMapObject obj : chr.getMap().getAllDoorsThreadsafe())
        {
            server.maps.MapleDoor door = (server.maps.MapleDoor) obj;
            if (door.getOwnerId() == oid)
            {
                door.warp(chr, mode);
                break;
            }
        }
    }

    public static void UseMechDoor(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        int oid = slea.readInt();
        Point pos = slea.readPos();
        int mode = slea.readByte();
        chr.getClient().getSession().write(MaplePacketCreator.enableActions());
        for (server.maps.MapleMapObject obj : chr.getMap().getAllMechDoorsThreadsafe())
        {
            server.maps.MechDoor door = (server.maps.MechDoor) obj;
            if ((door.getOwnerId() == oid) && (door.getId() == mode))
            {
                chr.checkFollow();
                chr.getMap().movePlayer(chr, pos);
                break;
            }
        }
    }


    public static void UseHolyFountain(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        int mode = slea.readByte();
        int oid = slea.readInt();
        int skillId = slea.readInt();
        Point position = slea.readPos();
        MapleMist mist = c.getPlayer().getMap().getMistByOid(oid);
        if ((mist == null) || (!mist.isHolyFountain()))
        {
            return;
        }
        if ((mist.getHealCount() > 0) && (mist.getBox().contains(position)))
        {
            MapleCharacter owner = chr.getMap().getCharacterById(mist.getOwnerId());
            if ((mist.getOwnerId() == chr.getId()) || ((owner != null) && (owner.getParty() != null) && (chr.getParty() != null) && (owner.getParty().getId() == chr.getParty().getId())))
            {
                int healHp = (int) (chr.getStat().getCurrentMaxHp() * (mist.getSource().getX() / 100.0D));
                chr.addHP(healHp);
                mist.setHealCount(mist.getHealCount() - 1);
                if (chr.isAdmin())
                {
                    chr.dropMessage(5, "使用神圣源泉 - 恢复血量: " + healHp + " 百分比: " + mist.getSource().getX() / 100.0D + " 剩余次数: " + mist.getHealCount());
                }
                c.getSession().write(MaplePacketCreator.showHolyFountain(skillId));
            }
        }
        else if (chr.isAdmin())
        {
            chr.dropMessage(5, "使用神圣源泉出现错误 - 源泉恢复的剩余次数: " + mist.getHealCount() + " 模式: " + mode + " 是否在范围内: " + mist.getBox().contains(position));
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void TransformPlayer(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        chr.updateTick(slea.readInt());
        byte slot = (byte) slea.readShort();
        int itemId = slea.readInt();
        String target = slea.readMapleAsciiString();
        Item toUse = c.getPlayer().getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        switch (itemId)
        {
            case 2212000:
                MapleCharacter search_chr = chr.getMap().getCharacterByName(target);
                if (search_chr != null)
                {
                    MapleItemInformationProvider.getInstance().getItemEffect(2210023).applyTo(search_chr);
                    search_chr.dropMessage(6, chr.getName() + " has played a prank on you!");
                    MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
                }
                else
                {
                    chr.dropMessage(1, "在当前地图中未找到 '" + target + "' 的玩家.");
                }
                break;
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void HitReactor(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int oid = slea.readInt();
        int charPos = slea.readInt();
        short stance = slea.readShort();
        MapleReactor reactor = c.getPlayer().getMap().getReactorByOid(oid);
        if ((reactor == null) || (!reactor.isAlive()))
        {
            return;
        }
        reactor.hitReactor(charPos, stance, c);
    }

    public static void TouchReactor(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int oid = slea.readInt();
        boolean touched = (slea.available() == 0L) || (slea.readByte() > 0);
        MapleReactor reactor = c.getPlayer().getMap().getReactorByOid(oid);
        if ((!touched) || (reactor == null) || (!reactor.isAlive()) || (reactor.getTouch() == 0))
        {
            return;
        }
        if (c.getPlayer().isAdmin())
        {
            c.getPlayer().dropMessage(5, "反应堆信息 - oid: " + oid + " Touch: " + reactor.getTouch() + " isTimerActive: " + reactor.isTimerActive() + " ReactorType: " + reactor.getReactorType());
        }
        if (reactor.getTouch() == 2)
        {
            scripting.reactor.ReactorScriptManager.getInstance().act(c, reactor);
        }
        else if ((reactor.getTouch() == 1) && (!reactor.isTimerActive()))
        {
            if (reactor.getReactorType() == 100)
            {
                int itemid = GameConstants.getCustomReactItem(reactor.getReactorId(), reactor.getReactItem().getLeft());
                if (c.getPlayer().haveItem(itemid, reactor.getReactItem().getRight()))
                {
                    if (reactor.getArea().contains(c.getPlayer().getTruePosition()))
                    {
                        MapleInventoryManipulator.removeById(c, constants.ItemConstants.getInventoryType(itemid), itemid, reactor.getReactItem().getRight(), true, false);
                        reactor.hitReactor(c);
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "距离太远。请靠近后重新尝试。");
                    }
                }
                else
                {
                    c.getPlayer().dropMessage(5, "You don't have the item required.");
                }
            }
            else
            {
                reactor.hitReactor(c);
            }
        }
    }


    public static void hitCoconut(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int id = slea.readShort();
        String co = "coconut";
        MapleCoconut map = (MapleCoconut) c.getChannelServer().getEvent(server.events.MapleEventType.Coconut);
        if ((map == null) || (!map.isRunning()))
        {
            map = (MapleCoconut) c.getChannelServer().getEvent(server.events.MapleEventType.CokePlay);
            co = "coke cap";
            if ((map == null) || (!map.isRunning()))
            {
                return;
            }
        }

        MapleCoconut.MapleCoconuts nut = map.getCoconut(id);
        if ((nut == null) || (!nut.isHittable()))
        {
            return;
        }
        if (System.currentTimeMillis() < nut.getHitTime())
        {
            return;
        }

        if ((nut.getHits() > 2) && (Math.random() < 0.4D) && (!nut.isStopped()))
        {
            nut.setHittable(false);
            if ((Math.random() < 0.01D) && (map.getStopped() > 0))
            {
                nut.setStopped(true);
                map.stopCoconut();
                c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.hitCoconut(false, id, 1));
                return;
            }
            nut.resetHits();

            if ((Math.random() < 0.05D) && (map.getBombings() > 0))
            {
                c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.hitCoconut(false, id, 2));
                map.bombCoconut();
            }
            else if (map.getFalling() > 0)
            {
                c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.hitCoconut(false, id, 3));
                map.fallCoconut();
                if (c.getPlayer().getTeam() == 0)
                {
                    map.addMapleScore();
                }
                else
                {
                    map.addStoryScore();
                }

                c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.coconutScore(map.getCoconutScore()));
            }
        }
        else
        {
            nut.hit();
            c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.hitCoconut(false, id, 1));
        }
    }

    public static void FollowRequest(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MapleCharacter tt = c.getPlayer().getMap().getCharacterById(slea.readInt());
        if (slea.readByte() > 0)
        {
            tt = c.getPlayer().getMap().getCharacterById(c.getPlayer().getFollowId());
            if ((tt != null) && (tt.getFollowId() == c.getPlayer().getId()))
            {
                tt.setFollowOn(true);
                c.getPlayer().setFollowOn(true);
            }
            else
            {
                c.getPlayer().checkFollow();
            }
            return;
        }
        if (slea.readByte() > 0)
        {
            tt = c.getPlayer().getMap().getCharacterById(c.getPlayer().getFollowId());
            if ((tt != null) && (tt.getFollowId() == c.getPlayer().getId()) && (c.getPlayer().isFollowOn()))
            {
                c.getPlayer().checkFollow();
            }
            return;
        }
        if ((tt != null) && (tt.getPosition().distanceSq(c.getPlayer().getPosition()) < 10000.0D) && (tt.getFollowId() == 0) && (c.getPlayer().getFollowId() == 0) && (tt.getId() != c.getPlayer().getId()))
        {
            tt.setFollowId(c.getPlayer().getId());
            tt.setFollowOn(false);
            tt.setFollowInitiator(false);
            c.getPlayer().setFollowOn(false);
            c.getPlayer().setFollowInitiator(false);
            tt.getClient().getSession().write(MaplePacketCreator.followRequest(c.getPlayer().getId()));
        }
        else
        {
            c.getSession().write(MaplePacketCreator.serverNotice(1, "距离太远。"));
        }
    }

    public static void FollowReply(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer().getFollowId() > 0) && (c.getPlayer().getFollowId() == slea.readInt()))
        {
            MapleCharacter tt = c.getPlayer().getMap().getCharacterById(c.getPlayer().getFollowId());
            if ((tt != null) && (tt.getPosition().distanceSq(c.getPlayer().getPosition()) < 10000.0D) && (tt.getFollowId() == 0) && (tt.getId() != c.getPlayer().getId()))
            {
                boolean accepted = slea.readByte() > 0;
                if (accepted)
                {
                    tt.setFollowId(c.getPlayer().getId());
                    tt.setFollowOn(true);
                    tt.setFollowInitiator(false);
                    c.getPlayer().setFollowOn(true);
                    c.getPlayer().setFollowInitiator(true);
                    c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.followEffect(tt.getId(), c.getPlayer().getId(), null));
                }
                else
                {
                    c.getPlayer().setFollowId(0);
                    tt.setFollowId(0);
                    tt.getClient().getSession().write(MaplePacketCreator.getFollowMsg(5));
                }
            }
            else
            {
                if (tt != null)
                {
                    tt.setFollowId(0);
                    c.getPlayer().setFollowId(0);
                }
                c.getSession().write(MaplePacketCreator.serverNotice(1, "距离太远."));
            }
        }
        else
        {
            c.getPlayer().setFollowId(0);
        }
    }

    public static void RingAction(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        byte mode = slea.readByte();
        if (mode == 0)
        {
            DoRing(c, slea.readMapleAsciiString(), slea.readInt());
        }
        else if (mode == 1)
        {
            c.getPlayer().setMarriageItemId(0);
        }
        else if (mode == 2)
        {
            boolean accepted = slea.readByte() > 0;
            String name = slea.readMapleAsciiString();
            int id = slea.readInt();
            MapleCharacter chr = c.getChannelServer().getPlayerStorage().getCharacterByName(name);
            if ((c.getPlayer().getMarriageId() > 0) || (chr == null) || (chr.getId() != id) || (chr.getMarriageItemId() <= 0) || (!chr.haveItem(chr.getMarriageItemId(), 1)) || (chr.getMarriageId() > 0) || (!chr.isAlive()) || (chr.getEventInstance() != null) || (!c.getPlayer().isAlive()) || (c.getPlayer().getEventInstance() != null))
            {
                c.getSession().write(MaplePacketCreator.sendEngagement((byte) 31, 0, null, null));
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if (accepted)
            {
                int itemid = chr.getMarriageItemId();
                int newItemId = getMarriageNewItemId(itemid);
                if ((!MapleInventoryManipulator.checkSpace(c, newItemId, 1, "")) || (!MapleInventoryManipulator.checkSpace(chr.getClient(), newItemId, 1, "")))
                {
                    c.getSession().write(MaplePacketCreator.sendEngagement((byte) 21, 0, null, null));
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                try
                {
                    int[] ringID = client.inventory.MapleRing.makeRing(newItemId, c.getPlayer(), chr);
                    client.inventory.Equip eq = (client.inventory.Equip) MapleItemInformationProvider.getInstance().getEquipById(newItemId, ringID[1]);
                    client.inventory.MapleRing ring = client.inventory.MapleRing.loadFromDb(ringID[1]);
                    if (ring != null)
                    {
                        eq.setRing(ring);
                    }
                    MapleInventoryManipulator.addbyItem(c, eq);
                    eq = (client.inventory.Equip) MapleItemInformationProvider.getInstance().getEquipById(newItemId, ringID[0]);
                    ring = client.inventory.MapleRing.loadFromDb(ringID[0]);
                    if (ring != null)
                    {
                        eq.setRing(ring);
                    }
                    MapleInventoryManipulator.addbyItem(chr.getClient(), eq);
                    MapleInventoryManipulator.removeById(chr.getClient(), MapleInventoryType.USE, chr.getMarriageItemId(), 1, false, false);
                    chr.getClient().getSession().write(MaplePacketCreator.sendEngagement((byte) 13, newItemId, chr, c.getPlayer()));
                    chr.setMarriageId(c.getPlayer().getId());
                    c.getPlayer().setMarriageId(chr.getId());
                    chr.fakeRelog();
                    c.getPlayer().fakeRelog();
                    handling.world.WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.yellowChat("[系统公告] 恭喜：" + c.getPlayer().getName() + " 和 " + chr.getName() + "结为夫妻。 希望你们在 " + chr.getClient().getChannelServer().getServerName() + " 游戏中玩的愉快!"));
                }
                catch (Exception e)
                {
                    tools.FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
                }
            }
            else
            {
                chr.getClient().getSession().write(MaplePacketCreator.sendEngagement((byte) 32, 0, null, null));
            }
            c.getSession().write(MaplePacketCreator.enableActions());
            chr.setMarriageItemId(0);
        }
        else if (mode == 3)
        {
            int itemId = slea.readInt();
            MapleInventoryType type = constants.ItemConstants.getInventoryType(itemId);
            Item item = c.getPlayer().getInventory(type).findById(itemId);
            if ((item != null) && (type == MapleInventoryType.ETC) && (itemId / 10000 == 421))
            {
                MapleInventoryManipulator.drop(c, type, item.getPosition(), item.getQuantity());
            }
        }
    }

    public static void DoRing(MapleClient c, String name, int itemid)
    {
        int newItemId = getMarriageNewItemId(itemid);
        MapleCharacter chr = c.getChannelServer().getPlayerStorage().getCharacterByName(name);


        int errcode = 0;
        if (GameConstants.is神之子(c.getPlayer().getJob()))
        {
            errcode = 21;
        }
        else if (c.getPlayer().getMarriageId() > 0)
        {
            errcode = 29;
        }
        else if (c.getPlayer().getMarriageItemId() > 0)
        {
            errcode = 27;
        }
        else if ((!c.getPlayer().haveItem(itemid, 1)) || (itemid < 2240004) || (itemid > 2240015))
        {
            errcode = 15;
        }
        else if (chr == null)
        {
            errcode = 22;
        }
        else if (GameConstants.is神之子(chr.getJob()))
        {
            errcode = 21;
        }
        else if (chr.getMapId() != c.getPlayer().getMapId())
        {
            errcode = 23;
        }
        else if (chr.getGender() == c.getPlayer().getGender())
        {
            errcode = 26;
        }
        else if (chr.getMarriageId() > 0)
        {
            errcode = 30;
        }
        else if (chr.getMarriageItemId() > 0)
        {
            errcode = 28;
        }
        else if (!MapleInventoryManipulator.checkSpace(c, newItemId, 1, ""))
        {
            errcode = 24;
        }
        else if (!MapleInventoryManipulator.checkSpace(chr.getClient(), newItemId, 1, ""))
        {
            errcode = 25;
        }

        if (errcode > 0)
        {
            c.getSession().write(MaplePacketCreator.sendEngagement((byte) errcode, 0, null, null));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        c.getPlayer().setMarriageItemId(itemid);
        chr.getClient().getSession().write(MaplePacketCreator.sendEngagementRequest(c.getPlayer().getName(), c.getPlayer().getId()));
    }

    private static int getMarriageNewItemId(int itemId)
    {
        int newItemId;
        if (itemId == 2240004)
        {
            newItemId = 1112300;
        }
        else
        {
            if (itemId == 2240005)
            {
                newItemId = 1112301;
            }
            else
            {
                if (itemId == 2240006)
                {
                    newItemId = 1112302;
                }
                else
                {
                    if (itemId == 2240007)
                    {
                        newItemId = 1112303;
                    }
                    else
                    {
                        if (itemId == 2240008)
                        {
                            newItemId = 1112304;
                        }
                        else
                        {
                            if (itemId == 2240009)
                            {
                                newItemId = 1112305;
                            }
                            else
                            {
                                if (itemId == 2240010)
                                {
                                    newItemId = 1112306;
                                }
                                else
                                {
                                    if (itemId == 2240011)
                                    {
                                        newItemId = 1112307;
                                    }
                                    else
                                    {
                                        if (itemId == 2240012)
                                        {
                                            newItemId = 1112308;
                                        }
                                        else
                                        {

                                            if (itemId == 2240013)
                                            {
                                                newItemId = 1112309;
                                            }
                                            else
                                            {

                                                if (itemId == 2240014)
                                                {
                                                    newItemId = 1112310;
                                                }
                                                else
                                                {

                                                    if (itemId == 2240015)
                                                    {
                                                        newItemId = 1112311;
                                                    }
                                                    else throw new RuntimeException("Invalid Item Maker id");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return newItemId;
    }

    public static void Solomon(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.enableActions());
        c.getPlayer().updateTick(slea.readInt());
        Item item = c.getPlayer().getInventory(MapleInventoryType.USE).getItem(slea.readShort());
        if ((item == null) || (item.getItemId() != slea.readInt()) || (item.getQuantity() <= 0) || (c.getPlayer().getGachExp() > 0) || (c.getPlayer().getLevel() > 50) || (MapleItemInformationProvider.getInstance().getItemEffect(item.getItemId()).getEXP() <= 0))
        {
            return;
        }
        c.getPlayer().setGachExp(c.getPlayer().getGachExp() + MapleItemInformationProvider.getInstance().getItemEffect(item.getItemId()).getEXP());
        MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, item.getPosition(), (short) 1, false);
        c.getPlayer().updateSingleStat(client.MapleStat.GACHAPONEXP, c.getPlayer().getGachExp());
    }

    public static void GachExp(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.enableActions());
        c.getPlayer().updateTick(slea.readInt());
        if (c.getPlayer().getGachExp() <= 0)
        {
            return;
        }
        c.getPlayer().gainExp(c.getPlayer().getGachExp() * GameConstants.getExpRate_Quest(c.getPlayer().getLevel()), true, true, false);
        c.getPlayer().setGachExp(0);
        c.getPlayer().updateSingleStat(client.MapleStat.GACHAPONEXP, 0L);
    }


    public static void Report(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        client.anticheat.ReportType type = client.anticheat.ReportType.getById(slea.readByte());
        MapleCharacter other = c.getPlayer().getMap().getCharacterByName(slea.readMapleAsciiString());

        if ((other == null) || (type == null) || (other.isIntern()))
        {
            c.getPlayer().dropMessage(1, "举报错误.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        client.MapleQuestStatus stat = c.getPlayer().getQuestNAdd(server.quest.MapleQuest.getInstance(123457));
        if (stat.getCustomData() == null)
        {
            stat.setCustomData("0");
        }
        long currentTime = System.currentTimeMillis();
        long theTime = Long.parseLong(stat.getCustomData());
        if ((theTime + 7200000L > currentTime) && (!c.getPlayer().isIntern()))
        {
            c.getPlayer().dropMessage(5, "每2小时才能举报1次.");
        }
        else
        {
            stat.setCustomData(String.valueOf(currentTime));
            other.addReport(type);

            c.getPlayer().dropMessage(1, "举报完成.");
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void MonsterBookInfoRequest(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null))
        {
            return;
        }
        slea.readInt();
        MapleCharacter player = c.getPlayer().getMap().getCharacterById(slea.readInt());
        c.getSession().write(MaplePacketCreator.enableActions());
        if ((player != null) && ((!player.isGM()) || (c.getPlayer().isGM())))
        {
            c.getSession().write(MaplePacketCreator.getMonsterBookInfo(player));
        }
    }

    public static void ChangeSet(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null))
        {
            return;
        }
        int set = slea.readInt();
        if (chr.getMonsterBook().changeSet(set))
        {
            chr.getMonsterBook().applyBook(chr, false);
            chr.getQuestNAdd(server.quest.MapleQuest.getInstance(122800)).setCustomData(String.valueOf(set));
            c.getSession().write(MaplePacketCreator.changeCardSet(set));
        }
    }

    public static void EnterPVP(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null) || (c.getPlayer().getMapId() != 960000000))
        {
            c.getSession().write(MaplePacketCreator.partyBlocked(1));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (c.getPlayer().getParty() != null)
        {
            c.getSession().write(MaplePacketCreator.partyBlocked(9));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        c.getPlayer().updateTick(slea.readInt());
        slea.skip(1);
        int type = slea.readByte();
        int lvl = slea.readByte();
        int playerCount = 0;
        boolean passed = false;
        switch (lvl)
        {
            case 0:
                passed = (c.getPlayer().getLevel() >= 30) && (c.getPlayer().getLevel() < 70);
                break;
            case 1:
                passed = c.getPlayer().getLevel() >= 70;
                break;
            case 2:
                passed = c.getPlayer().getLevel() >= 120;
                break;
            case 3:
                passed = c.getPlayer().getLevel() >= 180;
        }

        scripting.event.EventManager em = c.getChannelServer().getEventSM().getEventManager("PVP");
        if ((!passed) || (em == null))
        {
            c.getSession().write(MaplePacketCreator.partyBlocked(1));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        List<Integer> maps = new ArrayList<>();
        switch (type)
        {
            case 0:
                maps.add(960010100);
                maps.add(960010101);
                maps.add(960010102);
                break;
            case 1:
                maps.add(960020100);
                maps.add(960020101);
                maps.add(960020102);
                maps.add(960020103);
                break;
            case 2:
                maps.add(960030100);
                break;
            case 3:
                maps.add(689000000);
                maps.add(689000010);
                break;
            default:
                passed = false;
        }

        if (!passed)
        {
            c.getSession().write(MaplePacketCreator.partyBlocked(1));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        c.getPlayer().getStat().heal(c.getPlayer());
        c.getPlayer().cancelAllBuffs();
        c.getPlayer().dispelDebuffs();
        c.getPlayer().changeRemoval();
        c.getPlayer().clearAllCooldowns();
        c.getPlayer().unequipAllSpawnPets();
        StringBuilder key = new StringBuilder().append(lvl).append(" ").append(type).append(" ");

        for (Integer map : maps)
        {
            int i = map;
            EventInstanceManager eim = em.getInstance(("PVP" + key.toString() + i).replace(" ", "").replace(" ", ""));
            if ((eim != null) && ((eim.getProperty("started").equals("0")) || (eim.getPlayerCount() < 10)))
            {
                eim.registerPlayer(c.getPlayer());
                return;
            }
        }

        em.startInstance_Solo(maps.get(Randomizer.nextInt(maps.size())) + "", c.getPlayer());
    }

    public static void RespawnPVP(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null) || (!c.getPlayer().inPVP()) || (c.getPlayer().isAlive()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int type = Integer.parseInt(c.getPlayer().getEventInstance().getProperty("type"));
        c.getPlayer().getStat().heal_noUpdate(c.getPlayer());
        c.getPlayer().updateSingleStat(client.MapleStat.MP, c.getPlayer().getStat().getMp());
        c.getPlayer().getEventInstance().schedule("broadcastType", 500L);
        c.getPlayer().getEventInstance().schedule("updateScoreboard", 500L);
        c.getPlayer().changeMap(c.getPlayer().getMap(), c.getPlayer().getMap().getPortal(c.getPlayer().getTeam() == 0 ? 2 : type == 3 ? 1 : c.getPlayer().getTeam() == 0 ? 3 : type == 0 ? 0 : 3));
        c.getSession().write(MaplePacketCreator.getPVPScore(Integer.parseInt(c.getPlayer().getEventInstance().getProperty(String.valueOf(c.getPlayer().getId()))), false));
    }

    public static void LeavePVP(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null) || (!c.getPlayer().inPVP()))
        {
            c.getSession().write(MaplePacketCreator.partyBlocked(6));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int x = Integer.parseInt(c.getPlayer().getEventInstance().getProperty(String.valueOf(c.getPlayer().getId())));
        int lv = Integer.parseInt(c.getPlayer().getEventInstance().getProperty("lvl"));
        if ((lv < 2) && (c.getPlayer().getLevel() >= 120))
        {
            x /= 2;
        }
        c.getPlayer().setTotalBattleExp(c.getPlayer().getTotalBattleExp() + x / 10 * 3 / 2);
        c.getPlayer().setBattlePoints(c.getPlayer().getBattlePoints() + x / 10 * 3 / 2);
        c.getPlayer().cancelAllBuffs();
        c.getPlayer().changeRemoval();
        c.getPlayer().dispelDebuffs();
        c.getPlayer().clearAllCooldowns();
        c.getPlayer().updateTick(slea.readInt());
        c.getSession().write(tools.packet.UIPacket.clearMidMsg());
        c.getPlayer().changeMap(c.getChannelServer().getMapFactory().getMap(960000000));
        c.getPlayer().getStat().recalcLocalStats(c.getPlayer());
        c.getPlayer().getStat().heal(c.getPlayer());
    }

    public static void AttackPVP(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MapleCharacter chr = c.getPlayer();
        int trueSkill = slea.readInt();
        int skillid = trueSkill;
        if ((chr == null) || (chr.isHidden()) || (!chr.isAlive()) || (chr.hasBlockedInventory()) || (chr.getMap() == null) || (!chr.inPVP()) || (!chr.getEventInstance().getProperty("started").equals("1")) || (skillid >= 90000000))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int lvl = Integer.parseInt(chr.getEventInstance().getProperty("lvl"));
        int type = Integer.parseInt(chr.getEventInstance().getProperty("type"));
        int ice = Integer.parseInt(chr.getEventInstance().getProperty("ice"));
        int ourScore = Integer.parseInt(chr.getEventInstance().getProperty(String.valueOf(chr.getId())));
        int addedScore = 0;
        int skillLevel = 0;
        int trueSkillLevel = 0;
        int animation = -1;
        int attackCount = 1;
        int mobCount = 1;
        int fakeMastery = chr.getStat().passive_mastery();
        int ignoreDEF = chr.getStat().percent_ignore_mob_def_rate;
        int critRate = chr.getStat().passive_sharpeye_rate();
        int skillDamage = 100;
        boolean magic = false;
        boolean move = false;
        boolean pull = false;
        boolean push = false;

        double maxdamage = lvl == 3 ? chr.getStat().getCurrentMaxBasePVPDamageL() : chr.getStat().getCurrentMaxBasePVPDamage();
        MapleStatEffect effect = null;
        chr.checkFollow();
        Rectangle box = null;

        Item weapon = chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
        Item shield = chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
        boolean katara = (shield != null) && (shield.getItemId() / 10000 == 134);
        boolean aran = (weapon != null) && (weapon.getItemId() / 10000 == 144) && (GameConstants.is战神(chr.getJob()));
        slea.skip(1);
        int chargeTime = 0;
        if (GameConstants.isMagicChargeSkill(skillid))
        {
            chargeTime = slea.readInt();
        }
        else
        {
            slea.skip(4);
        }
        boolean facingLeft = slea.readByte() > 0;
        if (skillid > 0)
        {
            Skill skil = SkillFactory.getSkill(skillid);
            if ((skil == null) || (skil.isPVPDisabled()))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            magic = skil.isMagic();
            move = skil.isMovement();
            push = skil.isPush();
            pull = skil.isPull();
            if (chr.getTotalSkillLevel(GameConstants.getLinkedAttackSkill(skillid)) <= 0)
            {
                if ((!GameConstants.isIceKnightSkill(skillid)) && (chr.getTotalSkillLevel(GameConstants.getLinkedAttackSkill(skillid)) <= 0))
                {
                    chr.getClient().disconnect(true, false);
                    c.getSession().close(true);
                    return;
                }
                if ((GameConstants.isIceKnightSkill(skillid)) && (chr.getBuffSource(MapleBuffStat.变身术) % 10000 != 1105))
                {
                    return;
                }
            }
            animation = skil.getAnimation();
            if ((animation == -1) && (!skil.isMagic()))
            {
                String after = weapon == null ? "barehands" : katara ? "katara" : aran ? "aran" : MapleItemInformationProvider.getInstance().getAfterImage(weapon.getItemId());
                if (after != null)
                {
                    List<Triple<String, Point, Point>> p = MapleItemInformationProvider.getInstance().getAfterImage(after);
                    if (p != null)
                    {
                        while (animation == -1)
                        {
                            Triple<String, Point, Point> ep = p.get(Randomizer.nextInt(p.size()));
                            if ((ep.left.contains("stab")) && (

                                    (!ep.left.contains("stab")) || (weapon == null) || (weapon.getItemId() / 10000 != 144)))
                            {

                                if (SkillFactory.getDelay(ep.left) != null) animation = SkillFactory.getDelay(ep.left);
                            }
                        }
                    }
                }
            }
            else if ((animation == -1) && (skil.isMagic()))
            {
                animation = SkillFactory.getDelay(Randomizer.nextBoolean() ? "dash" : "dash2");
            }
            if (skil.isMagic())
            {
                fakeMastery = 0;
            }
            skillLevel = chr.getTotalSkillLevel(GameConstants.getLinkedAttackSkill(skillid));
            trueSkillLevel = chr.getTotalSkillLevel(GameConstants.getLinkedAttackSkill(trueSkill));
            effect = skil.getPVPEffect(skillLevel);
            ignoreDEF += effect.getIgnoreMob();
            critRate += effect.getCritical();

            skillDamage = effect.getDamage() + chr.getStat().getDamageIncrease(skillid);
            box = effect.calculateBoundingBox(chr.getTruePosition(), facingLeft, chr.getStat().defRange);
            attackCount = Math.max(effect.getBulletCount(chr), effect.getAttackCount(chr));
            mobCount = Math.max(1, effect.getMobCount());
            if ((effect.getCooldown(chr) > 0) && (!chr.isGM()))
            {
                if (chr.skillisCooling(skillid))
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                if (((skillid != 35111004) && (skillid != 35121013)) || (chr.getBuffSource(MapleBuffStat.金属机甲) != skillid))
                {
                    c.getSession().write(MaplePacketCreator.skillCooldown(skillid, effect.getCooldown(chr)));
                    chr.addCooldown(skillid, System.currentTimeMillis(), effect.getCooldown(chr) * 1000);
                }
            }
            switch (chr.getJob())
            {
                case 111:
                case 112:
                case 1111:
                case 1112:
                case 2411:
                case 2412:
                    if (PlayerHandler.isFinisher(skillid) > 0)
                    {
                        if ((chr.getBuffedValue(MapleBuffStat.斗气集中) == null) || (chr.getBuffedValue(MapleBuffStat.斗气集中) <= 2))
                        {
                            return;
                        }
                        skillDamage *= (chr.getBuffedValue(MapleBuffStat.斗气集中) - 1) / 2;
                        chr.handleOrbconsume(PlayerHandler.isFinisher(skillid));
                    }
                    break;
            }
        }
        else
        {
            attackCount = katara ? 2 : 1;
            Point lt = null;
            Point rb = null;
            String after = weapon == null ? "barehands" : katara ? "katara" : aran ? "aran" : MapleItemInformationProvider.getInstance().getAfterImage(weapon.getItemId());
            if (after != null)
            {
                List<Triple<String, Point, Point>> p = MapleItemInformationProvider.getInstance().getAfterImage(after);
                if (p != null)
                {
                    while (animation == -1)
                    {
                        Triple<String, Point, Point> ep = p.get(Randomizer.nextInt(p.size()));
                        if ((ep.left.contains("stab")) && (

                                (!ep.left.contains("stab")) || (weapon == null) || (weapon.getItemId() / 10000 != 147)))
                        {

                            if (SkillFactory.getDelay(ep.left) != null)
                            {
                                animation = SkillFactory.getDelay(ep.left);
                                lt = ep.mid;
                                rb = ep.right;
                            }
                        }
                    }
                }
            }
            box = MapleStatEffect.calculateBoundingBox(chr.getTruePosition(), facingLeft, lt, rb, chr.getStat().defRange);
        }
        chr.getCheatTracker().checkPVPAttack(skillid);
        MapleStatEffect shad = chr.getStatForBuff(MapleBuffStat.影分身);
        int originalAttackCount = attackCount;
        attackCount *= (shad != null ? 2 : 1);

        slea.skip(4);
        int speed = slea.readByte();
        int slot = slea.readShort();
        int csstar = slea.readShort();
        int visProjectile = 0;
        if ((chr.getJob() >= 3500) && (chr.getJob() <= 3512))
        {
            visProjectile = 2333000;
        }
        else if (GameConstants.is火炮手(chr.getJob()))
        {
            visProjectile = 2333001;
        }
        else if ((!GameConstants.is双弩精灵(chr.getJob())) && (chr.getBuffedValue(MapleBuffStat.无形箭弩) == null) && (slot > 0))
        {
            Item ipp = chr.getInventory(MapleInventoryType.USE).getItem((short) slot);
            if (ipp == null)
            {
                return;
            }
            if (csstar > 0)
            {
                ipp = chr.getInventory(MapleInventoryType.CASH).getItem((short) csstar);
                if (ipp == null)
                {
                    return;
                }
            }
            visProjectile = ipp.getItemId();
        }
        maxdamage *= skillDamage / 100.0D;
        maxdamage *= chr.getStat().getDamageRate() / 100.0D;
        List<tools.AttackPair> ourAttacks = new ArrayList(mobCount);
        boolean area = inArea(chr);
        boolean didAttack = false;
        boolean killed = false;
        if (!area)
        {
            for (MapleCharacter attacked : chr.getMap().getCharactersIntersect(box))
            {
                if ((attacked.getId() != chr.getId()) && (attacked.isAlive()) && (!attacked.isHidden()) && ((type == 0) || (attacked.getTeam() != chr.getTeam())))
                {
                    double rawDamage = maxdamage / Math.max(1.0D, (magic ? attacked.getStat().mdef : attacked.getStat().wdef) * Math.max(1.0D, 100.0D - ignoreDEF) / 100.0D * (type == 3 ? 0.2D :
                            0.5D));
                    if ((attacked.getBuffedValue(MapleBuffStat.INVINCIBILITY) != null) || (inArea(attacked)))
                    {
                        rawDamage = 0.0D;
                    }
                    rawDamage *= attacked.getStat().mesoGuard / 100.0D;
                    rawDamage += rawDamage * chr.getDamageIncrease(attacked.getId()) / 100.0D;
                    rawDamage = attacked.modifyDamageTaken(rawDamage, attacked).left;
                    double min = rawDamage * chr.getStat().trueMastery / 100.0D;
                    List<Pair<Integer, Boolean>> attacks = new ArrayList(attackCount);
                    int totalMPLoss = 0;
                    int totalHPLoss = 0;
                    double ourDamage;
                    for (int i = 0; i < attackCount; i++)
                    {
                        boolean critical_ = false;
                        int mploss = 0;
                        ourDamage = Randomizer.nextInt((int) Math.abs(Math.round(rawDamage - min)) + 2) + min;
                        if ((attacked.getStat().dodgeChance > 0) && (Randomizer.nextInt(100) < attacked.getStat().dodgeChance))
                        {
                            ourDamage = 0.0D;
                        }
                        else if ((attacked.hasDisease(MapleDisease.黑暗)) && (Randomizer.nextInt(100) < 50))
                        {
                            ourDamage = 0.0D;


                        }
                        else if ((attacked.getJob() == 122) && (attacked.getTotalSkillLevel(1221016) > 0) && (attacked.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10) != null))
                        {
                            MapleStatEffect eff = SkillFactory.getSkill(1221016).getEffect(attacked.getTotalSkillLevel(1221016));
                            if (eff.makeChanceResult())
                            {
                                ourDamage = 0.0D;
                            }
                        }
                        else if ((attacked.getJob() == 412) && (attacked.getTotalSkillLevel(4120002) > 0))
                        {
                            MapleStatEffect eff = SkillFactory.getSkill(4120002).getEffect(attacked.getTotalSkillLevel(4120002));
                            if (eff.makeChanceResult())
                            {
                                ourDamage = 0.0D;
                            }
                        }
                        else if ((attacked.getJob() == 422) && (attacked.getTotalSkillLevel(4220002) > 0))
                        {
                            MapleStatEffect eff = SkillFactory.getSkill(4220002).getEffect(attacked.getTotalSkillLevel(4220002));
                            if (eff.makeChanceResult())
                            {
                                ourDamage = 0.0D;
                            }
                        }
                        else if ((shad != null) && (i >= originalAttackCount))
                        {
                            ourDamage *= shad.getX() / 100.0D;
                        }
                        if ((ourDamage > 0.0D) && (skillid != 4211006) && ((skillid == 4221014) || (skillid == 3221007) || (skillid == 23121003) || (skillid == 4321006) || (skillid == 4331006) || (skillid == 21121013) || (Randomizer.nextInt(100) < critRate)))
                        {
                            ourDamage *= (100.0D + (Randomizer.nextInt(Math.max(2, chr.getStat().passive_sharpeye_percent() - chr.getStat().passive_sharpeye_min_percent())) + chr.getStat().passive_sharpeye_min_percent())) / 100.0D;
                            critical_ = true;
                        }
                        if (attacked.getBuffedValue(MapleBuffStat.魔法盾) != null)
                        {
                            mploss = (int) Math.min(attacked.getStat().getMp(), ourDamage * attacked.getBuffedValue(MapleBuffStat.魔法盾).doubleValue() / 100.0D);
                        }
                        ourDamage -= mploss;
                        if (attacked.getBuffedValue(MapleBuffStat.终极无限) != null)
                        {
                            mploss = 0;
                        }
                        attacks.add(new Pair((int) Math.floor(ourDamage), critical_));

                        totalHPLoss = (int) (totalHPLoss + Math.floor(ourDamage));
                        totalMPLoss += mploss;
                    }
                    addedScore += Math.min(attacked.getStat().getHp() / 100, totalHPLoss / 100 + totalMPLoss / 100);
                    attacked.addMPHP(-totalHPLoss, -totalMPLoss);
                    ourAttacks.add(new tools.AttackPair(attacked.getId(), attacked.getPosition(), attacks));
                    chr.onAttack(attacked.getStat().getCurrentMaxHp(), attacked.getStat().getCurrentMaxMp(attacked.getJob()), skillid, attacked.getObjectId(), totalHPLoss);
                    attacked.getCheatTracker().setAttacksWithoutHit(false);
                    if (totalHPLoss > 0)
                    {
                        didAttack = true;
                    }
                    if (attacked.getStat().getHPPercent() <= 20) SkillFactory.getSkill(PlayerStats.getSkillByJob(93, attacked.getJob())).getEffect(1).applyTo(attacked);
                    Map.Entry<client.status.MonsterStatus, Integer> z;
                    MapleDisease d;
                    if (effect != null)
                    {
                        if ((effect.getMonsterStati().size() > 0) && (effect.makeChanceResult()))
                        {
                            for (Map.Entry<MonsterStatus, Integer> monsterStatusIntegerEntry : effect.getMonsterStati().entrySet())
                            {
                                z = monsterStatusIntegerEntry;
                                d = MonsterStatus.getLinkedDisease(z.getKey());
                                if (d != null)
                                {
                                    attacked.giveDebuff(d, z.getValue(), effect.getDuration(), d.getDisease(), 1);
                                }
                            }
                        }
                        effect.handleExtraPVP(chr, attacked);
                    }
                    if ((chr.getJob() == 121) || (chr.getJob() == 122) || (chr.getJob() == 2110) || (chr.getJob() == 2111) || (chr.getJob() == 2112))
                    {
                        if ((chr.getBuffSource(MapleBuffStat.属性攻击) == 1201012) || (chr.getBuffSource(MapleBuffStat.属性攻击) == 21101006))
                        {
                            MapleStatEffect eff = chr.getStatForBuff(MapleBuffStat.属性攻击);
                            if (eff.makeChanceResult())
                            {
                                attacked.giveDebuff(MapleDisease.FREEZE, 1, eff.getDuration(), MapleDisease.FREEZE.getDisease(), 1);
                            }
                        }
                    }
                    else if (chr.getBuffedValue(MapleBuffStat.敏捷提升) != null)
                    {
                        MapleStatEffect eff = chr.getStatForBuff(MapleBuffStat.敏捷提升);

                        if ((eff != null) && (eff.makeChanceResult()))
                        {
                            attacked.giveDebuff(MapleDisease.缓慢, 100 - Math.abs(eff.getX()), eff.getDuration(), MapleDisease.缓慢.getDisease(), 1);
                        }
                    }
                    else if (chr.getBuffedValue(MapleBuffStat.缓速术) != null)
                    {
                        MapleStatEffect eff = chr.getStatForBuff(MapleBuffStat.缓速术);
                        if ((eff != null) && (eff.makeChanceResult()))
                        {
                            attacked.giveDebuff(MapleDisease.缓慢, 100 - Math.abs(eff.getX()), eff.getDuration(), MapleDisease.缓慢.getDisease(), 1);
                        }
                    }
                    else if ((chr.getJob() == 412) || (chr.getJob() == 422) || (chr.getJob() == 434) || (chr.getJob() == 1411) || (chr.getJob() == 1412))
                    {
                        int[] skills = {4110011, 4210010, 4320005, 14110004};
                        for (Integer skid : skills)
                        {
                            Skill skill = SkillFactory.getSkill(skid);
                            if (chr.getTotalSkillLevel(skill) > 0)
                            {
                                MapleStatEffect venomEffect = skill.getEffect(chr.getTotalSkillLevel(skill));
                                if (!venomEffect.makeChanceResult()) break;
                                attacked.giveDebuff(MapleDisease.中毒, 1, venomEffect.getDuration(), MapleDisease.中毒.getDisease(), 1);
                                break;
                            }
                        }
                    }


                    if (chr.getJob() / 100 % 10 == 2)
                    {
                        int[] skills = {2000007, 12000006, 22000002, 32000012};
                        for (Integer skid : skills)
                        {
                            Skill skill = SkillFactory.getSkill(skid);
                            if (chr.getTotalSkillLevel(skill) > 0)
                            {
                                MapleStatEffect venomEffect = skill.getEffect(chr.getTotalSkillLevel(skill));
                                if (!venomEffect.makeChanceResult()) break;
                                venomEffect.applyTo(attacked);
                                break;
                            }
                        }
                    }


                    if (ice == attacked.getId())
                    {
                        chr.getClient().getSession().write(MaplePacketCreator.getPVPIceHPBar(attacked.getStat().getHp(), attacked.getStat().getCurrentMaxHp()));
                    }
                    else
                    {
                        chr.getClient().getSession().write(MaplePacketCreator.getPVPHPBar(attacked.getId(), attacked.getStat().getHp(), attacked.getStat().getCurrentMaxHp()));
                    }
                    if (!attacked.isAlive())
                    {
                        addedScore += 5;
                        killed = true;
                    }
                    if (ourAttacks.size() >= mobCount)
                    {
                        break;
                    }
                }
            }
        }
        else if (type == 3)
        {
            if ((Integer.parseInt(chr.getEventInstance().getProperty("redflag")) == chr.getId()) && (chr.getMap().getArea(1).contains(chr.getTruePosition())))
            {
                chr.getEventInstance().setProperty("redflag", "0");
                chr.getEventInstance().setProperty("blue", String.valueOf(Integer.parseInt(chr.getEventInstance().getProperty("blue")) + 1));
                chr.getEventInstance().broadcastPlayerMsg(-7, "Blue Team has scored a point!");
                chr.getMap().spawnAutoDrop(2910000, chr.getMap().getGuardians().get(0).left);
                chr.getEventInstance().broadcastPacket(MaplePacketCreator.getCapturePosition(chr.getMap()));
                chr.getEventInstance().broadcastPacket(MaplePacketCreator.resetCapture());
                chr.getEventInstance().schedule("updateScoreboard", 1000L);
            }
            else if ((Integer.parseInt(chr.getEventInstance().getProperty("blueflag")) == chr.getId()) && (chr.getMap().getArea(0).contains(chr.getTruePosition())))
            {
                chr.getEventInstance().setProperty("blueflag", "0");
                chr.getEventInstance().setProperty("red", String.valueOf(Integer.parseInt(chr.getEventInstance().getProperty("red")) + 1));
                chr.getEventInstance().broadcastPlayerMsg(-7, "Red Team has scored a point!");
                chr.getMap().spawnAutoDrop(2910001, chr.getMap().getGuardians().get(1).left);
                chr.getEventInstance().broadcastPacket(MaplePacketCreator.getCapturePosition(chr.getMap()));
                chr.getEventInstance().broadcastPacket(MaplePacketCreator.resetCapture());
                chr.getEventInstance().schedule("updateScoreboard", 1000L);
            }
        }
        if (chr.getEventInstance() == null)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((killed) || (addedScore > 0))
        {
            chr.getEventInstance().addPVPScore(chr, addedScore);
            chr.getClient().getSession().write(MaplePacketCreator.getPVPScore(ourScore + addedScore, killed));
        }
        if (didAttack)
        {
            chr.afterAttack(ourAttacks.size(), attackCount, skillid);
            PlayerHandler.AranCombo(c, chr, ourAttacks.size() * attackCount);
            if ((skillid > 0) && ((ourAttacks.size() > 0) || (skillid != 4341002)) && (!GameConstants.isNoDelaySkill(skillid)))
            {
                effect.applyTo(chr, chr.getTruePosition());
            }
            else
            {
                c.getSession().write(MaplePacketCreator.enableActions());
            }
        }
        else
        {
            move = false;
            pull = false;
            push = false;
            c.getSession().write(MaplePacketCreator.enableActions());
        }
        chr.getMap().broadcastMessage(MaplePacketCreator.pvpAttack(chr.getId(), chr.getLevel(), trueSkill, trueSkillLevel, speed, fakeMastery, visProjectile, attackCount, chargeTime, animation,
                facingLeft ? 1 : 0, chr.getStat().defRange, skillid, skillLevel, move, push, pull, ourAttacks));
        if ((addedScore > 0) && (GameConstants.getAttackDelay(skillid, SkillFactory.getSkill(skillid)) >= 100))
        {
            CheatTracker tracker = chr.getCheatTracker();
            tracker.setAttacksWithoutHit(true);
            if (tracker.getAttacksWithoutHit() >= 50)
            {
                tracker.registerOffense(client.anticheat.CheatingOffense.ATTACK_WITHOUT_GETTING_HIT, "无敌自动封号.");
            }
        }
    }

    public static boolean inArea(MapleCharacter chr)
    {
        for (Rectangle rect : chr.getMap().getAreas())
        {
            if (rect.contains(chr.getTruePosition()))
            {
                return true;
            }
        }
        for (MapleMist mist : chr.getMap().getAllMistsThreadsafe())
        {
            if ((mist.getOwnerId() == chr.getId()) && (mist.getMistType() == 2) && (mist.getBox().contains(chr.getTruePosition())))
            {
                return true;
            }
        }
        return false;
    }


    public static void LieDetector(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr, boolean isItem)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        String target = slea.readMapleAsciiString();
        byte slot = 0;
        if (isItem)
        {
            if (!chr.getCheatTracker().canLieDetector())
            {
                chr.dropMessage(1, "您已经使用过一次，暂时还无法使用测谎仪道具.");
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            slot = (byte) slea.readShort();
            int itemId = slea.readInt();
            Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
            if ((toUse == null) || (toUse.getQuantity() <= 0) || (toUse.getItemId() != itemId) || (itemId != 2190000))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
        }
        else if (!chr.isIntern())
        {
            chr.getClient().disconnect(true, false);
            c.getSession().close(true);
            return;
        }
        if (((server.maps.FieldLimitType.PotionUse.check(chr.getMap().getFieldLimit())) && (isItem)) || (chr.getMap().getReturnMapId() == chr.getMapId()))
        {
            chr.dropMessage(5, "当前地图无法使用测谎仪.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleCharacter search_chr = chr.getMap().getCharacterByName(target);
        if ((search_chr == null) || (search_chr.getId() == chr.getId()) || ((search_chr.isIntern()) && (!chr.isIntern())))
        {
            chr.dropMessage(1, "未找到角色.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if ((search_chr.getEventInstance() != null) || (search_chr.getMapId() == 180000001))
        {
            chr.dropMessage(5, "当前地图无法使用测谎仪.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (search_chr.getAntiMacro().inProgress())
        {
            c.getSession().write(MaplePacketCreator.LieDetectorResponse((byte) 3));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (((search_chr.getAntiMacro().isPassed()) && (isItem)) || (search_chr.getAntiMacro().getAttempt() == 2))
        {
            c.getSession().write(MaplePacketCreator.LieDetectorResponse((byte) 2));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (!search_chr.getAntiMacro().startLieDetector(chr.getName(), isItem, false))
        {
            chr.dropMessage(5, "使用测谎仪失败.");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (isItem)
        {
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, false);
        }
        search_chr.dropMessage(5, chr.getName() + " 对你使用了测谎仪.");
    }

    public static void LieDetectorResponse(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null))
        {
            return;
        }
        String answer = slea.readMapleAsciiString();
        MapleLieDetector ld = c.getPlayer().getAntiMacro();
        if ((!ld.inProgress()) || ((ld.isPassed()) && (ld.getLastType() == 0)) || (ld.getAnswer() == null) || (answer.length() <= 0))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (answer.equalsIgnoreCase(ld.getAnswer()))
        {
            MapleCharacter search_chr = c.getPlayer().getMap().getCharacterByName(ld.getTester());
            if ((search_chr != null) && (search_chr.getId() != c.getPlayer().getId()))
            {
                search_chr.dropMessage(5, c.getPlayer().getName() + " 通过了测谎仪的检测.");
            }
            c.getSession().write(MaplePacketCreator.LieDetectorResponse((byte) 12, (byte) 1));
            c.getPlayer().gainMeso(5000, true);
            ld.end();
            handling.world.WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                    "[GM Message] 玩家: " + c.getPlayer().getName() + " (等级 " + c.getPlayer().getLevel() + ") 通过了测谎仪检测。"));
        }
        else if (ld.getAttempt() < 3)
        {
            ld.startLieDetector(ld.getTester(), ld.getLastType() == 0, true);
        }
        else
        {
            MapleCharacter search_chr = c.getPlayer().getMap().getCharacterByName(ld.getTester());
            if ((search_chr != null) && (search_chr.getId() != c.getPlayer().getId()))
            {
                search_chr.dropMessage(5, c.getPlayer().getName() + " 没用通过测谎仪的检测，恭喜你获得7000的金币.");
                search_chr.gainMeso(7000, true);
            }
            ld.end();
            c.getPlayer().getClient().getSession().write(MaplePacketCreator.LieDetectorResponse((byte) 10, (byte) 4));
            MapleMap map = c.getChannelServer().getMapFactory().getMap(180000001);
            c.getPlayer().getQuestNAdd(server.quest.MapleQuest.getInstance(123456)).setCustomData(String.valueOf(1800));
            c.getPlayer().changeMap(map, map.getPortal(0));
            handling.world.WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                    "[GM Message] 玩家: " + c.getPlayer().getName() + " (等级 " + c.getPlayer().getLevel() + ") 未通过测谎仪检测，系统将其监禁30分钟！"));
        }
    }

    public static void LieDetectorRefresh(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null))
        {
            return;
        }
        MapleLieDetector ld = c.getPlayer().getAntiMacro();
        if (ld.getAttempt() < 3)
        {
            ld.startLieDetector(ld.getTester(), ld.getLastType() == 0, true);
        }
        else
        {
            ld.end();
            c.getPlayer().getClient().getSession().write(MaplePacketCreator.LieDetectorResponse((byte) 10, (byte) 4));
            MapleMap map = c.getChannelServer().getMapFactory().getMap(180000001);
            c.getPlayer().getQuestNAdd(server.quest.MapleQuest.getInstance(123456)).setCustomData(String.valueOf(1800));
            c.getPlayer().changeMap(map, map.getPortal(0));
            handling.world.WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                    "[GM Message] 玩家: " + c.getPlayer().getName() + " (等级 " + c.getPlayer().getLevel() + ") 未通过测谎仪检测，系统将其监禁30分钟！"));
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\PlayersHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */