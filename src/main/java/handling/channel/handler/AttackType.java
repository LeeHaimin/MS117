package handling.channel.handler;

public enum AttackType
{
    NON_RANGED, RANGED, RANGED_WITH_SHADOWPARTNER, NON_RANGED_WITH_MIRROR;

    AttackType()
    {
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\AttackType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */