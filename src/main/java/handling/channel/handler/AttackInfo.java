package handling.channel.handler;

import java.awt.Point;
import java.util.List;

import client.MapleCharacter;
import client.Skill;
import client.SkillFactory;
import constants.GameConstants;
import server.MapleStatEffect;

public class AttackInfo
{
    public int skillId;
    public int charge;
    public int lastAttackTickCount;
    public List<tools.AttackPair> allDamage;
    public Point position;
    public Point skillposition = null;
    public int display;
    public int direction;
    public int stance;
    public int maxDamageOver = 999999;
    public short starSlot;
    public short cashSlot;
    public byte numDamage;
    public byte numAttacked;
    public byte numAttackedAndDamage;
    public byte speed;
    public byte AOE;
    public byte unk;
    public byte zeroUnk;
    public List<server.movement.LifeMovementFragment> movei;
    public boolean real = true;
    public boolean move = false;
    public boolean isCloseRangeAttack = false;
    public boolean isRangedAttack = false;
    public boolean isMagicAttack = false;

    public MapleStatEffect getAttackEffect(MapleCharacter chr, int skillLevel, Skill theSkill)
    {
        if ((GameConstants.isMulungSkill(this.skillId)) || (GameConstants.isPyramidSkill(this.skillId)) || (GameConstants.isInflationSkill(this.skillId)))
        {
            skillLevel = 1;
        }
        else if (skillLevel <= 0)
        {
            return null;
        }
        if (GameConstants.isLinkedAttackSkill(this.skillId))
        {
            Skill skillLink = SkillFactory.getSkill(this.skillId);
            return skillLink.getEffect(skillLevel);
        }
        if (chr.isAdmin())
        {
        }


        return theSkill.getEffect(skillLevel);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\AttackInfo.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */