package handling.channel.handler;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePotionPot;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.MapleStatEffect;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.InventoryPacket;


public class PotionPotHandler
{
    public static void PotionPotUse(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.getPotionPot() == null) || (!chr.isAlive()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        long time = System.currentTimeMillis();
        if (chr.getNextConsume() > time)
        {
            c.getSession().write(InventoryPacket.showPotionPotMsg(0, 8));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }

        slea.skip(4);
        short slot = slea.readShort();
        int itemId = slea.readInt();
        Item item = chr.getInventory(MapleInventoryType.CASH).getItem(slot);
        if ((item == null) || (item.getItemId() != itemId) || (itemId / 10000 != 582))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MaplePotionPot potionPot = chr.getPotionPot();
        int potHp = potionPot.getHp();
        int potMp = potionPot.getMp();
        if ((potHp == 0) && (potMp == 0))
        {
            c.getSession().write(InventoryPacket.updataPotionPot(potionPot));
            c.getSession().write(InventoryPacket.showPotionPotMsg(0, 6));
            return;
        }
        boolean usePot = false;
        int healHp = chr.getStat().getHealHp();
        int healMp = chr.getStat().getHealMp(chr.getJob());
        int usePotHp = potHp >= healHp ? healHp : potHp;
        int usePotMp = potMp >= healMp ? healMp : potMp;
        if (usePotHp > 0)
        {
            chr.addHP(usePotHp);
            potionPot.setHp(potHp - usePotHp);
            usePot = true;
        }
        if (usePotMp > 0)
        {
            chr.addMP(usePotMp);
            potionPot.setMp(potMp - usePotMp);
            usePot = true;
        }
        if ((usePot) && (chr.getMap().getConsumeItemCoolTime() > 0))
        {
            chr.setNextConsume(time + chr.getMap().getConsumeItemCoolTime() * 1000);
        }
        c.getSession().write(InventoryPacket.updataPotionPot(potionPot));
        c.getSession().write(InventoryPacket.showPotionPotMsg(usePot ? 1 : 0, 0));
    }


    public static void PotionPotAdd(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.getPotionPot() == null) || (!chr.isAlive()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }


        slea.skip(4);
        short slot = slea.readShort();
        int itemId = slea.readInt();
        short quantity = (short) slea.readInt();
        Item toUse = chr.getInventory(MapleInventoryType.USE).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId) || (toUse.getQuantity() < quantity))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MaplePotionPot potionPot = chr.getPotionPot();
        MapleStatEffect itemEffect = MapleItemInformationProvider.getInstance().getItemEffect(itemId);
        if (itemEffect != null)
        {
            int addHp = itemEffect.getHp();
            if (itemEffect.getHpR() > 0.0D)
            {
                int hp = chr.getStat().getCurrentMaxHp();
                if (hp > 100000)
                {
                    hp = (int) (itemEffect.getHpR() * 100000.0D) - 1;
                }
                else
                {
                    hp = (int) (itemEffect.getHpR() * hp);
                }
                addHp += hp;
            }
            addHp = (int) (addHp * 1.2D);

            int addMp = itemEffect.getMp();
            if (itemEffect.getMpR() > 0.0D)
            {
                int mp = chr.getStat().getCurrentMaxMp(chr.getJob());
                if (mp > 100000)
                {
                    mp = (int) (itemEffect.getMpR() * 100000.0D) - 1;
                }
                else
                {
                    mp = (int) (itemEffect.getMpR() * mp);
                }
                addMp += mp;
            }
            addMp = (int) (addMp * 1.2D);
            if ((addHp <= 0) && (addMp <= 0))
            {
                c.getSession().write(InventoryPacket.updataPotionPot(potionPot));
                c.getSession().write(InventoryPacket.showPotionPotMsg(0, 0));
                return;
            }
            if (potionPot.isFull(addHp, addMp))
            {
                c.getSession().write(InventoryPacket.updataPotionPot(potionPot));
                c.getSession().write(InventoryPacket.showPotionPotMsg(0, 2));
                return;
            }

            boolean isFull = false;
            short useQuantity = 0;
            while (!isFull)
            {
                potionPot.addHp(addHp > 0 ? addHp : 0);
                potionPot.addMp(addMp > 0 ? addMp : 0);
                useQuantity = (short) (useQuantity + 1);

                isFull = (potionPot.isFull(addHp, addMp)) || (useQuantity == quantity);
            }
            if (useQuantity > 0)
            {
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, useQuantity, false);
            }
            c.getSession().write(InventoryPacket.updataPotionPot(potionPot));
            c.getSession().write(InventoryPacket.showPotionPotMsg(2));
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void PotionPotMode(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.getPotionPot() == null))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void PotionPotIncr(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.getPotionPot() == null))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }

        slea.skip(4);
        slea.skip(1);
        int itemId = slea.readInt();
        short slot = slea.readShort();
        Item toUse = chr.getInventory(MapleInventoryType.CASH).getItem(slot);
        if ((toUse == null) || (toUse.getQuantity() < 1) || (toUse.getItemId() != itemId) || (itemId != 5821000))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MaplePotionPot potionPot = chr.getPotionPot();
        boolean useItem = potionPot.addMaxValue();
        if (useItem)
        {
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.CASH, slot, (short) 1, false);
        }
        c.getSession().write(InventoryPacket.updataPotionPot(potionPot));
        c.getSession().write(InventoryPacket.showPotionPotMsg(useItem ? 3 : 0, useItem ? 0 : 3));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\PotionPotHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */