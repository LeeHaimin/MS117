package handling.channel.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.Skill;
import handling.world.WorldAllianceService;
import handling.world.WorldGuildService;
import handling.world.guild.MapleGuild;
import server.MapleStatEffect;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.GuildPacket;

public class GuildHandler
{
    private static final Map<String, Pair<Integer, Long>> invited = new HashMap<>();
    private static final List<Integer> ApplyIDs = new ArrayList<>();
    private static long nextPruneTime = System.currentTimeMillis() + 60000L;

    public static void DenyGuildRequest(String from, MapleClient c)
    {
        MapleCharacter cfrom = c.getChannelServer().getPlayerStorage().getCharacterByName(from);
        if ((cfrom != null) && (invited.remove(c.getPlayer().getName().toLowerCase()) != null))
        {
            cfrom.getClient().getSession().write(GuildPacket.denyGuildInvitation(c.getPlayer().getName()));
        }
    }


    public static void GuildApply(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (c.getPlayer().getGuildId() > 0)
        {
            c.getPlayer().dropMessage(1, "您已经有家族了，无需重复申请.");
            return;
        }
        if (ApplyIDs.contains(c.getPlayer().getId()))
        {
            c.getPlayer().dropMessage(1, "您已经在家族申请列表中，暂时无法进行此操作.");
            return;
        }
        int guildId = slea.readInt();
        handling.world.guild.MapleGuildCharacter guildMember = new handling.world.guild.MapleGuildCharacter(c.getPlayer());
        guildMember.setGuildId(guildId);
        int ret = WorldGuildService.getInstance().addGuildApplyMember(guildMember);
        if (ret == 1)
        {
            ApplyIDs.add(c.getPlayer().getId());
            c.getPlayer().dropMessage(1, "您成功申请加入家族，请等待族长同意.");
        }
        else
        {
            c.getPlayer().dropMessage(1, "申请加入家族出现错误，请稍后再试.");
        }
    }


    public static void AcceptGuildApply(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer().getGuildId() <= 0) || (c.getPlayer().getGuildRank() > 2))
        {
            return;
        }
        int guildId = c.getPlayer().getGuildId();
        byte amount = slea.readByte();


        for (int i = 0; i < amount; i++)
        {
            int fromId = slea.readInt();
            MapleCharacter from = c.getChannelServer().getPlayerStorage().getCharacterById(fromId);

            if ((from != null) && (from.getGuildId() <= 0))
            {
                from.setGuildId(guildId);
                from.setGuildRank((byte) 5);
                int ret = WorldGuildService.getInstance().addGuildMember(from.getMGC());
                if (ret == 0)
                {
                    from.setGuildId(0);
                }
                else
                {
                    from.getClient().getSession().write(GuildPacket.showGuildInfo(from));
                    MapleGuild gs = WorldGuildService.getInstance().getGuild(guildId);
                    for (byte[] pack : WorldAllianceService.getInstance().getAllianceInfo(gs.getAllianceId(), true))
                    {
                        if (pack != null)
                        {
                            from.getClient().getSession().write(pack);
                        }
                    }
                    from.saveGuildStatus();
                    respawnPlayer(c.getPlayer());
                }
            }
            else if (ApplyIDs.contains(fromId))
            {
                ApplyIDs.remove(i);
            }
        }
    }

    private static void respawnPlayer(MapleCharacter chr)
    {
        if (chr.getMap() == null)
        {
            return;
        }
        chr.getMap().broadcastMessage(GuildPacket.loadGuildName(chr));
        chr.getMap().broadcastMessage(GuildPacket.loadGuildIcon(chr));
    }

    public static void DenyGuildApply(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer().getGuildId() <= 0) || (c.getPlayer().getGuildRank() > 2))
        {
            return;
        }
        int guildId = c.getPlayer().getGuildId();
        byte amount = slea.readByte();

        for (int i = 0; i < amount; i++)
        {
            int fromId = slea.readInt();
            WorldGuildService.getInstance().denyGuildApplyMember(guildId, fromId);
            if (ApplyIDs.contains(fromId))
            {
                ApplyIDs.remove(i);
            }
        }
    }

    public static void Guild(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        long currentTime = System.currentTimeMillis();
        if (currentTime >= nextPruneTime)
        {
            Iterator<Map.Entry<String, Pair<Integer, Long>>> itr = invited.entrySet().iterator();

            while (itr.hasNext())
            {
                Map.Entry<String, Pair<Integer, Long>> inv = itr.next();
                if (currentTime >= inv.getValue().right)
                {
                    itr.remove();
                }
            }
            nextPruneTime += 300000L;
        }
        MapleCharacter chr = c.getPlayer();
        byte mode = slea.readByte();
        int guildId, fromId;
        String name;
        Skill skill;
        int newLevel;
        switch (mode)
        {
            case 0:
                c.getSession().write(GuildPacket.showGuildInfo(chr));
                break;
            case 1:
                fromId = slea.readInt();


                MapleCharacter target = c.getChannelServer().getPlayerStorage().getCharacterById(fromId);
                if (target == null)
                {
                    MapleGuild guild = WorldGuildService.getInstance().getGuild(fromId);
                    if (guild == null)
                    {
                        chr.dropMessage(1, "找不到玩家或家族的信息.");
                        return;
                    }
                    c.getSession().write(GuildPacket.showPlayerGuildInfo(guild));
                    return;
                }

                if (target.getGuildId() <= 0)
                {
                    chr.dropMessage(1, "玩家[" + target.getName() + "]没有家族.");
                    return;
                }

                MapleGuild guild = WorldGuildService.getInstance().getGuild(target.getGuildId());
                if (guild == null)
                {
                    chr.dropMessage(1, "玩家[" + target.getName() + "]还没有家族.");
                    return;
                }
                c.getSession().write(GuildPacket.showPlayerGuildInfo(guild));
                break;
            case 4:
                int cost = c.getChannelServer().getCreateGuildCost();
                if ((chr.getGuildId() > 0) || (chr.getMapId() != 200000301))
                {
                    chr.dropMessage(1, "不能创建家族\r\n已经有家族或没在家族中心");
                    return;
                }
                if (chr.getMeso() < cost)
                {
                    chr.dropMessage(1, "你没有足够的金币创建一个家族。当前创建家族需要: " + cost + " 的金币.");
                    return;
                }
                String guildName = slea.readMapleAsciiString();
                if (!isGuildNameAcceptable(guildName))
                {
                    chr.dropMessage(1, "你不能使用这个名字。");
                    return;
                }
                guildId = WorldGuildService.getInstance().createGuild(chr.getId(), guildName);
                if (guildId == 0)
                {
                    chr.dropMessage(1, "创建家族出错\r\n请重试一次.");
                    return;
                }
                chr.gainMeso(-cost, true, true);
                chr.setGuildId(guildId);
                chr.setGuildRank((byte) 1);
                chr.saveGuildStatus();
                chr.finishAchievement(35);
                WorldGuildService.getInstance().setGuildMemberOnline(chr.getMGC(), true, c.getChannel());
                c.getSession().write(GuildPacket.showGuildInfo(chr));
                WorldGuildService.getInstance().gainGP(chr.getGuildId(), 500, chr.getId());
                chr.dropMessage(1, "恭喜你成功创建家族.");
                respawnPlayer(chr);
                break;
            case 7:
                if ((chr.getGuildId() <= 0) || (chr.getGuildRank() > 2))
                {
                    return;
                }
                name = slea.readMapleAsciiString().toLowerCase();
                if (invited.containsKey(name))
                {
                    chr.dropMessage(5, "玩家 " + name + " 已经在邀请的列表，请稍后在试。");
                    return;
                }
                handling.world.guild.MapleGuildResponse mgr = MapleGuild.sendInvite(c, name);
                if (mgr != null)
                {
                    c.getSession().write(mgr.getPacket());
                }
                else
                {
                    invited.put(name, new Pair(chr.getGuildId(), currentTime + 60000L));
                }
                break;
            case 54:
                if (chr.getGuildId() > 0)
                {
                    return;
                }
                guildId = slea.readInt();
                fromId = slea.readInt();
                if (fromId != chr.getId())
                {
                    return;
                }
                name = chr.getName().toLowerCase();
                Pair<Integer, Long> gid = invited.remove(name);
                if ((gid != null) && (guildId == gid.left))
                {
                    chr.setGuildId(guildId);
                    chr.setGuildRank((byte) 5);
                    int ret = WorldGuildService.getInstance().addGuildMember(chr.getMGC());
                    if (ret == 0)
                    {
                        chr.dropMessage(1, "尝试加入的家族成员数已到达最高限制。");
                        chr.setGuildId(0);
                        return;
                    }
                    c.getSession().write(GuildPacket.showGuildInfo(chr));
                    MapleGuild gs = WorldGuildService.getInstance().getGuild(guildId);
                    for (byte[] pack : WorldAllianceService.getInstance().getAllianceInfo(gs.getAllianceId(), true))
                    {
                        if (pack != null)
                        {
                            c.getSession().write(pack);
                        }
                    }
                    chr.saveGuildStatus();
                    respawnPlayer(c.getPlayer());
                }
                break;

            case 11:
                fromId = slea.readInt();
                name = slea.readMapleAsciiString();
                if ((fromId != chr.getId()) || (!name.equals(chr.getName())) || (chr.getGuildId() <= 0))
                {
                    return;
                }
                WorldGuildService.getInstance().leaveGuild(chr.getMGC());
                c.getSession().write(GuildPacket.showGuildInfo(null));
                break;
            case 12:
                fromId = slea.readInt();
                name = slea.readMapleAsciiString();
                if ((chr.getGuildRank() > 2) || (chr.getGuildId() <= 0))
                {
                    return;
                }
                WorldGuildService.getInstance().expelMember(chr.getMGC(), name, fromId);
                break;
            case 18:
                if ((chr.getGuildId() <= 0) || (chr.getGuildRank() != 1))
                {
                    return;
                }
                String[] ranks = new String[5];
                for (int i = 0; i < 5; i++)
                {
                    ranks[i] = slea.readMapleAsciiString();
                }
                WorldGuildService.getInstance().changeRankTitle(chr.getGuildId(), ranks);
                break;
            case 19:
                fromId = slea.readInt();
                byte newRank = slea.readByte();
                if ((newRank <= 1) || (newRank > 5) || (chr.getGuildRank() > 2) || ((newRank <= 2) && (chr.getGuildRank() != 1)) || (chr.getGuildId() <= 0))
                {
                    return;
                }
                WorldGuildService.getInstance().changeRank(chr.getGuildId(), fromId, newRank);
                break;
            case 20:
                if ((chr.getGuildId() <= 0) || (chr.getGuildRank() != 1))
                {
                    return;
                }
                if (chr.getMeso() < 1500000)
                {
                    chr.dropMessage(1, "金币不足。");
                    return;
                }
                short bg = slea.readShort();
                byte bgcolor = slea.readByte();
                short logo = slea.readShort();
                byte logocolor = slea.readByte();
                WorldGuildService.getInstance().setGuildEmblem(chr.getGuildId(), bg, bgcolor, logo, logocolor);
                chr.gainMeso(-1500000, true, true);
                respawnPlayer(c.getPlayer());
                break;
            case 49:
                String notice = slea.readMapleAsciiString();
                if ((notice.length() > 100) || (chr.getGuildId() <= 0) || (chr.getGuildRank() > 2))
                {
                    return;
                }
                WorldGuildService.getInstance().setGuildNotice(chr.getGuildId(), notice);
                break;
            case 35:
                int skillId = slea.readInt();
                byte level = slea.readByte();
                if (skillId > 0)
                {
                    chr.dropMessage(1, "当前暂不支持家族技能升级.");
                    return;
                }
                skill = client.SkillFactory.getSkill(skillId);
                if ((chr.getGuildId() <= 0) || (skill == null) || (skill.getId() < 91000000))
                {
                    return;
                }

                newLevel = WorldGuildService.getInstance().getSkillLevel(chr.getGuildId(), skill.getId()) + level;
                if (newLevel > skill.getMaxLevel())
                {
                    return;
                }
                MapleStatEffect skillid = skill.getEffect(newLevel);
                if ((skillid.getReqGuildLevel() <= 0) || (chr.getMeso() < skillid.getPrice()))
                {
                    return;
                }
                if (WorldGuildService.getInstance().purchaseSkill(chr.getGuildId(), skillid.getSourceId(), chr.getName(), chr.getId()))
                {
                    chr.gainMeso(-skillid.getPrice(), true);
                }
                break;
            case 62:
                skill = client.SkillFactory.getSkill(slea.readInt());
                if ((c.getPlayer().getGuildId() <= 0) || (skill == null))
                {
                    return;
                }
                newLevel = WorldGuildService.getInstance().getSkillLevel(chr.getGuildId(), skill.getId());
                if (newLevel <= 0)
                {
                    return;
                }
                MapleStatEffect skillii = skill.getEffect(newLevel);
                if ((skillii.getReqGuildLevel() < 0) || (chr.getMeso() < skillii.getExtendPrice()))
                {
                    return;
                }
                if (WorldGuildService.getInstance().activateSkill(chr.getGuildId(), skillii.getSourceId(), chr.getName()))
                {
                    chr.gainMeso(-skillii.getExtendPrice(), true);
                }
                break;
            case 40:
                fromId = slea.readInt();
                if ((chr.getGuildId() <= 0) || (chr.getGuildRank() > 1))
                {
                    return;
                }
                WorldGuildService.getInstance().setGuildLeader(chr.getGuildId(), fromId);
                break;
            case 44:
                if (chr.getGuildId() <= 0)
                {
                    return;
                }
                c.getSession().write(GuildPacket.showGuildBeginnerSkill());
                break;
            case 45:
                break;
            case 2:
            case 3:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 36:
            case 37:
            case 38:
            case 39:
            case 41:
            case 42:
            case 43:
            case 46:
            case 47:
            case 48:
            case 50:
            case 51:
            case 52:
            case 53:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            default:
                System.out.println("未知家族操作类型: ( 0x" + tools.StringUtil.getLeftPaddedStr(Integer.toHexString(mode).toUpperCase(), '0', 2) + " )" + slea.toString());
        }
    }

    private static boolean isGuildNameAcceptable(String name)
    {
        return (name.getBytes().length >= 3) && (name.getBytes().length <= 12);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\GuildHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */