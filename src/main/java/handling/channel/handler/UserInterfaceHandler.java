package handling.channel.handler;

import client.MapleClient;
import scripting.event.EventManager;
import scripting.npc.NPCScriptManager;
import tools.data.input.SeekableLittleEndianAccessor;

public class UserInterfaceHandler
{
    public static void CygnusSummon_NPCRequest(MapleClient c)
    {
        if (c.getPlayer().getJob() == 2000)
        {
            NPCScriptManager.getInstance().start(c, 1202000);
        }
        else if (c.getPlayer().getJob() == 1000)
        {
            NPCScriptManager.getInstance().start(c, 1101008);
        }
    }

    public static void InGame_Poll(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (constants.ServerConstants.PollEnabled)
        {
            c.getPlayer().updateTick(slea.readInt());
            int selection = slea.readInt();
            if ((selection >= 0) && (selection <= constants.ServerConstants.Poll_Answers.length) && (client.MapleCharacterUtil.SetPoll(c.getAccID(), selection)))
            {
                c.getSession().write(tools.MaplePacketCreator.getPollReply("Thank you."));
            }
        }
    }


    public static void ShipObjectRequest(int mapid, MapleClient c)
    {
        EventManager em;
        int effect = 3;
        switch (mapid)
        {
            case 101000300:
            case 200000111:
                em = c.getChannelServer().getEventSM().getEventManager("Boats");
                if ((em != null) && (em.getProperty("docked").equals("true")))
                {
                    effect = 1;
                }
                break;
            case 200000121:
            case 220000110:
                em = c.getChannelServer().getEventSM().getEventManager("Trains");
                if ((em != null) && (em.getProperty("docked").equals("true")))
                {
                    effect = 1;
                }
                break;
            case 200000151:
            case 260000100:
                em = c.getChannelServer().getEventSM().getEventManager("Geenie");
                if ((em != null) && (em.getProperty("docked").equals("true")))
                {
                    effect = 1;
                }
                break;
            case 200000131:
            case 240000110:
                em = c.getChannelServer().getEventSM().getEventManager("Flight");
                if ((em != null) && (em.getProperty("docked").equals("true")))
                {
                    effect = 1;
                }
                break;
            case 200090000:
            case 200090010:
                em = c.getChannelServer().getEventSM().getEventManager("Boats");
                if ((em != null) && (em.getProperty("haveBalrog").equals("true")))
                {
                    effect = 1;
                }
                else
                {
                    return;
                }
                break;
            default:
                System.out.println("Unhandled ship object, MapID : " + mapid);
        }

        c.getSession().write(tools.MaplePacketCreator.boatPacket(effect));
    }
}