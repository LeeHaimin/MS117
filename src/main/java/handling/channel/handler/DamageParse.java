package handling.channel.handler;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.PlayerStats;
import client.Skill;
import client.SkillFactory;
import client.anticheat.CheatTracker;
import client.anticheat.CheatingOffense;
import client.inventory.Item;
import client.status.MonsterStatus;
import client.status.MonsterStatusEffect;
import constants.GameConstants;
import handling.world.WorldBroadcastService;
import server.AutobanManager;
import server.MapleStatEffect;
import server.Randomizer;
import server.life.Element;
import server.life.MapleMonster;
import server.life.MapleMonsterStats;
import server.maps.MapleMap;
import server.maps.MapleMapItem;
import server.maps.pvp.MaplePvp;
import tools.AttackPair;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;

public class DamageParse
{
    private static final Logger log = Logger.getLogger(DamageParse.class);

    public static void applyAttack(AttackInfo attack, Skill theSkill, MapleCharacter player, int attackCount, double maxDamagePerMonster, MapleStatEffect effect, AttackType attack_type,
                                   int visProjectile)
    {
        if (!player.isAlive())
        {
            player.getCheatTracker().registerOffense(CheatingOffense.ATTACKING_WHILE_DEAD, "操作者已死亡.");
            return;
        }
        if (player.isBanned())
        {
            return;
        }
        if ((attack.real) && (GameConstants.getAttackDelay(attack.skillId, theSkill) >= 50))
        {
            player.getCheatTracker().checkAttack(attack.skillId, attack.lastAttackTickCount);
        }
        if (attack.skillId != 0)
        {
            if (effect == null)
            {
                player.getClient().getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if (GameConstants.isMulungSkill(attack.skillId))
            {
                if (player.getMapId() / 10000 != 92502)
                {
                    return;
                }
                if (player.getMulungEnergy() < 10000)
                {
                    return;
                }
                player.mulung_EnergyModify(false);
            }
            else if (GameConstants.isPyramidSkill(attack.skillId))
            {
                if (player.getMapId() / 1000000 != 926)
                {
                    return;
                }
                if ((player.getPyramidSubway() != null) && (player.getPyramidSubway().onSkillUse(player)))
                {
                }


            }
            else if (GameConstants.isInflationSkill(attack.skillId))
            {
                if (player.getBuffedValue(MapleBuffStat.GIANT_POTION) != null)
                {
                }

            }
            else if ((attack.numAttacked > effect.getMobCount(player)) && (attack.skillId != 1220010) && (attack.skillId != 32121003))
            {
                player.getCheatTracker().registerOffense(CheatingOffense.MISMATCHING_BULLETCOUNT, "异常的攻击次数.");
                if (player.isShowPacket())
                {
                    player.dropMessage(-5, "物理怪物数量检测 => 封包解析次数: " + attack.numAttacked + " 服务端设置次数: " + effect.getMobCount(player));
                }
                return;
            }
        }
        if ((player.getClient().getChannelServer().isAdminOnly()) && (player.isAdmin()))
        {
            player.dropMessage(-1, "攻击动作: " + Integer.toHexString(attack.display));
        }
        if (player.getStatForBuff(MapleBuffStat.光暗转换) != null)
        {
            attackCount *= 2;
        }
        boolean useAttackCount =
                (attack.skillId != 4211006) && (attack.skillId != 3221007) && (attack.skillId != 23121003) && (attack.skillId != 24100003) && (attack.skillId != 24120002) && (attack.skillId != 4120019) && (attack.skillId != 4100012);
        if ((attack.numDamage > attackCount) && (useAttackCount))
        {
            if (player.isShowPacket())
            {
                player.dropMessage(-5, "物理攻击次数检测 => 封包解析次数: " + attack.numDamage + " 服务端设置次数: " + attackCount);
            }

            player.getCheatTracker().registerOffense(CheatingOffense.MISMATCHING_BULLETCOUNT, "异常的攻击次数.");
            log.info("[作弊] " + player.getName() + " 物理攻击次数异常。 attack.hits " + attack.numDamage + " attackCount " + attackCount + " 技能ID " + attack.skillId);
            WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6, "[GM Message] " + player.getName() + " ID: " + player.getId() + " (等级 " + player.getLevel() +
                    ") 物理攻击次数异常。 attack.hits " + attack.numDamage + " attackCount " + attackCount + " 技能ID " + attack.skillId));
            return;
        }

        if ((attack.numDamage > 0) && (attack.numAttacked > 0))
        {
            if (!player.getStat().checkEquipDurabilitys(player, -1))
            {
                player.dropMessage(5, "An item has run out of durability but has no inventory room to go to.");
                return;
            }
        }
        int totDamage = 0;
        MapleMap map = player.getMap();
        Point Original_Pos = player.getPosition();

        if (map.isPvpMap())
        {
            MaplePvp.doPvP(player, map, attack, effect);
        }
        else if (map.isPartyPvpMap())
        {
            MaplePvp.doPartyPvP(player, map, attack, effect);
        }
        else if (map.isGuildPvpMap())
        {
            MaplePvp.doGuildPvP(player, map, attack, effect);
        }

        if (attack.skillId == 4211006)
        {
            for (AttackPair oned : attack.allDamage)
                if (oned.attack == null)
                {

                    server.maps.MapleMapObject mapobject = map.getMapObject(oned.objectid, server.maps.MapleMapObjectType.ITEM);
                    if (mapobject != null)
                    {
                        MapleMapItem mapitem = (MapleMapItem) mapobject;
                        mapitem.getLock().lock();
                        try
                        {
                            if (mapitem.getMeso() > 0)
                            {
                                if (mapitem.isPickedUp())
                                {
                                    return;
                                }
                                map.removeMapObject(mapitem);
                                map.broadcastMessage(tools.packet.InventoryPacket.explodeDrop(mapitem.getObjectId()));
                                mapitem.setPickedUp(true);
                            }
                            else
                            {
                                player.getCheatTracker().registerOffense(CheatingOffense.ETC_EXPLOSION, "异常的技能效果.");
                                return;
                            }
                        }
                        finally
                        {
                            mapitem.getLock().unlock();
                        }
                    }
                    else
                    {
                        player.getCheatTracker().registerOffense(CheatingOffense.EXPLODING_NONEXISTANT, "异常的技能效果.");
                        return;
                    }
                }
        }
        int totDamageToOneMonster = 0;
        long hpMob = 0L;
        PlayerStats stats = player.getStat();

        int criticalDamage = stats.passive_sharpeye_percent();
        int shdowPartnerAttackPercentage = 0;
        if ((attack_type == AttackType.RANGED_WITH_SHADOWPARTNER) || (attack_type == AttackType.NON_RANGED_WITH_MIRROR))
        {
            MapleStatEffect shadowPartnerEffect = player.getStatForBuff(MapleBuffStat.影分身);
            if (shadowPartnerEffect != null)
            {
                shdowPartnerAttackPercentage += shadowPartnerEffect.getShadowDamage();
            }
            attackCount /= 2;
        }
        shdowPartnerAttackPercentage *= (criticalDamage + 100) / 100;
        if ((attack.skillId == 4221014) || (attack.skillId == 4221016))
        {
            shdowPartnerAttackPercentage *= 30;
        }

        int maxDamagePerHit = 0;


        int maxMaxDamageOver = 0;

        for (AttackPair oned : attack.allDamage)
        {
            MapleMonster monster = map.getMonsterByOid(oned.objectid);
            if ((monster != null) && (monster.getLinkCID() <= 0))
            {
                totDamageToOneMonster = 0;
                hpMob = monster.getMobMaxHp();
                MapleMonsterStats monsterstats = monster.getStats();
                int fixeddmg = monsterstats.getFixedDamage();
                boolean Tempest = (monster.getStatusSourceID(MonsterStatus.结冰) == 21120006) || (attack.skillId == 21120006) || (attack.skillId == 1221011);
                if (!Tempest)
                {


                    if ((!monster.isBuffed(MonsterStatus.免疫伤害)) && (!monster.isBuffed(MonsterStatus.免疫魔攻)) && (!monster.isBuffed(MonsterStatus.反射魔攻)))
                    {
                        maxDamagePerHit = CalculateMaxWeaponDamagePerHit(player, monster, attack, theSkill, effect, maxDamagePerMonster, criticalDamage);
                    }
                    else
                    {
                        maxDamagePerHit = CalculateMaxWeaponDamagePerHit(player, monster, attack, theSkill, effect, maxDamagePerMonster, criticalDamage);
                    }
                }

                byte overallAttackCount = 0;

                for (Pair<Integer, Boolean> integerBooleanPair : oned.attack)
                {
                    Pair eachde = integerBooleanPair;
                    Integer eachd = (Integer) eachde.left;
                    overallAttackCount = (byte) (overallAttackCount + 1);
                    if ((useAttackCount) && (overallAttackCount - 1 == attackCount))
                    {
                        maxDamagePerHit =
                                (int) (maxDamagePerHit / 100 * (shdowPartnerAttackPercentage * (monsterstats.isBoss() ? stats.getBossDamageRate(attack.skillId) : stats.getDamageRate()) / 100.0D));
                    }
                    if ((player.isShowPacket()) && (eachd > 0))
                    {
                        player.dropMessage(-1, "物理攻击打怪伤害 : " + eachd + " 服务端预计伤害 : " + maxDamagePerHit + " 是否超过 : " + (eachd > maxDamagePerHit));
                    }

                    if (fixeddmg != -1)
                    {
                        if (monsterstats.getOnlyNoramlAttack())
                        {
                            eachd = attack.skillId != 0 ? 0 : fixeddmg;
                        }
                        else
                        {
                            eachd = fixeddmg;
                        }
                    }
                    else if (monsterstats.getOnlyNoramlAttack())
                    {
                        eachd = attack.skillId != 0 ? 0 : Math.min(eachd, maxDamagePerHit);
                    }
                    else if (!player.isGM())
                    {
                        if (Tempest)
                        {
                            if (eachd > monster.getMobMaxHp())
                            {
                                eachd = (int) Math.min(monster.getMobMaxHp(), 2147483647L);
                                player.getCheatTracker().registerOffense(CheatingOffense.HIGH_DAMAGE, "攻击伤害过高.");
                            }
                        }
                        else if (((player.getJob() >= 3200) && (player.getJob() <= 3212) && (!monster.isBuffed(MonsterStatus.免疫伤害)) && (!monster.isBuffed(MonsterStatus.免疫魔攻)) && (!monster.isBuffed(MonsterStatus.反射魔攻))) || (attack.skillId == 23121003) || (

                                ((player.getJob() < 3200) || (player.getJob() > 3212)) && (!monster.isBuffed(MonsterStatus.免疫伤害)) && (!monster.isBuffed(MonsterStatus.免疫物攻)) && (!monster.isBuffed(MonsterStatus.反射物攻))))
                        {
                            if ((eachd > maxDamagePerHit) && (maxDamagePerHit > 2))
                            {
                                player.getCheatTracker().registerOffense(CheatingOffense.HIGH_DAMAGE,
                                        "[伤害: " + eachd + ", 预计伤害: " + maxDamagePerHit + ", 怪物ID: " + monster.getId() + "] [职业: " + player.getJob() + ", 等级: " + player.getLevel() + ", 技能: " + attack.skillId + "]");
                                if (attack.real)
                                {
                                    player.getCheatTracker().checkSameDamage(eachd, maxDamagePerHit);
                                }
                                if ((eachd > maxDamagePerHit * 3) && (attack.skillId != 4001344))
                                {
                                    StringBuilder banReason =
                                            new StringBuilder(player.getName() + " 被系统封号.[异常攻击伤害值: " + eachd + ", 预计伤害: " + maxDamagePerHit + ", 怪物ID: " + monster.getId() + "] " + "[职业: " + player.getJob() + ", 等级: " + player.getLevel() + ", 技能: " + attack.skillId + "]");
                                    if ((player.getLevel() < 10) && (eachd >= 10000))
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    if ((player.getLevel() < 20) && (eachd >= 20000))
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    if ((player.getLevel() < 30) && (eachd >= 40000))
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    if ((player.getLevel() < 50) && (eachd >= 60000))
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    if ((player.getLevel() < 70) && (eachd >= 399999) && (attack.skillId != 3221007))
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    if ((player.getLevel() < 150) && (eachd >= 599999) && (attack.skillId != 3221007))
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    if (eachd > player.getMaxDamageOver(attack.skillId))
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    if (eachd > maxDamagePerHit * 5)
                                    {
                                        AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                        return;
                                    }
                                    player.getCheatTracker().registerOffense(CheatingOffense.HIGH_DAMAGE_2,
                                            "[伤害: " + eachd + ", 预计伤害: " + maxDamagePerHit + ", 怪物ID: " + monster.getId() + "] [职业: " + player.getJob() + ", 等级: " + player.getLevel() + ", 技能: " + attack.skillId + "]");
                                }
                            }
                        }
                        else if (eachd > maxDamagePerHit)
                        {
                            eachd = maxDamagePerHit;
                        }
                    }


                    totDamageToOneMonster += eachd;

                    if (((eachd == 0) || (monster.getId() == 9700021)) && (player.getPyramidSubway() != null)) player.getPyramidSubway().onMiss(player);
                }
                Pair<Integer, Boolean> eachde;
                StringBuilder banReason;
                totDamage += totDamageToOneMonster;
                player.checkMonsterAggro(monster);
                if ((GameConstants.getAttackDelay(attack.skillId, theSkill) >= 50) && (!GameConstants.isNoDelaySkill(attack.skillId)) && (!GameConstants.is不检测范围(attack.skillId)) && (!monster.getStats().isBoss()) && (player.getTruePosition().distanceSq(monster.getTruePosition()) > GameConstants.getAttackRange(effect, player.getStat().defRange)) && (player.getMapId() != 703002000))
                {
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                            "[GM Message] " + player.getName() + " ID: " + player.getId() + " (等级 " + player.getLevel() + ") 攻击范围异常。职业: " + player.getJob() + " 技能: " + attack.skillId + " [范围: " + player.getTruePosition().distanceSq(monster.getTruePosition()) + " 预期: " + GameConstants.getAttackRange(effect, player.getStat().defRange) + "]"));
                    player.getCheatTracker().registerOffense(CheatingOffense.ATTACK_FARAWAY_MONSTER,
                            "[范围: " + player.getTruePosition().distanceSq(monster.getTruePosition()) + ", 预期范围: " + GameConstants.getAttackRange(effect, player.getStat().defRange) + "] [职业: " + player.getJob() + " 技能: " + attack.skillId + " ]");
                }


                if (player.getBuffedValue(MapleBuffStat.敛财术) != null)
                {
                    switch (attack.skillId)
                    {
                        case 0:
                        case 4001334:
                        case 4201012:
                        case 4211002:
                        case 4211011:
                        case 4221007:
                        case 4221010:
                            handlePickPocket(player, monster, oned);
                    }

                }
                if ((totDamageToOneMonster > 0) || (attack.skillId == 1221011) || (attack.skillId == 21120006))
                {
                    if ((player.getJob() == 3001) || (player.getJob() == 3100) || (player.getJob() == 3110) || (player.getJob() == 3111) || (player.getJob() == 3112))
                    {
                        player.handleForceGain(monster.getObjectId(), attack.skillId);
                    }
                    else if ((player.getJob() == 410) || (player.getJob() == 411) || (player.getJob() == 412))
                    {
                        if ((attack.skillId != 0) && (attack.skillId != 4100012) && (attack.skillId != 4120019) && (visProjectile > 0))
                        {
                            player.handleAssassinStack(monster, visProjectile);
                        }
                    }
                    else if (player.getJob() == 422)
                    {
                        player.handleKillSpreeGain();
                    }
                    else if ((player.getJob() == 1500) || (player.getJob() == 1510) || (player.getJob() == 1511) || (player.getJob() == 1512))
                    {
                        player.handle元素雷电(attack.skillId);
                    }
                    if (attack.skillId != 1221011)
                    {
                        monster.damage(player, totDamageToOneMonster, true, attack.skillId);
                    }
                    else
                    {
                        monster.damage(player, monster.getStats().isBoss() ? 500000L : monster.getHp() - 1L, true, attack.skillId);
                    }
                    if (monster.isBuffed(MonsterStatus.反射物攻))
                    {
                        player.addHP(-(7000 + Randomizer.nextInt(8000)));
                    }
                    player.onAttack(monster.getMobMaxHp(), monster.getMobMaxMp(), attack.skillId, monster.getObjectId(), totDamage);
                    switch (attack.skillId)
                    {
                        case 4001334:
                        case 4001344:
                        case 4101008:
                        case 4101010:
                        case 4111010:
                        case 4111015:
                        case 4121013:
                        case 4201012:
                        case 4211002:
                        case 4211011:
                        case 4221007:
                        case 4221010:
                        case 4221014:
                        case 4311002:
                        case 4311003:
                        case 4321004:
                        case 4321006:
                        case 4331000:
                        case 4331006:
                        case 4341002:
                        case 4341004:
                        case 4341009:
                        case 14001004:
                        case 14101008:
                        case 14101009:
                        case 14111005:
                        case 14111008:
                            int[] skills = {4110011, 4210010, 4320005, 14110004};
                            for (int i = 0; i < skills.length; i++)
                            {
                                Skill skill = SkillFactory.getSkill(skills[i]);
                                if (player.getTotalSkillLevel(skill) > 0)
                                {
                                    MapleStatEffect venomEffect = skill.getEffect(player.getTotalSkillLevel(skill));
                                    if (!venomEffect.makeChanceResult()) break;
                                    monster.applyStatus(player, new MonsterStatusEffect(MonsterStatus.中毒, 1, i, null, false), true, venomEffect.getDuration(), true, venomEffect);
                                    break;
                                }
                            }
                            break;
                        case 4201004:
                            monster.handleSteal(player);
                            break;
                        case 21000002:
                        case 21000004:
                        case 21100001:
                        case 21100007:
                        case 21101011:
                        case 21110002:
                        case 21110007:
                        case 21110008:
                        case 21111013:
                        case 21111014:
                        case 21120006:
                        case 21120009:
                        case 21120010:
                        case 21121013:
                            if ((player.getBuffedValue(MapleBuffStat.属性攻击) != null) && (!monster.getStats().isBoss()))
                            {
                                MapleStatEffect eff = player.getStatForBuff(MapleBuffStat.属性攻击);
                                if (eff != null)
                                {
                                    monster.applyStatus(player, new MonsterStatusEffect(MonsterStatus.速度, eff.getX(), eff.getSourceId(), null, false), false, eff.getY() * 1000, true, eff);
                                }
                            }
                            if ((player.getBuffedValue(MapleBuffStat.战神抗压) != null) && (!monster.getStats().isBoss()))
                            {
                                MapleStatEffect eff = player.getStatForBuff(MapleBuffStat.战神抗压);
                                if ((eff != null) && (eff.makeChanceResult()) && (!monster.isBuffed(MonsterStatus.抗压)))
                                    monster.applyStatus(player, new MonsterStatusEffect(MonsterStatus.抗压, 1, eff.getSourceId(), null, false), false, eff.getX() * 1000, true, eff);
                            }
                            break;
                    }


                    if (totDamageToOneMonster > 0)
                    {
                        Item weapon_ = player.getInventory(client.inventory.MapleInventoryType.EQUIPPED).getItem((short) -11);
                        if (weapon_ != null)
                        {
                            MonsterStatus stat = GameConstants.getStatFromWeapon(weapon_.getItemId());
                            if ((stat != null) && (Randomizer.nextInt(100) < GameConstants.getStatChance()))
                            {
                                MonsterStatusEffect monsterStatusEffect = new MonsterStatusEffect(stat, GameConstants.getXForStat(stat), GameConstants.getSkillForStat(stat), null, false);
                                monster.applyStatus(player, monsterStatusEffect, false, 10000L, false, null);
                            }
                        }
                        if (player.getBuffedValue(MapleBuffStat.致盲) != null)
                        {
                            MapleStatEffect eff = player.getStatForBuff(MapleBuffStat.致盲);
                            if ((eff != null) && (eff.makeChanceResult()))
                            {
                                MonsterStatusEffect monsterStatusEffect = new MonsterStatusEffect(MonsterStatus.命中, eff.getX(), eff.getSourceId(), null, false);
                                monster.applyStatus(player, monsterStatusEffect, false, eff.getY() * 1000, true, eff);
                            }
                        }
                        if (player.getBuffedValue(MapleBuffStat.敏捷提升) != null)
                        {
                            MapleStatEffect eff = player.getStatForBuff(MapleBuffStat.敏捷提升);
                            if ((eff != null) && (eff.makeChanceResult()))
                            {
                                MonsterStatusEffect monsterStatusEffect = new MonsterStatusEffect(MonsterStatus.速度, eff.getX(), 3121007, null, false);
                                monster.applyStatus(player, monsterStatusEffect, false, eff.getY() * 1000, true, eff);
                            }
                        }
                        if ((player.getJob() == 121) || (player.getJob() == 122))
                        {
                            Skill skill = SkillFactory.getSkill(1201012);
                            if (player.isBuffFrom(MapleBuffStat.属性攻击, skill))
                            {
                                MapleStatEffect eff = skill.getEffect(player.getTotalSkillLevel(skill));
                                MonsterStatusEffect monsterStatusEffect = new MonsterStatusEffect(MonsterStatus.结冰, 1, skill.getId(), null, false);
                                monster.applyStatus(player, monsterStatusEffect, false, eff.getY() * 2000, true, eff);
                            }
                        }
                    }
                    if ((effect != null) && (effect.getMonsterStati().size() > 0) && (effect.makeChanceResult()))
                    {
                        for (Map.Entry<MonsterStatus, Integer> monsterStatusIntegerEntry : effect.getMonsterStati().entrySet())
                        {
                            Map.Entry z = monsterStatusIntegerEntry;
                            monster.applyStatus(player, new MonsterStatusEffect((MonsterStatus) z.getKey(), (Integer) z.getValue(), theSkill.getId(), null, false), effect.isPoison(),
                                    effect.getDuration(), true, effect);
                        }
                    }
                }
            }
        }
        MapleMonster monster;
        Object weapon_;
        if ((hpMob > 0L) && (totDamageToOneMonster > 0))
        {
            player.afterAttack(attack.numAttacked, attack.numDamage, attack.skillId);
        }

        boolean notApplyTo = (attack.skillId == 31121005) || (attack.skillId == 61101002) || (attack.skillId == 61120007) || (attack.skillId == 36110004);
        if ((attack.skillId != 0) && ((attack.numAttacked > 0) || (attack.skillId != 4341002)) && (!GameConstants.isNoDelaySkill(attack.skillId)) && (!notApplyTo))
        {
            if (player.isShowPacket())
            {
                player.dropSpouseMessage(10, "[applyAttack] 开始 => applyTo - skillId: " + attack.skillId);
            }
            effect.applyTo(player, attack.position);
        }

        if ((totDamage > 1) && (GameConstants.getAttackDelay(attack.skillId, theSkill) >= 100))
        {
            CheatTracker tracker = player.getCheatTracker();
            tracker.setAttacksWithoutHit(true);
            if (tracker.getAttacksWithoutHit() >= 50)
            {
                tracker.registerOffense(CheatingOffense.ATTACK_WITHOUT_GETTING_HIT, "无敌自动封号.");
            }
        }
    }

    private static int CalculateMaxWeaponDamagePerHit(MapleCharacter player, MapleMonster monster, AttackInfo attack, Skill theSkill, MapleStatEffect attackEffect, double maxDamagePerMonster,
                                                      Integer CriticalDamagePercent)
    {
        int dLevel = Math.max(monster.getStats().getLevel() - player.getLevel(), 0) * 2;
        int HitRate = Math.min((int) Math.floor(Math.sqrt(player.getStat().getAccuracy())) - (int) Math.floor(Math.sqrt(monster.getStats().getEva())) + 100, 100);
        if (dLevel > HitRate)
        {
            HitRate = dLevel;
        }
        HitRate -= dLevel;
        if ((HitRate <= 0) && ((!GameConstants.is新手职业(attack.skillId / 10000)) || (attack.skillId % 10000 != 1000)) && (!GameConstants.isPyramidSkill(attack.skillId)) && (!GameConstants.isMulungSkill(attack.skillId)) && (!GameConstants.isInflationSkill(attack.skillId)))
        {
            return 0;
        }
        if ((player.getMapId() / 1000000 == 914) || (player.getMapId() / 1000000 == 927))
        {
            return 999999;
        }
        List<Element> elements = new ArrayList<>();
        int CritPercent = CriticalDamagePercent;
        int PDRate = monster.getStats().getPDRate();
        MonsterStatusEffect pdr = monster.getBuff(MonsterStatus.物防);
        if (pdr != null)
        {
            PDRate += pdr.getX();
        }
        double elemMaxDamagePerMonster = maxDamagePerMonster;
        if (theSkill != null)
        {
            if (theSkill.getId() == 1321015)
            {
                PDRate = monster.getStats().isBoss() ? PDRate : 0;
            }

            elements.add(theSkill.getElement());
            if (player.getBuffedValue(MapleBuffStat.属性攻击) != null)
            {
                int chargeSkillId = player.getBuffSource(MapleBuffStat.属性攻击);
                switch (chargeSkillId)
                {
                    case 1201011:
                        elements.add(Element.FIRE);
                        break;
                    case 1201012:
                    case 21101006:
                        elements.add(Element.ICE);
                        break;
                    case 1211008:
                        elements.add(Element.LIGHTING);
                        break;
                    case 1221004:
                        elements.add(Element.HOLY);
                }

            }
            if (player.getBuffedValue(MapleBuffStat.雷鸣冲击) != null)
            {
                elements.add(Element.LIGHTING);
            }
            if (player.getBuffedValue(MapleBuffStat.自然力重置) != null) elements.clear();
            if (elements.size() > 0)
            {
                double elementalEffect;
                switch (attack.skillId)
                {
                    case 3111003:
                        elementalEffect = attackEffect.getX() / 100.0D;
                        break;
                    default:
                        elementalEffect = 0.5D / elements.size();
                }

                for (Element element : elements)
                {
                    switch (monster.getEffectiveness(element))
                    {
                        case 免疫:
                            elemMaxDamagePerMonster = 1.0D;
                            break;
                        case 虚弱:
                            elemMaxDamagePerMonster *= (1.0D + elementalEffect + player.getStat().getElementBoost(element));
                            break;
                        case 增强:
                            elemMaxDamagePerMonster *= (1.0D - elementalEffect - player.getStat().getElementBoost(element));
                    }

                }
            }

            elemMaxDamagePerMonster -= elemMaxDamagePerMonster * (Math.max(PDRate - Math.max(player.getStat().getIgnoreMobpdpR(attack.skillId), 0) - Math.max(attackEffect == null ? 0 :
                    attackEffect.getIgnoreMob(), 0), 0) / 100.0D);


            elemMaxDamagePerMonster += elemMaxDamagePerMonster / 100.0D * CritPercent;

            MonsterStatusEffect imprint = monster.getBuff(MonsterStatus.鬼刻符);
            if (imprint != null)
            {
                elemMaxDamagePerMonster += elemMaxDamagePerMonster * imprint.getX() / 100.0D;
            }
            elemMaxDamagePerMonster += elemMaxDamagePerMonster * player.getDamageIncrease(monster.getObjectId()) / 100.0D;
            elemMaxDamagePerMonster *= ((monster.getStats().isBoss()) && (attackEffect != null) ? player.getStat().getBossDamageRate(attack.skillId) + attackEffect.getBossDamage() :
                    player.getStat().getDamageRate()) / 100.0D;
        }
        else if (attack.skillId == 0)
        {
            elemMaxDamagePerMonster -= elemMaxDamagePerMonster * (Math.max(PDRate - Math.max(player.getStat().getIgnoreMobpdpR(attack.skillId), 0), 0) / 100.0D);
            elemMaxDamagePerMonster += elemMaxDamagePerMonster / 100.0D * CritPercent;
            elemMaxDamagePerMonster *= (monster.getStats().isBoss() ? player.getStat().getBossDamageRate(attack.skillId) : player.getStat().getDamageRate()) / 100.0D;
        }
        int maxDamageOver = player.getMaxDamageOver(attack.skillId);
        int maxDamagePerMob = (int) elemMaxDamagePerMonster;
        if (theSkill != null)
        {
            if (GameConstants.is新手职业(theSkill.getId() / 10000))
            {
                switch (theSkill.getId() % 10000)
                {
                    case 1000:
                        maxDamagePerMob = 40;
                        break;
                    case 1020:
                        maxDamagePerMob = 1;
                        break;
                    case 1009:
                        maxDamagePerMob = (int) (monster.getStats().isBoss() ? monster.getMobMaxHp() / 30L * 100L : monster.getMobMaxHp());
                }

            }
            switch (theSkill.getId())
            {
                case 1311011:
                    maxDamagePerMob = (int) (maxDamagePerMob * ((attackEffect.getY() - attackEffect.getDamage()) / 100.0D));
                    break;
                case 1321012:
                case 32001000:
                case 32101000:
                case 32111002:
                case 32121002:
                    maxDamagePerMob = (int) (maxDamagePerMob * 1.5D);
                    break;
                case 33101001:
                    maxDamagePerMob *= attackEffect.getMobCount();
                    break;
                case 61121102:
                case 61121203:
                    if (!monster.getStats().isBoss())
                    {
                        maxDamagePerMob = 999999;
                    }
                    break;
                case 1221011:
                    maxDamagePerMob = (int) (monster.getStats().isBoss() ? 500000L : monster.getHp() - 1L);
                    break;
                case 21120006:
                    maxDamagePerMob = (int) (monster.getStats().isBoss() ? 999999L : monster.getHp() - 1L);
                    break;
                case 3221007:
                case 5221016:
                case 5721006:
                case 27121303:
                    if (monster.getStats().isBoss())
                    {
                        maxDamagePerMob = (int) (maxDamagePerMob * (1.0D + attackEffect.getIgnoreMob() / 100.0D));
                    }
                    else
                    {
                        maxDamagePerMob = maxDamageOver;
                    }
                    break;
            }
        }
        if ((player.getJob() == 311) || (player.getJob() == 312) || (player.getJob() == 321) || (player.getJob() == 322))
        {
            Skill mortal = SkillFactory.getSkill((player.getJob() == 311) || (player.getJob() == 312) ? 3110001 : 3210001);
            if (player.getTotalSkillLevel(mortal) > 0)
            {
                MapleStatEffect mort = mortal.getEffect(player.getTotalSkillLevel(mortal));
                if ((mort != null) && (monster.getHPPercent() < mort.getX()))
                {
                    maxDamagePerMob = 999999;
                    if (mort.getZ() > 0)
                    {
                        player.addHP(player.getStat().getMaxHp() * mort.getZ() / 100);
                    }
                }
            }
        }
        else if ((player.getJob() == 221) || (player.getJob() == 222))
        {
            Skill mortal = SkillFactory.getSkill(2210000);
            if (player.getTotalSkillLevel(mortal) > 0)
            {
                MapleStatEffect mort = mortal.getEffect(player.getTotalSkillLevel(mortal));
                if ((mort != null) && (monster.getHPPercent() < mort.getX()))
                {
                    maxDamagePerMob = 999999;
                }
            }
        }

        if ((monster.getId() >= 9400900) && (monster.getId() <= 9400911))
        {
            maxDamagePerMob = 999999;
        }
        else if ((monster.getId() >= 9600101) && (monster.getId() <= 9600136))
        {
            maxDamagePerMob = 888888;
        }
        else if (maxDamagePerMob > maxDamageOver)
        {
            maxDamagePerMob = maxDamageOver;
        }
        else if (maxDamagePerMob <= 0)
        {
            maxDamagePerMob = 1;
        }
        return maxDamagePerMob;
    }

    private static void handlePickPocket(MapleCharacter player, MapleMonster mob, AttackPair oned)
    {
        int maxmeso = player.getBuffedValue(MapleBuffStat.敛财术);
        for (Pair<Integer, Boolean> eachde : oned.attack)
        {
            Integer eachd = eachde.left;
            if ((player.getStat().pickRate >= 100) || (Randomizer.nextInt(99) < player.getStat().pickRate))
            {
                player.getMap().spawnMesoDrop(Math.min((int) Math.max(eachd / 20000.0D * maxmeso, 1.0D), maxmeso), new Point((int) (mob.getTruePosition().getX() + Randomizer.nextInt(100) - 50.0D),
                        (int) mob.getTruePosition().getY()), mob, player, false, (byte) 0);
            }
        }
    }

    public static void applyAttackMagic(AttackInfo attack, Skill theSkill, MapleCharacter player, MapleStatEffect effect, double maxDamagePerMonster)
    {
        if (!player.isAlive())
        {
            player.getCheatTracker().registerOffense(CheatingOffense.ATTACKING_WHILE_DEAD);
            return;
        }
        if ((attack.real) && (GameConstants.getAttackDelay(attack.skillId, theSkill) >= 50))
        {
            player.getCheatTracker().checkAttack(attack.skillId, attack.lastAttackTickCount);
        }
        int mobCount = effect.getMobCount(player);
        int attackCount = effect.getAttackCount(player);
        if (player.getStatForBuff(MapleBuffStat.光暗转换) != null)
        {
            attackCount *= 2;
        }
        if ((player.getStatForBuff(MapleBuffStat.天使复仇) != null) && (attack.skillId == 2321007))
        {
            attackCount += 4;
        }
        if ((attack.numDamage > attackCount) || ((attack.numAttacked > mobCount) && (attack.skillId != 27121303)))
        {
            if (player.isShowPacket())
            {
                player.dropMessage(-5, "魔法攻击次数检测  attack.hits " + attack.numDamage + " attackCount " + attackCount + " attack.targets " + attack.numAttacked + " MobCount " + mobCount);
            }

            player.getCheatTracker().registerOffense(CheatingOffense.MISMATCHING_BULLETCOUNT, "异常的攻击次数.");
            log.info("[作弊] " + player.getName() + " 魔法攻击次数异常。attack.hits " + attack.numDamage + " attackCount " + attackCount + " attack.targets " + attack.numAttacked + " MobCount " + mobCount);
            WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6, "[GM Message] " + player.getName() + " ID: " + player.getId() + " (等级 " + player.getLevel() +
                    ") 魔法攻击次数异常。attack.hits " + attack.numDamage + " attackCount " + attackCount + " attack.targets " + attack.numAttacked + " MobCount " + mobCount + " 技能ID " + attack.skillId));
            return;
        }
        if ((attack.numDamage > 0) && (attack.numAttacked > 0) && (!player.getStat().checkEquipDurabilitys(player, -1)))
        {
            player.dropMessage(5, "An item has run out of durability but has no inventory room to go to.");
            return;
        }

        if (GameConstants.isMulungSkill(attack.skillId))
        {
            if (player.getMapId() / 10000 != 92502)
            {
                return;
            }
            if (player.getMulungEnergy() < 10000)
            {
                return;
            }
            player.mulung_EnergyModify(false);
        }
        else if (GameConstants.isPyramidSkill(attack.skillId))
        {
            if (player.getMapId() / 1000000 != 926)
            {
                return;
            }
            if ((player.getPyramidSubway() != null) && (player.getPyramidSubway().onSkillUse(player)))
            {
            }


        }
        else if ((GameConstants.isInflationSkill(attack.skillId)) && (player.getBuffedValue(MapleBuffStat.GIANT_POTION) == null))
        {
            return;
        }

        if ((player.getClient().getChannelServer().isAdminOnly()) && (player.isAdmin()))
        {
            player.dropMessage(-1, "攻击动作: " + Integer.toHexString(attack.display));
        }
        PlayerStats stats = player.getStat();
        Element element = player.getBuffedValue(MapleBuffStat.自然力重置) != null ? Element.NEUTRAL : theSkill.getElement();

        int maxDamagePerHit = 0;
        int totDamage = 0;


        int CriticalDamage = stats.passive_sharpeye_percent();
        Skill eaterSkill = SkillFactory.getSkill(GameConstants.getMPEaterForJob(player.getJob()));
        int eaterLevel = player.getTotalSkillLevel(eaterSkill);

        MapleMap map = player.getMap();

        if (attack.skillId != 2301002)
        {
            if (map.isPvpMap())
            {
                MaplePvp.doPvP(player, map, attack, effect);
            }
            else if (map.isPartyPvpMap())
            {
                MaplePvp.doPartyPvP(player, map, attack, effect);
            }
            else if (map.isGuildPvpMap())
            {
                MaplePvp.doGuildPvP(player, map, attack, effect);
            }
        }

        for (AttackPair oned : attack.allDamage)
        {
            MapleMonster monster = map.getMonsterByOid(oned.objectid);
            if ((monster != null) && (monster.getLinkCID() <= 0))
            {
                boolean Tempest = (monster.getStatusSourceID(MonsterStatus.结冰) == 21120006) && (!monster.getStats().isBoss());
                int totDamageToOneMonster = 0;
                MapleMonsterStats monsterstats = monster.getStats();
                int fixeddmg = monsterstats.getFixedDamage();
                if (!Tempest)
                {


                    if ((!monster.isBuffed(MonsterStatus.免疫魔攻)) && (!monster.isBuffed(MonsterStatus.反射魔攻)))
                    {
                        maxDamagePerHit = CalculateMaxMagicDamagePerHit(player, theSkill, monster, monsterstats, stats, element, CriticalDamage, maxDamagePerMonster, effect);
                    }
                    else
                    {
                        maxDamagePerHit = 1;
                    }
                }
                byte overallAttackCount = 0;

                for (Pair<Integer, Boolean> eachde : oned.attack)
                {
                    Integer eachd = eachde.left;
                    overallAttackCount = (byte) (overallAttackCount + 1);
                    if (fixeddmg != -1)
                    {
                        eachd = monsterstats.getOnlyNoramlAttack() ? 0 : fixeddmg;
                    }
                    else
                    {
                        if ((player.isShowPacket()) && (eachd > 0))
                        {
                            player.dropMessage(-1, "魔法攻击打怪伤害 : " + eachd + " 服务端预计伤害 : " + maxDamagePerHit + " 是否超过 : " + (eachd > maxDamagePerHit));
                        }
                        if (monsterstats.getOnlyNoramlAttack())
                        {
                            eachd = 0;
                        }
                        else if (!player.isGM())
                        {
                            if (Tempest)
                            {
                                if (eachd > monster.getMobMaxHp())
                                {
                                    eachd = (int) Math.min(monster.getMobMaxHp(), 2147483647L);
                                    player.getCheatTracker().registerOffense(CheatingOffense.HIGH_DAMAGE_MAGIC, "攻击伤害过高.");
                                }
                            }
                            else if ((!monster.isBuffed(MonsterStatus.免疫魔攻)) && (!monster.isBuffed(MonsterStatus.反射魔攻)))
                            {
                                if (eachd > maxDamagePerHit)
                                {
                                    player.getCheatTracker().registerOffense(CheatingOffense.HIGH_DAMAGE_MAGIC, "[伤害: " + eachd + ", 预期: " + maxDamagePerHit + ", 怪物ID: " + monster.getId() + "] " +
                                            "[职业:" + " " + player.getJob() + ", 等级: " + player.getLevel() + ", 技能: " + attack.skillId + "]");
                                    if (attack.real)
                                    {
                                        player.getCheatTracker().checkSameDamage(eachd, maxDamagePerHit);
                                    }
                                    if (eachd > maxDamagePerHit * 3)
                                    {
                                        StringBuilder banReason = new StringBuilder(player.getName() + " 被系统封号.[异常攻击伤害值: " + eachd + ", 预计伤害: " + maxDamagePerHit + ", 怪物ID: " + monster.getId() +
                                                "]" + " [职业: " + player.getJob() + ", 等级: " + player.getLevel() + ", 技能: " + attack.skillId + "]");
                                        if ((player.getLevel() < 10) && (eachd >= 10000))
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        if ((player.getLevel() < 20) && (eachd >= 20000))
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        if ((player.getLevel() < 30) && (eachd >= 40000))
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        if ((player.getLevel() < 50) && (eachd >= 60000))
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        if ((player.getLevel() < 70) && (eachd >= 399999))
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        if ((player.getLevel() < 150) && (eachd >= 599999))
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        if (eachd > player.getMaxDamageOver(attack.skillId))
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        if (eachd > maxDamagePerHit * 5)
                                        {
                                            AutobanManager.getInstance().autoban(player.getClient(), banReason.toString());
                                            return;
                                        }
                                        player.getCheatTracker().registerOffense(CheatingOffense.HIGH_DAMAGE_MAGIC_2, "[伤害: " + eachd + ", 预期: " + maxDamagePerHit + ", 怪物ID: " + monster.getId() +
                                                "] [职业: " + player.getJob() + ", 等级: " + player.getLevel() + ", 技能: " + attack.skillId + "]");
                                    }
                                }
                            }
                            else if (eachd > maxDamagePerHit)
                            {
                                eachd = maxDamagePerHit;
                            }
                        }
                    }

                    totDamageToOneMonster += eachd;
                }
                totDamage += totDamageToOneMonster;
                player.checkMonsterAggro(monster);
                if ((GameConstants.getAttackDelay(attack.skillId, theSkill) >= 50) && (!GameConstants.isNoDelaySkill(attack.skillId)) && (!GameConstants.is不检测范围(attack.skillId)) && (!monster.getStats().isBoss()) && (player.getTruePosition().distanceSq(monster.getTruePosition()) > GameConstants.getAttackRange(effect, player.getStat().defRange)) && (player.getMapId() != 703002000))
                {
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                            "[GM Message] " + player.getName() + " ID: " + player.getId() + " (等级 " + player.getLevel() + ") 攻击范围异常。职业: " + player.getJob() + " 技能: " + attack.skillId + " [范围: " + player.getTruePosition().distanceSq(monster.getTruePosition()) + " 预期: " + GameConstants.getAttackRange(effect, player.getStat().defRange) + "]"));
                    player.getCheatTracker().registerOffense(CheatingOffense.ATTACK_FARAWAY_MONSTER,
                            "[范围: " + player.getTruePosition().distanceSq(monster.getTruePosition()) + ", 预期范围: " + GameConstants.getAttackRange(effect, player.getStat().defRange) + " ] [职业: " + player.getJob() + " 技能: " + attack.skillId + " ]");
                }

                if ((attack.skillId == 2301002) && (!monsterstats.getUndead()))
                {
                    player.getCheatTracker().registerOffense(CheatingOffense.HEAL_ATTACKING_UNDEAD);
                    return;
                }
                if (totDamageToOneMonster > 0)
                {
                    monster.damage(player, totDamageToOneMonster, true, attack.skillId);
                    if (monster.isBuffed(MonsterStatus.反射魔攻))
                    {
                        player.addHP(-(7000 + Randomizer.nextInt(8000)));
                    }
                    if (player.getBuffedValue(MapleBuffStat.缓速术) != null)
                    {
                        MapleStatEffect eff = player.getStatForBuff(MapleBuffStat.缓速术);
                        if ((eff != null) && (eff.makeChanceResult()) && (!monster.isBuffed(MonsterStatus.速度)))
                        {
                            monster.applyStatus(player, new MonsterStatusEffect(MonsterStatus.速度, eff.getX(), eff.getSourceId(), null, false), false, eff.getY() * 1000, true, eff);
                        }
                    }
                    player.onAttack(monster.getMobMaxHp(), monster.getMobMaxMp(), attack.skillId, monster.getObjectId(), totDamage);
                    switch (attack.skillId)
                    {
                        case 2211010:
                            monster.setTempEffectiveness(Element.ICE, effect.getDuration());
                            break;
                        case 2121003:
                            monster.setTempEffectiveness(Element.FIRE, effect.getDuration());
                    }


                    if ((effect != null) && (effect.getMonsterStati().size() > 0) && (effect.makeChanceResult()))
                    {
                        for (Map.Entry<MonsterStatus, Integer> monsterStatusIntegerEntry : effect.getMonsterStati().entrySet())
                        {
                            Map.Entry<MonsterStatus, Integer> z = monsterStatusIntegerEntry;
                            monster.applyStatus(player, new MonsterStatusEffect(z.getKey(), z.getValue(), theSkill.getId(), null, false), effect.isPoison(), effect.getDuration(), true, effect);
                        }
                    }


                    if (eaterLevel > 0)
                    {
                        eaterSkill.getEffect(eaterLevel).applyPassive(player, monster);
                    }
                }
                else if ((attack.skillId == 27101101) && (effect != null) && (effect.getMonsterStati().size() > 0) && (effect.makeChanceResult()))
                {
                    for (Map.Entry<MonsterStatus, Integer> monsterStatusIntegerEntry : effect.getMonsterStati().entrySet())
                    {
                        Map.Entry<MonsterStatus, Integer> z = monsterStatusIntegerEntry;
                        monster.applyStatus(player, new MonsterStatusEffect(z.getKey(), z.getValue(), theSkill.getId(), null, false), effect.isPoison(), effect.getDuration(), true, effect);
                    }
                }
            }
        }
        MapleMonster monster;
        Object eff;
        if (attack.skillId != 2301002)
        {
            effect.applyTo(player);
        }
        if ((totDamage > 1) && (GameConstants.getAttackDelay(attack.skillId, theSkill) >= 100))
        {
            CheatTracker tracker = player.getCheatTracker();
            tracker.setAttacksWithoutHit(true);
            if (tracker.getAttacksWithoutHit() >= 50)
            {
                tracker.registerOffense(CheatingOffense.ATTACK_WITHOUT_GETTING_HIT, "无敌自动封号.");
            }
        }
    }

    private static int CalculateMaxMagicDamagePerHit(MapleCharacter chr, Skill skill, MapleMonster monster, MapleMonsterStats mobstats, PlayerStats stats, Element elem, Integer sharpEye,
                                                     double maxDamagePerMonster, MapleStatEffect attackEffect)
    {
        int dLevel = Math.max(mobstats.getLevel() - chr.getLevel(), 0) * 2;
        int HitRate = Math.min((int) Math.floor(Math.sqrt(stats.getAccuracy())) - (int) Math.floor(Math.sqrt(mobstats.getEva())) + 100, 100);
        if (dLevel > HitRate)
        {
            HitRate = dLevel;
        }
        HitRate -= dLevel;
        if ((HitRate <= 0) && ((!GameConstants.is新手职业(skill.getId() / 10000)) || (skill.getId() % 10000 != 1000)))
        {
            return 0;
        }

        int CritPercent = sharpEye;
        server.life.ElementalEffectiveness ee = monster.getEffectiveness(elem);
        double elemMaxDamagePerMob;
        switch (ee)
        {
            case 免疫:
                elemMaxDamagePerMob = 1.0D;
                break;
            default:
                elemMaxDamagePerMob = ElementalStaffAttackBonus(elem, maxDamagePerMonster * ee.getValue(), stats);
        }


        int MDRate = monster.getStats().getMDRate();
        MonsterStatusEffect pdr = monster.getBuff(MonsterStatus.魔防);
        if (pdr != null)
        {
            MDRate += pdr.getX();
        }
        elemMaxDamagePerMob -= elemMaxDamagePerMob * (Math.max(MDRate - stats.getIgnoreMobpdpR(skill.getId()) - attackEffect.getIgnoreMob(), 0) / 100.0D);


        elemMaxDamagePerMob += elemMaxDamagePerMob / 100.0D * CritPercent;
        elemMaxDamagePerMob *= (monster.getStats().isBoss() ? chr.getStat().getBossDamageRate(skill.getId()) : chr.getStat().getDamageRate()) / 100.0D;
        MonsterStatusEffect imprint = monster.getBuff(MonsterStatus.鬼刻符);
        if (imprint != null)
        {
            elemMaxDamagePerMob += elemMaxDamagePerMob * imprint.getX() / 100.0D;
        }
        elemMaxDamagePerMob += elemMaxDamagePerMob * chr.getDamageIncrease(monster.getObjectId()) / 100.0D;
        if (GameConstants.is新手职业(skill.getId() / 10000))
        {
            switch (skill.getId() % 10000)
            {
                case 1000:
                    elemMaxDamagePerMob = 40.0D;
                    break;
                case 1020:
                    elemMaxDamagePerMob = 1.0D;
                    break;
                case 1009:
                    elemMaxDamagePerMob = monster.getStats().isBoss() ? monster.getMobMaxHp() / 30L * 100L : monster.getMobMaxHp();
            }

        }
        int maxDamagePerMob = (int) elemMaxDamagePerMob;
        switch (skill.getId())
        {
            case 32001000:
            case 32101000:
            case 32111002:
            case 32121002:
                maxDamagePerMob = (int) (maxDamagePerMob * 1.5D);
                break;
            case 27121303:
                maxDamagePerMob = chr.getMaxDamageOver(skill.getId());
        }

        int skillMode = constants.SkillConstants.getLuminousSkillMode(skill.getId());
        switch (skillMode)
        {
            case 20040216:
            case 20040217:
                MapleStatEffect effect = chr.getStatForBuff(MapleBuffStat.光暗转换);
                if ((effect != null) && (effect.getSourceId() == skillMode))
                {
                    maxDamagePerMob = (int) (maxDamagePerMob * 1.5D);
                }
                break;
        }

        if ((monster.getId() >= 9400900) && (monster.getId() <= 9400911))
        {
            maxDamagePerMob = 999999;
        }
        else if ((monster.getId() >= 9600101) && (monster.getId() <= 9600136))
        {
            maxDamagePerMob = 888888;
        }
        if (maxDamagePerMob > 999999)
        {
            maxDamagePerMob = chr.getMaxDamageOver(skill.getId());
        }
        else if (elemMaxDamagePerMob <= 0.0D)
        {
            maxDamagePerMob = 1;
        }
        return maxDamagePerMob;
    }

    private static double ElementalStaffAttackBonus(Element elem, double elemMaxDamagePerMob, PlayerStats stats)
    {
        switch (elem)
        {
            case FIRE:
                return elemMaxDamagePerMob / 100.0D * (stats.element_fire + stats.getElementBoost(elem));
            case ICE:
                return elemMaxDamagePerMob / 100.0D * (stats.element_ice + stats.getElementBoost(elem));
            case LIGHTING:
                return elemMaxDamagePerMob / 100.0D * (stats.element_light + stats.getElementBoost(elem));
            case POISON:
                return elemMaxDamagePerMob / 100.0D * (stats.element_psn + stats.getElementBoost(elem));
        }
        return elemMaxDamagePerMob / 100.0D * (stats.def + stats.getElementBoost(elem));
    }

    public static AttackInfo DivideAttack(AttackInfo attack, int rate)
    {
        attack.real = false;
        if (rate <= 1)
        {
            return attack; //lol
        }
        for (AttackPair p : attack.allDamage)
        {
            if (p.attack != null)
            {
                for (Pair<Integer, Boolean> eachd : p.attack)
                {
                    eachd.left /= rate; //too ex.
                }
            }
        }
        return attack;
    }

    public static AttackInfo Modify_AttackCrit(AttackInfo attack, MapleCharacter chr, int type, MapleStatEffect effect)
    {
        int criticalRate;
        boolean shadow;
        List<Integer> damages;
        List<Integer> damage;
        if (attack.skillId != 4211006)
        {
            criticalRate = chr.getStat().passive_sharpeye_rate() + (effect == null ? 0 : effect.getCritical());
            shadow = (chr.getBuffedValue(MapleBuffStat.影分身) != null) && ((type == 1) || (type == 2));
            damages = new ArrayList<>();
            damage = new ArrayList<>();

            for (AttackPair p : attack.allDamage)
                if (p.attack != null)
                {
                    int hit = 0;
                    int mid_att = shadow ? p.attack.size() / 2 : p.attack.size();
                    int toCrit =
                            (attack.skillId == 4221014) || (attack.skillId == 4221016) || (attack.skillId == 3221007) || (attack.skillId == 23121003) || (attack.skillId == 4321006) || (attack.skillId == 4331006) || (attack.skillId == 21121013) ? mid_att : 0;
                    int i;
                    if (toCrit == 0)
                    {
                        for (Pair<Integer, Boolean> eachd : p.attack)
                        {
                            if ((!eachd.right) && (hit < mid_att))
                            {
                                if ((eachd.left > 999999) || (Randomizer.nextInt(100) < criticalRate))
                                {
                                    toCrit++;
                                }
                                damage.add(eachd.left);
                            }
                            hit++;
                        }
                        if (toCrit == 0)
                        {
                            damage.clear();
                        }
                        else
                        {
                            java.util.Collections.sort(damage);
                            for (i = damage.size(); i > damage.size() - toCrit; i--)
                            {
                                damages.add(damage.get(i - 1));
                            }
                            damage.clear();
                        }
                    }
                    else
                    {
                        for (Pair<Integer, Boolean> eachd : p.attack)
                        {
                            if (!eachd.right)
                            {
                                if (attack.skillId == 4221014)
                                {
                                    eachd.right = hit == 3;
                                }
                                else if ((attack.skillId == 3221007) || (attack.skillId == 23121003) || (attack.skillId == 21121013) || (attack.skillId == 4321006) || (attack.skillId == 4331006) || (eachd.left > 999999))
                                {
                                    eachd.right = Boolean.TRUE;
                                }
                                else if (hit >= mid_att)
                                {
                                    eachd.right = p.attack.get(hit - mid_att).right;
                                }
                                else
                                {
                                    eachd.right = damages.contains(eachd.left);
                                }
                            }
                            hit++;
                        }
                        damages.clear();
                    }
                }
        }
        return attack;
    }


    public static AttackInfo parseMagicDamage(SeekableLittleEndianAccessor lea, MapleCharacter chr)
    {
        AttackInfo ret = new AttackInfo();
        ret.isMagicAttack = true;
        lea.skip(1);
        ret.numAttackedAndDamage = lea.readByte();
        ret.numAttacked = ((byte) (ret.numAttackedAndDamage >>> 4 & 0xF));
        ret.numDamage = ((byte) (ret.numAttackedAndDamage & 0xF));
        ret.skillId = lea.readInt();
        lea.skip(5);
        if (GameConstants.isMagicChargeSkill(ret.skillId))
        {
            ret.charge = lea.readInt();
        }
        else
        {
            ret.charge = -1;
        }
        lea.skip(1);
        ret.unk = lea.readByte();
        ret.display = lea.readByte();
        ret.direction = lea.readByte();
        lea.skip(4);
        lea.skip(1);
        if (chr.getCygnusBless())
        {
            lea.skip(12);
        }
        ret.speed = lea.readByte();
        ret.lastAttackTickCount = lea.readInt();
        lea.skip(4);


        ret.allDamage = new ArrayList<>();
        for (int i = 0; i < ret.numAttacked; i++)
        {
            int oid = lea.readInt();
            lea.skip(20);
            List<Pair<Integer, Boolean>> allDamageNumbers = new ArrayList<>();
            for (int j = 0; j < ret.numDamage; j++)
            {
                int damage = lea.readInt();
                if (chr.isShowPacket())
                {
                    chr.dropMessage(-5, "魔法攻击 - 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage);
                }
                if ((damage > chr.getMaxDamageOver(ret.skillId)) || (damage < 0))
                {
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(-5, "魔法攻击出错次数: 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage);
                    }
                    if ((damage > chr.getMaxDamageOver(ret.skillId)) && (!chr.isGM()))
                    {
                        chr.sendPolice("系统检测到您的攻击伤害异常，系统对您进行掉线处理。");
                        WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                                "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") 魔法攻击伤害异常。打怪伤害: " + damage + " 地图ID: " + chr.getMapId()));
                    }
                    FileoutputUtil.log("log\\攻击出错.log",
                            "魔法攻击出错封包:  打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage + " 技能ID: " + ret.skillId + lea.toString(true));
                }
                allDamageNumbers.add(new Pair(damage, Boolean.FALSE));
            }
            lea.skip(4);
            lea.skip(4);
            lea.skip(4);
            ret.allDamage.add(new AttackPair(Integer.valueOf(oid), allDamageNumbers));
        }
        ret.position = lea.readPos();
        return ret;
    }


    public static AttackInfo parseCloseRangeAttack(SeekableLittleEndianAccessor lea, MapleCharacter chr)
    {
        AttackInfo ret = new AttackInfo();
        ret.isCloseRangeAttack = true;
        lea.skip(1);
        ret.numAttackedAndDamage = lea.readByte();
        ret.numAttacked = ((byte) (ret.numAttackedAndDamage >>> 4 & 0xF));
        ret.numDamage = ((byte) (ret.numAttackedAndDamage & 0xF));
        ret.skillId = lea.readInt();
        if (chr.isShowPacket())
        {
            chr.dropSpouseMessage(25, "[CloseRangeAttack] - 技能ID: " + ret.skillId + " 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage);
        }
        switch (ret.skillId)
        {
            case 2111007:
            case 2211007:
            case 2311007:
            case 12111007:
            case 21101003:
            case 22161005:
            case 32111010:
                lea.skip(5);
                break;
            default:
                lea.skip(6);
        }

        switch (ret.skillId)
        {
            case 1311011:
            case 2221012:
            case 2221052:
            case 4221052:
            case 4341002:
            case 5081001:
            case 5101012:
            case 5101014:
            case 5300007:
            case 5301001:
            case 11121052:
            case 11121055:
            case 14111006:
            case 24121000:
            case 24121005:
            case 27101202:
            case 27111100:
            case 27120211:
            case 27121201:
            case 31001000:
            case 31101000:
            case 31111005:
            case 31201001:
            case 31211001:
            case 32121003:
            case 36101001:
            case 36121000:
            case 61111100:
            case 61111111:
            case 61111113:
            case 65121003:
            case 65121052:
            case 101110101:
            case 101110102:
            case 101120200:
            case 112001008:
                ret.charge = lea.readInt();
                break;
            default:
                ret.charge = 0;
        }


        if ((GameConstants.is神之子(chr.getJob())) && (ret.skillId >= 100000000))
        {
            ret.zeroUnk = lea.readByte();
        }
        lea.skip(1);
        ret.unk = lea.readByte();
        ret.display = lea.readByte();
        ret.direction = lea.readByte();
        lea.skip(4);
        lea.skip(1);
        if (chr.getCygnusBless())
        {
            lea.skip(12);
        }
        ret.speed = lea.readByte();
        ret.lastAttackTickCount = lea.readInt();
        lea.skip(4);
        switch (ret.skillId)
        {
            case 2111007:
            case 2211007:
            case 2311007:
            case 5100015:
            case 12111007:
            case 21101003:
            case 22161005:
            case 32111010:
            case 35121003:
                break;
            default:
                int linkskill = lea.readInt();
                if (linkskill > 0)
                {
                    lea.skip(1);
                }
                break;
        }
        ret.allDamage = new ArrayList<>();
        if (ret.skillId == 4211006)
        {
            return parseMesoExplosion(lea, ret, chr);
        }


        for (int i = 0; i < ret.numAttacked; i++)
        {
            int oid = lea.readInt();
            lea.skip(20);
            List<Pair<Integer, Boolean>> allDamageNumbers = new ArrayList<>();
            for (int j = 0; j < ret.numDamage; j++)
            {
                int damage = lea.readInt();
                if (chr.isShowPacket())
                {
                    chr.dropMessage(-5, "近距离攻击 - 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage);
                }
                if ((damage > chr.getMaxDamageOver(ret.skillId)) || (damage < 0))
                {
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(-5, "近距离攻击出错次数: 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage);
                    }
                    if ((damage > chr.getMaxDamageOver(ret.skillId)) && (!chr.isGM()))
                    {
                        chr.sendPolice("系统检测到您的攻击伤害异常，系统对您进行掉线处理。");
                        WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                                "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") 近距离攻击伤害异常。打怪伤害: " + damage + " 地图ID: " + chr.getMapId()));
                    }
                    FileoutputUtil.log("log\\攻击出错.log",
                            "近距离攻击出错封包: 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage + " 技能ID: " + ret.skillId + lea.toString(true));
                }
                allDamageNumbers.add(new Pair(damage, Boolean.FALSE));
            }
            lea.skip(4);
            lea.skip(4);
            lea.skip(4);
            ret.allDamage.add(new AttackPair(Integer.valueOf(oid), allDamageNumbers));
        }
        ret.position = lea.readPos();
        return ret;
    }

    public static AttackInfo parseMesoExplosion(SeekableLittleEndianAccessor lea, AttackInfo ret, MapleCharacter chr)
    {
        if (ret.numDamage == 0)
        {
            lea.skip(4);
            byte bullets = lea.readByte();
            for (int j = 0; j < bullets; j++)
            {
                int mesoid = lea.readInt();
                lea.skip(2);
                if (chr.isShowPacket())
                {
                    chr.dropMessage(-5, "金钱炸弹攻击怪物: 无怪 " + ret.numDamage + " 金币ID: " + mesoid);
                }
                ret.allDamage.add(new AttackPair(Integer.valueOf(mesoid), null));
            }
            lea.skip(2);
            return ret;
        }


        for (int i = 0; i < ret.numAttacked; i++)
        {
            int oid = lea.readInt();
            lea.skip(19);
            byte bullets = lea.readByte();
            List<Pair<Integer, Boolean>> allDamageNumbers = new ArrayList<>();
            for (int j = 0; j < bullets; j++)
            {
                int damage = lea.readInt();
                if (chr.isShowPacket())
                {
                    chr.dropMessage(-5, "金钱炸弹攻击怪物: " + ret.numAttacked + " 攻击次数: " + bullets + " 打怪伤害: " + damage);
                }
                allDamageNumbers.add(new Pair(damage, Boolean.FALSE));
            }
            ret.allDamage.add(new AttackPair(Integer.valueOf(oid), allDamageNumbers));
            lea.skip(4);
            lea.skip(4);
            lea.skip(4);
        }
        lea.skip(4);
        byte bullets = lea.readByte();
        for (int j = 0; j < bullets; j++)
        {
            int mesoid = lea.readInt();
            lea.skip(2);
            if (chr.isShowPacket())
            {
                chr.dropMessage(-5, "金钱炸弹攻击怪物: 有怪 " + bullets + " 金币ID: " + mesoid);
            }
            ret.allDamage.add(new AttackPair(Integer.valueOf(mesoid), null));
        }

        return ret;
    }

    public static AttackInfo parseRangedAttack(SeekableLittleEndianAccessor lea, MapleCharacter chr)
    {
        AttackInfo ret = new AttackInfo();
        ret.isRangedAttack = true;
        lea.skip(2);
        ret.numAttackedAndDamage = lea.readByte();
        ret.numAttacked = ((byte) (ret.numAttackedAndDamage >>> 4 & 0xF));
        ret.numDamage = ((byte) (ret.numAttackedAndDamage & 0xF));
        ret.skillId = lea.readInt();
        if (chr.isShowPacket())
        {
            chr.dropSpouseMessage(25, "[RangedAttack] - 技能ID: " + ret.skillId + " 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage);
        }
        lea.skip(5);
        switch (ret.skillId)
        {
            case 3111009:
            case 3120019:
            case 3121013:
            case 5221004:
            case 5221022:
            case 5311002:
            case 5311010:
            case 5721001:
            case 13111020:
            case 13121001:
            case 23121000:
            case 33121009:
            case 35001001:
            case 35101009:
            case 60011216:
                lea.skip(4);
        }


        if ((GameConstants.is神之子(chr.getJob())) && (ret.skillId >= 100000000))
        {
            ret.zeroUnk = lea.readByte();
        }
        lea.skip(1);
        lea.skip(1);
        ret.charge = -1;
        ret.unk = lea.readByte();
        ret.display = lea.readByte();
        ret.direction = lea.readByte();
        if ((ret.skillId == 3121013) || (ret.skillId == 5221022) || (ret.skillId == 5220023))
        {
            lea.skip(8);
        }
        else if ((ret.skillId == 5311010) || (ret.skillId == 5310011))
        {
            lea.skip(4);
        }
        lea.skip(4);
        lea.skip(1);
        switch (ret.skillId)
        {
            case 23111001:
            case 36111010:
                lea.skip(12);
        }

        if (chr.getCygnusBless())
        {
            lea.skip(12);
        }
        ret.speed = lea.readByte();
        ret.lastAttackTickCount = lea.readInt();
        lea.skip(4);
        ret.starSlot = lea.readShort();
        ret.cashSlot = lea.readShort();
        ret.AOE = lea.readByte();


        ret.allDamage = new ArrayList<>();
        for (int i = 0; i < ret.numAttacked; i++)
        {
            int oid = lea.readInt();
            lea.skip(20);
            List<Pair<Integer, Boolean>> allDamageNumbers = new ArrayList<>();
            for (int j = 0; j < ret.numDamage; j++)
            {
                int damage = lea.readInt();
                if (chr.isShowPacket())
                {
                    chr.dropMessage(-5, "远距离攻击 - 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage);
                }
                if ((damage > chr.getMaxDamageOver(ret.skillId)) || (damage < 0))
                {
                    if (chr.isAdmin())
                    {
                        chr.dropMessage(-5, "远距离攻击出错次数: 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage);
                    }
                    if ((damage > chr.getMaxDamageOver(ret.skillId)) && (!chr.isGM()))
                    {
                        chr.sendPolice("系统检测到您的攻击伤害异常，系统对您进行掉线处理。");
                        WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                                "[GM Message] " + chr.getName() + " ID: " + chr.getId() + " (等级 " + chr.getLevel() + ") 远距离攻击伤害异常。打怪伤害: " + damage + " 地图ID: " + chr.getMapId()));
                    }
                    FileoutputUtil.log("log\\攻击出错.log",
                            "远距离攻击出错封包: 打怪数量: " + ret.numAttacked + " 打怪次数: " + ret.numDamage + " 怪物ID " + oid + " 伤害: " + damage + " 技能ID: " + ret.skillId + lea.toString(true));
                }
                allDamageNumbers.add(new Pair(damage, Boolean.FALSE));
            }
            lea.skip(4);
            lea.skip(4);
            lea.skip(4);
            ret.allDamage.add(new AttackPair(Integer.valueOf(oid), allDamageNumbers));
        }
        ret.position = lea.readPos();
        if (lea.available() == 4L)
        {
            ret.skillposition = lea.readPos();
        }
        return ret;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\DamageParse.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */