package handling.channel.handler;

import java.awt.Point;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleClient;
import client.PlayerStats;
import client.Skill;
import client.SkillFactory;
import client.inventory.MapleInventoryType;
import client.status.MonsterStatus;
import client.status.MonsterStatusEffect;
import server.MapleStatEffect;
import server.Randomizer;
import server.life.MapleMonster;
import server.life.MobAttackInfo;
import server.life.MobSkill;
import server.life.MobSkillFactory;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;


public class TakeDamageHandler
{
    public static void TakeDamage(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.isHidden()) || ((chr.isGM()) && (chr.isInvincible())))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        slea.readInt();
        chr.updateTick(slea.readInt());
        byte type = slea.readByte();
        slea.skip(1);
        int damage = slea.readInt();
        slea.skip(2);
        boolean isDeadlyAttack = false;
        boolean pPhysical = false;
        int oid = 0;
        int monsteridfrom = 0;
        int fake = 0;
        int mpattack = 0;
        byte direction = 0;
        int skillId = 0;
        int pOid = 0;
        int pDamage = 0;
        byte pType = 0;
        Point pPos = new Point(0, 0);
        MapleMonster attacker = null;
        PlayerStats stats = chr.getStat();
        if (chr.isShowPacket())
        {
            chr.dropMessage(5, "受伤类型: " + type + " 受伤数值: " + damage);
        }
        if ((type != -2) && (type != -3) && (type != -4))
        {
            monsteridfrom = slea.readInt();
            oid = slea.readInt();
            attacker = chr.getMap().getMonsterByOid(oid);
            direction = slea.readByte();

            if ((attacker == null) || (attacker.getId() != monsteridfrom) || (attacker.getLinkCID() > 0) || (attacker.isFake()) || (attacker.getStats().isFriendly()))
            {
                return;
            }
            if ((type != -1) && (damage > 0))
            {
                MobAttackInfo attackInfo = attacker.getStats().getMobAttack(type);
                if (attackInfo != null)
                {
                    if (attackInfo.isDeadlyAttack())
                    {
                        isDeadlyAttack = true;
                        mpattack = stats.getMp() - 1;
                    }
                    else
                    {
                        mpattack += attackInfo.getMpBurn();
                    }
                    MobSkill skill = MobSkillFactory.getInstance().getMobSkill(attackInfo.getDiseaseSkill(), attackInfo.getDiseaseLevel());
                    if ((skill != null) && ((damage == -1) || (damage > 0)))
                    {
                        skill.applyEffect(chr, attacker, false);
                    }
                    attacker.setMp(attacker.getMp() - attackInfo.getMpCon());
                }
            }
        }
        skillId = slea.readInt();
        pDamage = slea.readInt();
        byte defType = slea.readByte();
        slea.skip(1);
        if (chr.isShowPacket())
        {
            chr.dropMessage(5, "受到伤害: " + damage + " 技能ID: " + skillId + " 反射伤害: " + pDamage + " defType: " + defType);
        }
        if (defType == 1)
        {
            Skill bx = SkillFactory.getSkill(31110008);
            int bof = chr.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff = bx.getEffect(bof);
                if (Randomizer.nextInt(100) <= eff.getX())
                {
                    chr.handleForceGain(oid, 31110008, eff.getZ());
                }
            }
        }
        if ((skillId != 0) && (pDamage > 0))
        {
            pPhysical = slea.readByte() > 0;
            pOid = slea.readInt();
            pType = slea.readByte();
            slea.skip(4);
            pPos = slea.readPos();
        }
        if (damage == -1)
        {
            fake = 4020002 + (chr.getJob() / 10 - 40) * 100000;
            if ((fake != 4120002) && (fake != 4220002))
            {
                fake = 4120002;
            }
            if ((type == -1) && (chr.getJob() == 122) && (attacker != null) && (chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10) != null) && (chr.getTotalSkillLevel(1221016) > 0))
            {
                MapleStatEffect eff = SkillFactory.getSkill(1221016).getEffect(chr.getTotalSkillLevel(1221016));
                attacker.applyStatus(chr, new MonsterStatusEffect(MonsterStatus.眩晕, 1, 1221016, null, false), false, eff.getDuration(), true, eff);
                fake = 1221016;
            }

            if (chr.getTotalSkillLevel(fake) > 0)
            {
            }

        }
        else if ((damage < -1) || (damage > 200000))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (((chr.getStat().dodgeChance <= 0) || (Randomizer.nextInt(100) >= chr.getStat().dodgeChance)) || (


                (pPhysical) && (pDamage > 0) && (chr.getTotalSkillLevel(skillId) > 0)))
        {
            switch (skillId)
            {
                case 1101006:
                    Skill skill = SkillFactory.getSkill(skillId);
                    MapleStatEffect eff = skill.getEffect(chr.getTotalSkillLevel(skillId));
                    long bounceDamage = Math.min((int) (damage * (eff.getY() / 100.0D)), attacker.getMobMaxHp() / 2L);
                    damage -= damage * (eff.getX() + stats.reduceDamageRate);
                    if (damage < 1)
                    {
                        damage = 1;
                    }
                    if (chr.isShowPacket())
                    {
                        chr.dropMessage(5, "受到伤害 - 减少后的伤害: " + damage + " 处理反射伤害: " + bounceDamage + " 解析反射伤害: " + pDamage + " 技能ID: " + skillId + " - " + skill.getName());
                    }
                    if (bounceDamage > pDamage)
                    {
                        bounceDamage = pDamage;
                    }
                    attacker.damage(chr, bounceDamage, true, skillId);
            }

        }
        else if (stats.reduceDamageRate > 0)
        {
            damage = (int) (damage - damage * (stats.reduceDamageRate / 100.0D));
        }
        chr.getCheatTracker().checkTakeDamage(damage);
        Pair<Double, Boolean> modify = chr.modifyDamageTaken(damage, attacker);
        damage = modify.left.intValue();
        if (chr.isShowPacket())
        {
            chr.dropMessage(5, "最终受到伤害 " + damage);
        }
        if (damage > 0)
        {
            chr.getCheatTracker().setAttacksWithoutHit(false);
            if (chr.getBuffedValue(MapleBuffStat.变身术) != null)
            {
                chr.cancelMorphs();
            }
            boolean mpAttack = (chr.getBuffedValue(MapleBuffStat.金属机甲) != null) && (chr.getBuffSource(MapleBuffStat.金属机甲) != 35121005);
            if (chr.getBuffedValue(MapleBuffStat.魔法盾) != null)
            {
                int hploss = 0;
                int mploss = 0;
                if (isDeadlyAttack)
                {
                    if (stats.getHp() > 1)
                    {
                        hploss = stats.getHp() - 1;
                    }
                    if (stats.getMp() > 1)
                    {
                        mploss = stats.getMp() - 1;
                    }
                    if (chr.getBuffedValue(MapleBuffStat.终极无限) != null)
                    {
                        mploss = 0;
                    }
                    chr.addMPHP(-hploss, -mploss);
                }
                else
                {
                    mploss = (int) (damage * (chr.getBuffedValue(MapleBuffStat.魔法盾).doubleValue() / 100.0D)) + mpattack;
                    hploss = damage - mploss;
                    if (chr.getBuffedValue(MapleBuffStat.终极无限) != null)
                    {
                        mploss = 0;
                    }
                    else if (mploss > stats.getMp())
                    {
                        mploss = stats.getMp();
                        hploss = damage - mploss + mpattack;
                    }
                    chr.addMPHP(-hploss, -mploss);
                }
            }
            else if (chr.getTotalSkillLevel(27000003) > 0)
            {
                int hploss = 0;
                int mploss = 0;
                if (isDeadlyAttack)
                {
                    if (stats.getHp() > 1)
                    {
                        hploss = stats.getHp() - 1;
                    }
                    if (stats.getMp() > 1)
                    {
                        mploss = stats.getMp() - 1;
                    }
                    chr.addMPHP(-hploss, -mploss);
                }
                else
                {
                    Skill skill = SkillFactory.getSkill(27000003);
                    MapleStatEffect eff = skill.getEffect(chr.getTotalSkillLevel(27000003));
                    mploss = (int) (damage * (eff.getX() / 100.0D)) + mpattack;
                    hploss = damage - mploss;
                    if (mploss > stats.getMp())
                    {
                        mploss = stats.getMp();
                        hploss = damage - mploss + mpattack;
                    }
                    if (chr.isShowPacket())
                    {
                        chr.dropMessage(5, "受到伤害: " + damage + " 减少Hp: " + hploss + " 减少Mp: " + mploss + " 技能减少: " + eff.getX() / 100.0D);
                    }
                    chr.addMPHP(-hploss, -mploss);
                }
            }
            else if (chr.getStat().mesoGuardMeso > 0.0D)
            {
                if (chr.isShowPacket())
                {
                    chr.dropMessage(5, "受到伤害: " + damage);
                }
                damage = (int) Math.ceil(damage * chr.getStat().mesoGuard / 100.0D);
                int mesoloss = (int) (damage * (chr.getStat().mesoGuardMeso / 100.0D));
                if (chr.isShowPacket())
                {
                    chr.dropMessage(5, "金钱护盾 - 最终伤害: " + damage + " 减少金币: " + mesoloss);
                }
                if (chr.getMeso() < mesoloss)
                {
                    chr.gainMeso(-chr.getMeso(), false);
                    chr.cancelBuffStats(MapleBuffStat.金钱护盾);
                }
                else
                {
                    chr.gainMeso(-mesoloss, false);
                }
                if ((isDeadlyAttack) && (stats.getMp() > 1))
                {
                    mpattack = stats.getMp() - 1;
                }
                chr.addMPHP(-damage, -mpattack);
            }
            else if (isDeadlyAttack)
            {
                chr.addMPHP(stats.getHp() > 1 ? -(stats.getHp() - 1) : 0, (stats.getMp() > 1) && (!mpAttack) ? -(stats.getMp() - 1) : 0);
            }
            else
            {
                chr.addMPHP(-damage, mpAttack ? 0 : -mpattack);
            }

            if ((chr.inPVP()) && (chr.getStat().getHPPercent() <= 20))
            {
                SkillFactory.getSkill(PlayerStats.getSkillByJob(93, chr.getJob())).getEffect(1).applyTo(chr);
            }
        }
        byte offset = 0;
        int offset_d = 0;
        if (slea.available() == 1L)
        {
            offset = slea.readByte();
            if ((offset == 1) && (slea.available() >= 4L))
            {
                offset_d = slea.readInt();
            }
            if ((offset < 0) || (offset > 2))
            {
                offset = 0;
            }
        }
        if (chr.isShowPacket())
        {
            chr.dropMessage(-5, "玩家掉血 - 类型: " + type + " 怪物ID: " + monsteridfrom + " 伤害: " + damage + " fake: " + fake + " direction: " + direction + " oid: " + oid + " offset: " + offset);
        }
        if ((damage < -1) || (damage > 200000))
        {
            FileoutputUtil.log("log\\掉血错误.log", "玩家[" + chr.getName() + " 职业: " + chr.getJob() + "]掉血错误 - 类型: " + type + " 怪物ID: " + monsteridfrom + " 伤害: " + damage + " fake: " + fake + " " +
                    "direction: " + direction + " oid: " + oid + " offset: " + offset + " 封包:" + slea.toString(true));
            return;
        }
        c.getSession().write(MaplePacketCreator.enableActions());
        chr.getMap().broadcastMessage(chr, MaplePacketCreator.damagePlayer(chr.getId(), type, damage, monsteridfrom, direction, skillId, pDamage, pPhysical, pOid, pType, pPos, offset, offset_d,
                fake), false);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\TakeDamageHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */