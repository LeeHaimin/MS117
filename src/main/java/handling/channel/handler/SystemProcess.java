package handling.channel.handler;

import client.MapleCharacter;
import client.MapleClient;
import tools.FileoutputUtil;
import tools.data.input.SeekableLittleEndianAccessor;


public class SystemProcess
{
    public static void SystemProcess(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        int num = slea.readInt();
        slea.skip(1);
        String file = "log\\进程信息\\" + (chr != null ? chr.getName() + ".txt" : "进程信息.txt");
        StringBuilder ret = new StringBuilder("---------------------------------------------------\r\n");
        for (int i = 0; i < num; i++)
        {
            ret.append("路径: ");
            ret.append(slea.readMapleAsciiString());
            ret.append("\r\n");
            ret.append("未知01: ");
            ret.append(slea.readMapleAsciiString());
            ret.append("\r\n");
            ret.append("未知02: ");
            ret.append(slea.readMapleAsciiString());
            ret.append("\r\n");
            ret.append("MD501: ");
            ret.append(slea.readMapleAsciiString());
            ret.append("\r\n");
            ret.append("MD502: ");
            ret.append(slea.readMapleAsciiString());
            ret.append("\r\n");
        }
        ret.append("\r\n");
        FileoutputUtil.log(file, ret.toString(), true);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\SystemProcess.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */