package handling.channel.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import client.BuddyList;
import client.BuddylistEntry;
import client.MapleCharacter;
import client.MapleClient;
import handling.world.WorldBuddyService;
import handling.world.WorldFindService;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.BuddyListPacket;

public class BuddyListHandler
{
    public static void BuddyOperation(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int mode = slea.readByte();
        BuddyList buddylist = c.getPlayer().getBuddylist();
        if (mode == 1)
        {
            String addName = slea.readMapleAsciiString();
            String groupName = slea.readMapleAsciiString();
            BuddylistEntry ble = buddylist.get(addName);
            if ((addName.getBytes().length > 13) || (groupName.getBytes().length > 16))
            {
                return;
            }
            if ((ble != null) && ((ble.getGroup().equals(groupName)) || (!ble.isVisible())))
            {
                c.getSession().write(BuddyListPacket.buddylistMessage(13));
            }
            else if ((ble != null) && (ble.isVisible()))
            {
                ble.setGroup(groupName);
                c.getSession().write(BuddyListPacket.updateBuddylist(buddylist.getBuddies(), 10));
            }
            else if (buddylist.isFull())
            {
                c.getSession().write(BuddyListPacket.buddylistMessage(11));
            }
            else
            {
                try
                {
                    CharacterIdNameBuddyCapacity charWithId = null;
                    int channel = WorldFindService.getInstance().findChannel(addName);
                    MapleCharacter otherChar = null;
                    if (channel > 0)
                    {
                        otherChar = handling.channel.ChannelServer.getInstance(channel).getPlayerStorage().getCharacterByName(addName);
                        if (otherChar == null)
                        {
                            charWithId = getCharacterIdAndNameFromDatabase(addName, groupName);
                        }
                        else if ((!otherChar.isIntern()) || (c.getPlayer().isIntern()))
                        {
                            charWithId = new CharacterIdNameBuddyCapacity(otherChar.getId(), otherChar.getName(), groupName, otherChar.getBuddylist().getCapacity());
                        }
                    }
                    else
                    {
                        charWithId = getCharacterIdAndNameFromDatabase(addName, groupName);
                    }
                    if (charWithId != null)
                    {
                        BuddyList.BuddyAddResult buddyAddResult = null;
                        if (channel > 0)
                        {
                            buddyAddResult = WorldBuddyService.getInstance().requestBuddyAdd(addName, c.getChannel(), c.getPlayer().getId(), c.getPlayer().getName(), c.getPlayer().getLevel(),
                                    c.getPlayer().getJob());
                        }
                        else
                        {
                            Connection con = database.DatabaseConnection.getConnection();
                            PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) as buddyCount FROM buddies WHERE characterid = ? AND pending = 0");
                            ps.setInt(1, charWithId.getId());
                            ResultSet rs = ps.executeQuery();
                            if (!rs.next())
                            {
                                ps.close();
                                rs.close();
                                throw new RuntimeException("Result set expected");
                            }
                            int count = rs.getInt("buddyCount");
                            if (count >= charWithId.getBuddyCapacity())
                            {
                                buddyAddResult = BuddyList.BuddyAddResult.好友列表已满;
                            }

                            rs.close();
                            ps.close();

                            ps = con.prepareStatement("SELECT pending FROM buddies WHERE characterid = ? AND buddyid = ?");
                            ps.setInt(1, charWithId.getId());
                            ps.setInt(2, c.getPlayer().getId());
                            rs = ps.executeQuery();
                            if (rs.next())
                            {
                                buddyAddResult = BuddyList.BuddyAddResult.已经是好友关系;
                            }
                            rs.close();
                            ps.close();
                        }
                        if (buddyAddResult == BuddyList.BuddyAddResult.好友列表已满)
                        {
                            c.getSession().write(BuddyListPacket.buddylistMessage(12));
                        }
                        else
                        {
                            int displayChannel = -1;
                            int otherCid = charWithId.getId();
                            if ((buddyAddResult == BuddyList.BuddyAddResult.已经是好友关系) && (channel > 0))
                            {
                                displayChannel = channel;
                                notifyRemoteChannel(c, channel, otherCid, groupName, BuddyList.BuddyOperation.添加好友);
                            }
                            else if (buddyAddResult != BuddyList.BuddyAddResult.已经是好友关系)
                            {
                                Connection con = database.DatabaseConnection.getConnection();
                                PreparedStatement ps = con.prepareStatement("INSERT INTO buddies (`characterid`, `buddyid`, `groupname`, `pending`) VALUES (?, ?, ?, 1)");
                                ps.setInt(1, charWithId.getId());
                                ps.setInt(2, c.getPlayer().getId());
                                ps.setString(3, groupName);
                                ps.executeUpdate();
                                ps.close();
                            }
                            buddylist.put(new BuddylistEntry(charWithId.getName(), otherCid, groupName, displayChannel, true));
                            c.getSession().write(BuddyListPacket.updateBuddylist(buddylist.getBuddies(), 10));
                        }
                    }
                    else
                    {
                        c.getSession().write(BuddyListPacket.buddylistMessage(15));
                    }
                }
                catch (java.sql.SQLException e)
                {
                    System.err.println("SQL THROW" + e);
                }
            }
        }
        else if (mode == 2)
        {
            int otherCid = slea.readInt();
            BuddylistEntry ble = buddylist.get(otherCid);
            if ((!buddylist.isFull()) && (ble != null) && (!ble.isVisible()))
            {
                int channel = WorldFindService.getInstance().findChannel(otherCid);
                buddylist.put(new BuddylistEntry(ble.getName(), otherCid, "未指定群组", channel, true));
                c.getSession().write(BuddyListPacket.updateBuddylist(buddylist.getBuddies(), 10));
                notifyRemoteChannel(c, channel, otherCid, "未指定群组", BuddyList.BuddyOperation.添加好友);
            }
            else
            {
                c.getSession().write(BuddyListPacket.buddylistMessage(11));
            }
        }
        else if (mode == 3)
        {
            int otherCid = slea.readInt();
            BuddylistEntry blz = buddylist.get(otherCid);
            if ((blz != null) && (blz.isVisible()))
            {
                notifyRemoteChannel(c, WorldFindService.getInstance().findChannel(otherCid), otherCid, blz.getGroup(), BuddyList.BuddyOperation.删除好友);
            }
            buddylist.remove(otherCid);
            c.getSession().write(BuddyListPacket.updateBuddylist(buddylist.getBuddies(), 18));
        }
        else if (mode == 6)
        {
            int capacity = c.getPlayer().getBuddyCapacity();
            if ((capacity >= 100) || (c.getPlayer().getMeso() < 50000))
            {
                c.getPlayer().dropMessage(1, "金币不足，或已扩充达到上限。包括基本格数在内，好友目录中只能加入100个好友。您当前的好友数量为: " + capacity);
            }
            else
            {
                int newcapacity = capacity + 5;
                c.getPlayer().gainMeso(-50000, true, true);
                c.getPlayer().setBuddyCapacity((byte) newcapacity);
            }
        }
    }

    private static CharacterIdNameBuddyCapacity getCharacterIdAndNameFromDatabase(String name, String group) throws java.sql.SQLException
    {
        PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("SELECT * FROM characters WHERE name LIKE ?");
        ps.setString(1, name);
        ResultSet rs = ps.executeQuery();
        CharacterIdNameBuddyCapacity ret = null;
        if ((rs.next()) && (rs.getInt("gm") < 3))
        {
            ret = new CharacterIdNameBuddyCapacity(rs.getInt("id"), rs.getString("name"), group, rs.getInt("buddyCapacity"));
        }

        rs.close();
        ps.close();
        return ret;
    }

    private static void notifyRemoteChannel(MapleClient c, int remoteChannel, int otherCid, String group, BuddyList.BuddyOperation operation)
    {
        MapleCharacter player = c.getPlayer();
        if (remoteChannel > 0)
        {
            WorldBuddyService.getInstance().buddyChanged(otherCid, player.getId(), player.getName(), c.getChannel(), operation, group);
        }
    }

    private static final class CharacterIdNameBuddyCapacity extends client.CharacterNameAndId
    {
        private final int buddyCapacity;

        public CharacterIdNameBuddyCapacity(int id, String name, String group, int buddyCapacity)
        {
            super(id, name, group);
            this.buddyCapacity = buddyCapacity;
        }

        public int getBuddyCapacity()
        {
            return this.buddyCapacity;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\BuddyListHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */