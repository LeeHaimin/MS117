package handling.channel.handler;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.RockPaperScissors;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.MapleInventoryType;
import constants.ItemConstants;
import scripting.item.ItemScriptManager;
import scripting.npc.NPCScriptManager;
import scripting.quest.QuestScriptManager;
import server.AutobanManager;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.MapleStorage;
import server.life.MapleNPC;
import server.maps.MapleQuickMove;
import server.quest.MapleQuest;
import server.shop.MapleShop;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.data.output.MaplePacketLittleEndianWriter;
import tools.packet.NPCPacket;

public class NPCHandler
{
    private static final Logger log = Logger.getLogger(NPCHandler.class);


    public static void NPCAnimation(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(handling.SendPacketOpcode.NPC_ACTION.getValue());
        int length = (int) slea.available();
        if (length == 10)
        {
            mplew.writeInt(slea.readInt());
            mplew.writeShort(slea.readShort());
            mplew.writeInt(slea.readInt());
        }
        else if (length > 6)
        {
            mplew.write(slea.read(length - 9));
        }
        else
        {
            return;
        }
        c.getSession().write(mplew.getPacket());
    }


    public static void NPCShop(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        byte bmode = slea.readByte();
        if (chr == null)
        {
            return;
        }
        MapleShop shop;
        int itemId;
        short quantity;
        byte slot;
        switch (bmode)
        {
            case 0:
                shop = chr.getShop();
                if (shop == null)
                {
                    return;
                }
                short position = slea.readShort();
                itemId = slea.readInt();
                quantity = slea.readShort();
                shop.buy(c, itemId, quantity, position);
                break;

            case 1:
                shop = chr.getShop();
                if (shop == null)
                {
                    return;
                }
                slot = (byte) slea.readShort();
                itemId = slea.readInt();
                quantity = slea.readShort();
                shop.sell(c, ItemConstants.getInventoryType(itemId), slot, quantity);
                break;

            case 2:
                shop = chr.getShop();
                if (shop == null)
                {
                    return;
                }
                slot = (byte) slea.readShort();
                shop.recharge(c, slot);
                break;

            default:
                chr.setConversation(0);
        }

    }


    public static void NPCTalk(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.getBattle() != null))
        {
            return;
        }
        MapleNPC npc = chr.getMap().getNPCByOid(slea.readInt());
        if (npc == null)
        {
            return;
        }
        if (chr.hasBlockedInventory())
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (chr.getAntiMacro().inProgress())
        {
            chr.dropMessage(5, "被使用测谎仪时无法操作。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }

        if (npc.hasShop())
        {
            chr.setConversation(1);
            npc.sendShop(c);
        }
        else
        {
            NPCScriptManager.getInstance().start(c, npc.getId());
        }
    }


    public static void QuestAction(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        byte action = slea.readByte();
        int quest = slea.readUShort();

        if (chr == null)
        {
            return;
        }
        if (chr.getAntiMacro().inProgress())
        {
            chr.dropMessage(5, "被使用测谎仪时无法操作。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleQuest q = MapleQuest.getInstance(quest);
        int npc;
        switch (action)
        {
            case 0:
                slea.readInt();
                int itemid = slea.readInt();
                q.RestoreLostItem(chr, itemid);
                break;

            case 1:
                npc = slea.readInt();
                if (!q.hasStartScript())
                {
                    q.start(chr, npc);
                    if (chr.isShowPacket())
                    {
                        chr.dropMessage(6, "开始系统任务 NPC: " + npc + " Quest：" + quest);
                    }
                }

                break;
            case 2:
                npc = slea.readInt();

                slea.readInt();
                if (q.hasEndScript())
                {
                    return;
                }
                if (slea.available() >= 4L)
                {
                    q.complete(chr, npc, slea.readInt());
                }
                else
                {
                    q.complete(chr, npc);
                }
                if (chr.isShowPacket())
                {
                    chr.dropMessage(6, "完成系统任务 NPC: " + npc + " Quest: " + quest);
                }


                break;
            case 3:
                if (constants.GameConstants.canForfeit(q.getId()))
                {
                    q.forfeit(chr);
                    if (chr.isShowPacket())
                    {
                        chr.dropMessage(6, "放弃系统任务 Quest: " + quest);
                    }
                }
                else
                {
                    chr.dropMessage(1, "无法放弃这个任务.");
                }
                break;

            case 4:
                npc = slea.readInt();
                if (chr.hasBlockedInventory())
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }

                QuestScriptManager.getInstance().startQuest(c, npc, quest);
                if ((chr.isAdmin()) && (server.ServerProperties.ShowPacket()))
                {
                    chr.dropMessage(6, "执行脚本任务 NPC：" + npc + " Quest: " + quest);
                }

                break;
            case 5:
                npc = slea.readInt();
                if (chr.hasBlockedInventory())
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }

                QuestScriptManager.getInstance().endQuest(c, npc, quest, false);
                c.getSession().write(MaplePacketCreator.showSpecialEffect(13));
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.showSpecialEffect(chr.getId(), 13), false);
                if (chr.isShowPacket())
                {
                    chr.dropMessage(6, "完成脚本任务 NPC：" + npc + " Quest: " + quest);
                }

                break;
        }

    }


    public static void Storage(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        byte mode = slea.readByte();
        if (chr == null)
        {
            return;
        }
        if (chr.getAntiMacro().inProgress())
        {
            chr.dropMessage(5, "被使用测谎仪时无法操作。");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleStorage storage = chr.getStorage();
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        byte slot;
        Item item;
        int meso;
        switch (mode)
        {
            case 4:
                byte type = slea.readByte();
                slot = slea.readByte();
                slot = storage.getSlot(MapleInventoryType.getByType(type), slot);
                item = storage.getItem(slot);
                if (item != null)
                {
                    if ((ii.isPickupRestricted(item.getItemId())) && (chr.getItemQuantity(item.getItemId(), true) > 0))
                    {
                        c.getSession().write(NPCPacket.getStorageError((byte) 12));
                        return;
                    }

                    meso = (storage.getNpcId() == 9030100) || (storage.getNpcId() == 9031016) ? 1000 : 0;
                    if (chr.getMeso() < meso)
                    {
                        c.getSession().write(NPCPacket.getStorageError((byte) 11));
                        return;
                    }

                    if (MapleInventoryManipulator.checkSpace(c, item.getItemId(), item.getQuantity(), item.getOwner()))
                    {
                        item = storage.takeOut(slot);
                        short flag = item.getFlag();
                        if (ItemFlag.KARMA_EQ.check(flag))
                        {
                            item.setFlag((short) (flag - ItemFlag.KARMA_EQ.getValue()));
                        }
                        else if (ItemFlag.KARMA_USE.check(flag))
                        {
                            item.setFlag((short) (flag - ItemFlag.KARMA_USE.getValue()));
                        }
                        else if (ItemFlag.KARMA_ACC.check(flag))
                        {
                            item.setFlag((short) (flag - ItemFlag.KARMA_ACC.getValue()));
                        }
                        else if (ItemFlag.KARMA_ACC_USE.check(flag))
                        {
                            item.setFlag((short) (flag - ItemFlag.KARMA_ACC_USE.getValue()));
                        }
                        MapleInventoryManipulator.addFromDrop(c, item, false);
                        if (meso > 0)
                        {
                            chr.gainMeso(-meso, false);
                        }
                        storage.sendTakenOut(c, ItemConstants.getInventoryType(item.getItemId()));
                    }
                    else
                    {
                        c.getSession().write(NPCPacket.getStorageError((byte) 10));
                    }
                }
                else
                {
                    log.info("[作弊] " + chr.getName() + " (等级 " + chr.getLevel() + ") 试图从仓库取出不存在的道具.");
                    handling.world.WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6, "[GM Message] 玩家: " + chr.getName() + " (等级 " + chr.getLevel() + ") " +
                            "试图从仓库取出不存在的道具."));
                    c.getSession().write(MaplePacketCreator.enableActions());
                }
                break;

            case 5:
                slot = (byte) slea.readShort();
                int itemId = slea.readInt();
                short quantity = slea.readShort();

                if (quantity < 1)
                {
                    AutobanManager.getInstance().autoban(c, "试图存入到仓库的道具数量: " + quantity + " 道具ID: " + itemId);
                    return;
                }

                if (storage.isFull())
                {
                    c.getSession().write(NPCPacket.getStorageError((byte) 17));
                    return;
                }

                MapleInventoryType mType = ItemConstants.getInventoryType(itemId);
                if (chr.getInventory(mType).getItem(slot) == null)
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }

                meso = (storage.getNpcId() == 9030100) || (storage.getNpcId() == 9031016) ? 500 : 100;
                if (chr.getMeso() < meso)
                {
                    c.getSession().write(NPCPacket.getStorageError((byte) 16));
                    return;
                }

                item = chr.getInventory(mType).getItem(slot).copy();

                if (ItemConstants.isPet(item.getItemId()))
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }

                if ((ii.isPickupRestricted(item.getItemId())) && (storage.findById(item.getItemId()) != null))
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                if ((item.getItemId() == itemId) && ((item.getQuantity() >= quantity) || (ItemConstants.isRechargable(itemId))))
                {
                    if (ItemConstants.isRechargable(itemId))
                    {
                        quantity = item.getQuantity();
                    }
                    chr.gainMeso(-meso, false, false);
                    MapleInventoryManipulator.removeFromSlot(c, mType, slot, quantity, false);
                    item.setQuantity(quantity);
                    storage.store(item);
                    storage.sendStored(c, ItemConstants.getInventoryType(itemId));
                }
                else
                {
                    AutobanManager.getInstance().addPoints(c, 1000, 0L, "试图存入到仓库的道具: " + itemId + " 数量: " + quantity + " 当前玩家用道具: " + item.getItemId() + " 数量: " + item.getQuantity());
                }
                break;

            case 6:
                storage.arrange();
                storage.update(c);
                break;

            case 7:
                meso = slea.readInt();
                int storageMesos = storage.getMeso();
                int playerMesos = chr.getMeso();
                if (((meso > 0) && (storageMesos >= meso)) || ((meso < 0) && (playerMesos >= -meso)))
                {
                    if ((meso < 0) && (storageMesos - meso < 0))
                    {
                        meso = -(Integer.MAX_VALUE - storageMesos);
                        if (-meso <= playerMesos)
                        {
                        }

                    }
                    else if ((meso > 0) && (playerMesos + meso < 0))
                    {
                        meso = Integer.MAX_VALUE - playerMesos;
                        if (meso > storageMesos)
                        {
                            return;
                        }
                    }
                    storage.setMeso(storageMesos - meso);
                    chr.gainMeso(meso, false, false);
                }
                else
                {
                    AutobanManager.getInstance().addPoints(c, 1000, 0L,
                            "Trying to store or take out unavailable amount of mesos (" + meso + "/" + storage.getMeso() + "/" + c.getPlayer().getMeso() + ")");
                    return;
                }
                storage.sendMeso(c);
                break;

            case 8:
                storage.close();
                chr.setConversation(0);
                break;

            default:
                System.out.println("Unhandled Storage mode : " + mode);
        }

    }


    public static void NPCMoreTalk(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MapleCharacter player = c.getPlayer();
        if (player == null)
        {
            return;
        }
        if (player.getConversation() != 1)
        {
            return;
        }
        byte lastMsg = slea.readByte();
        if (lastMsg == 9)
        {
            slea.skip(2);
        }
        byte action = slea.readByte();
        if (lastMsg == 3)
        {
            if (action != 0)
            {
                String returnText = slea.readMapleAsciiString();
                if ((!player.isShowPacket()) ||


                        (c.getQM() != null))
                {
                    c.getQM().setGetText(returnText);
                    if (c.getQM().isStart())
                    {
                        QuestScriptManager.getInstance().startAction(c, action, lastMsg, -1);
                    }
                    else
                    {
                        QuestScriptManager.getInstance().endAction(c, action, lastMsg, -1);
                    }
                }
                else if (c.getIM() != null)
                {
                    c.getIM().setGetText(returnText);
                    ItemScriptManager.getInstance().action(c, action, lastMsg, -1);
                }
                else if (c.getCM() != null)
                {
                    c.getCM().setGetText(returnText);
                    NPCScriptManager.getInstance().action(c, action, lastMsg, -1);
                }
            }
            else if (c.getQM() != null)
            {
                c.getQM().dispose();
            }
            else if (c.getIM() != null)
            {
                c.getIM().dispose();
            }
            else if (c.getCM() != null)
            {
                c.getCM().dispose();
            }
        }
        else
        {
            int selection = -1;
            if (slea.available() >= 4L)
            {
                selection = slea.readInt();
            }
            else if (slea.available() > 0L)
            {
                selection = slea.readByte();
            }
            if ((!player.isShowPacket()) || (


                    (selection >= -1) && (action != -1)))
            {
                if (c.getQM() != null)
                {
                    if (c.getQM().isStart())
                    {
                        QuestScriptManager.getInstance().startAction(c, action, lastMsg, selection);
                    }
                    else
                    {
                        QuestScriptManager.getInstance().endAction(c, action, lastMsg, selection);
                    }
                }
                else if (c.getIM() != null)
                {
                    ItemScriptManager.getInstance().action(c, action, lastMsg, selection);
                }
                else if (c.getCM() != null)
                {
                    NPCScriptManager.getInstance().action(c, action, lastMsg, selection);
                }
            }
            else if (c.getQM() != null)
            {
                c.getQM().dispose();
            }
            else if (c.getIM() != null)
            {
                c.getIM().dispose();
            }
            else if (c.getCM() != null)
            {
                c.getCM().dispose();
            }
        }
    }


    public static void repairAll(MapleClient c)
    {
        int price = 0;

        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        Map<Equip, Integer> eqs = new HashMap<>();
        MapleInventoryType[] types = {MapleInventoryType.EQUIP, MapleInventoryType.EQUIPPED};
        for (MapleInventoryType type : types)
        {
            for (Item item : c.getPlayer().getInventory(type).newList())
            {
                if ((item instanceof Equip))
                {
                    Equip eq = (Equip) item;
                    if (eq.getDurability() >= 0)
                    {
                        Map<String, Integer> eqStats = ii.getEquipStats(eq.getItemId());
                        if ((eqStats.containsKey("durability")) && (eqStats.get("durability") > 0) && (eq.getDurability() < eqStats.get("durability")))
                        {
                            double rPercentage = 100.0D - Math.ceil(eq.getDurability() * 1000.0D / (eqStats.get("durability") * 10.0D));
                            eqs.put(eq, eqStats.get("durability"));
                            price += (int) Math.ceil(rPercentage * ii.getPrice(eq.getItemId()) / (ii.getReqLevel(eq.getItemId()) < 70 ? 100.0D : 1.0D));
                        }
                    }
                }
            }
        }
        if ((eqs.size() <= 0) || (c.getPlayer().getMeso() < price))
        {
            return;
        }
        c.getPlayer().gainMeso(-price, true);

        for (Object eqqz : eqs.entrySet())
        {
            Equip ez = (Equip) ((Map.Entry) eqqz).getKey();
            ez.setDurability((Integer) ((Map.Entry) eqqz).getValue());
            c.getPlayer().forceUpdateItem(ez.copy());
        }
    }


    public static void repair(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (slea.available() < 4L)
        {
            return;
        }
        int position = slea.readInt();
        MapleInventoryType type = position < 0 ? MapleInventoryType.EQUIPPED : MapleInventoryType.EQUIP;
        Item item = c.getPlayer().getInventory(type).getItem((byte) position);
        if (item == null)
        {
            return;
        }
        Equip eq = (Equip) item;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        Map<String, Integer> eqStats = ii.getEquipStats(item.getItemId());
        if ((eq.getDurability() < 0) || (!eqStats.containsKey("durability")) || (eqStats.get("durability") <= 0) || (eq.getDurability() >= eqStats.get("durability")))
        {
            return;
        }
        double rPercentage = 100.0D - Math.ceil(eq.getDurability() * 1000.0D / (eqStats.get("durability") * 10.0D));


        int price = (int) Math.ceil(rPercentage * ii.getPrice(eq.getItemId()) / (ii.getReqLevel(eq.getItemId()) < 70 ? 100.0D : 1.0D));

        if (c.getPlayer().getMeso() < price)
        {
            return;
        }
        c.getPlayer().gainMeso(-price, false);
        eq.setDurability(eqStats.get("durability"));
        c.getPlayer().forceUpdateItem(eq.copy());
    }


    public static void UpdateQuest(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MapleQuest quest = MapleQuest.getInstance(slea.readShort());
        if (quest != null)
        {
            c.getPlayer().updateQuest(c.getPlayer().getQuest(quest), true);
        }
    }

    public static void UseItemQuest(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        short slot = slea.readShort();
        int itemId = slea.readInt();
        Item item = c.getPlayer().getInventory(MapleInventoryType.ETC).getItem(slot);
        int qid = slea.readInt();
        MapleQuest quest = MapleQuest.getInstance(qid);
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        Pair<Integer, java.util.List<Integer>> questItemInfo = null;
        boolean found = false;
        for (Item i : c.getPlayer().getInventory(MapleInventoryType.ETC))
        {
            if (i.getItemId() / 10000 == 422)
            {
                questItemInfo = ii.questItemInfo(i.getItemId());
                if ((questItemInfo != null) && (questItemInfo.getLeft() == qid) && (questItemInfo.getRight() != null) && (questItemInfo.getRight().contains(itemId)))
                {
                    found = true;
                    break;
                }
            }
        }
        if ((quest != null) && (found) && (item != null) && (item.getQuantity() > 0) && (item.getItemId() == itemId))
        {
            int newData = slea.readInt();
            client.MapleQuestStatus stats = c.getPlayer().getQuestNoAdd(quest);
            if ((stats != null) && (stats.getStatus() == 1))
            {
                stats.setCustomData(String.valueOf(newData));
                c.getPlayer().updateQuest(stats, true);
                MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.ETC, slot, (short) 1, false);
            }
        }
    }

    public static void RPSGame(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((slea.available() == 0L) || (c.getPlayer() == null) || (c.getPlayer().getMap() == null) || (!c.getPlayer().getMap().containsNPC(9000019)))
        {
            if ((c.getPlayer() != null) && (c.getPlayer().getRPS() != null))
            {
                c.getPlayer().getRPS().dispose(c);
            }
            return;
        }
        byte mode = slea.readByte();
        switch (mode)
        {
            case 0:
            case 5:
                if (c.getPlayer().getRPS() != null)
                {
                    c.getPlayer().getRPS().reward(c);
                }
                if (c.getPlayer().getMeso() >= 1000)
                {
                    c.getPlayer().setRPS(new RockPaperScissors(c, mode));
                }
                else
                {
                    c.getSession().write(MaplePacketCreator.getRPSMode((byte) 8, -1, -1, -1));
                }
                break;
            case 1:
                if ((c.getPlayer().getRPS() == null) || (!c.getPlayer().getRPS().answer(c, slea.readByte())))
                {
                    c.getSession().write(MaplePacketCreator.getRPSMode((byte) 13, -1, -1, -1));
                }
                break;
            case 2:
                if ((c.getPlayer().getRPS() == null) || (!c.getPlayer().getRPS().timeOut(c)))
                {
                    c.getSession().write(MaplePacketCreator.getRPSMode((byte) 13, -1, -1, -1));
                }
                break;
            case 3:
                if ((c.getPlayer().getRPS() == null) || (!c.getPlayer().getRPS().nextRound(c)))
                {
                    c.getSession().write(MaplePacketCreator.getRPSMode((byte) 13, -1, -1, -1));
                }
                break;
            case 4:
                if (c.getPlayer().getRPS() != null)
                {
                    c.getPlayer().getRPS().dispose(c);
                }
                else
                {
                    c.getSession().write(MaplePacketCreator.getRPSMode((byte) 13, -1, -1, -1));
                }

                break;
        }

    }


    public static void OpenQuickMoveNpc(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int npcid = slea.readInt();
        if ((c.getPlayer().hasBlockedInventory()) || (c.getPlayer().isInBlockedMap()) || (c.getPlayer().getLevel() < 10))
        {
            c.getPlayer().dropMessage(-1, "您当前已经和1个NPC对话了. 如果不是请输入 @ea 命令进行解卡。");
            return;
        }
        for (MapleQuickMove pn : MapleQuickMove.values())
        {
            if (pn.getNpcId() == npcid)
            {
                NPCScriptManager.getInstance().start(c, npcid);
                break;
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\NPCHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */