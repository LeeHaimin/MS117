package handling.channel.handler;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.Skill;
import client.SkillEntry;
import client.SkillFactory;
import constants.GameConstants;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;


public class PhantomMemorySkill
{
    public static void MemorySkillChoose(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null) || (!GameConstants.is幻影(c.getPlayer().getJob())))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int skillId = slea.readInt();
        Skill skill = SkillFactory.getSkill(skillId);
        if (skill == null)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int teachId = slea.readInt();
        Skill theskill = SkillFactory.getSkill(teachId);
        if ((theskill == null) && (teachId != 0))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (c.getPlayer().getSkillLevel(skillId) > 0)
        {
            c.getPlayer().修改幻影装备技能(skillId, teachId);
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void MemorySkillChange(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null) || (!GameConstants.is幻影(c.getPlayer().getJob())))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int skillId = slea.readInt();
        int targetId = slea.readInt();
        byte type = slea.readByte();
        switch (type)
        {
            case 0:
                MapleCharacter target = c.getChannelServer().getPlayerStorage().getCharacterById(targetId);
                if ((target != null) && (target.getSkillLevel(skillId) > 0))
                {
                    int skillBook = SkillFactory.getIdFromSkillId(skillId);
                    if (skillBook > 0)
                    {
                        c.getPlayer().幻影技能复制(skillBook, skillId, target.getSkillLevel(skillId));
                    }
                    else
                    {
                        c.getPlayer().dropMessage(1, "复制技能出现错误");
                    }
                }
                else
                {
                    c.getSession().write(MaplePacketCreator.幻影复制错误());
                }
                break;
            case 1:
                c.getPlayer().幻影删除技能(skillId);
                Skill skill = SkillFactory.getSkill(skillId);
                if ((skill != null) && (skill.isBuffSkill()))
                {
                    c.getPlayer().cancelEffect(skill.getEffect(1), false, -1L);
                }
                break;
        }

        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void MemorySkillObtain(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null) || (!GameConstants.is幻影(c.getPlayer().getJob())))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MapleCharacter target = c.getChannelServer().getPlayerStorage().getCharacterById(slea.readInt());
        if (target != null)
        {
            List<Integer> memorySkills = new LinkedList<>();
            Map<Skill, SkillEntry> skills = target.getSkills();
            for (Map.Entry<Skill, SkillEntry> skill : skills.entrySet())
            {
                if ((SkillFactory.isMemorySkill(skill.getKey().getId())) && (target.getSkillLevel(skill.getKey()) > 0))
                {
                    memorySkills.add(skill.getKey().getId());
                }
            }
            if (!memorySkills.isEmpty())
            {
                c.getSession().write(MaplePacketCreator.封印之瞳(target, memorySkills));
            }
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\PhantomMemorySkill.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */