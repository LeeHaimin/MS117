package handling.channel.handler;

import java.util.ArrayList;
import java.util.List;

import client.MapleCharacter;
import client.MapleClient;
import constants.GameConstants;
import handling.channel.ChannelServer;
import handling.world.PartyOperation;
import handling.world.WorldFindService;
import handling.world.WrodlPartyService;
import handling.world.party.ExpeditionType;
import handling.world.party.MapleExpedition;
import handling.world.party.MapleParty;
import handling.world.party.MaplePartyCharacter;
import handling.world.party.PartySearch;
import handling.world.party.PartySearchType;
import server.maps.events.Event_DojoAgent;
import server.quest.MapleQuest;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.PartyPacket;

public class PartyHandler
{
    public static void DenyPartyRequest(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int action = slea.readByte();
        int partyId = slea.readInt();
        WrodlPartyService partyService = WrodlPartyService.getInstance();
        if ((c.getPlayer().getParty() == null) && (c.getPlayer().getQuestNoAdd(MapleQuest.getInstance(122901)) == null))
        {
            MapleParty party = partyService.getParty(partyId);
            if (party != null)
            {
                if (party.getExpeditionId() > 0)
                {
                    c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                    return;
                }
                switch (action)
                {
                    case 31:
                    case 33:
                        break;
                    case 35:
                    case 58:
                        MapleCharacter cfrom = c.getChannelServer().getPlayerStorage().getCharacterById(party.getLeader().getId());
                        if (cfrom == null) break;
                        cfrom.dropMessage(5, "'" + c.getPlayer().getName() + "'玩家拒绝了组队招待。");
                        break;


                    case 36:
                    case 59:
                        if (party.getMembers().size() < 6)
                        {
                            c.getPlayer().setParty(party);
                            partyService.updateParty(partyId, PartyOperation.加入队伍, new MaplePartyCharacter(c.getPlayer()));
                            c.getPlayer().receivePartyMemberHP();
                            c.getPlayer().updatePartyMemberHP();
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "组队成员已满");
                        }
                        break;
                    default:
                        System.out.println("第二方收到组队邀请处理( 0x" + tools.StringUtil.getLeftPaddedStr(Integer.toHexString(action).toUpperCase(), '0', 2) + " ) 未知.");
                        break;
                }
            }
            else
            {
                c.getPlayer().dropMessage(5, "要参加的队伍不存在。");
            }
        }
        else
        {
            c.getPlayer().dropMessage(5, "您已经有一个组队，无法加入其他组队!");
        }
    }

    public static void PartyOperation(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int operation = slea.readByte();
        MapleParty party = c.getPlayer().getParty();
        WrodlPartyService partyService = WrodlPartyService.getInstance();
        MaplePartyCharacter partyPlayer = new MaplePartyCharacter(c.getPlayer());
        switch (operation)
        {
            case 1:
                if (party == null)
                {
                    party = partyService.createParty(partyPlayer);
                    c.getPlayer().setParty(party);
                    c.getSession().write(PartyPacket.partyCreated(party.getId()));
                }
                else
                {
                    if (party.getExpeditionId() > 0)
                    {
                        c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                        return;
                    }
                    if ((partyPlayer.equals(party.getLeader())) && (party.getMembers().size() == 1))
                    {
                        c.getSession().write(PartyPacket.partyCreated(party.getId()));
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "你已经存在一个队伍中，无法创建！");
                    }
                }
                break;
            case 2:
                if (party != null)
                {
                    if (party.getExpeditionId() > 0)
                    {
                        c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                        return;
                    }
                    if (partyPlayer.equals(party.getLeader()))
                    {
                        if (GameConstants.isDojo(c.getPlayer().getMapId()))
                        {
                            Event_DojoAgent.failed(c.getPlayer());
                        }
                        if (c.getPlayer().getPyramidSubway() != null)
                        {
                            c.getPlayer().getPyramidSubway().fail(c.getPlayer());
                        }
                        partyService.updateParty(party.getId(), PartyOperation.解散队伍, partyPlayer);
                        if (c.getPlayer().getEventInstance() != null)
                        {
                            c.getPlayer().getEventInstance().disbandParty();
                        }
                    }
                    else
                    {
                        if (GameConstants.isDojo(c.getPlayer().getMapId()))
                        {
                            Event_DojoAgent.failed(c.getPlayer());
                        }
                        if (c.getPlayer().getPyramidSubway() != null)
                        {
                            c.getPlayer().getPyramidSubway().fail(c.getPlayer());
                        }
                        partyService.updateParty(party.getId(), PartyOperation.离开队伍, partyPlayer);
                        if (c.getPlayer().getEventInstance() != null)
                        {
                            c.getPlayer().getEventInstance().leftParty(c.getPlayer());
                        }
                    }
                    c.getPlayer().setParty(null);
                }
                break;
            case 3:
                int partyid = slea.readInt();
                if (party == null)
                {
                    party = partyService.getParty(partyid);
                    if (party != null)
                    {
                        if (party.getExpeditionId() > 0)
                        {
                            c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                            return;
                        }
                        if ((party.getMembers().size() < 6) && (c.getPlayer().getQuestNoAdd(MapleQuest.getInstance(122901)) == null))
                        {
                            c.getPlayer().setParty(party);
                            partyService.updateParty(party.getId(), PartyOperation.加入队伍, partyPlayer);
                            c.getPlayer().receivePartyMemberHP();
                            c.getPlayer().updatePartyMemberHP();
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "组队成员已满");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "要加入的队伍不存在");
                    }
                }
                else
                {
                    c.getPlayer().dropMessage(5, "您已经有一个组队，无法加入其他组队!");
                }
                break;
            case 4:
                if (party == null)
                {
                    party = partyService.createParty(partyPlayer);
                    c.getPlayer().setParty(party);
                    c.getSession().write(PartyPacket.partyCreated(party.getId()));
                }
                String theName = slea.readMapleAsciiString();
                int theCh = WorldFindService.getInstance().findChannel(theName);
                if (theCh > 0)
                {
                    MapleCharacter invited = ChannelServer.getInstance(theCh).getPlayerStorage().getCharacterByName(theName);
                    if (invited != null)
                    {
                        if (party.getExpeditionId() > 0)
                        {
                            c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                        }
                        else if (invited.getParty() != null)
                        {
                            c.getPlayer().dropMessage(5, "'" + theName + "'已经加入其他组。");
                        }
                        else if (invited.getQuestNoAdd(MapleQuest.getInstance(122901)) != null)
                        {
                            c.getPlayer().dropMessage(5, "'" + theName + "'玩家处于拒绝组队状态。");
                        }
                        else if (party.getMembers().size() < 6)
                        {
                            c.getSession().write(PartyPacket.partyStatusMessage(31, invited.getName()));
                            invited.getClient().getSession().write(PartyPacket.partyInvite(c.getPlayer()));
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "组队成员已满");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "在当前服务器找不到..'" + theName + "'。");
                    }
                }
                else
                {
                    c.getPlayer().dropMessage(5, "在当前服务器找不到..'" + theName + "'。");
                }
                break;
            case 6:
                if ((party != null) && (partyPlayer != null) && (partyPlayer.equals(party.getLeader())))
                {
                    if (party.getExpeditionId() > 0)
                    {
                        c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                        return;
                    }
                    MaplePartyCharacter expelled = party.getMemberById(slea.readInt());
                    if (expelled != null)
                    {
                        if ((GameConstants.isDojo(c.getPlayer().getMapId())) && (expelled.isOnline()))
                        {
                            Event_DojoAgent.failed(c.getPlayer());
                        }
                        if ((c.getPlayer().getPyramidSubway() != null) && (expelled.isOnline()))
                        {
                            c.getPlayer().getPyramidSubway().fail(c.getPlayer());
                        }
                        partyService.updateParty(party.getId(), PartyOperation.驱逐成员, expelled);
                        if ((c.getPlayer().getEventInstance() != null) && (expelled.isOnline()))
                        {
                            c.getPlayer().getEventInstance().disbandParty();
                        }
                    }
                }
                break;

            case 7:
                if (party != null)
                {
                    if (party.getExpeditionId() > 0)
                    {
                        c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                        return;
                    }
                    MaplePartyCharacter newleader = party.getMemberById(slea.readInt());
                    if ((newleader != null) && (partyPlayer.equals(party.getLeader()))) partyService.updateParty(party.getId(), PartyOperation.改变队长, newleader);
                }
                break;

            case 8:
                if (party != null)
                {
                    if ((c.getPlayer().getEventInstance() != null) || (c.getPlayer().getPyramidSubway() != null) || (party.getExpeditionId() > 0) || (GameConstants.isDojo(c.getPlayer().getMapId())))
                    {
                        c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                        return;
                    }
                    if (partyPlayer.equals(party.getLeader()))
                    {
                        partyService.updateParty(party.getId(), PartyOperation.解散队伍, partyPlayer);
                    }
                    else
                    {
                        partyService.updateParty(party.getId(), PartyOperation.离开队伍, partyPlayer);
                    }
                    c.getPlayer().setParty(null);
                }
                int toPartyId = slea.readInt();
                if (party == null)
                {
                    party = partyService.getParty(toPartyId);
                    if ((party != null) && (party.getMembers().size() < 6))
                    {
                        if (party.getExpeditionId() > 0)
                        {
                            c.getPlayer().dropMessage(5, "加入远征队伍的状态下无法进行此操作。");
                            return;
                        }
                        MapleCharacter cfrom = c.getPlayer().getMap().getCharacterById(party.getLeader().getId());
                        if ((cfrom != null) && (cfrom.getQuestNoAdd(MapleQuest.getInstance(122900)) == null))
                        {
                            c.getSession().write(PartyPacket.partyStatusMessage(65, c.getPlayer().getName()));
                            cfrom.getClient().getSession().write(PartyPacket.partyRequestInvite(c.getPlayer()));
                        }
                        else
                        {
                            c.getPlayer().dropMessage(5, "没有在该地图找此队伍的队长.");
                        }
                    }
                    else
                    {
                        c.getPlayer().dropMessage(5, "要加入的队伍不存在或者人数已满");
                    }
                }
                else
                {
                    c.getPlayer().dropMessage(5, "无法找到组队。请重新确认组队信息。");
                }
                break;
            case 9:
                if (slea.readByte() > 0)
                {
                    c.getPlayer().getQuestRemove(MapleQuest.getInstance(122900));
                }
                else
                {
                    c.getPlayer().getQuestNAdd(MapleQuest.getInstance(122900));
                }
                break;
            case 5:
            default:
                if (server.ServerProperties.ShowPacket())
                {
                    System.out.println("组队邀请处理( " + operation + " ) 未知.");
                }
                break;
        }
    }

    public static void AllowPartyInvite(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (slea.readByte() > 0)
        {
            c.getPlayer().getQuestRemove(MapleQuest.getInstance(122901));
        }
        else
        {
            c.getPlayer().getQuestNAdd(MapleQuest.getInstance(122901));
        }
    }

    public static void DenySidekickRequest(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int action = slea.readByte();
        int cid = slea.readInt();
        if ((c.getPlayer().getSidekick() == null) && (action == 90))
        {
            MapleCharacter party = c.getPlayer().getMap().getCharacterById(cid);
            if (party != null)
            {
                if ((party.getSidekick() != null) || (!handling.world.sidekick.MapleSidekick.checkLevels(c.getPlayer().getLevel(), party.getLevel())))
                {
                    return;
                }
                int sid = handling.world.WorldSidekickService.getInstance().createSidekick(c.getPlayer().getId(), party.getId());
                if (sid <= 0)
                {
                    c.getPlayer().dropMessage(5, "Please try again.");
                }
                else
                {
                    handling.world.sidekick.MapleSidekick s = handling.world.WorldSidekickService.getInstance().getSidekick(sid);
                    c.getPlayer().setSidekick(s);
                    c.getSession().write(PartyPacket.updateSidekick(c.getPlayer(), s, true));
                    party.setSidekick(s);
                    party.getClient().getSession().write(PartyPacket.updateSidekick(party, s, true));
                }
            }
            else
            {
                c.getPlayer().dropMessage(5, "The sidekick you are trying to join does not exist");
            }
        }
    }

    public static void SidekickOperation(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int operation = slea.readByte();
        switch (operation)
        {
            case 65:
                if (c.getPlayer().getSidekick() == null)
                {
                    MapleCharacter other = c.getPlayer().getMap().getCharacterByName(slea.readMapleAsciiString());
                    if ((other.getSidekick() == null) && (handling.world.sidekick.MapleSidekick.checkLevels(c.getPlayer().getLevel(), other.getLevel())))
                    {
                        other.getClient().getSession().write(PartyPacket.sidekickInvite(c.getPlayer()));
                        c.getPlayer().dropMessage(1, "You have sent the sidekick invite to " + other.getName() + ".");
                    }
                }
                break;

            case 63:
                if (c.getPlayer().getSidekick() != null)
                {
                    c.getPlayer().getSidekick().eraseToDB();
                }

                break;
        }

    }

    public static void MemberSearch(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer().isInBlockedMap()) || (server.maps.FieldLimitType.VipRock.check(c.getPlayer().getMap().getFieldLimit())))
        {
            c.getPlayer().dropMessage(5, "无法在这个地方进行搜索.");
            c.getSession().write(tools.MaplePacketCreator.enableActions());
            return;
        }
        List<MapleCharacter> members = new ArrayList<>();
        for (MapleCharacter findchr : c.getPlayer().getMap().getCharactersThreadsafe())
        {
            if ((findchr != null) && (findchr.getId() != c.getPlayer().getId()) && (!members.contains(c.getPlayer())) && (!findchr.isHidden()))
            {
                members.add(findchr);
            }
        }
        c.getSession().write(PartyPacket.showMemberSearch(members));
    }


    public static void PartySearch(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if ((c.getPlayer().isInBlockedMap()) || (server.maps.FieldLimitType.VipRock.check(c.getPlayer().getMap().getFieldLimit())))
        {
            c.getPlayer().dropMessage(5, "无法在这个地方进行搜索.");
            c.getSession().write(tools.MaplePacketCreator.enableActions());
            return;
        }
        MapleParty party = c.getPlayer().getParty();
        List<MapleParty> parties = new ArrayList<>();
        for (MapleCharacter findchr : c.getPlayer().getMap().getCharactersThreadsafe())
        {
            if ((findchr.getParty() != null) && (!findchr.isHidden()))
            {
                if ((party == null) || (findchr.getParty().getId() != party.getId()))
                {

                    if (!parties.contains(findchr.getParty())) parties.add(findchr.getParty());
                }
            }
        }
        c.getSession().write(PartyPacket.showPartySearch(parties));
    }

    public static void PartyListing(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int mode = slea.readByte();
        WrodlPartyService partyService = WrodlPartyService.getInstance();


        switch (mode)
        {
            case 111:
                int typeId = slea.readInt();
                PartySearchType pst = PartySearchType.getById(typeId);
                if ((pst == null) || (c.getPlayer().getLevel() > pst.maxLevel) || (c.getPlayer().getLevel() < pst.minLevel))
                {
                    System.out.println("创建远征队信息不符合条件 - 类型: " + (pst == null) + " ID: " + typeId);
                    return;
                }
                if ((c.getPlayer().getParty() == null) && (partyService.searchParty(pst).size() < 10))
                {
                    MapleParty party = partyService.createParty(new MaplePartyCharacter(c.getPlayer()), pst.id);
                    c.getPlayer().setParty(party);
                    c.getSession().write(PartyPacket.partyCreated(party.getId()));
                    PartySearch ps = new PartySearch(slea.readMapleAsciiString(), pst.exped ? party.getExpeditionId() : party.getId(), pst);
                    partyService.addSearch(ps);
                    if (pst.exped)
                    {
                        c.getSession().write(PartyPacket.expeditionStatus(partyService.getExped(party.getExpeditionId()), true));
                    }
                    c.getSession().write(PartyPacket.partyListingAdded(ps));
                }
                else
                {
                    c.getPlayer().dropMessage(1, "您已经有个1个队伍了，请离开队伍后在进行尝试。");
                }
                break;
            case 112:
                MapleParty party = c.getPlayer().getParty();
                if (party != null)
                {
                    PartySearch toRemove = partyService.getSearchByParty(party.getId());
                    if (toRemove != null)
                    {
                        partyService.removeSearch(toRemove, "组队广告已被删除。");
                    }
                    else
                    {
                        System.out.println("取消添加远征广告信息 - 广告信息为空");
                    }
                }
                else
                {
                    System.out.println("取消添加远征广告信息 - 是否有队伍: " + (party != null));
                }
                break;
            case 113:
                int typeId1 = slea.readInt();
                PartySearchType pst1 = PartySearchType.getById(typeId1);
                if ((pst1 == null) || (c.getPlayer().getLevel() > pst1.maxLevel) || (c.getPlayer().getLevel() < pst1.minLevel))
                {
                    System.out.println("显示远征队信息不符合条件 - 类型是否为空: " + (pst1 == null) + " ID: " + typeId1);
                    return;
                }
                c.getSession().write(PartyPacket.getPartyListing(pst1));
                break;
            case 114:
                break;
            case 115:
                MapleParty party1 = c.getPlayer().getParty();
                MaplePartyCharacter partyPlayer = new MaplePartyCharacter(c.getPlayer());
                if (party1 == null)
                {
                    int theId = slea.readInt();
                    party1 = partyService.getParty(theId);
                    if (party1 != null)
                    {
                        PartySearch ps = partyService.getSearchByParty(party1.getId());
                        if ((ps != null) && (c.getPlayer().getLevel() <= ps.getType().maxLevel) && (c.getPlayer().getLevel() >= ps.getType().minLevel) && (party1.getMembers().size() < 6))
                        {
                            c.getPlayer().setParty(party1);
                            partyService.updateParty(party1.getId(), PartyOperation.加入队伍, partyPlayer);
                            c.getPlayer().receivePartyMemberHP();
                            c.getPlayer().updatePartyMemberHP();
                        }
                        else
                        {
                            c.getSession().write(PartyPacket.partyStatusMessage(17));
                        }
                    }
                    else
                    {
                        MapleExpedition exped = partyService.getExped(theId);
                        if (exped != null)
                        {
                            PartySearch ps = partyService.getSearchByExped(exped.getId());
                            if ((ps != null) && (c.getPlayer().getLevel() <= ps.getType().maxLevel) && (c.getPlayer().getLevel() >= ps.getType().minLevel) && (exped.getAllMembers() < exped.getType().maxMembers))
                            {
                                int partyId = exped.getFreeParty();
                                if (partyId < 0)
                                {
                                    c.getSession().write(PartyPacket.partyStatusMessage(17));
                                }
                                else if (partyId == 0)
                                {
                                    party1 = partyService.createPartyAndAdd(partyPlayer, exped.getId());
                                    c.getPlayer().setParty(party1);
                                    c.getSession().write(PartyPacket.partyCreated(party1.getId()));
                                    c.getSession().write(PartyPacket.expeditionStatus(exped, true));
                                    partyService.sendExpedPacket(exped.getId(), PartyPacket.expeditionJoined(c.getPlayer().getName()), null);
                                    partyService.sendExpedPacket(exped.getId(), PartyPacket.expeditionUpdate(exped.getIndex(party1.getId()), party1), null);
                                }
                                else
                                {
                                    c.getPlayer().setParty(partyService.getParty(partyId));
                                    partyService.updateParty(partyId, PartyOperation.加入队伍, partyPlayer);
                                    c.getPlayer().receivePartyMemberHP();
                                    c.getPlayer().updatePartyMemberHP();
                                    c.getSession().write(PartyPacket.expeditionStatus(exped, true));
                                    partyService.sendExpedPacket(exped.getId(), PartyPacket.expeditionJoined(c.getPlayer().getName()), null);
                                }
                            }
                            else
                            {
                                c.getSession().write(PartyPacket.expeditionInviteMessage(0, c.getPlayer().getName()));
                            }
                        }
                    }
                }
                else
                {
                    c.getPlayer().dropMessage(1, "您已经有队伍，请退出队伍后在试.");
                }
                break;
            default:
                if (server.ServerProperties.ShowPacket())
                {
                    System.out.println("Unknown PartyListing : 0x" + tools.StringUtil.getLeftPaddedStr(Integer.toHexString(mode).toUpperCase(), '0', 2) + " " + slea);
                }

                break;
        }

    }

    public static void Expedition(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        MapleCharacter player = c.getPlayer();
        if ((player == null) || (player.getMap() == null))
        {
            return;
        }
        int mode = slea.readByte();
        WrodlPartyService partyService = WrodlPartyService.getInstance();
        int partyId;
        MapleExpedition exped;
        int cid;
        MapleParty part;
        int i;
        int partyIndexTo;
        switch (mode)
        {
            case 76:
                int partySearchId = slea.readInt();
                ExpeditionType et = ExpeditionType.getById(partySearchId);
                if ((et != null) && (player.getParty() == null) && (player.getLevel() <= et.maxLevel) && (player.getLevel() >= et.minLevel))
                {
                    MapleParty party = partyService.createParty(new MaplePartyCharacter(player), et.exped);
                    player.setParty(party);
                    c.getSession().write(PartyPacket.partyCreated(party.getId()));
                    c.getSession().write(PartyPacket.expeditionStatus(partyService.getExped(party.getExpeditionId()), true));
                }
                else
                {
                    c.getSession().write(PartyPacket.expeditionInviteMessage(0, "远征模式ID[" + partySearchId + "]"));
                }
                break;
            case 77:
                String name = slea.readMapleAsciiString();
                int theCh = WorldFindService.getInstance().findChannel(name);
                if (theCh > 0)
                {
                    MapleCharacter invited = ChannelServer.getInstance(theCh).getPlayerStorage().getCharacterByName(name);
                    MapleParty party = c.getPlayer().getParty();
                    if ((invited != null) && (invited.getParty() == null) && (party != null) && (party.getExpeditionId() > 0))
                    {
                        MapleExpedition me = partyService.getExped(party.getExpeditionId());
                        if ((me != null) && (me.getAllMembers() < me.getType().maxMembers) && (invited.getLevel() <= me.getType().maxLevel) && (invited.getLevel() >= me.getType().minLevel))
                        {
                            c.getSession().write(PartyPacket.expeditionInviteMessage(7, invited.getName()));
                            invited.getClient().getSession().write(PartyPacket.expeditionInvite(player, me.getType().exped));
                        }
                        else
                        {
                            c.getSession().write(PartyPacket.expeditionInviteMessage(3, invited.getName()));
                        }
                    }
                    else
                    {
                        c.getSession().write(PartyPacket.expeditionInviteMessage(2, name));
                    }
                }
                else
                {
                    c.getSession().write(PartyPacket.expeditionInviteMessage(0, name));
                }
                break;
            case 78:
                String name1 = slea.readMapleAsciiString();
                slea.readInt();
                int action = slea.readInt();
                int theChh = WorldFindService.getInstance().findChannel(name1);
                if (theChh > 0)
                {
                    MapleCharacter cfrom = ChannelServer.getInstance(theChh).getPlayerStorage().getCharacterByName(name1);
                    if ((cfrom != null) && (cfrom.getParty() != null) && (cfrom.getParty().getExpeditionId() > 0))
                    {
                        MapleParty party = cfrom.getParty();
                        MapleExpedition exped2 = partyService.getExped(party.getExpeditionId());
                        if ((exped2 != null) && (action == 8))
                        {
                            if ((player.getLevel() <= exped2.getType().maxLevel) && (player.getLevel() >= exped2.getType().minLevel) && (exped2.getAllMembers() < exped2.getType().maxMembers))
                            {
                                partyId = exped2.getFreeParty();
                                if (partyId < 0)
                                {
                                    c.getSession().write(PartyPacket.partyStatusMessage(17));
                                }
                                else if (partyId == 0)
                                {
                                    party = partyService.createPartyAndAdd(new MaplePartyCharacter(player), exped2.getId());
                                    player.setParty(party);
                                    c.getSession().write(PartyPacket.partyCreated(party.getId()));
                                    c.getSession().write(PartyPacket.expeditionStatus(exped2, true));
                                    partyService.sendExpedPacket(exped2.getId(), PartyPacket.expeditionJoined(player.getName()), null);
                                    partyService.sendExpedPacket(exped2.getId(), PartyPacket.expeditionUpdate(exped2.getIndex(party.getId()), party), null);
                                }
                                else
                                {
                                    player.setParty(partyService.getParty(partyId));
                                    partyService.updateParty(partyId, PartyOperation.加入队伍, new MaplePartyCharacter(player));
                                    player.receivePartyMemberHP();
                                    player.updatePartyMemberHP();
                                    partyService.sendExpedPacket(exped2.getId(), PartyPacket.expeditionJoined(player.getName()), null);
                                    c.getSession().write(PartyPacket.expeditionStatus(exped2, false));
                                }
                            }
                            else
                            {
                                c.getSession().write(PartyPacket.expeditionInviteMessage(3, cfrom.getName()));
                            }
                        }
                        else if (action == 9) cfrom.dropMessage(5, "'" + player.getName() + "'拒绝了远征队邀请。");
                    }
                }
                break;

            case 79:
                MapleParty part3 = player.getParty();
                if ((part3 != null) && (part3.getExpeditionId() > 0))
                {
                    MapleExpedition exped3 = partyService.getExped(part3.getExpeditionId());
                    if (exped3 != null)
                    {
                        if (GameConstants.isDojo(player.getMapId()))
                        {
                            Event_DojoAgent.failed(player);
                        }
                        if (exped3.getLeader() == player.getId())
                        {
                            partyService.disbandExped(exped3.getId());
                            if (player.getEventInstance() != null)
                            {
                                player.getEventInstance().disbandParty();
                            }
                        }
                        else if (part3.getLeader().getId() == player.getId())
                        {
                            partyService.updateParty(part3.getId(), PartyOperation.解散队伍, new MaplePartyCharacter(player));
                            if (player.getEventInstance() != null)
                            {
                                player.getEventInstance().disbandParty();
                            }
                        }
                        else
                        {
                            partyService.updateParty(part3.getId(), PartyOperation.离开队伍, new MaplePartyCharacter(player));
                            if (player.getEventInstance() != null)
                            {
                                player.getEventInstance().leftParty(player);
                            }

                            partyService.sendExpedPacket(exped3.getId(), PartyPacket.expeditionLeft(true, player.getName()), null);
                        }
                        if (player.getPyramidSubway() != null)
                        {
                            player.getPyramidSubway().fail(c.getPlayer());
                        }
                        player.setParty(null);
                    }
                }
                break;

            case 80:
                MapleParty part4 = player.getParty();
                if ((part4 != null) && (part4.getExpeditionId() > 0))
                {
                    exped = partyService.getExped(part4.getExpeditionId());
                    if ((exped != null) && (exped.getLeader() == player.getId()))
                    {
                        cid = slea.readInt();
                        for (Integer id : exped.getParties())
                        {
                            MapleParty par = partyService.getParty(id);
                            if (par != null)
                            {
                                MaplePartyCharacter expelled = par.getMemberById(cid);
                                if (expelled != null)
                                {
                                    if ((expelled.isOnline()) && (GameConstants.isDojo(player.getMapId())))
                                    {
                                        Event_DojoAgent.failed(player);
                                    }
                                    partyService.updateParty(id, PartyOperation.驱逐成员, expelled);
                                    if ((player.getEventInstance() != null) && (expelled.isOnline()))
                                    {
                                        player.getEventInstance().disbandParty();
                                    }

                                    if ((player.getPyramidSubway() != null) && (expelled.isOnline()))
                                    {
                                        player.getPyramidSubway().fail(player);
                                    }
                                    partyService.sendExpedPacket(exped.getId(), PartyPacket.expeditionLeft(false, expelled.getName()), null);
                                    break;
                                }
                            }
                        }
                    }
                }
                break;

            case 81:
                MapleParty part5 = player.getParty();
                if ((part5 != null) && (part5.getExpeditionId() > 0))
                {
                    MapleExpedition exped6 = partyService.getExped(part5.getExpeditionId());
                    if ((exped6 != null) && (exped6.getLeader() == player.getId()))
                    {
                        MaplePartyCharacter newleader = part5.getMemberById(slea.readInt());
                        if (newleader != null)
                        {
                            partyService.updateParty(part5.getId(), PartyOperation.改变队长, newleader);
                            exped6.setLeader(newleader.getId());
                            partyService.sendExpedPacket(exped6.getId(), PartyPacket.expeditionLeaderChanged(0), null);
                        }
                    }
                }
                break;

            case 82:
                part5 = player.getParty();
                if ((part5 != null) && (part5.getExpeditionId() > 0))
                {
                    MapleExpedition exped7 = partyService.getExped(part5.getExpeditionId());
                    if ((exped7 != null) && (exped7.getLeader() == player.getId()))
                    {
                        cid = slea.readInt();
                        for (Integer ids : exped7.getParties())
                        {
                            MapleParty par = partyService.getParty(ids);
                            if (par != null)
                            {
                                MaplePartyCharacter newleader = par.getMemberById(cid);
                                if ((newleader != null) && (par.getId() != part5.getId())) partyService.updateParty(par.getId(), PartyOperation.改变队长, newleader);
                            }
                        }
                    }
                }
                break;

            case 83:
                MapleParty part9 = player.getParty();
                if ((part9 != null) && (part9.getExpeditionId() > 0))
                {
                    exped = partyService.getExped(part9.getExpeditionId());
                    if ((exped != null) && (exped.getLeader() == player.getId()))
                    {
                        partyIndexTo = slea.readInt();
                        if ((partyIndexTo < exped.getType().maxParty) && (partyIndexTo <= exped.getParties().size()))
                        {
                            cid = slea.readInt();
                            for (Integer idd : exped.getParties())
                            {
                                MapleParty par = partyService.getParty(idd);
                                if (par != null)
                                {
                                    MaplePartyCharacter expelled = par.getMemberById(cid);
                                    if ((expelled != null) && (expelled.isOnline()))
                                    {
                                        MapleCharacter chr = handling.world.World.getStorage(expelled.getChannel()).getCharacterById(expelled.getId());
                                        if (chr == null)
                                        {
                                            break;
                                        }
                                        if (partyIndexTo < exped.getParties().size())
                                        {
                                            MapleParty party = partyService.getParty(exped.getParties().get(partyIndexTo));
                                            if ((party == null) || (party.getMembers().size() >= 6))
                                            {
                                                player.dropMessage(5, "Invalid party.");
                                                break;
                                            }
                                        }
                                        if (GameConstants.isDojo(player.getMapId()))
                                        {
                                            Event_DojoAgent.failed(player);
                                        }
                                        partyService.updateParty(idd, PartyOperation.驱逐成员, expelled);
                                        if (partyIndexTo < exped.getParties().size())
                                        {
                                            MapleParty party = partyService.getParty(exped.getParties().get(partyIndexTo));
                                            if ((party != null) && (party.getMembers().size() < 6))
                                            {
                                                partyService.updateParty(party.getId(), PartyOperation.加入队伍, expelled);
                                                chr.receivePartyMemberHP();
                                                chr.updatePartyMemberHP();
                                                chr.getClient().getSession().write(PartyPacket.expeditionStatus(exped, true));
                                            }
                                        }
                                        else
                                        {
                                            MapleParty party = partyService.createPartyAndAdd(expelled, exped.getId());
                                            chr.setParty(party);
                                            chr.getClient().getSession().write(PartyPacket.partyCreated(party.getId()));
                                            chr.getClient().getSession().write(PartyPacket.expeditionStatus(exped, true));
                                            partyService.sendExpedPacket(exped.getId(), PartyPacket.expeditionUpdate(exped.getIndex(party.getId()), party), null);
                                        }
                                        if ((player.getEventInstance() != null) && (expelled.isOnline()))
                                        {
                                            player.getEventInstance().disbandParty();
                                        }

                                        if (player.getPyramidSubway() == null) break;
                                        player.getPyramidSubway().fail(c.getPlayer());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                break;

            default:
                if (server.ServerProperties.ShowPacket())
                {
                    System.out.println("未知的远征队操作 : 0x" + tools.StringUtil.getLeftPaddedStr(Integer.toHexString(mode).toUpperCase(), '0', 2) + " " + slea);
                }
                break;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\PartyHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */