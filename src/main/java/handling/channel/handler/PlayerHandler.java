package handling.channel.handler;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleClient;
import client.PlayerStats;
import client.Skill;
import client.SkillFactory;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import constants.GameConstants;
import handling.channel.ChannelServer;
import server.MapleItemInformationProvider;
import server.MaplePortal;
import server.MapleStatEffect;
import server.PokemonBattle;
import server.Randomizer;
import server.events.MapleEvent;
import server.events.MapleEventType;
import server.life.MapleMonster;
import server.maps.FieldLimitType;
import server.maps.MapleMap;
import server.quest.MapleQuest;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.MTSCSPacket;

public class PlayerHandler
{
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PlayerHandler.class);

    public static void ChangeSkillMacro(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        int num = slea.readByte();


        for (int i = 0; i < num; i++)
        {
            String name = slea.readMapleAsciiString();
            int shout = slea.readByte();
            int skill1 = slea.readInt();
            int skill2 = slea.readInt();
            int skill3 = slea.readInt();
            client.SkillMacro macro = new client.SkillMacro(skill1, skill2, skill3, name, shout, i);
            chr.updateMacros(i, macro);
        }
    }

    public static void ChangeKeymap(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        if ((slea.available() > 8L) && (chr != null))
        {
            slea.skip(4);
            int numChanges = slea.readInt();
            for (int i = 0; i < numChanges; i++)
            {
                int key = slea.readInt();
                byte type = slea.readByte();
                int action = slea.readInt();
                if ((type == 1) && (action >= 1000))
                {
                    Skill skil = SkillFactory.getSkill(action);
                    if ((skil != null) && (((!skil.isFourthJob()) && (!skil.isBeginnerSkill()) && (skil.isInvisible()) && (chr.getSkillLevel(skil) <= 0)) || (GameConstants.isLinkedAttackSkill(action)) || (action % 10000 < 1000)))
                    {
                    }

                }
                else
                {
                    chr.changeKeybinding(key, type, action);
                }
            }
        }
        else if (chr != null)
        {
            int type = slea.readInt();
            int data = slea.readInt();
            switch (type)
            {
                case 1:
                    if (data <= 0)
                    {
                        chr.getQuestRemove(MapleQuest.getInstance(122221));
                    }
                    else
                    {
                        chr.getQuestNAdd(MapleQuest.getInstance(122221)).setCustomData(String.valueOf(data));
                    }
                    break;
                case 2:
                    if (data <= 0)
                    {
                        chr.getQuestRemove(MapleQuest.getInstance(122223));
                    }
                    else
                    {
                        chr.getQuestNAdd(MapleQuest.getInstance(122223)).setCustomData(String.valueOf(data));
                    }
                    break;
                case 3:
                    if (data <= 0)
                    {
                        chr.getQuestRemove(MapleQuest.getInstance(122224));
                    }
                    else
                    {
                        chr.getQuestNAdd(MapleQuest.getInstance(122224)).setCustomData(String.valueOf(data));
                    }
                    break;
            }
        }
    }

    public static void UseChair(int itemId, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        Item toUse = chr.getInventory(MapleInventoryType.SETUP).findById(itemId);
        if (toUse == null)
        {
            chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.USING_UNAVAILABLE_ITEM, Integer.toString(itemId));
            return;
        }
        if ((GameConstants.isFishingMap(chr.getMapId())) && (itemId == 3011000) && (chr.getStat().canFish))
        {
            chr.startFishingTask();
        }

        chr.setChair(itemId);
        chr.getMap().broadcastMessage(chr, MaplePacketCreator.showChair(chr.getId(), itemId), false);
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void CancelChair(short id, MapleClient c, MapleCharacter chr)
    {
        if (id == -1)
        {
            chr.cancelFishingTask();
            chr.setChair(0);
            c.getSession().write(MaplePacketCreator.cancelChair(-1));
            if (chr.getMap() != null)
            {
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.showChair(chr.getId(), 0), false);
            }
        }
        else
        {
            chr.setChair(id);
            c.getSession().write(MaplePacketCreator.cancelChair(id));
        }
    }

    public static void TrockAddMap(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        byte type = slea.readByte();
        byte vip = slea.readByte();
        if (type == 0)
        {
            int mapId = slea.readInt();
            if (vip == 1)
            {
                chr.deleteFromRegRocks(mapId);
            }
            else if (vip == 2)
            {
                chr.deleteFromRocks(mapId);
            }
            else if (vip == 3)
            {
                chr.deleteFromHyperRocks(mapId);
            }
            c.getSession().write(MTSCSPacket.getTrockRefresh(chr, vip, true));
        }
        else if (type == 1)
        {
            if (!FieldLimitType.VipRock.check(chr.getMap().getFieldLimit()))
            {
                if (vip == 1)
                {
                    chr.addRegRockMap();
                }
                else if (vip == 2)
                {
                    chr.addRockMap();
                }
                else if (vip == 3)
                {
                    chr.addHyperRockMap();
                }
                c.getSession().write(MTSCSPacket.getTrockRefresh(chr, vip, false));
            }
            else
            {
                chr.dropMessage(1, "你可能没有保存此地图.");
            }
        }
    }

    public static void CharInfoRequest(int objectid, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        MapleCharacter player = chr.getMap().getCharacterById(objectid);
        c.getSession().write(MaplePacketCreator.enableActions());
        if ((player != null) && ((!player.isGM()) || (chr.isGM())))
        {
            c.getSession().write(MaplePacketCreator.charInfo(player, chr.getId() == objectid));
        }
    }

    public static void AranCombo(MapleClient c, MapleCharacter chr, int toAdd)
    {
        if ((chr != null) && (chr.getJob() >= 2000) && (chr.getJob() <= 2112))
        {
            int combo = chr.getAranCombo();
            long currentTime = System.currentTimeMillis();
            if (((combo <= 0) || (currentTime - chr.getLastComboTime() <= 7000L)) || (


                    (toAdd < 0) && (combo <= 0)))
            {
                return;
            }
            chr.gainAranCombo(toAdd, true);
            chr.setLastComboTime(currentTime);
            switch (combo)
            {
                case 10:
                case 20:
                case 30:
                case 40:
                case 50:
                case 60:
                case 70:
                case 80:
                case 90:
                case 100:
                    if (chr.getSkillLevel(21000000) >= combo / 10)
                    {
                        SkillFactory.getSkill(21000000).getEffect(combo / 10).applyComboBuff(chr, combo);
                    }

                    break;
            }

        }
    }

    public static void UseItemEffect(int itemId, MapleClient c, MapleCharacter chr)
    {
        if (itemId == 0)
        {
            chr.setItemEffect(0);
        }
        else
        {
            Item toUse = chr.getInventory(MapleInventoryType.CASH).findById(itemId);
            if ((toUse == null) || (toUse.getItemId() != itemId) || (toUse.getQuantity() < 1))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if (itemId != 5510000)
            {
                chr.setItemEffect(itemId);
            }
        }
        chr.getMap().broadcastMessage(chr, MaplePacketCreator.itemEffect(chr.getId(), itemId), false);
    }

    public static void UseTitleEffect(int itemId, MapleClient c, MapleCharacter chr)
    {
        if (itemId == 0)
        {
            chr.setTitleEffect(0);
        }
        else
        {
            Item toUse = chr.getInventory(MapleInventoryType.SETUP).findById(itemId);
            if ((toUse == null) || (toUse.getItemId() != itemId) || (toUse.getQuantity() < 1))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if (itemId / 10000 == 370)
            {
                chr.setTitleEffect(itemId);
            }
        }
        chr.getMap().broadcastMessage(chr, MaplePacketCreator.showTitleEffect(chr.getId(), itemId), false);
    }

    public static void CancelItemEffect(int id, MapleCharacter chr)
    {
        chr.cancelEffect(MapleItemInformationProvider.getInstance().getItemEffect(-id), false, -1L);
    }

    public static void CancelBuffHandler(int sourceid, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        Skill skill = SkillFactory.getSkill(sourceid);
        if (chr.isShowPacket())
        {
            chr.dropSpouseMessage(10, "收到取消技能BUFF 技能ID " + sourceid + " 技能名字 " + SkillFactory.getSkillName(sourceid));
        }

        if (skill == null)
        {
            return;
        }
        if (skill.isChargeSkill())
        {
            chr.setKeyDownSkill_Time(0L);
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.skillCancel(chr, sourceid), false);
        }
        else
        {
            chr.cancelEffect(skill.getEffect(1), false, -1L);
        }
    }

    public static void CancelMech(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        int sourceid = slea.readInt();
        if ((sourceid % 10000 < 1000) && (SkillFactory.getSkill(sourceid) == null))
        {
            sourceid += 1000;
        }
        Skill skill = SkillFactory.getSkill(sourceid);
        if (skill == null)
        {
            return;
        }
        if (skill.isChargeSkill())
        {
            chr.setKeyDownSkill_Time(0L);
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.skillCancel(chr, sourceid), false);
        }
        else
        {
            chr.cancelEffect(skill.getEffect(slea.readByte()), false, -1L);
        }
    }

    public static void QuickSlot(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        chr.getQuickSlot().resetQuickSlot();
        for (int i = 0; i < 28; i++)
        {
            chr.getQuickSlot().addQuickSlot(i, slea.readInt());
        }
    }

    public static void SkillEffect(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        int skillId = slea.readInt();
        byte level = slea.readByte();
        byte display = slea.readByte();
        byte direction = slea.readByte();
        byte speed = slea.readByte();
        Point position = null;
        if (slea.available() >= 4L)
        {
            position = slea.readPos();
        }

        Skill skill = SkillFactory.getSkill(GameConstants.getLinkedAttackSkill(skillId));
        if ((chr == null) || (skill == null) || (chr.getMap() == null))
        {
            return;
        }
        int skilllevel_serv = chr.getTotalSkillLevel(skill);
        if ((skilllevel_serv > 0) && (skilllevel_serv == level) && (skill.isChargeSkill()))
        {
            chr.setKeyDownSkill_Time(System.currentTimeMillis());
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.skillEffect(chr.getId(), skillId, level, display, direction, speed, position), false);
        }
    }

    public static void specialAttack(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int pos_x = slea.readInt();
        int pos_y = slea.readInt();
        int pos_unk = slea.readInt();
        int display = slea.readInt();
        int skillId = slea.readInt();
        boolean isLeft = slea.readByte() > 0;
        int speed = slea.readInt();
        int tickCount = slea.readInt();
        Skill skill = SkillFactory.getSkill(GameConstants.getLinkedAttackSkill(skillId));
        int skilllevel = chr.getTotalSkillLevel(skill);
        if (chr.isShowPacket())
        {
            System.err.println("specialAttack - 技能ID: " + skillId + " 技能等级: " + skilllevel);
        }
        if ((skill == null) || (skilllevel <= 0))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.getMap().broadcastMessage(chr, MaplePacketCreator.showBuffeffect(chr.getId(), skillId, 1, chr.getLevel(), skilllevel), false);
        chr.getMap().broadcastMessage(chr, MaplePacketCreator.showSpecialAttack(chr.getId(), tickCount, pos_x, pos_y, display, skillId, skilllevel, isLeft, speed), chr.getTruePosition());
    }

    public static void SpecialMove(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.hasBlockedInventory()) || (chr.getMap() == null) || (slea.available() < 9L))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        slea.skip(4);
        int skillid = slea.readInt();
        if (skillid == 23111008)
        {
            skillid += Randomizer.nextInt(3);
        }
        else if (skillid == 5210015)
        {
            skillid += Randomizer.nextInt(4);
        }
        if ((GameConstants.is神之子(chr.getJob())) && (skillid >= 100000000))
        {
            slea.readByte();
        }
        int skillLevel = slea.readByte();
        if (chr.isShowPacket())
        {
            System.err.println("[SpecialMove] - 技能ID: " + skillid + " 技能等级: " + skillLevel);
        }
        Skill skill = SkillFactory.getSkill(skillid);
        if ((skill == null) || ((GameConstants.is天使技能(skillid)) && (chr.getStat().equippedSummon % 10000 != skillid % 10000)) || ((chr.inPVP()) && (skill.isPVPDisabled())))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int checkSkilllevel = chr.getTotalSkillLevel(GameConstants.getLinkedAttackSkill(skillid));
        if (chr.isShowPacket())
        {
            chr.dropSpouseMessage(25, "[SpecialMove] - 技能ID: " + skillid + " 技能等级: " + skillLevel);
            if (GameConstants.getLinkedAttackSkill(skillid) != skillid)
            {
                chr.dropSpouseMessage(25, "[SpecialMove] - 连接技能ID: " + GameConstants.getLinkedAttackSkill(skillid) + " 连接技能等级: " + checkSkilllevel);
            }
        }
        if ((checkSkilllevel <= 0) || (checkSkilllevel != skillLevel))
        {
            if ((!GameConstants.isMulungSkill(skillid)) && (!GameConstants.isPyramidSkill(skillid)) && (checkSkilllevel <= 0))
            {
                if (chr.isAdmin())
                {
                    chr.dropSpouseMessage(25, "[SpecialMove] 使用技能出现异常 技能ID: " + skillid + " 角色技能等级: " + checkSkilllevel + " 封包获取等级: " + skillLevel + " 是否相同: " + (checkSkilllevel == skillLevel));
                }
                FileoutputUtil.log("log\\SpecialMove.log",
                        "玩家[" + chr.getName() + " 职业: " + chr.getJob() + "] 使用技能: " + skillid + " 技能等级: " + checkSkilllevel + " - " + (!GameConstants.isMulungSkill(skillid)) + " - " + (!GameConstants.isPyramidSkill(skillid)) + " 封包:" + slea.toString(true));
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if (GameConstants.isMulungSkill(skillid))
            {
                if (chr.getMapId() / 10000 != 92502)
                {
                    return;
                }
                if (chr.getMulungEnergy() < 10000)
                {
                    return;
                }
                chr.mulung_EnergyModify(false);
            }
            else if ((GameConstants.isPyramidSkill(skillid)) && (chr.getMapId() / 10000 != 92602) && (chr.getMapId() / 10000 != 92601))
            {
                return;
            }
        }

        if (GameConstants.isEventMap(chr.getMapId()))
        {
            for (MapleEventType t : MapleEventType.values())
            {
                MapleEvent e = ChannelServer.getInstance(chr.getClient().getChannel()).getEvent(t);
                if ((e.isRunning()) && (!chr.isGM()))
                {
                    for (int i : e.getType().mapids)
                    {
                        if (chr.getMapId() == i)
                        {
                            chr.dropMessage(5, "无法在这里使用.");
                            return;
                        }
                    }
                }
            }
        }
        skillLevel = chr.getTotalSkillLevel(GameConstants.getLinkedAttackSkill(skillid));
        MapleStatEffect effect = chr.inPVP() ? skill.getPVPEffect(skillLevel) : skill.getEffect(skillLevel);
        if ((effect.getCooldown(chr) > 0) && (!chr.isGM()))
        {
            if (chr.skillisCooling(skillid))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if (skillid != 35111002)
            {
                c.getSession().write(MaplePacketCreator.skillCooldown(skillid, effect.getCooldown(chr)));
                chr.addCooldown(skillid, System.currentTimeMillis(), effect.getCooldown(chr) * 1000);
            }
        }

        switch (skillid)
        {
            case 9001020:
            case 9101020:
            case 31111003:
                byte number_of_mobs = slea.readByte();
                slea.skip(3);
                for (int i = 0; i < number_of_mobs; i++)
                {
                    int mobId = slea.readInt();
                    MapleMonster mob = chr.getMap().getMonsterByOid(mobId);
                    if (mob != null)
                    {
                        mob.switchController(chr, mob.isControllerHasAggro());
                        mob.applyStatus(chr, new client.status.MonsterStatusEffect(client.status.MonsterStatus.眩晕, 1, skillid, null, false), false, effect.getDuration(), true, effect);
                    }
                }
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.showBuffeffect(chr.getId(), skillid, 1, chr.getLevel(), skillLevel, slea.readByte()), chr.getTruePosition());
                c.getSession().write(MaplePacketCreator.enableActions());
                break;
            case 30001061:
                int mobID = slea.readInt();
                MapleMonster mob = chr.getMap().getMonsterByOid(mobID);
                if (mob != null)
                {
                    boolean success = (mob.getHp() <= mob.getMobMaxHp() / 2L) && (mob.getId() >= 9304000) && (mob.getId() < 9305000);
                    chr.getMap().broadcastMessage(chr, MaplePacketCreator.showBuffeffect(chr.getId(), skillid, 1, chr.getLevel(), skillLevel, (byte) (success ? 1 : 0)), chr.getTruePosition());
                    if (success)
                    {
                        chr.getQuestNAdd(MapleQuest.getInstance(111112)).setCustomData(String.valueOf((mob.getId() - 9303999) * 10));
                        chr.getMap().killMonster(mob, chr, true, false, (byte) 1);
                        chr.cancelEffectFromBuffStat(MapleBuffStat.骑兽技能);
                        c.getSession().write(MaplePacketCreator.updateJaguar(chr));
                    }
                    else
                    {
                        chr.dropMessage(5, "怪物体力过高，捕抓失败。");
                    }
                }
                c.getSession().write(MaplePacketCreator.enableActions());
                break;
            case 30001062:
                chr.dropMessage(5, "没有能被召唤的怪物，请先捕抓怪物。");
                c.getSession().write(MaplePacketCreator.enableActions());
                break;
            case 20040216:
            case 20040217:
                chr.changeLuminousMode(skillid);
                break;
            case 20040219:
            case 20040220:
            case 20041239:
                chr.dropMessage(5, "当前暂不支持该功能.");
                c.getSession().write(MaplePacketCreator.enableActions());
                break;
            case 36001005:
                byte mobCount = slea.readByte();
                for (int i = 0; i < mobCount; i++)
                {
                    int mobId = slea.readInt();
                    MapleMonster mobx = chr.getMap().getMonsterByOid(mobId);
                    if (mobx != null)
                    {
                        chr.handleCardStack(mobId, skillid);
                    }
                    if (chr.isShowPacket())
                    {
                        chr.dropMessage(5, "精准火箭 - " + i + " oldId: " + mobId + " 技能ID: " + skillid);
                    }
                }
                break;
            case 4341003:
                chr.setKeyDownSkill_Time(0L);
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.skillCancel(chr, skillid), false);
            default:
                Point pos = null;
                if ((slea.available() == 5L) || (slea.available() == 7L))
                {
                    pos = slea.readPos();
                }
                if (effect.is时空门())
                {
                    if (!FieldLimitType.MysticDoor.check(chr.getMap().getFieldLimit()))
                    {
                        effect.applyTo(c.getPlayer(), pos);
                    }
                    else
                    {
                        c.getSession().write(MaplePacketCreator.enableActions());
                    }
                }
                else
                {
                    int mountid = MapleStatEffect.parseMountInfo(c.getPlayer(), skill.getId());
                    if ((mountid != 0) && (mountid != GameConstants.getMountItem(skill.getId(), chr)) && (!chr.isIntern()) && (chr.getBuffedValue(MapleBuffStat.骑兽技能) == null) && (chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -122) == null) && (!GameConstants.isMountItemAvailable(mountid, chr.getJob())))
                    {
                        c.getSession().write(MaplePacketCreator.enableActions());
                        return;
                    }


                    if (effect.getSourceId() == 5321004)
                    {
                        effect.applyTo(chr, pos);
                        effect = SkillFactory.getSkill(5320011).getEffect(skillLevel);
                        if (pos != null)
                        {
                            pos.x -= 90;
                        }
                        if (effect != null)
                        {
                            effect.applyTo(chr, pos);
                        }
                    }
                    else if (effect.is集合船员())
                    {
                        effect.applyTo(chr, pos);
                        Object skillIds = new ArrayList<>();
                        for (int i = 5210015; i <= 5210018; i++)
                        {
                            if (i != effect.getSourceId())
                            {
                                ((List) skillIds).add(i);
                            }
                        }
                        skillid = (Integer) ((List) skillIds).get(Randomizer.nextInt(((List) skillIds).size()));
                        effect = SkillFactory.getSkill(skillid).getEffect(skillLevel);
                        if (pos != null)
                        {
                            pos.x -= 90;
                        }
                        if (effect != null)
                        {
                            effect.applyTo(chr, pos);
                        }
                    }
                    else
                    {
                        effect.applyTo(chr, pos);
                    }
                }

                break;
        }

    }

    public static void closeRangeAttack(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr, boolean energy)
    {
        if ((chr == null) || ((energy) && (chr.getBuffedValue(MapleBuffStat.能量获得) == null) && (chr.getBuffedValue(MapleBuffStat.战神抗压) == null) && (chr.getBuffedValue(MapleBuffStat.黑暗灵气) == null) && (chr.getBuffedValue(MapleBuffStat.幻灵飓风) == null) && (chr.getBuffedValue(MapleBuffStat.召唤兽) == null) && (chr.getBuffedValue(MapleBuffStat.地雷) == null) && (chr.getBuffedValue(MapleBuffStat.快速移动精通) == null)))
        {
            return;
        }
        if ((chr.hasBlockedInventory()) || (chr.getMap() == null))
        {
            return;
        }
        if ((chr.getGMLevel() >= 3) && (chr.getGMLevel() <= 5) && (chr.getMap().isBossMap()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }


        if ((!chr.isAdmin()) && (chr.getMap().isMarketMap()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        AttackInfo attack = DamageParse.parseCloseRangeAttack(slea, chr);
        if (attack == null)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (attack.skillId == 11121052)
        {
            chr.dropSpouseMessage(25, "[系统提示] 当前技能暂时无法使用");
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        boolean mirror = chr.getBuffedValue(MapleBuffStat.影分身) != null;
        boolean hasMoonBuff = chr.getBuffedIntValue(MapleBuffStat.月光转换) == 1;
        double maxdamage = chr.getStat().getCurrentMaxBaseDamage();
        Item shield = c.getPlayer().getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
        int attackCount = (shield != null) && (shield.getItemId() / 10000 == 134) ? 2 : 1;
        int skillLevel = 0;
        MapleStatEffect effect = null;
        Skill skill = null;

        if (attack.skillId != 0)
        {
            skill = SkillFactory.getSkill(GameConstants.getLinkedAttackSkill(attack.skillId));
            if ((skill == null) || ((GameConstants.is天使技能(attack.skillId)) && (chr.getStat().equippedSummon % 10000 != attack.skillId % 10000)))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            skillLevel = chr.getTotalSkillLevel(skill);
            effect = attack.getAttackEffect(chr, skillLevel, skill);
            if (effect == null)
            {
                FileoutputUtil.log("log\\SpecialMove.log", "近距离攻击效果为空 玩家[" + chr.getName() + " 职业: " + chr.getJob() + "] 使用技能: " + skill.getId() + " - " + skill.getName() + " 技能等级: " + skillLevel);
                return;
            }
            if ((chr.getJob() == 3101) || (chr.getJob() == 3120) || (chr.getJob() == 3121) || (chr.getJob() == 3122))
            {
                chr.handle超越状态(attack.skillId);
            }
            if (GameConstants.isEventMap(chr.getMapId()))
            {
                for (MapleEventType t : MapleEventType.values())
                {
                    MapleEvent e = ChannelServer.getInstance(chr.getClient().getChannel()).getEvent(t);
                    if ((e.isRunning()) && (!chr.isGM()))
                    {
                        for (int i : e.getType().mapids)
                        {
                            if (chr.getMapId() == i)
                            {
                                chr.dropMessage(5, "无法在这个地方使用.");
                                return;
                            }
                        }
                    }
                }
            }
            if (attack.skillId == 1321013)
            {
                maxdamage += chr.getStat().getCurrentMaxHp();
            }
            if ((attack.skillId == 4100012) || (attack.skillId == 4120019))
            {
                maxdamage *= (effect.getDamage() + chr.getStat().getDamageIncrease(attack.skillId) + effect.getX() * chr.getLevel()) / 100.0D;
            }
            else
            {
                maxdamage *= (effect.getDamage() + chr.getStat().getDamageIncrease(attack.skillId)) / 100.0D;
            }
            attackCount = effect.getAttackCount(chr);
            boolean notCooldown = (attack.skillId == 31121005) || (attack.skillId == 61101002) || (attack.skillId == 61120007);
            int cooldownTime = effect.getCooldown(chr);


            if (attack.skillId == 15121001)
            {
                attackCount += Math.min(chr.getBuffedIntValue(MapleBuffStat.百分比无视防御) / 5, chr.getStat().raidenCount);
            }
            else if (attack.skillId == 15111022)
            {
                effect.applyTo(chr);
            }
        }
        attack = DamageParse.Modify_AttackCrit(attack, chr, 1, effect);
        attackCount *= (mirror ? 2 : 1);
        attackCount *= (hasMoonBuff ? 2 : 1);
        if (!energy)
        {
            if (((chr.getMapId() == 109060000) || (chr.getMapId() == 109060002) || (chr.getMapId() == 109060004)) && (attack.skillId == 0))
            {
                server.events.MapleSnowball.MapleSnowballs.hitSnowball(chr);
            }

            int numFinisherOrbs = 0;
            Integer comboBuff = chr.getBuffedValue(MapleBuffStat.斗气集中);
            if (isFinisher(attack.skillId) > 0)
            {
                if (comboBuff != null)
                {
                    numFinisherOrbs = comboBuff - 1;
                }
                if (numFinisherOrbs <= 0)
                {
                    return;
                }
                chr.handleOrbconsume(isFinisher(attack.skillId));
                maxdamage *= numFinisherOrbs;
            }
        }
        chr.checkFollow();
        if (!chr.isHidden())
        {
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.closeRangeAttack(chr, skillLevel, 0, attack, energy, hasMoonBuff), chr.getTruePosition());
        }
        else
        {
            chr.getMap().broadcastGMMessage(chr, MaplePacketCreator.closeRangeAttack(chr, skillLevel, 0, attack, energy, hasMoonBuff), false);
        }
        DamageParse.applyAttack(attack, skill, c.getPlayer(), attackCount, maxdamage, effect, mirror ? AttackType.NON_RANGED_WITH_MIRROR : AttackType.NON_RANGED, 0);
    }

    public static int isFinisher(int skillid)
    {
        switch (skillid)
        {
            case 1101012:
                return 1;
            case 1111003:
                return 2;
            case 1121015:
                return 4;
        }
        return 0;
    }

    public static void rangedAttack(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.hasBlockedInventory()) || (chr.getMap() == null))
        {
            return;
        }
        if ((chr.getGMLevel() >= 3) && (chr.getGMLevel() <= 5) && (chr.getMap().isBossMap()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }


        if ((!chr.isAdmin()) && (chr.getMap().isMarketMap()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        AttackInfo attack = DamageParse.parseRangedAttack(slea, chr);
        if (attack == null)
        {
            if (chr.isShowPacket())
            {
                chr.dropSpouseMessage(25, "[RangedAttack] - 远距离攻击封包解析返回为空.");
            }
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int bulletCount = 1;
        int skillLevel = 0;
        MapleStatEffect effect = null;
        Skill skill = null;
        boolean noBullet =
                (attack.starSlot == 0) || ((chr.getJob() >= 3500) && (chr.getJob() <= 3512)) || (GameConstants.is火炮手(chr.getJob())) || (GameConstants.is双弩精灵(chr.getJob())) || (GameConstants.is龙的传人(chr.getJob())) || (GameConstants.is米哈尔(chr.getJob())) || (GameConstants.is狂龙战士(chr.getJob())) || (GameConstants.is爆莉萌天使(chr.getJob())) || (GameConstants.is尖兵(chr.getJob()));
        if (attack.skillId != 0)
        {
            skill = SkillFactory.getSkill(GameConstants.getLinkedAttackSkill(attack.skillId));
            if ((skill == null) || ((GameConstants.is天使技能(attack.skillId)) && (chr.getStat().equippedSummon % 10000 != attack.skillId % 10000)))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            skillLevel = chr.getTotalSkillLevel(skill);
            effect = attack.getAttackEffect(chr, skillLevel, skill);
            if (effect == null)
            {
                FileoutputUtil.log("log\\SpecialMove.log", "远距离攻击效果为空 玩家[" + chr.getName() + " 职业: " + chr.getJob() + "] 使用技能: " + skill.getId() + " - " + skill.getName() + " 技能等级: " + skillLevel);
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            if (GameConstants.isEventMap(chr.getMapId()))
            {
                for (MapleEventType eventType : MapleEventType.values())
                {
                    MapleEvent event = ChannelServer.getInstance(chr.getClient().getChannel()).getEvent(eventType);
                    if ((event.isRunning()) && (!chr.isGM()))
                    {
                        for (int i : event.getType().mapids)
                        {
                            if (chr.getMapId() == i)
                            {
                                chr.dropMessage(5, "无法在这个地方使用.");
                                return;
                            }
                        }
                    }
                }
            }
            bulletCount = Math.max(effect.getBulletCount(chr), effect.getAttackCount(chr));
            int cooldownTime = effect.getCooldown(chr);
            if ((cooldownTime > 0) && (((attack.skillId != 35111004) && (attack.skillId != 35121013)) || (chr.getBuffSource(MapleBuffStat.金属机甲) != attack.skillId)))
            {
                if (chr.skillisCooling(attack.skillId))
                {
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                c.getSession().write(MaplePacketCreator.skillCooldown(attack.skillId, cooldownTime));
                chr.addCooldown(attack.skillId, System.currentTimeMillis(), cooldownTime * 1000);
            }
        }
        attack = DamageParse.Modify_AttackCrit(attack, chr, 2, effect);
        boolean mirror = chr.getBuffedValue(MapleBuffStat.影分身) != null;
        bulletCount *= (mirror ? 2 : 1);
        int projectile = 0;
        int visProjectile = 0;
        if ((!noBullet) && (chr.getBuffedValue(MapleBuffStat.无形箭弩) == null) && (!GameConstants.is幻影(chr.getJob())))
        {
            Item item = chr.getInventory(MapleInventoryType.USE).getItem(attack.starSlot);
            if (item == null)
            {
                return;
            }
            projectile = item.getItemId();
            if (attack.cashSlot > 0)
            {
                if (chr.getInventory(MapleInventoryType.CASH).getItem(attack.cashSlot) == null)
                {
                    return;
                }
                visProjectile = chr.getInventory(MapleInventoryType.CASH).getItem(attack.cashSlot).getItemId();
            }
            else
            {
                visProjectile = projectile;
            }

            if (chr.getBuffedValue(MapleBuffStat.暗器伤人) == null)
            {
                int bulletConsume = bulletCount;
                if ((effect != null) && (effect.getBulletConsume() != 0))
                {
                    bulletConsume = effect.getBulletConsume() * (mirror ? 2 : 1);
                }
                if ((chr.getJob() == 412) && (bulletConsume > 0) && (item.getQuantity() < MapleItemInformationProvider.getInstance().getSlotMax(projectile)))
                {
                    Skill expert = SkillFactory.getSkill(4110012);
                    if (chr.getTotalSkillLevel(expert) > 0)
                    {
                        MapleStatEffect eff = expert.getEffect(chr.getTotalSkillLevel(expert));
                        if (eff.makeChanceResult())
                        {
                            item.setQuantity((short) (item.getQuantity() + 1));
                            c.getSession().write(tools.packet.InventoryPacket.modifyInventory(false, java.util.Collections.singletonList(new client.inventory.ModifyInventory(1, item))));
                            bulletConsume = 0;
                            c.getSession().write(tools.packet.InventoryPacket.getInventoryStatus());
                        }
                    }
                }
                if (bulletConsume > 0)
                {
                    boolean useItem = true;
                    if (chr.getBuffedValue(MapleBuffStat.子弹数量) != null)
                    {
                        int count = chr.getBuffedIntValue(MapleBuffStat.子弹数量) - bulletConsume;
                        if (count >= 0)
                        {
                            chr.setBuffedValue(MapleBuffStat.子弹数量, count);
                            useItem = false;
                        }
                        else
                        {
                            chr.cancelEffectFromBuffStat(MapleBuffStat.子弹数量);
                            bulletConsume += count;
                        }
                    }

                    if ((useItem) && (!server.MapleInventoryManipulator.removeById(c, MapleInventoryType.USE, projectile, bulletConsume, false, true)))
                    {
                        chr.dropMessage(5, "您的箭/子弹/飞镖不足。");
                        return;
                    }
                }
            }
        }
        else if ((chr.getJob() >= 3500) && (chr.getJob() <= 3512))
        {
            visProjectile = 2333000;
        }
        else if (GameConstants.is火炮手(chr.getJob()))
        {
            visProjectile = 2333001;
        }

        int projectileWatk = 0;
        if (projectile != 0)
        {
            projectileWatk = MapleItemInformationProvider.getInstance().getWatkForProjectile(projectile);
        }

        PlayerStats statst = chr.getStat();

        double basedamage = statst.getCurrentMaxBaseDamage() + statst.calculateMaxProjDamage(projectileWatk, chr);

        switch (attack.skillId)
        {
            case 3101005:
                if (effect != null)
                {
                    basedamage *= effect.getX() / 100.0D;
                }
                break;
        }
        if (effect != null)
        {
            basedamage *= (effect.getDamage() + statst.getDamageIncrease(attack.skillId)) / 100.0D;
            int money = effect.getMoneyCon();
            if (money != 0)
            {
                if (money > chr.getMeso())
                {
                    money = chr.getMeso();
                }
                chr.gainMeso(-money, false);
            }
        }
        chr.checkFollow();
        if (!chr.isHidden())
        {
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.rangedAttack(chr, skillLevel, visProjectile, attack), chr.getTruePosition());
        }
        else
        {
            chr.getMap().broadcastGMMessage(chr, MaplePacketCreator.rangedAttack(chr, skillLevel, visProjectile, attack), false);
        }
        DamageParse.applyAttack(attack, skill, chr, bulletCount, basedamage, effect, mirror ? AttackType.RANGED_WITH_SHADOWPARTNER : AttackType.RANGED, visProjectile);
    }

    public static void MagicDamage(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.hasBlockedInventory()) || (chr.getMap() == null))
        {
            return;
        }
        if ((chr.getGMLevel() >= 3) && (chr.getGMLevel() <= 5) && (chr.getMap().isBossMap()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }


        if ((!chr.isAdmin()) && (chr.getMap().isMarketMap()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        AttackInfo attack = DamageParse.parseMagicDamage(slea, chr);
        if (attack == null)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        Skill skill = SkillFactory.getSkill(GameConstants.getLinkedAttackSkill(attack.skillId));
        if ((skill == null) || ((GameConstants.is天使技能(attack.skillId)) && (chr.getStat().equippedSummon % 10000 != attack.skillId % 10000)))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int skillLevel = chr.getTotalSkillLevel(skill);
        MapleStatEffect effect = attack.getAttackEffect(chr, skillLevel, skill);
        if (effect == null)
        {
            FileoutputUtil.log("log\\SpecialMove.log", "魔法攻击效果为空 玩家[" + chr.getName() + " 职业: " + chr.getJob() + "] 使用技能: " + skill.getId() + " - " + skill.getName() + " 技能等级: " + skillLevel);
            return;
        }
        attack = DamageParse.Modify_AttackCrit(attack, chr, 3, effect);
        if (GameConstants.isEventMap(chr.getMapId()))
        {
            for (MapleEventType t : MapleEventType.values())
            {
                MapleEvent e = ChannelServer.getInstance(chr.getClient().getChannel()).getEvent(t);
                if ((e.isRunning()) && (!chr.isGM()))
                {
                    for (int i : e.getType().mapids)
                    {
                        if (chr.getMapId() == i)
                        {
                            chr.dropMessage(5, "无法在这个地方使用.");
                            return;
                        }
                    }
                }
            }
        }
        double maxdamage = chr.getStat().getCurrentMaxBaseDamage() * (effect.getDamage() + chr.getStat().getDamageIncrease(attack.skillId)) / 100.0D;
        if (GameConstants.isPyramidSkill(attack.skillId))
        {
            maxdamage = 1.0D;
        }
        else if ((GameConstants.is新手职业(skill.getId() / 10000)) && (skill.getId() % 10000 == 1000))
        {
            maxdamage = 40.0D;
        }
        int cooldownTime = effect.getCooldown(chr);
        if (cooldownTime > 0)
        {
            if (chr.skillisCooling(attack.skillId))
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            c.getSession().write(MaplePacketCreator.skillCooldown(attack.skillId, cooldownTime));
            chr.addCooldown(attack.skillId, System.currentTimeMillis(), cooldownTime * 1000);
        }
        chr.checkFollow();
        if (!chr.isHidden())
        {
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.magicAttack(chr, skillLevel, 0, attack), chr.getTruePosition());
        }
        else
        {
            chr.getMap().broadcastGMMessage(chr, MaplePacketCreator.magicAttack(chr, skillLevel, 0, attack), false);
        }
        DamageParse.applyAttackMagic(attack, skill, c.getPlayer(), effect, maxdamage);
    }

    public static void DropMeso(int meso, MapleCharacter chr)
    {
        if ((!chr.isAlive()) || (meso < 10) || (meso > 50000) || (meso > chr.getMeso()))
        {
            chr.getClient().getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.gainMeso(-meso, false, true);
        chr.getMap().spawnMesoDrop(meso, chr.getTruePosition(), chr, chr, true, (byte) 0);
        chr.getCheatTracker().checkDrop(true);
    }


    public static void ChangeAndroidEmotion(int emote, MapleCharacter chr)
    {
        if ((emote > 0) && (chr != null) && (chr.getMap() != null) && (!chr.isHidden()) && (emote <= 17) && (chr.getAndroid() != null))
        {
            chr.getMap().broadcastMessage(tools.packet.AndroidPacket.showAndroidEmotion(chr.getId(), emote));
        }
    }


    public static void MoveAndroid(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        slea.skip(4);
        slea.skip(4);
        slea.skip(4);
        List<server.movement.LifeMovementFragment> res = MovementParse.parseMovement(slea, 3);
        if ((res != null) && (chr != null) && (!res.isEmpty()) && (chr.getMap() != null) && (chr.getAndroid() != null))
        {
            Point pos = new Point(chr.getAndroid().getPos());
            chr.getAndroid().updatePosition(res);
            chr.getMap().broadcastMessage(chr, tools.packet.AndroidPacket.moveAndroid(chr.getId(), pos, res), false);
        }
    }


    public static void ChangeEmotion(int emote, MapleCharacter chr)
    {
        if (emote > 7)
        {
            int emoteid = 5159992 + emote;
            MapleInventoryType type = constants.ItemConstants.getInventoryType(emoteid);
            if (chr.getInventory(type).findById(emoteid) == null)
            {
                chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.USING_UNAVAILABLE_ITEM, Integer.toString(emoteid));
                return;
            }
        }
        if ((emote > 0) && (chr != null) && (chr.getMap() != null) && (!chr.isHidden()))
        {
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.facialExpression(chr, emote), false);
        }
    }


    public static void Heal(SeekableLittleEndianAccessor slea, MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        chr.updateTick(slea.readInt());
        slea.skip(4);
        slea.skip(4);
        int healHP = slea.readShort();
        int healMP = slea.readShort();
        PlayerStats stats = chr.getStat();
        if (stats.getHp() <= 0)
        {
            return;
        }
        long now = System.currentTimeMillis();
        if ((healHP != 0) && (chr.canHP(now + 1000L)))
        {
            if (healHP > stats.getHealHP())
            {
                healHP = (int) stats.getHealHP();
            }
            chr.addHP(healHP);
        }
        if ((healMP != 0) && (!GameConstants.isNotMpJob(chr.getJob())) && (chr.canMP(now + 1000L)))
        {
            if (healMP > stats.getHealMP())
            {
                healMP = (int) stats.getHealMP();
            }
            chr.addMP(healMP);
        }
    }

    public static void MovePlayer(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        Point Original_Pos = chr.getPosition();
        slea.skip(22);
        List<server.movement.LifeMovementFragment> res;
        try
        {
            res = MovementParse.parseMovement(slea, 1);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println("AIOBE Type1:\r\n" + slea.toString(true));
            return;
        }
        if ((res != null) && (chr.getMap() != null))
        {
            if (slea.available() != 8L)
            {
                System.out.println("slea.available != 8 (角色移动出错) 剩余封包长度: " + slea.available());
                FileoutputUtil.log("log\\Movement.log", "slea.available != 8 (角色移动出错) 封包: " + slea.toString(true));
                return;
            }
            MapleMap map = c.getPlayer().getMap();

            if (chr.isHidden())
            {
                chr.setLastRes(res);
                chr.getMap().broadcastGMMessage(chr, MaplePacketCreator.movePlayer(chr.getId(), res, Original_Pos), false);
            }
            else
            {
                chr.getMap().broadcastMessage(chr, MaplePacketCreator.movePlayer(chr.getId(), res, Original_Pos), false);
            }

            MovementParse.updatePosition(res, chr, 0);
            Point pos = chr.getTruePosition();
            map.movePlayer(chr, pos);
            if ((chr.getFollowId() > 0) && (chr.isFollowOn()) && (chr.isFollowInitiator()))
            {
                MapleCharacter fol = map.getCharacterById(chr.getFollowId());
                if (fol != null)
                {
                    Point original_pos = fol.getPosition();
                    fol.getClient().getSession().write(MaplePacketCreator.moveFollow(Original_Pos, original_pos, pos, res));
                    MovementParse.updatePosition(res, fol, 0);
                    map.movePlayer(fol, pos);
                    map.broadcastMessage(fol, MaplePacketCreator.movePlayer(fol.getId(), res, original_pos), false);
                }
                else
                {
                    chr.checkFollow();
                }
            }
            int count = chr.getFallCounter();
            boolean samepos = (pos.y > chr.getOldPosition().y) && (Math.abs(pos.x - chr.getOldPosition().x) < 5);
            if ((samepos) && ((pos.y > map.getBottom() + 250) || (map.getFootholds().findBelow(pos) == null)))
            {
                if (count > 5)
                {
                    chr.changeMap(map, map.getPortal(0));
                    chr.setFallCounter(0);
                }
                else
                {
                    chr.setFallCounter(++count);
                }
            }
            else if (count > 0)
            {
                chr.setFallCounter(0);
            }
            chr.setOldPosition(pos);
            if ((!samepos) && (chr.getBuffSource(MapleBuffStat.黑暗灵气) == 32120000))
            {
                chr.getStatForBuff(MapleBuffStat.黑暗灵气).applyMonsterBuff(c.getPlayer());
            }
            else if ((!samepos) && (chr.getBuffSource(MapleBuffStat.黄色灵气) == 32120001))
            {
                chr.getStatForBuff(MapleBuffStat.黄色灵气).applyMonsterBuff(c.getPlayer());
            }
            constants.BattleConstants.PokemonMap mapp = constants.BattleConstants.getMap(chr.getMapId());
            if ((!samepos) && (chr.getBattler(0) != null) && (mapp != null) && (!chr.isHidden()) && (!chr.hasBlockedInventory()))
                if (Randomizer.nextInt(c.getPlayer().getBattler(0).getAbility() == constants.BattleConstants.PokemonAbility.Illuminate ? 5 :
                        chr.getBattler(0).getAbility() == constants.BattleConstants.PokemonAbility.Stench ? 20 : 10) == 0)
                {
                    java.util.LinkedList<Pair<Integer, Integer>> set = constants.BattleConstants.getMobs(mapp);
                    java.util.Collections.shuffle(set);
                    int resulting = 0;
                    for (Pair<Integer, Integer> i : set)
                    {
                        if (Randomizer.nextInt(i.right) == 0)
                        {
                            resulting = i.left;
                            break;
                        }
                    }
                    if (resulting > 0)
                    {
                        PokemonBattle wild = new PokemonBattle(chr, resulting, mapp);
                        chr.changeMap(wild.getMap(), wild.getMap().getPortal(mapp.portalId));
                        if (chr != null)
                        {
                            chr.setBattle(wild);
                            wild.initiate(chr, mapp);
                        }
                    }
                }
        }
    }

    public static void ChangeMapSpecial(String portal_name, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        MaplePortal portal = chr.getMap().getPortal(portal_name);
        if ((portal != null) && (!chr.hasBlockedInventory()))
        {
            portal.enterPortal(c);
        }
        else
        {
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }

    public static void ChangeMap(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        if (chr.isBanned())
        {
            MapleMap to = ChannelServer.getInstance(c.getChannel()).getMapFactory().getMap(741000000);
            chr.changeMap(to, to.getPortal(0));
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        if (slea.available() != 0L)
        {
            slea.readByte();
            int targetid = slea.readInt();
            MaplePortal portal = chr.getMap().getPortal(slea.readMapleAsciiString());
            if (slea.available() >= 7L)
            {
                chr.updateTick(slea.readInt());
            }
            slea.skip(1);
            boolean wheel = (slea.readShort() > 0) && (!GameConstants.isEventMap(chr.getMapId())) && (chr.haveItem(5510000, 1, false, true)) && (chr.getMapId() / 1000000 != 925);
            if ((targetid != -1) && (!chr.isAlive()))
            {
                chr.setStance(0);
                if ((chr.getEventInstance() != null) && (chr.getEventInstance().revivePlayer(chr)) && (chr.isAlive()))
                {
                    return;
                }
                if (chr.getPyramidSubway() != null)
                {
                    chr.getStat().setHp(50, chr);
                    chr.getPyramidSubway().fail(chr);
                    return;
                }
                if (!wheel)
                {
                    chr.getStat().setHp(50, chr);
                    MapleMap to = chr.getMap().getReturnMap();
                    chr.changeMap(to, to.getPortal(0));
                }
                else
                {
                    c.getSession().write(MTSCSPacket.useWheel((byte) (chr.getInventory(MapleInventoryType.CASH).countById(5510000) - 1)));
                    chr.getStat().setHp(chr.getStat().getMaxHp() / 100 * 40, chr);
                    server.MapleInventoryManipulator.removeById(c, MapleInventoryType.CASH, 5510000, 1, true, false);
                    MapleMap to = chr.getMap();
                    chr.changeMap(to, to.getPortal(0));
                }
            }
            else if ((targetid != -1) && (chr.isIntern()))
            {
                MapleMap to = ChannelServer.getInstance(c.getChannel()).getMapFactory().getMap(targetid);
                chr.changeMap(to, to.getPortal(0));
            }
            else if ((targetid != -1) && (!chr.isIntern()))
            {
                int divi = chr.getMapId() / 100;
                boolean unlock = false;
                boolean warp = false;
                if (divi == 9130401)
                {
                    warp = (targetid / 100 == 9130400) || (targetid / 100 == 9130401);
                    if (targetid / 10000 != 91304)
                    {
                        warp = true;
                        unlock = true;
                        targetid = 130030000;
                    }
                }
                else if (divi == 9130400)
                {
                    warp = (targetid / 100 == 9130400) || (targetid / 100 == 9130401);
                    if (targetid / 10000 != 91304)
                    {
                        warp = true;
                        unlock = true;
                        targetid = 130030000;
                    }
                }
                else if (divi == 9140900)
                {
                    warp = (targetid == 914090011) || (targetid == 914090012) || (targetid == 914090013) || (targetid == 140090000);
                }
                else if ((divi == 9120601) || (divi == 9140602) || (divi == 9140603) || (divi == 9140604) || (divi == 9140605))
                {
                    warp = (targetid == 912060100) || (targetid == 912060200) || (targetid == 912060300) || (targetid == 912060400) || (targetid == 912060500) || (targetid == 3000100);
                    unlock = true;
                }
                else if (divi == 9101500)
                {
                    warp = (targetid == 910150006) || (targetid == 101050010);
                    unlock = true;
                }
                else if ((divi == 9140901) && (targetid == 140000000))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((divi == 9240200) && (targetid == 924020000))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((targetid == 980040000) && (divi >= 9800410) && (divi <= 9800450))
                {
                    warp = true;
                }
                else if ((divi == 9140902) && ((targetid == 140030000) || (targetid == 140000000)))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((divi == 9000900) && (targetid / 100 == 9000900) && (targetid > chr.getMapId()))
                {
                    warp = true;
                }
                else if ((divi / 1000 == 9000) && (targetid / 100000 == 9000))
                {
                    unlock = (targetid < 900090000) || (targetid > 900090004);
                    warp = true;
                }
                else if ((divi / 10 == 1020) && (targetid == 1020000))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((chr.getMapId() == 900090101) && (targetid == 100030100))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((chr.getMapId() == 2010000) && (targetid == 104000000))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((chr.getMapId() == 106020001) || (chr.getMapId() == 106020502))
                {
                    if (targetid == chr.getMapId() - 1)
                    {
                        unlock = true;
                        warp = true;
                    }
                }
                else if ((chr.getMapId() == 0) && (targetid == 10000))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((chr.getMapId() == 931000011) && (targetid == 931000012))
                {
                    unlock = true;
                    warp = true;
                }
                else if ((chr.getMapId() == 931000021) && (targetid == 931000030))
                {
                    unlock = true;
                    warp = true;
                }
                if (unlock)
                {
                    c.getSession().write(tools.packet.UIPacket.IntroDisableUI(false));
                    c.getSession().write(tools.packet.UIPacket.IntroLock(false));
                    c.getSession().write(MaplePacketCreator.enableActions());
                }
                if (warp)
                {
                    MapleMap to = ChannelServer.getInstance(c.getChannel()).getMapFactory().getMap(targetid);
                    chr.changeMap(to, to.getPortal(0));
                }
            }
            else if ((portal != null) && (!chr.hasBlockedInventory()))
            {
                portal.enterPortal(c);
            }
            else
            {
                c.getSession().write(MaplePacketCreator.enableActions());
            }
        }
    }

    public static void InnerPortal(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            return;
        }
        MaplePortal portal = chr.getMap().getPortal(slea.readMapleAsciiString());
        int toX = slea.readShort();
        int toY = slea.readShort();


        if (portal == null) return;
        if ((portal.getPosition().distanceSq(chr.getTruePosition()) > 22500.0D) && (!chr.isGM()))
        {
            chr.getCheatTracker().registerOffense(client.anticheat.CheatingOffense.USING_FARAWAY_PORTAL);
            return;
        }
        chr.getMap().movePlayer(chr, new Point(toX, toY));
        chr.checkFollow();
    }


    public static void snowBall(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.enableActions());
    }

    public static void leftKnockBack(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        if (c.getPlayer().getMapId() / 10000 == 10906)
        {
            c.getSession().write(MaplePacketCreator.leftKnockBack());
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void ReIssueMedal(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        int questId = slea.readShort();
        int itemId = slea.readInt();
        MapleQuest quest = MapleQuest.getInstance(questId);
        if ((((quest != null ? 1 : 0) & (quest.getMedalItem() > 0 ? 1 : 0)) != 0) && (chr.getQuestStatus(quest.getId()) == 2) && (quest.getMedalItem() == itemId))
        {
            if (!chr.haveItem(itemId))
            {
                int price = 100;
                int infoQuestId = 29949;
                String infoData = "count=1";
                if (chr.containsInfoQuest(infoQuestId, "count="))
                {
                    String line = chr.getInfoQuest(infoQuestId);
                    String[] splitted = line.split("=");
                    if (splitted.length == 2)
                    {
                        int data = Integer.parseInt(splitted[1]);
                        infoData = "count=" + (data + 1);
                        if (data == 1)
                        {
                            price = 1000;
                        }
                        else if (data == 2)
                        {
                            price = 10000;
                        }
                        else if (data == 3)
                        {
                            price = 100000;
                        }
                        else
                        {
                            price = 1000000;
                        }
                    }
                    else
                    {
                        chr.dropMessage(1, "重新领取勋章出现错误");
                        c.getSession().write(MaplePacketCreator.enableActions());
                        return;
                    }
                }
                if (chr.getMeso() < price)
                {
                    chr.dropMessage(1, "本次重新需要金币: " + price + "\r\n请检查金币是否足够");
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
                chr.gainMeso(-price, true, true);
                server.MapleInventoryManipulator.addById(c, itemId, (short) 1, "");
                chr.updateInfoQuest(infoQuestId, infoData);
                c.getSession().write(MaplePacketCreator.updateMedalQuestInfo((byte) 0, itemId));
            }
            else
            {
                c.getSession().write(MaplePacketCreator.updateMedalQuestInfo((byte) 3, itemId));
            }
        }
        else
        {
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void PlayerUpdate(MapleClient c, MapleCharacter chr)
    {
        boolean autoSave = true;
        if ((!autoSave) || (chr == null) || (chr.getMap() == null))
        {
            return;
        }
        if (chr.getCheatTracker().canSaveDB())
        {
            long startTime = System.currentTimeMillis();
            chr.saveToDB(false, false);
            if (chr.isAdmin())
            {
                long endTime = System.currentTimeMillis() - startTime;
                chr.dropMessage(-11, "更新保存玩家数据 耗时: " + endTime + " 毫秒 ==> " + endTime / 1000L + " 秒..");
            }
        }
        else if (chr.isAdmin())
        {
            chr.dropMessage(-11, "更新保存玩家数据 离下次保存还有: " + chr.getCheatTracker().getlastSaveTime() + " 秒..");
        }
    }


    public static void TeachSkill(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.hasBlockedInventory()) || (chr.getLevel() < 70))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int skillId = slea.readInt();
        if (chr.getSkillLevel(skillId) < 1)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int toChrId = slea.readInt();
        Pair<String, Integer> toChrInfo = client.MapleCharacterUtil.getNameById(toChrId, 0);
        if (toChrInfo == null)
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int toChrAccId = toChrInfo.getRight();
        String toChrName = toChrInfo.getLeft();
        MapleQuest quest = MapleQuest.getInstance(7783);
        if ((quest != null) && (chr.getAccountID() == toChrAccId))
        {
            int toSkillId;
            if (GameConstants.is火炮手(chr.getJob()))
            {
                toSkillId = 80000000;
            }
            else
            {
                if (GameConstants.is恶魔猎手(chr.getJob()))
                {
                    toSkillId = 80000001;
                }
                else
                {
                    if (GameConstants.is双弩精灵(chr.getJob()))
                    {
                        toSkillId = 80001040;
                    }
                    else
                    {
                        if (GameConstants.is幻影(chr.getJob()))
                        {
                            toSkillId = 80000002;
                        }
                        else
                        {
                            if (GameConstants.is夜光(chr.getJob()))
                            {
                                toSkillId = 80000005;
                            }
                            else
                            {
                                if (GameConstants.is米哈尔(chr.getJob()))
                                {
                                    toSkillId = 80001140;
                                }
                                else
                                {
                                    if (GameConstants.is狂龙战士(chr.getJob()))
                                    {
                                        toSkillId = 80000006;
                                    }
                                    else
                                    {
                                        if (GameConstants.is爆莉萌天使(chr.getJob()))
                                        {
                                            toSkillId = 80001155;
                                        }
                                        else
                                        {
                                            if (GameConstants.is恶魔复仇者(chr.getJob()))
                                            {
                                                toSkillId = 80000050;
                                            }
                                            else
                                            {
                                                if (GameConstants.is尖兵(chr.getJob()))
                                                {
                                                    toSkillId = 80000047;
                                                }
                                                else
                                                {
                                                    if (GameConstants.is龙的传人(chr.getJob()))
                                                    {
                                                        toSkillId = 80001151;
                                                    }
                                                    else
                                                    {
                                                        chr.dropMessage(1, "传授技能失败");
                                                        c.getSession().write(MaplePacketCreator.enableActions());
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if ((chr.teachSkill(toSkillId, toChrId) > 0) && (toSkillId >= 80000000))
            {
                chr.changeTeachSkill(skillId, toChrId);
                quest.forceComplete(chr, 0);
                c.getSession().write(MaplePacketCreator.giveCharacterSkill(skillId, toChrId, toChrName));
            }
            else
            {
                chr.dropMessage(1, "传授技能失败角色[" + toChrName + "]已经获得该技能");
            }
        }
        else
        {
            chr.dropMessage(1, "传授技能失败。");
        }
        c.getSession().write(MaplePacketCreator.enableActions());
    }


    public static void ChangeMarketMap(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.hasBlockedInventory()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int chc = slea.readByte() + 1;
        int toMapId = slea.readInt();

        if ((toMapId >= 910000001) && (toMapId <= 910000022))
        {
            if (c.getChannel() != chc)
            {
                if (chr.getMapId() != toMapId)
                {
                    MapleMap to = ChannelServer.getInstance(chc).getMapFactory().getMap(toMapId);
                    chr.setMap(to);
                    chr.changeChannel(chc);
                }
                else
                {
                    chr.changeChannel(chc);
                }
            }
            else
            {
                MapleMap to = ChannelServer.getInstance(c.getChannel()).getMapFactory().getMap(toMapId);
                chr.changeMap(to, to.getPortal(0));
            }
        }
        else
        {
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }


    public static void 超时空卷(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null) || (chr.hasBlockedInventory()))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        chr.updateTick(slea.readInt());
        int toMapId = slea.readInt();
        if (isBossMap(toMapId))
        {
            c.getSession().write(MTSCSPacket.getTrockMessage((byte) 11));
            c.getSession().write(MaplePacketCreator.时空移动错误());
            return;
        }
        MapleMap moveTo = ChannelServer.getInstance(c.getChannel()).getMapFactory().getMap(toMapId);
        if (moveTo == null)
        {
            c.getSession().write(MTSCSPacket.getTrockMessage((byte) 11));
            c.getSession().write(MaplePacketCreator.时空移动错误());
        }
        else if (chr.haveItem(5040005, 1))
        {
            chr.removeAll(5040005);
            chr.changeMap(moveTo, moveTo.getPortal(0));
        }
        else
        {
            if (chr.getBossLog("超时空卷") < 30)
            {
                chr.setBossLog("超时空卷");
                chr.dropMessage(5, "您使用了" + c.getChannelServer().getServerName() + "传送功能从 " + chr.getMap().getMapName() + " --> " + moveTo.getMapName() + " 今天还可以免费使用: " + (30 - chr.getBossLog(
                        "超时空卷")) + " 次。");
                chr.changeMap(moveTo, moveTo.getPortal(0));
            }
            else if (chr.getCSPoints(2) >= 200)
            {
                chr.dropMessage(5, "您使用了" + c.getChannelServer().getServerName() + "传送功能从 " + chr.getMap().getMapName() + " --> " + moveTo.getMapName() + " 抵用卷减少 200 点。");
                chr.changeMap(moveTo, moveTo.getPortal(0));
                chr.modifyCSPoints(2, 65336);
            }
            else
            {
                chr.dropMessage(5, "传送失败，您今天的免费传送次数已经用完或者您的抵用卷不足200点。");
            }
            if (chr.getBossLog("超时空卷") == 30)
            {
                chr.dropMessage(1, "今天的免费传送次数已经使用完\r\n再次使用将消耗抵用卷200点。");
            }
        }
    }

    public static boolean isBossMap(int mapid)
    {
        switch (mapid)
        {
            case 0:
            case 105100300:
            case 105100400:
            case 211070100:
            case 211070101:
            case 211070110:
            case 220080001:
            case 240040700:
            case 240060200:
            case 240060201:
            case 270050100:
            case 271040100:
            case 271040200:
            case 280030000:
            case 280030001:
            case 280030100:
            case 300030310:
            case 551030200:
            case 802000111:
            case 802000211:
            case 802000311:
            case 802000411:
            case 802000611:
            case 802000711:
            case 802000801:
            case 802000802:
            case 802000803:
            case 802000821:
            case 802000823:
                return true;
        }
        return false;
    }


    public static void useTempestBlades(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.hasBlockedInventory()) || (chr.getMap() == null) || (chr.getBuffedValue(MapleBuffStat.剑刃之壁) == null))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int skillId = chr.getTrueBuffSource(MapleBuffStat.剑刃之壁);
        int attackCount = skillId == 61120007 ? 5 : skillId == 61101002 ? 3 : 0;
        if (attackCount <= 0)
        {
            chr.cancelEffectFromBuffStat(MapleBuffStat.剑刃之壁);
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int mobs = slea.readInt();
        List<Integer> moboids = new ArrayList<>();
        for (int i = 0; i < mobs; i++)
        {
            int moboid = slea.readInt();
            MapleMonster mob = chr.getMap().getMonsterByOid(moboid);
            if ((mob != null) && (moboids.size() < attackCount))
            {
                moboids.add(moboid);
            }
        }
        if (!moboids.isEmpty())
        {
            chr.getSpecialStat().gainForceCounter();
            c.getSession().write(MaplePacketCreator.showTempestBladesAttack(chr.getId(), skillId, chr.getSpecialStat().getForceCounter(), moboids, attackCount));
            chr.getSpecialStat().gainForceCounter(attackCount - 1);
        }
        chr.cancelEffectFromBuffStat(MapleBuffStat.剑刃之壁);
    }


    public static void showPlayerCash(SeekableLittleEndianAccessor slea, MapleClient c)
    {
        int accId = slea.readInt();
        int playerId = slea.readInt();
    }


    public static void quickBuyCashShopItem(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int accId = slea.readInt();
        int playerId = slea.readInt();
        int mode = slea.readInt();
        int cssn = slea.readInt();
        int toCharge = slea.readByte() == 1 ? 1 : 2;
        if ((chr.getId() != playerId) || (chr.getAccountID() != accId))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        switch (mode)
        {
            case 10:
                if ((chr.getCSPoints(toCharge) >= 600) && (chr.getStorage().getSlots() < 93))
                {
                    chr.modifyCSPoints(toCharge, 64936, false);
                    chr.getStorage().increaseSlots((byte) 4);
                    chr.getStorage().saveToDB();
                    c.getSession().write(MaplePacketCreator.playerCashUpdate(mode, toCharge, chr));
                }
                else
                {
                    chr.dropMessage(5, "扩充失败，点卷余额不足或者仓库栏位已超过上限。");
                }
                break;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                int iv = mode == 15 ? 5 : mode == 14 ? 4 : mode == 13 ? 3 : mode == 12 ? 2 : mode == 11 ? 1 : -1;
                if (iv > 0)
                {
                    MapleInventoryType tpye = MapleInventoryType.getByType((byte) iv);
                    if ((chr.getCSPoints(toCharge) >= 600) && (chr.getInventory(tpye).getSlotLimit() < 93))
                    {
                        chr.modifyCSPoints(toCharge, 64936, false);
                        chr.getInventory(tpye).addSlot((byte) 4);
                        c.getSession().write(MaplePacketCreator.playerCashUpdate(mode, toCharge, chr));
                    }
                    else
                    {
                        chr.dropMessage(1, "扩充失败，点卷余额不足或者栏位已超过上限。");
                    }
                }
                else
                {
                    chr.dropMessage(1, "扩充失败，扩充的类型不正确。");
                }

                break;
        }

    }


    public static void changeZeroLook(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr, boolean end)
    {
        if ((chr == null) || (chr.getMap() == null))
        {
            c.getSession().write(MaplePacketCreator.enableActions());
            return;
        }

        if (end)
        {
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.removeZeroFromMap(chr.getId()), false);
        }
        else
        {
            chr.changeZeroLook();
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\channel\handler\PlayerHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */