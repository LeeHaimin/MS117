package handling.channel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import handling.world.CharacterTransfer;
import handling.world.CheaterData;
import handling.world.WorldFindService;

public class PlayerStorage
{
    private final int channel;
    private final ReentrantReadWriteLock mutex = new ReentrantReadWriteLock();
    private final Lock readLock = this.mutex.readLock();
    private final Lock writeLock = this.mutex.writeLock();
    private final ReentrantReadWriteLock mutex2 = new ReentrantReadWriteLock();
    private final Lock connectcheckReadLock = this.mutex2.readLock();
    private final Lock pendingWriteLock = this.mutex2.writeLock();
    private final Map<String, MapleCharacter> nameToChar = new HashMap<>();
    private final Map<Integer, MapleCharacter> idToChar = new HashMap<>();
    private final Map<Integer, CharacterTransfer> PendingCharacter = new HashMap<>();

    public PlayerStorage(int channel)
    {
        this.channel = channel;
        server.Timer.PingTimer.getInstance().register(new PersistingTask(), 60000L);
        server.Timer.PingTimer.getInstance().register(new ConnectChecker(), 30000L);
    }

    public java.util.ArrayList<MapleCharacter> getAllCharacters()
    {
        this.readLock.lock();
        try
        {
            return new ArrayList(this.idToChar.values());
        }
        finally
        {
            this.readLock.unlock();
        }
    }

    public void registerPlayer(MapleCharacter chr)
    {
        writeLock.lock();
        try
        {
            nameToChar.put(chr.getName().toLowerCase(), chr);
            idToChar.put(chr.getId(), chr);
        }
        finally
        {
            writeLock.unlock();
        }
        WorldFindService.getInstance().register(chr.getId(), chr.getName(), channel);
    }

    public void registerPendingPlayer(CharacterTransfer chr, int playerId)
    {
        pendingWriteLock.lock();
        try
        {
            PendingCharacter.put(playerId, chr);//new Pair(System.currentTimeMillis(), chr));
        }
        finally
        {
            pendingWriteLock.unlock();
        }
    }

    public void deregisterPlayer(MapleCharacter chr)
    {
        writeLock.lock();
        try
        {
            nameToChar.remove(chr.getName().toLowerCase());
            idToChar.remove(chr.getId());
        }
        finally
        {
            writeLock.unlock();
        }
        WorldFindService.getInstance().forceDeregister(chr.getId(), chr.getName());
    }

    public void deregisterPlayer(int idz, String namez)
    {
        writeLock.lock();
        try
        {
            nameToChar.remove(namez.toLowerCase());
            idToChar.remove(idz);
        }
        finally
        {
            writeLock.unlock();
        }
        WorldFindService.getInstance().forceDeregister(idz, namez);
    }

    public void disconnectPlayer(MapleCharacter chr)
    {
        chr.getClient().disconnect(true, true, true);
    }

    public int pendingCharacterSize()
    {
        return this.PendingCharacter.size();
    }

    public void deregisterPendingPlayer(int charId)
    {
        pendingWriteLock.lock();
        try
        {
            PendingCharacter.remove(charId);
        }
        finally
        {
            pendingWriteLock.unlock();
        }
    }

    public CharacterTransfer getPendingCharacter(int charId)
    {
        this.pendingWriteLock.lock();
        try
        {
            return this.PendingCharacter.remove(charId);
        }
        finally
        {
            this.pendingWriteLock.unlock();
        }
    }

    public MapleCharacter getCharacterByName(String name)
    {
        this.readLock.lock();
        try
        {
            return this.nameToChar.get(name.toLowerCase());
        }
        finally
        {
            this.readLock.unlock();
        }
    }

    public MapleCharacter getCharacterById(int id)
    {
        this.readLock.lock();
        try
        {
            return this.idToChar.get(id);
        }
        finally
        {
            this.readLock.unlock();
        }
    }

    public int getConnectedClients()
    {
        return this.idToChar.size();
    }

    public List<CheaterData> getCheaters()
    {
        List<CheaterData> cheaters = new ArrayList<>();
        this.readLock.lock();
        try
        {

            for (MapleCharacter chr : this.nameToChar.values())
            {
                if (chr.getCheatTracker().getPoints() > 0)
                {
                    cheaters.add(new CheaterData(chr.getCheatTracker().getPoints(),
                            client.MapleCharacterUtil.makeMapleReadable(chr.getName()) + " ID: " + chr.getId() + " (" + chr.getCheatTracker().getPoints() + ") " + chr.getCheatTracker().getSummary()));
                }
            }
        }
        finally
        {
            this.readLock.unlock();
        }
        return cheaters;
    }

    public List<CheaterData> getReports()
    {
        List<CheaterData> cheaters = new ArrayList<>();
        this.readLock.lock();
        try
        {

            for (MapleCharacter chr : this.nameToChar.values())
            {
                if (chr.getReportPoints() > 0)
                {
                    cheaters.add(new CheaterData(chr.getReportPoints(),
                            client.MapleCharacterUtil.makeMapleReadable(chr.getName()) + " ID: " + chr.getId() + " (" + chr.getReportPoints() + ") " + chr.getReportSummary()));
                }
            }
        }
        finally
        {
            this.readLock.unlock();
        }
        return cheaters;
    }


    public void disconnectAll()
    {
        disconnectAll(false);
    }


    public void disconnectAll(boolean checkGM)
    {
        this.writeLock.lock();
        try
        {
            Iterator<MapleCharacter> chrit = this.nameToChar.values().iterator();

            while (chrit.hasNext())
            {
                MapleCharacter chr = chrit.next();
                if ((!chr.isGM()) || (!checkGM))
                {
                    chr.getClient().disconnect(false, false, true);
                    if (chr.getClient().getSession().isConnected())
                    {
                        chr.getClient().getSession().close(true);
                    }
                    handling.world.WorldFindService.getInstance().forceDeregister(chr.getId(), chr.getName());
                    chrit.remove();
                }
            }
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    public String getOnlinePlayers(boolean byGM)
    {
        StringBuilder sb = new StringBuilder();
        if (byGM)
        {
            this.readLock.lock();
            try
            {
                for (MapleCharacter mapleCharacter : this.nameToChar.values())
                {
                    sb.append(client.MapleCharacterUtil.makeMapleReadable(mapleCharacter.getName()));
                    sb.append(", ");
                }
            }
            finally
            {
                this.readLock.unlock();
            }
        }
        else
        {
            this.readLock.lock();
            try
            {

                for (MapleCharacter chr : this.nameToChar.values())
                {
                    if (!chr.isGM())
                    {
                        sb.append(client.MapleCharacterUtil.makeMapleReadable(chr.getName()));
                        sb.append(", ");
                    }
                }
            }
            finally
            {
                this.readLock.unlock();
            }
        }
        return sb.toString();
    }

    public void broadcastPacket(byte[] data)
    {
        readLock.lock();
        try
        {
            for (MapleCharacter mapleCharacter : nameToChar.values())
            {
                mapleCharacter.getClient().getSession().write(data);
            }
        }
        finally
        {
            readLock.unlock();
        }
    }

    public void broadcastSmegaPacket(byte[] data)
    {
        this.readLock.lock();
        try
        {

            for (MapleCharacter chr : this.nameToChar.values())
            {
                if ((chr.getClient().isLoggedIn()) && (chr.getSmega()))
                {
                    chr.getClient().getSession().write(data);
                }
            }
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    public void broadcastGMPacket(byte[] data)
    {
        this.readLock.lock();
        try
        {

            for (MapleCharacter chr : this.nameToChar.values())
            {
                if ((chr.getClient().isLoggedIn()) && (chr.isIntern()))
                {
                    chr.getClient().getSession().write(data);
                }
            }
        }
        finally
        {
            this.readLock.unlock();
        }
    }

    public class PersistingTask implements Runnable
    {
        public PersistingTask()
        {
        }

        public void run()
        {
            PlayerStorage.this.pendingWriteLock.lock();
            try
            {
                long currenttime = System.currentTimeMillis();
                Iterator<Map.Entry<Integer, CharacterTransfer>> itr = PlayerStorage.this.PendingCharacter.entrySet().iterator();
                while (itr.hasNext())
                {
                    if (currenttime - ((CharacterTransfer) ((Map.Entry) itr.next()).getValue()).TranferTime > 40000L)
                    {
                        itr.remove();
                    }
                }
            }
            finally
            {
                PlayerStorage.this.pendingWriteLock.unlock();
            }
        }
    }

    private class ConnectChecker implements Runnable
    {
        private ConnectChecker()
        {
        }

        public void run()
        {
            PlayerStorage.this.connectcheckReadLock.lock();
            try
            {
                Iterator<MapleCharacter> chrit = PlayerStorage.this.nameToChar.values().iterator();
                Map<Integer, MapleCharacter> disconnectList = new LinkedHashMap<>();

                while (chrit.hasNext())
                {
                    MapleCharacter player = chrit.next();
                    if ((player != null) && (player.getClient().getSession().isClosing()))
                    {
                        disconnectList.put(player.getId(), player);
                    }
                }
                Iterator<MapleCharacter> dcitr = disconnectList.values().iterator();
                while (dcitr.hasNext())
                {
                    MapleCharacter player = dcitr.next();
                    if (player != null)
                    {
                        player.getClient().disconnect(false, false);
                        player.getClient().updateLoginState(0);
                        PlayerStorage.this.disconnectPlayer(player);
                        dcitr.remove();
                    }
                }
            }
            finally
            {
                PlayerStorage.this.connectcheckReadLock.unlock();
            }
        }
    }
}