package handling;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import constants.ServerConstants;


public enum RecvPacketOpcode implements WritableIntValueHolder
{
    PONG(false),

    CLIENT_HELLO(false),


    LOGIN_PASSWORD(false),

    CHARLIST_REQUEST,

    CHAR_SELECT,

    PLAYER_LOGGEDIN(false),

    CHECK_CHAR_NAME,

    CREATE_CHAR,

    DELETE_CHAR,

    CREATE_ULTIMATE,

    CLIENT_ERROR(false),

    STRANGE_DATA,

    AUTH_SECOND_PASSWORD,

    SET_WORK,

    CHARACTER_CARDS,

    LICENSE_REQUEST,

    SET_GENDER,

    SET_CHAR_CARDS,

    SET_ACC_CASH,

    QUICK_BUY_CS_ITEM,

    SERVERSTATUS_REQUEST,

    SERVERLIST_REQUEST, SEND_ENCRYPTED(false), REDISPLAY_SERVERLIST, VIEW_ALL_CHAR, VIEW_REGISTER_PIC, VIEW_SELECT_PIC, PICK_ALL_CHAR, CHAR_SELECT_NO_PIC, VIEW_SERVERLIST,

    RSA_KEY(false),

    CLIENT_ERROR1(false), CLIENT_START(false), CLIENT_FAILED(false),


    CHANGE_MAP,

    CHANGE_CHANNEL,

    ENTER_CASH_SHOP,

    MOVE_PLAYER,

    CANCEL_CHAIR,

    USE_CHAIR,

    CLOSE_RANGE_ATTACK,

    RANGED_ATTACK,

    MAGIC_ATTACK,

    PASSIVE_ENERGY,

    TAKE_DAMAGE,

    GENERAL_CHAT,

    CLOSE_CHALKBOARD,

    FACE_EXPRESSION,

    FACE_ANDROID,

    USE_ITEM_EFFECT,

    WHEEL_OF_FORTUNE,

    USE_TITLE_EFFECT,

    USE_UNK_EFFECT,

    NPC_TALK,

    REMOTE_STORE,

    NPC_TALK_MORE,

    NPC_SHOP,

    STORAGE,

    USE_HIRED_MERCHANT,

    MERCH_ITEM_STORE,

    DUEY_ACTION,

    MECH_CANCEL,

    USE_HOLY_FOUNTAIN,

    OWL,

    OWL_WARP,

    ITEM_SORT,

    ITEM_GATHER,

    ITEM_MOVE,

    MOVE_BAG,

    SWITCH_BAG,

    USE_ITEM,

    CANCEL_ITEM_EFFECT,

    USE_SUMMON_BAG,

    PET_FOOD,

    USE_MOUNT_FOOD,

    USE_SCRIPTED_NPC_ITEM,

    USE_RECIPE,

    USE_NEBULITE, USE_ALIEN_SOCKET, USE_ALIEN_SOCKET_RESPONSE, USE_NEBULITE_FUSION,

    USE_CASH_ITEM,

    USE_ADDITIONAL_ITEM,

    ALLOW_PET_LOOT,

    ALLOW_PET_AOTO_EAT,

    USE_CATCH_ITEM,

    USE_SKILL_BOOK,

    USE_SP_RESET,

    USE_AP_RESET,

    POTION_POT_USE,

    POTION_POT_ADD,

    POTION_POT_MODE,

    POTION_POT_INCR,

    USE_OWL_MINERVA,

    USE_TELE_ROCK,

    USE_RETURN_SCROLL,

    USE_UPGRADE_SCROLL,

    USE_FLAG_SCROLL,

    USE_EQUIP_SCROLL,

    USE_POTENTIAL_SCROLL,

    USE_POTENTIAL_ADD_SCROLL,

    USE_SOULS_SCROLL,

    USE_SOUL_MARBLE,

    USE_BAG,

    USE_MAGNIFY_GLASS,

    DISTRIBUTE_AP,

    AUTO_ASSIGN_AP,

    HEAL_OVER_TIME,

    TEACH_SKILL,

    DISTRIBUTE_SP,

    SPECIAL_MOVE,

    CANCEL_BUFF,

    SKILL_EFFECT,

    MESO_DROP,

    GIVE_FAME,

    CHAR_INFO_REQUEST,

    SPAWN_PET,

    PET_AUTO_BUFF,

    CANCEL_DEBUFF,

    CHANGE_MAP_SPECIAL,

    UNK0A3,

    USE_INNER_PORTAL,

    TROCK_ADD_MAP,

    LIE_DETECTOR,

    LIE_DETECTOR_SKILL,

    LIE_DETECTOR_RESPONSE,

    LIE_DETECTOR_REFRESH,

    QUEST_ACTION,

    REISSUE_MEDAL,


    SPECIAL_ATTACK,

    SKILL_MACRO,

    REWARD_ITEM,

    ITEM_MAKER,


    REPAIR_ALL,

    REPAIR,

    SOLOMON,

    GACH_EXP,

    FOLLOW_REQUEST,

    FOLLOW_REPLY,

    AUTO_FOLLOW_REPLY,

    REPORT,

    PROFESSION_INFO,

    USE_POT,

    CLEAR_POT,

    FEED_POT,

    CURE_POT,

    REWARD_POT,

    USE_COSMETIC,

    USE_REDUCER,

    CHANGE_ZERO_LOOK,

    CHANGE_ZERO_LOOK_END,

    PARTYCHAT,

    WHISPER,

    MESSENGER,

    PLAYER_INTERACTION,

    PARTY_OPERATION,

    DENY_PARTY_REQUEST,

    ALLOW_PARTY_INVITE,

    EXPEDITION_OPERATION,

    EXPEDITION_LISTING,

    GUILD_OPERATION,

    DENY_GUILD_REQUEST,

    GUILD_APPLY,

    ACCEPT_GUILD_APPLY,

    DENY_GUILD_APPLY,

    ADMIN_COMMAND,

    ADMIN_LOG,

    BUDDYLIST_MODIFY,

    NOTE_ACTION,

    USE_DOOR,

    USE_MECH_DOOR,

    CHANGE_KEYMAP,

    RPS_GAME,

    RING_ACTION,

    ALLIANCE_OPERATION,

    DENY_ALLIANCE_REQUEST,

    REQUEST_FAMILY,

    OPEN_FAMILY,

    FAMILY_OPERATION,

    DELETE_JUNIOR,

    DELETE_SENIOR,

    ACCEPT_FAMILY,

    USE_FAMILY,

    FAMILY_PRECEPT,

    FAMILY_SUMMON,

    CYGNUS_SUMMON,

    ARAN_COMBO,

    LOST_ARAN_COMBO,

    CRAFT_DONE,

    CRAFT_EFFECT,

    CRAFT_MAKE,

    BBS_OPERATION,

    CHANGE_MARKET_MAP,


    CHANGE_PLAYER,

    MEMORY_SKILL_CHOOSE,

    MEMORY_SKILL_CHANGE,

    MEMORY_SKILL_OBTAIN,

    GAME_POLL,

    BUY_CROSS_ITEM,

    USE_TEMPEST_BLADES,

    DISTRIBUTE_HYPER_SP,

    RESET_HYPER_SP,

    UNKNOWN_168,

    MOVE_PET,

    PET_CHAT,

    PET_COMMAND,

    PET_LOOT,

    PET_AUTO_POT,

    PET_EXCEPTION_LIST,

    PET_AOTO_EAT,

    MOVE_SUMMON,

    SUMMON_ATTACK,

    DAMAGE_SUMMON,

    SUB_SUMMON,

    REMOVE_SUMMON,

    MOVE_DRAGON,

    DRAGON_FLY,

    MOVE_ANDROID,

    QUICK_SLOT,

    PLAYER_VIEW_RANGE,

    OPEN_ROOT_NPC,

    SYSTEM_PROCESS_LIST,

    SHOW_LOVE_RANK,

    TRANSFORM_PLAYER,

    OPEN_AVATAR_RANDOM_BOX,

    ENTER_MTS,

    USE_TREASUER_CHEST,

    SHIKONGJUAN,

    PAM_SONG,

    SET_CHAR_CASH,

    MOVE_LIFE,

    AUTO_AGGRO,

    FRIENDLY_DAMAGE,

    MONSTER_BOMB,

    HYPNOTIZE_DMG, MOB_BOMB, MOB_NODE, DISPLAY_NODE,

    NPC_ACTION,

    ITEM_PICKUP,

    DAMAGE_REACTOR,

    TOUCH_REACTOR,

    MAKE_EXTRACTOR,

    SNOWBALL, LEFT_KNOCK_BACK, COCONUT,

    MONSTER_CARNIVAL, SHIP_OBJECT,

    PLAYER_UPDATE,

    PARTY_SEARCH_START,

    PARTY_SEARCH_STOP,

    START_HARVEST,

    STOP_HARVEST,

    QUICK_MOVE,

    CS_UPDATE,

    BUY_CS_ITEM,

    COUPON_CODE,

    SEND_CS_GIFI,

    SEND_CS_HOT,

    MAPLETV,

    UPDATE_QUEST,

    QUEST_ITEM,

    USE_ITEM_QUEST,

    TOUCHING_MTS, MTS_TAB, CHANGE_SET, GET_BOOK_INFO, CLICK_REACTOR, USE_FAMILIAR, SPAWN_FAMILIAR, RENAME_FAMILIAR, MOVE_FAMILIAR, TOUCH_FAMILIAR, ATTACK_FAMILIAR, SIDEKICK_OPERATION,
    DENY_SIDEKICK_REQUEST, PVP_INFO, ENTER_PVP, ENTER_PVP_PARTY, LEAVE_PVP, PVP_RESPAWN, PVP_ATTACK, PVP_SUMMON,

    USE_HAMMER,

    HAMMER_RESPONSE,

    POINT_POWER;

    static
    {
        reloadValues();
    }

    private short code = -2;
    private boolean CheckState;

    RecvPacketOpcode()
    {
        this.CheckState = true;
    }

    RecvPacketOpcode(boolean CheckState)
    {
        this.CheckState = CheckState;
    }

    public static void reloadValues()
    {
        try
        {
            if (ServerConstants.loadop)
            {
                Properties props = new Properties();
                props.load(new FileInputStream("config/recvops.properties"));
                ExternalCodeTableGetter.populateValues(props, values());
            }
            else
            {
                ExternalCodeTableGetter.populateValues(getDefaultProperties(), values());
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException("加载 recvops.properties 文件出现错误", e);
        }
    }

    public static Properties getDefaultProperties() throws IOException
    {
        Properties props = new Properties();
        FileInputStream fileInputStream = new FileInputStream("config/recvops.properties");
        props.load(fileInputStream);
        fileInputStream.close();
        return props;
    }

    public short getValue()
    {
        return this.code;
    }

    public void setValue(short code)
    {
        this.code = code;
    }

    public boolean NeedsChecking()
    {
        return this.CheckState;
    }
}