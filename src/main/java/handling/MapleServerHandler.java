package handling;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import client.MapleClient;
import configs.ServerConfig;
import handling.cashshop.handler.CashShopOperation;
import handling.channel.handler.ChatHandler;
import handling.channel.handler.FamilyHandler;
import handling.channel.handler.GuildHandler;
import handling.channel.handler.HiredMerchantHandler;
import handling.channel.handler.InterServerHandler;
import handling.channel.handler.InventoryHandler;
import handling.channel.handler.ItemMakerHandler;
import handling.channel.handler.ItemScrollHandler;
import handling.channel.handler.MobHandler;
import handling.channel.handler.NPCHandler;
import handling.channel.handler.PartyHandler;
import handling.channel.handler.PetHandler;
import handling.channel.handler.PhantomMemorySkill;
import handling.channel.handler.PlayerHandler;
import handling.channel.handler.PlayersHandler;
import handling.channel.handler.PotionPotHandler;
import handling.channel.handler.StatsHandling;
import handling.channel.handler.SummonHandler;
import handling.channel.handler.UserInterfaceHandler;
import handling.login.LoginServer;
import handling.login.handler.CharSelectedHandler;
import handling.login.handler.CharlistRequestHandler;
import handling.login.handler.CheckCharNameHandler;
import handling.login.handler.ClientErrorLogHandler;
import handling.login.handler.CreateCharHandler;
import handling.login.handler.CreateUltimateHandler;
import handling.login.handler.DeleteCharHandler;
import handling.login.handler.LicenseRequestHandler;
import handling.login.handler.LoginPasswordHandler;
import handling.login.handler.MapLoginHandler;
import handling.login.handler.PacketErrorHandler;
import handling.login.handler.ServerListRequestHandler;
import handling.login.handler.ServerStatusRequestHandler;
import handling.login.handler.SetGenderHandler;
import handling.login.handler.ShowAccCash;
import handling.login.handler.ShowCharCards;
import handling.login.handler.UpdateCharCards;
import handling.login.handler.ViewCharHandler;
import handling.login.handler.WithSecondPasswordHandler;
import handling.login.handler.WithoutSecondPasswordHandler;
import scripting.npc.NPCScriptManager;
import tools.FileoutputUtil;
import tools.MapleAESOFB;
import tools.Pair;
import tools.data.input.ByteArrayByteStream;
import tools.data.input.GenericSeekableLittleEndianAccessor;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.LoginPacket;

public class MapleServerHandler extends IoHandlerAdapter
{
    private static final boolean show = false;
    private final List<String> BlockIPList = new ArrayList<>();
    private final Map<String, Pair<Long, Byte>> tracker = new ConcurrentHashMap();
    private int channel = -1;
    private ServerType type = null;

    public MapleServerHandler(ServerType type)
    {
        this.type = type;
    }

    public MapleServerHandler(int channel, ServerType type)
    {
        this.channel = channel;
        this.type = type;
    }

    public void sessionOpened(IoSession session) throws Exception
    {
        String address = session.getRemoteAddress().toString().split(":")[0];
        if (this.BlockIPList.contains(address))
        {
            session.close(true);
            return;
        }
        Pair<Long, Byte> track = this.tracker.get(address);
        byte count;
        if (track == null)
        {
            count = 1;
        }
        else
        {
            count = track.right;
            long difference = System.currentTimeMillis() - track.left;
            if (difference < 2000L)
            {
                count = (byte) (count + 1);
            }
            else if (difference > 20000L)
            {
                count = 1;
            }
            if (count > 10)
            {
                this.BlockIPList.add(address);
                this.tracker.remove(address);
                session.close(true);
                return;
            }
        }
        this.tracker.put(address, new Pair(System.currentTimeMillis(), count));

        String IP = address.substring(address.indexOf('/') + 1);
        if (this.channel > -1)
        {
            if (handling.channel.ChannelServer.getInstance(this.channel).isShutdown())
            {
                session.close(true);
                return;
            }
            if (!LoginServer.containsIPAuth(IP))
            {
                session.close(true);
            }
        }
        else if (this.type == ServerType.商城服务器)
        {
            if (handling.cashshop.CashShopServer.isShutdown())
            {
                session.close(true);
            }
        }
        else if ((this.type == ServerType.登录服务器) && (LoginServer.isShutdown()))
        {
            session.close(true);
            return;
        }

        LoginServer.removeIPAuth(IP);

        byte[] ivRecv = {70, 114, 122, 82};
        byte[] ivSend = {82, 48, 120, 115};
        ivRecv[3] = ((byte) (int) (Math.random() * 255.0D));
        ivSend[3] = ((byte) (int) (Math.random() * 255.0D));
        MapleAESOFB sendCypher = new MapleAESOFB(ivSend, (short) (65535 - ServerConfig.MAPLE_VERSION));
        MapleAESOFB recvCypher = new MapleAESOFB(ivRecv, ServerConfig.MAPLE_VERSION);
        MapleClient client = new MapleClient(sendCypher, recvCypher, session);
        client.setChannel(this.channel);

        handling.mina.MaplePacketDecoder.DecoderState decoderState = new handling.mina.MaplePacketDecoder.DecoderState();
        session.setAttribute(handling.mina.MaplePacketDecoder.DECODER_STATE_KEY, decoderState);
        session.write(LoginPacket.getHello(ServerConfig.MAPLE_VERSION, ivSend, ivRecv));
        session.setAttribute("CLIENT", client);

        if (server.ServerProperties.ShowPacket())
        {
            server.ServerProperties.loadSettings();
            RecvPacketOpcode.reloadValues();
            SendPacketOpcode.reloadValues();
            CashShopOpcode.reloadValues();
            InteractionOpcode.reloadValues();
        }

        StringBuilder sb = new StringBuilder();
        if (this.channel > -1)
        {
            sb.append("[Channel Server] Channel ").append(this.channel).append(" : ");
        }
        else if (this.type == ServerType.商城服务器)
        {
            sb.append("[Cash Server] ");
        }
        else
        {
            sb.append("[Login Server] ");
        }
        sb.append("IoSession opened ").append(address);
        System.out.println(sb.toString());
    }

    public void sessionClosed(IoSession session) throws Exception
    {
        final MapleClient client = (MapleClient) session.getAttribute(MapleClient.CLIENT_KEY);

        if (client != null)
        {
            try
            {
                client.disconnect(true, show);
            }
            finally
            {
                session.close();
                session.removeAttribute(MapleClient.CLIENT_KEY);
            }
        }
        super.sessionClosed(session);
    }

    public void sessionIdle(IoSession session, org.apache.mina.core.session.IdleStatus status) throws Exception
    {
        MapleClient client = (MapleClient) session.getAttribute("CLIENT");
        if (client != null)
        {
            client.sendPing();
            if (client.getPlayer() == null)
            {
            }
        }


        super.sessionIdle(session, status);
    }

    public void exceptionCaught(IoSession session, Throwable cause) throws Exception
    {
        if ((!(cause instanceof java.io.IOException)) && (session != null))
        {
            MapleClient client = (MapleClient) session.getAttribute("CLIENT");
            if ((client != null) && (client.getPlayer() != null))
            {
                client.getPlayer().saveToDB(false, false);
                FileoutputUtil.printError("log\\exceptionCaught.txt", cause, "Exception caught by: " + client.getPlayer());
            }
        }
    }

    public void messageReceived(IoSession session, Object message)
    {
        if ((message == null) || (session == null))
        {
            System.out.println("message == null");
            return;
        }
        SeekableLittleEndianAccessor slea = new GenericSeekableLittleEndianAccessor(new ByteArrayByteStream((byte[]) message));
        if (slea.available() < 2L)
        {
            return;
        }
        MapleClient client = (MapleClient) session.getAttribute("CLIENT");
        if ((client == null) || (!client.isReceiving()))
        {
            return;
        }
        short packetId = slea.readShort();
        for (RecvPacketOpcode recv : RecvPacketOpcode.values())
        {
            if (recv.getValue() == packetId)
            {
                if ((recv.NeedsChecking()) && (!client.isLoggedIn()))
                {
                    return;
                }
                try
                {
                    if ((client.getPlayer() != null) && (client.isMonitored()) && (show))
                    {
                        FileWriter fw = new FileWriter(new java.io.File("MonitorLogs/" + client.getPlayer().getName() + "_log.txt"), true);
                        fw.write(recv + " (" + Integer.toHexString(packetId) + ") Handled: \r\n" + slea.toString() + "\r\n");
                        fw.flush();
                        fw.close();
                    }
//                    System.out.println("Received data  :" + "\n" + tools.HexTool.toString((byte[]) message) + "\n" + tools.HexTool.toStringFromAscii((byte[]) message));
                    System.out.println("Received data Type:" + recv);
                    handlePacket(recv, slea, client, this.type);
                }
                catch (NegativeArraySizeException | ArrayIndexOutOfBoundsException e)
                {
                    if (!constants.ServerConstants.Use_Fixed_IV)
                    {
                        FileoutputUtil.log("log\\Packet_Except.log", "封包: " + lookupRecv(packetId) + "\r\n" + slea.toString(true));
                        FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
                    }
                }
                catch (Exception e)
                {
                    FileoutputUtil.log("log\\Packet_Except.log", "封包: " + lookupRecv(packetId) + "\r\n" + slea.toString(true));
                    FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
                }
                return;
            }
        }
    }

    public static void handlePacket(RecvPacketOpcode header, SeekableLittleEndianAccessor slea, MapleClient c, ServerType type) throws Exception
    {
        switch (header)
        {
            case PONG:
                c.pongReceived();
                break;
            case STRANGE_DATA:
            case PLAYER_VIEW_RANGE:
            case MAPLETV:
            case CANCEL_DEBUFF:
            case OPEN_AVATAR_RANDOM_BOX:
            case LIE_DETECTOR:
            case USE_UNK_EFFECT:
            case WHEEL_OF_FORTUNE:


            case RSA_KEY:
                break;

            case CLIENT_ERROR:
                ClientErrorLogHandler.handlePacket(slea, c);
                break;
            case CLIENT_ERROR1:
                PacketErrorHandler.handlePacket(slea, c);
                break;
            case LOGIN_PASSWORD:
                LoginPasswordHandler.handlePacket(slea, c);
                break;
            case LICENSE_REQUEST:
                LicenseRequestHandler.handlePacket(slea, c);
                break;
            case SET_GENDER:
                SetGenderHandler.handlePacket(slea, c);
                break;
            case CHARACTER_CARDS:
                UpdateCharCards.handlePacket(slea, c);
                break;
            case SET_CHAR_CARDS:
                ShowCharCards.handlePacket(slea, c);
                break;
            case SET_ACC_CASH:
                ShowAccCash.handlePacket(slea, c);
                break;
            case SEND_ENCRYPTED:
//                if (c.isLocalhost())
//                {
                LoginPasswordHandler.handlePacket(slea, c);
//                }
//                else
//                {
//                    c.getSession().write(LoginPacket.getCustomEncryption());
//                }
//                break;
            case CLIENT_START:
            case CLIENT_FAILED:
                c.getSession().write(LoginPacket.getCustomEncryption());
                break;
            case VIEW_SERVERLIST:
                if (slea.readByte() == 0)
                {
                    ServerListRequestHandler.handlePacket(c, false);
                }
                break;
            case REDISPLAY_SERVERLIST:
            case SERVERLIST_REQUEST:
                ServerListRequestHandler.handlePacket(c, true);
                break;
            case CLIENT_HELLO:
                MapLoginHandler.handlePacket(slea, c);
                break;
            case CHARLIST_REQUEST:
                CharlistRequestHandler.handlePacket(slea, c);
                break;
            case SERVERSTATUS_REQUEST:
                ServerStatusRequestHandler.handlePacket(c);
                break;
            case CHECK_CHAR_NAME:
                CheckCharNameHandler.handlePacket(slea, c);
                break;
            case CREATE_CHAR:
                CreateCharHandler.handlePacket(slea, c);
                break;
            case CREATE_ULTIMATE:
                CreateUltimateHandler.handlePacket(slea, c);
                break;
            case DELETE_CHAR:
                DeleteCharHandler.handlePacket(slea, c);
                break;
            case VIEW_ALL_CHAR:
                ViewCharHandler.handlePacket(slea, c);
                break;
            case PICK_ALL_CHAR:
                WithoutSecondPasswordHandler.handlePacket(slea, c, false, true);
                break;
            case CHAR_SELECT_NO_PIC:
                WithoutSecondPasswordHandler.handlePacket(slea, c, false, false);
                break;
            case VIEW_REGISTER_PIC:
                WithoutSecondPasswordHandler.handlePacket(slea, c, true, true);
                break;
            case CHAR_SELECT:
                CharSelectedHandler.handlePacket(slea, c);
                break;
            case VIEW_SELECT_PIC:
                WithSecondPasswordHandler.handlePacket(slea, c, true);
                break;
            case AUTH_SECOND_PASSWORD:
                WithSecondPasswordHandler.handlePacket(slea, c, false);
                break;


            case CHANGE_CHANNEL:
                InterServerHandler.ChangeChannel(slea, c, c.getPlayer());
                break;
            case PLAYER_LOGGEDIN:
                slea.readInt();
                int playerid = slea.readInt();

                InterServerHandler.Loggedin(playerid, c);
                break;
            case ENTER_PVP:
            case ENTER_PVP_PARTY:
                PlayersHandler.EnterPVP(slea, c);
                break;
            case PVP_RESPAWN:
                PlayersHandler.RespawnPVP(slea, c);
                break;
            case LEAVE_PVP:
                PlayersHandler.LeavePVP(slea, c);
                break;
            case PVP_ATTACK:
                PlayersHandler.AttackPVP(slea, c);
                break;
            case PVP_SUMMON:
                SummonHandler.SummonPVP(slea, c);
                break;
            case ENTER_CASH_SHOP:
                InterServerHandler.EnterCS(c, c.getPlayer());
                break;
            case ENTER_MTS:
                InterServerHandler.EnterMTS(c, c.getPlayer());
                break;
            case CHANGE_PLAYER:
                InterServerHandler.ChangePlayer(slea, c);
                break;
            case MOVE_PLAYER:
                PlayerHandler.MovePlayer(slea, c, c.getPlayer());
                break;
            case CHAR_INFO_REQUEST:
                c.getPlayer().updateTick(slea.readInt());
                PlayerHandler.CharInfoRequest(slea.readInt(), c, c.getPlayer());
                break;
            case CLOSE_RANGE_ATTACK:
                PlayerHandler.closeRangeAttack(slea, c, c.getPlayer(), false);
                break;
            case RANGED_ATTACK:
                PlayerHandler.rangedAttack(slea, c, c.getPlayer());
                break;
            case MAGIC_ATTACK:
                PlayerHandler.MagicDamage(slea, c, c.getPlayer());
                break;
            case SPECIAL_MOVE:
                PlayerHandler.SpecialMove(slea, c, c.getPlayer());
                break;
            case PASSIVE_ENERGY:
                PlayerHandler.closeRangeAttack(slea, c, c.getPlayer(), true);
                break;
            case GET_BOOK_INFO:
                PlayersHandler.MonsterBookInfoRequest(slea, c, c.getPlayer());
                break;
            case CHANGE_SET:
                PlayersHandler.ChangeSet(slea, c, c.getPlayer());
                break;
            case PROFESSION_INFO:
                ItemMakerHandler.ProfessionInfo(slea, c, c.getPlayer());
                break;
            case CRAFT_DONE:
                ItemMakerHandler.CraftComplete(slea, c, c.getPlayer());
                break;
            case CRAFT_MAKE:
                ItemMakerHandler.CraftMake(slea, c, c.getPlayer());
                break;
            case CRAFT_EFFECT:
                ItemMakerHandler.CraftEffect(slea, c, c.getPlayer());
                break;
            case START_HARVEST:
                ItemMakerHandler.StartHarvest(slea, c, c.getPlayer());
                break;
            case STOP_HARVEST:
                ItemMakerHandler.StopHarvest(slea, c, c.getPlayer());
                break;
            case MAKE_EXTRACTOR:
                ItemMakerHandler.MakeExtractor(slea, c, c.getPlayer());
                break;
            case USE_BAG:
                ItemMakerHandler.UseBag(slea, c, c.getPlayer());
                break;
            case USE_FAMILIAR:
                MobHandler.UseFamiliar(slea, c, c.getPlayer());
                break;
            case SPAWN_FAMILIAR:
                MobHandler.SpawnFamiliar(slea, c, c.getPlayer());
                break;
            case RENAME_FAMILIAR:
                MobHandler.RenameFamiliar(slea, c, c.getPlayer());
                break;
            case MOVE_FAMILIAR:
                MobHandler.MoveFamiliar(slea, c, c.getPlayer());
                break;
            case ATTACK_FAMILIAR:
                MobHandler.AttackFamiliar(slea, c, c.getPlayer());
                break;
            case TOUCH_FAMILIAR:
                MobHandler.TouchFamiliar(slea, c, c.getPlayer());
                break;
            case USE_RECIPE:
                ItemMakerHandler.UseRecipe(slea, c, c.getPlayer());
                break;
            case USE_NEBULITE:
                InventoryHandler.UseNebulite(slea, c, c.getPlayer());
                break;
            case MOVE_ANDROID:
                PlayerHandler.MoveAndroid(slea, c, c.getPlayer());
                break;
            case FACE_EXPRESSION:
                PlayerHandler.ChangeEmotion(slea.readInt(), c.getPlayer());
                break;
            case FACE_ANDROID:
                PlayerHandler.ChangeAndroidEmotion(slea.readInt(), c.getPlayer());
                break;
            case TAKE_DAMAGE:
                handling.channel.handler.TakeDamageHandler.TakeDamage(slea, c, c.getPlayer());
                break;
            case HEAL_OVER_TIME:
                PlayerHandler.Heal(slea, c.getPlayer());
                break;
            case CANCEL_BUFF:
                PlayerHandler.CancelBuffHandler(slea.readInt(), c.getPlayer());
                break;
            case MECH_CANCEL:
                PlayerHandler.CancelMech(slea, c.getPlayer());
                break;
            case USE_HOLY_FOUNTAIN:
                PlayersHandler.UseHolyFountain(slea, c, c.getPlayer());
                break;
            case CANCEL_ITEM_EFFECT:
                PlayerHandler.CancelItemEffect(slea.readInt(), c.getPlayer());
                break;
            case USE_CHAIR:
                PlayerHandler.UseChair(slea.readInt(), c, c.getPlayer());
                break;
            case CANCEL_CHAIR:
                PlayerHandler.CancelChair(slea.readShort(), c, c.getPlayer());
                break;
            case USE_ITEM_EFFECT:
                PlayerHandler.UseItemEffect(slea.readInt(), c, c.getPlayer());
                break;
            case USE_TITLE_EFFECT:
                PlayerHandler.UseTitleEffect(slea.readInt(), c, c.getPlayer());
                break;
            case SKILL_EFFECT:
                PlayerHandler.SkillEffect(slea, c.getPlayer());
                break;
            case QUICK_SLOT:
                PlayerHandler.QuickSlot(slea, c.getPlayer());
                break;
            case MESO_DROP:
                c.getPlayer().updateTick(slea.readInt());
                PlayerHandler.DropMeso(slea.readInt(), c.getPlayer());
                break;
            case CHANGE_KEYMAP:
                PlayerHandler.ChangeKeymap(slea, c.getPlayer());
                break;
            case CHANGE_MAP:
                if (type == ServerType.商城服务器)
                {
                    CashShopOperation.LeaveCS(slea, c, c.getPlayer());
                }
                else
                {
                    PlayerHandler.ChangeMap(slea, c, c.getPlayer());
                }
                break;
            case CHANGE_MAP_SPECIAL:
                slea.skip(1);
                PlayerHandler.ChangeMapSpecial(slea.readMapleAsciiString(), c, c.getPlayer());
                break;
            case USE_INNER_PORTAL:
                slea.skip(1);
                PlayerHandler.InnerPortal(slea, c, c.getPlayer());
                break;
            case TROCK_ADD_MAP:
                PlayerHandler.TrockAddMap(slea, c, c.getPlayer());
                break;

            case LIE_DETECTOR_SKILL:
                PlayersHandler.LieDetector(slea, c, c.getPlayer(), false);
                break;
            case LIE_DETECTOR_RESPONSE:
                PlayersHandler.LieDetectorResponse(slea, c);
                break;
            case LIE_DETECTOR_REFRESH:
                PlayersHandler.LieDetectorRefresh(slea, c);
                break;
            case ARAN_COMBO:
                PlayerHandler.AranCombo(c, c.getPlayer(), 1);
                break;
            case LOST_ARAN_COMBO:
                PlayerHandler.AranCombo(c, c.getPlayer(), -1);
                break;
            case SPECIAL_ATTACK:
                PlayerHandler.specialAttack(slea, c, c.getPlayer());
                break;
            case SKILL_MACRO:
                PlayerHandler.ChangeSkillMacro(slea, c.getPlayer());
                break;
            case GIVE_FAME:
                PlayersHandler.GiveFame(slea, c, c.getPlayer());
                break;
            case TRANSFORM_PLAYER:
                PlayersHandler.TransformPlayer(slea, c, c.getPlayer());
                break;
            case NOTE_ACTION:
                PlayersHandler.Note(slea, c.getPlayer());
                break;
            case USE_DOOR:
                PlayersHandler.UseDoor(slea, c.getPlayer());
                break;
            case USE_MECH_DOOR:
                PlayersHandler.UseMechDoor(slea, c.getPlayer());
                break;
            case DAMAGE_REACTOR:
                PlayersHandler.HitReactor(slea, c);
                break;
            case CLICK_REACTOR:
            case TOUCH_REACTOR:
                PlayersHandler.TouchReactor(slea, c);
                break;
            case CLOSE_CHALKBOARD:
                c.getPlayer().setChalkboard(null);
                break;
            case ITEM_SORT:
                InventoryHandler.ItemSort(slea, c);
                break;
            case ITEM_GATHER:
                InventoryHandler.ItemGather(slea, c);
                break;
            case ITEM_MOVE:
                InventoryHandler.ItemMove(slea, c);
                break;
            case MOVE_BAG:
                InventoryHandler.MoveBag(slea, c);
                break;
            case SWITCH_BAG:
                InventoryHandler.SwitchBag(slea, c);
                break;
            case ITEM_MAKER:
                ItemMakerHandler.ItemMaker(slea, c);
                break;
            case ITEM_PICKUP:
                InventoryHandler.Pickup_Player(slea, c, c.getPlayer());
                break;
            case USE_CASH_ITEM:
                handling.channel.handler.UseCashItemHandler.handlePacket(slea, c, c.getPlayer());
                break;
            case USE_ADDITIONAL_ITEM:
                InventoryHandler.UseAdditionalItem(slea, c, c.getPlayer());
                break;
            case USE_ITEM:
                InventoryHandler.UseItem(slea, c, c.getPlayer());
                break;
            case USE_COSMETIC:
                InventoryHandler.UseCosmetic(slea, c, c.getPlayer());
                break;
            case USE_REDUCER:
                InventoryHandler.UseReducer(slea, c, c.getPlayer());
                break;
            case USE_MAGNIFY_GLASS:
                InventoryHandler.UseMagnify(slea, c, c.getPlayer());
                break;
            case USE_SCRIPTED_NPC_ITEM:
                InventoryHandler.UseScriptedNPCItem(slea, c, c.getPlayer());
                break;
            case USE_RETURN_SCROLL:
                InventoryHandler.UseReturnScroll(slea, c, c.getPlayer());
                break;
            case USE_UPGRADE_SCROLL:
                ItemScrollHandler.handlePacket(slea, c, c.getPlayer(), false);
                break;
            case USE_FLAG_SCROLL:
                ItemScrollHandler.handlePacket(slea, c, c.getPlayer(), true);
                break;
            case USE_POTENTIAL_SCROLL:
            case USE_POTENTIAL_ADD_SCROLL:
            case USE_EQUIP_SCROLL:
                ItemScrollHandler.handlePacket(slea, c, c.getPlayer(), false);
                break;
            case USE_SUMMON_BAG:
                InventoryHandler.UseSummonBag(slea, c, c.getPlayer());
                break;
            case USE_TREASUER_CHEST:
                InventoryHandler.UseTreasureChest(slea, c, c.getPlayer());
                break;
            case USE_SKILL_BOOK:
                c.getPlayer().updateTick(slea.readInt());
                InventoryHandler.UseSkillBook((byte) slea.readShort(), slea.readInt(), c, c.getPlayer());
                break;
            case USE_SP_RESET:
                c.getPlayer().updateTick(slea.readInt());
                InventoryHandler.UseSpReset((byte) slea.readShort(), slea.readInt(), c, c.getPlayer());
                break;
            case USE_AP_RESET:
                c.getPlayer().updateTick(slea.readInt());
                InventoryHandler.UseApReset((byte) slea.readShort(), slea.readInt(), c, c.getPlayer());
                break;
            case USE_CATCH_ITEM:
                InventoryHandler.UseCatchItem(slea, c, c.getPlayer());
                break;
            case USE_MOUNT_FOOD:
                InventoryHandler.UseMountFood(slea, c, c.getPlayer());
                break;
            case REWARD_ITEM:
                InventoryHandler.UseRewardItem((byte) slea.readShort(), slea.readInt(), c, c.getPlayer());
                break;
            case BUY_CROSS_ITEM:
                InventoryHandler.BuyCrossHunterItem(slea, c, c.getPlayer());
                break;
            case HYPNOTIZE_DMG:
                MobHandler.HypnotizeDmg(slea, c.getPlayer());
                break;
            case MOB_NODE:
                MobHandler.MobNode(slea, c.getPlayer());
                break;
            case DISPLAY_NODE:
                MobHandler.DisplayNode(slea, c.getPlayer());
                break;
            case MOVE_LIFE:
                MobHandler.MoveMonster(slea, c, c.getPlayer());
                break;
            case AUTO_AGGRO:
                MobHandler.AutoAggro(slea.readInt(), c.getPlayer());
                break;
            case FRIENDLY_DAMAGE:
                MobHandler.FriendlyDamage(slea, c.getPlayer());
                break;
            case MONSTER_BOMB:
                MobHandler.MonsterBomb(slea.readInt(), c.getPlayer());
                break;
            case MOB_BOMB:
                MobHandler.MobBomb(slea, c.getPlayer());
                break;
            case NPC_SHOP:
                NPCHandler.NPCShop(slea, c, c.getPlayer());
                break;
            case NPC_TALK:
                NPCHandler.NPCTalk(slea, c, c.getPlayer());
                break;
            case NPC_TALK_MORE:
                NPCHandler.NPCMoreTalk(slea, c);
                break;
            case NPC_ACTION:
                NPCHandler.NPCAnimation(slea, c);
                break;
            case QUEST_ACTION:
                NPCHandler.QuestAction(slea, c, c.getPlayer());
                break;
            case REISSUE_MEDAL:
                PlayerHandler.ReIssueMedal(slea, c, c.getPlayer());
                break;
            case STORAGE:
                NPCHandler.Storage(slea, c, c.getPlayer());
                break;
            case GENERAL_CHAT:
                if ((c.getPlayer() != null) && (c.getPlayer().getMap() != null))
                {
                    c.getPlayer().updateTick(slea.readInt());
                    ChatHandler.GeneralChat(slea.readMapleAsciiString(), slea.readByte(), c, c.getPlayer());
                }
                break;
            case PARTYCHAT:
                ChatHandler.Others(slea, c, c.getPlayer());
                break;
            case WHISPER:
                ChatHandler.Whisper_Find(slea, c);
                break;
            case MESSENGER:
                ChatHandler.Messenger(slea, c);
                break;
            case SHOW_LOVE_RANK:
                ChatHandler.ShowLoveRank(slea, c);
                break;
            case AUTO_ASSIGN_AP:
                StatsHandling.AutoAssignAP(slea, c, c.getPlayer());
                break;
            case DISTRIBUTE_AP:
                StatsHandling.DistributeAP(slea, c, c.getPlayer());
                break;
            case DISTRIBUTE_SP:
                StatsHandling.DistributeSP(slea, c, c.getPlayer());
                break;
            case DISTRIBUTE_HYPER_SP:
                c.getPlayer().updateTick(slea.readInt());
                StatsHandling.DistributeHyperSP(slea.readInt(), c, c.getPlayer());
                break;
            case RESET_HYPER_SP:
                StatsHandling.ResetHyperSP(slea, c, c.getPlayer());
                break;
            case PLAYER_INTERACTION:
                handling.channel.handler.PlayerInteractionHandler.PlayerInteraction(slea, c, c.getPlayer());
                break;
            case GUILD_OPERATION:
                GuildHandler.Guild(slea, c);
                break;
            case DENY_GUILD_REQUEST:
                slea.skip(1);
                GuildHandler.DenyGuildRequest(slea.readMapleAsciiString(), c);
                break;
            case GUILD_APPLY:
                GuildHandler.GuildApply(slea, c);
                break;
            case ACCEPT_GUILD_APPLY:
                GuildHandler.AcceptGuildApply(slea, c);
                break;
            case DENY_GUILD_APPLY:
                GuildHandler.DenyGuildApply(slea, c);
                break;
            case ALLIANCE_OPERATION:
                handling.channel.handler.AllianceHandler.HandleAlliance(slea, c, false);
                break;
            case DENY_ALLIANCE_REQUEST:
                handling.channel.handler.AllianceHandler.HandleAlliance(slea, c, true);
                break;
            case QUICK_MOVE:
                NPCHandler.OpenQuickMoveNpc(slea, c);
                break;
            case BBS_OPERATION:
                handling.channel.handler.BBSHandler.BBSOperation(slea, c);
                break;
            case PARTY_OPERATION:
                PartyHandler.PartyOperation(slea, c);
                break;
            case DENY_PARTY_REQUEST:
                PartyHandler.DenyPartyRequest(slea, c);
                break;
            case ALLOW_PARTY_INVITE:
                PartyHandler.AllowPartyInvite(slea, c);
                break;
            case SIDEKICK_OPERATION:
                PartyHandler.SidekickOperation(slea, c);
                break;
            case DENY_SIDEKICK_REQUEST:
                PartyHandler.DenySidekickRequest(slea, c);
                break;
            case BUDDYLIST_MODIFY:
                handling.channel.handler.BuddyListHandler.BuddyOperation(slea, c);
                break;
            case CYGNUS_SUMMON:
                UserInterfaceHandler.CygnusSummon_NPCRequest(c);
                break;
            case SHIP_OBJECT:
                UserInterfaceHandler.ShipObjectRequest(slea.readInt(), c);
                break;
            case BUY_CS_ITEM:
                handling.cashshop.handler.BuyCashItemHandler.BuyCashItem(slea, c, c.getPlayer());
                break;
            case COUPON_CODE:
                handling.cashshop.handler.CouponCodeHandler.handlePacket(slea, c, c.getPlayer());
                break;
            case CS_UPDATE:
                handling.cashshop.handler.CashShopOperation.CSUpdate(c);
                break;
            case SEND_CS_GIFI:
                handling.cashshop.handler.BuyCashItemHandler.商城送礼(slea, c, c.getPlayer());
                break;
            case TOUCHING_MTS:
                handling.cashshop.handler.MTSOperation.MTSUpdate(server.MTSStorage.getInstance().getCart(c.getPlayer().getId()), c);
                break;
            case MTS_TAB:
                handling.cashshop.handler.MTSOperation.MTSOperation(slea, c);
                break;
            case USE_POT:
                ItemMakerHandler.UsePot(slea, c);
                break;
            case CLEAR_POT:
                ItemMakerHandler.ClearPot(slea, c);
                break;
            case FEED_POT:
                ItemMakerHandler.FeedPot(slea, c);
                break;
            case CURE_POT:
                ItemMakerHandler.CurePot(slea, c);
                break;
            case REWARD_POT:
                ItemMakerHandler.RewardPot(slea, c);
                break;
            case DAMAGE_SUMMON:
                SummonHandler.DamageSummon(slea, c.getPlayer());
                break;
            case MOVE_SUMMON:
                SummonHandler.MoveSummon(slea, c.getPlayer());
                break;
            case SUMMON_ATTACK:
                SummonHandler.SummonAttack(slea, c, c.getPlayer());
                break;
            case MOVE_DRAGON:
                SummonHandler.MoveDragon(slea, c.getPlayer());
                break;
            case DRAGON_FLY:
                SummonHandler.DragonFly(slea, c.getPlayer());
                break;
            case SUB_SUMMON:
                SummonHandler.SubSummon(slea, c.getPlayer());
                break;
            case REMOVE_SUMMON:
                SummonHandler.RemoveSummon(slea, c);
                break;
            case SPAWN_PET:
                PetHandler.SpawnPet(slea, c, c.getPlayer());
                break;
            case PET_AUTO_BUFF:
                PetHandler.Pet_AutoBuff(slea, c, c.getPlayer());
                break;
            case MOVE_PET:
                PetHandler.MovePet(slea, c.getPlayer());
                break;
            case PET_CHAT:
                PetHandler.PetChat(slea, c, c.getPlayer());
                break;
            case PET_COMMAND:
                PetHandler.PetCommand(slea, c, c.getPlayer());
                break;
            case PET_FOOD:
                PetHandler.PetFood(slea, c, c.getPlayer());
                break;
            case PET_LOOT:
                InventoryHandler.Pickup_Pet(slea, c, c.getPlayer());
                break;
            case PET_AUTO_POT:
                PetHandler.Pet_AutoPotion(slea, c, c.getPlayer());
                break;
            case PET_EXCEPTION_LIST:
                PetHandler.PetExcludeItems(slea, c, c.getPlayer());
                break;
            case PET_AOTO_EAT:
                slea.skip(4);
                PetHandler.PetFood(slea, c, c.getPlayer());
                break;
            case ALLOW_PET_LOOT:
                PetHandler.AllowPetLoot(slea, c, c.getPlayer());
                break;
            case ALLOW_PET_AOTO_EAT:
                PetHandler.AllowPetAutoEat(slea, c, c.getPlayer());
                break;
            case MONSTER_CARNIVAL:
                handling.channel.handler.MonsterCarnivalHandler.MonsterCarnival(slea, c);
                break;
            case DUEY_ACTION:
                handling.channel.handler.DueyHandler.DueyOperation(slea, c);
                break;
            case USE_HIRED_MERCHANT:
                HiredMerchantHandler.UseHiredMerchant(c, true);
                break;
            case MERCH_ITEM_STORE:
                HiredMerchantHandler.MerchantItemStore(slea, c);
                break;

            case LEFT_KNOCK_BACK:
                PlayerHandler.leftKnockBack(slea, c);
                break;
            case SNOWBALL:
                PlayerHandler.snowBall(slea, c);
                break;
            case COCONUT:
                PlayersHandler.hitCoconut(slea, c);
                break;
            case REPAIR:
                NPCHandler.repair(slea, c);
                break;
            case REPAIR_ALL:
                NPCHandler.repairAll(c);
                break;
            case GAME_POLL:
                UserInterfaceHandler.InGame_Poll(slea, c);
                break;
            case OWL:
                InventoryHandler.Owl(slea, c);
                break;
            case OWL_WARP:
                InventoryHandler.OwlWarp(slea, c);
                break;
            case USE_OWL_MINERVA:
                InventoryHandler.OwlMinerva(slea, c);
                break;
            case RPS_GAME:
                NPCHandler.RPSGame(slea, c);
                break;
            case UPDATE_QUEST:
                NPCHandler.UpdateQuest(slea, c);
                break;
            case USE_ITEM_QUEST:
                NPCHandler.UseItemQuest(slea, c);
                break;
            case FOLLOW_REQUEST:
                PlayersHandler.FollowRequest(slea, c);
                break;
            case AUTO_FOLLOW_REPLY:
            case FOLLOW_REPLY:
                PlayersHandler.FollowReply(slea, c);
                break;
            case RING_ACTION:
                PlayersHandler.RingAction(slea, c);
                break;
            case REQUEST_FAMILY:
                FamilyHandler.RequestFamily(slea, c);
                break;
            case OPEN_FAMILY:
                FamilyHandler.OpenFamily(slea, c);
                break;
            case FAMILY_OPERATION:
                FamilyHandler.FamilyOperation(slea, c);
                break;
            case DELETE_JUNIOR:
                FamilyHandler.DeleteJunior(slea, c);
                break;
            case DELETE_SENIOR:
                FamilyHandler.DeleteSenior(slea, c);
                break;
            case USE_FAMILY:
                FamilyHandler.UseFamily(slea, c);
                break;
            case FAMILY_PRECEPT:
                FamilyHandler.FamilyPrecept(slea, c);
                break;
            case FAMILY_SUMMON:
                FamilyHandler.FamilySummon(slea, c);
                break;
            case ACCEPT_FAMILY:
                FamilyHandler.AcceptFamily(slea, c);
                break;
            case SOLOMON:
                PlayersHandler.Solomon(slea, c);
                break;
            case GACH_EXP:
                PlayersHandler.GachExp(slea, c);
                break;
            case PARTY_SEARCH_START:
                PartyHandler.MemberSearch(slea, c);
                break;
            case PARTY_SEARCH_STOP:
                PartyHandler.PartySearch(slea, c);
                break;
            case EXPEDITION_LISTING:
                PartyHandler.PartyListing(slea, c);
                break;
            case EXPEDITION_OPERATION:
                PartyHandler.Expedition(slea, c);
                break;
            case USE_TELE_ROCK:
                InventoryHandler.TeleRock(slea, c);
                break;
            case PAM_SONG:
                InventoryHandler.PamSong(slea, c);
                break;
            case REPORT:
                PlayersHandler.Report(slea, c);
                break;
            case REMOTE_STORE:
                HiredMerchantHandler.RemoteStore(slea, c);
                break;
            case SHIKONGJUAN:
                PlayerHandler.超时空卷(slea, c, c.getPlayer());
                break;
            case PLAYER_UPDATE:
                PlayerHandler.PlayerUpdate(c, c.getPlayer());
                break;
            case CHANGE_MARKET_MAP:
                PlayerHandler.ChangeMarketMap(slea, c, c.getPlayer());
                break;
            case TEACH_SKILL:
                PlayerHandler.TeachSkill(slea, c, c.getPlayer());
                break;
            case SET_CHAR_CASH:
                PlayerHandler.showPlayerCash(slea, c);
                break;
            case USE_HAMMER:
                handling.channel.handler.UseHammerHandler.UseHammer(slea, c);
                break;
            case HAMMER_RESPONSE:
                handling.channel.handler.UseHammerHandler.HammerResponse(slea, c);
                break;
            case MEMORY_SKILL_CHOOSE:
                PhantomMemorySkill.MemorySkillChoose(slea, c);
                break;
            case MEMORY_SKILL_CHANGE:
                PhantomMemorySkill.MemorySkillChange(slea, c);
                break;
            case MEMORY_SKILL_OBTAIN:
                PhantomMemorySkill.MemorySkillObtain(slea, c);
                break;
            case OPEN_ROOT_NPC:
                NPCScriptManager.getInstance().dispose(c);
                NPCScriptManager.getInstance().start(c, 1064026, 1);
                break;
            case POINT_POWER:
                if (c.getPlayer() != null)
                {
                    c.getPlayer().startPower();
                }
                break;
            case USE_TEMPEST_BLADES:
                PlayerHandler.useTempestBlades(slea, c, c.getPlayer());
                break;


            case UNKNOWN_168:
                c.getSession().write(tools.MaplePacketCreator.sendUnkPacket1FC());
                break;
            case QUICK_BUY_CS_ITEM:
                PlayerHandler.quickBuyCashShopItem(slea, c, c.getPlayer());
                break;
            case SYSTEM_PROCESS_LIST:
                handling.channel.handler.SystemProcess.SystemProcess(slea, c, c.getPlayer());
                break;
            case POTION_POT_USE:
                PotionPotHandler.PotionPotUse(slea, c, c.getPlayer());
                break;
            case POTION_POT_ADD:
                PotionPotHandler.PotionPotAdd(slea, c, c.getPlayer());
                break;
            case POTION_POT_MODE:
                PotionPotHandler.PotionPotMode(slea, c, c.getPlayer());
                break;
            case POTION_POT_INCR:
                PotionPotHandler.PotionPotIncr(slea, c, c.getPlayer());
                break;
            case CHANGE_ZERO_LOOK:
                PlayerHandler.changeZeroLook(slea, c, c.getPlayer(), false);
                break;
            case CHANGE_ZERO_LOOK_END:
                PlayerHandler.changeZeroLook(slea, c, c.getPlayer(), true);
                break;
            default:
                System.out.println("[未处理封包] Recv " + header.toString() + " [" + tools.HexTool.getOpcodeToString(header.getValue()) + "]");
        }
    }

    private String lookupRecv(short header)
    {
        for (RecvPacketOpcode recv : RecvPacketOpcode.values())
        {
            if (recv.getValue() == header)
            {
                return recv.name();
            }
        }
        return "UNKNOWN";
    }
}