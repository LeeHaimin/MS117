package handling.cashshop;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.buffer.SimpleBufferAllocator;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.net.InetSocketAddress;

import client.MapleCharacter;
import constants.ServerConstants;
import handling.MapleServerHandler;
import handling.ServerType;
import handling.channel.PlayerStorage;
import handling.mina.MapleCodecFactory;
import server.ServerProperties;

public class CashShopServer
{
    private static final short DEFAULT_PORT = 8600;
    private static final Logger log = Logger.getLogger(CashShopServer.class);
    private static String ip;
    private static IoAcceptor acceptor;
    private static PlayerStorage players;
    private static boolean finishedShutdown = false;
    private static short port;
    private static int autoPaoDian;

    public static void run_startup_configurations()
    {
        autoPaoDian = Integer.parseInt(ServerProperties.getProperty("world.autoPaoDian", "1"));
        port = Short.parseShort(ServerProperties.getProperty("cashshop.port", String.valueOf(8600)));
        ip = ServerProperties.getProperty("world.host", ServerConstants.HOST) + ":" + port;

        IoBuffer.setUseDirectBuffer(false);
        IoBuffer.setAllocator(new SimpleBufferAllocator());

        acceptor = new NioSocketAcceptor();
        acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new MapleCodecFactory()));
        acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 30);
        players = new PlayerStorage(-10);
        try
        {
            acceptor.setHandler(new MapleServerHandler(ServerType.商城服务器));
            acceptor.bind(new InetSocketAddress(port));
            ((SocketSessionConfig) acceptor.getSessionConfig()).setTcpNoDelay(true);
            log.info("商城服务器绑定端口: " + port + ".");
        }
        catch (Exception e)
        {
            log.error("商城服务器绑定端口 " + port + " 失败");
            throw new RuntimeException("绑定端口失败.", e);
        }
    }

    public static String getIP()
    {
        return ip;
    }

    public static int getConnectedClients()
    {
        return getPlayerStorage().getConnectedClients();
    }

    public static PlayerStorage getPlayerStorage()
    {
        return players;
    }

    public static void shutdown()
    {
        if (finishedShutdown)
        {
            return;
        }
        log.info("正在关闭商城服务器...");
        players.disconnectAll();
        log.info("商城服务器解除端口绑定...");
        acceptor.unbind();
        finishedShutdown = true;
    }

    public static boolean isShutdown()
    {
        return finishedShutdown;
    }

    public static String getCashBlockedMsg(int itemId)
    {
        switch (itemId)
        {
            case 5060003:
                return "该道具只能在游戏中获得.";
        }
        return "该道具禁止购买.";
    }

    public static void AutoPaoDian()
    {
        for (MapleCharacter chr : players.getAllCharacters())
        {
            if (chr != null)
            {
                chr.setBossLog("在线泡点", 0, getAutoPaoDian());
            }
        }
    }

    public static int getAutoPaoDian()
    {
        return autoPaoDian;
    }

    public static void setAutoPaoDian(int rate)
    {
        autoPaoDian = rate;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\cashshop\CashShopServer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */