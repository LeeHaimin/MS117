package handling.cashshop.handler;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleCharacterUtil;
import client.MapleClient;
import client.MapleQuestStatus;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryIdentifier;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePotionPot;
import constants.ItemConstants;
import handling.channel.ChannelServer;
import handling.world.WorldFindService;
import server.AutobanManager;
import server.MapleItemInformationProvider;
import server.cashshop.CashItemFactory;
import server.cashshop.CashItemInfo;
import server.cashshop.CashShop;
import server.quest.MapleQuest;
import tools.FileoutputUtil;
import tools.StringUtil;
import tools.Triple;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.MTSCSPacket;

public class BuyCashItemHandler
{
    private static final Logger log = Logger.getLogger(BuyCashItemHandler.class);

    public static void BuyCashItem(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        byte action = slea.readByte();
        CashShop cs = chr.getCashInventory();
        CashItemFactory cashinfo = CashItemFactory.getInstance();
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();

        int err;
        CashItemInfo cItem;
        int toCharge;
        Item item;
        boolean coupon;
        int iCoupon;
        switch (action)
        {
            case 3:
                toCharge = slea.readByte() + 1;
                slea.skip(1);
                int snCS = slea.readInt();

                cItem = cashinfo.getItem(snCS);
                if (chr.isShowPacket())
                {
                    System.out.println("商城 => 购买 - 物品 " + snCS + " 是否为空 " + (cItem == null));
                }
                if (cItem != null)
                {
                    if (snCS == 92000046)
                    {
                        AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买道具.");
                        return;
                    }
                    if ((cItem.getId() / 1000 == 5533) && (!cashinfo.hasRandomItem(cItem.getId())))
                    {
                        chr.dropMessage(1, "该道具暂时无法购买.");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (cItem.getId() == 5451001)
                    {
                        chr.dropMessage(1, "该道具禁止购买.");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (cItem.getId() == 5532000)
                    {
                        chr.dropMessage(1, "极光戒指只能通过游戏里获获取.");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (cItem.getId() == 5532001)
                    {
                        chr.dropMessage(1, "极光戒指只能通过游戏里获获取.");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (cItem.getId() == 5532002)
                    {
                        chr.dropMessage(1, "极光戒指只能通过游戏里获获取.");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (chr.getCSPoints(toCharge) < cItem.getPrice())
                    {
                        chr.dropMessage(1, "点卷余额不足");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (!cItem.genderEquals(chr.getGender()))
                    {
                        chr.dropMessage(1, "请确认角色名是否错误");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (cs.getItemsSize() >= 100)
                    {
                        chr.dropMessage(1, "保管箱已满");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if ((cashinfo.isBlockedCashItemId(cItem.getId())) || (cashinfo.isBlockCashSnId(snCS)))
                    {
                        chr.dropMessage(1, "该道具禁止购买.");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    if (cItem.getPrice() <= 0)
                    {
                        AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买道具.");
                        return;
                    }
                    if (chr.isShowPacket())
                    {
                        System.out.println("商城 => 购买 - 点卷类型 " + toCharge + " 减少 " + cItem.getPrice());
                    }
                    item = cs.toItem(cItem);

                    if ((item != null) && (item.getUniqueId() > 0) && (item.getItemId() == cItem.getId()) && (item.getQuantity() == cItem.getCount()))
                    {
                        chr.modifyCSPoints(toCharge, -cItem.getPrice(), false);
                        if (ii.isCash(item.getItemId()))
                        {
                            cs.addToInventory(item);
                            c.getSession().write(MTSCSPacket.购买商城道具(item, cItem.getSN(), c.getAccID()));
                            addCashshopLog(chr, cItem.getSN(), cItem.getId(), toCharge, cItem.getPrice(), cItem.getCount(), chr.getName() + " 购买道具: " + ii.getName(cItem.getId()));
                            if (item.getItemId() == 5820000)
                            {
                                if (chr.getPotionPot() == null)
                                {
                                    MaplePotionPot pot = MaplePotionPot.createPotionPot(chr.getId(), item.getItemId(), item.getExpiration());
                                    if (pot == null)
                                    {
                                        chr.dropMessage(1, "创建1个新的药剂罐出现错误.");
                                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                                        return;
                                    }
                                    chr.setPotionPot(pot);
                                }
                                c.getSession().write(MTSCSPacket.updataPotionPot(chr.getPotionPot()));
                            }
                        }
                        else
                        {
                            log.info("[作弊] " + chr.getName() + " 商城非法购买道具.道具: " + item.getItemId() + " - " + ii.getName(item.getItemId()));
                            AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买道具.");
                        }
                    }
                    else
                    {
                        chr.dropMessage(1, "购买道具出错 代码（2）");
                    }
                }
                else
                {
                    chr.dropMessage(1, "购买道具出错 代码（1）");
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;

            case 4:
                chr.dropMessage(1, "暂不支持，直接选了点送礼吧！");
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;


            case 5:
                chr.clearWishlist();
                if (slea.available() < 40L)
                {
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                int[] wishlist = new int[12];
                for (int i = 0; i < 12; i++)
                {
                    wishlist[i] = slea.readInt();
                }
                chr.setWishlist(wishlist);
                c.getSession().write(MTSCSPacket.商城购物车(chr, true));
                break;
            case 6:
                toCharge = slea.readByte() + 1;
                coupon = slea.readByte() > 0;
                if (coupon)
                {
                    snCS = slea.readInt();
                    cItem = cashinfo.getItem(snCS);
                    if (cItem == null)
                    {
                        chr.dropMessage(1, "未知错误");
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        break;
                    }
                    int types = (cItem.getId() - 9110000) / 1000;
                    MapleInventoryType type = MapleInventoryType.getByType((byte) types);
                    if (chr.isShowPacket())
                    {
                        System.out.println("增加道具栏  snCS " + snCS + " 扩充: " + types);
                    }
                    if ((chr.getCSPoints(toCharge) >= 1100) && (chr.getInventory(type).getSlotLimit() < 89))
                    {
                        chr.modifyCSPoints(toCharge, 64436, false);
                        chr.getInventory(type).addSlot((byte) 8);

                        c.getSession().write(MTSCSPacket.扩充道具栏(type.getType(), chr.getInventory(type).getSlotLimit()));
                    }
                    else
                    {
                        chr.dropMessage(1, "扩充失败，点卷余额不足或者栏位已超过上限。");
                    }
                }
                else
                {
                    MapleInventoryType type = MapleInventoryType.getByType(slea.readByte());
                    if ((chr.getCSPoints(toCharge) >= 600) && (chr.getInventory(type).getSlotLimit() < 93))
                    {
                        chr.modifyCSPoints(toCharge, 64936, false);
                        chr.getInventory(type).addSlot((byte) 4);

                        c.getSession().write(MTSCSPacket.扩充道具栏(type.getType(), chr.getInventory(type).getSlotLimit()));
                    }
                    else
                    {
                        chr.dropMessage(1, "扩充失败，点卷余额不足或者栏位已超过上限。");
                    }
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;


            case 7:
                toCharge = slea.readByte() + 1;
                iCoupon = slea.readByte() > 0 ? 2 : 1;
                if (chr.getCSPoints(toCharge) >= (iCoupon == 2 ? 1100 : 600)) if (chr.getStorage().getSlots() < 97 - 4 * iCoupon)
                {
                    chr.modifyCSPoints(toCharge, iCoupon == 2 ? 64436 : 64936, false);
                    chr.getStorage().increaseSlots((byte) (4 * iCoupon));
                    chr.getStorage().saveToDB();

                    c.getSession().write(MTSCSPacket.扩充仓库(chr.getStorage().getSlots()));
                    break;
                }
                chr.dropMessage(1, "仓库扩充失败，点卷余额不足或者栏位已超过上限 96 个位置。");

                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;


            case 8:
                toCharge = slea.readByte() + 1;
                snCS = slea.readInt();
                CashItemInfo cashItem = cashinfo.getItem(snCS, false);
                int slots = c.getAccCharSlots();
                if ((cashItem == null) || (cashItem.getId() != 5430000))
                {
                    String msg = "角色栏扩充失败，找不到指定的道具信息或者道具ID不正确。";
                    if (chr.isAdmin())
                    {
                        msg = "角色栏扩充失败:\r\n道具ID是否正确: " + (cashItem.getId() == 5430000) + " 当前ID：" + cashItem.getId();
                    }
                    chr.dropMessage(1, msg);
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if ((chr.getCSPoints(toCharge) < cashItem.getPrice()) || (slots > 21))
                {
                    chr.dropMessage(1, "角色栏扩充失败，点卷余额不足或者栏位已超过上限。");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (c.gainAccCharSlot())
                {
                    chr.modifyCSPoints(toCharge, -cashItem.getPrice(), false);
                    chr.dropMessage(1, "角色栏扩充成功，当前栏位: " + (slots + 1));
                }
                else
                {
                    chr.dropMessage(1, "角色栏扩充失败。");
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;


            case 9:
                toCharge = slea.readByte() + 1;
                int sn = slea.readInt();
                cashItem = cashinfo.getItem(sn);
                chr.dropMessage(1, "暂时不支持。");
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;
            case 11:
                toCharge = slea.readByte() + 1;
                sn = slea.readInt();
                cashItem = cashinfo.getItem(sn);
                if ((cashItem == null) || (chr.getCSPoints(toCharge) < cashItem.getPrice() * 10) || (cashItem.getId() / 10000 != 555))
                {
                    chr.dropMessage(1, "项链扩充失败，点卷余额不足或者出现其他错误。");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                MapleQuestStatus marr = chr.getQuestNoAdd(MapleQuest.getInstance(122700));
                if ((marr != null) && (marr.getCustomData() != null) && (Long.parseLong(marr.getCustomData()) >= System.currentTimeMillis()))
                {
                    chr.dropMessage(1, "项链扩充失败，您已经进行过项链扩充。");
                }
                else
                {
                    long days = 0L;
                    if (cashItem.getId() == 5550000)
                    {
                        days = 30L;
                    }
                    else if (cashItem.getId() == 5550001)
                    {
                        days = 7L;
                    }
                    String customData = String.valueOf(System.currentTimeMillis() + days * 24L * 60L * 60L * 1000L);
                    chr.getQuestNAdd(MapleQuest.getInstance(122700)).setCustomData(customData);
                    chr.modifyCSPoints(toCharge, -cashItem.getPrice() * 1, false);
                    chr.dropMessage(1, "项链扩充成功，本次扩充花费:\r\n" + (toCharge == 1 ? "点卷" : "抵用卷") + cashItem.getPrice() * 1 + " 点，持续时间为: " + days + " 天。");
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;

            case 14:
                int uniqueId = (int) slea.readLong();
                item = cs.findByCashId(uniqueId);
                if (item == null)
                {
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    if (chr.isShowPacket())
                    {
                        System.out.println("删除商城道具 - 道具为空 删除失败");
                    }
                    return;
                }
                cs.removeFromInventory(item);
                c.getSession().write(MTSCSPacket.商城删除道具(uniqueId));
                break;

            case 15:
                item = cs.findByCashId((int) slea.readLong());
                if (chr.isShowPacket())
                {
                    System.out.println("商城 => 背包 - 道具是否为空 " + (item == null));
                }
                if (item == null)
                {
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (chr.getInventory(ItemConstants.getInventoryType(item.getItemId())).addItem(item) != -1)
                {
                    cs.removeFromInventory(item);
                    c.getSession().write(MTSCSPacket.商城到背包(item));
                    if (chr.isShowPacket())
                    {
                        System.out.println("商城 => 背包 - 移动成功");
                    }
                }

                break;
            case 16:
                int cashId = (int) slea.readLong();
                byte type = slea.readByte();
                MapleInventory mi = chr.getInventory(MapleInventoryType.getByType(type));
                item = mi.findByUniqueId(cashId);
                if (chr.isShowPacket())
                {
                    System.out.println("背包 => 商城 - 道具是否为空 " + (item == null));
                }
                if (item == null)
                {
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (cs.getItemsSize() < 100)
                {
                    snCS = cashinfo.getSnFromId(item.getItemId());
                    cs.addToInventory(item);
                    mi.removeSlot(item.getPosition());
                    c.getSession().write(MTSCSPacket.背包到商城(item, c.getAccID(), snCS));
                    if (chr.isShowPacket())
                    {
                        System.out.println("背包 => 商城 - 移动成功");
                    }
                }
                else
                {
                    chr.dropMessage(1, "移动失败。");
                }
                break;


            case 32:
                slea.readMapleAsciiString();
                toCharge = 2;
                uniqueId = (int) slea.readLong();
                item = cs.findByCashId(uniqueId);
                if (item == null)
                {
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                snCS = cashinfo.getSnFromId(item.getItemId());
                cItem = cashinfo.getItem(snCS);
                if ((cItem == null) || (cashinfo.isBlockRefundableItemId(item.getItemId())))
                {
                    if (chr.isAdmin())
                    {
                        if (cItem == null)
                        {
                            chr.dropMessage(1, "换购失败:\r\n道具是否为空: " + (cItem == null));
                        }
                        else
                        {
                            chr.dropMessage(1, "换购失败:\r\n道具禁止回购: " + cashinfo.isBlockRefundableItemId(item.getItemId()));
                        }
                    }
                    else
                    {
                        chr.dropMessage(1, "换购失败，当前道具不支持换购。");
                    }
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (!ii.isCash(cItem.getId()))
                {
                    AutobanManager.getInstance().autoban(chr.getClient(), "商城非法换购道具.");
                    return;
                }
                int Money = cItem.getPrice() / 10 * 3;
                cs.removeFromInventory(item);
                chr.modifyCSPoints(toCharge, Money, false);
                c.getSession().write(MTSCSPacket.商城换购道具(uniqueId, Money));
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;

            case 35:
            case 42:
                slea.readMapleAsciiString();
                slea.skip(1);
                toCharge = 1;
                snCS = slea.readInt();
                cItem = cashinfo.getItem(snCS);
                slea.skip(4);
                String partnerName = slea.readMapleAsciiString();
                String msg = slea.readMapleAsciiString();
                if ((cItem == null) || (!ItemConstants.isEffectRing(cItem.getId())) || (chr.getCSPoints(toCharge) < cItem.getPrice()) || (msg.length() > 73) || (msg.length() < 1))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(0));
                    return;
                }
                if (!cItem.genderEquals(chr.getGender()))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(7));
                    return;
                }
                if (chr.getCashInventory().getItemsSize() >= 100)
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(24));
                    return;
                }
                if (!ii.isCash(cItem.getId()))
                {
                    AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买戒指道具.");
                    return;
                }
                if ((cashinfo.isBlockedCashItemId(cItem.getId())) || (cashinfo.isBlockCashSnId(snCS)))
                {
                    chr.dropMessage(1, "该道具禁止购买.");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                Triple<Integer, Integer, Integer> info = MapleCharacterUtil.getInfoByName(partnerName, chr.getWorld());
                if ((info == null) || (info.getLeft() <= 0))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(7));
                }
                else if ((info.getMid() == c.getAccID()) || (info.getLeft() == chr.getId()))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(6));
                }
                else
                {
                    if ((info.getRight() == chr.getGender()) && (action == 35))
                    {
                        c.getSession().write(MTSCSPacket.商城错误提示(26));
                        return;
                    }
                    err = client.inventory.MapleRing.createRing(cItem.getId(), chr, partnerName, msg, info.getLeft(), cItem.getSN());
                    if (err != 1)
                    {
                        c.getSession().write(MTSCSPacket.商城错误提示(1));
                        return;
                    }
                    chr.modifyCSPoints(toCharge, -cItem.getPrice(), false);
                    c.getSession().write(MTSCSPacket.商城送礼(cItem.getId(), cItem.getCount(), partnerName));
                    addCashshopLog(chr, cItem.getSN(), cItem.getId(), toCharge, cItem.getPrice(), cItem.getCount(), chr.getName() + " 购买戒指: " + ii.getName(cItem.getId()) + " 送给 " + partnerName);
                    chr.sendNote(partnerName, partnerName + " 您已收到" + chr.getName() + "送给您的礼物，请进入现金商城查看！");
                    int chz = WorldFindService.getInstance().findChannel(partnerName);
                    if (chz > 0)
                    {
                        MapleCharacter receiver = ChannelServer.getInstance(chz).getPlayerStorage().getCharacterByName(partnerName);
                        if (receiver != null)
                        {
                            receiver.showNote();
                        }
                    }
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;


            case 37:
                toCharge = slea.readByte() + 1;
                int snCsId = slea.readInt();
                int count = slea.readInt();
                if ((snCsId == 10200551) || (snCsId == 10200552) || (snCsId == 10200553))
                {
                    chr.dropMessage(1, "当前服务器未开放购买商城活动栏里面的道具.");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (cashinfo.isBlockCashSnId(snCsId))
                {
                    chr.dropMessage(1, "该礼包禁止购买.");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                cItem = cashinfo.getItem(snCsId);
                List<Integer> packageIds = null;
                if (cItem != null)
                {
                    packageIds = cashinfo.getPackageItems(cItem.getId());
                }
                if ((cItem == null) || (packageIds == null))
                {
                    msg = "未知错误";
                    if (chr.isAdmin())
                    {
                        if (cItem == null)
                        {
                            msg = msg + "\r\n\r\n 礼包道具信息为空";
                        }
                        if (packageIds == null)
                        {
                            msg = msg + "\r\n\r\n 礼包道具里面的物品道具为空";
                        }
                    }
                    chr.dropMessage(1, msg);
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (chr.getCSPoints(toCharge) < cItem.getPrice())
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(3));
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (!cItem.genderEquals(c.getPlayer().getGender()))
                {
                    chr.dropMessage(1, "性别不符合");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (c.getPlayer().getCashInventory().getItemsSize() >= 100 - packageIds.size())
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(24));
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (cItem.getPrice() <= 0)
                {
                    AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买礼包道具.");
                    return;
                }
                chr.modifyCSPoints(toCharge, -cItem.getPrice(), false);
                Map<Integer, Item> packageItems = new HashMap<>();
                for (Integer packageId : packageIds)
                {
                    int i = packageId;
                    CashItemInfo cii = cashinfo.getSimpleItem(i);
                    if (cii != null)
                    {

                        Item itemz = chr.getCashInventory().toItem(cii);
                        if ((itemz != null) && (itemz.getUniqueId() > 0) &&


                                (!cashinfo.isBlockedCashItemId(cItem.getId())))
                        {

                            if (!ii.isCash(itemz.getItemId()))
                            {
                                log.info("[作弊] " + chr.getName() + " 商城非法购买礼包道具.道具: " + itemz.getItemId() + " - " + ii.getName(itemz.getItemId()));
                                AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买礼包道具.");
                            }
                            else
                            {
                                packageItems.put(i, itemz);
                                chr.getCashInventory().addToInventory(itemz);
                                addCashshopLog(chr, snCsId, itemz.getItemId(), toCharge, cItem.getPrice(), itemz.getQuantity(), chr.getName() + " 购买礼包: " + ii.getName(itemz.getItemId()) + " - " + i);
                            }
                        }
                    }
                }
                c.getSession().write(MTSCSPacket.商城购买礼包(packageItems, c.getAccID()));
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;

            case 38:
                slea.readMapleAsciiString();
                snCsId = slea.readInt();
                cItem = cashinfo.getItem(snCsId);
                partnerName = slea.readMapleAsciiString();
                msg = slea.readMapleAsciiString();
                if ((cItem == null) || (chr.getCSPoints(1) < cItem.getPrice()) || (msg.length() > 73) || (msg.length() < 1))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(3));
                    return;
                }
                if (cashinfo.isBlockCashSnId(snCsId))
                {
                    chr.dropMessage(1, "该礼包禁止购买.");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                Triple<Integer, Integer, Integer> info1 = MapleCharacterUtil.getInfoByName(partnerName, chr.getWorld());
                if ((info1 == null) || (info1.getLeft() <= 0))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(7));
                }
                else if ((info1.getLeft() == chr.getId()) || (info1.getMid() == c.getAccID()))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(6));
                }
                else if (!cItem.genderEquals(info1.getRight()))
                {
                    c.getSession().write(MTSCSPacket.商城错误提示(8));
                }
                else
                {
                    if (cItem.getPrice() <= 0)
                    {
                        AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买礼包道具.");
                        return;
                    }
                    chr.getCashInventory().gift(info1.getLeft(), chr.getName(), msg, cItem.getSN(), MapleInventoryIdentifier.getInstance());
                    chr.modifyCSPoints(1, -cItem.getPrice(), false);

                    c.getSession().write(MTSCSPacket.商城送礼包(cItem.getId(), cItem.getCount(), partnerName));
                    chr.sendNote(partnerName, partnerName + " 您已收到" + chr.getName() + "送给您的礼物，请进入现金商城查看！");
                    addCashshopLog(chr, cItem.getSN(), cItem.getId(), 1, cItem.getPrice(), cItem.getCount(), chr.getName() + " 赠送礼包给 " + partnerName);
                    int chz = WorldFindService.getInstance().findChannel(partnerName);
                    if (chz > 0)
                    {
                        MapleCharacter receiver = ChannelServer.getInstance(chz).getPlayerStorage().getCharacterByName(partnerName);
                        if (receiver != null)
                        {
                            receiver.showNote();
                        }
                    }
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;

            case 39:
                cItem = cashinfo.getItem(slea.readInt());
                if ((cItem == null) || (!MapleItemInformationProvider.getInstance().isQuestItem(cItem.getId())))
                {
                    chr.dropMessage(1, "该道具不是任务物品");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if ((chr.getMeso() < cItem.getPrice()) || (cItem.getPrice() <= 0))
                {
                    chr.dropMessage(1, "金币不足");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (chr.getItemQuantity(cItem.getId()) > 0)
                {
                    chr.dropMessage(1, "你已经有这个道具\r\n不能购买.");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (chr.getInventory(ItemConstants.getInventoryType(cItem.getId())).getNextFreeSlot() < 0)
                {
                    chr.dropMessage(1, "背包空间不足");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if (cashinfo.isBlockedCashItemId(cItem.getId()))
                {
                    chr.dropMessage(1, handling.cashshop.CashShopServer.getCashBlockedMsg(cItem.getId()));
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                if ((cItem.getId() == 4031063) || (cItem.getId() == 4031191) || (cItem.getId() == 4031192))
                {
                    byte pos = server.MapleInventoryManipulator.addId(c, cItem.getId(), (short) cItem.getCount(), null, "商城: 任务物品 在 " + FileoutputUtil.CurrentReadable_Date());
                    if (pos < 0)
                    {
                        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                        return;
                    }
                    chr.gainMeso(-cItem.getPrice(), false);
                    c.getSession().write(MTSCSPacket.updataMeso(chr));
                    c.getSession().write(MTSCSPacket.商城购买任务道具(cItem.getPrice(), (short) cItem.getCount(), pos, cItem.getId()));
                }
                else
                {
                    AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买任务道具.");
                }
                break;

            case 50:
                slea.readByte();
                snCS = slea.readInt();
                slea.readInt();
                if ((snCS == 50200031) && (chr.getCSPoints(1) >= 500))
                {
                    chr.modifyCSPoints(1, 65036, false);
                    chr.modifyCSPoints(2, 500, false);
                    chr.dropMessage(1, "兑换抵用卷成功");
                }
                else if ((snCS == 50200032) && (chr.getCSPoints(1) >= 1000))
                {
                    chr.modifyCSPoints(1, 64536, false);
                    chr.modifyCSPoints(2, 1000, false);
                    chr.dropMessage(1, "兑换抵用卷成功");
                }
                else if ((snCS == 50200033) && (chr.getCSPoints(1) >= 5000))
                {
                    chr.modifyCSPoints(1, 60536, false);
                    chr.modifyCSPoints(2, 5000, false);
                    chr.dropMessage(1, "兑换抵用卷成功");
                }
                else
                {
                    chr.dropMessage(1, "没有找到这个道具的信息。");
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;

            case 52:
                c.getSession().write(MTSCSPacket.redeemResponse());
                break;
            case 65:
                long uniqueId1 = slea.readLong();
                Item boxItem = cs.findByCashId((int) uniqueId1);
                if ((boxItem == null) || (!cashinfo.hasRandomItem(boxItem.getItemId())))
                {
                    chr.dropMessage(1, "打开箱子失败，服务器找不到对应的道具信息。");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                List<Integer> boxItemSNs = cashinfo.getRandomItem(boxItem.getItemId());
                if (boxItemSNs.isEmpty())
                {
                    chr.dropMessage(1, "打开箱子失败，服务器找不到对应的道具信息。");
                    c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                    return;
                }
                snCS = boxItemSNs.get(server.Randomizer.nextInt(boxItemSNs.size()));
                cItem = cashinfo.getItem(snCS);
                if (cItem != null)
                {
                    item = cs.toItem(cItem);
                    if ((item != null) && (item.getUniqueId() > 0) && (item.getItemId() == cItem.getId()) && (item.getQuantity() == cItem.getCount()))
                    {
                        if (chr.getInventory(ItemConstants.getInventoryType(item.getItemId())).addItem(item) != -1)
                        {
                            cs.removeFromInventory(boxItem);
                            c.getSession().write(MTSCSPacket.商城打开箱子(item, uniqueId1));
                        }
                        else
                        {
                            chr.dropMessage(1, "打开箱子失败，请确认背包是否有足够的空间。");
                        }
                    }
                }
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                break;
            case 10:
            case 12:
            case 13:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 33:
            case 34:
            case 36:
            case 40:
            case 41:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 51:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            default:
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
                System.out.println("商城操作未知的操作类型: 0x" + StringUtil.getLeftPaddedStr(Integer.toHexString(action).toUpperCase(), '0', 2) + " " + slea.toString());
        }
    }

    private static void addCashshopLog(MapleCharacter chr, int SN, int itemId, int type, int price, int count, String itemLog)
    {
        if (chr == null)
        {
            return;
        }
        try
        {
            PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("INSERT INTO cashshop_log (accId, chrId, name, SN, itemId, type, price, count, cash, points, itemlog)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, chr.getAccountID());
            ps.setInt(2, chr.getId());
            ps.setString(3, chr.getName());
            ps.setInt(4, SN);
            ps.setInt(5, itemId);
            ps.setInt(6, type);
            ps.setInt(7, price);
            ps.setInt(8, count);
            ps.setInt(9, chr.getCSPoints(1));
            ps.setInt(10, chr.getCSPoints(2));
            ps.setString(11, itemLog);
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            FileoutputUtil.outputFileError("log\\Packet_Except.log", e, "玩家: " + chr.getName() + " ID: " + chr.getId() + " 购买商城道具保存日志出错.");
        }
    }

    public static void 商城送礼(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        slea.readMapleAsciiString();
        int snCS = slea.readInt();
        CashItemFactory cashinfo = CashItemFactory.getInstance();
        CashItemInfo item = cashinfo.getItem(snCS);
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        String partnerName = slea.readMapleAsciiString();
        String msg = slea.readMapleAsciiString();
        if (snCS == 92000046)
        {
            AutobanManager.getInstance().autoban(chr.getClient(), "商城非法购买道具.");
            return;
        }
        if ((cashinfo.isBlockedCashItemId(item.getId())) || (cashinfo.isBlockCashSnId(snCS)))
        {
            chr.dropMessage(1, "该道具禁止购买.");
            c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
            return;
        }

        if ((item == null) || (chr.getCSPoints(1) < item.getPrice()) || (msg.length() > 73) || (msg.length() < 1))
        {
            c.getSession().write(MTSCSPacket.商城错误提示(3));

            return;
        }
        Triple<Integer, Integer, Integer> info = MapleCharacterUtil.getInfoByName(partnerName, chr.getWorld());
        if ((info == null) || (info.getLeft() <= 0))
        {
            c.getSession().write(MTSCSPacket.商城错误提示(7));
        }
        else if ((info.getLeft() == chr.getId()) || (info.getMid() == c.getAccID()))
        {
            c.getSession().write(MTSCSPacket.商城错误提示(6));
        }
        else if (!item.genderEquals(info.getRight()))
        {
            c.getSession().write(MTSCSPacket.商城错误提示(8));
        }
        else
        {
            if (!ii.isCash(item.getId()))
            {
                log.info("[作弊] " + chr.getName() + " 商城非法购买礼物道具.道具: " + item.getId() + " - " + ii.getName(item.getId()));
                chr.dropMessage(1, "购买商城礼物道具出现错误.");
                c.getSession().write(MTSCSPacket.刷新点卷信息(chr));

                return;
            }
            if (item.getPrice() <= 0)
            {
                AutobanManager.getInstance().autoban(chr.getClient(), "商城非法赠送礼包道具.");
                return;
            }

            chr.getCashInventory().gift(info.getLeft(), chr.getName(), msg, item.getSN(), MapleInventoryIdentifier.getInstance());
            chr.modifyCSPoints(1, -item.getPrice(), false);
            c.getSession().write(MTSCSPacket.商城送礼(item.getId(), item.getCount(), partnerName));
            c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
            addCashshopLog(chr, item.getSN(), item.getId(), 1, item.getPrice(), item.getCount(), chr.getName() + " 购买道具: " + ii.getName(item.getId()) + " 送给 " + partnerName);
            chr.sendNote(partnerName, partnerName + " 您已收到" + chr.getName() + "送给您的礼物，请进入现金商城查看！");
            int chz = WorldFindService.getInstance().findChannel(partnerName);
            if (chz > 0)
            {
                MapleCharacter receiver = ChannelServer.getInstance(chz).getPlayerStorage().getCharacterByName(partnerName);
                if (receiver != null)
                {
                    receiver.showNote();
                }
            }
        }
    }

    private static MapleInventoryType getInventoryType(int id)
    {
        switch (id)
        {
            case 50200075:
                return MapleInventoryType.EQUIP;
            case 50200074:
                return MapleInventoryType.USE;
            case 50200073:
                return MapleInventoryType.ETC;
        }
        return MapleInventoryType.UNDEFINED;
    }

    public static void openAvatarRandomBox(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        CashShop cs = chr.getCashInventory();
        Item item = cs.findByCashId((int) slea.readLong());
        if ((item == null) || (item.getItemId() != 5222036))
        {
            c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
            return;
        }
        chr.dropMessage(1, "当前游戏不支持此功能");
        c.getSession().write(MTSCSPacket.刷新点卷信息(chr));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\cashshop\handler\BuyCashItemHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */