package handling.cashshop.handler;

import org.apache.log4j.Logger;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import handling.cashshop.CashShopServer;
import handling.channel.ChannelServer;
import handling.world.CharacterTransfer;
import handling.world.World;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.data.input.SeekableLittleEndianAccessor;
import tools.packet.MTSCSPacket;

public class CashShopOperation
{
    private static final Logger log = Logger.getLogger(CashShopOperation.class);

    public static void LeaveCS(SeekableLittleEndianAccessor slea, MapleClient c, MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        int channel = c.getChannel();
        ChannelServer toch = ChannelServer.getInstance(channel);
        if (toch == null)
        {
            FileoutputUtil.log("log\\LeaveCashShop.txt", "玩家: " + chr.getName() + " 从商城离开发生错误.找不到频道[" + channel + "]的信息.", true);
            c.getSession().close(true);
            return;
        }

        World.ChannelChange_Data(new CharacterTransfer(chr), chr.getId(), c.getChannel());
        CashShopServer.getPlayerStorage().deregisterPlayer(chr);
        c.updateLoginState(1, c.getSessionIPAddress());
        String s = c.getSessionIPAddress();
        handling.login.LoginServer.addIPAuth(s.substring(s.indexOf('/') + 1));
        c.getSession().write(MaplePacketCreator.getChannelChange(c, Integer.parseInt(toch.getIP().split(":")[1])));
        chr.saveToDB(false, true);
        c.setPlayer(null);
        c.setReceiving(false);
    }

    public static void EnterCS(CharacterTransfer transfer, MapleClient c)
    {
        if (transfer == null)
        {
            c.getSession().close(true);
            return;
        }
        MapleCharacter chr = MapleCharacter.ReconstructChr(transfer, c, false);

        c.setPlayer(chr);
        c.setAccID(chr.getAccountID());

        if (!c.CheckIPAddress())
        {
            c.getSession().close(true);
            log.info("商城检测连接 - 2 " + (!c.CheckIPAddress()));
            return;
        }

        int state = c.getLoginState();
        boolean allowLogin = false;
        if (((state == 1) || (state == 3)) && (!World.isCharacterListConnected(c.loadCharacterNames(c.getWorld()))))
        {
            allowLogin = true;
        }

        if (!allowLogin)
        {
            c.setPlayer(null);
            c.getSession().close(true);
            log.info("商城检测连接 - 3 " + (!allowLogin));
            return;
        }
        c.updateLoginState(2, c.getSessionIPAddress());
        CashShopServer.getPlayerStorage().registerPlayer(chr);
        c.getSession().write(MTSCSPacket.warpchartoCS(c));
        c.getSession().write(MTSCSPacket.warpCS(c));
        java.util.List<tools.Pair<Item, String>> gifts = chr.getCashInventory().loadGifts();
        c.getSession().write(MTSCSPacket.enableCSUse(0));
        c.getSession().write(MTSCSPacket.商城道具栏信息(c));

        c.getSession().write(MTSCSPacket.商城礼物信息(c, gifts));
        c.getSession().write(MTSCSPacket.商城购物车(c.getPlayer(), false));

        c.getSession().write(MTSCSPacket.刷新点卷信息(c.getPlayer()));
        c.getSession().write(MTSCSPacket.刷新点卷信息(c.getPlayer()));
        c.getPlayer().getCashInventory().checkExpire(c);
    }

    public static void CSUpdate(MapleClient c)
    {
        c.getSession().write(MTSCSPacket.刷新点卷信息(c.getPlayer()));
    }

    public static void doCSPackets(MapleClient c)
    {
        c.getSession().write(MTSCSPacket.商城道具栏信息(c));
        c.getSession().write(MTSCSPacket.刷新点卷信息(c.getPlayer()));
        c.getSession().write(MTSCSPacket.enableCSUse(1));
        c.getPlayer().getCashInventory().checkExpire(c);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\cashshop\handler\CashShopOperation.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */