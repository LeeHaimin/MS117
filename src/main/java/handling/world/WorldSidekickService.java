package handling.world;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import handling.channel.ChannelServer;
import handling.world.sidekick.MapleSidekick;


public class WorldSidekickService
{
    private final Map<Integer, MapleSidekick> sidekickList;
    private final ReentrantReadWriteLock lock;

    private WorldSidekickService()
    {
        this.lock = new ReentrantReadWriteLock();
        this.sidekickList = new LinkedHashMap<>();
        for (MapleSidekick sidekick : MapleSidekick.loadAll())
        {
            if (sidekick.getId() >= 0)
            {
                this.sidekickList.put(sidekick.getId(), sidekick);
            }
        }
        System.out.println("[WorldSidekickService] 已经启动...");
    }

    public static WorldSidekickService getInstance()
    {
        return SingletonHolder.instance;
    }

    public void addLoadedSidekick(MapleSidekick sidekick)
    {
        if (sidekick.getId() >= 0)
        {
            this.sidekickList.put(sidekick.getId(), sidekick);
        }
    }

    public int createSidekick(int leaderId, int leaderId2)
    {
        return MapleSidekick.create(leaderId, leaderId2);
    }

    public void eraseSidekick(int id)
    {
        this.sidekickList.remove(id);
    }

    public void erasePlayer(int targetId)
    {
        int ch = WorldFindService.getInstance().findChannel(targetId);
        if (ch < 0)
        {
            return;
        }
        MapleCharacter player = ChannelServer.getInstance(ch).getPlayerStorage().getCharacterById(targetId);
        if (player != null)
        {
            player.setSidekick(null);
        }
    }

    public MapleSidekick getSidekick(int id)
    {
        return sidekickList.get(id);
    }

    public MapleSidekick getSidekickByChr(int id)
    {
        this.lock.readLock().lock();
        try
        {
            for (MapleSidekick sidekick : this.sidekickList.values())
            {
                if ((sidekick.getCharacter(0).getId() == id) || (sidekick.getCharacter(1).getId() == id))
                {
                    return sidekick;
                }
            }
        }
        finally
        {
            this.lock.readLock().unlock();
        }
        return null;
    }

    private static class SingletonHolder
    {
        protected static final WorldSidekickService instance = new WorldSidekickService();
    }
}