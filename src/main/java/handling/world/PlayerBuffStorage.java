package handling.world;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import client.MapleCoolDownValueHolder;
import client.MapleDiseaseValueHolder;


public class PlayerBuffStorage implements Serializable
{
    private static final Map<Integer, List<PlayerBuffValueHolder>> buffs = new ConcurrentHashMap();
    private static final Map<Integer, List<MapleCoolDownValueHolder>> coolDowns = new ConcurrentHashMap();
    private static final Map<Integer, List<MapleDiseaseValueHolder>> diseases = new ConcurrentHashMap();

    public static void addBuffsToStorage(int chrid, List<PlayerBuffValueHolder> toStore)
    {
        buffs.put(chrid, toStore);
    }

    public static void addCooldownsToStorage(int chrid, List<MapleCoolDownValueHolder> toStore)
    {
        coolDowns.put(chrid, toStore);
    }

    public static void addDiseaseToStorage(int chrid, List<MapleDiseaseValueHolder> toStore)
    {
        diseases.put(chrid, toStore);
    }

    public static List<PlayerBuffValueHolder> getBuffsFromStorage(int chrid)
    {
        return buffs.remove(chrid);
    }

    public static List<MapleCoolDownValueHolder> getCooldownsFromStorage(int chrid)
    {
        return coolDowns.remove(chrid);
    }

    public static List<MapleDiseaseValueHolder> getDiseaseFromStorage(int chrid)
    {
        return diseases.remove(chrid);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\PlayerBuffStorage.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */