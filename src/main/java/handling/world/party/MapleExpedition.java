package handling.world.party;

import java.util.ArrayList;
import java.util.List;

public class MapleExpedition
{
    private final List<Integer> parties;
    private final ExpeditionType type;
    private final int id;
    private int leaderId;

    public MapleExpedition(ExpeditionType etype, int leaderId, int id)
    {
        this.type = etype;
        this.id = id;
        this.leaderId = leaderId;
        this.parties = new ArrayList(etype.maxParty);
    }

    public ExpeditionType getType()
    {
        return this.type;
    }

    public int getLeader()
    {
        return this.leaderId;
    }

    public void setLeader(int newLead)
    {
        this.leaderId = newLead;
    }

    public List<Integer> getParties()
    {
        return this.parties;
    }

    public int getId()
    {
        return this.id;
    }

    public int getAllMembers()
    {
        int ret = 0;
        for (int i = 0; i < this.parties.size(); i++)
        {
            MapleParty pp = handling.world.WrodlPartyService.getInstance().getParty(this.parties.get(i));
            if (pp == null)
            {
                this.parties.remove(i);
            }
            else
            {
                ret += pp.getMembers().size();
            }
        }
        return ret;
    }

    public int getFreeParty()
    {
        for (int i = 0; i < this.parties.size(); i++)
        {
            MapleParty party = handling.world.WrodlPartyService.getInstance().getParty(this.parties.get(i));
            if (party == null)
            {
                this.parties.remove(i);
            }
            else if (party.getMembers().size() < 6)
            {
                return party.getId();
            }
        }
        if (this.parties.size() < this.type.maxParty)
        {
            return 0;
        }
        return -1;
    }

    public int getIndex(int partyId)
    {
        for (int i = 0; i < this.parties.size(); i++)
        {
            if (this.parties.get(i) == partyId)
            {
                return i;
            }
        }
        return -1;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\party\MapleExpedition.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */