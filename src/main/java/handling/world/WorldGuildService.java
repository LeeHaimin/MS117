package handling.world;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import handling.Auction.AuctionServer;
import handling.cashshop.CashShopServer;
import handling.channel.ChannelServer;
import handling.channel.PlayerStorage;
import handling.world.guild.MapleBBSThread;
import handling.world.guild.MapleGuild;
import handling.world.guild.MapleGuildCharacter;
import tools.packet.GuildPacket;


public class WorldGuildService
{
    private final Map<Integer, MapleGuild> guildList;
    private final ReentrantReadWriteLock lock;

    private WorldGuildService()
    {
        this.lock = new ReentrantReadWriteLock();
        this.guildList = new LinkedHashMap<>();
        System.out.println("[WorldGuildService] 已经启动...");
    }

    public static WorldGuildService getInstance()
    {
        return SingletonHolder.instance;
    }

    public void addLoadedGuild(MapleGuild guild)
    {
        if (guild.isProper())
        {
            this.guildList.put(guild.getId(), guild);
        }
    }


    public int createGuild(int leaderId, String name)
    {
        return MapleGuild.createGuild(leaderId, name);
    }

    public MapleGuild getGuild(MapleCharacter chr)
    {
        return getGuild(chr.getGuildId());
    }

    public MapleGuild getGuild(int id)
    {
        MapleGuild ret = null;
        lock.readLock().lock();
        try
        {
            ret = guildList.get(id);
        }
        finally
        {
            lock.readLock().unlock();
        }
        if (ret == null)
        {
            lock.writeLock().lock();
            try
            {
                ret = new MapleGuild(id);
                if (ret == null || ret.getId() <= 0 || !ret.isProper())
                { //failed to load
                    return null;
                }
                guildList.put(id, ret);
            }
            finally
            {
                lock.writeLock().unlock();
            }
        }
        return ret; //Guild doesn't exist?
    }

    public void setGuildMemberOnline(MapleGuildCharacter guildMember, boolean isOnline, int channel)
    {
        MapleGuild guild = getGuild(guildMember.getGuildId());
        if (guild != null)
        {
            guild.setOnline(guildMember.getId(), isOnline, channel);
        }
    }

    public void guildPacket(int guildId, byte[] message)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.broadcast(message);
        }
    }

    public int addGuildMember(MapleGuildCharacter guildMember)
    {
        MapleGuild guild = getGuild(guildMember.getGuildId());
        if (guild != null)
        {
            return guild.addGuildMember(guildMember);
        }
        return 0;
    }

    public int addGuildMember(int guildId, int chrId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.addGuildMember(chrId);
        }
        return 0;
    }

    public int addGuildApplyMember(MapleGuildCharacter guildMember)
    {
        MapleGuild guild = getGuild(guildMember.getGuildId());
        if (guild != null)
        {
            return guild.addGuildApplyMember(guildMember);
        }
        return 0;
    }

    public int denyGuildApplyMember(int guildId, int chrId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.denyGuildApplyMember(chrId);
            int ch = WorldFindService.getInstance().findChannel(chrId);
            if (ch < 0)
            {
                return 0;
            }
            MapleCharacter player = getStorage(ch).getCharacterById(chrId);
            if (player == null)
            {
                return 0;
            }
            player.getClient().getSession().write(GuildPacket.DenyGuildApply(chrId));
            return 1;
        }
        return 0;
    }

    public PlayerStorage getStorage(int channel)
    {
        if (channel == -20) return AuctionServer.getPlayerStorage();
        if (channel == -10)
        {
            return CashShopServer.getPlayerStorage();
        }
        return ChannelServer.getInstance(channel).getPlayerStorage();
    }

    public void leaveGuild(MapleGuildCharacter guildMember)
    {
        MapleGuild guild = getGuild(guildMember.getGuildId());
        if (guild != null)
        {
            guild.leaveGuild(guildMember);
        }
    }


    public void guildChat(int guildId, String name, int chrId, String msg)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.guildChat(name, chrId, msg);
        }
    }


    public void changeRank(int guildId, int chrId, int newRank)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.changeRank(chrId, newRank);
        }
    }


    public void expelMember(MapleGuildCharacter initiator, String name, int chrId)
    {
        MapleGuild guild = getGuild(initiator.getGuildId());
        if (guild != null)
        {
            guild.expelMember(initiator, name, chrId);
        }
    }


    public void setGuildNotice(int guildId, String notice)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.setGuildNotice(notice);
        }
    }


    public void setGuildLeader(int guildId, int chrId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.changeGuildLeader(chrId);
        }
    }


    public int getSkillLevel(int guildId, int skillId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.getSkillLevel(skillId);
        }
        return 0;
    }


    public boolean purchaseSkill(int guildId, int skillId, String name, int chrId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.purchaseSkill(skillId, name, chrId);
        }
        return false;
    }


    public boolean activateSkill(int guildId, int skillId, String name)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.activateSkill(skillId, name);
        }
        return false;
    }


    public void memberLevelJobUpdate(MapleGuildCharacter guildMember)
    {
        MapleGuild guild = getGuild(guildMember.getGuildId());
        if (guild != null)
        {
            guild.memberLevelJobUpdate(guildMember);
        }
    }


    public void changeRankTitle(int guildId, String[] ranks)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.changeRankTitle(ranks);
        }
    }


    public void setGuildEmblem(int guildId, short bg, byte bgcolor, short logo, byte logocolor)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.setGuildEmblem(bg, bgcolor, logo, logocolor);
        }
    }

    public void disbandGuild(int gid)
    {
        MapleGuild g = getGuild(gid);
        lock.writeLock().lock();
        try
        {
            if (g != null)
            {
                g.disbandGuild();
                guildList.remove(gid);
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public void deleteGuildCharacter(int guildId, int charId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            MapleGuildCharacter mc = guild.getMGC(charId);
            if (mc != null)
            {
                if (mc.getGuildRank() > 1)
                {
                    guild.leaveGuild(mc);
                }
                else
                {
                    guild.disbandGuild();
                }
            }
        }
    }


    public boolean increaseGuildCapacity(int guildId, boolean b)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.increaseCapacity(b);
        }
        return false;
    }


    public void gainGP(int guildId, int amount)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.gainGP(amount);
        }
    }

    public void gainGP(int guildId, int amount, int chrId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.gainGP(amount, false, chrId);
        }
    }

    public int getGP(int guildId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.getGP();
        }
        return 0;
    }

    public int getInvitedId(int guildId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.getInvitedId();
        }
        return 0;
    }

    public void setInvitedId(int guildId, int inviteId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.setInvitedId(inviteId);
        }
    }

    public int getGuildLeader(int guildName)
    {
        MapleGuild guild = getGuild(guildName);
        if (guild != null)
        {
            return guild.getLeaderId();
        }
        return 0;
    }

    public int getGuildLeader(String guildName)
    {
        MapleGuild guild = getGuildByName(guildName);
        if (guild != null)
        {
            return guild.getLeaderId();
        }
        return 0;
    }

    public MapleGuild getGuildByName(String guildName)
    {
        this.lock.readLock().lock();
        try
        {
            for (MapleGuild guild : this.guildList.values())
            {
                if (guild.getName().equalsIgnoreCase(guildName))
                {
                    return guild;
                }
            }
            return null;
        }
        finally
        {
            this.lock.readLock().unlock();
        }
    }

    public void save()
    {
        System.out.println("Saving guilds...");
        lock.writeLock().lock();
        try
        {
            for (MapleGuild a : guildList.values())
            {
                a.writeToDB(false);
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public List<MapleBBSThread> getBBS(int guildId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.getBBS();
        }
        return null;
    }

    public int addBBSThread(int guildId, String title, String text, int icon, boolean bNotice, int posterId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            return guild.addBBSThread(title, text, icon, bNotice, posterId);
        }
        return -1;
    }

    public void editBBSThread(int guildId, int localthreadId, String title, String text, int icon, int posterId, int guildRank)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.editBBSThread(localthreadId, title, text, icon, posterId, guildRank);
        }
    }

    public void deleteBBSThread(int guildId, int localthreadId, int posterId, int guildRank)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.deleteBBSThread(localthreadId, posterId, guildRank);
        }
    }

    public void addBBSReply(int guildId, int localthreadId, String text, int posterId)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.addBBSReply(localthreadId, text, posterId);
        }
    }

    public void deleteBBSReply(int guildId, int localthreadId, int replyId, int posterId, int guildRank)
    {
        MapleGuild guild = getGuild(guildId);
        if (guild != null)
        {
            guild.deleteBBSReply(localthreadId, replyId, posterId, guildRank);
        }
    }

    public void changeEmblem(int guildId, int affectedPlayers, MapleGuild guild)
    {
        WorldBroadcastService.getInstance().sendGuildPacket(affectedPlayers, GuildPacket.guildEmblemChange(guildId, (short) guild.getLogoBG(), (byte) guild.getLogoBGColor(), (short) guild.getLogo()
                , (byte) guild.getLogoColor()), -1, guildId);
        setGuildAndRank(affectedPlayers, -1, -1, -1, -1);
    }

    public void setGuildAndRank(int chrId, int guildId, int rank, int contribution, int alliancerank)
    {
        int ch = WorldFindService.getInstance().findChannel(chrId);
        if (ch == -1)
        {
            return;
        }
        MapleCharacter player = getStorage(ch).getCharacterById(chrId);
        if (player == null) return;
        boolean isDifferentGuild;
        if ((guildId == -1) && (rank == -1))
        {
            isDifferentGuild = true;
        }
        else
        {
            isDifferentGuild = guildId != player.getGuildId();
            player.setGuildId(guildId);
            player.setGuildRank((byte) rank);
            player.setGuildContribution(contribution);
            player.setAllianceRank((byte) alliancerank);
            player.saveGuildStatus();
        }
        if ((isDifferentGuild) && (ch > 0))
        {
            player.getMap().broadcastMessage(player, GuildPacket.loadGuildName(player), false);
            player.getMap().broadcastMessage(player, GuildPacket.loadGuildIcon(player), false);
        }
    }

    private static class SingletonHolder
    {
        protected static final WorldGuildService instance = new WorldGuildService();
    }
}