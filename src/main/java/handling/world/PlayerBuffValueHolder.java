package handling.world;

import java.io.Serializable;
import java.util.ArrayList;

import client.MapleBuffStat;
import server.MapleStatEffect;
import tools.Pair;

public class PlayerBuffValueHolder implements Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    public final long startTime;
    public final int localDuration;
    public final int fromChrId;
    public final MapleStatEffect effect;
    public final ArrayList<Pair<MapleBuffStat, Integer>> statup;

    public PlayerBuffValueHolder(long startTime, MapleStatEffect effect, ArrayList<Pair<MapleBuffStat, Integer>> statup, int localDuration, int fromChrId)
    {
        this.startTime = startTime;
        this.effect = effect;
        this.statup = statup;
        this.localDuration = localDuration;
        this.fromChrId = fromChrId;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\PlayerBuffValueHolder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */