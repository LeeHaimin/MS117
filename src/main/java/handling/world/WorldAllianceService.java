package handling.world;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import handling.world.guild.MapleGuild;
import handling.world.guild.MapleGuildAlliance;
import tools.MaplePacketCreator;
import tools.packet.GuildPacket;

public class WorldAllianceService
{
    private final Map<Integer, MapleGuildAlliance> allianceList;
    private final ReentrantReadWriteLock lock;

    private WorldAllianceService()
    {
        this.lock = new ReentrantReadWriteLock();
        this.allianceList = new LinkedHashMap<>();
        Collection<MapleGuildAlliance> allGuilds = MapleGuildAlliance.loadAll();
        for (MapleGuildAlliance alliance : allGuilds)
        {
            this.allianceList.put(alliance.getId(), alliance);
        }
        System.out.println("[WorldAllianceService] 已经启动...");
    }

    public static WorldAllianceService getInstance()
    {
        return SingletonHolder.instance;
    }

    public int getAllianceLeader(int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.getLeaderId();
        }
        return 0;
    }

    public MapleGuildAlliance getAlliance(int allianceId)
    {
        MapleGuildAlliance ret = null;
        lock.readLock().lock();
        try
        {
            ret = allianceList.get(allianceId);
        }
        finally
        {
            lock.readLock().unlock();
        }
        if (ret == null)
        {
            lock.writeLock().lock();
            try
            {
                ret = new MapleGuildAlliance(allianceId);
                if (ret == null || ret.getId() <= 0)
                { //failed to load
                    return null;
                }
                allianceList.put(allianceId, ret);
            }
            finally
            {
                lock.writeLock().unlock();
            }
        }
        return ret;
    }

    public void updateAllianceRanks(int allianceId, String[] ranks)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            alliance.setRank(ranks);
        }
    }


    public void updateAllianceNotice(int allianceId, String notice)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            alliance.setNotice(notice);
        }
    }


    public boolean canInvite(int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.getCapacity() > alliance.getNoGuilds();
        }
        return false;
    }


    public boolean changeAllianceLeader(int allianceId, int chrId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.setLeaderId(chrId);
        }
        return false;
    }


    public boolean changeAllianceLeader(int allianceId, int chrId, boolean sameGuild)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.setLeaderId(chrId, sameGuild);
        }
        return false;
    }


    public boolean changeAllianceRank(int allianceId, int chrId, int change)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.changeAllianceRank(chrId, change);
        }
        return false;
    }


    public boolean changeAllianceCapacity(int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.setCapacity();
        }
        return false;
    }


    public boolean disbandAlliance(int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.disband();
        }
        return false;
    }


    public boolean addGuildToAlliance(int allianceId, int guildId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.addGuild(guildId);
        }
        return false;
    }


    public boolean removeGuildFromAlliance(int allianceId, int guildId, boolean expelled)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            return alliance.removeGuild(guildId, expelled);
        }
        return false;
    }


    public void sendGuild(int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            sendGuild(GuildPacket.getAllianceUpdate(alliance), -1, allianceId);
            sendGuild(GuildPacket.getGuildAlliance(alliance), -1, allianceId);
        }
    }

    public void sendGuild(byte[] packet, int exceptionId, int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            for (int i = 0; i < alliance.getNoGuilds(); i++)
            {
                int guildId = alliance.getGuildId(i);
                if ((guildId > 0) && (guildId != exceptionId))
                {
                    WorldGuildService.getInstance().guildPacket(guildId, packet);
                }
            }
        }
    }


    public boolean createAlliance(String allianceName, int chrId, int chrId2, int guildId, int guildId2)
    {
        int allianceId = MapleGuildAlliance.createToDb(chrId, allianceName, guildId, guildId2);
        if (allianceId <= 0)
        {
            return false;
        }
        MapleGuild leaderGuild = WorldGuildService.getInstance().getGuild(guildId);
        MapleGuild memberGuild = WorldGuildService.getInstance().getGuild(guildId2);
        leaderGuild.setAllianceId(allianceId);
        memberGuild.setAllianceId(allianceId);
        leaderGuild.changeARank(true);
        memberGuild.changeARank(false);

        MapleGuildAlliance alliance = getAlliance(allianceId);

        sendGuild(GuildPacket.createGuildAlliance(alliance), -1, allianceId);
        sendGuild(GuildPacket.getAllianceInfo(alliance), -1, allianceId);
        sendGuild(GuildPacket.getGuildAlliance(alliance), -1, allianceId);
        sendGuild(GuildPacket.changeAlliance(alliance, true), -1, allianceId);
        return true;
    }


    public void allianceChat(int guildId, String name, int chrId, String msg)
    {
        MapleGuild chrGuild = WorldGuildService.getInstance().getGuild(guildId);
        if (chrGuild != null)
        {
            MapleGuildAlliance alliance = getAlliance(chrGuild.getAllianceId());
            if (alliance != null)
            {
                for (int i = 0; i < alliance.getNoGuilds(); i++)
                {
                    MapleGuild guild = WorldGuildService.getInstance().getGuild(alliance.getGuildId(i));
                    if (guild != null)
                    {
                        guild.allianceChat(name, chrId, msg);
                    }
                }
            }
        }
    }

    public void setNewAlliance(int guildId, int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        MapleGuild guild = WorldGuildService.getInstance().getGuild(guildId);
        if ((alliance != null) && (guild != null))
        {
            for (int i = 0; i < alliance.getNoGuilds(); i++)
            {
                if (guildId == alliance.getGuildId(i))
                {
                    guild.setAllianceId(allianceId);
                    guild.broadcast(GuildPacket.getAllianceInfo(alliance));
                    guild.broadcast(GuildPacket.getGuildAlliance(alliance));
                    guild.broadcast(GuildPacket.changeAlliance(alliance, true));
                    guild.changeARank();
                    guild.writeToDB(false);
                }
                else
                {
                    MapleGuild guild_ = WorldGuildService.getInstance().getGuild(alliance.getGuildId(i));
                    if (guild_ != null)
                    {
                        guild_.broadcast(GuildPacket.addGuildToAlliance(alliance, guild));
                        guild_.broadcast(GuildPacket.changeGuildInAlliance(alliance, guild, true));
                    }
                }
            }
        }
    }

    public void setOldAlliance(int guildId, boolean expelled, int allianceId)
    {
        MapleGuildAlliance alliance = getAlliance(allianceId);
        MapleGuild guild_ = WorldGuildService.getInstance().getGuild(guildId);
        if (alliance != null)
        {
            for (int i = 0; i < alliance.getNoGuilds(); i++)
            {
                MapleGuild guild = WorldGuildService.getInstance().getGuild(alliance.getGuildId(i));
                if (guild == null)
                {
                    if (guildId != alliance.getGuildId(i))
                    {
                        alliance.removeGuild(guildId, false, true);
                    }

                }
                else if ((guild_ == null) || (guildId == alliance.getGuildId(i)))
                {
                    guild.changeARank(5);
                    guild.setAllianceId(0);
                    guild.broadcast(GuildPacket.disbandAlliance(allianceId));
                }
                else if (guild_ != null)
                {
                    guild.broadcast(MaplePacketCreator.serverNotice(5, "[" + guild_.getName() + "] 家族退出家族联盟."));
                    guild.broadcast(GuildPacket.changeGuildInAlliance(alliance, guild_, false));
                    guild.broadcast(GuildPacket.removeGuildFromAlliance(alliance, guild_, expelled));
                }
            }
        }
        if (guildId == -1)
        {
            this.lock.writeLock().lock();
            try
            {
                this.allianceList.remove(allianceId);
            }
            finally
            {
                this.lock.writeLock().unlock();
            }
        }
    }

    public List<byte[]> getAllianceInfo(int allianceId, boolean start)
    {
        List<byte[]> ret = new ArrayList<>();
        MapleGuildAlliance alliance = getAlliance(allianceId);
        if (alliance != null)
        {
            if (start)
            {
                ret.add(GuildPacket.getAllianceInfo(alliance));
                ret.add(GuildPacket.getGuildAlliance(alliance));
            }
            ret.add(GuildPacket.getAllianceUpdate(alliance));
        }
        return ret;
    }

    public void save()
    {
        System.out.println("Saving alliances...");
        lock.writeLock().lock();
        try
        {
            for (MapleGuildAlliance a : this.allianceList.values())
            {
                a.saveToDb();
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    private static class SingletonHolder
    {
        protected static final WorldAllianceService instance = new WorldAllianceService();
    }
}
