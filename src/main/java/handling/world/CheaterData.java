package handling.world;

import java.io.Serializable;

public class CheaterData implements Serializable, Comparable<CheaterData>
{
    private static final long serialVersionUID = -8733673311051249885L;
    private final int points;
    private final String info;

    public CheaterData(int points, String info)
    {
        this.points = points;
        this.info = info;
    }

    public String getInfo()
    {
        return this.info;
    }

    public int compareTo(CheaterData o)
    {
        int thisVal = getPoints();
        int anotherVal = o.getPoints();
        return thisVal == anotherVal ? 0 : thisVal < anotherVal ? 1 : -1;
    }

    public int getPoints()
    {
        return this.points;
    }

    public boolean equals(Object oth)
    {
        if (!(oth instanceof CheaterData))
        {
            return false;
        }
        CheaterData obj = (CheaterData) oth;
        return (obj.points == this.points) && (obj.info.equals(this.info));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\CheaterData.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */