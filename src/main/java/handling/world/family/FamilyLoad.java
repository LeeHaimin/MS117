package handling.world.family;

import org.apache.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import handling.world.WorldFamilyService;

public class FamilyLoad
{
    public static final int NumSavingThreads = 8;
    private static final TimingThread[] Threads = new TimingThread[8];
    private static final Logger log = Logger.getLogger(FamilyLoad.class);
    private static final AtomicInteger Distribute = new AtomicInteger(0);

    static
    {
        for (int i = 0; i < Threads.length; i++)
            Threads[i] = new TimingThread(new FamilyLoadRunnable());
    }

    public static void QueueFamilyForLoad(int hm)
    {
        int Current = Distribute.getAndIncrement() % 8;
        Threads[Current].getRunnable().Queue(hm);
    }

    public static void Execute(Object ToNotify)
    {
        for (TimingThread timingThread : Threads)
        {
            timingThread.getRunnable().SetToNotify(ToNotify);
        }
        for (TimingThread thread : Threads)
        {
            thread.start();
        }
    }

    private static class FamilyLoadRunnable implements Runnable
    {
        private final ArrayBlockingQueue<Integer> Queue = new ArrayBlockingQueue(1000);
        private Object ToNotify;

        public void run()
        {
            try
            {
                while (!this.Queue.isEmpty())
                {
                    WorldFamilyService.getInstance().addLoadedFamily(new MapleFamily(this.Queue.take()));
                }
                synchronized (this.ToNotify)
                {
                    this.ToNotify.notify();
                }
            }
            catch (InterruptedException ex)
            {
                FamilyLoad.log.error("[FamilyLoad] 加载学院信息出错." + ex);
            }
        }

        private void Queue(Integer hm)
        {
            this.Queue.add(hm);
        }

        private void SetToNotify(Object o)
        {
            if (this.ToNotify == null)
            {
                this.ToNotify = o;
            }
        }
    }

    private static class TimingThread extends Thread
    {
        private final FamilyLoad.FamilyLoadRunnable ext;

        public TimingThread(FamilyLoad.FamilyLoadRunnable r)
        {
            super();
            this.ext = r;
        }

        public FamilyLoad.FamilyLoadRunnable getRunnable()
        {
            return this.ext;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\family\FamilyLoad.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */