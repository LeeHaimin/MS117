package handling.world.family;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import client.MapleCharacter;
import database.DatabaseConnection;
import handling.world.WorldFamilyService;

public final class MapleFamily implements java.io.Serializable
{
    public static final long serialVersionUID = 6322150443228168192L;
    private static final Logger log = Logger.getLogger(MapleFamily.class);
    private final Map<Integer, MapleFamilyCharacter> members = new ConcurrentHashMap();
    private String leadername = null;
    private String notice;
    private int id;
    private int leaderid;
    private boolean proper = true;
    private boolean bDirty = false;
    private boolean changed = false;

    public MapleFamily(int familyid)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM families WHERE familyid = ?");
            ps.setInt(1, familyid);
            ResultSet rs = ps.executeQuery();

            if (!rs.next())
            {
                rs.close();
                ps.close();
                this.id = -1;
                this.proper = false;
                return;
            }
            this.id = familyid;
            this.leaderid = rs.getInt("leaderid");
            this.notice = rs.getString("notice");
            rs.close();
            ps.close();

            ps = con.prepareStatement("SELECT id, name, level, job, seniorid, junior1, junior2, currentrep, totalrep FROM characters WHERE familyid = ?", 1008);
            ps.setInt(1, familyid);
            rs = ps.executeQuery();
            while (rs.next())
            {
                if (rs.getInt("id") == this.leaderid)
                {
                    this.leadername = rs.getString("name");
                }
                this.members.put(rs.getInt("id"), new MapleFamilyCharacter(rs.getInt("id"), rs.getShort("level"), rs.getString("name"), -1, rs.getInt("job"), familyid, rs.getInt("seniorid"),
                        rs.getInt("junior1"), rs.getInt("junior2"), rs.getInt("currentrep"), rs.getInt("totalrep"), false));
            }
            rs.close();
            ps.close();

            if ((this.leadername == null) || (this.members.size() < 2))
            {
                System.err.println("Leader " + this.leaderid + " isn't in family " + this.id + ". Members: " + this.members.size() + ".  Impossible... family is disbanding.");
                writeToDB(true);
                this.proper = false;
                return;
            }

            for (MapleFamilyCharacter mfc : this.members.values())
            {
                if ((mfc.getJunior1() > 0) && ((getMFC(mfc.getJunior1()) == null) || (mfc.getId() == mfc.getJunior1())))
                {
                    mfc.setJunior1(0);
                }
                if ((mfc.getJunior2() > 0) && ((getMFC(mfc.getJunior2()) == null) || (mfc.getId() == mfc.getJunior2()) || (mfc.getJunior1() == mfc.getJunior2())))
                {
                    mfc.setJunior2(0);
                }
                if ((mfc.getSeniorId() > 0) && ((getMFC(mfc.getSeniorId()) == null) || (mfc.getId() == mfc.getSeniorId())))
                {
                    mfc.setSeniorId(0);
                }
                if ((mfc.getJunior2() > 0) && (mfc.getJunior1() <= 0))
                {
                    mfc.setJunior1(mfc.getJunior2());
                    mfc.setJunior2(0);
                }
                if (mfc.getJunior1() > 0)
                {
                    MapleFamilyCharacter mfc2 = getMFC(mfc.getJunior1());
                    if (mfc2.getJunior1() == mfc.getId())
                    {
                        mfc2.setJunior1(0);
                    }
                    if (mfc2.getJunior2() == mfc.getId())
                    {
                        mfc2.setJunior2(0);
                    }
                    if (mfc2.getSeniorId() != mfc.getId())
                    {
                        mfc2.setSeniorId(mfc.getId());
                    }
                }
                if (mfc.getJunior2() > 0)
                {
                    MapleFamilyCharacter mfc2 = getMFC(mfc.getJunior2());
                    if (mfc2.getJunior1() == mfc.getId())
                    {
                        mfc2.setJunior1(0);
                    }
                    if (mfc2.getJunior2() == mfc.getId())
                    {
                        mfc2.setJunior2(0);
                    }
                    if (mfc2.getSeniorId() != mfc.getId())
                    {
                        mfc2.setSeniorId(mfc.getId());
                    }
                }
            }
            resetPedigree();
            resetDescendants();
        }
        catch (SQLException se)
        {
            log.error("[MapleFamily] 从数据库中读取学院信息出错." + se);
        }
    }

    public void writeToDB(boolean isDisband)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            if (!isDisband)
            {
                if (this.changed)
                {
                    PreparedStatement ps = con.prepareStatement("UPDATE families SET notice = ? WHERE familyid = ?");
                    ps.setString(1, this.notice);
                    ps.setInt(2, this.id);
                    ps.execute();
                    ps.close();
                }
                this.changed = false;
            }
            else
            {
                if ((this.leadername == null) || (this.members.size() < 2))
                {
                    broadcast(null, -1, FCOp.DISBAND, null);
                }

                PreparedStatement ps = con.prepareStatement("DELETE FROM families WHERE familyid = ?");
                ps.setInt(1, this.id);
                ps.execute();
                ps.close();
            }
        }
        catch (SQLException se)
        {
            log.error("[MapleFamily] 保存学院信息出错." + se);
        }
    }

    public MapleFamilyCharacter getMFC(int chrId)
    {
        return this.members.get(chrId);
    }

    public void resetPedigree()
    {
        for (MapleFamilyCharacter mfc : this.members.values())
        {
            mfc.resetPedigree(this);
        }
        this.bDirty = true;
    }

    public void resetDescendants()
    {
        MapleFamilyCharacter mfc = getMFC(this.leaderid);
        if (mfc != null)
        {
            mfc.resetDescendants(this);
        }
        this.bDirty = true;
    }

    public void broadcast(byte[] packet, int exceptionId, FCOp bcop, List<Integer> chrIds)
    {
        buildNotifications();
        if (this.members.size() < 2)
        {
            this.bDirty = true;
            return;
        }
        for (MapleFamilyCharacter mgc : this.members.values())
        {
            if ((chrIds == null) || (chrIds.contains(mgc.getId())))
            {
                if (bcop == FCOp.DISBAND)
                {
                    if (mgc.isOnline())
                    {
                        WorldFamilyService.getInstance().setFamily(0, 0, 0, 0, mgc.getCurrentRep(), mgc.getTotalRep(), mgc.getId());
                    }
                    else
                    {
                        setOfflineFamilyStatus(0, 0, 0, 0, mgc.getCurrentRep(), mgc.getTotalRep(), mgc.getId());
                    }
                }
                else if ((mgc.isOnline()) && (mgc.getId() != exceptionId))
                {
                    handling.world.WorldBroadcastService.getInstance().sendFamilyPacket(mgc.getId(), packet, exceptionId, this.id);
                }
            }
        }
    }

    private void buildNotifications()
    {
        if (!this.bDirty)
        {
            return;
        }
        Iterator<Map.Entry<Integer, MapleFamilyCharacter>> toRemove = this.members.entrySet().iterator();
        while (toRemove.hasNext())
        {
            MapleFamilyCharacter mfc = (MapleFamilyCharacter) ((Map.Entry) toRemove.next()).getValue();
            if ((mfc.getJunior1() > 0) && (getMFC(mfc.getJunior1()) == null))
            {
                mfc.setJunior1(0);
            }
            if ((mfc.getJunior2() > 0) && (getMFC(mfc.getJunior2()) == null))
            {
                mfc.setJunior2(0);
            }
            if ((mfc.getSeniorId() > 0) && (getMFC(mfc.getSeniorId()) == null))
            {
                mfc.setSeniorId(0);
            }
            if (mfc.getFamilyId() != this.id)
            {
                toRemove.remove();
            }
        }

        if ((this.members.size() < 2) && (WorldFamilyService.getInstance().getFamily(this.id) != null))
        {
            WorldFamilyService.getInstance().disbandFamily(this.id);
        }
        this.bDirty = false;
    }

    public static void setOfflineFamilyStatus(int familyid, int seniorid, int junior1, int junior2, int currentrep, int totalrep, int chrid)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE characters SET familyid = ?, seniorid = ?, junior1 = ?, junior2 = ?, currentrep = ?, totalrep = ? " +
                    "WHERE id = ?");
            ps.setInt(1, familyid);
            ps.setInt(2, seniorid);
            ps.setInt(3, junior1);
            ps.setInt(4, junior2);
            ps.setInt(5, currentrep);
            ps.setInt(6, totalrep);
            ps.setInt(7, chrid);
            ps.execute();
            ps.close();
        }
        catch (SQLException se)
        {
            System.out.println("SQLException: " + se.getLocalizedMessage());
        }
    }

    public static void loadAll()
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT familyid FROM families");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                WorldFamilyService.getInstance().addLoadedFamily(new MapleFamily(rs.getInt("familyid")));
            }
            rs.close();
            ps.close();
        }
        catch (SQLException se)
        {
            log.error("[MapleFamily] 从数据库中读取学院信息出错." + se);
        }
    }

    public static void loadAll(Object toNotify)
    {
        try
        {
            boolean cont = false;
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT familyid FROM families");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                FamilyLoad.QueueFamilyForLoad(rs.getInt("familyid"));
                cont = true;
            }
            rs.close();
            ps.close();
            if (!cont)
            {
                return;
            }
        }
        catch (SQLException se)
        {
            log.error("[MapleFamily] 从数据库中读取学院信息出错." + se);
        }
        AtomicInteger FinishedThreads = new AtomicInteger(0);
        FamilyLoad.Execute(toNotify);
        synchronized (toNotify)
        {
            try
            {
                toNotify.wait();
            }
            catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
        }
        while (FinishedThreads.incrementAndGet() != 8)
        {
            synchronized (toNotify)
            {
                try
                {
                    toNotify.wait();
                }
                catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static int createFamily(int leaderId)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("INSERT INTO families (`leaderid`) VALUES (?)", 1);
            ps.setInt(1, leaderId);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                return 0;
            }
            int ret = rs.getInt(1);
            rs.close();
            ps.close();
            return ret;
        }
        catch (Exception e)
        {
            log.error("[MapleFamily] 创建学院信息出错." + e);
        }
        return 0;
    }

    public static void mergeFamily(MapleFamily newfam, MapleFamily oldfam)
    {
        for (MapleFamilyCharacter mgc : oldfam.members.values())
        {
            mgc.setFamilyId(newfam.getId());
            if (mgc.isOnline())
            {
                WorldFamilyService.getInstance().setFamily(newfam.getId(), mgc.getSeniorId(), mgc.getJunior1(), mgc.getJunior2(), mgc.getCurrentRep(), mgc.getTotalRep(), mgc.getId());
            }
            else
            {
                setOfflineFamilyStatus(newfam.getId(), mgc.getSeniorId(), mgc.getJunior1(), mgc.getJunior2(), mgc.getCurrentRep(), mgc.getTotalRep(), mgc.getId());
            }
            newfam.members.put(mgc.getId(), mgc);
            newfam.setOnline(mgc.getId(), mgc.isOnline(), mgc.getChannel());
        }
        newfam.resetPedigree();

        WorldFamilyService.getInstance().disbandFamily(oldfam.getId());
    }

    public int getId()
    {
        return this.id;
    }

    public void setOnline(int chrId, boolean online, int channel)
    {
        MapleFamilyCharacter mgc = getMFC(chrId);
        if ((mgc != null) && (mgc.getFamilyId() == this.id))
        {
            if (mgc.isOnline() != online)
            {
                broadcast(tools.packet.FamilyPacket.familyLoggedIn(online, mgc.getName()), chrId, mgc.getId() == this.leaderid ? null : mgc.getPedigree());
            }
            mgc.setOnline(online);
            mgc.setChannel((byte) channel);
        }
        this.bDirty = true;
    }

    public void broadcast(byte[] packet, int exception, List<Integer> chrIds)
    {
        broadcast(packet, exception, FCOp.NONE, chrIds);
    }

    public boolean isProper()
    {
        return this.proper;
    }

    public int getLeaderId()
    {
        return this.leaderid;
    }

    public String getNotice()
    {
        if (this.notice == null)
        {
            return "";
        }
        return this.notice;
    }

    public void setNotice(String notice)
    {
        this.changed = true;
        this.notice = notice;
    }

    public String getLeaderName()
    {
        return this.leadername;
    }

    public void broadcast(byte[] packet, List<Integer> chrIds)
    {
        broadcast(packet, -1, FCOp.NONE, chrIds);
    }

    public int setRep(int chrId, int addrep, int oldLevel, String oldName)
    {
        MapleFamilyCharacter mgc = getMFC(chrId);
        if ((mgc != null) && (mgc.getFamilyId() == this.id))
        {
            if (oldLevel > mgc.getLevel())
            {
                addrep /= 2;
            }
            if (mgc.isOnline())
            {
                List<Integer> dummy = new ArrayList<>();
                dummy.add(mgc.getId());
                broadcast(tools.packet.FamilyPacket.changeRep(addrep, oldName), -1, dummy);
                WorldFamilyService.getInstance().setFamily(this.id, mgc.getSeniorId(), mgc.getJunior1(), mgc.getJunior2(), mgc.getCurrentRep() + addrep, mgc.getTotalRep() + addrep, mgc.getId());
            }
            else
            {
                mgc.setCurrentRep(mgc.getCurrentRep() + addrep);
                mgc.setTotalRep(mgc.getTotalRep() + addrep);
                setOfflineFamilyStatus(this.id, mgc.getSeniorId(), mgc.getJunior1(), mgc.getJunior2(), mgc.getCurrentRep(), mgc.getTotalRep(), mgc.getId());
            }
            return mgc.getSeniorId();
        }
        return 0;
    }

    public MapleFamilyCharacter addFamilyMemberInfo(MapleCharacter player, int seniorid, int junior1, int junior2)
    {
        MapleFamilyCharacter ret = new MapleFamilyCharacter(player, this.id, seniorid, junior1, junior2);
        this.members.put(player.getId(), ret);
        ret.resetPedigree(this);
        this.bDirty = true;
        List<Integer> toRemove = new ArrayList<>();
        for (int i = 0; i < ret.getPedigree().size(); i++)
        {
            if (ret.getPedigree().get(i) != ret.getId())
            {

                MapleFamilyCharacter mfc = getMFC(ret.getPedigree().get(i));
                if (mfc == null)
                {
                    toRemove.add(i);
                }
                else mfc.resetPedigree(this);
            }
        }
        for (int value : toRemove)
        {
            ret.getPedigree().remove(value);
        }
        return ret;
    }

    public int addFamilyMember(MapleFamilyCharacter mgc)
    {
        mgc.setFamilyId(this.id);
        this.members.put(mgc.getId(), mgc);
        mgc.resetPedigree(this);
        this.bDirty = true;
        for (Integer integer : mgc.getPedigree())
        {
            int i = integer;
            getMFC(i).resetPedigree(this);
        }
        return 1;
    }

    public void leaveFamily(int chrId)
    {
        leaveFamily(getMFC(chrId), true);
    }

    public void leaveFamily(MapleFamilyCharacter mgc, boolean skipLeader)
    {
        this.bDirty = true;
        if ((mgc.getId() == this.leaderid) && (!skipLeader))
        {
            this.leadername = null;
            WorldFamilyService.getInstance().disbandFamily(this.id);
        }
        else
        {
            if (mgc.getJunior1() > 0)
            {
                MapleFamilyCharacter j = getMFC(mgc.getJunior1());
                if (j != null)
                {
                    j.setSeniorId(0);
                    splitFamily(j.getId(), j);
                }
            }
            if (mgc.getJunior2() > 0)
            {
                MapleFamilyCharacter j = getMFC(mgc.getJunior2());
                if (j != null)
                {
                    j.setSeniorId(0);
                    splitFamily(j.getId(), j);
                }
            }
            if (mgc.getSeniorId() > 0)
            {
                MapleFamilyCharacter mfc = getMFC(mgc.getSeniorId());
                if (mfc != null)
                {
                    if (mfc.getJunior1() == mgc.getId())
                    {
                        mfc.setJunior1(0);
                    }
                    else
                    {
                        mfc.setJunior2(0);
                    }
                }
            }
            List<Integer> dummy = new ArrayList<>();
            dummy.add(mgc.getId());
            broadcast(null, -1, FCOp.DISBAND, dummy);
            resetPedigree();
        }
        this.members.remove(mgc.getId());
        this.bDirty = true;
    }

    public void memberLevelJobUpdate(MapleCharacter mgc)
    {
        MapleFamilyCharacter member = getMFC(mgc.getId());
        if (member != null)
        {
            int old_level = member.getLevel();
            int old_job = member.getJobId();
            member.setJobId(mgc.getJob());
            member.setLevel(mgc.getLevel());
            if (old_level != mgc.getLevel())
            {
                broadcast(tools.MaplePacketCreator.sendLevelup(true, mgc.getLevel(), mgc.getName()), mgc.getId(), mgc.getId() == this.leaderid ? null : member.getPedigree());
            }
            if (old_job != mgc.getJob())
            {
                broadcast(tools.MaplePacketCreator.sendJobup(true, mgc.getJob(), mgc.getName()), mgc.getId(), mgc.getId() == this.leaderid ? null : member.getPedigree());
            }
        }
    }

    public void disbandFamily()
    {
        writeToDB(true);
    }

    public int getMemberSize()
    {
        return this.members.size();
    }

    public boolean splitFamily(int splitId, MapleFamilyCharacter def)
    {
        MapleFamilyCharacter leader = getMFC(splitId);
        if (leader == null)
        {
            leader = def;
            if (leader == null)
            {
                return false;
            }
        }
        try
        {
            List<MapleFamilyCharacter> all = leader.getAllJuniors(this);
            if (all.size() <= 1)
            {
                leaveFamily(leader, false);
                return true;
            }
            int newId = createFamily(leader.getId());
            if (newId <= 0)
            {
                return false;
            }
            for (MapleFamilyCharacter mgc : all)
            {
                mgc.setFamilyId(newId);
                setOfflineFamilyStatus(newId, mgc.getSeniorId(), mgc.getJunior1(), mgc.getJunior2(), mgc.getCurrentRep(), mgc.getTotalRep(), mgc.getId());
                this.members.remove(mgc.getId());
            }
            MapleFamily newfam = WorldFamilyService.getInstance().getFamily(newId);
            for (MapleFamilyCharacter mgc : all)
            {
                if (mgc.isOnline())
                {
                    WorldFamilyService.getInstance().setFamily(newId, mgc.getSeniorId(), mgc.getJunior1(), mgc.getJunior2(), mgc.getCurrentRep(), mgc.getTotalRep(), mgc.getId());
                }
                newfam.setOnline(mgc.getId(), mgc.isOnline(), mgc.getChannel());
            }
        }
        finally
        {
            int newId;
            MapleFamilyCharacter mgc;
            MapleFamily newfam;
            if (this.members.size() <= 1)
            {
                WorldFamilyService.getInstance().disbandFamily(this.id);
                return true;
            }
        }
        this.bDirty = true;
        return false;
    }


    public enum FCOp
    {
        NONE, DISBAND;

        FCOp()
        {
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\family\MapleFamily.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */