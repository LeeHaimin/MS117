package handling.world;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleCoolDownValueHolder;
import client.MapleDiseaseValueHolder;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePet;
import client.status.MonsterStatusEffect;
import handling.channel.ChannelServer;
import server.Timer;
import server.life.MapleMonster;
import server.maps.MapleMap;
import server.maps.MapleMapItem;
import tools.MaplePacketCreator;

public class WorldRespawnService
{
    private static final int CHANNELS_PER_THREAD = 3;

    private WorldRespawnService()
    {
        Integer[] chs = ChannelServer.getAllInstance().toArray(new Integer[0]);
        for (int i = 0; i < chs.length; i += 3)
        {
            Timer.WorldTimer.getInstance().register(new Respawn(chs, i), 4500L);
        }
        System.out.println("[WorldRespawnService] 已经启动...");
    }

    public static WorldRespawnService getInstance()
    {
        return SingletonHolder.instance;
    }

    public static void handleMap(MapleMap map, int numTimes, int size, long now)
    {
        Iterator localIterator1;
        if (map.getItemsSize() > 0)
        {
            for (localIterator1 = map.getAllItemsThreadsafe().iterator(); localIterator1.hasNext(); )
            {
                MapleMapItem item = (MapleMapItem) localIterator1.next();
                if (item.shouldExpire(now))
                {
                    item.expire(map);
                }
                else if (item.shouldFFA(now))
                {
                    item.setDropType((byte) 2);
                }
            }
        }

        MapleMapItem item;

        if ((map.getCharactersSize() > 0) || (map.getId() == 931000500))
        {


            if (map.canSpawn(now))
            {
                map.respawn(false, now);
            }


            boolean hurt = map.canHurt(now);
            for (MapleCharacter chr : map.getCharactersThreadsafe())
            {
                handleCooldowns(chr, numTimes, hurt, now);
            }


            if (map.getMobsSize() > 0)
            {
                for (MapleMonster mons : map.getAllMonstersThreadsafe())
                {
                    if ((mons.isAlive()) && (mons.shouldKill(now)))
                    {
                        map.killMonster(mons);
                    }
                    else if ((mons.isAlive()) && (mons.shouldDrop(now)))
                    {
                        mons.doDropItem(now);
                    }
                    else if ((mons.isAlive()) && (mons.getStatiSize() > 0))
                    {
                        for (MonsterStatusEffect mse : mons.getAllBuffs())
                        {
                            if (mse.shouldCancel(now))
                            {
                                mons.cancelSingleStatus(mse);
                            }
                        }
                    }
                }
            }
        }

        MapleMonster mons;
    }

    public static void handleCooldowns(MapleCharacter chr, int numTimes, boolean hurt, long now)
    {
        if (chr == null)
        {
            return;
        }


        if (chr.getCooldownSize() > 0)
        {
            for (MapleCoolDownValueHolder m : chr.getCooldowns())
            {
                if (m.startTime + m.length < now)
                {
                    int skillId = m.skillId;
                    chr.removeCooldown(skillId);
                    chr.getClient().getSession().write(MaplePacketCreator.skillCooldown(skillId, 0));
                }
            }
        }


        if (chr.isAlive())
        {
            if (((chr.getJob() == 131) || (chr.getJob() == 132)) && (chr.canBlood(now)))
            {
                chr.doDragonBlood();
            }

            if (chr.canRecover(now))
            {
                chr.doRecovery();
            }
            if (chr.canRecoverEM(now))
            {
                chr.doRecoveryEM();
            }
            if (chr.canHPRecover(now))
            {
                chr.addHP((int) chr.getStat().getHealHP());
            }
            if (chr.canMPRecover(now))
            {
                chr.addMP((int) chr.getStat().getHealMP());
                if ((chr.getJob() == 3111) || (chr.getJob() == 3112))
                {
                    chr.addDemonMp((int) chr.getStat().getHealMP());
                }
            }

            if (chr.canRecoverPower(now))
            {
                chr.doRecoveryPower();
            }
            if (chr.canFairy(now))
            {
                chr.doFairy();
            }
            if (chr.canFish(now))
            {
                chr.doFish(now);
            }
            if (chr.canDOT(now))
            {
                chr.doDOT();
            }
            if (chr.canExpiration(now))
            {
                chr.expirationTask();
            }
            if (chr.canMorphLost(now))
            {
                chr.morphLostTask();
            }
        }


        if (chr.getDiseaseSize() > 0)
        {
            for (MapleDiseaseValueHolder m : chr.getAllDiseases())
            {
                if ((m != null) && (m.startTime + m.length < now))
                {
                    chr.dispelDebuff(m.disease);
                }
            }
        }


        if ((numTimes % 7 == 0) && (chr.getMount() != null) && (chr.getMount().canTire(now)))
        {
            chr.getMount().increaseFatigue();
        }
        if (numTimes % 13 == 0)
        {
            chr.doFamiliarSchedule(now);
            for (MaplePet pet : chr.getSummonedPets())
            {
                if ((pet.getPetItemId() == 5000054) && (pet.getSecondsLeft() > 0))
                {
                    pet.setSecondsLeft(pet.getSecondsLeft() - 1);
                    if (pet.getSecondsLeft() <= 0)
                    {
                        chr.unequipSpawnPet(pet, true, true);
                        return;
                    }
                }
                int newFullness = pet.getFullness() - client.inventory.PetDataFactory.getHunger(pet.getPetItemId());
                if (newFullness <= 5)
                {
                    pet.setFullness(15);
                    chr.unequipSpawnPet(pet, true, true);
                }
                else
                {
                    pet.setFullness(newFullness);
                    chr.getClient().getSession().write(tools.packet.PetPacket.updatePet(pet, chr.getInventory(MapleInventoryType.CASH).getItem(pet.getInventoryPosition()), true));
                }
            }
        }
        if ((hurt) && (chr.isAlive()))
        {


            if (chr.getInventory(MapleInventoryType.EQUIPPED).findById(chr.getMap().getHPDecProtect()) == null)
            {
                if ((chr.getMapId() == 749040100) && (chr.getInventory(MapleInventoryType.CASH).findById(5451000) == null))
                {
                    chr.addHP(-chr.getMap().getHPDec());
                }
                else if (chr.getMapId() != 749040100)
                {
                    chr.addHP(-(chr.getMap().getHPDec() - (chr.getBuffedValue(MapleBuffStat.HP_LOSS_GUARD) == null ? 0 : chr.getBuffedValue(MapleBuffStat.HP_LOSS_GUARD))));
                }
            }
        }
    }

    private static class Respawn implements Runnable
    {
        private final List<ChannelServer> cservs = new ArrayList(3);
        private int numTimes = 0;

        public Respawn(Integer[] chs, int c)
        {
            StringBuilder s = new StringBuilder("[Respawn Worker] Registered for channels ");
            for (int i = 1; (i <= 3) && (chs.length >= c + i); i++)
            {
                this.cservs.add(ChannelServer.getInstance(c + i));
                s.append(c + i).append(" ");
            }
            System.out.println(s.toString());
        }

        public void run()
        {
            this.numTimes += 1;
            long now = System.currentTimeMillis();
            for (ChannelServer cserv : this.cservs)
            {
                if (!cserv.hasFinishedShutdown())
                {
                    for (MapleMap map : cserv.getMapFactory().getAllLoadedMaps())
                    {
                        WorldRespawnService.handleMap(map, this.numTimes, map.getCharactersSize(), now);
                    }
                }
            }
        }
    }

    private static class SingletonHolder
    {
        protected static final WorldRespawnService instance = new WorldRespawnService();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\WorldRespawnService.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */