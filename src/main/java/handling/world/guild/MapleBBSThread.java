package handling.world.guild;

import java.io.Serializable;
import java.util.HashMap;

public class MapleBBSThread implements Serializable
{
    public static final long serialVersionUID = 3565477792085301248L;
    public final java.util.Map<Integer, MapleBBSReply> replies = new HashMap<>();
    public String name;
    public String text;
    public long timestamp;
    public int localthreadID;
    public int guildID;
    public int ownerID;
    public int icon;

    public MapleBBSThread(int localthreadID, String name, String text, long timestamp, int guildID, int ownerID, int icon)
    {
        this.localthreadID = localthreadID;
        this.name = name;
        this.text = text;
        this.timestamp = timestamp;
        this.guildID = guildID;
        this.ownerID = ownerID;
        this.icon = icon;
    }

    public int getReplyCount()
    {
        return this.replies.size();
    }

    public boolean isNotice()
    {
        return this.localthreadID == 0;
    }

    public void setLocalthreadID(int id)
    {
        this.localthreadID = id;
    }

    public void setTitle(String name)
    {
        this.name = name;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void setTimestamp(long timestamp)
    {
        this.timestamp = timestamp;
    }

    public void setGuildId(int guildID)
    {
        this.guildID = guildID;
    }

    public void setOwnerID(int ownerID)
    {
        this.ownerID = ownerID;
    }

    public void setIcon(int icon)
    {
        this.icon = icon;
    }

    public static class ThreadComparator implements java.util.Comparator<MapleBBSThread>, Serializable
    {
        public int compare(MapleBBSThread o1, MapleBBSThread o2)
        {
            if (o1.localthreadID < o2.localthreadID) return 1;
            if (o1.localthreadID == o2.localthreadID)
            {
                return 0;
            }
            return -1;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\guild\MapleBBSThread.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */