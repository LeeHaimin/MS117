package handling.world.guild;

import java.io.Serializable;


public class MapleBBSReply implements Serializable
{
    public final int replyid;
    public final int ownerID;
    public final long timestamp;
    public final String content;

    public MapleBBSReply(int replyid, int ownerID, String content, long timestamp)
    {
        this.ownerID = ownerID;
        this.replyid = replyid;
        this.content = content;
        this.timestamp = timestamp;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\guild\MapleBBSReply.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */