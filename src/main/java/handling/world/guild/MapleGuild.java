package handling.world.guild;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import client.MapleClient;
import database.DatabaseConnection;
import handling.world.WorldAllianceService;
import handling.world.WorldGuildService;
import server.MapleStatEffect;
import tools.data.output.MaplePacketLittleEndianWriter;
import tools.packet.GuildPacket;

public class MapleGuild implements java.io.Serializable
{
    public static final long serialVersionUID = 6322150443228168192L;
    private static final Logger log = Logger.getLogger(MapleGuild.class);
    private final List<MapleGuildCharacter> members = new CopyOnWriteArrayList();
    private final List<MapleGuildCharacter> applyMembers = new ArrayList<>();
    private final Map<Integer, MapleGuildSkill> guildSkills = new HashMap<>();
    private final String[] rankTitles = new String[5];
    private final Map<Integer, MapleBBSThread> bbs = new HashMap<>();
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final int[] guildExp = {0, 15000, 60000, 135000, 240000, 375000, 540000, 735000, 960000, 1215000, 1500000, 1815000, 2160000, 2535000, 2940000, 3375000, 3840000, 4335000, 4860000,
            5415000, 6000000, 6615000, 7260000, 7935000, 8640000};
    private String name;
    private String notice;
    private int id;
    private int gp;
    private int contribution;
    private int logo;
    private int logoColor;
    private int leader;
    private int capacity;
    private int logoBG;
    private int logoBGColor;
    private int signature;
    private int level;
    private boolean bDirty = true;
    private boolean proper = true;
    private int allianceid = 0;
    private int invitedid = 0;
    private boolean init = false;
    private boolean changed = false;
    private boolean changed_skills = false;

    public MapleGuild(int guildid)
    {
        this(guildid, null);
    }

    public MapleGuild(int guildid, Map<Integer, Map<Integer, MapleBBSReply>> replies)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM guilds WHERE guildid = ?");
            ps.setInt(1, guildid);
            ResultSet rs = ps.executeQuery();
            if (!rs.first())
            {
                rs.close();
                ps.close();
                this.id = -1;
                return;
            }
            this.id = guildid;
            this.name = rs.getString("name");
            this.gp = rs.getInt("GP");
            this.logo = rs.getInt("logo");
            this.logoColor = rs.getInt("logoColor");
            this.logoBG = rs.getInt("logoBG");
            this.logoBGColor = rs.getInt("logoBGColor");
            this.capacity = rs.getInt("capacity");
            this.rankTitles[0] = rs.getString("rank1title");
            this.rankTitles[1] = rs.getString("rank2title");
            this.rankTitles[2] = rs.getString("rank3title");
            this.rankTitles[3] = rs.getString("rank4title");
            this.rankTitles[4] = rs.getString("rank5title");
            this.leader = rs.getInt("leader");
            this.notice = rs.getString("notice");
            this.signature = rs.getInt("signature");
            this.allianceid = rs.getInt("alliance");
            rs.close();
            ps.close();

            MapleGuildAlliance alliance = WorldAllianceService.getInstance().getAlliance(this.allianceid);
            if (alliance == null)
            {
                this.allianceid = 0;
            }

            ps = con.prepareStatement("SELECT id, name, level, job, guildrank, guildContribution, alliancerank FROM characters WHERE guildid = ? ORDER BY guildrank ASC, name ASC", 1008);
            ps.setInt(1, guildid);
            rs = ps.executeQuery();
            if (!rs.first())
            {
                System.err.println("家族ID: " + this.id + " 没用成员，系统自动解散该家族。");
                rs.close();
                ps.close();
                writeToDB(true);
                this.proper = false;
                return;
            }
            boolean leaderCheck = false;
            byte gFix = 0;
            byte aFix = 0;
            do
            {
                int chrId = rs.getInt("id");
                byte gRank = rs.getByte("guildrank");
                byte aRank = rs.getByte("alliancerank");

                if (chrId == this.leader)
                {
                    leaderCheck = true;
                    if (gRank != 1)
                    {
                        gRank = 1;
                        gFix = 1;
                    }
                    if (alliance != null)
                    {
                        if ((alliance.getLeaderId() == chrId) && (aRank != 1))
                        {
                            aRank = 1;
                            aFix = 1;
                        }
                        else if ((alliance.getLeaderId() != chrId) && (aRank != 2))
                        {
                            aRank = 2;
                            aFix = 2;
                        }
                    }
                }
                else
                {
                    if (gRank == 1)
                    {
                        gRank = 2;
                        gFix = 2;
                    }
                    if (aRank < 3)
                    {
                        aRank = 3;
                        aFix = 3;
                    }
                }
                this.members.add(new MapleGuildCharacter(chrId, rs.getShort("level"), rs.getString("name"), (byte) -1, rs.getInt("job"), gRank, rs.getInt("guildContribution"), aRank, guildid, false));
            }
            while (rs.next());
            rs.close();
            ps.close();

            if (!leaderCheck)
            {
                System.err.println("族长[ " + this.leader + " ]没有在家族ID为 " + this.id + " 的家族中，系统自动解散这个家族。");
                writeToDB(true);
                this.proper = false;
                return;
            }

            if (gFix > 0)
            {
                ps = con.prepareStatement("UPDATE characters SET guildrank = ? WHERE id = ?");
                ps.setByte(1, gFix);
                ps.setInt(2, this.leader);
                ps.executeUpdate();
                ps.close();
            }

            if (aFix > 0)
            {
                ps = con.prepareStatement("UPDATE characters SET alliancerank = ? WHERE id = ?");
                ps.setByte(1, aFix);
                ps.setInt(2, this.leader);
                ps.executeUpdate();
                ps.close();
            }

            ps = con.prepareStatement("SELECT * FROM bbs_threads WHERE guildid = ? ORDER BY localthreadid DESC");
            ps.setInt(1, guildid);
            rs = ps.executeQuery();
            while (rs.next())
            {
                int tID = rs.getInt("localthreadid");
                MapleBBSThread thread = new MapleBBSThread(tID, rs.getString("name"), rs.getString("startpost"), rs.getLong("timestamp"), guildid, rs.getInt("postercid"), rs.getInt("icon"));
                if ((replies != null) && (replies.containsKey(rs.getInt("threadid"))))
                {
                    thread.replies.putAll(replies.get(rs.getInt("threadid")));
                }
                this.bbs.put(tID, thread);
            }
            rs.close();
            ps.close();

            ps = con.prepareStatement("SELECT * FROM guildskills WHERE guildid = ?");
            ps.setInt(1, guildid);
            rs = ps.executeQuery();
            while (rs.next())
            {
                int skillId = rs.getInt("skillid");
                if (skillId < 91000000)
                {
                    rs.close();
                    ps.close();
                    System.err.println("非家族技能ID: " + skillId + " 在家族ID为 " + this.id + " 的家族中，系统自动解散该家族。");
                    writeToDB(true);
                    this.proper = false;
                    return;
                }
                this.guildSkills.put(skillId, new MapleGuildSkill(skillId, rs.getInt("level"), rs.getLong("timestamp"), rs.getString("purchaser"), ""));
            }
            rs.close();
            ps.close();
            this.level = calculateLevel();
        }
        catch (SQLException se)
        {
            log.error("[MapleGuild] 从数据库中加载家族信息出错." + se);
        }
    }

    public final void writeToDB(boolean bDisband)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            if (!bDisband)
            {
                StringBuilder buf = new StringBuilder("UPDATE guilds SET GP = ?, logo = ?, logoColor = ?, logoBG = ?, logoBGColor = ?, ");
                for (int i = 1; i < 6; i++)
                {
                    buf.append("rank").append(i).append("title = ?, ");
                }
                buf.append("capacity = ?, notice = ?, alliance = ?, leader = ? WHERE guildid = ?");

                PreparedStatement ps = con.prepareStatement(buf.toString());
                ps.setInt(1, this.gp);
                ps.setInt(2, this.logo);
                ps.setInt(3, this.logoColor);
                ps.setInt(4, this.logoBG);
                ps.setInt(5, this.logoBGColor);
                ps.setString(6, this.rankTitles[0]);
                ps.setString(7, this.rankTitles[1]);
                ps.setString(8, this.rankTitles[2]);
                ps.setString(9, this.rankTitles[3]);
                ps.setString(10, this.rankTitles[4]);
                ps.setInt(11, this.capacity);
                ps.setString(12, this.notice);
                ps.setInt(13, this.allianceid);
                ps.setInt(14, this.leader);
                ps.setInt(15, this.id);
                ps.executeUpdate();
                ps.close();
                PreparedStatement pse;
                if (this.changed)
                {
                    ps = con.prepareStatement("DELETE FROM bbs_threads WHERE guildid = ?");
                    ps.setInt(1, this.id);
                    ps.execute();
                    ps.close();

                    ps = con.prepareStatement("DELETE FROM bbs_replies WHERE guildid = ?");
                    ps.setInt(1, this.id);
                    ps.execute();
                    ps.close();

                    pse = con.prepareStatement("INSERT INTO bbs_replies (`threadid`, `postercid`, `timestamp`, `content`, `guildid`) VALUES (?, ?, ?, ?, ?)");
                    ps = con.prepareStatement("INSERT INTO bbs_threads(`postercid`, `name`, `timestamp`, `icon`, `startpost`, `guildid`, `localthreadid`) VALUES(?, ?, ?, ?, ?, ?, ?)", 1);
                    ps.setInt(6, this.id);
                    for (MapleBBSThread bb : this.bbs.values())
                    {
                        int ourId;
                        ps.setInt(1, bb.ownerID);
                        ps.setString(2, bb.name);
                        ps.setLong(3, bb.timestamp);
                        ps.setInt(4, bb.icon);
                        ps.setString(5, bb.text);
                        ps.setInt(7, bb.localthreadID);
                        ps.execute();
                        ResultSet rs = ps.getGeneratedKeys();
                        if (!rs.next())
                        {
                            rs.close();
                        }
                        else
                        {
                            ourId = rs.getInt(1);
                            rs.close();
                            pse.setInt(5, this.id);
                            for (MapleBBSReply r : bb.replies.values())
                            {
                                pse.setInt(1, ourId);
                                pse.setInt(2, r.ownerID);
                                pse.setLong(3, r.timestamp);
                                pse.setString(4, r.content);
                                pse.addBatch();
                            }
                        }
                    }
                    pse.executeBatch();
                    pse.close();
                    ps.close();
                }
                if (this.changed_skills)
                {
                    ps = con.prepareStatement("DELETE FROM guildskills WHERE guildid = ?");
                    ps.setInt(1, this.id);
                    ps.execute();
                    ps.close();

                    ps = con.prepareStatement("INSERT INTO guildskills(`guildid`, `skillid`, `level`, `timestamp`, `purchaser`) VALUES(?, ?, ?, ?, ?)");
                    ps.setInt(1, this.id);
                    for (MapleGuildSkill i : this.guildSkills.values())
                    {
                        ps.setInt(2, i.skillID);
                        ps.setByte(3, (byte) i.level);
                        ps.setLong(4, i.timestamp);
                        ps.setString(5, i.purchaser);
                        ps.execute();
                    }
                    ps.close();
                }
                this.changed_skills = false;
                this.changed = false;
            }
            else
            {
                PreparedStatement ps = con.prepareStatement("DELETE FROM bbs_threads WHERE guildid = ?");
                ps.setInt(1, this.id);
                ps.execute();
                ps.close();


                ps = con.prepareStatement("DELETE FROM bbs_replies WHERE guildid = ?");
                ps.setInt(1, this.id);
                ps.execute();
                ps.close();

                ps = con.prepareStatement("DELETE FROM guildskills WHERE guildid = ?");
                ps.setInt(1, this.id);
                ps.execute();
                ps.close();

                ps = con.prepareStatement("DELETE FROM guilds WHERE guildid = ?");
                ps.setInt(1, this.id);
                ps.executeUpdate();
                ps.close();


                if (this.allianceid > 0)
                {
                    MapleGuildAlliance alliance = WorldAllianceService.getInstance().getAlliance(this.allianceid);
                    if (alliance != null)
                    {
                        alliance.removeGuild(this.id, false);
                    }
                }

                broadcast(GuildPacket.guildDisband(this.id));
            }
        }
        catch (SQLException se)
        {
            log.error("[MapleGuild] 保存家族信息出错." + se);
        }
    }

    public final int calculateLevel()
    {
        for (int i = 1; i < 10; i++)
        {
            if (this.gp < getGuildExpNeededForLevel(i))
            {
                return i;
            }
        }
        return 10;
    }

    public void broadcast(byte[] packet)
    {
        broadcast(packet, -1, BCOp.NONE);
    }

    public int getGuildExpNeededForLevel(int levelx)
    {
        if ((levelx < 0) || (levelx >= this.guildExp.length))
        {
            return Integer.MAX_VALUE;
        }
        return this.guildExp[levelx];
    }

    public void broadcast(byte[] packet, int exceptionId, BCOp bcop)
    {
        this.lock.writeLock().lock();
        try
        {
            buildNotifications();
        }
        finally
        {
            this.lock.writeLock().unlock();
        }

        this.lock.readLock().lock();
        try
        {
            for (MapleGuildCharacter mgc : this.members)
            {
                if (bcop == BCOp.DISBAND)
                {
                    if (mgc.isOnline())
                    {
                        WorldGuildService.getInstance().setGuildAndRank(mgc.getId(), 0, 5, 0, 5);
                    }
                    else
                    {
                        setOfflineGuildStatus(0, (byte) 5, 0, (byte) 5, mgc.getId());
                    }
                }
                else if ((mgc.isOnline()) && (mgc.getId() != exceptionId))
                {
                    if (bcop == BCOp.EMBELMCHANGE)
                    {
                        WorldGuildService.getInstance().changeEmblem(this.id, mgc.getId(), this);
                    }
                    else
                    {
                        handling.world.WorldBroadcastService.getInstance().sendGuildPacket(mgc.getId(), packet, exceptionId, this.id);
                    }
                }
            }
        }
        finally
        {
            this.lock.readLock().unlock();
        }
    }

    private void buildNotifications()
    {
        if (!this.bDirty)
        {
            return;
        }
        List<Integer> mem = new LinkedList<>();
        for (MapleGuildCharacter mgc : this.members)
        {
            if (mgc.isOnline())
            {

                if ((mem.contains(mgc.getId())) || (mgc.getGuildId() != this.id))
                {
                    this.members.remove(mgc);
                }
                else mem.add(mgc.getId());
            }
        }
        this.bDirty = false;
    }

    public static void setOfflineGuildStatus(int guildId, byte guildrank, int contribution, byte alliancerank, int chrId)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE characters SET guildid = ?, guildrank = ?, guildContribution = ?, alliancerank = ? WHERE id = ?");
            ps.setInt(1, guildId);
            ps.setInt(2, guildrank);
            ps.setInt(3, contribution);
            ps.setInt(4, alliancerank);
            ps.setInt(5, chrId);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException se)
        {
            System.out.println("SQLException: " + se.getLocalizedMessage());
        }
    }

    public static void loadAll()
    {
        Map<Integer, Map<Integer, MapleBBSReply>> replies = new LinkedHashMap<>();
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM bbs_replies");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                int tID = rs.getInt("threadid");
                Map<Integer, MapleBBSReply> reply = replies.get(tID);
                if (reply == null)
                {
                    reply = new HashMap<>();
                    replies.put(tID, reply);
                }
                reply.put(reply.size(), new MapleBBSReply(reply.size(), rs.getInt("postercid"), rs.getString("content"), rs.getLong("timestamp")));
            }
            rs.close();
            ps.close();
            ps = con.prepareStatement("SELECT guildid FROM guilds");
            rs = ps.executeQuery();
            while (rs.next())
            {
                WorldGuildService.getInstance().addLoadedGuild(new MapleGuild(rs.getInt("guildid"), replies));
            }
            rs.close();
            ps.close();
        }
        catch (SQLException se)
        {
            log.error("[MapleGuild] 从数据库中加载家族信息出错." + se);
        }
    }

    public static void loadAll(Object toNotify)
    {
        Map<Integer, Map<Integer, MapleBBSReply>> replies = new LinkedHashMap<>();
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM bbs_replies");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                int tID = rs.getInt("threadid");
                Map<Integer, MapleBBSReply> reply = replies.get(tID);
                if (reply == null)
                {
                    reply = new HashMap<>();
                    replies.put(tID, reply);
                }
                reply.put(reply.size(), new MapleBBSReply(reply.size(), rs.getInt("postercid"), rs.getString("content"), rs.getLong("timestamp")));
            }
            rs.close();
            ps.close();
            boolean cont = false;
            ps = con.prepareStatement("SELECT guildid FROM guilds");
            rs = ps.executeQuery();
            while (rs.next())
            {
                GuildLoad.QueueGuildForLoad(rs.getInt("guildid"), replies);
                cont = true;
            }
            rs.close();
            ps.close();
            if (!cont)
            {
                return;
            }
        }
        catch (SQLException se)
        {
            log.error("[MapleGuild] 从数据库中加载家族信息出错." + se);
        }
        java.util.concurrent.atomic.AtomicInteger FinishedThreads = new AtomicInteger(0);
        GuildLoad.Execute(toNotify);
        synchronized (toNotify)
        {
            try
            {
                toNotify.wait();
            }
            catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
        }
        while (FinishedThreads.incrementAndGet() != 6)
        {
            synchronized (toNotify)
            {
                try
                {
                    toNotify.wait();
                }
                catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static int createGuild(int leaderId, String name)
    {
        if (name.length() > 12)
        {
            return 0;
        }
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT guildid FROM guilds WHERE name = ?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();

            if (rs.first())
            {
                rs.close();
                ps.close();
                return 0;
            }
            ps.close();
            rs.close();

            ps = con.prepareStatement("INSERT INTO guilds (`leader`, `name`, `signature`, `alliance`) VALUES (?, ?, ?, 0)", 1);
            ps.setInt(1, leaderId);
            ps.setString(2, name);
            ps.setInt(3, (int) (System.currentTimeMillis() / 1000L));
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            int ret = 0;
            if (rs.next())
            {
                ret = rs.getInt(1);
            }
            rs.close();
            ps.close();
            return ret;
        }
        catch (SQLException se)
        {
            log.error("[MapleGuild] 创建家族信息出错." + se);
        }
        return 0;
    }

    public static MapleGuildResponse sendInvite(MapleClient c, String targetName)
    {
        MapleCharacter target = c.getChannelServer().getPlayerStorage().getCharacterByName(targetName);
        if (target == null)
        {
            return MapleGuildResponse.找不到角色;
        }
        if (target.getGuildId() > 0)
        {
            return MapleGuildResponse.已经有家族;
        }
        c.getSession().write(tools.MaplePacketCreator.showQuestMessage("已邀请'" + targetName + "'加入公会。"));
        target.getClient().getSession().write(GuildPacket.guildInvite(c.getPlayer().getGuildId(), c.getPlayer().getName(), c.getPlayer().getLevel(), c.getPlayer().getJob()));
        return null;
    }

    public boolean isProper()
    {
        return this.proper;
    }

    public int getId()
    {
        return this.id;
    }

    public int getLeaderId()
    {
        return this.leader;
    }

    public MapleCharacter getLeader(MapleClient c)
    {
        return c.getChannelServer().getPlayerStorage().getCharacterById(this.leader);
    }

    public int getGP()
    {
        return this.gp;
    }

    public int getGontribution()
    {
        return this.contribution;
    }

    public int getLogo()
    {
        return this.logo;
    }

    public void setLogo(int l)
    {
        this.logo = l;
    }

    public int getLogoColor()
    {
        return this.logoColor;
    }

    public void setLogoColor(int c)
    {
        this.logoColor = c;
    }

    public int getLogoBG()
    {
        return this.logoBG;
    }

    public void setLogoBG(int bg)
    {
        this.logoBG = bg;
    }

    public int getLogoBGColor()
    {
        return this.logoBGColor;
    }

    public void setLogoBGColor(int c)
    {
        this.logoBGColor = c;
    }

    public String getNotice()
    {
        if (this.notice == null)
        {
            return "";
        }
        return this.notice;
    }

    public String getName()
    {
        return this.name;
    }

    public int getCapacity()
    {
        return this.capacity;
    }

    public int getSignature()
    {
        return this.signature;
    }

    public void setOnline(int chrId, boolean online, int channel)
    {
        boolean isBroadcast = true;
        for (MapleGuildCharacter mgc : this.members)
        {
            if ((mgc.getGuildId() == this.id) && (mgc.getId() == chrId))
            {
                if (mgc.isOnline() == online)
                {
                    isBroadcast = false;
                }
                mgc.setOnline(online);
                mgc.setChannel((byte) channel);
                break;
            }
        }
        if (isBroadcast)
        {
            broadcast(GuildPacket.guildMemberOnline(this.id, chrId, online), chrId);
            if (this.allianceid > 0)
            {
                WorldAllianceService.getInstance().sendGuild(GuildPacket.allianceMemberOnline(this.allianceid, this.id, chrId, online), this.id, this.allianceid);
            }
        }
        this.bDirty = true;
        this.init = true;
    }

    public void broadcast(byte[] packet, int exception)
    {
        broadcast(packet, exception, BCOp.NONE);
    }

    public void guildChat(String name, int chrId, String msg)
    {
        broadcast(tools.MaplePacketCreator.multiChat(name, msg, 2), chrId);
    }

    public void allianceChat(String name, int chrId, String msg)
    {
        broadcast(tools.MaplePacketCreator.multiChat(name, msg, 3), chrId);
    }

    public String getRankTitle(int rank)
    {
        return this.rankTitles[(rank - 1)];
    }

    public int getAllianceId()
    {
        return this.allianceid;
    }

    public void setAllianceId(int a)
    {
        this.allianceid = a;
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE guilds SET alliance = ? WHERE guildid = ?");
            ps.setInt(1, a);
            ps.setInt(2, this.id);
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            log.error("[MapleGuild] 保存家族联盟信息出错." + e);
        }
    }

    public int getInvitedId()
    {
        return this.invitedid;
    }

    public void setInvitedId(int iid)
    {
        this.invitedid = iid;
    }

    public int addGuildMember(MapleGuildCharacter newMember)
    {
        this.lock.writeLock().lock();
        try
        {
            if (this.members.size() >= this.capacity)
            {
                return 0;
            }
            for (int i = this.members.size() - 1; i >= 0; i--)
            {
                if ((this.members.get(i).getGuildRank() < 5) || (this.members.get(i).getName().compareTo(newMember.getName()) < 0))
                {
                    this.members.add(i + 1, newMember);
                    this.bDirty = true;
                    break;
                }
            }

            Object itr = this.applyMembers.iterator();
            while (((Iterator) itr).hasNext())
            {
                MapleGuildCharacter mgcc = (MapleGuildCharacter) ((Iterator) itr).next();
                if (mgcc.getId() == newMember.getId())
                {
                    this.applyMembers.remove(mgcc);
                    break;
                }
            }
        }
        finally
        {
            this.lock.writeLock().unlock();
        }
        gainGP(500, true, newMember.getId());
        broadcast(GuildPacket.newGuildMember(newMember, false));
        if (this.allianceid > 0)
        {
            WorldAllianceService.getInstance().sendGuild(this.allianceid);
        }
        return 1;
    }

    public int addGuildMember(int chrId)
    {
        MapleGuildCharacter newMember = getApplyMGC(chrId);
        if (newMember == null)
        {
            return 0;
        }
        this.applyMembers.remove(newMember);

        this.lock.writeLock().lock();
        try
        {
            if (this.members.size() >= this.capacity)
            {
                return 0;
            }
            for (int i = this.members.size() - 1; i >= 0; i--)
            {
                if ((this.members.get(i).getGuildRank() < 5) || (this.members.get(i).getName().compareTo(newMember.getName()) < 0))
                {
                    this.members.add(i + 1, newMember);
                    this.bDirty = true;
                    break;
                }
            }
        }
        finally
        {
            this.lock.writeLock().unlock();
        }
        gainGP(500, true, newMember.getId());
        broadcast(GuildPacket.newGuildMember(newMember, false));
        if (this.allianceid > 0)
        {
            WorldAllianceService.getInstance().sendGuild(this.allianceid);
        }
        return 1;
    }

    public int addGuildApplyMember(MapleGuildCharacter applyMember)
    {
        if (getApplyMGC(applyMember.getId()) != null)
        {
            return 0;
        }
        this.applyMembers.add(applyMember);
        broadcast(GuildPacket.newGuildMember(applyMember, true));
        return 1;
    }

    public MapleGuildCharacter getApplyMGC(int chrId)
    {
        for (MapleGuildCharacter mgc : this.applyMembers)
        {
            if (mgc.getId() == chrId)
            {
                return mgc;
            }
        }
        return null;
    }

    public int denyGuildApplyMember(int fromId)
    {
        MapleGuildCharacter applyMember = getApplyMGC(fromId);
        if (applyMember == null)
        {
            return 0;
        }
        this.applyMembers.remove(applyMember);
        return 1;
    }

    public void leaveGuild(MapleGuildCharacter mgc)
    {
        this.lock.writeLock().lock();
        try
        {
            for (MapleGuildCharacter mgcc : this.members)
            {
                if (mgcc.getId() == mgc.getId())
                {
                    broadcast(GuildPacket.memberLeft(mgcc, false));
                    this.bDirty = true;
                    gainGP(mgcc.getGuildContribution() > 0 ? -mgcc.getGuildContribution() : 65036);
                    this.members.remove(mgcc);
                    if (mgc.isOnline())
                    {
                        WorldGuildService.getInstance().setGuildAndRank(mgcc.getId(), 0, 5, 0, 5);
                        break;
                    }
                    setOfflineGuildStatus(0, (byte) 5, 0, (byte) 5, mgcc.getId());

                    break;
                }
            }
        }
        finally
        {
            this.lock.writeLock().unlock();
        }
        if ((this.bDirty) && (this.allianceid > 0))
        {
            WorldAllianceService.getInstance().sendGuild(this.allianceid);
        }
    }

    public void expelMember(MapleGuildCharacter initiator, String name, int chrId)
    {
        this.lock.writeLock().lock();
        try
        {
            for (MapleGuildCharacter mgc : this.members)
            {
                if ((mgc.getId() == chrId) && (initiator.getGuildRank() < mgc.getGuildRank()))
                {
                    broadcast(GuildPacket.memberLeft(mgc, true));
                    this.bDirty = true;
                    gainGP(mgc.getGuildContribution() > 0 ? -mgc.getGuildContribution() : 65036);
                    if (mgc.isOnline())
                    {
                        WorldGuildService.getInstance().setGuildAndRank(chrId, 0, 5, 0, 5);
                    }
                    else
                    {
                        client.MapleCharacterUtil.sendNote(mgc.getName(), initiator.getName(), "被家族除名了。", 0);
                        setOfflineGuildStatus(0, (byte) 5, 0, (byte) 5, chrId);
                    }
                    this.members.remove(mgc);
                    break;
                }
            }
        }
        finally
        {
            this.lock.writeLock().unlock();
        }
        if ((this.bDirty) && (this.allianceid > 0))
        {
            WorldAllianceService.getInstance().sendGuild(this.allianceid);
        }
    }

    public void changeARank()
    {
        changeARank(false);
    }

    public void changeARank(boolean leader)
    {
        if (this.allianceid <= 0)
        {
            return;
        }
        for (MapleGuildCharacter mgc : this.members)
        {
            byte newRank = 3;
            if (this.leader == mgc.getId())
            {
                newRank = (byte) (leader ? 1 : 2);
            }
            if (mgc.isOnline())
            {
                WorldGuildService.getInstance().setGuildAndRank(mgc.getId(), this.id, mgc.getGuildRank(), mgc.getGuildContribution(), newRank);
            }
            else
            {
                setOfflineGuildStatus(this.id, mgc.getGuildRank(), mgc.getGuildContribution(), newRank, mgc.getId());
            }
            mgc.setAllianceRank(newRank);
        }
        WorldAllianceService.getInstance().sendGuild(this.allianceid);
    }

    public void changeARank(int newRank)
    {
        if (this.allianceid <= 0)
        {
            return;
        }
        for (MapleGuildCharacter mgc : this.members)
        {
            if (mgc.isOnline())
            {
                WorldGuildService.getInstance().setGuildAndRank(mgc.getId(), this.id, mgc.getGuildRank(), mgc.getGuildContribution(), newRank);
            }
            else
            {
                setOfflineGuildStatus(this.id, mgc.getGuildRank(), mgc.getGuildContribution(), (byte) newRank, mgc.getId());
            }
            mgc.setAllianceRank((byte) newRank);
        }
        WorldAllianceService.getInstance().sendGuild(this.allianceid);
    }

    public boolean changeARank(int chrId, int newRank)
    {
        if (this.allianceid <= 0)
        {
            return false;
        }
        for (MapleGuildCharacter mgc : this.members)
        {
            if (chrId == mgc.getId())
            {
                if (mgc.isOnline())
                {
                    WorldGuildService.getInstance().setGuildAndRank(chrId, this.id, mgc.getGuildRank(), mgc.getGuildContribution(), newRank);
                }
                else
                {
                    setOfflineGuildStatus(this.id, mgc.getGuildRank(), mgc.getGuildContribution(), (byte) newRank, chrId);
                }
                mgc.setAllianceRank((byte) newRank);
                WorldAllianceService.getInstance().sendGuild(this.allianceid);
                return true;
            }
        }
        return false;
    }

    public void changeGuildLeader(int newLeader)
    {
        if ((changeRank(newLeader, 1)) && (changeRank(this.leader, 2)))
        {
            if (this.allianceid > 0)
            {
                int aRank = getMGC(this.leader).getAllianceRank();
                if (aRank == 1)
                {
                    WorldAllianceService.getInstance().changeAllianceLeader(this.allianceid, newLeader, true);
                }
                else
                {
                    changeARank(newLeader, aRank);
                }
                changeARank(this.leader, 3);
            }
            broadcast(GuildPacket.guildLeaderChanged(this.id, this.leader, newLeader, this.allianceid));
            this.leader = newLeader;
            try
            {
                PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE guilds SET leader = ? WHERE guildid = ?");
                ps.setInt(1, newLeader);
                ps.setInt(2, this.id);
                ps.execute();
                ps.close();
            }
            catch (SQLException e)
            {
                log.error("[MapleGuild] Saving leaderid ERROR." + e);
            }
        }
    }

    public boolean changeRank(int chrId, int newRank)
    {
        for (MapleGuildCharacter mgc : this.members)
        {
            if (chrId == mgc.getId())
            {
                if (mgc.isOnline())
                {
                    WorldGuildService.getInstance().setGuildAndRank(chrId, this.id, newRank, mgc.getGuildContribution(), mgc.getAllianceRank());
                }
                else
                {
                    setOfflineGuildStatus(this.id, (byte) newRank, mgc.getGuildContribution(), mgc.getAllianceRank(), chrId);
                }
                mgc.setGuildRank((byte) newRank);
                broadcast(GuildPacket.changeRank(mgc));
                return true;
            }
        }
        return false;
    }

    public void setGuildNotice(String notice)
    {
        this.notice = notice;
        broadcast(GuildPacket.guildNotice(this.id, notice));
    }

    public void memberLevelJobUpdate(MapleGuildCharacter mgc)
    {
        for (MapleGuildCharacter member : this.members)
        {
            if (member.getId() == mgc.getId())
            {
                int old_level = member.getLevel();
                int old_job = member.getJobId();
                member.setJobId(mgc.getJobId());
                member.setLevel((short) mgc.getLevel());
                if (mgc.getLevel() > old_level)
                {
                    gainGP((mgc.getLevel() - old_level) * mgc.getLevel(), false, mgc.getId());
                }

                if (old_level != mgc.getLevel())
                {
                    broadcast(tools.MaplePacketCreator.sendLevelup(false, mgc.getLevel(), mgc.getName()), mgc.getId());
                }
                if (old_job != mgc.getJobId())
                {
                    broadcast(tools.MaplePacketCreator.sendJobup(false, mgc.getJobId(), mgc.getName()), mgc.getId());
                }
                broadcast(GuildPacket.guildMemberLevelJobUpdate(mgc));
                if (this.allianceid <= 0) break;
                WorldAllianceService.getInstance().sendGuild(GuildPacket.updateAlliance(mgc, this.allianceid), this.id, this.allianceid);
                break;
            }
        }
    }

    public void changeRankTitle(String[] ranks)
    {
        for (int i = 0; i < 5; i++)
        {
            this.rankTitles[i] = ranks[i];
        }
        broadcast(GuildPacket.rankTitleChange(this.id, ranks));
    }

    public void disbandGuild()
    {
        writeToDB(true);
        broadcast(null, -1, BCOp.DISBAND);
    }

    public void setGuildEmblem(short bg, byte bgcolor, short logo, byte logocolor)
    {
        this.logoBG = bg;
        this.logoBGColor = bgcolor;
        this.logo = logo;
        this.logoColor = logocolor;
        broadcast(null, -1, BCOp.EMBELMCHANGE);
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE guilds SET logo = ?, logoColor = ?, logoBG = ?, logoBGColor = ? WHERE guildid = ?");
            ps.setInt(1, logo);
            ps.setInt(2, this.logoColor);
            ps.setInt(3, this.logoBG);
            ps.setInt(4, this.logoBGColor);
            ps.setInt(5, this.id);
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            log.error("[MapleGuild] Saving guild logo / BG colo ERROR." + e);
        }
    }

    public boolean increaseCapacity(boolean trueMax)
    {
        if (this.capacity < (trueMax ? 200 : 100))
        {
            if (this.capacity + 5 <= (trueMax ? 200 : 100))
            {
            }
        }
        else
        {
            return false;
        }
        if ((trueMax) && (this.gp < 25000))
        {
            return false;
        }
        if ((trueMax) && (this.gp - 25000 < getGuildExpNeededForLevel(getLevel() - 1)))
        {
            return false;
        }
        this.capacity += 5;
        broadcast(GuildPacket.guildCapacityChange(this.id, this.capacity));
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE guilds SET capacity = ? WHERE guildid = ?");
            ps.setInt(1, this.capacity);
            ps.setInt(2, this.id);
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            log.error("[MapleGuild] Saving guild capacity ERROR." + e);
        }
        return true;
    }

    public int getLevel()
    {
        return this.level;
    }

    public void gainGP(int amount)
    {
        gainGP(amount, true, -1);
    }

    public void gainGP(int amount, boolean broadcast)
    {
        gainGP(amount, broadcast, -1);
    }

    public void gainGP(int amount, boolean broadcast, int chrId)
    {
        if (amount == 0)
        {
            return;
        }
        if (amount + this.gp < 0)
        {
            amount = -this.gp;
        }
        if ((chrId > 0) && (amount > 0))
        {
            MapleGuildCharacter mgc = getMGC(chrId);
            if (mgc != null)
            {
                mgc.setGuildContribution(mgc.getGuildContribution() + amount);
                if (mgc.isOnline())
                {
                    WorldGuildService.getInstance().setGuildAndRank(chrId, this.id, mgc.getGuildRank(), mgc.getGuildContribution(), mgc.getAllianceRank());
                }
                else
                {
                    setOfflineGuildStatus(this.id, mgc.getGuildRank(), mgc.getGuildContribution(), mgc.getAllianceRank(), chrId);
                }
                broadcast(GuildPacket.updatePlayerContribution(this.id, chrId, mgc.getGuildContribution()));
            }
        }
        this.gp += amount;
        this.level = calculateLevel();
        broadcast(GuildPacket.updateGuildInfo(this.id, this.gp, this.level));
        if (broadcast)
        {
            broadcast(tools.packet.UIPacket.getGPMsg(amount));
        }
    }

    public MapleGuildCharacter getMGC(int chrId)
    {
        for (MapleGuildCharacter mgc : this.members)
        {
            if (mgc.getId() == chrId)
            {
                return mgc;
            }
        }
        return null;
    }

    public java.util.Collection<MapleGuildSkill> getSkills()
    {
        return this.guildSkills.values();
    }

    public int getSkillLevel(int skillId)
    {
        if (!this.guildSkills.containsKey(skillId))
        {
            return 0;
        }
        return this.guildSkills.get(skillId).level;
    }

    public boolean activateSkill(int skillId, String name)
    {
        if (!this.guildSkills.containsKey(skillId))
        {
            return false;
        }
        MapleGuildSkill ourSkill = this.guildSkills.get(skillId);
        MapleStatEffect effect = client.SkillFactory.getSkill(skillId).getEffect(ourSkill.level);
        if ((ourSkill.timestamp > System.currentTimeMillis()) || (effect.getPeriod() <= 0))
        {
            return false;
        }
        ourSkill.timestamp = (System.currentTimeMillis() + effect.getPeriod() * 60000L);
        ourSkill.activator = name;
        broadcast(GuildPacket.guildSkillPurchased(this.id, skillId, ourSkill.level, ourSkill.timestamp, ourSkill.purchaser, name));
        return true;
    }

    public boolean purchaseSkill(int skillId, String name, int chrId)
    {
        MapleStatEffect effect = client.SkillFactory.getSkill(skillId).getEffect(getSkillLevel(skillId) + 1);
        if ((effect.getReqGuildLevel() > getLevel()) || (effect.getLevel() <= getSkillLevel(skillId)))
        {
            return false;
        }
        MapleGuildSkill ourSkill = this.guildSkills.get(skillId);
        if (ourSkill == null)
        {
            ourSkill = new MapleGuildSkill(skillId, effect.getLevel(), 0L, name, name);
            this.guildSkills.put(skillId, ourSkill);
        }
        else
        {
            ourSkill.level = effect.getLevel();
            ourSkill.purchaser = name;
            ourSkill.activator = name;
        }
        if (effect.getPeriod() <= 0)
        {
            ourSkill.timestamp = -1L;
        }
        else
        {
            ourSkill.timestamp = (System.currentTimeMillis() + effect.getPeriod() * 60000L);
        }
        this.changed_skills = true;
        gainGP(1000, true, chrId);
        broadcast(GuildPacket.guildSkillPurchased(this.id, skillId, ourSkill.level, ourSkill.timestamp, name, name));
        return true;
    }

    public int[] getGuildExp()
    {
        return this.guildExp;
    }

    public void addMemberData(MaplePacketLittleEndianWriter mplew)
    {
        List<MapleGuildCharacter> players = new ArrayList<>();
        for (MapleGuildCharacter mgc : this.members)
        {
            if (mgc.getId() == this.leader)
            {
                players.add(mgc);
            }
        }
        for (MapleGuildCharacter mgc : this.members)
        {
            if (mgc.getId() != this.leader)
            {
                players.add(mgc);
            }
        }
        if (players.size() != this.members.size())
        {
            System.out.println("家族成员信息加载错误 - 实际加载: " + players.size() + " 应当加载: " + this.members.size());
        }

        mplew.writeShort(players.size());
        for (MapleGuildCharacter mgc : players)
        {
            mplew.writeInt(mgc.getId());
        }
        for (MapleGuildCharacter mgc : players)
        {
            mplew.writeAsciiString(mgc.getName(), 13);
            mplew.writeInt(mgc.getJobId());
            mplew.writeInt(mgc.getLevel());
            mplew.writeInt(mgc.getGuildRank());
            mplew.writeInt(mgc.isOnline() ? 1 : 0);
            mplew.writeInt(mgc.getAllianceRank());
            mplew.writeInt(mgc.getGuildContribution());
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeLong(tools.packet.PacketHelper.getTime(-2L));
        }

        mplew.writeShort(this.applyMembers.size());
        for (MapleGuildCharacter mgc : this.applyMembers)
        {
            mplew.writeInt(mgc.getId());
        }
        for (MapleGuildCharacter mgc : this.applyMembers)
        {
            mplew.writeAsciiString(mgc.getName(), 13);
            mplew.writeInt(mgc.getJobId());
            mplew.writeInt(mgc.getLevel());
            mplew.writeInt(0);
            mplew.writeInt(mgc.isOnline() ? 1 : 0);
            mplew.writeInt(3);
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeLong(tools.packet.PacketHelper.getTime(-2L));
        }
    }

    public java.util.Collection<MapleGuildCharacter> getMembers()
    {
        return java.util.Collections.unmodifiableCollection(this.members);
    }

    public boolean isInit()
    {
        return this.init;
    }


    public List<MapleBBSThread> getBBS()
    {
        List<MapleBBSThread> ret = new ArrayList(this.bbs.values());
        java.util.Collections.sort(ret, new MapleBBSThread.ThreadComparator());
        return ret;
    }


    public int addBBSThread(String title, String text, int icon, boolean bNotice, int posterID)
    {
        int add = this.bbs.get(0) == null ? 1 : 0;
        this.changed = true;
        int ret = bNotice ? 0 : Math.max(1, this.bbs.size() + add);
        this.bbs.put(ret, new MapleBBSThread(ret, title, text, System.currentTimeMillis(), this.id, posterID, icon));
        return ret;
    }


    public void editBBSThread(int localthreadid, String title, String text, int icon, int posterID, int guildRank)
    {
        MapleBBSThread thread = this.bbs.get(localthreadid);
        if ((thread != null) && ((thread.ownerID == posterID) || (guildRank <= 2)))
        {
            this.changed = true;
            thread.setTitle(title);
            thread.setText(text);
            thread.setIcon(icon);
            thread.setTimestamp(System.currentTimeMillis());
        }
    }


    public void deleteBBSThread(int localthreadid, int posterID, int guildRank)
    {
        MapleBBSThread thread = this.bbs.get(localthreadid);
        if ((thread != null) && ((thread.ownerID == posterID) || (guildRank <= 2)))
        {
            this.changed = true;
            this.bbs.remove(localthreadid);
        }
    }


    public void addBBSReply(int localthreadid, String text, int posterID)
    {
        MapleBBSThread thread = this.bbs.get(localthreadid);
        if (thread != null)
        {
            this.changed = true;
            thread.replies.put(thread.replies.size(), new MapleBBSReply(thread.replies.size(), posterID, text, System.currentTimeMillis()));
        }
    }


    public void deleteBBSReply(int localthreadid, int replyid, int posterID, int guildRank)
    {
        MapleBBSThread thread = this.bbs.get(localthreadid);
        if (thread != null)
        {
            MapleBBSReply reply = thread.replies.get(replyid);
            if ((reply != null) && ((reply.ownerID == posterID) || (guildRank <= 2)))
            {
                this.changed = true;
                thread.replies.remove(replyid);
            }
        }
    }

    public boolean hasSkill(int id)
    {
        return this.guildSkills.containsKey(id);
    }

    private enum BCOp
    {
        NONE, DISBAND, EMBELMCHANGE;

        BCOp()
        {
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\guild\MapleGuild.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */