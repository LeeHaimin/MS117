package handling.world.guild;

import client.MapleCharacter;

public class MapleGuildCharacter implements java.io.Serializable
{
    public static final long serialVersionUID = 2058609046116597760L;
    private final int id;
    private final String name;
    private byte channel = -1;
    private byte guildrank;
    private byte allianceRank;
    private short level;
    private int jobid;
    private int guildid;
    private int guildContribution;
    private boolean online;

    public MapleGuildCharacter(MapleCharacter chr)
    {
        this.name = chr.getName();
        this.level = chr.getLevel();
        this.id = chr.getId();
        this.channel = ((byte) chr.getClient().getChannel());
        this.jobid = chr.getJob();
        this.guildrank = chr.getGuildRank();
        this.guildid = chr.getGuildId();
        this.guildContribution = chr.getGuildContribution();
        this.allianceRank = chr.getAllianceRank();
        this.online = true;
    }

    public MapleGuildCharacter(int id, short lv, String name, byte channel, int job, byte rank, int guildContribution, byte allianceRank, int guildid, boolean on)
    {
        this.level = lv;
        this.id = id;
        this.name = name;
        if (on)
        {
            this.channel = channel;
        }
        this.jobid = job;
        this.online = on;
        this.guildrank = rank;
        this.allianceRank = allianceRank;
        this.guildContribution = guildContribution;
        this.guildid = guildid;
    }

    public int getLevel()
    {
        return this.level;
    }

    public void setLevel(short l)
    {
        this.level = l;
    }

    public int getId()
    {
        return this.id;
    }

    public int getChannel()
    {
        return this.channel;
    }

    public void setChannel(byte ch)
    {
        this.channel = ch;
    }

    public int getJobId()
    {
        return this.jobid;
    }

    public void setJobId(int job)
    {
        this.jobid = job;
    }

    public int getGuildId()
    {
        return this.guildid;
    }

    public void setGuildId(int gid)
    {
        this.guildid = gid;
    }

    public byte getGuildRank()
    {
        return this.guildrank;
    }

    public void setGuildRank(byte rank)
    {
        this.guildrank = rank;
    }

    public int getGuildContribution()
    {
        return this.guildContribution;
    }

    public void setGuildContribution(int c)
    {
        this.guildContribution = c;
    }

    public boolean isOnline()
    {
        return this.online;
    }

    public void setOnline(boolean f)
    {
        this.online = f;
    }

    public String getName()
    {
        return this.name;
    }

    public byte getAllianceRank()
    {
        return this.allianceRank;
    }

    public void setAllianceRank(byte rank)
    {
        this.allianceRank = rank;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\guild\MapleGuildCharacter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */