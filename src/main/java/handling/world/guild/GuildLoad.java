package handling.world.guild;

import org.apache.log4j.Logger;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import handling.world.WorldGuildService;

public class GuildLoad
{
    public static final int NumSavingThreads = 6;
    private static final TimingThread[] Threads = new TimingThread[6];
    private static final Logger log = Logger.getLogger(GuildLoad.class);
    private static final AtomicInteger Distribute = new AtomicInteger(0);
    private static Map<Integer, Map<Integer, MapleBBSReply>> replies = null;

    static
    {
        for (int i = 0; i < Threads.length; i++)
            Threads[i] = new TimingThread(new GuildLoadRunnable());
    }

    public static void QueueGuildForLoad(int hm, Map<Integer, Map<Integer, MapleBBSReply>> replie)
    {
        int Current = Distribute.getAndIncrement() % 6;
        Threads[Current].getRunnable().Queue(hm);
        if (replies == null)
        {
            replies = replie;
        }
    }

    public static void Execute(Object ToNotify)
    {
        for (TimingThread timingThread : Threads)
        {
            timingThread.getRunnable().SetToNotify(ToNotify);
        }
        for (TimingThread thread : Threads)
        {
            thread.start();
        }
    }

    private static class GuildLoadRunnable implements Runnable
    {
        private final ArrayBlockingQueue<Integer> Queue = new ArrayBlockingQueue(1000);
        private Object ToNotify;

        public void run()
        {
            try
            {
                while (!this.Queue.isEmpty())
                {
                    WorldGuildService.getInstance().addLoadedGuild(new MapleGuild(this.Queue.take(), GuildLoad.replies));
                }
                synchronized (this.ToNotify)
                {
                    this.ToNotify.notify();
                }
            }
            catch (InterruptedException ex)
            {
                GuildLoad.log.error("[GuildLoad] 加载家族信息出错." + ex);
            }
        }

        private void Queue(Integer hm)
        {
            this.Queue.add(hm);
        }

        private void SetToNotify(Object o)
        {
            if (this.ToNotify == null)
            {
                this.ToNotify = o;
            }
        }
    }

    private static class TimingThread extends Thread
    {
        private final GuildLoad.GuildLoadRunnable ext;

        public TimingThread(GuildLoad.GuildLoadRunnable r)
        {
            super();
            this.ext = r;
        }

        public GuildLoad.GuildLoadRunnable getRunnable()
        {
            return this.ext;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\guild\GuildLoad.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */