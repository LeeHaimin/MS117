package handling.world.guild;

import java.io.Serializable;

public class MapleGuildSkill implements Serializable
{
    public static final long serialVersionUID = 3565477792055301248L;
    public final int skillID;
    public String purchaser;
    public String activator;
    public long timestamp;
    public int level;

    public MapleGuildSkill(int skillID, int level, long timestamp, String purchaser, String activator)
    {
        this.timestamp = timestamp;
        this.skillID = skillID;
        this.level = level;
        this.purchaser = purchaser;
        this.activator = activator;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\guild\MapleGuildSkill.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */