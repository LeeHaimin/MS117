package handling.world.guild;

import tools.packet.GuildPacket;

public enum MapleGuildResponse
{
    已经有家族(61), 找不到角色(63), 没有参加的家族(51);

    private final int value;

    MapleGuildResponse(int val)
    {
        this.value = val;
    }

    public int getValue()
    {
        return this.value;
    }

    public byte[] getPacket()
    {
        return GuildPacket.genericGuildMessage((byte) this.value);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\guild\MapleGuildResponse.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */