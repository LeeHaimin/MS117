package handling.world;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import handling.channel.ChannelServer;
import handling.channel.PlayerStorage;

public class World
{
    public static void init()
    {
        WorldFindService.getInstance();
        WorldBroadcastService.getInstance();
        WrodlPartyService.getInstance();
        WorldSidekickService.getInstance();
        WorldAllianceService.getInstance();
        WorldBuddyService.getInstance();
        WorldFamilyService.getInstance();
        WorldGuildService.getInstance();
        WorldMessengerService.getInstance();
    }

    public static String getStatus()
    {
        StringBuilder ret = new StringBuilder();
        int totalUsers = 0;
        for (ChannelServer cs : ChannelServer.getAllInstances())
        {
            ret.append("频道 ");
            ret.append(cs.getChannel());
            ret.append(": ");
            int channelUsers = cs.getConnectedClients();
            totalUsers += channelUsers;
            ret.append(channelUsers);
            ret.append(" 玩家\n");
        }
        ret.append("总计在线: ");
        ret.append(totalUsers);
        ret.append("\n");
        return ret.toString();
    }

    public static Map<Integer, Integer> getConnected()
    {
        Map<Integer, Integer> ret = new LinkedHashMap<>();
        int total = 0;
        for (ChannelServer ch : ChannelServer.getAllInstances())
        {
            int chOnline = ch.getConnectedClients();
            ret.put(ch.getChannel(), chOnline);
            total += chOnline;
        }
        int csOnline = handling.cashshop.CashShopServer.getConnectedClients();
        ret.put(-10, csOnline);
        total += csOnline;

        ret.put(0, total);
        return ret;
    }

    public static List<CheaterData> getCheaters()
    {
        List<CheaterData> allCheaters = new ArrayList<>();
        for (ChannelServer cs : ChannelServer.getAllInstances())
        {
            allCheaters.addAll(cs.getCheaters());
        }
        java.util.Collections.sort(allCheaters);
        return tools.CollectionUtil.copyFirst(allCheaters, 20);
    }

    public static List<CheaterData> getReports()
    {
        List<CheaterData> allCheaters = new ArrayList<>();
        for (ChannelServer cs : ChannelServer.getAllInstances())
        {
            allCheaters.addAll(cs.getReports());
        }
        java.util.Collections.sort(allCheaters);
        return tools.CollectionUtil.copyFirst(allCheaters, 20);
    }

    public static boolean isConnected(String charName)
    {
        return WorldFindService.getInstance().findChannel(charName) > 0;
    }

    public static void toggleMegaphoneMuteState()
    {
        for (ChannelServer cs : ChannelServer.getAllInstances())
        {
            cs.toggleMegaphoneMuteState();
        }
    }

    public static void ChannelChange_Data(CharacterTransfer Data, int characterid, int toChannel)
    {
        getStorage(toChannel).registerPendingPlayer(Data, characterid);
    }

    public static PlayerStorage getStorage(int channel)
    {
        if (channel == -20) return handling.Auction.AuctionServer.getPlayerStorage();
        if (channel == -10)
        {
            return handling.cashshop.CashShopServer.getPlayerStorage();
        }
        return ChannelServer.getInstance(channel).getPlayerStorage();
    }

    public static boolean isCharacterListConnected(List<String> charNames)
    {
        for (ChannelServer cserv : ChannelServer.getAllInstances())
        {
            for (String name : charNames)
            {
                if (cserv.isConnected(name)) return true;
            }
        }
        ChannelServer cserv;
        return false;
    }

    public static String getAllowLoginTip(List<String> charNames)
    {
        StringBuilder ret = new StringBuilder("账号下其他角色在游戏: ");
        for (ChannelServer cserv : ChannelServer.getAllInstances())
        {
            for (String name : charNames)
                if (cserv.isConnected(name))
                {
                    ret.append(name);
                    ret.append(" ");
                }
        }
        ChannelServer cserv;
        return ret.toString();
    }

    public static boolean hasMerchant(int accountID)
    {
        for (ChannelServer cs : ChannelServer.getAllInstances())
        {
            if (cs.containsMerchant(accountID))
            {
                return true;
            }
        }
        return false;
    }

    public static boolean hasMerchant(int accountID, int characterID)
    {
        for (ChannelServer cs : ChannelServer.getAllInstances())
        {
            if (cs.containsMerchant(accountID, characterID))
            {
                return true;
            }
        }
        return false;
    }

    public static server.shops.HiredMerchant getMerchant(int accountID, int characterID)
    {
        for (ChannelServer cs : ChannelServer.getAllInstances())
        {
            if (cs.containsMerchant(accountID, characterID))
            {
                return cs.getHiredMerchants(accountID, characterID);
            }
        }
        return null;
    }

    public static int getPendingCharacterSize()
    {
        int ret = handling.cashshop.CashShopServer.getPlayerStorage().pendingCharacterSize() + handling.Auction.AuctionServer.getPlayerStorage().pendingCharacterSize();
        for (ChannelServer cserv : ChannelServer.getAllInstances())
        {
            ret += cserv.getPlayerStorage().pendingCharacterSize();
        }
        return ret;
    }

    public static boolean isChannelAvailable(int ch)
    {
        if ((ChannelServer.getInstance(ch) == null) || (ChannelServer.getInstance(ch).getPlayerStorage() == null))
        {
            return false;
        }
        return ChannelServer.getInstance(ch).getPlayerStorage().getConnectedClients() < (ch == 1 ? 600 : 400);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\World.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */