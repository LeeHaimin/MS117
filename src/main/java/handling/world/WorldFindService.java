package handling.world;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import handling.channel.ChannelServer;


public class WorldFindService
{
    private final ReentrantReadWriteLock lock;
    private final HashMap<Integer, Integer> idToChannel;
    private final HashMap<String, Integer> nameToChannel;

    private WorldFindService()
    {
        this.lock = new ReentrantReadWriteLock();
        this.idToChannel = new HashMap<>();
        this.nameToChannel = new HashMap<>();
        System.out.println("[WorldFindService] 已经启动...");
    }

    public static WorldFindService getInstance()
    {
        return SingletonHolder.instance;
    }

    public void register(int chrId, String chrName, int channel)
    {
        this.lock.writeLock().lock();
        try
        {
            this.idToChannel.put(chrId, channel);
            this.nameToChannel.put(chrName.toLowerCase(), channel);
        }
        finally
        {
            this.lock.writeLock().unlock();
        }
        if (channel == -10)
        {
            System.out.println("玩家连接 - 角色ID: " + chrId + " 名字: " + chrName + " 进入商城");
        }
        else if (channel == -20)
        {
            System.out.println("玩家连接 - 角色ID: " + chrId + " 名字: " + chrName + " 进入拍卖");
        }
        else if (channel > -1)
        {
            System.out.println("玩家连接 - 角色ID: " + chrId + " 名字: " + chrName + " 频道: " + channel);
        }
        else
        {
            System.out.println("玩家连接 - 角色ID: " + chrId + " 未处理的频道...");
        }
    }

    public void forceDeregister(int chrId, String chrName)
    {
        lock.writeLock().lock();
        try
        {
            idToChannel.remove(chrId);
            nameToChannel.remove(chrName.toLowerCase());
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public CharacterIdChannelPair[] multiBuddyFind(int charIdFrom, int[] characterIds)
    {
        List<CharacterIdChannelPair> foundsChars = new ArrayList(characterIds.length);
        for (int i : characterIds)
        {
            int channel = findChannel(i);
            if (channel > 0)
            {
                foundsChars.add(new CharacterIdChannelPair(i, channel));
            }
        }
        Collections.sort(foundsChars);
        return foundsChars.toArray(new CharacterIdChannelPair[foundsChars.size()]);
    }

    public int findChannel(int id)
    {
        Integer ret;
        lock.readLock().lock();
        try
        {
            ret = idToChannel.get(id);
        }
        finally
        {
            lock.readLock().unlock();
        }
        if (ret != null)
        {
            if (ret != -10 && ret != -20 && ChannelServer.getInstance(ret) == null)
            { //wha
                forceDeregister(id);
                return -1;
            }
            return ret;
        }
        return -1;
    }

    public void forceDeregister(int chrId)
    {
        lock.writeLock().lock();
        try
        {
            idToChannel.remove(chrId);
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public MapleCharacter findCharacterByName(String name)
    {
        int ch = findChannel(name);
        if (ch > 0)
        {
            return ChannelServer.getInstance(ch).getPlayerStorage().getCharacterByName(name);
        }
        return null;
    }

    public int findChannel(String st)
    {
        Integer ret;
        lock.readLock().lock();
        try
        {
            ret = nameToChannel.get(st.toLowerCase());
        }
        finally
        {
            lock.readLock().unlock();
        }
        if (ret != null)
        {
            if (ret != -10 && ret != -20 && ChannelServer.getInstance(ret) == null)
            { //wha
                forceDeregister(st);
                return -1;
            }
            return ret;
        }
        return -1;
    }

    public void forceDeregister(String chrName)
    {
        lock.writeLock().lock();
        try
        {
            nameToChannel.remove(chrName.toLowerCase());
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public MapleCharacter findCharacterById(int id)
    {
        int ch = findChannel(id);
        if (ch > 0)
        {
            return ChannelServer.getInstance(ch).getPlayerStorage().getCharacterById(id);
        }
        return null;
    }

    private static class SingletonHolder
    {
        protected static final WorldFindService instance = new WorldFindService();
    }
}