package handling.world;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.Map;

import client.MapleCharacter;
import client.MapleTraitType;
import client.inventory.MaplePet;

public class CharacterTransfer implements java.io.Externalizable
{
    public final java.util.Map<Integer, Integer> mbook;
    public final java.util.Map<Byte, Integer> reports = new LinkedHashMap<>();
    public final java.util.Map<Integer, tools.Pair<Byte, Integer>> keymap;
    public final java.util.List<tools.Pair<Integer, Integer>> quickslot;
    public final java.util.Map<Integer, client.MonsterFamiliar> familiars;
    public final java.util.Map<client.MapleTraitType, Integer> traits = new EnumMap(client.MapleTraitType.class);
    public final java.util.List boxed;
    public final java.util.Map<client.CharacterNameAndId, Boolean> buddies = new LinkedHashMap<>();
    public final java.util.Map<Integer, Object> Quest = new LinkedHashMap<>();
    public final java.util.Map<Integer, String> InfoQuest;
    public final java.util.Map<String, String> KeyValue;
    public final java.util.Map<Integer, client.SkillEntry> Skills = new LinkedHashMap<>();
    public final java.util.Map<Integer, client.CardData> cardsInfo = new LinkedHashMap<>();
    public int characterid;
    public int accountid;
    public int fame;
    public int pvpExp;
    public int pvpPoints;
    public int meso;
    public int hair;
    public int face;
    public int mapid;
    public int guildid;
    public int sidekick;
    public int partyid;
    public int messengerid;
    public int ACash;
    public int MaplePoints;
    public int mount_itemid;
    public int mount_exp;
    public int points;
    public int vpoints;
    public int marriageId;
    public int maxhp;
    public int maxmp;
    public int hp;
    public int mp;
    public int familyid;
    public int seniorid;
    public int junior1;
    public int junior2;
    public int currentrep;
    public int totalrep;
    public int gachexp;
    public int guildContribution;
    public int totalWins;
    public int totalLosses;
    public byte channel;
    public byte gender;
    public byte gmLevel;
    public byte guildrank;
    public byte alliancerank;
    public byte fairyExp;
    public byte buddysize;
    public byte world;
    public byte initialSpawnPoint;
    public byte skinColor;
    public byte mount_level;
    public byte mount_Fatigue;
    public byte subcategory;
    public long lastfametime;
    public long TranferTime;
    public long exp;
    public String name;
    public String accountname;
    public String BlessOfFairy;
    public String BlessOfEmpress;
    public String chalkboard;
    public String tempIP;
    public short level;
    public short str;
    public short dex;
    public short int_;
    public short luk;
    public short remainingAp;
    public short hpApUsed;
    public short job;
    public short fatigue;
    public Object inventorys;
    public Object skillmacro;
    public Object storage;
    public Object cs;
    public Object battlers;
    public Object anticheat;
    public Object antiMacro;
    public Object innerSkills;
    public int[] savedlocation;
    public int[] wishlist;
    public int[] rocks;
    public int[] remainingSp;
    public int[] regrocks;
    public int[] hyperrocks;
    public byte[] petStore;
    public client.inventory.MapleImp[] imps;
    public java.util.List<Integer> finishedAchievements = null;
    public java.util.List<Integer> famedcharacters = null;
    public java.util.List<Integer> battledaccs = null;
    public java.util.List<Integer> extendedSlots = null;
    public java.util.List<server.shop.MapleShopItem> rebuy = null;
    public int decorate;
    public int beans;
    public int warning;
    public int dollars;
    public int shareLots;
    public int vip;
    public java.sql.Timestamp viptime;
    public int reborns;
    public int reborns1;
    public int reborns2;
    public int reborns3;
    public int apstorage;
    public int honorLevel;
    public int honorExp;
    public int love;
    public long lastLoveTime;
    public java.util.Map<Integer, Long> loveCharacters = null;

    public int playerPoints;

    public int playerEnergy;

    public Object pvpStats;

    public int pvpDeaths;

    public int pvpKills;
    public int pvpVictory;
    public int runningDark;
    public int runningDarkSlot;
    public int runningLight;
    public int runningLightSlot;
    public Object potionPot;
    public Object coreAura;
    public Object SpecialStats;

    public CharacterTransfer()
    {
        this.boxed = new ArrayList<>();
        this.finishedAchievements = new ArrayList<>();
        this.famedcharacters = new ArrayList<>();
        this.battledaccs = new ArrayList<>();
        this.extendedSlots = new ArrayList<>();
        this.loveCharacters = new LinkedHashMap<>();
        this.rebuy = new ArrayList<>();
        this.KeyValue = new LinkedHashMap<>();
        this.InfoQuest = new LinkedHashMap<>();
        this.keymap = new LinkedHashMap<>();
        this.quickslot = new ArrayList<>();
        this.familiars = new LinkedHashMap<>();
        this.mbook = new LinkedHashMap<>();
    }

    public CharacterTransfer(MapleCharacter chr)
    {
        this.characterid = chr.getId();
        this.accountid = chr.getAccountID();
        this.accountname = chr.getClient().getAccountName();
        this.channel = ((byte) chr.getClient().getChannel());
        this.ACash = chr.getCSPoints(1);
        this.MaplePoints = chr.getCSPoints(2);
        this.vpoints = chr.getVPoints();
        this.name = chr.getName();
        this.fame = chr.getFame();
        this.love = chr.getLove();
        this.gender = chr.getGender();
        this.level = chr.getLevel();
        this.str = chr.getStat().getStr();
        this.dex = chr.getStat().getDex();
        this.int_ = chr.getStat().getInt();
        this.luk = chr.getStat().getLuk();
        this.hp = chr.getStat().getHp();
        this.mp = chr.getStat().getMp();
        this.maxhp = chr.getStat().getMaxHp();
        this.maxmp = chr.getStat().getMaxMp();
        this.exp = chr.getExp();
        this.hpApUsed = chr.getHpApUsed();
        this.remainingAp = chr.getRemainingAp();
        this.remainingSp = chr.getRemainingSps();
        this.meso = chr.getMeso();
        this.pvpExp = chr.getTotalBattleExp();
        this.pvpPoints = chr.getBattlePoints();
        this.skinColor = chr.getSkinColor();
        this.job = chr.getJob();
        this.hair = chr.getHair();
        this.face = chr.getFace();
        this.mapid = chr.getMapId();
        this.initialSpawnPoint = chr.getInitialSpawnpoint();
        this.marriageId = chr.getMarriageId();
        this.world = chr.getWorld();
        this.guildid = chr.getGuildId();
        this.guildrank = chr.getGuildRank();
        this.guildContribution = chr.getGuildContribution();
        this.alliancerank = chr.getAllianceRank();
        this.gmLevel = ((byte) chr.getGMLevel());
        this.points = chr.getPoints();
        this.fairyExp = chr.getFairyExp();
        this.petStore = chr.getPetStores();
        this.subcategory = chr.getSubcategory();
        this.imps = chr.getImps();
        this.fatigue = chr.getFatigue();
        this.currentrep = chr.getCurrentRep();
        this.totalrep = chr.getTotalRep();
        this.familyid = chr.getFamilyId();
        this.totalWins = chr.getTotalWins();
        this.totalLosses = chr.getTotalLosses();
        this.seniorid = chr.getSeniorId();
        this.junior1 = chr.getJunior1();
        this.junior2 = chr.getJunior2();
        this.gachexp = chr.getGachExp();
        this.boxed = chr.getBoxed();
        this.familiars = chr.getFamiliars();
        chr.getCheatTracker().dispose();
        this.anticheat = chr.getCheatTracker();
        this.antiMacro = chr.getAntiMacro();
        this.tempIP = chr.getClient().getTempIP();
        this.rebuy = chr.getRebuy();
        this.decorate = chr.getDecorate();
        this.beans = chr.getBeans();
        this.warning = chr.getWarning();
        this.dollars = chr.getDollars();
        this.shareLots = chr.getShareLots();
        this.reborns = chr.getReborns();
        this.reborns1 = chr.getReborns1();
        this.reborns2 = chr.getReborns2();
        this.reborns3 = chr.getReborns3();
        this.apstorage = chr.getAPS();
        this.honorLevel = chr.getHonorLevel();
        this.honorExp = chr.getHonorExp();
        this.vip = chr.getVip();
        this.viptime = chr.getViptime();
        this.playerPoints = chr.getPlayerPoints();
        this.playerEnergy = chr.getPlayerEnergy();
        this.pvpDeaths = chr.getPvpDeaths();
        this.pvpKills = chr.getPvpKills();
        this.pvpVictory = chr.getPvpVictory();
        this.runningDark = chr.getDarkType();
        this.runningDarkSlot = chr.getDarkTotal();
        this.runningLight = chr.getLightType();
        this.runningLightSlot = chr.getLightTotal();

        boolean uneq = false;
        for (int i = 0; i < this.petStore.length; i++)
        {
            MaplePet pet = chr.getSpawnPet(i);
            if (this.petStore[i] == 0)
            {
                this.petStore[i] = -1;
            }
            if (pet != null)
            {
                uneq = true;
                this.petStore[i] = ((byte) Math.max(this.petStore[i], pet.getInventoryPosition()));
            }
        }
        if (uneq)
        {
            chr.unequipAllSpawnPets();
        }
        if (chr.getSidekick() != null)
        {
            this.sidekick = chr.getSidekick().getId();
        }
        else
        {
            this.sidekick = 0;
        }
        for (MapleTraitType type : MapleTraitType.values())
        {

            this.traits.put(type, chr.getTrait(type).getTotalExp());
        }
        for (client.BuddylistEntry qs : chr.getBuddylist().getBuddies())
        {
            this.buddies.put(new client.CharacterNameAndId(qs.getCharacterId(), qs.getName(), qs.getGroup()), qs.isVisible());
        }
        for (Map.Entry<client.anticheat.ReportType, Integer> ss : chr.getReports().entrySet())
        {
            this.reports.put(ss.getKey().i, ss.getValue());
        }
        this.buddysize = chr.getBuddyCapacity();

        this.partyid = (chr.getParty() == null ? -1 : chr.getParty().getId());

        if (chr.getMessenger() != null)
        {
            this.messengerid = chr.getMessenger().getId();
        }
        else
        {
            this.messengerid = 0;
        }
        this.finishedAchievements = chr.getFinishedAchievements();
        this.KeyValue = chr.getKeyValue_Map();
        this.InfoQuest = chr.getInfoQuest_Map();
        for (Map.Entry<server.quest.MapleQuest, client.MapleQuestStatus> qs : chr.getQuest_Map().entrySet())
        {
            this.Quest.put(qs.getKey().getId(), qs.getValue());
        }
        this.mbook = chr.getMonsterBook().getCards();
        this.inventorys = chr.getInventorys();
        for (Map.Entry<client.Skill, client.SkillEntry> qs : chr.getSkills().entrySet())
        {
            this.Skills.put(qs.getKey().getId(), qs.getValue());
        }
        for (Map.Entry<Integer, client.CardData> ii : chr.getCharacterCard().getCards().entrySet())
        {
            this.cardsInfo.put(ii.getKey(), ii.getValue());
        }
        this.SpecialStats = chr.getSpecialStat();
        this.BlessOfFairy = chr.getBlessOfFairyOrigin();
        this.BlessOfEmpress = chr.getBlessOfEmpressOrigin();
        this.chalkboard = chr.getChalkboard();
        this.skillmacro = chr.getMacros();
        this.innerSkills = chr.getInnerSkills();
        this.keymap = chr.getKeyLayout().Layout();
        this.quickslot = chr.getQuickSlot().Layout();
        this.savedlocation = chr.getSavedLocations();
        this.wishlist = chr.getWishlist();
        this.rocks = chr.getRocks();
        this.regrocks = chr.getRegRocks();
        this.hyperrocks = chr.getHyperRocks();
        this.famedcharacters = chr.getFamedCharacters();
        this.battledaccs = chr.getBattledCharacters();
        this.lastfametime = chr.getLastFameTime();
        this.storage = chr.getStorage();
        this.pvpStats = chr.getPvpStats();
        this.potionPot = chr.getPotionPot();
        this.coreAura = chr.getCoreAura();
        this.cs = chr.getCashInventory();
        this.extendedSlots = chr.getExtendedSlots();
        client.inventory.MapleMount mount = chr.getMount();
        this.mount_itemid = mount.getItemId();
        this.mount_Fatigue = mount.getFatigue();
        this.mount_level = mount.getLevel();
        this.mount_exp = mount.getExp();
        this.battlers = chr.getBattlers();
        this.lastLoveTime = chr.getLastLoveTime();
        this.loveCharacters = chr.getLoveCharacters();
        this.TranferTime = System.currentTimeMillis();
    }

    public void writeExternal(java.io.ObjectOutput out) throws java.io.IOException
    {
    }

    public void readExternal(java.io.ObjectInput in) throws java.io.IOException, ClassNotFoundException
    {
        this.TranferTime = System.currentTimeMillis();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\world\CharacterTransfer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */