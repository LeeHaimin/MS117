package handling;

public interface WritableIntValueHolder
{
    short getValue();

    void setValue(short paramShort);
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\handling\WritableIntValueHolder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */