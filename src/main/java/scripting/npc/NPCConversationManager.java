package scripting.npc;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.script.Invocable;

import client.Battler;
import client.MapleCharacter;
import client.MapleCharacterUtil;
import client.MapleClient;
import client.MapleStat;
import client.Skill;
import client.SkillEntry;
import client.SkillFactory;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import client.inventory.MapleRing;
import constants.BattleConstants;
import constants.GameConstants;
import database.DatabaseConnection;
import handling.channel.ChannelServer;
import handling.channel.MapleGuildRanking;
import handling.world.WorldAllianceService;
import handling.world.WorldBroadcastService;
import handling.world.WorldGuildService;
import handling.world.guild.MapleGuild;
import handling.world.party.MapleParty;
import handling.world.party.MaplePartyCharacter;
import scripting.event.EventInstanceManager;
import server.MapleCarnivalChallenge;
import server.MapleCarnivalParty;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.MapleStatEffect;
import server.PokemonBattle;
import server.Randomizer;
import server.RankingWorker;
import server.StructItemOption;
import server.life.MonsterDropEntry;
import server.life.MonsterGlobalDropEntry;
import server.maps.MapleMap;
import server.quest.MapleQuest;
import server.squad.MapleSquad;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.Triple;
import tools.packet.NPCPacket;

public class NPCConversationManager extends scripting.AbstractPlayerInteraction
{
    private static final Logger _log = Logger.getLogger(NPCConversationManager.class);
    private final int npcId;
    private final int npcMode;
    private final Invocable iv;
    public boolean pendingDisposal = false;
    private String getText;

    public NPCConversationManager(MapleClient c, int npc, int npcMode, Invocable iv)
    {
        super(c, npc, npcMode);
        this.npcId = npc;
        this.npcMode = npcMode;
        this.iv = iv;
    }

    public Invocable getIv()
    {
        return this.iv;
    }

    public int getNpcMode()
    {
        return this.npcMode;
    }

    public void safeDispose()
    {
        this.pendingDisposal = true;
    }

    public void dispose()
    {
        NPCScriptManager.getInstance().dispose(this);
    }

    public void sendNext(String text)
    {
        sendNext(text, this.id);
    }

    public void sendNext(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 0, text, "00 01", (byte) 0));
    }

    public void sendPlayerToNpc(String text)
    {
        sendNextS(text, (byte) 3, this.id);
    }

    public void sendNextNoESC(String text)
    {
        sendNextS(text, (byte) 1, this.id);
    }

    public void sendNextNoESC(String text, int id)
    {
        sendNextS(text, (byte) 1, id);
    }

    public void sendNextS(String text, byte type)
    {
        sendNextS(text, type, this.id);
    }

    public void sendNextS(String text, byte type, int idd)
    {
        if (text.contains("#L"))
        {
            sendSimpleS(text, type);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(this.id, (byte) 0, text, "00 01", type, idd));
    }

    public void sendPrev(String text)
    {
        sendPrev(text, this.id);
    }

    public void sendPrev(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 0, text, "01 00", (byte) 0));
    }

    public void sendPrevS(String text, byte type)
    {
        sendPrevS(text, type, this.id);
    }

    public void sendPrevS(String text, byte type, int idd)
    {
        if (text.contains("#L"))
        {
            sendSimpleS(text, type);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(this.id, (byte) 0, text, "01 00", type, idd));
    }

    public void sendNextPrev(String text)
    {
        sendNextPrev(text, this.id);
    }

    public void sendNextPrev(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 0, text, "01 01", (byte) 0));
    }

    public void PlayerToNpc(String text)
    {
        sendNextPrevS(text, (byte) 3);
    }

    public void sendNextPrevS(String text)
    {
        sendNextPrevS(text, (byte) 3);
    }

    public void sendNextPrevS(String text, byte type)
    {
        sendNextPrevS(text, type, this.id);
    }

    public void sendNextPrevS(String text, byte type, int idd)
    {
        if (text.contains("#L"))
        {
            sendSimpleS(text, type);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(this.id, (byte) 0, text, "01 01", type, idd));
    }

    public void sendOk(String text)
    {
        sendOk(text, this.id);
    }

    public void sendOk(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 0, text, "00 00", (byte) 0));
    }

    public void sendOkS(String text, byte type)
    {
        sendOkS(text, type, this.id);
    }

    public void sendOkS(String text, byte type, int idd)
    {
        if (text.contains("#L"))
        {
            sendSimpleS(text, type);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(this.id, (byte) 0, text, "00 00", type, idd));
    }

    public void sendYesNo(String text)
    {
        sendYesNo(text, this.id);
    }

    public void sendYesNo(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 2, text, "", (byte) 0));
    }

    public void sendYesNoS(String text, byte type)
    {
        sendYesNoS(text, type, this.id);
    }

    public void sendYesNoS(String text, byte type, int idd)
    {
        if (text.contains("#L"))
        {
            sendSimpleS(text, type);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(this.id, (byte) 2, text, "", type, idd));
    }

    public void sendAcceptDecline(String text)
    {
        askAcceptDecline(text);
    }

    public void sendAcceptDeclineNoESC(String text)
    {
        askAcceptDeclineNoESC(text);
    }

    public void askAcceptDecline(String text)
    {
        askAcceptDecline(text, this.id);
    }

    public void askAcceptDecline(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }

        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 14, text, "", (byte) 0));
    }

    public void askAcceptDeclineNoESC(String text)
    {
        askAcceptDeclineNoESC(text, this.id);
    }

    public void askAcceptDeclineNoESC(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }

        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 14, text, "", (byte) 1));
    }

    public void askMapSelection(String sel)
    {
        this.c.getSession().write(NPCPacket.getMapSelection(this.id, (byte) 16, sel));
    }

    public void sendSimple(String text)
    {
        sendSimple(text, this.id);
    }

    public void sendSimple(String text, int id)
    {
        if (!text.contains("#L"))
        {
            sendNext(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(id, (byte) 5, text, "", (byte) 0));
    }

    public void sendSimpleS(String text, byte type)
    {
        sendSimpleS(text, type, this.id);
    }

    public void sendSimpleS(String text, byte type, int idd)
    {
        if (!text.contains("#L"))
        {
            sendNextS(text, type);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalk(this.id, (byte) 5, text, "", type, idd));
    }

    public void askAvatar(String text, int[] styles, int card)
    {
        this.c.getSession().write(NPCPacket.getNPCTalkStyle(this.id, text, styles, card, false));
    }

    public void sendStyle(String text, int[] styles, int card)
    {
        this.c.getSession().write(NPCPacket.getNPCTalkStyle(this.id, text, styles, card, false));
    }

    public void sendAStyle(String text, int[] styles, int card)
    {
        this.c.getSession().write(NPCPacket.getNPCTalkStyle(this.id, text, styles, card, true));
    }

    public void sendGetNumber(String text, int def, int min, int max)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalkNum(this.id, (byte) 4, text, def, min, max));
    }

    public void sendGetText(String text)
    {
        sendGetText(text, this.id);
    }

    public void sendGetText(String text, int id)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getNPCTalkText(id, (byte) 3, text));
    }

    public void setGetText(String text)
    {
        this.getText = text;
    }

    public void sendPlayerOk(String text)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getPlayerTalk(this.id, (byte) 0, text, "00 00", (byte) 16));
    }

    public void sendPlayerNext(String text)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getPlayerTalk(this.id, (byte) 0, text, "00 01", (byte) 17));
    }

    public void sendPlayerNextPrev(String text)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }
        this.c.getSession().write(NPCPacket.getPlayerTalk(this.id, (byte) 0, text, "01 01", (byte) 17));
    }

    public void sendRevivePet(String text)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
        }
    }

    public void sendPlayerStart(String text)
    {
        if (text.contains("#L"))
        {
            sendSimple(text);
            return;
        }

        this.c.getSession().write(NPCPacket.getPlayerTalk(this.id, (byte) 14, text, "", (byte) 16));
    }

    public void setHair(int hair)
    {
        getPlayer().setHair(hair);
        getPlayer().updateSingleStat(MapleStat.发型, hair);
        getPlayer().equipChanged();
    }

    public void setFace(int face)
    {
        getPlayer().setFace(face);
        getPlayer().updateSingleStat(MapleStat.脸型, face);
        getPlayer().equipChanged();
    }

    public void setSkin(int color)
    {
        getPlayer().setSkinColor((byte) color);
        getPlayer().updateSingleStat(MapleStat.皮肤, color);
        getPlayer().equipChanged();
    }

    public void setAndroidHair(int hair)
    {
        this.c.getPlayer().getAndroid().setHair(hair);
        this.c.getPlayer().getAndroid().saveToDb();
        this.c.getPlayer().setAndroid(this.c.getPlayer().getAndroid());
    }

    public void setAndroidFace(int face)
    {
        this.c.getPlayer().getAndroid().setFace(face);
        this.c.getPlayer().getAndroid().saveToDb();
        this.c.getPlayer().setAndroid(this.c.getPlayer().getAndroid());
    }

    public void setAndroidSkin(int skin)
    {
        this.c.getPlayer().getAndroid().setSkin(skin);
        this.c.getPlayer().getAndroid().saveToDb();
        this.c.getPlayer().setAndroid(this.c.getPlayer().getAndroid());
    }

    public int setRandomAvatarA(int ticket, int... args_all)
    {
        if (!haveItem(ticket))
        {
            return -1;
        }
        gainItem(ticket, (short) -1);
        int args = args_all[Randomizer.nextInt(args_all.length)];
        if (args < 100)
        {
            this.c.getPlayer().getAndroid().setSkin(args);
        }
        else if (args < 30000)
        {
            this.c.getPlayer().getAndroid().setFace(args);
        }
        else
        {
            this.c.getPlayer().getAndroid().setHair(args);
        }
        this.c.getPlayer().getAndroid().saveToDb();
        this.c.getPlayer().setAndroid(this.c.getPlayer().getAndroid());
        return 1;
    }

    public int setAvatarA(int ticket, int args)
    {
        if (!haveItem(ticket))
        {
            return -1;
        }
        gainItem(ticket, (short) -1);
        if (args < 100)
        {
            this.c.getPlayer().getAndroid().setSkin(args);
        }
        else if (args < 30000)
        {
            this.c.getPlayer().getAndroid().setFace(args);
        }
        else
        {
            this.c.getPlayer().getAndroid().setHair(args);
        }
        this.c.getPlayer().getAndroid().saveToDb();
        this.c.getPlayer().setAndroid(this.c.getPlayer().getAndroid());
        return 1;
    }

    public int setRandomAvatar(int ticket, int... args_all)
    {
        if (!haveItem(ticket))
        {
            return -1;
        }
        gainItem(ticket, (short) -1);
        int args = args_all[Randomizer.nextInt(args_all.length)];
        if (args < 100)
        {
            this.c.getPlayer().setSkinColor((byte) args);
            this.c.getPlayer().updateSingleStat(MapleStat.皮肤, args);
        }
        else if (args < 30000)
        {
            this.c.getPlayer().setFace(args);
            this.c.getPlayer().updateSingleStat(MapleStat.脸型, args);
        }
        else
        {
            this.c.getPlayer().setHair(args);
            this.c.getPlayer().updateSingleStat(MapleStat.发型, args);
        }
        this.c.getPlayer().equipChanged();
        return 1;
    }

    public int setAvatar(int ticket, int args)
    {
        if (!haveItem(ticket))
        {
            return -1;
        }
        gainItem(ticket, (short) -1);
        if (args < 100)
        {
            this.c.getPlayer().setSkinColor((byte) args);
            this.c.getPlayer().updateSingleStat(MapleStat.皮肤, args);
        }
        else if (args < 30000)
        {
            this.c.getPlayer().setFace(args);
            this.c.getPlayer().updateSingleStat(MapleStat.脸型, args);
        }
        else
        {
            this.c.getPlayer().setHair(args);
            this.c.getPlayer().updateSingleStat(MapleStat.发型, args);
        }
        this.c.getPlayer().equipChanged();
        return 1;
    }

    public void sendStorage()
    {
        this.c.getPlayer().setConversation(4);
        this.c.getPlayer().getStorage().sendStorage(this.c, this.id);
    }

    public void openShop(int id)
    {
        server.shop.MapleShopFactory.getInstance().getShop(id).sendShop(this.c);
    }

    public void openShopNPC(int id)
    {
        server.shop.MapleShopFactory.getInstance().getShop(id).sendShop(this.c, this.id);
    }

    public int gainGachaponItem(int id, int quantity)
    {
        return gainGachaponItem(id, quantity, this.c.getPlayer().getMap().getStreetName() + " - " + this.c.getPlayer().getMap().getMapName());
    }

    public int gainGachaponItem(int id, int quantity, String msg)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        try
        {
            if (!ii.itemExists(id))
            {
                return -1;
            }
            Item item = MapleInventoryManipulator.addbyId_Gachapon(this.c, id, (short) quantity, "从 " + msg + " 中获得时间: " + FileoutputUtil.CurrentReadable_Time());
            if (item == null)
            {
                return -1;
            }
            byte rareness = GameConstants.gachaponRareItem(item.getItemId());
            if ((rareness == 1) || (rareness == 2) || (rareness == 3))
            {
                WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(this.c.getPlayer().getName(), " : 从" + msg + "中获得{" + ii.getName(item.getItemId()) +
                        "}！大家一起恭喜他（她）吧！！！！", item, rareness, this.c.getChannel()));
            }
            return item.getItemId();
        }
        catch (Exception e)
        {
            _log.error("gainGachaponItem 错误", e);
        }
        return -1;
    }

    public int gainGachaponItem(int id, int quantity, String msg, int rareness)
    {
        return gainGachaponItem(id, quantity, msg, rareness, false, 0L);
    }

    public int gainGachaponItem(int id, int quantity, String msg, int rareness, boolean buy, long period)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        try
        {
            if (!ii.itemExists(id))
            {
                return -1;
            }
            Item item = MapleInventoryManipulator.addbyId_Gachapon(this.c, id, (short) quantity, "从 " + msg + " 中" + (buy ? "购买" : "获得") + "时间: " + FileoutputUtil.CurrentReadable_Time(), period);
            if (item == null)
            {
                return -1;
            }
            if ((rareness == 1) || (rareness == 2) || (rareness == 3))
            {
                WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(this.c.getPlayer().getName(),
                        " : 从" + msg + "中" + (buy ? "购买" : "获得") + "{" + ii.getName(item.getItemId()) + "}！大家一起恭喜他（她）吧！！！！", item, (byte) rareness, this.c.getChannel()));
            }
            return item.getItemId();
        }
        catch (Exception e)
        {
            _log.error("gainGachaponItem 错误", e);
        }
        return -1;
    }

    public int gainGachaponItem(int id, int quantity, String msg, int rareness, long period)
    {
        return gainGachaponItem(id, quantity, msg, rareness, false, period);
    }

    public int gainGachaponItem(int id, int quantity, String msg, int rareness, boolean buy)
    {
        return gainGachaponItem(id, quantity, msg, rareness, buy, 0L);
    }

    public void changeJob(int jobId)
    {
        this.c.getPlayer().changeJob(jobId);
    }

    public boolean isValidJob(int jobId)
    {
        return MapleCarnivalChallenge.getJobNameByIdNull(jobId) != null;
    }

    public String getJobNameById(int jobId)
    {
        return MapleCarnivalChallenge.getJobNameByIdNull(jobId);
    }

    public void startQuest(int questId)
    {
        MapleQuest.getInstance(questId).start(getPlayer(), getNpc());
    }

    public int getNpc()
    {
        return this.npcId;
    }

    public void completeQuest(int questId)
    {
        MapleQuest.getInstance(questId).complete(getPlayer(), getNpc());
    }

    public void forfeitQuest(int questId)
    {
        MapleQuest.getInstance(questId).forfeit(getPlayer());
    }

    public void forceStartQuest(int questId)
    {
        MapleQuest.getInstance(questId).forceStart(getPlayer(), getNpc(), null);
    }

    public void forceCompleteQuest(int questId)
    {
        MapleQuest.getInstance(questId).forceComplete(getPlayer(), getNpc());
    }

    public int getMeso()
    {
        return getPlayer().getMeso();
    }

    public void gainAp(int amount)
    {
        this.c.getPlayer().gainAp((short) amount);
    }

    public void expandInventory(byte type, int amt)
    {
        this.c.getPlayer().expandInventory(type, amt);
    }

    public void unequipEverything()
    {
        MapleInventory equipped = getPlayer().getInventory(MapleInventoryType.EQUIPPED);
        MapleInventory equip = getPlayer().getInventory(MapleInventoryType.EQUIP);
        List<Short> itemIds = new LinkedList<>();
        for (Item item : equipped.newList())
        {
            itemIds.add(item.getPosition());
        }
        for (short ids : itemIds)
        {
            MapleInventoryManipulator.unequip(getC(), ids, equip.getNextFreeSlot());
        }
    }

    public void showEffect(boolean broadcast, String effect)
    {
        if (broadcast)
        {
            this.c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.showEffect(effect));
        }
        else
        {
            this.c.getSession().write(MaplePacketCreator.showEffect(effect));
        }
    }

    public void playSound(boolean broadcast, String sound)
    {
        if (broadcast)
        {
            this.c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.playSound(sound));
        }
        else
        {
            this.c.getSession().write(MaplePacketCreator.playSound(sound));
        }
    }

    public void environmentChange(boolean broadcast, String env)
    {
        if (broadcast)
        {
            this.c.getPlayer().getMap().broadcastMessage(MaplePacketCreator.environmentChange(env, 2));
        }
        else
        {
            this.c.getSession().write(MaplePacketCreator.environmentChange(env, 2));
        }
    }

    public void updateBuddyCapacity(int capacity)
    {
        this.c.getPlayer().setBuddyCapacity((byte) capacity);
    }

    public int getBuddyCapacity()
    {
        return this.c.getPlayer().getBuddyCapacity();
    }

    public int partyMembersInMap()
    {
        int inMap = 0;
        if (getPlayer().getParty() == null)
        {
            return inMap;
        }
        for (MapleCharacter chr : getPlayer().getMap().getCharactersThreadsafe())
        {
            if ((chr.getParty() != null) && (chr.getParty().getId() == getPlayer().getParty().getId()))
            {
                inMap++;
            }
        }
        return inMap;
    }

    public List<MapleCharacter> getPartyMembers()
    {
        if (getPlayer().getParty() == null)
        {
            return null;
        }
        List<MapleCharacter> chars = new LinkedList<>();
        for (MaplePartyCharacter partychr : getPlayer().getParty().getMembers())
        {
            for (ChannelServer channel : ChannelServer.getAllInstances())
            {
                MapleCharacter chr = channel.getPlayerStorage().getCharacterById(partychr.getId());
                if (chr != null) chars.add(chr);
            }
        }
        MaplePartyCharacter partychr;
        return chars;
    }

    public void warpPartyWithExp(int mapId, int exp)
    {
        if (getPlayer().getParty() == null)
        {
            warp(mapId, 0);
            gainExp(exp);
            return;
        }
        MapleMap target = getMap(mapId);
        for (MaplePartyCharacter partychr : getPlayer().getParty().getMembers())
        {
            MapleCharacter chr = this.c.getChannelServer().getPlayerStorage().getCharacterByName(partychr.getName());
            if (((chr.getEventInstance() == null) && (getPlayer().getEventInstance() == null)) || (chr.getEventInstance() == getPlayer().getEventInstance()))
            {
                chr.changeMap(target, target.getPortal(0));
                chr.gainExp(exp, true, false, true);
            }
        }
    }

    public void warpPartyWithExpMeso(int mapId, int exp, int meso)
    {
        if (getPlayer().getParty() == null)
        {
            warp(mapId, 0);
            gainExp(exp);
            gainMeso(meso);
            return;
        }
        MapleMap target = getMap(mapId);
        for (MaplePartyCharacter partychr : getPlayer().getParty().getMembers())
        {
            MapleCharacter chr = this.c.getChannelServer().getPlayerStorage().getCharacterByName(partychr.getName());
            if (((chr.getEventInstance() == null) && (getPlayer().getEventInstance() == null)) || (chr.getEventInstance() == getPlayer().getEventInstance()))
            {
                chr.changeMap(target, target.getPortal(0));
                chr.gainExp(exp, true, false, true);
                chr.gainMeso(meso, true);
            }
        }
    }

    public int getSquadAvailability(String type)
    {
        MapleSquad squad = this.c.getChannelServer().getMapleSquad(type);
        if (squad == null)
        {
            return -1;
        }
        return squad.getStatus();
    }

    public boolean registerSquad(String type, int minutes, String startText)
    {
        if (this.c.getChannelServer().getMapleSquad(type) == null)
        {


            MapleSquad squad = new MapleSquad(this.c.getChannel(), type, this.c.getPlayer(), minutes * 60 * 1000, startText);


            boolean ret = this.c.getChannelServer().addMapleSquad(squad, type);
            if (ret)
            {
                MapleMap map = this.c.getPlayer().getMap();
                map.broadcastMessage(MaplePacketCreator.getClock(minutes * 60));
                map.broadcastMessage(MaplePacketCreator.serverNotice(6, this.c.getPlayer().getName() + startText));
            }
            else
            {
                squad.clear();
            }
            return ret;
        }
        return false;
    }

    public boolean getSquadList(String type, byte type_)
    {
        try
        {
            MapleSquad squad = this.c.getChannelServer().getMapleSquad(type);
            if (squad == null)
            {
                return false;
            }
            if ((type_ == 0) || (type_ == 3))
            {
                sendNext(squad.getSquadMemberString(type_));
            }
            else if (type_ == 1)
            {
                sendSimple(squad.getSquadMemberString(type_));
            }
            else if (type_ == 2)
            {
                if (squad.getBannedMemberSize() > 0)
                {
                    sendSimple(squad.getSquadMemberString(type_));
                }
                else
                {
                    sendNext(squad.getSquadMemberString(type_));
                }
            }
            return true;
        }
        catch (NullPointerException ex)
        {
            FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", ex);
        }
        return false;
    }

    public byte isSquadLeader(String type)
    {
        MapleSquad squad = this.c.getChannelServer().getMapleSquad(type);
        if (squad != null)
        {
            if ((squad.getLeader() != null) && (squad.getLeader().getId() == this.c.getPlayer().getId()))
            {
                return 1;
            }
            return 0;
        }

        return -1;
    }

    public boolean reAdd(String eim, String squad)
    {
        EventInstanceManager eimz = getDisconnected(eim);
        MapleSquad squadz = getSquad(squad);
        if ((eimz != null) && (squadz != null))
        {
            squadz.reAddMember(getPlayer());
            eimz.registerPlayer(getPlayer());
            return true;
        }
        return false;
    }

    public MapleSquad getSquad(String type)
    {
        return this.c.getChannelServer().getMapleSquad(type);
    }

    public void banMember(String type, int pos)
    {
        MapleSquad squad = this.c.getChannelServer().getMapleSquad(type);
        if (squad != null)
        {
            squad.banMember(pos);
        }
    }

    public void acceptMember(String type, int pos)
    {
        MapleSquad squad = this.c.getChannelServer().getMapleSquad(type);
        if (squad != null)
        {
            squad.acceptMember(pos);
        }
    }

    public int addMember(String type, boolean join)
    {
        MapleSquad squad = this.c.getChannelServer().getMapleSquad(type);
        if (squad != null)
        {
            return squad.addMember(this.c.getPlayer(), join);
        }
        return -1;
    }

    public byte isSquadMember(String type)
    {
        MapleSquad squad = this.c.getChannelServer().getMapleSquad(type);
        if (squad != null)
        {
            if (squad.containsMember(this.c.getPlayer())) return 1;
            if (squad.isBanned(this.c.getPlayer()))
            {
                return 2;
            }
            return 0;
        }

        return -1;
    }

    public void resetReactors()
    {
        getPlayer().getMap().resetReactors();
    }

    public void genericGuildMessage(int code)
    {
        this.c.getSession().write(tools.packet.GuildPacket.genericGuildMessage((byte) code));
    }

    public void disbandGuild()
    {
        int gid = this.c.getPlayer().getGuildId();
        if ((gid <= 0) || (this.c.getPlayer().getGuildRank() != 1))
        {
            return;
        }
        WorldGuildService.getInstance().disbandGuild(gid);
    }

    public void increaseGuildCapacity(boolean trueMax)
    {
        increaseGuildCapacity(trueMax, 50000000);
    }

    public void increaseGuildCapacity(boolean trueMax, int meso)
    {
        if ((this.c.getPlayer().getMeso() < meso) && (!trueMax))
        {
            this.c.getSession().write(MaplePacketCreator.serverNotice(1, "金币不足.要金币: " + meso));
            return;
        }
        int gid = this.c.getPlayer().getGuildId();
        if (gid <= 0)
        {
            return;
        }
        if (WorldGuildService.getInstance().increaseGuildCapacity(gid, trueMax))
        {
            if (!trueMax)
            {
                this.c.getPlayer().gainMeso(-meso, true, true);
            }
            else
            {
                gainGP(40536);
            }
        }
        else if (!trueMax)
        {
            sendNext("请检查家族成员是否到达上限. (最大人数: 100)");
        }
        else
        {
            sendNext("请检查家族成员是否到达上限, if you have the GP needed or if subtracting GP would decrease a guild level. (最大人数: 200)");
        }
    }

    public void displayGuildRanks()
    {
        displayGuildRanks(false);
    }

    public void displayGuildRanks(boolean show)
    {
        this.c.getSession().write(tools.packet.GuildPacket.showGuildRanks(this.id, MapleGuildRanking.getInstance().getRank(), show));
    }

    public int getCreateGuildCost()
    {
        return this.c.getChannelServer().getCreateGuildCost();
    }

    public boolean removePlayerFromInstance()
    {
        if (this.c.getPlayer().getEventInstance() != null)
        {
            this.c.getPlayer().getEventInstance().removePlayer(this.c.getPlayer());
            return true;
        }
        return false;
    }

    public boolean isPlayerInstance()
    {
        return this.c.getPlayer().getEventInstance() != null;
    }

    public void changeStat(byte slot, int type, int amount)
    {
        Equip sel = (Equip) this.c.getPlayer().getInventory(MapleInventoryType.EQUIPPED).getItem(slot);
        switch (type)
        {
            case 0:
                sel.setStr((short) amount);
                break;
            case 1:
                sel.setDex((short) amount);
                break;
            case 2:
                sel.setInt((short) amount);
                break;
            case 3:
                sel.setLuk((short) amount);
                break;
            case 4:
                sel.setHp((short) amount);
                break;
            case 5:
                sel.setMp((short) amount);
                break;
            case 6:
                sel.setWatk((short) amount);
                break;
            case 7:
                sel.setMatk((short) amount);
                break;
            case 8:
                sel.setWdef((short) amount);
                break;
            case 9:
                sel.setMdef((short) amount);
                break;
            case 10:
                sel.setAcc((short) amount);
                break;
            case 11:
                sel.setAvoid((short) amount);
                break;
            case 12:
                sel.setHands((short) amount);
                break;
            case 13:
                sel.setSpeed((short) amount);
                break;
            case 14:
                sel.setJump((short) amount);
                break;
            case 15:
                sel.setUpgradeSlots((byte) amount);
                break;
            case 16:
                sel.setViciousHammer((byte) amount);
                break;
            case 17:
                sel.setLevel((byte) amount);
                break;
            case 18:
                sel.setState((byte) amount);
                break;
            case 19:
                sel.setEnhance((byte) amount);
                break;
            case 20:
                sel.setPotential1(amount);
                break;
            case 21:
                sel.setPotential2(amount);
                break;
            case 22:
                sel.setPotential3(amount);
                break;
            case 23:
                sel.setOwner(getText());
                break;
        }


        this.c.getPlayer().equipChanged();
        fakeRelog();
    }

    public String getText()
    {
        return this.getText;
    }

    public void fakeRelog()
    {
        if ((!this.c.getPlayer().isAlive()) || (this.c.getPlayer().getEventInstance() != null) || (server.maps.FieldLimitType.ChannelSwitch.check(this.c.getPlayer().getMap().getFieldLimit())))
        {
            this.c.getPlayer().dropMessage(1, "刷新人物数据失败.");
            return;
        }
        this.c.getPlayer().dropMessage(5, "正在刷新人数据.请等待...");
        this.c.getPlayer().fakeRelog();
    }

    public void changePotentialStat(byte slot, int type, int amount)
    {
        Equip sel = (Equip) this.c.getPlayer().getInventory(MapleInventoryType.EQUIPPED).getItem(slot);
        switch (type)
        {
            case 0:
                if (amount == 0)
                {
                    sel.setPotential1(-5);
                }
                else if (amount == 1)
                {
                    sel.setPotential1(-6);
                }
                else if (amount == 2)
                {
                    sel.setPotential1(-7);
                }
                else if (amount == 3)
                {
                    sel.setPotential1(-8);
                }
                break;
            case 1:
                sel.setPotential1(amount);
                sel.setStateMsg(3);
                break;
            case 2:
                sel.setPotential2(amount);
                sel.setStateMsg(3);
                break;
            case 3:
                sel.setPotential3(amount);
                sel.setStateMsg(3);
                break;
        }


        this.c.getPlayer().equipChanged();
        fakeRelog();
    }

    public void openDuey()
    {
        this.c.getPlayer().setConversation(2);
        this.c.getSession().write(MaplePacketCreator.sendDuey((byte) 9, null));
    }

    public void openMerchantItemStore()
    {
        this.c.getPlayer().setConversation(3);
        this.c.getSession().write(tools.packet.PlayerShopPacket.merchItemStore((byte) 40));
    }

    public void sendPVPWindow()
    {
        this.c.getSession().write(MaplePacketCreator.sendPVPWindow(0));
        this.c.getSession().write(MaplePacketCreator.sendPVPMaps());
    }

    public void sendPartyWindow()
    {
        this.c.getSession().write(MaplePacketCreator.sendPartyWindow(this.id));
    }

    public void sendPartyWindow(int id)
    {
        this.c.getSession().write(MaplePacketCreator.sendPartyWindow(id));
    }

    public void sendRepairWindow()
    {
        this.c.getSession().write(MaplePacketCreator.sendRepairWindow(this.id));
    }

    public void sendProfessionWindow()
    {
        this.c.getSession().write(MaplePacketCreator.sendProfessionWindow(0));
    }

    public void sendEventWindow()
    {
        this.c.getSession().write(MaplePacketCreator.sendEventWindow(0));
    }

    public void sendLinkSkillWindow(int skillId)
    {
        if (hasSkill(skillId))
        {
            this.c.getSession().write(MaplePacketCreator.sendLinkSkillWindow(skillId));
        }
    }

    public boolean hasSkill(int skillid)
    {
        Skill theSkill = SkillFactory.getSkill(skillid);
        if (theSkill != null)
        {
            return this.c.getPlayer().getSkillLevel(theSkill) > 0;
        }
        return false;
    }

    public int getDojoPoints()
    {
        return dojo_getPts();
    }

    public int getDojoRecord()
    {
        return this.c.getPlayer().getIntNoRecord(150101);
    }

    public void setDojoRecord(boolean reset)
    {
        if (reset)
        {
            this.c.getPlayer().getQuestNAdd(MapleQuest.getInstance(150101)).setCustomData("0");
            this.c.getPlayer().getQuestNAdd(MapleQuest.getInstance(150100)).setCustomData("0");
        }
        else
        {
            this.c.getPlayer().getQuestNAdd(MapleQuest.getInstance(150101)).setCustomData(String.valueOf(this.c.getPlayer().getIntRecord(150101) + 1));
        }
    }

    public boolean start_DojoAgent(boolean dojo, boolean party)
    {
        if (dojo)
        {
            return server.maps.events.Event_DojoAgent.warpStartDojo(this.c.getPlayer(), party);
        }
        return server.maps.events.Event_DojoAgent.warpStartAgent(this.c.getPlayer(), party);
    }

    public boolean start_PyramidSubway(int pyramid)
    {
        if (pyramid >= 0)
        {
            return server.maps.events.Event_PyramidSubway.warpStartPyramid(this.c.getPlayer(), pyramid);
        }
        return server.maps.events.Event_PyramidSubway.warpStartSubway(this.c.getPlayer());
    }

    public boolean bonus_PyramidSubway(int pyramid)
    {
        if (pyramid >= 0)
        {
            return server.maps.events.Event_PyramidSubway.warpBonusPyramid(this.c.getPlayer(), pyramid);
        }
        return server.maps.events.Event_PyramidSubway.warpBonusSubway(this.c.getPlayer());
    }

    public short getKegs()
    {
        return this.c.getChannelServer().getFireWorks().getKegsPercentage();
    }

    public void giveKegs(int kegs)
    {
        this.c.getChannelServer().getFireWorks().giveKegs(this.c.getPlayer(), kegs);
    }

    public short getSunshines()
    {
        return this.c.getChannelServer().getFireWorks().getSunsPercentage();
    }

    public void addSunshines(int kegs)
    {
        this.c.getChannelServer().getFireWorks().giveSuns(this.c.getPlayer(), kegs);
    }

    public short getDecorations()
    {
        return this.c.getChannelServer().getFireWorks().getDecsPercentage();
    }

    public void addDecorations(int kegs)
    {
        try
        {
            this.c.getChannelServer().getFireWorks().giveDecs(this.c.getPlayer(), kegs);
        }
        catch (Exception e)
        {
            _log.error("addDecorations 错误", e);
        }
    }

    public MapleCarnivalParty getCarnivalParty()
    {
        return this.c.getPlayer().getCarnivalParty();
    }

    public MapleCarnivalChallenge getNextCarnivalRequest()
    {
        return this.c.getPlayer().getNextCarnivalRequest();
    }

    public MapleCarnivalChallenge getCarnivalChallenge(MapleCharacter chr)
    {
        return new MapleCarnivalChallenge(chr);
    }

    public void maxStats()
    {
        Map<MapleStat, Long> statup = new EnumMap(MapleStat.class);
        this.c.getPlayer().getStat().str = Short.MAX_VALUE;
        this.c.getPlayer().getStat().dex = Short.MAX_VALUE;
        this.c.getPlayer().getStat().int_ = Short.MAX_VALUE;
        this.c.getPlayer().getStat().luk = Short.MAX_VALUE;

        this.c.getPlayer().getStat().maxhp = this.c.getPlayer().getMaxHpForSever();
        this.c.getPlayer().getStat().maxmp = this.c.getPlayer().getMaxMpForSever();
        this.c.getPlayer().getStat().setHp(this.c.getPlayer().getMaxHpForSever(), this.c.getPlayer());
        this.c.getPlayer().getStat().setMp(this.c.getPlayer().getMaxMpForSever(), this.c.getPlayer());

        statup.put(MapleStat.力量, 32767L);
        statup.put(MapleStat.敏捷, 32767L);
        statup.put(MapleStat.运气, 32767L);
        statup.put(MapleStat.智力, 32767L);
        statup.put(MapleStat.HP, (long) this.c.getPlayer().getMaxHpForSever());
        statup.put(MapleStat.MAXHP, (long) this.c.getPlayer().getMaxHpForSever());
        statup.put(MapleStat.MP, (long) this.c.getPlayer().getMaxMpForSever());
        statup.put(MapleStat.MAXMP, (long) this.c.getPlayer().getMaxMpForSever());
        this.c.getPlayer().getStat().recalcLocalStats(this.c.getPlayer());
        this.c.getSession().write(MaplePacketCreator.updatePlayerStats(statup, this.c.getPlayer()));
    }

    public Triple<String, Map<Integer, String>, Long> getSpeedRun(String typ)
    {
        handling.world.party.ExpeditionType types = handling.world.party.ExpeditionType.valueOf(typ);
        if (server.SpeedRunner.getSpeedRunData(types) != null)
        {
            return server.SpeedRunner.getSpeedRunData(types);
        }
        return new Triple("", new HashMap(), 0L);
    }

    public boolean getSR(Triple<String, Map<Integer, String>, Long> ma, int sel)
    {
        if ((((Map) ma.mid).get(sel) == null) || (((String) ((Map) ma.mid).get(sel)).length() <= 0))
        {
            dispose();
            return false;
        }
        sendOk((String) ((Map) ma.mid).get(sel));
        return true;
    }

    public Equip getEquip(int itemid)
    {
        return (Equip) MapleItemInformationProvider.getInstance().getEquipById(itemid);
    }

    public void setExpiration(Object statsSel, long expire)
    {
        if ((statsSel instanceof Equip))
        {
            ((Equip) statsSel).setExpiration(System.currentTimeMillis() + expire * 24L * 60L * 60L * 1000L);
        }
    }

    public void setLock(Object statsSel)
    {
        if ((statsSel instanceof Equip))
        {
            Equip eq = (Equip) statsSel;
            if (eq.getExpiration() == -1L)
            {
                eq.setFlag((byte) (eq.getFlag() | ItemFlag.LOCK.getValue()));
            }
            else
            {
                eq.setFlag((byte) (eq.getFlag() | ItemFlag.UNTRADEABLE.getValue()));
            }
        }
    }

    public void setItemLock()
    {
        setItemLock(true);
    }

    public void setItemLock(boolean lock)
    {
        setItemLock(1, lock);
    }

    public void setItemLock(int slot, boolean lock)
    {
        Item item = this.c.getPlayer().getInventory(MapleInventoryType.EQUIP).getItem((byte) slot);
        if (item == null)
        {
            this.c.getPlayer().dropMessage(6, "装备栏的第[" + slot + "]个道具为空，操作失败。");
            return;
        }
        short flag = (short) ItemFlag.LOCK.getValue();
        if (lock)
        {
            if (ItemFlag.LOCK.check(item.getFlag()))
            {
                this.c.getPlayer().dropMessage(6, "装备栏的第[" + slot + "]个道具已经是锁定状态，无需进行此操作。");
                return;
            }
            item.addFlag(flag);
        }
        else
        {
            if (!ItemFlag.LOCK.check(item.getFlag()))
            {
                this.c.getPlayer().dropMessage(6, "装备栏的第[" + slot + "]个道具不是锁定状态，无需进行此操作。");
                return;
            }
            item.removeFlag(flag);
        }
        this.c.getPlayer().forceUpdateItem(item);
    }

    public boolean addFromDrop(Object statsSel)
    {
        if ((statsSel instanceof Item))
        {
            Item it = (Item) statsSel;
            return (MapleInventoryManipulator.checkSpace(getClient(), it.getItemId(), it.getQuantity(), it.getOwner())) && (MapleInventoryManipulator.addFromDrop(getClient(), it, false));
        }
        return false;
    }

    public boolean replaceItem(int slot, int invType, Object statsSel, int upgradeSlots)
    {
        return replaceItem(slot, invType, statsSel, upgradeSlots, "Slots");
    }

    public boolean replaceItem(int slot, int invType, Object statsSel, int offset, String type)
    {
        return replaceItem(slot, invType, statsSel, offset, type, false);
    }

    public boolean replaceItem(int slot, int invType, Object statsSel, int offset, String type, boolean takeSlot)
    {
        MapleInventoryType inv = MapleInventoryType.getByType((byte) invType);
        if (inv == null)
        {
            return false;
        }
        Item item = getPlayer().getInventory(inv).getItem((byte) slot);
        if ((item == null) || ((statsSel instanceof Item)))
        {
            item = (Item) statsSel;
        }
        if (offset > 0)
        {
            if (inv != MapleInventoryType.EQUIP)
            {
                return false;
            }
            Equip eq = (Equip) item;
            if (takeSlot)
            {
                if (eq.getUpgradeSlots() < 1)
                {
                    return false;
                }
                eq.setUpgradeSlots((byte) (eq.getUpgradeSlots() - 1));

                if (eq.getExpiration() == -1L)
                {
                    eq.setFlag((byte) (eq.getFlag() | ItemFlag.LOCK.getValue()));
                }
                else
                {
                    eq.setFlag((byte) (eq.getFlag() | ItemFlag.UNTRADEABLE.getValue()));
                }
            }
            if (type.equalsIgnoreCase("Slots"))
            {
                eq.setUpgradeSlots((byte) (eq.getUpgradeSlots() + offset));
                eq.setViciousHammer((byte) (eq.getViciousHammer() + offset));
            }
            else if (type.equalsIgnoreCase("Level"))
            {
                eq.setLevel((byte) (eq.getLevel() + offset));
            }
            else if (type.equalsIgnoreCase("Hammer"))
            {
                eq.setViciousHammer((byte) (eq.getViciousHammer() + offset));
            }
            else if (type.equalsIgnoreCase("STR"))
            {
                eq.setStr((short) (eq.getStr() + offset));
            }
            else if (type.equalsIgnoreCase("DEX"))
            {
                eq.setDex((short) (eq.getDex() + offset));
            }
            else if (type.equalsIgnoreCase("INT"))
            {
                eq.setInt((short) (eq.getInt() + offset));
            }
            else if (type.equalsIgnoreCase("LUK"))
            {
                eq.setLuk((short) (eq.getLuk() + offset));
            }
            else if (type.equalsIgnoreCase("HP"))
            {
                eq.setHp((short) (eq.getHp() + offset));
            }
            else if (type.equalsIgnoreCase("MP"))
            {
                eq.setMp((short) (eq.getMp() + offset));
            }
            else if (type.equalsIgnoreCase("WATK"))
            {
                eq.setWatk((short) (eq.getWatk() + offset));
            }
            else if (type.equalsIgnoreCase("MATK"))
            {
                eq.setMatk((short) (eq.getMatk() + offset));
            }
            else if (type.equalsIgnoreCase("WDEF"))
            {
                eq.setWdef((short) (eq.getWdef() + offset));
            }
            else if (type.equalsIgnoreCase("MDEF"))
            {
                eq.setMdef((short) (eq.getMdef() + offset));
            }
            else if (type.equalsIgnoreCase("ACC"))
            {
                eq.setAcc((short) (eq.getAcc() + offset));
            }
            else if (type.equalsIgnoreCase("Avoid"))
            {
                eq.setAvoid((short) (eq.getAvoid() + offset));
            }
            else if (type.equalsIgnoreCase("Hands"))
            {
                eq.setHands((short) (eq.getHands() + offset));
            }
            else if (type.equalsIgnoreCase("Speed"))
            {
                eq.setSpeed((short) (eq.getSpeed() + offset));
            }
            else if (type.equalsIgnoreCase("Jump"))
            {
                eq.setJump((short) (eq.getJump() + offset));
            }
            else if (type.equalsIgnoreCase("ItemEXP"))
            {
                eq.setItemEXP(eq.getItemEXP() + offset);
            }
            else if (type.equalsIgnoreCase("Expiration"))
            {
                eq.setExpiration(eq.getExpiration() + offset);
            }
            else if (type.equalsIgnoreCase("Flag"))
            {
                eq.setFlag((byte) (eq.getFlag() + offset));
            }
            item = eq.copy();
        }
        MapleInventoryManipulator.removeFromSlot(getClient(), inv, (short) slot, item.getQuantity(), false);
        return MapleInventoryManipulator.addFromDrop(getClient(), item, false);
    }

    public boolean isCash(int itemId)
    {
        return MapleItemInformationProvider.getInstance().isCash(itemId);
    }

    public int getTotalStat(int itemId)
    {
        return MapleItemInformationProvider.getInstance().getTotalStat((Equip) MapleItemInformationProvider.getInstance().getEquipById(itemId));
    }

    public int getReqLevel(int itemId)
    {
        return MapleItemInformationProvider.getInstance().getReqLevel(itemId);
    }

    public MapleStatEffect getEffect(int buff)
    {
        return MapleItemInformationProvider.getInstance().getItemEffect(buff);
    }

    public void buffGuild(int buff, int duration, String msg)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        MapleStatEffect mse;
        if ((ii.getItemEffect(buff) != null) && (getPlayer().getGuildId() > 0))
        {
            mse = ii.getItemEffect(buff);
            for (ChannelServer cserv : ChannelServer.getAllInstances())
            {
                for (MapleCharacter chr : cserv.getPlayerStorage().getAllCharacters())
                {
                    if (chr.getGuildId() == getPlayer().getGuildId())
                    {
                        mse.applyTo(chr, chr, true, null, duration);
                        chr.dropMessage(5, "Your guild has gotten a " + msg + " buff.");
                    }
                }
            }
        }
    }

    public boolean createAlliance(String alliancename)
    {
        MapleParty pt = this.c.getPlayer().getParty();
        MapleCharacter otherChar = this.c.getChannelServer().getPlayerStorage().getCharacterById(pt.getMemberByIndex(1).getId());
        if ((otherChar == null) || (otherChar.getId() == this.c.getPlayer().getId()))
        {
            return false;
        }
        try
        {
            return WorldAllianceService.getInstance().createAlliance(alliancename, this.c.getPlayer().getId(), otherChar.getId(), this.c.getPlayer().getGuildId(), otherChar.getGuildId());
        }
        catch (Exception re)
        {
            _log.error("createAlliance 错误", re);
        }
        return false;
    }

    public boolean addCapacityToAlliance()
    {
        try
        {
            MapleGuild guild = WorldGuildService.getInstance().getGuild(this.c.getPlayer().getGuildId());
            if ((guild != null) && (this.c.getPlayer().getGuildRank() == 1) && (this.c.getPlayer().getAllianceRank() == 1) && (WorldAllianceService.getInstance().getAllianceLeader(guild.getAllianceId()) == this.c.getPlayer().getId()) && (WorldAllianceService.getInstance().changeAllianceCapacity(guild.getAllianceId())))
            {
                gainMeso(-10000000);
                return true;
            }
        }
        catch (Exception re)
        {
            _log.error("addCapacityToAlliance 错误", re);
        }
        return false;
    }

    public boolean disbandAlliance()
    {
        try
        {
            MapleGuild guild = WorldGuildService.getInstance().getGuild(this.c.getPlayer().getGuildId());
            if ((guild != null) && (this.c.getPlayer().getGuildRank() == 1) && (this.c.getPlayer().getAllianceRank() == 1) && (WorldAllianceService.getInstance().getAllianceLeader(guild.getAllianceId()) == this.c.getPlayer().getId()) && (WorldAllianceService.getInstance().disbandAlliance(guild.getAllianceId())))
            {
                return true;
            }
        }
        catch (Exception re)
        {
            _log.error("disbandAlliance 错误", re);
        }
        return false;
    }

    public void maxAllSkills()
    {
        HashMap<Skill, SkillEntry> sDate = new HashMap<>();
        for (Skill skil : SkillFactory.getAllSkills())
        {
            if ((GameConstants.isApplicableSkill(skil.getId())) && (skil.getId() < 90000000))
            {
                sDate.put(skil, new SkillEntry((byte) skil.getMaxLevel(), (byte) skil.getMaxLevel(), SkillFactory.getDefaultSExpiry(skil)));
            }
        }
        getPlayer().changeSkillsLevel(sDate);
        sDate.clear();
    }

    public void maxSkillsByJob()
    {
        List<Integer> skillIds = new ArrayList<>();
        HashMap<Skill, SkillEntry> sDate = new HashMap<>();
        for (Skill skil : SkillFactory.getAllSkills())
        {
            if ((skil.canBeLearnedBy(getPlayer().getJob())) && (!GameConstants.is新手职业(skil.getId() / 10000)) && (!skil.isSpecialSkill()) && (!skil.isHyperSkill()))
            {
                sDate.put(skil, new SkillEntry((byte) skil.getMaxLevel(), (byte) skil.getMaxLevel(), SkillFactory.getDefaultSExpiry(skil)));
                skillIds.add(skil.getId());
            }
        }
        getPlayer().changeSkillsLevel(sDate);
        Collections.sort(skillIds);
        String job;
        if (getPlayer().isShowPacket())
        {
            job = "Skill\\" + MapleCarnivalChallenge.getJobNameById(getPlayer().getJob()) + ".txt";
            for (Integer skillId : skillIds)
            {
                for (Map.Entry<Skill, SkillEntry> data : sDate.entrySet())
                    if (data.getKey().getId() == skillId)
                    {
                        String txt = "public static final int " + data.getKey().getName() + " = " + data.getKey().getId() + "; //技能最大等级" + data.getKey().getMaxLevel();
                        FileoutputUtil.log(job, txt, true);
                    }
            }
        }
        sDate.clear();
        skillIds.clear();
    }

    public void clearSkills()
    {
        this.c.getPlayer().clearSkills();
    }

    public void maxHyperSkillsByJob()
    {
        List<Integer> skillIds = new ArrayList<>();
        HashMap<Skill, SkillEntry> sDate = new HashMap<>();
        for (Skill skil : SkillFactory.getAllSkills())
        {
            if ((skil.canBeLearnedBy(getPlayer().getJob())) && (skil.isHyperSkill()))
            {
                sDate.put(skil, new SkillEntry((byte) skil.getMaxLevel(), (byte) skil.getMaxLevel(), SkillFactory.getDefaultSExpiry(skil)));
                skillIds.add(skil.getId());
            }
        }
        getPlayer().changeSkillsLevel(sDate);
        Collections.sort(skillIds);
        String job;
        if (getPlayer().isShowPacket())
        {
            job = "Skill\\" + MapleCarnivalChallenge.getJobNameById(getPlayer().getJob()) + ".txt";
            for (Integer skillId : skillIds)
            {
                for (Map.Entry<Skill, SkillEntry> data : sDate.entrySet())
                    if (data.getKey().getId() == skillId)
                    {
                        String txt = "public static final int " + data.getKey().getName() + " = " + data.getKey().getId() + "; //技能最大等级" + data.getKey().getMaxLevel();
                        FileoutputUtil.log(job, txt, true);
                    }
            }
        }
        sDate.clear();
        skillIds.clear();
    }

    public void maxBeginnerSkills()
    {
        List<Integer> skillIds = new ArrayList<>();
        HashMap<Skill, SkillEntry> sDate = new HashMap<>();
        for (Skill skil : SkillFactory.getAllSkills())
        {
            if ((skil.canBeLearnedBy(getPlayer().getJob())) && (skil.isBeginnerSkill()) && (!skil.isSpecialSkill()) && (!skil.isHyperSkill()))
            {
                sDate.put(skil, new SkillEntry((byte) skil.getMaxLevel(), (byte) skil.getMaxLevel(), SkillFactory.getDefaultSExpiry(skil)));
                skillIds.add(skil.getId());
            }
        }
        getPlayer().changeSkillsLevel(sDate);
        Collections.sort(skillIds);
        String job;
        if (getPlayer().isShowPacket())
        {
            job = "Skill\\" + MapleCarnivalChallenge.getJobNameById(getPlayer().getJob()) + "_新手技能.txt";
            for (Integer skillId : skillIds)
            {
                for (Map.Entry<Skill, SkillEntry> data : sDate.entrySet())
                    if (data.getKey().getId() == skillId)
                    {
                        String txt = "public static final int " + data.getKey().getName() + " = " + data.getKey().getId() + "; //技能最大等级" + data.getKey().getMaxLevel();
                        FileoutputUtil.log(job, txt, true);
                    }
            }
        }
        Integer skillId;
        sDate.clear();
        skillIds.clear();
    }

    public void resetStats(int str, int dex, int z, int luk)
    {
        this.c.getPlayer().resetStats(str, dex, z, luk);
    }

    public boolean dropItem(int slot, int invType, int quantity)
    {
        MapleInventoryType inv = MapleInventoryType.getByType((byte) invType);
        if (inv == null)
        {
            return false;
        }
        return MapleInventoryManipulator.drop(this.c, inv, (short) slot, (short) quantity, true);
    }

    public boolean removeItem(int slot, int invType, int quantity)
    {
        MapleInventoryType inv = MapleInventoryType.getByType((byte) invType);
        if (inv == null)
        {
            return false;
        }
        return MapleInventoryManipulator.removeFromSlot(this.c, inv, (short) slot, (short) quantity, true);
    }

    public List<Integer> getAllPotentialInfo()
    {
        List<Integer> list = new ArrayList(MapleItemInformationProvider.getInstance().getAllPotentialInfo().keySet());
        Collections.sort(list);
        return list;
    }

    public List<Integer> getAllPotentialInfoSearch(String content)
    {
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<Integer, List<StructItemOption>> integerListEntry : MapleItemInformationProvider.getInstance().getAllPotentialInfo().entrySet())
        {
            Map.Entry<Integer, List<StructItemOption>> i = integerListEntry;
            for (StructItemOption ii : i.getValue())
            {
                if (ii.toString().contains(content)) list.add(i.getKey());
            }
        }
        Collections.sort(list);
        return list;
    }

    public String getPotentialInfo(int id)
    {
        List<StructItemOption> potInfo = MapleItemInformationProvider.getInstance().getPotentialInfo(id);
        StringBuilder builder = new StringBuilder("#b#e以下是潜能ID为 ");
        builder.append(id);
        builder.append(" 的信息#n#k\r\n\r\n");
        int minLevel = 1;
        int maxLevel = 10;
        for (StructItemOption item : potInfo)
        {
            builder.append("#e等级范围 ");
            builder.append(minLevel);
            builder.append("~");
            builder.append(maxLevel);
            builder.append(": #n");
            builder.append(item.toString());
            minLevel += 10;
            maxLevel += 10;
            builder.append("\r\n");
        }
        return builder.toString();
    }

    public void sendRPS()
    {
        this.c.getSession().write(MaplePacketCreator.getRPSMode((byte) 8, -1, -1, -1));
    }

    public final void doWeddingEffect(Object ch)
    {
        final MapleCharacter chr = (MapleCharacter) ch;
        final MapleCharacter player = getPlayer();
        WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.yellowChat(player.getName() + ", 你愿意娶 " + chr.getName() + " 为妻吗？无论她将来是富有还是贫穷、或无论她将来身体健康或不适，你都愿意和她永远在一起吗？"));
        server.Timer.CloneTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                if ((chr == null) || (player == null))
                {
                    NPCConversationManager.this.warpMap(700000000, 0);
                }
                else WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.yellowChat(chr.getName() + ", 你愿意嫁给 " + player.getName() + " 吗？无论他将来是富有还是贫穷、或无论他将来身体健康或不适，你都愿意和他永远在一起吗？"));
            }
        }, 10000L);


        server.Timer.CloneTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                if ((chr == null) || (player == null))
                {
                    if (player != null)
                    {
                        NPCConversationManager.this.setQuestRecord(player, 160001, "3");
                        NPCConversationManager.this.setQuestRecord(player, 160002, "0");
                    }
                    else if (chr != null)
                    {
                        NPCConversationManager.this.setQuestRecord(chr, 160001, "3");
                        NPCConversationManager.this.setQuestRecord(chr, 160002, "0");
                    }
                    NPCConversationManager.this.warpMap(700000000, 0);
                }
                else
                {
                    NPCConversationManager.this.setQuestRecord(player, 160001, "2");
                    NPCConversationManager.this.setQuestRecord(chr, 160001, "2");
                    chr.setMarriageId(player.getId());
                    player.setMarriageId(chr.getId());
                    NPCConversationManager.this.sendNPCText("好，我以圣灵、圣父、圣子的名义宣布：" + player.getName() + " 和 " + chr.getName() + "结为夫妻。 希望你们在 " + chr.getClient().getChannelServer().getServerName() +
                            " 游戏中玩的愉快!", 9201002);
                    chr.getMap().startExtendedMapEffect("现在，新郎可以亲吻新娘了。 " + player.getName() + "!", 5120006);
                    WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.yellowChat("好，我以圣灵、圣父、圣子的名义宣布：" + player.getName() + " 和 " + chr.getName() + "结为夫妻。 希望你们在 " + chr.getClient().getChannelServer().getServerName() + " 游戏中玩的愉快!"));
                    if (chr.getGuildId() > 0)
                    {
                        WorldGuildService.getInstance().guildPacket(chr.getGuildId(), MaplePacketCreator.sendMarriage(false, chr.getName()));
                    }
                    if (chr.getFamilyId() > 0)
                    {
                        handling.world.WorldFamilyService.getInstance().familyPacket(chr.getFamilyId(), MaplePacketCreator.sendMarriage(true, chr.getName()), chr.getId());
                    }
                    if (player.getGuildId() > 0)
                    {
                        WorldGuildService.getInstance().guildPacket(player.getGuildId(), MaplePacketCreator.sendMarriage(false, player.getName()));
                    }
                    if (player.getFamilyId() > 0)
                        handling.world.WorldFamilyService.getInstance().familyPacket(player.getFamilyId(), MaplePacketCreator.sendMarriage(true, chr.getName()), player.getId());
                }
            }
        }, 20000L);
    }

    public void setQuestRecord(Object ch, int questid, String data)
    {
        ((MapleCharacter) ch).getQuestNAdd(MapleQuest.getInstance(questid)).setCustomData(data);
    }

    public void putKey(int key, int type, int action)
    {
        getPlayer().changeKeybinding(key, (byte) type, action);
        getClient().getSession().write(MaplePacketCreator.getKeymap(getPlayer()));
    }

    public void logDonator(String log, int previous_points)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO donorlog VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, MapleCharacterUtil.makeMapleReadable(getClient().getAccountName()));
            ps.setInt(2, getClient().getAccID());
            ps.setString(3, MapleCharacterUtil.makeMapleReadable(getPlayer().getName()));
            ps.setInt(4, getPlayer().getId());
            ps.setString(5, log);
            ps.setString(6, FileoutputUtil.CurrentReadable_Time());
            ps.setInt(7, previous_points);
            ps.setInt(8, getPlayer().getPoints());
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e)
        {
            _log.error("logDonator 错误", e);
        }
        String logg =
                MapleCharacterUtil.makeMapleReadable(getPlayer().getName()) + " [角色ID: " + getPlayer().getId() + "] " + " [账号: " + MapleCharacterUtil.makeMapleReadable(getClient().getAccountName()) + "] " + log + " [以前: " + previous_points + "] [现在: " + getPlayer().getPoints() + "]";
        FileoutputUtil.log("log\\Donator.log", logg);
    }

    public void doRing(String name, int itemid)
    {
        handling.channel.handler.PlayersHandler.DoRing(getClient(), name, itemid);
    }

    public int getNaturalStats(int itemid, String it)
    {
        Map<String, Integer> eqStats = MapleItemInformationProvider.getInstance().getEquipStats(itemid);
        if ((eqStats != null) && (eqStats.containsKey(it)))
        {
            return eqStats.get(it);
        }
        return 0;
    }

    public boolean isEligibleName(String t)
    {
        return (MapleCharacterUtil.canCreateChar(t, getPlayer().isGM())) && ((!handling.login.LoginInformationProvider.getInstance().isForbiddenName(t)) || (getPlayer().isGM()));
    }

    public String checkDrop(int mobId)
    {
        List<MonsterDropEntry> ranks = server.life.MapleMonsterInformationProvider.getInstance().retrieveDrop(mobId);
        if ((ranks != null) && (ranks.size() > 0))
        {
            int num = 0;

            StringBuilder name = new StringBuilder();
            for (MonsterDropEntry de : ranks)
            {
                if ((de.chance > 0) && ((de.questid <= 0) || ((de.questid > 0) && (MapleQuest.getInstance(de.questid).getName().length() > 0))))
                {
                    int itemId = de.itemId;
                    if (num == 0)
                    {
                        name.append("当前怪物 #o").append(mobId).append("# 的爆率为:\r\n");
                        name.append("--------------------------------------\r\n");
                    }


                    String namez = "#z" + itemId + "#";
                    if (itemId == 0)
                    {
                        itemId = 4031041;
                        namez = de.Minimum * getClient().getChannelServer().getMesoRate() + " - " + de.Maximum * getClient().getChannelServer().getMesoRate() + " 的金币";
                    }
                    int chance = de.chance * getClient().getChannelServer().getDropRate();
                    if (getPlayer().isAdmin())
                    {
                        name.append(num + 1).append(") #v").append(itemId).append("#").append(namez).append(" - ").append(Integer.valueOf(chance >= 999999 ? 1000000 : chance).doubleValue() / 10000.0D).append("%的爆率. ").append((de.questid > 0) && (MapleQuest.getInstance(de.questid).getName().length() > 0) ? "需要接受任务: " + MapleQuest.getInstance(de.questid).getName() : "").append("\r\n");
                    }
                    else
                    {
                        name.append(num + 1).append(") #v").append(itemId).append("#").append(namez).append((de.questid > 0) && (MapleQuest.getInstance(de.questid).getName().length() > 0) ?
                                "需要接受任务: " + MapleQuest.getInstance(de.questid).getName() : "").append("\r\n");
                    }
                    num++;
                }
            }
            if (name.length() > 0)
            {
                return name.toString();
            }
        }
        return "没有找到这个怪物的爆率数据。";
    }

    public String checkMapDrop()
    {
        List<MonsterGlobalDropEntry> ranks = new ArrayList(server.life.MapleMonsterInformationProvider.getInstance().getGlobalDrop());
        int mapid = this.c.getPlayer().getMap().getId();
        int cashServerRate = getClient().getChannelServer().getCashRate();
        int globalServerRate = getClient().getChannelServer().getGlobalRate();
        if ((ranks != null) && (ranks.size() > 0))
        {
            int num = 0;

            StringBuilder name = new StringBuilder();
            for (MonsterGlobalDropEntry de : ranks)
            {
                if ((de.continent < 0) || ((de.continent < 10) && (mapid / 100000000 == de.continent)) || ((de.continent < 100) && (mapid / 10000000 == de.continent)) || ((de.continent < 1000) && (mapid / 1000000 == de.continent)))
                {
                    int itemId = de.itemId;
                    if (num == 0)
                    {
                        name.append("当前地图 #r").append(mapid).append("#k - #m").append(mapid).append("# 的全局爆率为:");
                        name.append("\r\n--------------------------------------\r\n");
                    }
                    String names = "#z" + itemId + "#";
                    if ((itemId == 0) && (cashServerRate != 0))
                    {
                        itemId = 4031041;
                        names = de.Minimum * cashServerRate + " - " + de.Maximum * cashServerRate + " 的抵用卷";
                    }
                    int chance = de.chance * globalServerRate;
                    if (getPlayer().isAdmin())
                    {
                        name.append(num + 1).append(") #v").append(itemId).append("#").append(names).append(" - ").append(Integer.valueOf(chance >= 999999 ? 1000000 : chance).doubleValue() / 10000.0D).append("%的爆率. ").append((de.questid > 0) && (MapleQuest.getInstance(de.questid).getName().length() > 0) ? "需要接受任务: " + MapleQuest.getInstance(de.questid).getName() : "").append("\r\n");
                    }
                    else
                    {
                        name.append(num + 1).append(") #v").append(itemId).append("#").append(names).append((de.questid > 0) && (MapleQuest.getInstance(de.questid).getName().length() > 0) ?
                                "需要接受任务: " + MapleQuest.getInstance(de.questid).getName() : "").append("\r\n");
                    }
                    num++;
                }
            }
            if (name.length() > 0)
            {
                return name.toString();
            }
        }
        return "当前地图没有设置全局爆率。";
    }

    public void outputWithLogging(int mobId, String buff)
    {
        String file = "drop_data\\" + mobId + ".sql";
        FileoutputUtil.log(file, buff, true);
    }

    public String getLeftPadded(String in, char padchar, int length)
    {
        return tools.StringUtil.getLeftPaddedStr(in, padchar, length);
    }

    public void preparePokemonBattle(List<Integer> npcTeam, int restrictedLevel)
    {
        int theId = server.life.MapleLifeFactory.getRandomNPC();
        PokemonBattle wild = new PokemonBattle(getPlayer(), npcTeam, theId, restrictedLevel);
        getPlayer().changeMap(wild.getMap(), wild.getMap().getPortal(0));
        getPlayer().setBattle(wild);
        wild.initiate(getPlayer(), server.life.MapleLifeFactory.getNPC(theId));
    }

    public List<Integer> makeTeam(int lowRange, int highRange, int neededLevel, int restrictedLevel)
    {
        List<Integer> ret = new ArrayList<>();
        int averageLevel = 0;
        int numBattlers = 0;
        for (Battler b : getPlayer().getBattlers())
        {
            if (b != null)
            {
                if (b.getLevel() > averageLevel)
                {
                    averageLevel = b.getLevel();
                }
                numBattlers++;
            }
        }
        boolean hell = lowRange == highRange;
        if ((numBattlers < 3) || (averageLevel < neededLevel))
        {
            return null;
        }
        if (averageLevel > restrictedLevel)
        {
            averageLevel = restrictedLevel;
        }
        List<BattleConstants.PokedexEntry> pokeEntries = new ArrayList(getAllPokedex());
        Collections.shuffle(pokeEntries);
        while (ret.size() < numBattlers)
        {
            for (BattleConstants.PokedexEntry d : pokeEntries)
            {
                if (((d.dummyBattler.getStats().isBoss()) && (hell)) || ((!d.dummyBattler.getStats().isBoss()) && (!hell)))
                {
                    if (!hell)
                    {
                        if ((d.dummyBattler.getLevel() <= averageLevel + highRange) && (d.dummyBattler.getLevel() >= averageLevel + lowRange) && (Randomizer.nextInt(numBattlers) == 0))
                        {
                            ret.add(d.id);
                            if (ret.size() >= numBattlers)
                            {
                                break;
                            }
                        }
                    }
                    else if ((d.dummyBattler.getFamily().type != constants.BattleConstants.MobExp.EASY) && (d.dummyBattler.getLevel() >= neededLevel) && (d.dummyBattler.getLevel() <= averageLevel) && (Randomizer.nextInt(numBattlers) == 0))
                    {
                        ret.add(d.id);
                        if (ret.size() >= numBattlers)
                        {
                            break;
                        }
                    }
                }
            }
        }
        return ret;
    }

    public List<BattleConstants.PokedexEntry> getAllPokedex()
    {
        return constants.BattleConstants.getAllPokedex();
    }

    public constants.BattleConstants.HoldItem[] getAllHoldItems()
    {
        return constants.BattleConstants.HoldItem.values();
    }

    public void handleDivorce()
    {
        if (getPlayer().getMarriageId() <= 0)
        {
            sendNext("你还没结婚，怎么能离婚呢？");
            return;
        }
        int chz = handling.world.WorldFindService.getInstance().findChannel(getPlayer().getMarriageId());
        MapleRing mRing = getPlayer().getMarriageRing();
        if (chz == -1)
        {
            try
            {
                Connection con = DatabaseConnection.getConnection();
                PreparedStatement ps = con.prepareStatement("UPDATE queststatus SET customData = ? WHERE characterid = ? AND (quest = ? OR quest = ?)");
                ps.setString(1, "0");
                ps.setInt(2, getPlayer().getMarriageId());
                ps.setInt(3, 160001);
                ps.setInt(4, 160002);
                ps.executeUpdate();
                ps.close();
                ps = con.prepareStatement("UPDATE characters SET marriageid = ? WHERE id = ?");
                ps.setInt(1, 0);
                ps.setInt(2, getPlayer().getMarriageId());
                ps.executeUpdate();
                ps.close();
                if (mRing != null)
                {
                    ps = DatabaseConnection.getConnection().prepareStatement("DELETE FROM inventoryitems WHERE itemid = ? AND characterid = ?");
                    ps.setInt(1, mRing.getItemId());
                    ps.setInt(2, getPlayer().getMarriageId());
                    ps.executeUpdate();
                    ps.close();
                }
            }
            catch (SQLException e)
            {
                outputFileError(e);
                return;
            }
            if (mRing != null)
            {
                getPlayer().removeAll(mRing.getItemId(), true, true);
                MapleRing.removeRingFromDb(mRing.getRingId(), mRing.getPartnerRingId());
                WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.yellowChat("[系统公告] " + getPlayer().getName() + " 和 " + mRing.getPartnerName() + " 离婚了。"));
            }
            setQuestRecord(getPlayer(), 160001, "0");
            setQuestRecord(getPlayer(), 160002, "0");
            getPlayer().setMarriageId(0);
            sendNext("离婚成功.");
            return;
        }
        if (chz < -1)
        {
            sendNext("请确保你的伴侣是在线的.");
            return;
        }
        MapleCharacter cPlayer = ChannelServer.getInstance(chz).getPlayerStorage().getCharacterById(getPlayer().getMarriageId());
        if (cPlayer != null)
        {
            if (mRing != null)
            {
                cPlayer.removeAll(mRing.getItemId(), true, true);
                getPlayer().removeAll(mRing.getItemId(), true, true);
                MapleRing.removeRingFromDb(mRing.getRingId(), mRing.getPartnerRingId());
            }
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.yellowChat("[系统公告] " + getPlayer().getName() + " 和 " + cPlayer.getName() + " 离婚了。"));
            cPlayer.dropMessage(1, "你的伴侣和你离婚了.");
            cPlayer.setMarriageId(0);
            setQuestRecord(cPlayer, 160001, "0");
            setQuestRecord(getPlayer(), 160001, "0");
            setQuestRecord(cPlayer, 160002, "0");
            setQuestRecord(getPlayer(), 160002, "0");
            getPlayer().setMarriageId(0);
            sendNext("离婚成功...");
        }
        else
        {
            sendNext("出现了未知的错误...");
        }
    }

    public String getReadableMillis(long startMillis, long endMillis)
    {
        return tools.StringUtil.getReadableMillis(startMillis, endMillis);
    }

    public boolean canCreateUltimate()
    {
        if (getPlayer().getLevel() < 120)
        {
            return false;
        }
        int jobId = getPlayer().getJob();
        return (jobId == 1111) || (jobId == 1112) || (jobId == 1211) || (jobId == 1212) || (jobId == 1311) || (jobId == 1312) || (jobId == 1411) || (jobId == 1412) || (jobId == 1511) || (jobId == 1512);
    }

    public void sendUltimateExplorer()
    {
        getClient().getSession().write(MaplePacketCreator.ultimateExplorer());
    }

    public String getRankingInformation(int job)
    {
        StringBuilder sb = new StringBuilder();
        for (RankingWorker.RankingInformation pi : server.RankingWorker.getRankingInfo(job))
        {
            sb.append(pi.toString());
        }
        return sb.toString();
    }

    public String getPokemonRanking()
    {
        StringBuilder sb = new StringBuilder();
        for (RankingWorker.PokemonInformation pi : server.RankingWorker.getPokemonInfo())
        {
            sb.append(pi.toString());
        }
        return sb.toString();
    }

    public String getPokemonRanking_Caught()
    {
        StringBuilder sb = new StringBuilder();
        for (RankingWorker.PokedexInformation pi : server.RankingWorker.getPokemonCaught())
        {
            sb.append(pi.toString());
        }
        return sb.toString();
    }

    public String getPokemonRanking_Ratio()
    {
        StringBuilder sb = new StringBuilder();
        for (RankingWorker.PokebattleInformation pi : server.RankingWorker.getPokemonRatio())
        {
            sb.append(pi.toString());
        }
        return sb.toString();
    }

    public void sendPendant(boolean b)
    {
        this.c.getSession().write(MaplePacketCreator.pendantSlot(b));
    }

    public Triple<Integer, Integer, Integer> getCompensation()
    {
        Triple<Integer, Integer, Integer> ret = null;
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM compensationlog_confirmed WHERE chrname LIKE ?");
            ps.setString(1, getPlayer().getName());
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                ret = new Triple(rs.getInt("value"), rs.getInt("taken"), rs.getInt("donor"));
            }
            rs.close();
            ps.close();
            return ret;
        }
        catch (SQLException e)
        {
            FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", e);
        }
        return ret;
    }

    public boolean deleteCompensation(int taken)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE compensationlog_confirmed SET taken = ? WHERE chrname LIKE ?");
            ps.setInt(1, taken);
            ps.setString(2, getPlayer().getName());
            ps.executeUpdate();
            ps.close();
            return true;
        }
        catch (SQLException e)
        {
            FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", e);
        }
        return false;
    }

    public void testPacket(String testmsg)
    {
        this.c.getSession().write(MaplePacketCreator.testPacket(testmsg));
    }

    public void testPacket(int op)
    {
        this.c.getSession().write(tools.packet.FamilyPacket.sendFamilyMessage(op));
    }

    public void testPacket(String op, String msg)
    {
        this.c.getSession().write(MaplePacketCreator.testPacket(op, msg));
    }

    public short getSpace(byte type)
    {
        return getPlayer().getSpace(type);
    }

    public boolean haveSpace(int type)
    {
        return getPlayer().haveSpace(type);
    }

    public boolean haveSpaceForId(int itemid)
    {
        return getPlayer().haveSpaceForId(itemid);
    }

    public int getMoney()
    {
        int money = 0;
        try
        {
            int cid = getPlayer().getId();
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from bank where charid=?");
            ps.setInt(1, cid);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                money = rs.getInt("money");
            }
            else
            {
                PreparedStatement psu = con.prepareStatement("insert into bank (charid, money) VALUES (?, ?)");
                psu.setInt(1, cid);
                psu.setInt(2, 0);
                psu.executeUpdate();
                psu.close();
            }
            ps.close();
            rs.close();
        }
        catch (SQLException ex)
        {
            _log.error("银行存款获取信息发生错误", ex);
        }
        return money;
    }

    public int addMoney(int money, int type)
    {
        try
        {
            int cid = getPlayer().getId();
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from bank where charid=?");
            ps.setInt(1, cid);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                if ((type == 1) && (money > rs.getInt("money")))
                {
                    return -1;
                }

                ps = con.prepareStatement("UPDATE bank SET money =money+ " + money + " WHERE charid = " + cid + "");
                return ps.executeUpdate();
            }
            ps.close();
            rs.close();
        }
        catch (SQLException ex)
        {
            _log.error("银行存款添加数量发生错误", ex);
        }
        return 0;
    }

    public int getHyPay(int type)
    {
        return getPlayer().getHyPay(type);
    }

    public int addHyPay(int hypay)
    {
        return getPlayer().addHyPay(hypay);
    }

    public int delPayReward(int pay)
    {
        return getPlayer().delPayReward(pay);
    }

    public MapleCharacter getCharByName(String name)
    {
        try
        {
            return this.c.getChannelServer().getPlayerStorage().getCharacterByName(name);
        }
        catch (Exception ignored)
        {
        }
        return null;
    }


    public String EquipList(MapleClient c)
    {
        StringBuilder str = new StringBuilder();
        MapleInventory equip = c.getPlayer().getInventory(MapleInventoryType.EQUIP);
        List<String> stra = new LinkedList<>();
        for (Item item : equip.list())
        {
            stra.add("#L" + item.getPosition() + "##v" + item.getItemId() + "##l");
        }
        for (String strb : stra)
        {
            str.append(strb);
        }
        return str.toString();
    }


    public String UseList(MapleClient c)
    {
        StringBuilder str = new StringBuilder();
        MapleInventory use = c.getPlayer().getInventory(MapleInventoryType.USE);
        List<String> stra = new LinkedList<>();
        for (Item item : use.list())
        {
            stra.add("#L" + item.getPosition() + "##v" + item.getItemId() + "##l");
        }
        for (String strb : stra)
        {
            str.append(strb);
        }
        return str.toString();
    }


    public String CashList(MapleClient c)
    {
        StringBuilder str = new StringBuilder();
        MapleInventory cash = c.getPlayer().getInventory(MapleInventoryType.CASH);
        List<String> stra = new LinkedList<>();
        for (Item item : cash.list())
        {
            stra.add("#L" + item.getPosition() + "##v" + item.getItemId() + "##l");
        }
        for (String strb : stra)
        {
            str.append(strb);
        }
        return str.toString();
    }


    public String EtcList(MapleClient c)
    {
        StringBuilder str = new StringBuilder();
        MapleInventory etc = c.getPlayer().getInventory(MapleInventoryType.ETC);
        List<String> stra = new LinkedList<>();
        for (Item item : etc.list())
        {
            stra.add("#L" + item.getPosition() + "##v" + item.getItemId() + "##l");
        }
        for (String strb : stra)
        {
            str.append(strb);
        }
        return str.toString();
    }


    public String SetupList(MapleClient c)
    {
        StringBuilder str = new StringBuilder();
        MapleInventory setup = c.getPlayer().getInventory(MapleInventoryType.SETUP);
        List<String> stra = new LinkedList<>();
        for (Item item : setup.list())
        {
            stra.add("#L" + item.getPosition() + "##v" + item.getItemId() + "##l");
        }
        for (String strb : stra)
        {
            str.append(strb);
        }
        return str.toString();
    }


    public void deleteAll(int itemId)
    {
        MapleInventoryManipulator.removeAllById(getClient(), itemId, true);
    }


    public int getCurrentSharesPrice()
    {
        return ChannelServer.getInstance(1).getSharePrice();
    }

    public int getDollars()
    {
        return getPlayer().getDollars();
    }

    public int getShareLots()
    {
        return getPlayer().getShareLots();
    }

    public void addDollars(int n)
    {
        getPlayer().addDollars(n);
    }

    public void addShareLots(int n)
    {
        getPlayer().addShareLots(n);
    }

    public int useNebuliteGachapon()
    {
        try
        {
            if ((this.c.getPlayer().getInventory(MapleInventoryType.EQUIP).getNumFreeSlot() < 1) || (this.c.getPlayer().getInventory(MapleInventoryType.USE).getNumFreeSlot() < 1) || (this.c.getPlayer().getInventory(MapleInventoryType.SETUP).getNumFreeSlot() < 1) || (this.c.getPlayer().getInventory(MapleInventoryType.ETC).getNumFreeSlot() < 1) || (this.c.getPlayer().getInventory(MapleInventoryType.CASH).getNumFreeSlot() < 1))
            {
                return -1;
            }
            int grade = 0;
            int chance = Randomizer.nextInt(100);
            MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
            if (chance < 1)
            {
                grade = 3;
            }
            else if (chance < 5)
            {
                grade = 2;
            }
            else if (chance < 35)
            {
                grade = 1;
            }
            else
            {
                grade = Randomizer.nextInt(100) < 25 ? 5 : 0;
            }
            int newId = 0;
            if (grade == 5)
            {
                newId = 4420000;
            }
            else
            {
                List<StructItemOption> pots = new LinkedList(ii.getAllSocketInfo(grade).values());
                while (newId == 0)
                {
                    StructItemOption pot = pots.get(Randomizer.nextInt(pots.size()));
                    if (pot != null)
                    {
                        newId = pot.opID;
                    }
                }
            }
            Item item = MapleInventoryManipulator.addbyId_Gachapon(this.c, newId, (short) 1, "从星岩中获得 时间: " + FileoutputUtil.CurrentReadable_Time());
            if (item == null)
            {
                return -1;
            }
            if ((grade >= 2) && (grade != 5))
            {
                WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.getGachaponMega(this.c.getPlayer().getName(), " : 从星岩中获得{" + ii.getName(item.getItemId()) +
                        "}！大家一起恭喜他（她）吧！！！！", item, 2, this.c.getChannel()));
            }
            this.c.getSession().write(MaplePacketCreator.getShowItemGain(newId, (short) 1, true));
            return item.getItemId();
        }
        catch (Exception e)
        {
            System.out.println("[Error] Failed to use Nebulite Gachapon. " + e);
        }
        return -1;
    }

    public void giveMountSkill(int itemId, int mountSkillId, long period)
    {
        giveMountSkill(itemId, mountSkillId, period, false);
    }

    public void giveMountSkill(int itemId, int mountSkillId, long period, boolean test)
    {
        if ((mountSkillId > 0) && (haveItem(itemId)))
        {
            if (test)
            {
                System.err.println("骑宠技能 - 1 " + mountSkillId + " LinkedMountItem: " + mountSkillId % 1000);
            }
            mountSkillId = mountSkillId > 80001000 ? mountSkillId : client.PlayerStats.getSkillByJob(mountSkillId, this.c.getPlayer().getJob());
            int fk = GameConstants.getMountItem(mountSkillId, this.c.getPlayer());
            if (test)
            {
                System.err.println("骑宠技能 - 2 " + mountSkillId + " 骑宠ID: " + fk);
            }
            if ((fk > 0) && (mountSkillId < 80001000))
            {
                for (int i = 80001001; i < 80001999; i++)
                {
                    Skill skill = SkillFactory.getSkill(i);
                    if ((skill != null) && (GameConstants.getMountItem(skill.getId(), this.c.getPlayer()) == fk))
                    {
                        mountSkillId = i;
                        break;
                    }
                }
            }
            if (test)
            {
                System.err.println("骑宠技能 - 3 " + mountSkillId + " 技能是否为空: " + (SkillFactory.getSkill(mountSkillId) == null) + " 骑宠: " + (GameConstants.getMountItem(mountSkillId, this.c.getPlayer()) == 0));
            }
            if (this.c.getPlayer().getSkillLevel(mountSkillId) > 0)
            {
                this.c.getPlayer().dropMessage(1, "您已经拥有了[" + SkillFactory.getSkill(mountSkillId).getName() + "]这个骑宠的技能，无法使用该道具。");
            }
            else if ((SkillFactory.getSkill(mountSkillId) == null) || (GameConstants.getMountItem(mountSkillId, this.c.getPlayer()) == 0))
            {
                this.c.getPlayer().dropMessage(1, "暂时无法使用这个骑宠的技能.");
            }
            else if (period > 0L)
            {
                gainItem(itemId, (short) -1);
                this.c.getPlayer().changeSingleSkillLevel(SkillFactory.getSkill(mountSkillId), 1, (byte) 1, System.currentTimeMillis() + period * 24L * 60L * 60L * 1000L);
                this.c.getPlayer().dropMessage(1, "恭喜您获得[" + SkillFactory.getSkill(mountSkillId).getName() + "]骑宠技能 " + period + " 权。");
            }
            else if (period == -1L)
            {
                gainItem(itemId, (short) -1);
                this.c.getPlayer().changeSingleSkillLevel(SkillFactory.getSkill(mountSkillId), 1, (byte) 1, -1L);
                this.c.getPlayer().dropMessage(1, "恭喜您获得[" + SkillFactory.getSkill(mountSkillId).getName() + "]骑宠技能永久权。");
            }
        }
        else
        {
            this.c.getPlayer().dropMessage(1, "暂时无法使用这个骑宠的技能\r\n我的道具ID为: " + itemId);
        }
        this.c.getSession().write(MaplePacketCreator.enableActions());
    }


    public boolean checkLevelAndItem(int minLevel, int maxLevel, int itemId)
    {
        return checkLevelAndItem(minLevel, maxLevel, itemId, 2);
    }

    public boolean checkLevelAndItem(int minLevel, int maxLevel, int itemId, int minSize)
    {
        MapleParty party = this.c.getPlayer().getParty();
        if ((party == null) || (party.getLeader().getId() != this.c.getPlayer().getId()))
        {
            this.c.getPlayer().dropMessage(5, "您没有队伍 或者 不是队长..");
            return false;
        }
        int partySize = party.getMembers().size();
        if (partySize < minSize)
        {
            this.c.getPlayer().dropMessage(5, "队伍的人数成员不够 必须 " + minSize + " 人才可以开始组队任务，当前队伍人数: " + partySize);
            return false;
        }
        int chrSize = 0;
        for (MaplePartyCharacter partyPlayer : party.getMembers())
        {
            MapleCharacter player = getPlayer().getMap().getCharacterById(partyPlayer.getId());
            if (player == null)
            {
                this.c.getPlayer().dropMessage(5, "队伍中的成员 " + partyPlayer.getName() + " 不在线 或者 不在同一地图.");
            }
            else if ((player.getLevel() < minLevel) || (player.getLevel() > maxLevel))
            {
                this.c.getPlayer().dropMessage(5, "队伍中的成员 " + partyPlayer.getName() + " 等级不符合要求.等级限制: Lv." + minLevel + "～" + maxLevel);
            }
            else if (!player.haveItem(itemId))
            {
                this.c.getPlayer().dropMessage(5, "队伍中的成员 " + partyPlayer.getName() + " 没有开始组队任务需要的道具.");
            }
            else
            {
                chrSize++;
            }
        }
        return partySize == chrSize;
    }


    public void changeInnerSkill(int skillId, int skillevel, int position, int rank)
    {
        changeInnerSkill(skillId, skillevel, position, rank, false);
    }

    public void changeInnerSkill(int skillId, int skillevel, int position, int rank, boolean replace)
    {
        if ((replace) || (this.c.getPlayer().getInnerSkillIdByPos(position) <= 0))
        {
            this.c.getPlayer().changeInnerSkill(skillId, skillevel, (byte) position, (byte) rank);
        }
    }
}