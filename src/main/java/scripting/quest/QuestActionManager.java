package scripting.quest;

import javax.script.Invocable;

import client.MapleClient;
import scripting.npc.NPCConversationManager;
import server.quest.MapleQuest;
import tools.MaplePacketCreator;


public class QuestActionManager extends NPCConversationManager
{
    private final int quest;
    private final boolean start;

    public QuestActionManager(MapleClient c, int npc, int quest, boolean start, Invocable iv)
    {
        super(c, npc, quest, iv);
        this.quest = quest;
        this.start = start;
    }

    public int getQuest()
    {
        return this.quest;
    }

    public boolean isStart()
    {
        return this.start;
    }

    public void dispose()
    {
        QuestScriptManager.getInstance().dispose(this, getClient());
    }

    public void forceStartQuest()
    {
        MapleQuest.getInstance(this.quest).forceStart(getPlayer(), getNpc(), null);
    }

    public void forceStartQuest(String customData)
    {
        MapleQuest.getInstance(this.quest).forceStart(getPlayer(), getNpc(), customData);
    }

    public void forceCompleteQuest()
    {
        MapleQuest.getInstance(this.quest).forceComplete(getPlayer(), getNpc());
    }

    public String getQuestCustomData()
    {
        return this.c.getPlayer().getQuestNAdd(MapleQuest.getInstance(this.quest)).getCustomData();
    }

    public void setQuestCustomData(String customData)
    {
        this.c.getPlayer().getQuestNAdd(MapleQuest.getInstance(this.quest)).setCustomData(customData);
    }

    public void showCompleteQuestEffect()
    {
        this.c.getPlayer().getClient().getSession().write(MaplePacketCreator.showSpecialEffect(13));
        this.c.getPlayer().getMap().broadcastMessage(this.c.getPlayer(), MaplePacketCreator.showSpecialEffect(this.c.getPlayer().getId(), 13), false);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\scripting\quest\QuestActionManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */