package scripting.item;

import javax.script.Invocable;

import client.MapleClient;
import scripting.npc.NPCConversationManager;


public class ItemActionManager extends NPCConversationManager
{
    private final int itemId;

    public ItemActionManager(MapleClient c, int npc, int itemId, Invocable iv)
    {
        super(c, npc, itemId, iv);
        this.itemId = itemId;
    }

    public int getItem()
    {
        return this.itemId;
    }

    public int getItemId()
    {
        return this.itemId;
    }

    public void dispose()
    {
        ItemScriptManager.getInstance().dispose(this, getClient());
    }
}