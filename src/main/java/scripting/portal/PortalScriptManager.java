package scripting.portal;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import client.MapleClient;
import server.MaplePortal;
import tools.FileoutputUtil;

public class PortalScriptManager
{
    private static final PortalScriptManager instance = new PortalScriptManager();
    private static final javax.script.ScriptEngineFactory sef = new javax.script.ScriptEngineManager().getEngineByName("javascript").getFactory();
    private final Map<String, PortalScript> scripts = new HashMap<>();

    public static synchronized PortalScriptManager getInstance()
    {
        return instance;
    }

    public void executePortalScript(MaplePortal portal, MapleClient c)
    {
        PortalScript script = getPortalScript(portal.getScriptName());
        if (script != null)
        {
            try
            {
                script.enter(new PortalPlayerInteraction(c, portal));
                if (c.getPlayer().isAdmin())
                {
                    c.getPlayer().dropMessage(5, "执行Portal为:(" + portal.getScriptName() + ".js)的文件 在地图 " + c.getPlayer().getMapId() + " - " + c.getPlayer().getMap().getMapName());
                }
            }
            catch (Exception e)
            {
                if (c.getPlayer().isAdmin())
                {
                    c.getPlayer().dropMessage(5, "执行地图脚本过程中发生错误.请检查Portal为:( " + portal.getScriptName() + ".js)的文件." + e);
                }
                FileoutputUtil.log("log\\Script\\Portal_Script_Except.log", "执行地图脚本过程中发生错误.请检查Portal为:( " + portal.getScriptName() + ".js)的文件.\r\n错误信息:" + e);
            }
        }
        else
        {
            if (c.getPlayer().isAdmin())
            {
                c.getPlayer().dropMessage(5, "未找到Portal为:(" + portal.getScriptName() + ".js)的文件 在地图 " + c.getPlayer().getMapId() + " - " + c.getPlayer().getMap().getMapName());
            }
            FileoutputUtil.log("log\\Script\\Portal_Script_Except.log",
                    "执行地图脚本过程中发生错误.未找到Portal为:(" + portal.getScriptName() + ".js)的文件 在地图 " + c.getPlayer().getMapId() + " - " + c.getPlayer().getMap().getMapName());
        }
    }

    private PortalScript getPortalScript(String scriptName)
    {
        PortalScript script = null;
        if (this.scripts.containsKey(scriptName))
        {
            return this.scripts.get(scriptName);
        }
        java.io.File scriptFile = new java.io.File("scripts/portal/" + scriptName + ".js");
        if (!scriptFile.exists())
        {
            return null;
        }
        FileReader fr = null;
        javax.script.ScriptEngine portal = sef.getScriptEngine();
        try
        {
            fr = new FileReader(scriptFile);
            javax.script.CompiledScript compiled = ((javax.script.Compilable) portal).compile(fr);
            compiled.eval();


            if (fr != null)
            {
                try
                {
                    fr.close();
                }
                catch (IOException e)
                {
                    System.err.println("ERROR CLOSING" + e);
                }
            }

            script = ((javax.script.Invocable) portal).getInterface(PortalScript.class);
        }
        catch (Exception e)
        {
            System.err.println("请检查Portal为:(" + scriptName + ".js)的文件." + e);
            FileoutputUtil.log("log\\Script\\Portal_Script_Except.log", "请检查Portal为:(" + scriptName + ".js)的文件. " + e);
        }
        finally
        {
            if (fr != null)
            {
                try
                {
                    fr.close();
                }
                catch (IOException e)
                {
                    System.err.println("ERROR CLOSING" + e);
                }
            }
        }
        this.scripts.put(scriptName, script);
        return script;
    }

    public void clearScripts()
    {
        this.scripts.clear();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\scripting\portal\PortalScriptManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */