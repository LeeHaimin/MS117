package scripting.reactor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.script.Invocable;
import javax.script.ScriptEngine;

import client.MapleClient;
import server.maps.MapleReactor;
import server.maps.ReactorDropEntry;
import tools.FileoutputUtil;

public class ReactorScriptManager extends scripting.AbstractScriptManager
{
    private static final ReactorScriptManager instance = new ReactorScriptManager();
    private final Map<Integer, List<ReactorDropEntry>> drops = new HashMap<>();

    public static synchronized ReactorScriptManager getInstance()
    {
        return instance;
    }

    public void act(MapleClient c, MapleReactor reactor)
    {
        try
        {
            Invocable iv = getInvocable("reactor/" + reactor.getReactorId() + ".js", c);
            if (iv == null)
            {
                if (c.getPlayer().isAdmin())
                {
                    c.getPlayer().dropMessage(5, "未找到 Reactor 文件中的 " + reactor.getReactorId() + ".js 文件.");
                }
                FileoutputUtil.log("log\\Script\\Reactor_Script_Except.log", "未找到 Reactor 文件中的 " + reactor.getReactorId() + ".js 文件.");
                return;
            }
            ScriptEngine scriptengine = (ScriptEngine) iv;
            ReactorActionManager rm = new ReactorActionManager(c, reactor);

            scriptengine.put("rm", rm);
            iv.invokeFunction("act");
        }
        catch (Exception e)
        {
            System.err.println("执行Reactor文件出错 ReactorID: " + reactor.getReactorId() + ", ReactorName: " + reactor.getName() + " 错误信息: " + e);
            FileoutputUtil.log("log\\Script\\Reactor_Script_Except.log", "执行Reactor文件出错 ReactorID: " + reactor.getReactorId() + ", ReactorName: " + reactor.getName() + " 错误信息: " + e);
        }
    }

    public List<ReactorDropEntry> getDrops(int reactorId)
    {
        List<ReactorDropEntry> ret = this.drops.get(reactorId);
        if (ret != null)
        {
            return ret;
        }
        ret = new LinkedList<>();
        try
        {
            PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("SELECT * FROM reactordrops WHERE reactorid = ?");
            ps.setInt(1, reactorId);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                ret.add(new ReactorDropEntry(rs.getInt("itemid"), rs.getInt("chance"), rs.getInt("questid")));
            }
            rs.close();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("从SQL中读取箱子的爆率出错.箱子的ID: " + reactorId + " 错误信息: " + e);
            FileoutputUtil.log("log\\Script\\Reactor_Script_Except.log", "从SQL中读取箱子的爆率出错.箱子的ID: " + reactorId + " 错误信息: " + e);
        }
        this.drops.put(reactorId, ret);
        return ret;
    }

    public void clearDrops()
    {
        this.drops.clear();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\scripting\reactor\ReactorScriptManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */