package scripting.map;

import java.awt.Point;

import client.MapleClient;
import client.MapleQuestStatus;
import scripting.AbstractPlayerInteraction;
import server.MapleItemInformationProvider;
import server.life.MapleLifeFactory;
import server.life.MapleMonster;
import server.life.OverrideMonsterStats;
import server.maps.MapleMap;
import server.quest.MapleQuest;
import server.quest.MedalQuest;
import tools.MaplePacketCreator;
import tools.packet.UIPacket;


public class MapScriptMethods extends AbstractPlayerInteraction
{
    public MapScriptMethods(MapleClient c)
    {
        super(c);
    }

    public void displayAranIntro()
    {
        String data = null;
        switch (this.c.getPlayer().getMapId())
        {
            case 914090010:
                data = "Effect/Direction1.img/aranTutorial/Scene0";
                break;
            case 914090011:
                data = "Effect/Direction1.img/aranTutorial/Scene1" + (this.c.getPlayer().getGender() == 0 ? "0" : "1");
                break;
            case 914090012:
                data = "Effect/Direction1.img/aranTutorial/Scene2" + (this.c.getPlayer().getGender() == 0 ? "0" : "1");
                break;
            case 914090013:
                data = "Effect/Direction1.img/aranTutorial/Scene3";
                break;
            case 914090100:
                data = "Effect/Direction1.img/aranTutorial/HandedPoleArm" + (this.c.getPlayer().getGender() == 0 ? "0" : "1");
                break;
            case 914090200:
                data = "Effect/Direction1.img/aranTutorial/Maha";
        }

        if (data != null)
        {
            showIntro(this.c, data);
        }
    }

    private void showIntro(MapleClient c, String data)
    {
        c.getSession().write(UIPacket.IntroDisableUI(true));
        c.getSession().write(UIPacket.IntroLock(true));
        c.getSession().write(UIPacket.ShowWZEffect(data));
    }

    public void startMapEffect(MapleClient c, String data, int itemId)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        if (!ii.itemExists(itemId))
        {
            c.getPlayer().dropMessage(5, "地图效果触发 道具: " + itemId + " 不存在.");
            return;
        }
        if (!ii.isFloatCashItem(itemId))
        {
            c.getPlayer().dropMessage(5, "地图效果触发 道具: " + itemId + " 不具有漂浮公告的效果.");
            return;
        }
        c.getPlayer().getMap().startMapEffect(data, itemId);
    }

    public void explorationPoint()
    {
        if (this.c.getPlayer().getMapId() == 104000000)
        {
            this.c.getSession().write(UIPacket.IntroDisableUI(false));
            this.c.getSession().write(UIPacket.IntroLock(false));
            this.c.getSession().write(MaplePacketCreator.enableActions());
            this.c.getSession().write(UIPacket.MapNameDisplay(this.c.getPlayer().getMapId()));
        }

        MedalQuest m = null;
        for (MedalQuest mq : MedalQuest.values())
        {
            for (int i : mq.maps)
            {
                if (this.c.getPlayer().getMapId() == i)
                {
                    m = mq;
                    break;
                }
            }
        }
        if ((m != null) && (this.c.getPlayer().getLevel() >= m.level) && (this.c.getPlayer().getQuestStatus(m.questid) != 2))
        {
            if (this.c.getPlayer().getQuestStatus(m.lquestid) != 1)
            {
                MapleQuest.getInstance(m.lquestid).forceStart(this.c.getPlayer(), 0, "0");
            }
            if (this.c.getPlayer().getQuestStatus(m.questid) != 1)
            {
                MapleQuest.getInstance(m.questid).forceStart(this.c.getPlayer(), 0, null);
                StringBuilder sb = new StringBuilder("enter=");
                for (int i = 0; i < m.maps.length; i++)
                {
                    sb.append("0");
                }
                this.c.getPlayer().updateInfoQuest(m.questid - 2005, sb.toString());
                MapleQuest.getInstance(m.questid - 1995).forceStart(this.c.getPlayer(), 0, "0");
            }
            String quest = this.c.getPlayer().getInfoQuest(m.questid - 2005);
            if (quest.length() != m.maps.length + 6)
            {
                StringBuilder sb = new StringBuilder("enter=");
                for (int i = 0; i < m.maps.length; i++)
                {
                    sb.append("0");
                }
                quest = sb.toString();
                this.c.getPlayer().updateInfoQuest(m.questid - 2005, quest);
            }
            MapleQuestStatus stat = this.c.getPlayer().getQuestNAdd(MapleQuest.getInstance(m.questid - 1995));
            if (stat.getCustomData() == null)
            {
                stat.setCustomData("0");
            }
            int number = Integer.parseInt(stat.getCustomData());
            StringBuilder sb = new StringBuilder("enter=");
            boolean changedd = false;
            for (int i = 0; i < m.maps.length; i++)
            {
                boolean changed = false;
                if ((this.c.getPlayer().getMapId() == m.maps[i]) && (quest.substring(i + 6, i + 7).equals("0")))
                {
                    sb.append("1");
                    changed = true;
                    changedd = true;
                }

                if (!changed)
                {
                    sb.append(quest, i + 6, i + 7);
                }
            }
            if (changedd)
            {
                number++;
                this.c.getPlayer().updateInfoQuest(m.questid - 2005, sb.toString());
                MapleQuest.getInstance(m.questid - 1995).forceStart(this.c.getPlayer(), 0, String.valueOf(number));
                this.c.getPlayer().dropMessage(-1, "探险了 " + number + "/" + m.maps.length + " 个地区");
                this.c.getPlayer().dropMessage(-1, "正在挑战称号 - " + m + "");
                this.c.getSession().write(MaplePacketCreator.showQuestMsg("正在挑战称号 - " + m + "。" + number + "/" + m.maps.length + " 完成"));
            }
        }
    }

    public void reloadWitchTower()
    {
        MapleMap map = this.c.getPlayer().getMap();
        map.killAllMonsters(false);
        int level = this.c.getPlayer().getLevel();
        int mob;

        if (level <= 10)
        {
            mob = 9300367;
        }
        else
        {

            if (level <= 20)
            {
                mob = 9300368;
            }
            else
            {

                if (level <= 30)
                {
                    mob = 9300369;
                }
                else
                {

                    if (level <= 40)
                    {
                        mob = 9300370;
                    }
                    else
                    {

                        if (level <= 50)
                        {
                            mob = 9300371;
                        }
                        else
                        {

                            if (level <= 60)
                            {
                                mob = 9300372;
                            }
                            else
                            {

                                if (level <= 70)
                                {
                                    mob = 9300373;
                                }
                                else
                                {

                                    if (level <= 80)
                                    {
                                        mob = 9300374;
                                    }
                                    else
                                    {

                                        if (level <= 90)
                                        {
                                            mob = 9300375;
                                        }
                                        else
                                        {

                                            if (level <= 100)
                                            {
                                                mob = 9300376;
                                            }
                                            else mob = 9300377;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        MapleMonster theMob = MapleLifeFactory.getMonster(mob);
        OverrideMonsterStats oms = new OverrideMonsterStats();
        oms.setOMp(theMob.getMobMaxMp());
        oms.setOExp(theMob.getMobExp());
        oms.setOHp((long) Math.ceil(theMob.getMobMaxHp() * (level / 5.0D)));
        theMob.setOverrideStats(oms);
        map.spawnMonsterOnGroundBelow(theMob, new Point(-60, 184));
    }

    public void sendMapNameDisplay(boolean enabled)
    {
        if (enabled)
        {
            this.c.getSession().write(UIPacket.IntroDisableUI(false));
            this.c.getSession().write(UIPacket.IntroLock(false));
        }
        this.c.getSession().write(UIPacket.MapNameDisplay(this.c.getPlayer().getMapId()));
    }

    public void handlePinkBeanStart()
    {
        MapleMap map = this.c.getPlayer().getMap();
        map.resetFully();
        if (!map.containsNPC(2141000))
        {
            map.spawnNpc(2141000, new Point(65346, -42));
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\scripting\map\MapScriptMethods.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */