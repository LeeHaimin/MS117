package scripting.map;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import client.MapleClient;
import tools.FileoutputUtil;


public class MapScriptManager
{
    private static final MapScriptManager instance = new MapScriptManager();
    private static final ScriptEngineFactory sef = new ScriptEngineManager().getEngineByName("javascript").getFactory();
    private final Map<String, MapScript> scripts = new HashMap<>();

    public static synchronized MapScriptManager getInstance()
    {
        return instance;
    }

    public void getMapScript(MapleClient c, String scriptName, boolean firstUser)
    {
        if (this.scripts.containsKey(scriptName))
        {
            this.scripts.get(scriptName).start(new MapScriptMethods(c));
            return;
        }
        String type = "onUserEnter/";
        if (firstUser)
        {
            type = "onFirstUserEnter/";
        }
        File scriptFile = new File("scripts/map/" + type + scriptName + ".js");
        if (!scriptFile.exists())
        {
            if (c.getPlayer().isAdmin())
            {
                c.getPlayer().dropMessage(5, "地图触发: 未找到 map/" + type + " 文件中的 " + scriptName + ".js 文件.");
            }
            FileoutputUtil.log("log\\Script\\Map_Script_Except.log",
                    "地图触发: 未找到 map/" + type + " 文件中的 " + scriptName + ".js 文件. 在地图 " + c.getPlayer().getMapId() + " - " + c.getPlayer().getMap().getMapName());
            return;
        }
        FileReader fr = null;
        ScriptEngine map = sef.getScriptEngine();
        try
        {
            fr = new FileReader(scriptFile);
            CompiledScript compiled = ((Compilable) map).compile(fr);
            compiled.eval();


            if (fr != null)
            {
                try
                {
                    fr.close();
                }
                catch (IOException e)
                {
                    System.err.println("ERROR CLOSING" + e);
                }
            }

            if (!c.getPlayer().isAdmin())
            {
            }
        }
        catch (Exception e)
        {
            System.err.println("请检查(map/" + type + " 文件中的 " + scriptName + ".js)的文件." + e);
            FileoutputUtil.log("log\\Script\\Map_Script_Except.log", "请检查(map/" + type + " 文件中的 " + scriptName + ".js)的文件." + e);
        }
        finally
        {
            if (fr != null)
            {
                try
                {
                    fr.close();
                }
                catch (IOException e)
                {
                    System.err.println("ERROR CLOSING" + e);
                }
            }
        }

        c.getPlayer().dropMessage(5, "开始执行地图触发: map/" + type + " 文件中的 " + scriptName + ".js 文件.");
        MapScript script = ((Invocable) map).getInterface(MapScript.class);
        this.scripts.put(scriptName, script);
        script.start(new MapScriptMethods(c));
    }

    public void clearScripts()
    {
        this.scripts.clear();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\scripting\map\MapScriptManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */