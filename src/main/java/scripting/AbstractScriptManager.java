package scripting;

import java.io.File;
import java.io.FileReader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import client.MapleClient;
import tools.FileoutputUtil;


public abstract class AbstractScriptManager
{
    private final ScriptEngineManager sem;

    protected AbstractScriptManager()
    {
        this.sem = new ScriptEngineManager();
    }

    protected Invocable getInvocable(String path, MapleClient c)
    {
        return getInvocable(path, c, false);
    }

    protected Invocable getInvocable(String path, MapleClient c, boolean npc)
    {
        try
        {
            path = "scripts/" + path;
            ScriptEngine engine = null;
            if (c != null)
            {
                engine = c.getScriptEngine(path);
            }
            if (engine == null)
            {
                File scriptFile = new File(path);
                if (!scriptFile.exists())
                {
                    return null;
                }
                engine = this.sem.getEngineByName("javascript");
                if (c != null)
                {
                    c.setScriptEngine(path, engine);
                }
                FileReader fr = new FileReader(scriptFile);
                engine.eval(fr);
                fr.close();
            }
            else if ((c != null) && (npc))
            {
                c.getPlayer().dropMessage(-1, "您当前已经和1个NPC对话了. 如果不是请输入 @ea 命令进行解卡。");
            }
            return (Invocable) engine;
        }
        catch (Exception e)
        {
            System.err.println("Error executing script. Path: " + path + "\r\nException " + e);
            FileoutputUtil.log("log\\Script\\Script_Except.log", "Error executing script. Path: " + path + "\r\nException " + e);
        }
        return null;
    }
}