package scripting.event;

import javax.script.Invocable;


public class EventEntry
{
    public final String script;
    public final Invocable iv;
    public final EventManager em;

    public EventEntry(String script, Invocable iv, EventManager em)
    {
        this.script = script;
        this.iv = iv;
        this.em = em;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\scripting\event\EventEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */