package scripting.event;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.script.Invocable;
import javax.script.ScriptEngine;

import handling.channel.ChannelServer;
import scripting.AbstractScriptManager;
import tools.FileoutputUtil;


public class EventScriptManager extends AbstractScriptManager
{
    private static final AtomicInteger runningInstanceMapId = new AtomicInteger(0);
    private final Map<String, EventEntry> events = new LinkedHashMap<>();

    public EventScriptManager(ChannelServer cserv, String[] scripts)
    {
        for (String script : scripts)
        {
            if (!script.equals(""))
            {
                Invocable iv = getInvocable("event/" + script + ".js", null);
                if (iv != null)
                {
                    this.events.put(script, new EventEntry(script, iv, new EventManager(cserv, iv, script)));
                }
            }
        }
    }

    public static int getNewInstanceMapId()
    {
        return runningInstanceMapId.addAndGet(1);
    }

    public EventManager getEventManager(String event)
    {
        EventEntry entry = this.events.get(event);
        if (entry == null)
        {
            return null;
        }
        return entry.em;
    }

    public void init()
    {
        for (EventEntry entry : this.events.values())
        {
            try
            {
                ((ScriptEngine) entry.iv).put("em", entry.em);
                entry.iv.invokeFunction("init", new Object[]{null});
            }
            catch (Exception ex)
            {
                System.out.println("Error initiating event: " + entry.script + ":" + ex);
                FileoutputUtil.log("log\\Script\\Event_Script_Except.log", "Error initiating event: " + entry.script + ":" + ex);
            }
        }
    }

    public void cancel()
    {
        for (EventEntry entry : this.events.values())
        {
            entry.em.cancel();
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\scripting\event\EventScriptManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */