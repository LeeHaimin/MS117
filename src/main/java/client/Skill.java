package client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import constants.GameConstants;
import constants.SkillConstants;
import provider.MapleData;
import provider.MapleDataTool;
import server.MapleStatEffect;
import server.life.Element;
import tools.FileoutputUtil;
import tools.Pair;

public class Skill
{
    private final List<MapleStatEffect> effects = new ArrayList<>();
    private final List<Pair<String, Byte>> requiredSkill = new ArrayList<>();
    private final int id;
    private final Map<Integer, Integer> bonusExpInfo = new HashMap<>();
    private String name = "";
    private List<MapleStatEffect> pvpEffects = null;
    private List<Integer> animation = null;
    private Element element = Element.NEUTRAL;
    private int animationTime = 0;
    private int masterLevel = 0;
    private int maxLevel = 0;
    private int delay = 0;
    private int trueMax = 0;
    private int eventTamingMob = 0;
    private int skillType = 0;
    private boolean invisible = false;
    private boolean chargeskill = false;
    private boolean timeLimited = false;
    private boolean combatOrders = false;
    private boolean pvpDisabled = false;
    private boolean magic = false;
    private boolean casterMove = false;
    private boolean pushTarget = false;
    private boolean pullTarget = false;
    private boolean isBuffSkill = false;
    private boolean isSummonSkill = false;
    private boolean notRemoved = false;
    private int hyper = 0;
    private int reqLev = 0;
    private int maxDamageOver = 999999;
    private boolean petPassive = false;
    private int setItemReason;
    private int setItemPartsCount;

    public Skill(int id)
    {
        this.id = id;
    }

    public static Skill loadFromData(int id, MapleData data, MapleData delayData)
    {
        boolean showSkill = false;
        if (showSkill)
        {
            System.out.println("正在解析技能id: " + id + " 名字: " + SkillFactory.getSkillName(id));
            FileoutputUtil.log("log\\SkillsLog.log", "正在解析技能id: " + id + " 名字: " + SkillFactory.getSkillName(id), true);
        }
        Skill ret = new Skill(id);


        int skillType = MapleDataTool.getInt("skillType", data, -1);
        String elem = MapleDataTool.getString("elemAttr", data, null);
        if (elem != null)
        {
            ret.element = Element.getFromChar(elem.charAt(0));
        }
        ret.skillType = skillType;
        ret.invisible = (MapleDataTool.getInt("invisible", data, 0) > 0);
        ret.notRemoved = (MapleDataTool.getInt("notRemoved", data, 0) > 0);
        ret.timeLimited = (MapleDataTool.getInt("timeLimited", data, 0) > 0);
        ret.combatOrders = (MapleDataTool.getInt("combatOrders", data, 0) > 0);
        ret.masterLevel = MapleDataTool.getInt("masterLevel", data, 0);
        ret.eventTamingMob = MapleDataTool.getInt("eventTamingMob", data, 0);

        ret.hyper = MapleDataTool.getInt("hyper", data, 0);
        ret.reqLev = MapleDataTool.getInt("reqLev", data, 0);

        ret.petPassive = (MapleDataTool.getInt("petPassive", data, 0) > 0);
        ret.setItemReason = MapleDataTool.getInt("setItemReason", data, 0);
        ret.setItemPartsCount = MapleDataTool.getInt("setItemPartsCount", data, 0);
        MapleData inf = data.getChildByPath("info");
        if (inf != null)
        {
            ret.pvpDisabled = (MapleDataTool.getInt("pvp", inf, 1) <= 0);
            ret.magic = (MapleDataTool.getInt("magicDamage", inf, 0) > 0);
            ret.casterMove = (MapleDataTool.getInt("casterMove", inf, 0) > 0);
            ret.pushTarget = (MapleDataTool.getInt("pushTarget", inf, 0) > 0);
            ret.pullTarget = (MapleDataTool.getInt("pullTarget", inf, 0) > 0);
        }
        MapleData effect = data.getChildByPath("effect");
        boolean isBuff;
        if (skillType == 2)
        {
            isBuff = true;
        }
        else if (skillType == 3)
        {
            ret.animation = new ArrayList<>();
            ret.animation.add(0);
            isBuff = effect != null;
            switch (id)
            {
                case 20040216:
                case 20040217:
                case 20040219:
                case 20040220:
                case 20041239:
                    isBuff = true;
            }
        }
        else
        {
            MapleData action_ = data.getChildByPath("action");
            MapleData hit = data.getChildByPath("hit");
            MapleData ball = data.getChildByPath("ball");

            boolean action = false;
            if ((action_ == null) && (data.getChildByPath("prepare/action") != null))
            {
                action_ = data.getChildByPath("prepare/action");
                action = true;
            }

            isBuff = (effect != null) && (hit == null) && (ball == null);
            String d;
            if (action_ != null)
            {
                d = null;
                if (action)
                {
                    d = MapleDataTool.getString(action_, null);
                }
                else
                {
                    d = MapleDataTool.getString("0", action_, null);
                }
                if (d != null)
                {
                    isBuff |= d.equals("alert2");
                    MapleData dd = delayData.getChildByPath(d);
                    if (dd != null)
                    {
                        for (MapleData del : dd)
                        {
                            ret.delay += Math.abs(MapleDataTool.getInt("delay", del, 0));
                        }
                        if (ret.delay > 30)
                        {
                            ret.delay = ((int) Math.round(ret.delay * 11.0D / 16.0D));
                            ret.delay -= ret.delay % 30;
                        }
                    }
                    if (SkillFactory.getDelay(d) != null)
                    {
                        ret.animation = new ArrayList<>();
                        ret.animation.add(SkillFactory.getDelay(d));
                        if (!action)
                        {
                            for (MapleData ddc : action_)
                            {
                                if ((!MapleDataTool.getString(ddc, d).equals(d)) && (!ddc.getName().contentEquals("delay")))
                                {
                                    String c = MapleDataTool.getString(ddc);
                                    if (SkillFactory.getDelay(c) != null)
                                    {
                                        ret.animation.add(SkillFactory.getDelay(c));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            switch (id)
            {

                case 1076:
                case 2111002:
                case 2111003:
                case 2301002:
                case 2321001:
                case 4301004:
                case 12111005:
                case 14111006:
                case 22161003:
                case 32121006:
                case 100001266:
                    isBuff = false;
                    break;


                case 93:
                case 1004:
                case 1026:
                case 1101013:
                case 1121016:
                case 1210016:
                case 1221014:
                case 1310016:
                case 1321014:
                case 2120010:
                case 2120012:
                case 2220010:
                case 2220013:
                case 2320011:
                case 2320012:
                case 3101004:
                case 3111011:
                case 3201004:
                case 3211012:
                case 4211003:
                case 4221013:
                case 4330009:
                case 4341002:
                case 5001005:
                case 5100015:
                case 5111007:
                case 5120011:
                case 5120012:
                case 5121009:
                case 5211007:
                case 5220012:
                case 5220014:
                case 5220019:
                case 5221015:
                case 5311005:
                case 5320007:
                case 5321003:
                case 5321004:
                case 5711001:
                case 5711011:
                case 5720005:
                case 5720012:
                case 5721003:
                case 9001004:
                case 9101004:
                case 10000093:
                case 10001004:
                case 10001026:
                case 13110026:
                case 15111022:
                case 20000093:
                case 20001004:
                case 20001026:
                case 20010093:
                case 20011004:
                case 20011026:
                case 20020093:
                case 20021026:
                case 20031209:
                case 20031210:
                case 21000000:
                case 21101003:
                case 22131001:
                case 22131002:
                case 22141002:
                case 22151002:
                case 22151003:
                case 22161002:
                case 22161004:
                case 22171000:
                case 22171004:
                case 22181000:
                case 22181003:
                case 22181004:
                case 24111002:
                case 27100003:
                case 27110007:
                case 30000093:
                case 30001026:
                case 30010093:
                case 30010242:
                case 30011026:
                case 30020232:
                case 31121005:
                case 32001003:
                case 32101003:
                case 32110000:
                case 32110007:
                case 32110008:
                case 32110009:
                case 32111005:
                case 32111006:
                case 32111012:
                case 32120000:
                case 32120001:
                case 32121003:
                case 33101005:
                case 33111003:
                case 35001001:
                case 35001002:
                case 35101005:
                case 35101007:
                case 35101009:
                case 35111001:
                case 35111002:
                case 35111004:
                case 35111005:
                case 35111009:
                case 35111010:
                case 35111011:
                case 35111013:
                case 35120000:
                case 35120014:
                case 35121003:
                case 35121005:
                case 35121006:
                case 35121009:
                case 35121010:
                case 35121013:
                case 60001216:
                case 60001217:
                case 61101002:
                case 61111002:
                case 61120007:
                case 80001000:
                case 80001089:
                case 80001140:
                    isBuff = true;
            }

        }
        ret.chargeskill = (data.getChildByPath("keydown") != null);
        if (ret.chargeskill)
        {
        }


        MapleData level = data.getChildByPath("common");
        if (level != null)
        {
            ret.maxLevel = MapleDataTool.getInt("maxLevel", level, 1);
            ret.trueMax = (ret.maxLevel + (ret.combatOrders ? 2 : 0));
            for (int i = 1; i <= ret.trueMax; i++)
            {
                ret.effects.add(MapleStatEffect.loadSkillEffectFromData(level, id, isBuff, i, "x", ret.notRemoved));
            }
            ret.maxDamageOver = MapleDataTool.getInt("MDamageOver", level, 999999);
        }
        else
        {
            for (MapleData leve : data.getChildByPath("level"))
            {
                ret.effects.add(MapleStatEffect.loadSkillEffectFromData(leve, id, isBuff, Byte.parseByte(leve.getName()), null, ret.notRemoved));
            }
            ret.maxLevel = ret.effects.size();
            ret.trueMax = ret.effects.size();
        }
        boolean loadPvpSkill = false;
        if (loadPvpSkill)
        {
            MapleData level2 = data.getChildByPath("PVPcommon");
            if (level2 != null)
            {
                ret.pvpEffects = new ArrayList<>();
                for (int i = 1; i <= ret.trueMax; i++)
                {
                    ret.pvpEffects.add(MapleStatEffect.loadSkillEffectFromData(level2, id, isBuff, i, "x", ret.notRemoved));
                }
            }
        }
        MapleData reqDataRoot = data.getChildByPath("req");
        if (reqDataRoot != null)
        {
            for (MapleData reqData : reqDataRoot.getChildren())
            {
                ret.requiredSkill.add(new Pair(reqData.getName(), (byte) MapleDataTool.getInt(reqData, 1)));
            }
        }
        ret.animationTime = 0;
        if (effect != null) for (MapleData effectEntry : effect)
        {
            ret.animationTime += MapleDataTool.getIntConvert("delay", effectEntry, 0);
        }
        MapleData effectEntry;
        ret.isBuffSkill = isBuff;
        switch (id)
        {
            case 27000207:
            case 27001100:
            case 27001201:
                ret.masterLevel = ret.maxLevel;
        }

        ret.isSummonSkill = (data.getChildByPath("summon") != null);
        if (ret.isSummonSkill)
        {
        }


        MapleData growthInfo = data.getChildByPath("growthInfo/level");
        if (growthInfo != null)
        {
            for (MapleData expData : growthInfo.getChildren())
            {
                ret.bonusExpInfo.put(Integer.parseInt(expData.getName()), MapleDataTool.getInt("maxExp", expData, 100000000));
            }
        }
        return ret;
    }

    public int getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public MapleStatEffect getPVPEffect(int level)
    {
        if (this.pvpEffects == null)
        {
            return getEffect(level);
        }
        if (this.pvpEffects.size() < level)
        {
            if (this.pvpEffects.size() > 0)
            {
                return this.pvpEffects.get(this.pvpEffects.size() - 1);
            }
            return null;
        }
        if (level <= 0)
        {
            return this.pvpEffects.get(0);
        }
        return this.pvpEffects.get(level - 1);
    }

    public MapleStatEffect getEffect(int level)
    {
        if (this.effects.size() < level)
        {
            if (this.effects.size() > 0)
            {
                return this.effects.get(this.effects.size() - 1);
            }
            return null;
        }
        if (level <= 0)
        {
            return this.effects.get(0);
        }
        return this.effects.get(level - 1);
    }

    public int getSkillType()
    {
        return this.skillType;
    }

    public List<Integer> getAllAnimation()
    {
        return this.animation;
    }

    public int getAnimation()
    {
        if (this.animation == null)
        {
            return -1;
        }
        return this.animation.get(server.Randomizer.nextInt(this.animation.size()));
    }

    public boolean isPVPDisabled()
    {
        return this.pvpDisabled;
    }

    public boolean isChargeSkill()
    {
        return this.chargeskill;
    }

    public boolean isInvisible()
    {
        return this.invisible;
    }

    public boolean isNotRemoved()
    {
        return this.notRemoved;
    }

    public boolean hasRequiredSkill()
    {
        return this.requiredSkill.size() > 0;
    }

    public List<Pair<String, Byte>> getRequiredSkills()
    {
        return this.requiredSkill;
    }

    public int getTrueMax()
    {
        return this.trueMax;
    }

    public boolean combatOrders()
    {
        return this.combatOrders;
    }

    public boolean canBeLearnedBy(int job)
    {
        int jid = job;
        int skillForJob = this.id / 10000;
        if (skillForJob == 2001) return GameConstants.is龙神(job);
        if (skillForJob == 0) return GameConstants.is冒险家(job);
        if (skillForJob == 500) return (GameConstants.is拳手(job)) || (GameConstants.is火枪手(job));
        if (skillForJob == 501) return GameConstants.is火炮手(job);
        if (skillForJob == 508) return GameConstants.is龙的传人(job);
        if (skillForJob == 509) return (GameConstants.is拳手新(job)) || (GameConstants.is火枪手新(job));
        if (skillForJob == 1000) return GameConstants.is骑士团(job);
        if (skillForJob == 2000) return GameConstants.is战神(job);
        if (skillForJob == 2002) return GameConstants.is双弩精灵(job);
        if (skillForJob == 2003) return GameConstants.is幻影(job);
        if (skillForJob == 2004) return GameConstants.is夜光(job);
        if (skillForJob == 3000) return GameConstants.is反抗者(job);
        if (skillForJob == 3001) return GameConstants.is恶魔猎手(job);
        if (skillForJob == 3002) return GameConstants.is尖兵(job);
        if (skillForJob == 5000) return GameConstants.is米哈尔(job);
        if (skillForJob == 6000) return GameConstants.is狂龙战士(job);
        if (skillForJob == 6001) return GameConstants.is爆莉萌天使(job);
        if (skillForJob == 10000) return GameConstants.is神之子(job);
        if (skillForJob == 11000) return GameConstants.is林之灵(job);
        if (jid / 100 != skillForJob / 100) return false;
        if (jid / 1000 != skillForJob / 1000) return false;
        if ((GameConstants.is林之灵(skillForJob)) && (!GameConstants.is林之灵(job))) return false;
        if ((GameConstants.is神之子(skillForJob)) && (!GameConstants.is神之子(job))) return false;
        if ((GameConstants.is爆莉萌天使(skillForJob)) && (!GameConstants.is爆莉萌天使(job))) return false;
        if ((GameConstants.is狂龙战士(skillForJob)) && (!GameConstants.is狂龙战士(job))) return false;
        if ((GameConstants.is米哈尔(skillForJob)) && (!GameConstants.is米哈尔(job))) return false;
        if ((GameConstants.is尖兵(skillForJob)) && (!GameConstants.is尖兵(job))) return false;
        if ((GameConstants.is夜光(skillForJob)) && (!GameConstants.is夜光(job))) return false;
        if ((GameConstants.is幻影(skillForJob)) && (!GameConstants.is幻影(job))) return false;
        if ((GameConstants.is龙的传人(skillForJob)) && (!GameConstants.is龙的传人(job))) return false;
        if ((GameConstants.is火炮手(skillForJob)) && (!GameConstants.is火炮手(job))) return false;
        if ((GameConstants.is拳手(skillForJob)) && (!GameConstants.is拳手(job))) return false;
        if ((GameConstants.is火枪手(skillForJob)) && (!GameConstants.is火枪手(job))) return false;
        if ((GameConstants.is拳手新(skillForJob)) && (!GameConstants.is拳手新(job))) return false;
        if ((GameConstants.is火枪手新(skillForJob)) && (!GameConstants.is火枪手新(job))) return false;
        if ((GameConstants.is恶魔复仇者(skillForJob)) && (!GameConstants.is恶魔复仇者(job))) return false;
        if ((GameConstants.is恶魔猎手(skillForJob)) && (!GameConstants.is恶魔猎手(job))) return false;
        if ((GameConstants.is冒险家(skillForJob)) && (!GameConstants.is冒险家(job))) return false;
        if ((GameConstants.is骑士团(skillForJob)) && (!GameConstants.is骑士团(job))) return false;
        if ((GameConstants.is战神(skillForJob)) && (!GameConstants.is战神(job))) return false;
        if ((GameConstants.is龙神(skillForJob)) && (!GameConstants.is龙神(job))) return false;
        if ((GameConstants.is双弩精灵(skillForJob)) && (!GameConstants.is双弩精灵(job))) return false;
        if ((GameConstants.is反抗者(skillForJob)) && (!GameConstants.is反抗者(job))) return false;
        if ((jid / 10 % 10 == 0) && (skillForJob / 10 % 10 > jid / 10 % 10)) return false;
        if ((skillForJob / 10 % 10 != 0) && (skillForJob / 10 % 10 != jid / 10 % 10)) return false;
        return skillForJob % 10 <= jid % 10;
    }

    public boolean isTimeLimited()
    {
        return this.timeLimited;
    }

    public boolean isFourthJob()
    {
        if (isHyperSkill())
        {
            return true;
        }
        switch (this.id)
        {
            case 1120012:
            case 1320011:
            case 3110014:
            case 4320005:
            case 4340010:
            case 4340012:
            case 5120011:
            case 5120012:
            case 5220012:
            case 5220014:
            case 5321006:
            case 5720008:
            case 21120011:
            case 21120014:
            case 22181004:
            case 23120011:
            case 23120013:
            case 23121008:
            case 33120010:
            case 33121005:
                return false;
        }
        switch (this.id / 10000)
        {
            case 2312:
            case 2412:
            case 2712:
            case 3122:
            case 6112:
            case 6512:
                return true;
            case 10100:
                return this.id == 101000101;
            case 10110:
                return (this.id == 101100101) || (this.id == 101100201);
            case 10111:
                return (this.id == 101110102) || (this.id == 101110200) || (this.id == 101110203);
            case 10112:
                return (this.id == 101120104) || (this.id == 101120204);
        }
        if ((getMaxLevel() <= 15) && (!this.invisible) && (getMasterLevel() <= 0))
        {
            return false;
        }
        if ((this.id / 10000 >= 2212) && (this.id / 10000 < 3000))
        {
            return this.id / 10000 % 10 >= 7;
        }
        if ((this.id / 10000 >= 430) && (this.id / 10000 <= 434))
        {
            return (this.id / 10000 % 10 == 4) || (getMasterLevel() > 0);
        }
        return (this.id / 10000 % 10 == 2) && (this.id < 90000000) && (!isBeginnerSkill());
    }

    public boolean isHyperSkill()
    {
        return (this.hyper > 0) && (this.reqLev > 0);
    }

    public int getMaxLevel()
    {
        return this.maxLevel;
    }

    public int getMasterLevel()
    {
        return this.masterLevel;
    }

    public boolean isBeginnerSkill()
    {
        int jobId = this.id / 10000;
        return GameConstants.is新手职业(jobId);
    }

    public Element getElement()
    {
        return this.element;
    }

    public int getAnimationTime()
    {
        return this.animationTime;
    }

    public int getDelay()
    {
        return this.delay;
    }

    public int getTamingMob()
    {
        return this.eventTamingMob;
    }

    public int getHyper()
    {
        return this.hyper;
    }

    public int getReqLevel()
    {
        return this.reqLev;
    }

    public int getMaxDamageOver()
    {
        return this.maxDamageOver;
    }

    public int getBonusExpInfo(int level)
    {
        if (this.bonusExpInfo.isEmpty())
        {
            return -1;
        }
        if (this.bonusExpInfo.containsKey(level))
        {
            return this.bonusExpInfo.get(level);
        }
        return -1;
    }

    public boolean isMagic()
    {
        return this.magic;
    }

    public boolean isMovement()
    {
        return this.casterMove;
    }

    public boolean isPush()
    {
        return this.pushTarget;
    }

    public boolean isPull()
    {
        return this.pullTarget;
    }

    public boolean isBuffSkill()
    {
        return this.isBuffSkill;
    }

    public boolean isSummonSkill()
    {
        return this.isSummonSkill;
    }

    public boolean isGuildSkill()
    {
        int jobId = this.id / 10000;
        return jobId == 9100;
    }

    public boolean isAdminSkill()
    {
        int jobId = this.id / 10000;
        return (jobId == 800) || (jobId == 900);
    }


    public boolean isInnerSkill()
    {
        int jobId = this.id / 10000;
        return jobId == 7000;
    }


    public boolean isSpecialSkill()
    {
        int jobId = this.id / 10000;
        return (jobId == 7000) || (jobId == 7100) || (jobId == 8000) || (jobId == 9000) || (jobId == 9100) || (jobId == 9200) || (jobId == 9201) || (jobId == 9202) || (jobId == 9203) || (jobId == 9204);
    }

    public int getSkillByJobBook()
    {
        return getSkillByJobBook(this.id);
    }

    public int getSkillByJobBook(int skillid)
    {
        switch (skillid / 10000)
        {
            case 112:
            case 122:
            case 132:
            case 212:
            case 222:
            case 232:
            case 312:
            case 322:
            case 412:
            case 422:
            case 512:
            case 522:
                return 4;
            case 111:
            case 121:
            case 131:
            case 211:
            case 221:
            case 231:
            case 311:
            case 321:
            case 411:
            case 421:
            case 511:
            case 521:
                return 3;
            case 110:
            case 120:
            case 130:
            case 210:
            case 220:
            case 230:
            case 310:
            case 320:
            case 410:
            case 420:
            case 510:
            case 520:
                return 2;
            case 100:
            case 200:
            case 300:
            case 400:
            case 500:
                return 1;
        }
        return -1;
    }


    public boolean isPetPassive()
    {
        return this.petPassive;
    }


    public int getSetItemReason()
    {
        return this.setItemReason;
    }


    public int geSetItemPartsCount()
    {
        return this.setItemPartsCount;
    }


    public boolean isTeachSkills()
    {
        switch (this.id)
        {
            case 110:
            case 1214:
            case 20021110:
            case 20030204:
            case 20040218:
            case 30010112:
            case 30010241:
            case 30020233:
            case 50001214:
            case 60000222:
            case 60011219:
            case 110000800:
                return true;
        }
        return false;
    }


    public boolean isLinkSkills()
    {
        switch (this.id)
        {
            case 80000000:
            case 80000001:
            case 80000002:
            case 80000005:
            case 80000006:
            case 80000047:
            case 80000050:
            case 80001040:
            case 80001140:
            case 80001151:
            case 80001155:
                return true;
        }
        return false;
    }

    public boolean is老技能()
    {
        switch (this.id)
        {


            case 11000005:
            case 11000006:
            case 11001001:
            case 11001002:
            case 11001003:
            case 11001004:
            case 11100000:
            case 11100007:
            case 11101001:
            case 11101002:
            case 11101003:
            case 11101004:
            case 11101005:
            case 11101006:
            case 11101008:
            case 11110000:
            case 11110005:
            case 11111001:
            case 11111002:
            case 11111003:
            case 11111004:
            case 11111006:
            case 11111007:
            case 11111008:
            case 13000000:
            case 13000001:
            case 13000005:
            case 13001003:
            case 13001004:
            case 13100000:
            case 13100008:
            case 13101001:
            case 13101002:
            case 13101003:
            case 13101004:
            case 13101005:
            case 13101006:
            case 13101007:
            case 13110003:
            case 13110008:
            case 13110009:
            case 13111000:
            case 13111001:
            case 13111002:
            case 13111004:
            case 13111005:
            case 13111006:
            case 13111007:
            case 15000000:
            case 15000005:
            case 15000006:
            case 15000008:
            case 15001002:
            case 15001003:
            case 15001004:
            case 15001007:
            case 15100001:
            case 15100004:
            case 15100009:
            case 15101002:
            case 15101003:
            case 15101005:
            case 15101008:
            case 15101010:
            case 15110009:
            case 15110010:
            case 15111004:
            case 15111005:
            case 15111006:
            case 15111007:
            case 15111008:
            case 15111011:
            case 15111012:
                return true;
        }

        return false;
    }

    public boolean isAngelSkill()
    {
        return GameConstants.is天使技能(this.id);
    }

    public boolean isLinkedAttackSkill()
    {
        return GameConstants.isLinkedAttackSkill(this.id);
    }

    public boolean isDefaultSkill()
    {
        return SkillConstants.isDefaultSkill(this.id);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\Skill.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */