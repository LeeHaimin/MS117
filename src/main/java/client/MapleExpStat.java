package client;


public enum MapleExpStat
{
    活动奖励经验(1L), 特别经验(2L), 结婚奖励经验(32L), 活动组队经验(4L), 组队经验(16L), 道具佩戴经验(64L), 网吧特别经验(128L), 彩虹周奖励经验(256L), 欢享奖励经验(512L), 飞跃奖励经验(1024L), 精灵祝福经验(2048L), 增益奖励经验(4096L), 休息经验(8192L), 物品奖励经验(16384L),
    阿斯旺获胜者奖励经验(32768L), 使用道具经验(65536L), 格外获得经验(524288L);

    private final long i;

    MapleExpStat(long i)
    {
        this.i = i;
    }

    public long getValue()
    {
        return this.i;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleExpStat.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */