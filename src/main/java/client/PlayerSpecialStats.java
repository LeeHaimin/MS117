package client;

import java.io.Serializable;


public class PlayerSpecialStats implements Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private int forceCounter;
    private int cardStack;
    private int morphCount;
    private long lastMorphLostTime;
    private int powerCount;
    private boolean usePower;
    private long lastRecoveryPowerTime;
    private boolean energyfull;
    private int runningDark;
    private int runningDarkSlot;
    private int runningLight;
    private int runningLightSlot;

    public void resetSpecialStats()
    {
        this.forceCounter = 0;
        this.cardStack = 0;
        this.morphCount = 0;
        this.lastMorphLostTime = System.currentTimeMillis();
        this.powerCount = 0;
        this.usePower = false;
        this.lastRecoveryPowerTime = System.currentTimeMillis();
        this.energyfull = false;
        this.runningDark = 0;
        this.runningDarkSlot = 0;
        this.runningLight = 0;
        this.runningLight = 0;
        this.runningLightSlot = 0;
    }


    public int getForceCounter()
    {
        if (this.forceCounter < 0)
        {
            this.forceCounter = 0;
        }
        return this.forceCounter;
    }

    public void setForceCounter(int amount)
    {
        this.forceCounter = amount;
    }

    public void gainForceCounter()
    {
        this.forceCounter += 1;
    }

    public void gainForceCounter(int amount)
    {
        this.forceCounter += amount;
    }


    public int getCardStack()
    {
        if (this.cardStack < 0)
        {
            this.cardStack = 0;
        }
        return this.cardStack;
    }

    public void setCardStack(int amount)
    {
        this.cardStack = amount;
    }

    public void gainCardStack()
    {
        this.cardStack += 1;
    }


    public int getMorphCount()
    {
        if (this.morphCount < 0)
        {
            this.morphCount = 0;
        }
        return this.morphCount;
    }

    public void setMorphCount(int amount)
    {
        this.morphCount = amount;
    }

    public void gainMorphCount()
    {
        this.morphCount += 1;
    }

    public void gainMorphCount(int amount)
    {
        this.morphCount += amount;
    }

    public long getLastMorphLostTime()
    {
        if (this.lastMorphLostTime <= 0L)
        {
            this.lastMorphLostTime = System.currentTimeMillis();
        }
        return this.lastMorphLostTime;
    }

    public void prepareMorphLostTime()
    {
        this.lastMorphLostTime = System.currentTimeMillis();
    }


    public int getPowerCount()
    {
        if (this.powerCount < 0)
        {
            this.powerCount = 0;
        }
        return this.powerCount;
    }

    public void setPowerCount(int amount)
    {
        this.powerCount = amount;
    }

    public boolean isUsePower()
    {
        return this.usePower;
    }

    public void changePower(boolean b)
    {
        this.usePower = b;
    }

    public long getLastRecoveryPowerTime()
    {
        if (this.lastRecoveryPowerTime <= 0L)
        {
            this.lastRecoveryPowerTime = System.currentTimeMillis();
        }
        return this.lastRecoveryPowerTime;
    }

    public void prepareRecoveryPowerTime()
    {
        this.lastRecoveryPowerTime = System.currentTimeMillis();
    }


    public boolean isEnergyfull()
    {
        return this.energyfull;
    }

    public void changeEnergyfull(boolean full)
    {
        this.energyfull = full;
    }


    public int getLightTotal()
    {
        if (this.runningLightSlot < 0)
        {
            this.runningLightSlot = 0;
        }
        return this.runningLightSlot;
    }

    public void setLightTotal(int amount)
    {
        this.runningLightSlot = amount;
    }

    public void gainLightTotal(int amount)
    {
        this.runningLightSlot += amount;
    }

    public int getLightType()
    {
        if (this.runningLight < 0)
        {
            this.runningLight = 0;
        }
        return this.runningLight;
    }

    public void setLightType(int amount)
    {
        this.runningLight = amount;
    }

    public void gainLightType(int amount)
    {
        this.runningLight += amount;
    }

    public int getDarkTotal()
    {
        if (this.runningDarkSlot < 0)
        {
            this.runningDarkSlot = 0;
        }
        return this.runningDarkSlot;
    }

    public void setDarkTotal(int amount)
    {
        this.runningDarkSlot = amount;
    }

    public void gainDarkTotal(int amount)
    {
        this.runningDarkSlot += amount;
    }

    public int getDarkType()
    {
        if (this.runningDark < 0)
        {
            this.runningDark = 0;
        }
        return this.runningDark;
    }

    public void setDarkType(int amount)
    {
        this.runningDark = amount;
    }

    public void gainDarkType(int amount)
    {
        this.runningDark += amount;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\PlayerSpecialStats.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */