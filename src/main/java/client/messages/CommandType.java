package client.messages;


public enum CommandType
{
    NORMAL(0), TRADE(1), POKEMON(2);

    private final int level;

    CommandType(int level)
    {
        this.level = level;
    }

    public int getType()
    {
        return this.level;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\messages\CommandType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */