package client.messages;


public enum PlayerGMRank
{
    NORMAL('@', 0), DONATOR('#', 1), SUPERDONATOR('$', 2), INTERN('%', 3), GM('!', 4), SUPERGM('!', 5), ADMIN('!', 6);

    private final char commandPrefix;
    private final int level;

    PlayerGMRank(char ch, int level)
    {
        this.commandPrefix = ch;
        this.level = level;
    }

    public char getCommandPrefix()
    {
        return this.commandPrefix;
    }

    public int getLevel()
    {
        return this.level;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\messages\PlayerGMRank.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */