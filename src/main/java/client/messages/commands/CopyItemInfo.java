package client.messages.commands;


public class CopyItemInfo
{
    public final int itemId;


    public final int chrId;

    public final String name;

    public boolean first;


    public CopyItemInfo(int itemId, int chrId, String name)
    {
        this.itemId = itemId;
        this.chrId = chrId;
        this.name = name;
        this.first = true;
    }

    public boolean isFirst()
    {
        return this.first;
    }

    public void setFirst(boolean f)
    {
        this.first = f;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\messages\commands\CopyItemInfo.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */