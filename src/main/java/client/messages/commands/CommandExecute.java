package client.messages.commands;

import client.MapleClient;
import client.messages.CommandType;


public abstract class CommandExecute
{
    public abstract int execute(MapleClient paramMapleClient, String[] paramArrayOfString);

    public CommandType getType()
    {
        return CommandType.NORMAL;
    }

    enum ReturnValue
    {
        DONT_LOG, LOG;

        ReturnValue()
        {
        }
    }

    public static abstract class TradeExecute extends CommandExecute
    {
        public CommandType getType()
        {
            return CommandType.TRADE;
        }
    }

    public static abstract class PokemonExecute extends CommandExecute
    {
        public CommandType getType()
        {
            return CommandType.POKEMON;
        }
    }
}