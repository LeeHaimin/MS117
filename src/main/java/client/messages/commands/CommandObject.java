package client.messages.commands;

import client.MapleClient;
import client.messages.CommandType;


public class CommandObject
{
    private final int gmLevelReq;
    private final CommandExecute exe;

    public CommandObject(CommandExecute c, int gmLevel)
    {
        this.exe = c;
        this.gmLevelReq = gmLevel;
    }


    public int execute(MapleClient c, String[] splitted)
    {
        return this.exe.execute(c, splitted);
    }

    public CommandType getType()
    {
        return this.exe.getType();
    }


    public int getReqGMLevel()
    {
        return this.gmLevelReq;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\messages\commands\CommandObject.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */