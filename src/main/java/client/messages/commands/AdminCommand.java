package client.messages.commands;

import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import client.MapleCharacter;
import client.MapleCharacterUtil;
import client.MapleClient;
import client.SkillFactory;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import client.messages.PlayerGMRank;
import handling.RecvPacketOpcode;
import handling.SendPacketOpcode;
import handling.cashshop.CashShopServer;
import handling.channel.ChannelServer;
import handling.world.WorldBroadcastService;
import scripting.map.MapScriptManager;
import scripting.npc.NPCScriptManager;
import scripting.portal.PortalScriptManager;
import scripting.reactor.ReactorScriptManager;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.ServerProperties;
import server.ShutdownServer;
import server.Timer;
import server.cashshop.CashItemFactory;
import server.life.MapleMonsterInformationProvider;
import server.shop.MapleShopFactory;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.StringUtil;
import tools.packet.UIPacket;
import tools.performance.CPUSampler;

public class AdminCommand
{
    public static PlayerGMRank getPlayerLevelRequired()
    {
        return PlayerGMRank.ADMIN;
    }

    public static class StripEveryone extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            ChannelServer cs = c.getChannelServer();
            for (MapleCharacter mchr : cs.getPlayerStorage().getAllCharacters())
            {
                if (!mchr.isGM())
                {

                    MapleInventory equipped = mchr.getInventory(MapleInventoryType.EQUIPPED);
                    MapleInventory equip = mchr.getInventory(MapleInventoryType.EQUIP);
                    List<Short> ids = new ArrayList<>();
                    for (Item item : equipped.newList())
                    {
                        ids.add(item.getPosition());
                    }
                    for (Short aShort : ids)
                    {
                        short id = aShort;
                        MapleInventoryManipulator.unequip(mchr.getClient(), id, equip.getNextFreeSlot());
                    }
                }
            }
            MapleCharacter mchr;
            MapleInventory equip;
            return 1;
        }
    }

    public static class 给所有人金币 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 1)
            {
                c.getPlayer().dropMessage(6, "用法: !MesoEveryone [金币数量]");
                return 0;
            }
            try
            {
                int meso = Integer.parseInt(splitted[1]);
            }
            catch (NumberFormatException nfe)
            {
                int meso;
                c.getPlayer().dropMessage(6, "输入的金币数量无效.");
                return 0;
            }
            int meso = 0;
            for (ChannelServer cserv : ChannelServer.getAllInstances())
            {
                for (MapleCharacter mch : cserv.getPlayerStorage().getAllCharacters())
                {
                    mch.gainMeso(meso, true);
                }
            }
            return 1;
        }
    }

    public static class 距离 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(6, "用法: !测试距离 x坐标 y坐标  说明: 此命令可以测试输入的坐标信息和角色当前坐标的距离");
                return 0;
            }
            int x = Integer.parseInt(splitted[1]);
            int y = Integer.parseInt(splitted[2]);
            Point test = new Point(x, y);
            c.getPlayer().dropMessage(6, "当前距离: " + c.getPlayer().getTruePosition().distanceSq(test));
            return 1;
        }
    }

    public static class 给所有人点券 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(6, "用法: !CashEveryone [点卷类型1-2] [点卷数量]");
                return 0;
            }
            int type = Integer.parseInt(splitted[1]);
            int quantity = Integer.parseInt(splitted[2]);
            if ((type <= 0) || (type > 2))
            {
                type = 2;
            }
            if (quantity > 9000)
            {
                quantity = 9000;
            }
            int ret = 0;
            StringBuilder sb = new StringBuilder();
            for (ChannelServer cserv : ChannelServer.getAllInstances())
            {
                for (MapleCharacter mch : cserv.getPlayerStorage().getAllCharacters())
                {
                    mch.modifyCSPoints(type, quantity, false);
                    mch.dropMessage(-11, "[系统提示] 恭喜您获得管理员赠送给您的" + (type == 1 ? "点券 " : " 抵用券 ") + quantity + " 点.");
                    ret++;
                    sb.append(MapleCharacterUtil.makeMapleReadable(mch.getName()));
                    sb.append(", ");
                }
            }
            c.getPlayer().dropMessage(6, "以下是获得" + (type == 1 ? "点券 " : " 抵用券 ") + "的玩家名单:");
            c.getPlayer().dropMessage(6, sb.toString());
            c.getPlayer().dropMessage(6, "命令使用成功，当前共有: " + ret + " 个玩家获得: " + quantity + " 点的" + (type == 1 ? "点券 " : " 抵用券 ") + " 总计: " + ret * quantity);
            return 1;
        }
    }

    public static class ExpRate extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length > 1)
            {
                int rate = Integer.parseInt(splitted[1]);
                if ((splitted.length > 2) && (splitted[2].equalsIgnoreCase("all")))
                {
                    for (ChannelServer cserv : ChannelServer.getAllInstances())
                    {
                        cserv.setExpRate(rate);
                    }
                }
                else
                {
                    c.getChannelServer().setExpRate(rate);
                }
                c.getPlayer().dropMessage(6, "经验倍率已经修改为: " + rate + "倍.");
            }
            else
            {
                c.getPlayer().dropMessage(6, "用法: !exprate <number> [all]");
            }
            return 1;
        }
    }

    public static class MesoRate extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length > 1)
            {
                int rate = Integer.parseInt(splitted[1]);
                if ((splitted.length > 2) && (splitted[2].equalsIgnoreCase("all")))
                {
                    for (ChannelServer cserv : ChannelServer.getAllInstances())
                    {
                        cserv.setMesoRate(rate);
                    }
                }
                else
                {
                    c.getChannelServer().setMesoRate(rate);
                }
                c.getPlayer().dropMessage(6, "金币爆率已经修改为: " + rate + "倍.");
            }
            else
            {
                c.getPlayer().dropMessage(6, "用法: !mesorate <number> [all]");
            }
            return 1;
        }
    }

    public static class DropRate extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length > 1)
            {
                int rate = Integer.parseInt(splitted[1]);
                if ((splitted.length > 2) && (splitted[2].equalsIgnoreCase("all")))
                {
                    for (ChannelServer cserv : ChannelServer.getAllInstances())
                    {
                        cserv.setDropRate(rate);
                    }
                }
                else
                {
                    c.getChannelServer().setDropRate(rate);
                }
                c.getPlayer().dropMessage(6, "怪物爆率已经修改为: " + rate + "倍.");
            }
            else
            {
                c.getPlayer().dropMessage(6, "用法: !droprate <number> [all]");
            }
            return 1;
        }
    }

    public static class 双倍经验 extends CommandExecute
    {
        private int change = 0;

        public int execute(MapleClient c, String[] splitted)
        {
            this.change = Integer.parseInt(splitted[1]);
            if ((this.change == 1) || (this.change == 2))
            {
                c.getPlayer().dropMessage(6, "以前 - 经验: " + c.getChannelServer().getExpRate() + " 金币: " + c.getChannelServer().getMesoRate() + " 爆率: " + c.getChannelServer().getDropRate());
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                {
                    cserv.setDoubleExp(this.change);
                }
                c.getPlayer().dropMessage(6, "现在 - 经验: " + c.getChannelServer().getExpRate() + " 金币: " + c.getChannelServer().getMesoRate() + " 爆率: " + c.getChannelServer().getDropRate());
                return 1;
            }
            c.getPlayer().dropMessage(6, "输入的数字无效，1为关闭活动经验，2为开启活动经验。当前输入为: " + this.change);
            return 0;
        }
    }

    public static class 经验信息 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            c.getPlayer().dropMessage(5, "当前游戏设置信息:");
            for (ChannelServer cserv : ChannelServer.getAllInstances())
            {
                String rateStr = "频道 " + cserv.getChannel() + " 经验: " + cserv.getExpRate() + " 金币: " + cserv.getMesoRate() + " 爆率: " + cserv.getDropRate() + " 活动: " + cserv.getDoubleExp() + " " +
                        "活动活动信息 经验: " + cserv.getAutoGain() + " 点卷: " + cserv.getAutoNx() + " 泡点: " + cserv.getAutoPaoDian();
                c.getPlayer().dropMessage(5, rateStr);
            }
            return 1;
        }
    }

    public static class 在线经验 extends CommandExecute
    {
        private int change = 10;

        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(6, "用法: !在线经验 <经验数值>");
                return 0;
            }
            try
            {
                this.change = Integer.parseInt(splitted[1]);
            }
            catch (NumberFormatException nfe)
            {
                c.getPlayer().dropMessage(6, "输入的数字无效.");
                return 0;
            }
            if ((this.change >= 10) && (this.change <= 1000))
            {
                c.getPlayer().dropMessage(6, "以前 - 在线经验: " + c.getChannelServer().getAutoGain());
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                {
                    cserv.setAutoGain(this.change);
                }
                c.getPlayer().dropMessage(6, "现在 - 在线经验: " + c.getChannelServer().getAutoGain() + " 在线经验修改完成...");
            }
            else
            {
                c.getPlayer().dropMessage(6, "输入的在线经验数值不正确 只能输入 10 - 1000 的数字 当前输入为: " + this.change);
            }
            return 1;
        }
    }

    public static class 在线点卷 extends CommandExecute
    {
        private int change = 1;

        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(6, "用法: !在线点卷 <点卷数值>");
                return 0;
            }
            try
            {
                this.change = Integer.parseInt(splitted[1]);
            }
            catch (NumberFormatException nfe)
            {
                c.getPlayer().dropMessage(6, "输入的数字无效.");
                return 0;
            }
            if ((this.change >= 1) && (this.change <= 100))
            {
                c.getPlayer().dropMessage(6, "以前 - 在线点卷: " + c.getChannelServer().getAutoNx());
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                {
                    cserv.setAutoNx(this.change);
                }
                c.getPlayer().dropMessage(6, "现在 - 在线点卷: " + c.getChannelServer().getAutoNx() + " 在线点卷修改完成...");
            }
            else
            {
                c.getPlayer().dropMessage(6, "输入的在线点卷数值不正确 只能输入 1 - 100 的数字 当前输入为: " + this.change);
            }
            return 1;
        }
    }

    public static class 在线泡点 extends CommandExecute
    {
        private int change = 1;

        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(6, "用法: !在线泡点 <泡点数值>");
                return 0;
            }
            try
            {
                this.change = Integer.parseInt(splitted[1]);
            }
            catch (NumberFormatException nfe)
            {
                c.getPlayer().dropMessage(6, "输入的数字无效.");
                return 0;
            }
            if ((this.change == 1) || (this.change == 2))
            {
                c.getPlayer().dropMessage(6, "以前 - 在线泡点 - 频道: " + c.getChannelServer().getAutoPaoDian() + " 商城: " + CashShopServer.getAutoPaoDian());
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                {
                    cserv.setAutoPaoDian(this.change);
                }
                CashShopServer.setAutoPaoDian(this.change);
                c.getPlayer().dropMessage(6, "现在 - 在线泡点 - 频道: " + c.getChannelServer().getAutoPaoDian() + " 商城: " + CashShopServer.getAutoPaoDian() + " 在线泡点修改完成...");
            }
            else
            {
                c.getPlayer().dropMessage(6, "输入的在线泡点数值不正确 只能输入 1 和 2 的数字 当前输入为: " + this.change);
            }
            return 1;
        }
    }

    public static class 解除批量卡号 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            int range = -1;
            if (splitted[1].equals("m"))
            {
                range = 0;
            }
            else if (splitted[1].equals("c"))
            {
                range = 1;
            }
            else if (splitted[1].equals("w"))
            {
                range = 2;
            }
            if (range == -1)
            {
                range = 1;
            }
            if (range == 0)
            {
                c.getPlayer().getMap().disconnectAll();
                c.getPlayer().dropMessage(5, "已成功断开当前地图所有玩家的连接.");
            }
            else if (range == 1)
            {
                c.getChannelServer().getPlayerStorage().disconnectAll(true);
                c.getPlayer().dropMessage(5, "已成功断开当前频道所有玩家的连接.");
            }
            else if (range == 2)
            {
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                {
                    cserv.getPlayerStorage().disconnectAll(true);
                }
                c.getPlayer().dropMessage(5, "已成功断开当前游戏所有玩家的连接.");
            }
            return 1;
        }
    }

    public static class 查看爆率 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            NPCScriptManager.getInstance().start(c, 9010000, 1);
            return 1;
        }
    }

    public static class Shutdown extends CommandExecute
    {
        protected static Thread t = null;

        public int execute(MapleClient c, String[] splitted)
        {
            c.getPlayer().dropMessage(6, "游戏即将关闭...");
            if ((t == null) || (!t.isAlive()))
            {
                t = new Thread(ShutdownServer.getInstance());
                ShutdownServer.getInstance().shutdown();
                t.start();
            }
            else
            {
                c.getPlayer().dropMessage(6, "已经使用过一次这个命令，暂时无法使用.");
            }
            return 1;
        }
    }

    public static class 重启 extends AdminCommand.Shutdown
    {
        private static ScheduledFuture<?> ts = null;
        private int minutesLeft = 0;

        public int execute(MapleClient c, String[] splitted)
        {
            this.minutesLeft = Integer.parseInt(splitted[1]);
            c.getPlayer().dropMessage(6, "游戏将在 " + this.minutesLeft + " 分钟之后关闭...");
            if ((ts == null) && ((t == null) || (!t.isAlive())))
            {
                t = new Thread(ShutdownServer.getInstance());
                ts = Timer.EventTimer.getInstance().register(new Runnable()
                {
                    public void run()
                    {
                        if (AdminCommand.重启.this.minutesLeft == 0)
                        {
                            ShutdownServer.getInstance().shutdown();
                            AdminCommand.Shutdown.t.start();
                            AdminCommand.重启.ts.cancel(false);
                            return;
                        }
                        WorldBroadcastService.getInstance().broadcastMessage(UIPacket.clearMidMsg());
                        WorldBroadcastService.getInstance().broadcastMessage(UIPacket.getMidMsg("游戏将于 " + AdminCommand.重启.this.minutesLeft + " 分钟之后关闭维护.请玩家安全下线.", true, 0));
                        WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(0, " 游戏将于 " + AdminCommand.重启.this.minutesLeft + " 分钟之后关闭维护.请玩家安全下线."));
//                        AdminCommand.重启.access$010(AdminCommand.重启.this);
                    }
                }, 60000L);
            }
            else
            {
                c.getPlayer().dropMessage(6, "已经使用过一次这个命令，暂时无法使用.");
            }
            return 1;
        }
    }

    public static class StartProfiling extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            CPUSampler sampler = CPUSampler.getInstance();
            sampler.addIncluded("GUI/client");
            sampler.addIncluded("GUI/constants");
            sampler.addIncluded("GUI/database");
            sampler.addIncluded("GUI/handling");
            sampler.addIncluded("provider");
            sampler.addIncluded("scripting");
            sampler.addIncluded("server");
            sampler.addIncluded("tools");
            sampler.start();
            c.getPlayer().dropMessage(6, "已经开启服务端性能监测.");
            return 1;
        }
    }

    public static class StopProfiling extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            CPUSampler sampler = CPUSampler.getInstance();
            try
            {
                String filename = "odinprofile.txt";
                if (splitted.length > 1)
                {
                    filename = splitted[1];
                }
                File file = new File(filename);
                if (file.exists())
                {
                    c.getPlayer().dropMessage(6, "输入的文件名字已经存在，请重新输入1个新的文件名。");
                    return 0;
                }
                sampler.stop();
                FileWriter fw = new FileWriter(file);
                sampler.save(fw, 1, 10);
                fw.close();
            }
            catch (IOException e)
            {
                System.err.println("保存文件出错." + e);
            }
            sampler.reset();
            c.getPlayer().dropMessage(6, "已经停止服务端性能监测.");
            return 1;
        }
    }

    public static class Subcategory extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            c.getPlayer().setSubcategory(Byte.parseByte(splitted[1]));
            return 1;
        }
    }

    public static class 刷金币 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            c.getPlayer().gainMeso(Integer.MAX_VALUE - c.getPlayer().getMeso(), true);
            return 1;
        }
    }

    public static class 刷点卷 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            c.getPlayer().modifyCSPoints(1, Integer.parseInt(splitted[1]), true);
            return 1;
        }
    }

    public static class 刷抵用卷 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            c.getPlayer().modifyCSPoints(2, Integer.parseInt(splitted[1]), true);
            return 1;
        }
    }

    public static class GainP extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            c.getPlayer().setPoints(c.getPlayer().getPoints() + Integer.parseInt(splitted[1]));
            return 1;
        }
    }

    public static class GainVP extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            c.getPlayer().setVPoints(c.getPlayer().getVPoints() + Integer.parseInt(splitted[1]));
            return 1;
        }
    }

    public static class 重载包头 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (ServerProperties.ShowPacket())
            {
                ServerProperties.loadSettings();
            }
            RecvPacketOpcode.reloadValues();
            SendPacketOpcode.reloadValues();
            handling.CashShopOpcode.reloadValues();
            handling.InteractionOpcode.reloadValues();
            c.getPlayer().dropMessage(5, "重新获取包头完成.");
            return 1;
        }
    }

    public static class 重载爆率 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            MapleMonsterInformationProvider.getInstance().clearDrops();
            ReactorScriptManager.getInstance().clearDrops();
            c.getPlayer().dropMessage(5, "重新加载爆率完成.");
            return 1;
        }
    }

    public static class 重载传送 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            PortalScriptManager.getInstance().clearScripts();
            c.getPlayer().dropMessage(5, "重新加载传送点脚本完成.");
            return 1;
        }
    }

    public static class 重载商店 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            MapleShopFactory.getInstance().clear();
            c.getPlayer().dropMessage(5, "重新加载商店贩卖道具完成.");
            return 1;
        }
    }

    public static class 重载活动 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            ServerProperties.loadProperties("config/world.properties");
            for (ChannelServer instance : ChannelServer.getAllInstances())
            {
                instance.reloadEvents();
            }
            c.getPlayer().dropMessage(5, "重新加载活动脚本完成.");
            return 1;
        }
    }

    public static class 重载地图触发 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            MapScriptManager.getInstance().clearScripts();
            c.getPlayer().dropMessage(5, "重新加载地图触发脚本完成.");
            return 1;
        }
    }

    public static class 封包测试 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            c.StartWindow();
            return 1;
        }
    }

    public static class 增加股价 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            int share = Integer.parseInt(splitted[1]);
            ChannelServer.getInstance(1).increaseShare(share);
            c.getPlayer().dropMessage(5, "股价提高: " + share + " 当前的股价为: " + ChannelServer.getInstance(1).getSharePrice());
            return 1;
        }
    }

    public static class 降低股价 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            int share = Integer.parseInt(splitted[1]);
            ChannelServer.getInstance(1).decreaseShare(share);
            c.getPlayer().dropMessage(5, "股价降低: " + share + " 当前的股价为: " + ChannelServer.getInstance(1).getSharePrice());
            return 1;
        }
    }

    public static class 查看股价 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            c.getPlayer().dropMessage(5, "当前的股价为: " + ChannelServer.getInstance(1).getSharePrice());
            return 1;
        }
    }

    public static class 重载复制 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            SkillFactory.loadMemorySkills();
            c.getPlayer().dropMessage(5, "加载复制技能完成...");
            return 1;
        }
    }

    public static class 重载商城禁止 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            CashItemFactory.getInstance().loadBlockedCash();
            c.getPlayer().dropMessage(5, "加载商城禁止道具信息完成...");
            c.getPlayer().dropMessage(5, "共有: " + CashItemFactory.getInstance().getBlockedCashItem().size() + " 个道具ID信息 ");
            c.getPlayer().dropMessage(5, "共有: " + CashItemFactory.getInstance().getBlockCashSn().size() + " 个SNID信息");
            return 1;
        }
    }

    public static class 在线玩家 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            int total = 0;
            c.getPlayer().dropMessage(6, "---------------------------------------------------------------------------------------");
            for (ChannelServer cserv : ChannelServer.getAllInstances())
            {
                int curConnected = cserv.getConnectedClients();
                c.getPlayer().dropMessage(6, "频道: " + cserv.getChannel() + " 在线人数: " + curConnected);
                total += curConnected;
                for (MapleCharacter chr : cserv.getPlayerStorage().getAllCharacters())
                {
                    if (chr != null)
                    {
                        StringBuilder ret = new StringBuilder();
                        ret.append(StringUtil.getRightPaddedStr(chr.getName(), ' ', 13));
                        ret.append(" ID: ");
                        ret.append(chr.getId());
                        ret.append(" 等级: ");
                        ret.append(StringUtil.getRightPaddedStr(String.valueOf(chr.getLevel()), ' ', 3));
                        if (chr.getMap() != null)
                        {
                            ret.append(" 地图: ");
                            ret.append(chr.getMapId());
                            ret.append(" - ");
                            ret.append(chr.getMap().getMapName());
                        }
                        c.getPlayer().dropMessage(6, ret.toString());
                    }
                }
            }
            c.getPlayer().dropMessage(6, "当前服务器总计在线: " + total);
            c.getPlayer().dropMessage(6, "---------------------------------------------------------------------------------------");
            return 1;
        }
    }


    public static class 增加经验 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            c.getPlayer().addTraitExp(Integer.parseInt(splitted[1]));
            return 1;
        }
    }

    public static class 设置经验 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(5, "请输入数量.");
                return 0;
            }
            c.getPlayer().setTraitExp(Integer.parseInt(splitted[1]));
            return 1;
        }
    }

    public static class 检测复制 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
            List<String> msgs = new ArrayList<>();
            Map<Integer, CopyItemInfo> checkItems = new LinkedHashMap<>();
            for (ChannelServer cserv : ChannelServer.getAllInstances())
                for (MapleCharacter player : cserv.getPlayerStorage().getAllCharacters())
                {
                    if ((player != null) && (player.getMap() != null))
                    {
                        MapleInventory equip = player.getInventory(MapleInventoryType.EQUIP);
                        for (Item item : equip.list())
                        {
                            if (item.getEquipOnlyId() > 0)
                            {
                                CopyItemInfo ret = new CopyItemInfo(item.getItemId(), player.getId(), player.getName());
                                if (checkItems.containsKey(item.getEquipOnlyId()))
                                {
                                    ret = checkItems.get(item.getEquipOnlyId());
                                    if (ret.itemId == item.getItemId())
                                    {
                                        if (ret.isFirst())
                                        {
                                            ret.setFirst(false);
                                            msgs.add("角色: " + StringUtil.getRightPaddedStr(ret.name, ' ', 13) + " 角色ID: " + StringUtil.getRightPaddedStr(String.valueOf(ret.chrId), ' ', 6) + " 道具: " + ret.itemId + " - " + ii.getName(ret.itemId) + " 唯一ID: " + item.getEquipOnlyId());
                                        }
                                        else
                                        {
                                            msgs.add("角色: " + StringUtil.getRightPaddedStr(player.getName(), ' ', 13) + " 角色ID: " + StringUtil.getRightPaddedStr(String.valueOf(player.getId()), ' ',
                                                    6) + " 道具: " + item.getItemId() + " - " + ii.getName(item.getItemId()) + " 唯一ID: " + item.getEquipOnlyId());
                                        }
                                    }
                                }
                                else
                                {
                                    checkItems.put(item.getEquipOnlyId(), ret);
                                }
                            }
                        }

                        equip = player.getInventory(MapleInventoryType.EQUIPPED);
                        for (Item item : equip.list())
                            if (item.getEquipOnlyId() > 0)
                            {
                                CopyItemInfo ret = new CopyItemInfo(item.getItemId(), player.getId(), player.getName());
                                if (checkItems.containsKey(item.getEquipOnlyId()))
                                {
                                    ret = checkItems.get(item.getEquipOnlyId());
                                    if (ret.itemId == item.getItemId())
                                    {
                                        if (ret.isFirst())
                                        {
                                            ret.setFirst(false);
                                            msgs.add("角色: " + StringUtil.getRightPaddedStr(ret.name, ' ', 13) + " 角色ID: " + StringUtil.getRightPaddedStr(String.valueOf(ret.chrId), ' ', 6) + " 道具: " + ret.itemId + " - " + ii.getName(ret.itemId) + " 唯一ID: " + item.getEquipOnlyId());
                                        }
                                        else
                                        {
                                            msgs.add("角色: " + StringUtil.getRightPaddedStr(player.getName(), ' ', 13) + " 角色ID: " + StringUtil.getRightPaddedStr(String.valueOf(player.getId()), ' ',
                                                    6) + " 道具: " + item.getItemId() + " - " + ii.getName(item.getItemId()) + " 唯一ID: " + item.getEquipOnlyId());
                                        }
                                    }
                                }
                                else
                                {
                                    checkItems.put(item.getEquipOnlyId(), ret);
                                }
                            }
                    }
                }
            Iterator localIterator2;
            MapleCharacter player;
            checkItems.clear();
            if (msgs.size() > 0)
            {
                c.getPlayer().dropMessage(5, "检测完成，共有: " + msgs.size() + " 个复制信息");
                FileoutputUtil.log("装备复制.txt", "检测完成，共有: " + msgs.size() + " 个复制信息", true);
                for (String s : msgs)
                {
                    c.getPlayer().dropMessage(5, s);
                    FileoutputUtil.log("装备复制.txt", s, true);
                }
                c.getPlayer().dropMessage(5, "以上信息为拥有复制道具的玩家.");
            }
            else
            {
                c.getPlayer().dropMessage(5, "未检测到游戏中的角色有复制的道具信息.");
            }
            return 1;
        }
    }

    public static class 查看进程 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            if (splitted.length < 2)
            {
                c.getPlayer().dropMessage(6, "正在查看自己的进程信息.");
                c.getSession().write(MaplePacketCreator.SystemProcess());
                return 0;
            }
            String name = splitted[1];
            MapleCharacter chrs = c.getChannelServer().getPlayerStorage().getCharacterByName(splitted[1]);
            if (chrs == null)
            {
                c.getPlayer().dropMessage(6, "当前频道么有找到玩家[" + name + "]的信息.");
            }
            else
            {
                c.getPlayer().dropMessage(6, "正在查看玩家[" + name + "]的进程信息.");
                chrs.getClient().getSession().write(MaplePacketCreator.SystemProcess());
            }
            return 1;
        }
    }

    public static class 重载配置 extends CommandExecute
    {
        public int execute(MapleClient c, String[] splitted)
        {
            c.getPlayer().dropMessage(6, "重新加载配置文件完成.");
            return 1;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\messages\commands\AdminCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */