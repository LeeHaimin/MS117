package client.messages;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import client.MapleCharacter;
import client.MapleClient;
import client.messages.commands.CommandExecute;
import client.messages.commands.CommandObject;
import client.messages.commands.PlayerCommand;
import tools.FileoutputUtil;

public class CommandProcessor
{
    private static final HashMap<String, CommandObject> commands = new HashMap<>();
    private static final HashMap<Integer, ArrayList<String>> commandList = new HashMap<>();

    static
    {
        Class<?>[] CommandFiles = {PlayerCommand.class, client.messages.commands.InternCommand.class, client.messages.commands.GMCommand.class, client.messages.commands.AdminCommand.class,
                client.messages.commands.DonatorCommand.class, client.messages.commands.SuperDonatorCommand.class, client.messages.commands.SuperGMCommand.class};


        for (Class<?> clasz : CommandFiles)
        {
            try
            {
                PlayerGMRank rankNeeded = (PlayerGMRank) clasz.getMethod("getPlayerLevelRequired", new Class[0]).invoke(null, (Object[]) null);
                Class<?>[] a = clasz.getDeclaredClasses();
                ArrayList<String> cL = new ArrayList<>();
                for (Class<?> c : a)
                {
                    try
                    {
                        if ((!java.lang.reflect.Modifier.isAbstract(c.getModifiers())) && (!c.isSynthetic()))
                        {
                            Object o = c.newInstance();
                            boolean enabled;
                            try
                            {
                                enabled = c.getDeclaredField("enabled").getBoolean(c.getDeclaredField("enabled"));
                            }
                            catch (NoSuchFieldException ex)
                            {
                                enabled = true;
                            }
                            if (((o instanceof CommandExecute)) && (enabled))
                            {
                                cL.add(rankNeeded.getCommandPrefix() + c.getSimpleName().toLowerCase());
                                commands.put(rankNeeded.getCommandPrefix() + c.getSimpleName().toLowerCase(), new CommandObject((CommandExecute) o, rankNeeded.getLevel()));
                                if ((rankNeeded.getCommandPrefix() != PlayerGMRank.GM.getCommandPrefix()) && (rankNeeded.getCommandPrefix() != PlayerGMRank.NORMAL.getCommandPrefix()))
                                {
                                    commands.put("!" + c.getSimpleName().toLowerCase(), new CommandObject((CommandExecute) o, PlayerGMRank.GM.getLevel()));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", ex);
                    }
                }
                java.util.Collections.sort(cL);
                commandList.put(rankNeeded.getLevel(), cL);
            }
            catch (Exception ex)
            {
                FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", ex);
            }
        }
    }

    public static boolean processCommand(MapleClient c, String line, CommandType type)
    {
        if ((line.charAt(0) == PlayerGMRank.NORMAL.getCommandPrefix()) || ((c.getPlayer().getGMLevel() > PlayerGMRank.NORMAL.getLevel()) && (line.charAt(0) == PlayerGMRank.DONATOR.getCommandPrefix())))
        {
            String[] splitted = line.split(" ");
            splitted[0] = splitted[0].toLowerCase();

            CommandObject co = commands.get(splitted[0]);

            if ((co == null) || (co.getType() != type))
            {
                sendDisplayMessage(c, "输入的玩家命令不存在.", type);
                return true;
            }
            try
            {
                int i = co.execute(c, splitted);
            }
            catch (Exception e)
            {
                sendDisplayMessage(c, "使用命令出现错误.", type);
                if (c.getPlayer().isGM())
                {
                    sendDisplayMessage(c, "错误: " + e, type);
                    FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
                }
            }
            return true;
        }

        if ((c.getPlayer().getGMLevel() > PlayerGMRank.NORMAL.getLevel()) && ((line.charAt(0) == PlayerGMRank.SUPERGM.getCommandPrefix()) || (line.charAt(0) == PlayerGMRank.INTERN.getCommandPrefix()) || (line.charAt(0) == PlayerGMRank.GM.getCommandPrefix()) || (line.charAt(0) == PlayerGMRank.ADMIN.getCommandPrefix())))
        {
            String[] splitted = line.split(" ");
            splitted[0] = splitted[0].toLowerCase();

            CommandObject co = commands.get(splitted[0]);
            if (co == null)
            {
                if (splitted[0].equals(line.charAt(0) + "help"))
                {
                    dropHelp(c);
                    return true;
                }
                sendDisplayMessage(c, "输入的命令不存在.", type);
                return true;
            }
            if (c.getPlayer().getGMLevel() >= co.getReqGMLevel())
            {
                int ret = 0;
                try
                {
                    ret = co.execute(c, splitted);
                }
                catch (ArrayIndexOutOfBoundsException x)
                {
                    sendDisplayMessage(c, "使用命令出错，该命令必须带参数才能使用: " + x, type);
                }
                catch (Exception e)
                {
                    FileoutputUtil.outputFileError("log\\Command_Except.log", e);
                }
                if ((ret > 0) && (c.getPlayer() != null))
                {
                    if (c.getPlayer().isGM())
                    {
                        logCommandToDB(c.getPlayer(), line, "gmlog");
                    }
                    else
                    {
                        logCommandToDB(c.getPlayer(), line, "internlog");
                    }
                }
            }
            else
            {
                sendDisplayMessage(c, "您的权限等级不足以使用次命令.", type);
            }
            return true;
        }

        return false;
    }

    private static void sendDisplayMessage(MapleClient c, String msg, CommandType type)
    {
        if (c.getPlayer() == null)
        {
            return;
        }
        switch (type)
        {
            case NORMAL:
                c.getPlayer().dropMessage(6, msg);
                break;
            case TRADE:
                c.getPlayer().dropMessage(-2, "错误 : " + msg);
                break;
            case POKEMON:
                c.getPlayer().dropMessage(-3, "(..." + msg + "..)");
        }

    }

    public static void dropHelp(MapleClient c)
    {
        StringBuilder sb = new StringBuilder("命令列表: ");
        for (int i = 0; i <= c.getPlayer().getGMLevel(); i++)
        {
            if (commandList.containsKey(i))
            {
                for (String s : commandList.get(i))
                {
                    sb.append(s);
                    sb.append(" ");
                }
            }
        }
        c.getPlayer().dropMessage(6, sb.toString());
    }

    private static void logCommandToDB(MapleCharacter player, String command, String table)
    {
        PreparedStatement ps = null;
        try
        {
            ps = database.DatabaseConnection.getConnection().prepareStatement("INSERT INTO " + table + " (cid, name, command, mapid) VALUES (?, ?, ?, ?)");
            ps.setInt(1, player.getId());
            ps.setString(2, player.getName());
            ps.setString(3, command);
            ps.setInt(4, player.getMap().getId());
            ps.executeUpdate();
            return;
        }
        catch (SQLException ex)
        {
            FileoutputUtil.outputFileError("log\\Packet_Except.log", ex);
        }
        finally
        {
            try
            {
                ps.close();
            }
            catch (SQLException ignored)
            {
            }
        }
    }
}