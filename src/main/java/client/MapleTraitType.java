package client;


public enum MapleTraitType
{
    charisma(500, MapleStat.领袖), insight(500, MapleStat.洞察), will(500, MapleStat.意志), craft(500, MapleStat.手技), sense(500, MapleStat.感性), charm(5000, MapleStat.魅力);

    private final int limit;
    private final MapleStat stat;

    MapleTraitType(int type, MapleStat theStat)
    {
        this.limit = type;
        this.stat = theStat;
    }

    public static MapleTraitType getByQuestName(String q)
    {
        String qq = q.substring(0, q.length() - 3);
        for (MapleTraitType t : values())
        {
            if (t.name().equals(qq))
            {
                return t;
            }
        }
        return null;
    }

    public int getLimit()
    {
        return this.limit;
    }

    public MapleStat getStat()
    {
        return this.stat;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleTraitType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */