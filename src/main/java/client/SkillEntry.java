package client;

import java.io.Serializable;

public class SkillEntry implements Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    public final byte masterlevel;
    public final byte position;
    public final long expiration;
    public int skillevel;
    public int teachId;
    public byte rank;

    public SkillEntry(int skillevel, byte masterlevel, long expiration)
    {
        this.skillevel = skillevel;
        this.masterlevel = masterlevel;
        this.expiration = expiration;
        this.teachId = 0;
        this.position = -1;
    }


    public SkillEntry(int skillevel, byte masterlevel, long expiration, int teachId)
    {
        this.skillevel = skillevel;
        this.masterlevel = masterlevel;
        this.expiration = expiration;
        this.teachId = teachId;
        this.position = -1;
    }


    public SkillEntry(int skillevel, byte masterlevel, long expiration, int teachId, byte position)
    {
        this.skillevel = skillevel;
        this.masterlevel = masterlevel;
        this.expiration = expiration;
        this.teachId = teachId;
        this.position = position;
    }

    public String toString()
    {
        return this.skillevel + ":" + this.masterlevel;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\SkillEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */