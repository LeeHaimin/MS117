package client;

import handling.channel.ChannelServer;
import server.Timer;


public class PlayMSEvent
{
    public static void start()
    {
        System.out.println("服务端启用在线泡点1分钟1次.");
        Timer.WorldTimer.getInstance().register(new Runnable()
        {
            public void run()
            {
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                    cserv.AutoPaoDian(cserv.getAutoPaoDian());
            }
        }, 60000L);


        System.out.println("服务端启用自由市场自动加经验3分钟1次.");
        Timer.WorldTimer.getInstance().register(new Runnable()
        {
            public void run()
            {
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                    cserv.AutoGain(cserv.getAutoGain());
            }
        }, 180000L);


        System.out.println("服务端启用自由市场自动加点卷10分钟1次.");
        Timer.WorldTimer.getInstance().register(new Runnable()
        {
            public void run()
            {
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                    cserv.AutoNx(cserv.getAutoNx());
            }
        }, 600000L);


        System.out.println("所有定时活动已经启动完毕...");
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\PlayMSEvent.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */