package client;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.inventory.MapleWeaponType;
import constants.GameConstants;
import handling.world.guild.MapleGuild;
import handling.world.guild.MapleGuildSkill;
import server.MapleItemInformationProvider;
import server.MapleStatEffect;
import server.StructItemOption;
import server.StructSetItem;
import server.StructSetItemStat;
import tools.Pair;
import tools.Triple;
import tools.data.output.MaplePacketLittleEndianWriter;

public class PlayerStats implements java.io.Serializable
{
    public static final int[] pvpSkills = {1000007, 2000007, 3000006, 4000010, 5000006, 5010004, 11000006, 12000006, 13000005, 14000006, 15000005, 21000005, 22000002, 23000004, 31000005, 32000012,
            33000004, 35000005};
    private static final int[] allJobs = {0, 10000000, 20000000, 20010000, 20020000, 20030000, 20040000, 30000000, 30010000, 30020000, 50000000, 60000000, 60010000, 100000000, 110000000};
    private static final long serialVersionUID = -679541993413738569L;
    private final Map<Integer, Integer> setHandling = new HashMap<>();
    private final Map<Integer, Integer> skillsIncrement = new HashMap<>();
    private final Map<Integer, Integer> damageIncrease = new HashMap<>();
    private final java.util.EnumMap<server.life.Element, Integer> elemBoosts = new EnumMap(server.life.Element.class);
    private final List<Equip> durabilityHandling = new ArrayList<>();
    private final List<Equip> equipLevelHandling = new ArrayList<>();
    private final Map<Integer, Integer> add_skill_duration = new HashMap<>();
    private final Map<Integer, Integer> add_skill_attackCount = new HashMap<>();
    private final Map<Integer, Integer> add_skill_targetPlus = new HashMap<>();
    private final Map<Integer, Integer> add_skill_bossDamageRate = new HashMap<>();
    private final Map<Integer, Integer> add_skill_dotTime = new HashMap<>();
    private final Map<Integer, Integer> add_skill_prop = new HashMap<>();
    private final Map<Integer, Integer> add_skill_coolTimeR = new HashMap<>();
    private final Map<Integer, Integer> add_skill_ignoreMobpdpR = new HashMap<>();
    public transient int hpRecoverTime = 0;
    public transient int mpRecoverTime = 0;
    public short str;
    public short dex;
    public short luk;
    public short int_;
    public int hp;
    public int maxhp;
    public int mp;
    public int maxmp;
    public transient boolean equippedWelcomeBackRing;
    public transient boolean hasClone;
    public transient boolean hasPartyBonus;
    public transient boolean Berserk;
    public transient boolean canFish;
    public transient boolean canFishVIP;
    public transient double expBuff;
    public transient double dropBuff;
    public transient double mesoBuff;
    public transient double cashBuff;
    public transient double mesoGuard;
    public transient double mesoGuardMeso;
    public transient double expMod;
    public transient double pickupRange;
    public transient double incRewardProp;
    public transient int recoverHP;
    public transient int recoverMP;
    public transient int mpconReduce;
    public transient int mpconPercent;
    public transient int incMesoProp;
    public transient int reduceCooltime;
    public transient int coolTimeR;
    public transient int suddenDeathR;
    public transient int expLossReduceR;
    public transient int DAMreflect;
    public transient int DAMreflect_rate;
    public transient int ignoreDAMr;
    public transient int ignoreDAMr_rate;
    public transient int ignoreDAM;
    public transient int ignoreDAM_rate;
    public transient int mpRestore;
    public transient int hpRecover;
    public transient int hpRecoverProp;
    public transient int hpRecoverPercent;
    public transient int mpRecover;
    public transient int mpRecoverProp;
    public transient int RecoveryUP;
    public transient int BuffUP;
    public transient int RecoveryUP_Skill;
    public transient int BuffUP_Skill;
    public transient int incAllskill;
    public transient int combatOrders;
    public transient int defRange;
    public transient int BuffUP_Summon;
    public transient int dodgeChance;
    public transient int speed;
    public transient int speedMax;
    public transient int jump;
    public transient int harvestingTool;
    public transient int equipmentBonusExp;
    public transient int dropMod;
    public transient int cashMod;
    public transient int levelBonus;
    public transient int ASR;
    public transient int TER;
    public transient int pickRate;
    public transient int decreaseDebuff;
    public transient int equippedFairy;
    public transient int equippedSummon;
    public transient int pvpDamage;
    public transient int dot;
    public transient int dotTime;
    public transient int questBonus;
    public transient int pvpRank;
    public transient int pvpExp;
    public transient int wdef;
    public transient int mdef;
    public transient int trueMastery;
    public transient int damX;
    public transient int incMaxDamage;
    public transient int incMaxDF;
    public transient int stanceProp;
    public transient int percent_wdef;
    public transient int percent_mdef;
    public transient int percent_hp;
    public transient int percent_mp;
    public transient int percent_str;
    public transient int percent_dex;
    public transient int percent_int;
    public transient int percent_luk;
    public transient int percent_acc;
    public transient int percent_atk;
    public transient int percent_matk;
    public transient int percent_ignore_mob_def_rate;
    public transient double percent_damage;
    public transient int percent_damage_rate;
    public transient int percent_boss_damage_rate;
    public transient int ignore_mob_damage_rate;
    public transient int reduceDamageRate;
    public transient int def;
    public transient int element_ice;
    public transient int element_fire;
    public transient int element_light;
    public transient int element_psn;
    public transient int raidenCount;
    public transient int raidenPorp;
    private transient float shouldHealHP;
    private transient float shouldHealMP;
    private transient byte passive_mastery;
    private transient int localstr;
    private transient int localdex;
    private transient int localluk;
    private transient int localint_;
    private transient int localmaxhp;
    private transient int localmaxmp;
    private transient int addmaxhp;
    private transient int addmaxmp;
    private transient int IndieStrFX;
    private transient int IndieDexFX;
    private transient int IndieLukFX;
    private transient int IndieIntFX;
    private transient int magic;
    private transient int watk;
    private transient int hands;
    private transient int accuracy;
    private transient short passive_sharpeye_rate;
    private transient short passive_sharpeye_max_percent;
    private transient short passive_sharpeye_min_percent;
    private transient float localmaxbasedamage;
    private transient float localmaxbasepvpdamage;
    private transient float localmaxbasepvpdamageL;

    public double getDropBuff()
    {
        if (this.incRewardProp > 100.0D)
        {
            this.incRewardProp = 100.0D;
        }
        return this.dropBuff + this.incRewardProp;
    }

    public boolean checkEquipLevels(MapleCharacter chr, int gain)
    {
        boolean changed = false;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        List<Equip> all = new ArrayList(this.equipLevelHandling);
        for (Equip eq : all)
        {
            int lvlz = eq.getEquipLevel();
            eq.setItemEXP(eq.getItemEXP() + gain);
            if (eq.getEquipLevel() > lvlz)
            {
                Iterator localIterator2;
                for (int i = eq.getEquipLevel() - lvlz; i > 0; i--)
                {
                    Map<Integer, Map<String, Integer>> inc = ii.getEquipIncrements(eq.getItemId());
                    if ((inc != null) && (inc.containsKey(lvlz + i)))
                    {
                        eq = ii.levelUpEquip(eq, inc.get(lvlz + i));
                    }

                    if ((GameConstants.getStatFromWeapon(eq.getItemId()) == null) && (GameConstants.getMaxLevel(eq.getItemId()) < lvlz + i) && (Math.random() < 0.1D) && (eq.getIncSkill() <= 0) && (ii.getEquipSkills(eq.getItemId()) != null))
                    {
                        for (localIterator2 = ii.getEquipSkills(eq.getItemId()).iterator(); localIterator2.hasNext(); )
                        {
                            int zzz = (Integer) localIterator2.next();
                            Skill skil = SkillFactory.getSkill(zzz);
                            if ((skil != null) && (skil.canBeLearnedBy(chr.getJob())))
                            {
                                eq.setIncSkill(skil.getId());
                                chr.dropMessage(5, "Your skill has gained a levelup: " + skil.getName() + " +1");
                            }
                        }
                    }
                }
                changed = true;
            }
            chr.forceUpdateItem(eq.copy());
        }
        if (changed)
        {
            chr.equipChanged();
            chr.getClient().getSession().write(tools.MaplePacketCreator.showItemLevelupEffect());
            chr.getMap().broadcastMessage(chr, tools.MaplePacketCreator.showForeignItemLevelupEffect(chr.getId()), false);
        }
        return changed;
    }

    public boolean checkEquipDurabilitys(MapleCharacter chr, int gain)
    {
        return checkEquipDurabilitys(chr, gain, false);
    }

    public boolean checkEquipDurabilitys(MapleCharacter chr, int gain, boolean aboveZero)
    {
        if (chr.inPVP())
        {
            return true;
        }
        List<Equip> all = new ArrayList(this.durabilityHandling);
        for (Equip item : all)
        {
            if (item != null) if (item.getPosition() >= 0 == aboveZero)
            {
                item.setDurability(item.getDurability() + gain);
                if (item.getDurability() < 0)
                {
                    item.setDurability(0);
                }
            }
        }
        for (Equip eqq : all)
        {
            if ((eqq != null) && (eqq.getDurability() == 0) && (eqq.getPosition() < 0))
            {
                if (chr.getInventory(MapleInventoryType.EQUIP).isFull())
                {
                    chr.getClient().getSession().write(tools.packet.InventoryPacket.getInventoryFull());
                    chr.getClient().getSession().write(tools.packet.InventoryPacket.getShowInventoryFull());
                    return false;
                }
                this.durabilityHandling.remove(eqq);
                short pos = chr.getInventory(MapleInventoryType.EQUIP).getNextFreeSlot();
                server.MapleInventoryManipulator.unequip(chr.getClient(), eqq.getPosition(), pos);
            }
            else if (eqq != null)
            {
                chr.forceUpdateItem(eqq.copy());
            }
        }
        return true;
    }

    public short passive_sharpeye_rate()
    {
        return this.passive_sharpeye_rate;
    }

    public short passive_sharpeye_min_percent()
    {
        return this.passive_sharpeye_min_percent;
    }

    public short passive_sharpeye_percent()
    {
        return this.passive_sharpeye_max_percent;
    }

    public byte passive_mastery()
    {
        return this.passive_mastery;
    }

    public double calculateMaxProjDamage(int projectileWatk, MapleCharacter chra)
    {
        if (projectileWatk < 0)
        {
            return 0.0D;
        }
        Item weapon_item = chra.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
        MapleWeaponType weapon = weapon_item == null ? MapleWeaponType.没有武器 : constants.ItemConstants.getWeaponType(weapon_item.getItemId());
        int secondarystat;
        int mainstat;
        switch (weapon)
        {
            case 弓:
            case 弩:
            case 短枪:
                mainstat = this.localdex;
                secondarystat = this.localstr;
                break;
            case 拳套:
                mainstat = this.localluk;
                secondarystat = this.localdex;
                break;
            default:
                mainstat = 0;
                secondarystat = 0;
        }

        float maxProjDamage = weapon.getMaxDamageMultiplier() * (4 * mainstat + secondarystat) * (projectileWatk / 100.0F);
        maxProjDamage = (float) (maxProjDamage + maxProjDamage * (this.percent_damage / 100.0D));
        return maxProjDamage;
    }

    public float getHealHP()
    {
        return this.shouldHealHP;
    }

    public float getHealMP()
    {
        return this.shouldHealMP;
    }

    public void connectData(MaplePacketLittleEndianWriter mplew)
    {
        mplew.writeShort(this.str);
        mplew.writeShort(this.dex);
        mplew.writeShort(this.int_);
        mplew.writeShort(this.luk);

        mplew.writeInt(this.hp);
        mplew.writeInt(this.maxhp);
        mplew.writeInt(this.mp);
        mplew.writeInt(this.maxmp);
    }

    public void zeroData(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        mplew.write(255);
        mplew.write(255);
        mplew.write(chr.isZeroSecondLook() ? 1 : 0);
        mplew.writeInt(this.maxhp);
        mplew.writeInt(this.maxmp);
        mplew.write(0);
        mplew.writeInt(chr.isZeroSecondLook() ? chr.getSecondHair() : chr.getHair());
        mplew.writeInt(chr.isZeroSecondLook() ? chr.getSecondFace() : chr.getFace());
        mplew.writeInt(this.maxhp);
        mplew.writeInt(this.maxmp);
        mplew.writeInt(0);
    }

    public int getSkillIncrement(int skillID)
    {
        if (this.skillsIncrement.containsKey(skillID))
        {
            return this.skillsIncrement.get(skillID);
        }
        return 0;
    }

    public int getElementBoost(server.life.Element key)
    {
        if (this.elemBoosts.containsKey(key))
        {
            return this.elemBoosts.get(key);
        }
        return 0;
    }

    public int getDamageIncrease(int key)
    {
        if (this.damageIncrease.containsKey(key))
        {
            return this.damageIncrease.get(key);
        }
        return 0;
    }

    public int getAccuracy()
    {
        return this.accuracy;
    }

    public void heal(MapleCharacter chra)
    {
        heal_noUpdate(chra);
        chra.updateSingleStat(MapleStat.HP, getCurrentMaxHp());
        chra.updateSingleStat(MapleStat.MP, getCurrentMaxMp(chra.getJob()));
    }

    public void heal_noUpdate(MapleCharacter chra)
    {
        setHp(getCurrentMaxHp(), chra);
        setMp(getCurrentMaxMp(chra.getJob()), chra);
    }

    public int getCurrentMaxHp()
    {
        return this.localmaxhp;
    }

    public int getCurrentMaxMp(int job)
    {
        if ((GameConstants.isNotMpJob(job)) && (GameConstants.getMPByJob(job) <= 0))
        {
            return 50;
        }
        return this.localmaxmp;
    }

    public boolean setHp(int newhp, MapleCharacter chra)
    {
        return setHp(newhp, false, chra);
    }

    public boolean setMp(int newmp, MapleCharacter chra)
    {
        int oldMp = this.mp;
        int tmp = newmp;
        if (tmp < 0)
        {
            tmp = 0;
        }
        if (tmp > this.localmaxmp)
        {
            tmp = this.localmaxmp;
        }
        this.mp = tmp;
        return this.mp != oldMp;
    }

    public boolean setHp(int newhp, boolean silent, MapleCharacter chra)
    {
        int oldHp = this.hp;
        int thp = newhp;
        if (thp < 0)
        {
            thp = 0;
        }
        if (thp > this.localmaxhp)
        {
            thp = this.localmaxhp;
        }
        this.hp = thp;
        if (chra != null)
        {
            if (!silent)
            {
                chra.checkBerserk();
                chra.updatePartyMemberHP();
            }
            if ((oldHp > this.hp) && (!chra.isAlive()))
            {
                chra.playerDead();
            }
        }
        return this.hp != oldHp;
    }

    public int getHPPercent()
    {
        return (int) Math.ceil(this.hp * 100.0D / this.localmaxhp);
    }

    public void init(MapleCharacter chra)
    {
        recalcLocalStats(chra);
    }

    public void recalcLocalStats(MapleCharacter chra)
    {
        recalcLocalStats(false, chra);
    }

    public void recalcLocalStats(boolean first_login, MapleCharacter chra)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int oldmaxhp = this.localmaxhp;
        int localmaxhp_ = getMaxHp();
        int localmaxmp_ = getMaxMp();
        resetLocalStats(chra.getJob());
        for (MapleTraitType t : MapleTraitType.values())
        {
            chra.getTrait(t).clearLocalExp();
        }

        Object sData = new HashMap<>();
        Object itera = chra.getInventory(MapleInventoryType.EQUIPPED).newList().iterator();
        Skill skil;
        while (((Iterator) itera).hasNext())
        {
            Equip equip = (Equip) ((Iterator) itera).next();
            Map<String, Integer> eqstat;
            if ((equip.getPosition() == -11) && (constants.ItemConstants.isMagicWeapon(equip.getItemId())))
            {
                eqstat = ii.getEquipStats(equip.getItemId());
                if (eqstat != null)
                {
                    if (eqstat.containsKey("incRMAF"))
                    {
                        this.element_fire = eqstat.get("incRMAF");
                    }
                    if (eqstat.containsKey("incRMAI"))
                    {
                        this.element_ice = eqstat.get("incRMAI");
                    }
                    if (eqstat.containsKey("incRMAL"))
                    {
                        this.element_light = eqstat.get("incRMAL");
                    }
                    if (eqstat.containsKey("incRMAS"))
                    {
                        this.element_psn = eqstat.get("incRMAS");
                    }
                    if (eqstat.containsKey("elemDefault"))
                    {
                        this.def = eqstat.get("elemDefault");
                    }
                }
            }

            if ((equip.getItemId() / 10000 == 166) && (equip.getAndroid() != null) && (chra.getAndroid() == null))
            {
                chra.setAndroid(equip.getAndroid());
            }
            chra.getTrait(MapleTraitType.craft).addLocalExp(equip.getHands());
            this.accuracy += equip.getAcc();
            localmaxhp_ += equip.getHp();
            localmaxmp_ += equip.getMp();
            this.localdex += equip.getDex();
            this.localint_ += equip.getInt();
            this.localstr += equip.getStr();
            this.localluk += equip.getLuk();
            this.magic += equip.getMatk();
            this.watk += equip.getWatk();
            this.wdef += equip.getWdef();
            this.mdef += equip.getMdef();
            this.speed += equip.getSpeed();
            this.jump += equip.getJump();
            this.pvpDamage += equip.getPVPDamage();
            switch (equip.getItemId())
            {
                case 1112918:
                    this.equippedWelcomeBackRing = true;
                    break;
                case 1122017:
                    this.equippedFairy = 10;
                    break;
                case 1122158:
                    this.equippedFairy = 5;
                    break;
                case 1112585:
                    this.equippedSummon = 1085;
                    break;
                case 1112586:
                    this.equippedSummon = 1087;
                    break;
                case 1112663:
                    this.equippedSummon = 1179;
                    break;
                default:
                    for (int eb_bonus : GameConstants.Equipments_Bonus)
                    {
                        if (equip.getItemId() == eb_bonus)
                        {
                            this.equipmentBonusExp += GameConstants.Equipment_Bonus_EXP(eb_bonus);
                            break;
                        }
                    }
            }


            if (equip.getItemId() / 1000 == 1099)
            {
                this.incMaxDF += equip.getMp();
            }
            this.percent_hp += ii.getItemIncMHPr(equip.getItemId());
            this.percent_mp += ii.getItemIncMMPr(equip.getItemId());
            this.percent_boss_damage_rate += equip.getBossDamage();
            this.percent_ignore_mob_def_rate += equip.getIgnorePDR();
            this.percent_damage_rate += equip.getTotalDamage();

            Integer set = ii.getSetItemID(equip.getItemId());
            int value;
            if ((set != null) && (set > 0))
            {
                value = 1;
                if (this.setHandling.containsKey(set))
                {
                    value += this.setHandling.get(set);
                }
                this.setHandling.put(set, value);
            }
            Iterator localIterator1;
            if ((equip.getIncSkill() > 0) && (ii.getEquipSkills(equip.getItemId()) != null)) for (localIterator1 = ii.getEquipSkills(equip.getItemId()).iterator(); localIterator1.hasNext(); )
            {
                int skillId = (Integer) localIterator1.next();
                skil = SkillFactory.getSkill(skillId);
                if ((skil != null) && (skil.canBeLearnedBy(chra.getJob())))
                {
                    value = 1;
                    if (this.skillsIncrement.get(skil.getId()) != null)
                    {
                        value += this.skillsIncrement.get(skil.getId());
                    }
                    this.skillsIncrement.put(skil.getId(), value);
                }
            }
            Object ix = handleEquipAdditions(ii, chra, first_login, (Map) sData, equip.getItemId());
            if (ix != null)
            {
                localmaxhp_ += (Integer) ((Pair) ix).getLeft();
                localmaxmp_ += (Integer) ((Pair) ix).getRight();
            }
            if (equip.getState() >= 17)
            {
                int[] potentials = {equip.getPotential1(), equip.getPotential2(), equip.getPotential3(), equip.getPotential4(), equip.getPotential5(), equip.getPotential6()};
                for (int i : potentials)
                {
                    if (i > 0)
                    {
                        int itemReqLevel = ii.getReqLevel(equip.getItemId());

                        StructItemOption soc = ii.getPotentialInfo(i).get((itemReqLevel - 1) / 10);
                        if (soc != null)
                        {
                            localmaxhp_ += soc.get("incMHP");
                            localmaxmp_ += soc.get("incMMP");
                            handleItemOption(soc, chra, first_login, (Map) sData);
                        }
                    }
                }
            }
            if (equip.getSocketState() >= 19)
            {
                int[] sockets = {equip.getSocket1(), equip.getSocket2(), equip.getSocket3()};
                for (int i : sockets)
                {
                    if (i > 0)
                    {
                        StructItemOption soc = ii.getSocketInfo(i);
                        if (soc != null)
                        {
                            localmaxhp_ += soc.get("incMHP");
                            localmaxmp_ += soc.get("incMMP");
                            handleItemOption(soc, chra, first_login, (Map) sData);
                        }
                    }
                }
            }
            if (equip.getDurability() > 0)
            {
                this.durabilityHandling.add(equip);
            }
            if ((GameConstants.getMaxLevel(equip.getItemId()) > 0) && (GameConstants.getStatFromWeapon(equip.getItemId()) == null ?
                    equip.getEquipLevel() <= GameConstants.getMaxLevel(equip.getItemId()) : equip.getEquipLevel() < GameConstants.getMaxLevel(equip.getItemId())))
            {
                this.equipLevelHandling.add(equip);
            }
        }

        Iterator<Map.Entry<Integer, Integer>> iter = this.setHandling.entrySet().iterator();
        Map.Entry<Integer, Integer> entry;
        while (iter.hasNext())
        {
            entry = iter.next();
            StructSetItem setItem = ii.getSetItem(entry.getKey());
            if (setItem != null)
            {
                Map<Integer, StructSetItemStat> setItemStats = setItem.getSetItemStats();
                for (Map.Entry<Integer, StructSetItemStat> ent : (setItemStats).entrySet())
                {
                    if (ent.getKey() <= entry.getValue())
                    {
                        StructSetItemStat setItemStat = ent.getValue();
                        this.localstr += setItemStat.incSTR + setItemStat.incAllStat;
                        this.localdex += setItemStat.incDEX + setItemStat.incAllStat;
                        this.localint_ += setItemStat.incINT + setItemStat.incAllStat;
                        this.localluk += setItemStat.incLUK + setItemStat.incAllStat;
                        this.watk += setItemStat.incPAD;
                        this.magic += setItemStat.incMAD;
                        this.speed += setItemStat.incSpeed;
                        this.accuracy += setItemStat.incACC;
                        localmaxhp_ += setItemStat.incMHP;
                        localmaxmp_ += setItemStat.incMMP;
                        this.percent_hp += setItemStat.incMHPr;
                        this.percent_mp += setItemStat.incMMPr;
                        this.wdef += setItemStat.incPDD;
                        this.mdef += setItemStat.incMDD;
                        if ((setItemStat.option1 > 0) && (setItemStat.option1Level > 0))
                        {
                            StructItemOption soc = ii.getPotentialInfo(setItemStat.option1).get(setItemStat.option1Level);
                            if (soc != null)
                            {
                                localmaxhp_ += soc.get("incMHP");
                                localmaxmp_ += soc.get("incMMP");
                                handleItemOption(soc, chra, first_login, (Map) sData);
                            }
                        }
                        if ((setItemStat.option2 > 0) && (setItemStat.option2Level > 0))
                        {
                            StructItemOption soc = ii.getPotentialInfo(setItemStat.option2).get(setItemStat.option2Level);
                            if (soc != null)
                            {
                                localmaxhp_ += soc.get("incMHP");
                                localmaxmp_ += soc.get("incMMP");
                                handleItemOption(soc, chra, first_login, (Map) sData);
                            }
                        }
                    }
                }
            }
        }

        handleProfessionTool(chra);
        int hour = java.util.Calendar.getInstance().get(11);
        for (Object setItem = chra.getInventory(MapleInventoryType.CASH).newList().iterator(); ((Iterator) setItem).hasNext(); )
        {
            Item item = (Item) ((Iterator) setItem).next();
            if (item.getItemId() / 100000 == 52)
            {
                if ((this.expMod < 3.0D) && (item.getItemId() == 5211060))
                {
                    this.expMod = 3.0D;
                }
                else if ((this.expMod < 2.0D) && ((item.getItemId() == 5210000) || (item.getItemId() == 5210001) || (item.getItemId() == 5210002) || (item.getItemId() == 5210003) || (item.getItemId() == 5210004) || (item.getItemId() == 5210005) || (item.getItemId() == 5210006) || (item.getItemId() == 5211047)))
                {
                    this.expMod = 2.0D;
                }
                else if ((this.expMod < 1.5D) && ((item.getItemId() == 5211063) || (item.getItemId() == 5211064) || (item.getItemId() == 5211065) || (item.getItemId() == 5211066) || (item.getItemId() == 5211069) || (item.getItemId() == 5211070)))
                {
                    this.expMod = 1.5D;
                }
                else if ((this.expMod < 1.2D) && ((item.getItemId() == 5211071) || (item.getItemId() == 5211072) || (item.getItemId() == 5211073) || (item.getItemId() == 5211074) || (item.getItemId() == 5211075) || (item.getItemId() == 5211076) || (item.getItemId() == 5211067)))
                {
                    this.expMod = 1.2D;
                }
            }
            else if ((this.dropMod == 1) && (item.getItemId() / 10000 == 536))
            {
                if ((item.getItemId() == 5360000) || (item.getItemId() == 5360014) || (item.getItemId() == 5360015) || (item.getItemId() == 5360016))
                {
                    this.dropMod = 2;
                }
            }
            else if (item.getItemId() == 5650000)
            {
                this.hasPartyBonus = true;
            }
            else if (item.getItemId() == 5590001)
            {
                this.levelBonus = 10;
            }
            else if ((this.levelBonus == 0) && (item.getItemId() == 5590000))
            {
                this.levelBonus = 5;
            }
            else if (item.getItemId() == 5710000)
            {
                this.questBonus = 2;
            }
            else if (item.getItemId() == 5340000)
            {
                this.canFish = true;
            }
            else if (item.getItemId() == 5340001)
            {
                this.canFish = true;
                this.canFishVIP = true;
            }
        }
        for (Item item : chra.getInventory(MapleInventoryType.ETC).list())
        {
            switch (item.getItemId())
            {
                case 4030003:
                    break;
                case 4030004:

            }

        }


        if ((first_login) && (chra.getLevel() >= 30))
        {
            if (chra.isGM())
            {
                for (int allJob : allJobs)
                {
                    ((Map) sData).put(SkillFactory.getSkill(1085 + allJob), new SkillEntry(1, (byte) 0, -1L));
                    ((Map) sData).put(SkillFactory.getSkill(1087 + allJob), new SkillEntry(1, (byte) 0, -1L));
                    ((Map) sData).put(SkillFactory.getSkill(1179 + allJob), new SkillEntry(1, (byte) 0, -1L));
                }
            }
            else
            {
                ((Map) sData).put(SkillFactory.getSkill(getSkillByJob(1085, chra.getJob())), new SkillEntry(1, (byte) 0, -1L));
                ((Map) sData).put(SkillFactory.getSkill(getSkillByJob(1087, chra.getJob())), new SkillEntry(1, (byte) 0, -1L));
                ((Map) sData).put(SkillFactory.getSkill(getSkillByJob(1179, chra.getJob())), new SkillEntry(1, (byte) 0, -1L));
            }
        }
        if (this.equippedSummon > 0)
        {
            this.equippedSummon = getSkillByJob(this.equippedSummon, chra.getJob());
        }


        if (chra.getCoreAura() != null)
        {
            this.watk += chra.getCoreAura().getWatk();
            this.magic += chra.getCoreAura().getMagic();
            this.localstr += chra.getCoreAura().getStr();
            this.localdex += chra.getCoreAura().getDex();
            this.localint_ += chra.getCoreAura().getInt();
            this.localluk += chra.getCoreAura().getLuk();
        }


        handlePassiveSkills(chra);


        handleBuffStats(chra);
        Integer buff = chra.getBuffedValue(MapleBuffStat.增强_MAXHP);
        if (buff != null)
        {
            localmaxhp_ += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.增强_MAXMP);
        if (buff != null)
        {
            localmaxmp_ += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.indieMaxHp);
        if (buff != null)
        {
            localmaxhp_ += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.indieMaxMp);
        if (buff != null)
        {
            localmaxmp_ += buff;
        }
        MapleStatEffect eff = chra.getStatForBuff(MapleBuffStat.进阶祝福);
        if (eff != null)
        {
            this.watk += eff.getX();
            this.magic += eff.getY();
            this.accuracy += eff.getV();
            this.mpconReduce += eff.getMPConReduce();
        }


        Skill bx = SkillFactory.getSkill(5710004);
        int bof = chra.getTotalSkillLevel(bx);
        if (bof > 0)
        {
            eff = bx.getEffect(bof);
            this.percent_wdef += eff.getWDEFRate();
            this.percent_mdef += eff.getMDEFRate();
            localmaxhp_ += eff.getMaxHpX();
            localmaxmp_ += eff.getMaxMpX();
        }
        bx = SkillFactory.getSkill(5210012);
        bof = chra.getTotalSkillLevel(bx);
        if (bof > 0)
        {
            eff = bx.getEffect(bof);
            this.percent_wdef += eff.getWDEFRate();
            this.percent_mdef += eff.getMDEFRate();
            localmaxhp_ += eff.getMaxHpX();
            localmaxmp_ += eff.getMaxMpX();
        }
        bx = SkillFactory.getSkill(36101003);
        bof = chra.getTotalSkillLevel(bx);
        if (bof > 0)
        {
            eff = bx.getEffect(bof);
            localmaxhp_ += eff.getMaxHpX();
            localmaxmp_ += eff.getMaxMpX();
        }
        bx = SkillFactory.getSkill(65110005);
        bof = chra.getTotalSkillLevel(bx);
        if (bof > 0)
        {
            eff = bx.getEffect(bof);
            this.wdef += eff.getWdefX();
            this.mdef += eff.getMdefX();
            localmaxhp_ += eff.getMaxHpX();
        }

        long now;

        if (chra.getGuildId() > 0)
        {
            MapleGuild g = handling.world.WorldGuildService.getInstance().getGuild(chra.getGuildId());
            if ((g != null) && (g.getSkills().size() > 0))
            {
                now = System.currentTimeMillis();
                for (MapleGuildSkill gs : g.getSkills())
                {
                    if ((gs.timestamp > now) && (gs.activator.length() > 0))
                    {
                        MapleStatEffect e = SkillFactory.getSkill(gs.skillID).getEffect(gs.level);
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + e.getCritical()));
                        this.watk += e.getAttackX();
                        this.magic += e.getMagicX();
                        this.expBuff *= (e.getEXPRate() + 100.0D) / 100.0D;
                        this.dodgeChance += e.getER();
                        this.percent_wdef += e.getWDEFRate();
                        this.percent_mdef += e.getMDEFRate();
                    }
                }
            }
        }


        Object cardSkills = new LinkedList<>();
        for (Pair<Integer, Integer> ix : chra.getCharacterCard().getCardEffects())
        {
            MapleStatEffect cardEff = SkillFactory.getSkill(ix.getLeft()).getEffect(ix.getRight());
            if ((cardEff != null) && ((!((List) cardSkills).contains(ix.getLeft())) || (ix.getLeft() >= 71001100)))
            {

                ((List) cardSkills).add(ix.getLeft());
                if (server.ServerProperties.ShowPacket())
                {
                    System.err.println("角色卡系统 - 等级: " + ix.getRight() + " 技能: " + ix.getLeft() + " - " + SkillFactory.getSkillName(ix.getLeft()));
                }
                this.percent_wdef += cardEff.getWDEFRate();
                this.damX = ((int) (this.damX + cardEff.getLevelToWatkX() * chra.getLevel() * 0.5D));
                this.damX = ((int) (this.damX + cardEff.getLevelToMatkX() * chra.getLevel() * 0.5D));
                this.percent_hp += cardEff.getPercentHP();
                this.percent_mp += cardEff.getPercentMP();
                this.RecoveryUP += cardEff.getMPConsumeEff();
                this.percent_acc += cardEff.getPercentAcc();
                this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + cardEff.getCritical()));
                this.jump += cardEff.getPassiveJump();
                this.speed += cardEff.getPassiveSpeed();
                this.dodgeChance += cardEff.getPercentAvoid();
                this.damX += cardEff.getLevelToDamageX() * chra.getLevel();
                this.BuffUP_Summon += cardEff.getSummonTimeInc();
                this.expLossReduceR += cardEff.getEXPLossRate();
                this.ASR += cardEff.getASRRate();

                this.suddenDeathR = ((int) (this.suddenDeathR + cardEff.getSuddenDeathR() * 0.5D));
                this.BuffUP_Skill += cardEff.getBuffTimeRate();


                this.coolTimeR += cardEff.getCooltimeReduceR();
                this.incMesoProp += cardEff.getMesoAcquisition();
                this.damX = ((int) (this.damX + Math.floor(cardEff.getHpToDamageX() * oldmaxhp / 100.0F)));
                this.damX = ((int) (this.damX + Math.floor(cardEff.getMpToDamageX() * oldmaxhp / 100.0F)));

                this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + cardEff.getCriticalMax()));
                this.percent_ignore_mob_def_rate += cardEff.getIgnoreMob();
                this.localstr += cardEff.getStrX();
                this.localdex += cardEff.getDexX();
                this.localint_ += cardEff.getIntX();
                this.localluk += cardEff.getLukX();
                this.IndieStrFX += cardEff.getStrFX();
                this.IndieDexFX += cardEff.getDexFX();
                this.IndieIntFX += cardEff.getIntFX();
                this.IndieLukFX += cardEff.getLukFX();
                localmaxhp_ += cardEff.getMaxHpX();
                localmaxmp_ += cardEff.getMaxMpX();
                this.watk += cardEff.getAttackX();
                this.magic += cardEff.getMagicX();
                this.percent_boss_damage_rate += cardEff.getBossDamage();
            }
        }
        ((List) cardSkills).clear();

        InnerSkillEntry innerSkill;
        MapleStatEffect InnerEffect;
        for (int i = 0; i < 3; i++)
        {
            innerSkill = chra.getInnerSkills()[i];
            if (innerSkill != null)
            {

                InnerEffect = SkillFactory.getSkill(innerSkill.getSkillId()).getEffect(innerSkill.getSkillLevel());
                if (InnerEffect != null)
                {

                    this.wdef += InnerEffect.getWdefX();
                    this.mdef += InnerEffect.getWdefX();
                    this.percent_wdef += InnerEffect.getWDEFRate();
                    this.percent_mdef += InnerEffect.getMDEFRate();
                    this.percent_hp += InnerEffect.getPercentHP();
                    this.percent_mp += InnerEffect.getPercentMP();
                    this.accuracy += InnerEffect.getAccX();
                    this.dodgeChance += InnerEffect.getPercentAvoid();
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + InnerEffect.getCritical()));
                    this.jump += InnerEffect.getPassiveJump();
                    this.speed += InnerEffect.getPassiveSpeed();
                    this.IndieStrFX += InnerEffect.getStrFX();
                    this.IndieDexFX += InnerEffect.getDexFX();
                    this.IndieIntFX += InnerEffect.getIntFX();
                    this.IndieLukFX += InnerEffect.getLukFX();
                    localmaxhp_ += InnerEffect.getMaxHpX();
                    localmaxmp_ += InnerEffect.getMaxMpX();
                    this.watk += InnerEffect.getAttackX();
                    this.magic += InnerEffect.getMagicX();
                    if (InnerEffect.getDexToStr() > 0)
                    {
                        this.IndieStrFX = ((int) (this.IndieStrFX + Math.floor(getDex() * InnerEffect.getDexToStr() / 100.0F)));
                    }
                    if (InnerEffect.getStrToDex() > 0)
                    {
                        this.IndieDexFX = ((int) (this.IndieDexFX + Math.floor(getStr() * InnerEffect.getStrToDex() / 100.0F)));
                    }
                    if (InnerEffect.getIntToLuk() > 0)
                    {
                        this.IndieLukFX = ((int) (this.IndieLukFX + Math.floor(getInt() * InnerEffect.getIntToLuk() / 100.0F)));
                    }
                    if (InnerEffect.getLukToDex() > 0)
                    {
                        this.IndieDexFX = ((int) (this.IndieDexFX + Math.floor(getLuk() * InnerEffect.getLukToDex() / 100.0F)));
                    }
                    if (InnerEffect.getLevelToWatk() > 0)
                    {
                        this.watk = ((int) (this.watk + Math.floor(chra.getLevel() / InnerEffect.getLevelToWatk())));
                    }
                    if (InnerEffect.getLevelToMatk() > 0)
                    {
                        this.magic = ((int) (this.magic + Math.floor(chra.getLevel() / InnerEffect.getLevelToMatk())));
                    }
                    this.percent_boss_damage_rate += InnerEffect.getBossDamage();
                }
            }
        }
        double d;
        if (GameConstants.is尖兵(chra.getJob()))
        {
            d = chra.getSpecialStat().getPowerCount() / 100.0D;
            this.localstr = ((int) (this.localstr + d * this.str));
            this.localdex = ((int) (this.localdex + d * this.dex));
            this.localluk = ((int) (this.localluk + d * this.luk));
            this.localint_ = ((int) (this.localint_ + d * this.int_));
        }
        this.localstr = ((int) (this.localstr + (Math.floor(this.localstr * this.percent_str / 100.0F) + this.IndieStrFX)));
        this.localdex = ((int) (this.localdex + (Math.floor(this.localdex * this.percent_dex / 100.0F) + this.IndieDexFX)));
        this.localint_ = ((int) (this.localint_ + (Math.floor(this.localint_ * this.percent_int / 100.0F) + this.IndieIntFX)));
        this.localluk = ((int) (this.localluk + (Math.floor(this.localluk * this.percent_luk / 100.0F) + this.IndieLukFX)));
        if (this.localint_ > this.localdex)
        {
            this.accuracy = ((int) (this.accuracy + Math.floor(this.localint_ * 1.6D + this.localluk * 0.8D + this.localdex * 0.4D)));
        }
        else
        {
            this.accuracy = ((int) (this.accuracy + Math.floor(this.localdex * 1.6D + this.localluk * 0.8D + this.localstr * 0.4D)));
        }
        this.watk = ((int) (this.watk + Math.floor(this.watk * this.percent_atk / 100.0F)));
        this.magic = ((int) (this.magic + Math.floor(this.magic * this.percent_matk / 100.0F)));
        this.localint_ = ((int) (this.localint_ + Math.floor(this.localint_ * this.percent_matk / 100.0F)));

        this.wdef = ((int) (this.wdef + Math.floor(this.localstr * 1.5D + (this.localdex + this.localluk) * 0.4D)));
        this.mdef = ((int) (this.mdef + Math.floor(this.localint_ * 1.5D + (this.localdex + this.localluk) * 0.4D)));
        this.wdef += chra.getTrait(MapleTraitType.will).getLevel();
        this.mdef += chra.getTrait(MapleTraitType.will).getLevel();
        this.wdef = ((int) (this.wdef + Math.min(9999.0D, Math.floor(this.wdef * this.percent_wdef / 100.0F))));
        this.mdef = ((int) (this.mdef + Math.min(9999.0D, Math.floor(this.mdef * this.percent_mdef / 100.0F))));

        this.hands = (this.localdex + this.localint_ + this.localluk);
        calculateFame(chra);
        this.percent_ignore_mob_def_rate += chra.getTrait(MapleTraitType.charisma).getLevel() / 10;
        this.pvpDamage += chra.getTrait(MapleTraitType.charisma).getLevel() / 10;
        this.ASR += chra.getTrait(MapleTraitType.will).getLevel() / 5;

        this.accuracy += chra.getTrait(MapleTraitType.insight).getLevel() * 15 / 10;
        this.accuracy = ((int) (this.accuracy + Math.min(9999.0D, Math.floor(this.accuracy * this.percent_acc / 100.0F))));


        localmaxhp_ += chra.getTrait(MapleTraitType.will).getLevel() / 5 * 100;
        localmaxhp_ += this.addmaxhp;
        localmaxhp_ = (int) (localmaxhp_ + Math.floor(this.percent_hp * localmaxhp_ / 100.0F));
        this.localmaxhp = Math.min(chra.getMaxHpForSever(), Math.abs(Math.max(-chra.getMaxHpForSever(), localmaxhp_)));


        localmaxmp_ += chra.getTrait(MapleTraitType.sense).getLevel() / 5 * 100;
        localmaxmp_ = (int) (localmaxmp_ + Math.floor(this.percent_mp * localmaxmp_ / 100.0F));
        localmaxmp_ += this.addmaxmp;
        this.localmaxmp = Math.min(chra.getMaxMpForSever(), Math.abs(Math.max(-chra.getMaxMpForSever(), localmaxmp_)));
        if ((chra.getEventInstance() != null) && (chra.getEventInstance().getName().startsWith("PVP")))
        {
            this.localmaxhp = Math.min(40000, this.localmaxhp * 3);
            this.localmaxmp = Math.min(20000, this.localmaxmp * 2);

            for (int i = 0; i < pvpSkills.length; i++)
            {
                skil = SkillFactory.getSkill(pvpSkills[i]);
                if ((skil != null) && (skil.canBeLearnedBy(chra.getJob())))
                {
                    ((Map) sData).put(skil, new SkillEntry(1, (byte) 0, -1L));
                    eff = skil.getEffect(1);
                    switch (i / 1000000 % 10)
                    {
                        case 1:
                            if (eff.getX() > 0)
                            {
                                this.pvpDamage += this.wdef / eff.getX();
                            }
                            break;
                        case 3:
                            this.hpRecoverProp += eff.getProb();
                            this.hpRecover += eff.getX();
                            this.mpRecoverProp += eff.getProb();
                            this.mpRecover += eff.getX();
                            break;
                        case 5:
                            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getProb()));
                            this.passive_sharpeye_max_percent = 100;
                    }

                    break;
                }
            }
            eff = chra.getStatForBuff(MapleBuffStat.变身术);
            if ((eff != null) && (eff.getSourceId() % 10000 == 1105))
            {
                this.localmaxhp = 99999;
                this.localmaxmp = 99999;
            }
        }


        chra.changeSkillLevel_Skip((Map) sData, false);


        if (GameConstants.is恶魔猎手(chra.getJob()))
        {
            this.localmaxmp = GameConstants.getMPByJob(chra.getJob());
            this.localmaxmp += this.incMaxDF;
        }
        else if (GameConstants.is神之子(chra.getJob()))
        {
            this.localmaxmp = 100;
        }
        else if (GameConstants.isNotMpJob(chra.getJob()))
        {
            this.localmaxmp = 10;
        }


        if (GameConstants.is尖兵(chra.getJob()))
        {
            int[] skillIds = {30020234, 36000004, 36100007, 36110007, 36120010};


            for (int i : skillIds)
            {
                bx = SkillFactory.getSkill(i);
                if (bx != null)
                {
                    bof = chra.getSkillLevel(bx);
                    if (bof > 0)
                    {
                        eff = bx.getEffect(bof);
                        if (this.localdex >= eff.getX())
                        {
                            this.ASR += eff.getZ();
                            this.TER += eff.getZ();
                        }
                        if (this.localluk >= eff.getX())
                        {
                            this.dodgeChance += eff.getZ();
                        }
                        if ((this.localstr >= eff.getX()) && (this.localdex >= eff.getX()) && (this.localluk >= eff.getX()))
                        {
                            this.percent_damage_rate += eff.getW();
                            this.percent_boss_damage_rate += eff.getW();
                        }
                    }
                }
            }
        }
        CalcPassive_SharpEye(chra);
        CalcPassive_Mastery(chra);
        recalcPVPRank(chra);
        if (first_login)
        {
            chra.silentEnforceMaxHpMp();
            relocHeal(chra);
        }
        else
        {
            chra.enforceMaxHpMp();
        }
        chra.check血之契约();
        calculateMaxBaseDamage(Math.max(this.magic, this.watk), this.damX, this.pvpDamage, chra);
        this.trueMastery = Math.min(100, this.trueMastery);
        this.passive_sharpeye_min_percent = ((short) Math.min(this.passive_sharpeye_min_percent, this.passive_sharpeye_max_percent));
        if ((oldmaxhp != 0) && (oldmaxhp != this.localmaxhp))
        {
            chra.updatePartyMemberHP();
        }
    }

    public int getMaxHp()
    {
        return this.maxhp;
    }

    public int getMaxMp()
    {
        return this.maxmp;
    }

    private void resetLocalStats(int job)
    {
        this.accuracy = 0;
        this.wdef = 0;
        this.mdef = 0;
        this.damX = 0;
        this.addmaxhp = 0;
        this.addmaxmp = 0;
        this.localdex = getDex();
        this.localint_ = getInt();
        this.localstr = getStr();
        this.localluk = getLuk();
        this.IndieDexFX = 0;
        this.IndieIntFX = 0;
        this.IndieStrFX = 0;
        this.IndieLukFX = 0;
        this.speed = 100;
        this.jump = 100;
        this.pickupRange = 0.0D;
        this.decreaseDebuff = 0;
        this.ASR = 0;
        this.TER = 0;
        this.dot = 0;
        this.questBonus = 1;
        this.dotTime = 0;
        this.trueMastery = 0;
        this.stanceProp = 0;
        this.percent_wdef = 0;
        this.percent_mdef = 0;
        this.percent_hp = 0;
        this.percent_mp = 0;
        this.percent_str = 0;
        this.percent_dex = 0;
        this.percent_int = 0;
        this.percent_luk = 0;
        this.percent_acc = 0;
        this.percent_atk = 0;
        this.percent_matk = 0;
        this.percent_ignore_mob_def_rate = 0;
        this.passive_sharpeye_rate = 5;
        this.passive_sharpeye_min_percent = 20;
        this.passive_sharpeye_max_percent = 50;
        this.percent_damage_rate = 100;
        this.percent_boss_damage_rate = 100;
        this.magic = 0;
        this.watk = 0;
        this.dodgeChance = 0;
        this.pvpDamage = 0;
        this.mesoGuard = 50.0D;
        this.mesoGuardMeso = 0.0D;
        this.percent_damage = 0.0D;
        this.expBuff = 100.0D;
        this.cashBuff = 100.0D;
        this.dropBuff = 100.0D;
        this.mesoBuff = 100.0D;
        this.recoverHP = 0;
        this.recoverMP = 0;
        this.mpconReduce = 0;
        this.mpconPercent = 100;
        this.incMesoProp = 0;
        this.reduceCooltime = 0;
        this.coolTimeR = 0;
        this.suddenDeathR = 0;
        this.expLossReduceR = 0;
        this.incRewardProp = 0.0D;
        this.DAMreflect = 0;
        this.DAMreflect_rate = 0;
        this.ignoreDAMr = 0;
        this.ignoreDAMr_rate = 0;
        this.ignoreDAM = 0;
        this.ignoreDAM_rate = 0;
        this.hpRecover = 0;
        this.hpRecoverProp = 0;
        this.hpRecoverPercent = 0;
        this.mpRecover = 0;
        this.mpRecoverProp = 0;
        this.mpRestore = 0;
        this.pickRate = 0;
        this.incMaxDamage = 0;
        this.equippedWelcomeBackRing = false;
        this.equippedFairy = 0;
        this.equippedSummon = 0;
        this.hasPartyBonus = false;
        this.hasClone = false;
        this.Berserk = false;
        this.canFish = false;
        this.canFishVIP = false;
        this.equipmentBonusExp = 0;
        this.RecoveryUP = 100;
        this.BuffUP = 100;
        this.RecoveryUP_Skill = 100;
        this.BuffUP_Skill = 100;
        this.BuffUP_Summon = 100;
        this.dropMod = 1;
        this.expMod = 1.0D;
        this.cashMod = 1;
        this.levelBonus = 0;
        this.incMaxDF = 0;
        this.incAllskill = 0;
        this.combatOrders = 0;
        this.defRange = (isRangedJob(job) ? 200 : 0);
        this.durabilityHandling.clear();
        this.equipLevelHandling.clear();
        this.skillsIncrement.clear();
        this.damageIncrease.clear();
        this.setHandling.clear();
        this.add_skill_duration.clear();
        this.add_skill_attackCount.clear();
        this.add_skill_targetPlus.clear();
        this.add_skill_dotTime.clear();
        this.add_skill_prop.clear();
        this.add_skill_coolTimeR.clear();
        this.add_skill_ignoreMobpdpR.clear();
        this.harvestingTool = 0;
        this.element_fire = 100;
        this.element_ice = 100;
        this.element_light = 100;
        this.element_psn = 100;
        this.def = 100;
        this.raidenCount = 0;
        this.raidenPorp = 0;
        this.ignore_mob_damage_rate = 0;
        this.reduceDamageRate = 0;
    }

    public Pair<Integer, Integer> handleEquipAdditions(MapleItemInformationProvider ii, MapleCharacter chra, boolean first_login, Map<Skill, SkillEntry> sData, int itemId)
    {
        List<Triple<String, String, String>> additions = ii.getEquipAdditions(itemId);
        if (additions == null)
        {
            return null;
        }
        int localmaxhp_x = 0;
        int localmaxmp_x = 0;
        int skillid = 0;
        int skilllevel = 0;

        for (Triple<String, String, String> add : additions)
            if (!add.getMid().contains("con"))
            {

                int right = add.getMid().equals("elemVol") ? 0 : Integer.parseInt(add.getRight());
                if (add.getLeft().equals("elemboost"))
                {
                    String craft = ii.getEquipAddReqs(itemId, add.getLeft(), "craft");
                    if ((add.getMid().equals("elemVol")) && ((craft == null) || (chra.getTrait(MapleTraitType.craft).getLocalTotalExp() >= Integer.parseInt(craft))))
                    {
                        int value = Integer.parseInt(add.getRight().substring(1));
                        server.life.Element key = server.life.Element.getFromChar(add.getRight().charAt(0));
                        if (this.elemBoosts.get(key) != null)
                        {
                            value += this.elemBoosts.get(key);
                        }
                        this.elemBoosts.put(key, value);
                    }
                }
                else if (add.getLeft().equals("mobcategory"))
                {
                    if (add.getMid().equals("damage"))
                    {
                        this.percent_damage_rate += right;
                        this.percent_boss_damage_rate += right;
                    }
                }
                else if (add.getLeft().equals("critical"))
                {
                    boolean canJob = false;
                    boolean canLevel = false;
                    String job = ii.getEquipAddReqs(itemId, add.getLeft(), "job");
                    if (job != null)
                    {
                        if (job.contains(","))
                        {
                            String[] jobs = job.split(",");
                            for (String x : jobs)
                            {
                                if (chra.getJob() == Integer.parseInt(x))
                                {
                                    canJob = true;
                                }
                            }
                        }
                        else if (chra.getJob() == Integer.parseInt(job))
                        {
                            canJob = true;
                        }
                    }

                    String level = ii.getEquipAddReqs(itemId, add.getLeft(), "level");
                    if ((level != null) && (chra.getLevel() >= Integer.parseInt(level)))
                    {
                        canLevel = true;
                    }

                    if (((job != null) && (canJob)) || ((job == null) && (level == null || (canLevel))))
                    {
                        if (add.getMid().equals("prob"))
                        {
                            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + right));
                        }
                        else if (add.getMid().equals("damage"))
                        {
                            this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + right));
                            this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + right));
                        }
                    }
                }
                else if (add.getLeft().equals("boss"))
                {
                    String craft = ii.getEquipAddReqs(itemId, add.getLeft(), "craft");
                    if ((add.getMid().equals("damage")) && ((craft == null) || (chra.getTrait(MapleTraitType.craft).getLocalTotalExp() >= Integer.parseInt(craft))))
                    {
                        this.percent_boss_damage_rate += right;
                    }
                }
                else if (add.getLeft().equals("mobdie"))
                {
                    String craft = ii.getEquipAddReqs(itemId, add.getLeft(), "craft");
                    if ((craft == null) || (chra.getTrait(MapleTraitType.craft).getLocalTotalExp() >= Integer.parseInt(craft)))
                    {
                        if (add.getMid().equals("hpIncOnMobDie"))
                        {
                            this.hpRecover += right;
                            this.hpRecoverProp += 5;
                        }
                        else if (add.getMid().equals("mpIncOnMobDie"))
                        {
                            this.mpRecover += right;
                            this.mpRecoverProp += 5;
                        }
                    }
                }
                else if (add.getLeft().equals("skill"))
                {
                    if (first_login)
                    {
                        String craft = ii.getEquipAddReqs(itemId, add.getLeft(), "craft");
                        if ((craft == null) || (chra.getTrait(MapleTraitType.craft).getLocalTotalExp() >= Integer.parseInt(craft)))
                        {
                            if (add.getMid().equals("id"))
                            {
                                skillid = right;
                            }
                            else if (add.getMid().equals("level"))
                            {
                                skilllevel = right;
                            }
                        }
                    }
                }
                else if (add.getLeft().equals("hpmpchange"))
                {
                    if (add.getMid().equals("hpChangerPerTime"))
                    {
                        this.recoverHP += right;
                    }
                    else if (add.getMid().equals("mpChangerPerTime"))
                    {
                        this.recoverMP += right;
                    }
                }
                else if (add.getLeft().equals("statinc"))
                {
                    boolean canJobx = false;
                    boolean canLevelx = false;
                    String job = ii.getEquipAddReqs(itemId, add.getLeft(), "job");
                    if (job != null)
                    {
                        if (job.contains(","))
                        {
                            String[] jobs = job.split(",");
                            for (String x : jobs)
                            {
                                if (chra.getJob() == Integer.parseInt(x))
                                {
                                    canJobx = true;
                                }
                            }
                        }
                        else if (chra.getJob() == Integer.parseInt(job))
                        {
                            canJobx = true;
                        }
                    }
                    String level = ii.getEquipAddReqs(itemId, add.getLeft(), "level");
                    if ((level != null) && (chra.getLevel() >= Integer.parseInt(level)))
                    {
                        canLevelx = true;
                    }
                    if (((canJobx) || (job == null)) && ((canLevelx) || (level == null)))
                    {

                        if (itemId == 1142367)
                        {
                            int day = java.util.Calendar.getInstance().get(7);
                            if ((day != 1) && (day != 7))
                            {
                            }


                        }
                        else if (add.getMid().equals("incPAD"))
                        {
                            this.watk += right;
                        }
                        else if (add.getMid().equals("incMAD"))
                        {
                            this.magic += right;
                        }
                        else if (add.getMid().equals("incSTR"))
                        {
                            this.localstr += right;
                        }
                        else if (add.getMid().equals("incDEX"))
                        {
                            this.localdex += right;
                        }
                        else if (add.getMid().equals("incINT"))
                        {
                            this.localint_ += right;
                        }
                        else if (add.getMid().equals("incLUK"))
                        {
                            this.localluk += right;
                        }
                        else if (add.getMid().equals("incJump"))
                        {
                            this.jump += right;
                        }
                        else if (add.getMid().equals("incMHP"))
                        {
                            localmaxhp_x += right;
                        }
                        else if (add.getMid().equals("incMMP"))
                        {
                            localmaxmp_x += right;
                        }
                        else if (add.getMid().equals("incPDD"))
                        {
                            this.wdef += right;
                        }
                        else if (add.getMid().equals("incMDD"))
                        {
                            this.mdef += right;
                        }
                        else if (add.getMid().equals("incACC"))
                        {
                            this.accuracy += right;
                        }
                        else if (!add.getMid().equals("incEVA"))
                        {
                            if (add.getMid().equals("incSpeed"))
                            {
                                this.speed += right;
                            }
                            else if (add.getMid().equals("incMMPr")) this.percent_mp += right;
                        }
                    }
                }
            }
        if ((skillid != 0) && (skilllevel != 0))
        {
            sData.put(SkillFactory.getSkill(skillid), new SkillEntry((byte) skilllevel, (byte) 0, -1L));
        }
        return new Pair(localmaxhp_x, localmaxmp_x);
    }

    public void handleItemOption(StructItemOption soc, MapleCharacter chra, boolean first_login, Map<Skill, SkillEntry> sData)
    {
        this.localstr += soc.get("incSTR");
        this.localdex += soc.get("incDEX");
        this.localint_ += soc.get("incINT");
        this.localluk += soc.get("incLUK");
        if (soc.get("incSTRlv") > 0)
        {
            this.localstr += chra.getLevel() / 10 * soc.get("incSTRlv");
        }
        if (soc.get("incDEXlv") > 0)
        {
            this.localdex += chra.getLevel() / 10 * soc.get("incDEXlv");
        }
        if (soc.get("incINTlv") > 0)
        {
            this.localint_ += chra.getLevel() / 10 * soc.get("incINTlv");
        }
        if (soc.get("incLUKlv") > 0)
        {
            this.localluk += chra.getLevel() / 10 * soc.get("incLUKlv");
        }
        this.accuracy += soc.get("incACC");

        this.speed += soc.get("incSpeed");
        this.jump += soc.get("incJump");
        this.watk += soc.get("incPAD");
        if (soc.get("incPADlv") > 0)
        {
            this.watk += chra.getLevel() / 10 * soc.get("incPADlv");
        }
        this.magic += soc.get("incMAD");
        if (soc.get("incMADlv") > 0)
        {
            this.magic += chra.getLevel() / 10 * soc.get("incMADlv");
        }
        this.wdef += soc.get("incPDD");
        this.mdef += soc.get("incMDD");
        this.percent_str += soc.get("incSTRr");
        this.percent_dex += soc.get("incDEXr");
        this.percent_int += soc.get("incINTr");
        this.percent_luk += soc.get("incLUKr");
        this.percent_hp += soc.get("incMHPr");
        this.percent_mp += soc.get("incMMPr");
        this.percent_acc += soc.get("incACCr");
        this.dodgeChance += soc.get("incEVAr");
        this.percent_atk += soc.get("incPADr");
        this.percent_matk += soc.get("incMADr");
        this.percent_wdef += soc.get("incPDDr");
        this.percent_mdef += soc.get("incMDDr");
        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + soc.get("incCr")));
        this.percent_boss_damage_rate += soc.get("incDAMr");
        if (soc.get("boss") <= 0)
        {
            this.percent_damage_rate += soc.get("incDAMr");
        }
        this.recoverHP += soc.get("RecoveryHP");
        this.recoverMP += soc.get("RecoveryMP");
        if (soc.get("HP") > 0)
        {
            this.hpRecover += soc.get("HP");
            this.hpRecoverProp += soc.get("prop");
        }
        if ((soc.get("MP") > 0) && (!GameConstants.isNotMpJob(chra.getJob())))
        {
            this.mpRecover += soc.get("MP");
            this.mpRecoverProp += soc.get("prop");
        }
        this.percent_ignore_mob_def_rate += soc.get("ignoreTargetDEF");
        if (soc.get("ignoreDAM") > 0)
        {
            this.ignoreDAM += soc.get("ignoreDAM");
            this.ignoreDAM_rate += soc.get("prop");
        }
        this.incAllskill += soc.get("incAllskill");
        if (soc.get("ignoreDAMr") > 0)
        {
            this.ignoreDAMr += soc.get("ignoreDAMr");
            this.ignoreDAMr_rate += soc.get("prop");
        }
        if (soc.get("incMaxDamage") > 0)
        {
            this.incMaxDamage += soc.get("incMaxDamage");
        }
        this.RecoveryUP += soc.get("RecoveryUP");
        this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + soc.get("incCriticaldamageMin")));
        this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + soc.get("incCriticaldamageMax")));
        this.TER += soc.get("incTerR");
        this.ASR += soc.get("incAsrR");
        if (soc.get("DAMreflect") > 0)
        {
            this.DAMreflect += soc.get("DAMreflect");
            this.DAMreflect_rate += soc.get("prop");
        }
        this.mpconReduce += soc.get("mpconReduce");
        this.reduceCooltime += soc.get("reduceCooltime");
        this.incMesoProp += soc.get("incMesoProp");
        this.incRewardProp += soc.get("incRewardProp");
        if ((first_login) && (soc.get("skillID") > 0))
        {
            sData.put(SkillFactory.getSkill(getSkillByJob(soc.get("skillID"), chra.getJob())), new SkillEntry(1, (byte) 0, -1L));
        }
    }

    public void handleProfessionTool(MapleCharacter chra)
    {
        if ((chra.getProfessionLevel(92000000) > 0) || (chra.getProfessionLevel(92010000) > 0))
        {
            for (Item item : chra.getInventory(MapleInventoryType.EQUIP).newList())
            {
                Equip equip = (Equip) item;
                if (((equip.getDurability() != 0) && (equip.getItemId() / 10000 == 150) && (chra.getProfessionLevel(92000000) > 0)) || ((equip.getItemId() / 10000 == 151) && (chra.getProfessionLevel(92010000) > 0)))
                {
                    if (equip.getDurability() > 0)
                    {
                        this.durabilityHandling.add(equip);
                    }
                    this.harvestingTool = equip.getPosition();
                    break;
                }
            }
        }
    }

    public static int getSkillByJob(int skillId, int job)
    {
        if (GameConstants.is骑士团(job)) return skillId + 10000000;
        if (GameConstants.is战神(job)) return skillId + 20000000;
        if (GameConstants.is龙神(job)) return skillId + 20010000;
        if (GameConstants.is双弩精灵(job)) return skillId + 20020000;
        if (GameConstants.is幻影(job)) return skillId + 20030000;
        if (GameConstants.is夜光(job)) return skillId + 20040000;
        if (GameConstants.is反抗者(job)) return skillId + 30000000;
        if ((GameConstants.is恶魔猎手(job)) || (GameConstants.is恶魔复仇者(job))) return skillId + 30010000;
        if (GameConstants.is尖兵(job)) return skillId + 30020000;
        if (GameConstants.is米哈尔(job)) return skillId + 50000000;
        if (GameConstants.is狂龙战士(job)) return skillId + 60000000;
        if (GameConstants.is爆莉萌天使(job)) return skillId + 60010000;
        if (GameConstants.is神之子(job)) return skillId + 100000000;
        if (GameConstants.is林之灵(job))
        {
            return skillId + 110000000;
        }
        return skillId;
    }

    private void handlePassiveSkills(MapleCharacter chra)
    {
        if (GameConstants.is骑士团(chra.getJob()))
        {
            Skill bx = SkillFactory.getSkill(10000074);
            int bof = chra.getSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff = bx.getEffect(bof);
                this.percent_hp += eff.getX();
                this.percent_mp += eff.getX();
            }
        }
        Skill bx;
        int bof;
        switch (chra.getJob())
        {
            case 100:
            case 110:
            case 111:
            case 112:
            case 120:
            case 121:
            case 122:
            case 130:
            case 131:
            case 132:
                bx = SkillFactory.getSkill(1001003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(1000009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.addmaxhp += eff.getLevelToMaxHp() * chra.getLevel();
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getSpeedMax();
                }

                break;
            case 200:
            case 210:
            case 211:
            case 212:
            case 220:
            case 221:
            case 222:
            case 230:
            case 231:
            case 232:
                bx = SkillFactory.getSkill(2000006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_mp += eff.getPercentMP();
                    this.addmaxmp += eff.getLevelToMaxMp() * chra.getLevel();
                }

                break;
            case 300:
            case 310:
            case 311:
            case 312:
            case 320:
            case 321:
            case 322:
                bx = SkillFactory.getSkill(3000002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.defRange += eff.getRange();
                    this.accuracy += eff.getAcc();
                }

                break;
            case 400:
            case 410:
            case 411:
            case 412:
            case 420:
            case 421:
            case 422:
            case 431:
            case 432:
            case 433:
            case 434:
                bx = SkillFactory.getSkill(4000010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.ASR += eff.getASRRate();
                }
                bx = SkillFactory.getSkill(4001005);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.speed += eff.getSpeedMax();
                }
                bx = SkillFactory.getSkill(4000012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getER();
                }

                break;
            case 500:
            case 510:
            case 511:
            case 512:
            case 520:
            case 521:
            case 522:
                bx = SkillFactory.getSkill(5000000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.accuracy += eff.getAccX();
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getSpeedMax();
                }


                break;
        }
        bx = SkillFactory.getSkill(80000000);
        bof = chra.getSkillLevel(bx);
        if (bof > 0)
        {
            MapleStatEffect eff = bx.getEffect(bof);
            this.localstr += eff.getStrX();
            this.localdex += eff.getDexX();
            this.localint_ += eff.getIntX();
            this.localluk += eff.getLukX();
            this.percent_hp = ((int) (this.percent_hp + eff.getHpR()));
            this.percent_mp = ((int) (this.percent_mp + eff.getMpR()));
        }
        bx = SkillFactory.getSkill(80000001);
        bof = chra.getSkillLevel(bx);
        if (bof > 0)
        {
            this.percent_boss_damage_rate += bx.getEffect(bof).getBossDamage();
        }
        bx = SkillFactory.getSkill(80000002);
        bof = chra.getSkillLevel(bx);
        if (bof > 0)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + bx.getEffect(bof).getCritical()));
        }
        bx = SkillFactory.getSkill(80000005);
        bof = chra.getSkillLevel(bx);
        if (bof > 0)
        {
            this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
        }
        bx = SkillFactory.getSkill(80000006);
        bof = chra.getSkillLevel(bx);
        if (bof > 0)
        {
            this.percent_hp += bx.getEffect(bof).getPercentHP();
        }
        int[] skillIds = {80000007, 80000008, 80000009, 80000013, 80000014, 80000015, 80000017, 80000018, 80000019, 80000020, 80000021, 80000022, 80000026, 80000027, 80000028, 80000029, 80000030,
                80000031, 80000056, 80000057, 80000058, 80000063, 80000064, 80000065, 80000072, 80000073, 80000074, 80000077, 80000078, 80000079, 80000081, 80000082, 80000083, 80000098, 80000099, 80000100
                , 80000101, 80000102, 80000103, 80000111, 80000112, 80000113, 80000120, 80000121, 80000122};


        for (int i : skillIds)
        {
            bx = SkillFactory.getSkill(i);
            if (bx != null)
            {
                bof = chra.getSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    this.magic += eff.getMagicX();
                }
            }
        }
        bx = SkillFactory.getSkill(80000025);
        bof = chra.getSkillLevel(bx);
        if (bof > 0)
        {
            MapleStatEffect eff = bx.getEffect(bof);
            this.localstr += eff.getStrX();
            this.localdex += eff.getDexX();
            this.localint_ += eff.getIntX();
            this.localluk += eff.getLukX();
            this.percent_hp = ((int) (this.percent_hp + eff.getHpR()));
            this.percent_mp = ((int) (this.percent_mp + eff.getMpR()));
            this.jump += eff.getPassiveJump();
            this.speed += eff.getPassiveSpeed();
        }
        bx = SkillFactory.getSkill(80000050);
        bof = chra.getTotalSkillLevel(bx);
        if (bof > 0)
        {
            MapleStatEffect eff = bx.getEffect(bof);
            this.percent_damage += eff.getDAMRate();
        }
        bx = SkillFactory.getSkill(80001040);
        bof = chra.getSkillLevel(bx);
        if (bof > 0)
        {
        }


        bx = SkillFactory.getSkill(80000047);
        bof = chra.getTotalSkillLevel(bx);
        if (bof > 0)
        {
            MapleStatEffect eff = bx.getEffect(bof);
            this.localstr = ((int) (this.localstr + eff.getStrRate() / 100.0D * this.str));
            this.localdex = ((int) (this.localdex + eff.getDexRate() / 100.0D * this.dex));
            this.localluk = ((int) (this.localluk + eff.getLukRate() / 100.0D * this.luk));
            this.localint_ = ((int) (this.localint_ + eff.getIntRate() / 100.0D * this.int_));
        }
        if (GameConstants.is冒险家(chra.getJob()))
        {
            bx = SkillFactory.getSkill(74);
            bof = chra.getSkillLevel(bx);
            if (bof > 0)
            {
                this.levelBonus += bx.getEffect(bof).getX();
            }
            bx = SkillFactory.getSkill(80);
            bof = chra.getSkillLevel(bx);
            if (bof > 0)
            {
                this.levelBonus += bx.getEffect(bof).getX();
            }
            bx = SkillFactory.getSkill(10074);
            bof = chra.getSkillLevel(bx);
            if (bof > 0)
            {
                this.levelBonus += bx.getEffect(bof).getX();
            }
            bx = SkillFactory.getSkill(10080);
            bof = chra.getSkillLevel(bx);
            if (bof > 0)
            {
                this.levelBonus += bx.getEffect(bof).getX();
            }
            bx = SkillFactory.getSkill(110);
            bof = chra.getSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff = bx.getEffect(bof);
                this.localstr += eff.getStrX();
                this.localdex += eff.getDexX();
                this.localint_ += eff.getIntX();
                this.localluk += eff.getLukX();
                this.percent_hp = ((int) (this.percent_hp + eff.getHpR()));
                this.percent_mp = ((int) (this.percent_mp + eff.getMpR()));
            }
            bx = SkillFactory.getSkill(10110);
            bof = chra.getSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff = bx.getEffect(bof);
                this.localstr += eff.getStrX();
                this.localdex += eff.getDexX();
                this.localint_ += eff.getIntX();
                this.localluk += eff.getLukX();
                this.percent_hp = ((int) (this.percent_hp + eff.getHpR()));
                this.percent_mp = ((int) (this.percent_mp + eff.getMpR()));
            }
        }


        Skill skillBof = SkillFactory.getSkill(GameConstants.getBOF_ForJob(chra.getJob()));
        Skill skillEmpress = SkillFactory.getSkill(GameConstants.getEmpress_ForJob(chra.getJob()));
        if ((chra.getSkillLevel(skillBof) > 0) || (chra.getSkillLevel(skillEmpress) > 0))
        {
            if (chra.getSkillLevel(skillBof) > chra.getSkillLevel(skillEmpress))
            {
                MapleStatEffect eff = skillBof.getEffect(chra.getSkillLevel(skillBof));
                this.watk += eff.getX();
                this.magic += eff.getY();
                this.accuracy += eff.getX();
            }
            else
            {
                MapleStatEffect eff = skillEmpress.getEffect(chra.getSkillLevel(skillEmpress));
                this.watk += eff.getX();
                this.magic += eff.getY();
                this.accuracy += eff.getX();
            }
        }
        switch (chra.getJob())
        {
            case 110:
            case 111:
            case 112:
                bx = SkillFactory.getSkill(1100009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(1110011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.ASR += bx.getEffect(bof).getASRRate();
                    this.TER += bx.getEffect(bof).getTERRate();
                }
                bx = SkillFactory.getSkill(1110009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage_rate += eff.getDamage();
                    this.percent_boss_damage_rate += eff.getDamage();
                }
                bx = SkillFactory.getSkill(1120012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }
                bx = SkillFactory.getSkill(1120013);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }


                bx = SkillFactory.getSkill(1120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    MapleStatEffect 斗气效果 = chra.getStatForBuff(MapleBuffStat.斗气集中);
                    Integer 斗气状态 = chra.getBuffedValue(MapleBuffStat.斗气集中);
                    if ((斗气效果 != null) && (斗气状态 != null))
                    {
                        this.percent_damage_rate += eff.getDAMRate() * (斗气状态 - 1);
                        this.percent_boss_damage_rate += eff.getDAMRate() * (斗气状态 - 1);
                    }
                }
                bx = SkillFactory.getSkill(1120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addSkillProp(1101013, bx.getEffect(bof).getProb());
                    addSkillProp(1120003, bx.getEffect(bof).getProb());
                }
                bx = SkillFactory.getSkill(1120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    MapleStatEffect 斗气效果 = chra.getStatForBuff(MapleBuffStat.斗气集中);
                    Integer 斗气状态 = chra.getBuffedValue(MapleBuffStat.斗气集中);
                    if ((斗气效果 != null) && (斗气状态 != null))
                    {
                        this.percent_boss_damage_rate += eff.getW() * (斗气状态 - 1);
                    }
                }
                bx = SkillFactory.getSkill(1120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_acc += bx.getEffect(bof).getArRate();
                }
                bx = SkillFactory.getSkill(1120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }
                bx = SkillFactory.getSkill(1120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(1121008, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(1120017, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(1120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(1121008, bx.getEffect(bof).getTargetPlus());
                    addTargetPlus(1120017, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(1120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(1121008, bx.getEffect(bof).getAttackCount());
                    addAttackCount(1120017, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 120:
            case 121:
            case 122:
                bx = SkillFactory.getSkill(1200009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(1210001);
                bof = chra.getTotalSkillLevel(bx);
                Item shield = chra.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
                if ((bof > 0) && (shield != null))
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getX();
                    this.percent_mdef += eff.getX();
                    this.dodgeChance += eff.getER();
                }
                bx = SkillFactory.getSkill(1210015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.reduceDamageRate += bx.getEffect(bof).getT();
                }
                bx = SkillFactory.getSkill(1221016);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.ASR += bx.getEffect(bof).getASRRate();
                }
                bx = SkillFactory.getSkill(1220010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addAttackCount(1201011, eff.getAttackCount());
                    addAttackCount(1201012, eff.getAttackCount());
                    addAttackCount(1211008, eff.getAttackCount());
                    addAttackCount(1221004, eff.getAttackCount());
                    addAttackCount(1221009, eff.getAttackCount());

                    addTargetPlus(1201011, eff.getTargetPlus());
                    addTargetPlus(1201012, eff.getTargetPlus());
                    addTargetPlus(1211008, eff.getTargetPlus());
                    addTargetPlus(1221004, eff.getTargetPlus());
                    addTargetPlus(1221009, eff.getTargetPlus());
                }


                bx = SkillFactory.getSkill(1220043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(1211013, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(1220044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addSkillProp(1211013, bx.getEffect(bof).getProb());
                }
                bx = SkillFactory.getSkill(1220046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(1221009, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(1220048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(1221009, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(1220049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(1221011, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(1220050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(1221011, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(1220051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(1221011, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 130:
            case 131:
            case 132:
                bx = SkillFactory.getSkill(1300009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(1310010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.ASR += bx.getEffect(bof).getASRRate();
                    this.TER += bx.getEffect(bof).getTERRate();
                }
                bx = SkillFactory.getSkill(1310009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                    this.hpRecoverProp += eff.getProb();
                    this.hpRecoverPercent += eff.getX();
                }
                bx = SkillFactory.getSkill(1321015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }


                bx = SkillFactory.getSkill(1320043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(1301007, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(1320046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage_rate += eff.getDamage();
                    this.percent_boss_damage_rate += eff.getDamage();
                }
                bx = SkillFactory.getSkill(1320047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                }
                bx = SkillFactory.getSkill(1320048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                }
                bx = SkillFactory.getSkill(1320049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(1321012, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(1320050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(1321012, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(1320051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(1321012, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 210:
            case 211:
            case 212:
                bx = SkillFactory.getSkill(2100007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(2110000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.dotTime += eff.getX();
                    this.dot += eff.getZ();
                }
                bx = SkillFactory.getSkill(2110001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.mpconPercent += eff.getCostMpRate();
                    this.percent_damage += eff.getDAMRate();
                }
                bx = SkillFactory.getSkill(2121003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(2111003, eff.getX());
                }
                bx = SkillFactory.getSkill(2120012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.magic += eff.getMagicX();
                    this.BuffUP_Skill += eff.getBuffTimeRate();
                }
                bx = SkillFactory.getSkill(2120010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage_rate += eff.getX() * eff.getY();
                    this.percent_boss_damage_rate += eff.getX() * eff.getY();
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                }


                bx = SkillFactory.getSkill(2120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(2121006, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(2120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(2121003, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(2120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(2121003, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(2120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(2121003, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 220:
            case 221:
            case 222:
                bx = SkillFactory.getSkill(2200007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(2210000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dot += bx.getEffect(bof).getZ();
                }
                bx = SkillFactory.getSkill(2210001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.mpconPercent += eff.getCostMpRate();
                    this.percent_damage += eff.getDAMRate();
                }
                bx = SkillFactory.getSkill(2220013);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.magic += eff.getMagicX();
                    this.BuffUP_Skill += eff.getBuffTimeRate();
                }
                bx = SkillFactory.getSkill(2220010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage_rate += eff.getX() * eff.getY();
                    this.percent_boss_damage_rate += eff.getX() * eff.getY();
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                }


                bx = SkillFactory.getSkill(2220043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(2211007, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(2220044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(2211007, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(2220046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(2221006, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(2220047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(2221006, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(2220048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(2221006, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(2220049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(2211010, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(2220050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(2211010, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(2220051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(2211010, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 230:
            case 231:
            case 232:
                bx = SkillFactory.getSkill(2300007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(2320012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.magic += eff.getMagicX();
                    this.BuffUP_Skill += eff.getBuffTimeRate();
                }
                bx = SkillFactory.getSkill(2320011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage_rate += eff.getX() * eff.getY();
                    this.percent_boss_damage_rate += eff.getX() * eff.getY();
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                }


                bx = SkillFactory.getSkill(2320044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(2311009, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(2320045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(2311009, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 310:
            case 311:
            case 312:
                bx = SkillFactory.getSkill(3100006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(3110012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }
                bx = SkillFactory.getSkill(3110014);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    this.percent_acc += eff.getArRate();
                    this.percent_damage += eff.getDAMRate();
                }
                bx = SkillFactory.getSkill(3111005);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getWDEFRate();
                    this.percent_mdef += eff.getMDEFRate();
                }
                bx = SkillFactory.getSkill(3111010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(3110007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getER();
                }
                bx = SkillFactory.getSkill(3120008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    addDamageIncrease(3100001, eff.getDamage());
                }


                bx = SkillFactory.getSkill(3120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(3121002, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(3120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                }


                bx = SkillFactory.getSkill(3120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                }


                bx = SkillFactory.getSkill(3120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(3121015, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(3120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(3121015, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(3120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(3121015, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(3120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(3121013, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(3120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                }


                bx = SkillFactory.getSkill(3120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(3121013, bx.getEffect(bof).getTargetPlus());
                }

                break;
            case 320:
            case 321:
            case 322:
                bx = SkillFactory.getSkill(3200006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(3210007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getER();
                }
                bx = SkillFactory.getSkill(3210015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    this.percent_acc += eff.getArRate();
                    this.percent_damage += eff.getDAMRate();
                }
                bx = SkillFactory.getSkill(3211005);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getWDEFRate();
                    this.percent_mdef += eff.getMDEFRate();
                }
                bx = SkillFactory.getSkill(3211010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(3211011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }


                bx = SkillFactory.getSkill(3220043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(3221002, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(3220044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                }


                bx = SkillFactory.getSkill(3220045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                }


                bx = SkillFactory.getSkill(3220046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(3221017, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(3220047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(3221017, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(3220048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(3221017, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(3220049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(3221007, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(3220050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                }


                bx = SkillFactory.getSkill(3220051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(3221007, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 410:
            case 411:
            case 412:
                bx = SkillFactory.getSkill(4100007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localluk += eff.getLukX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(4110008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }
                bx = SkillFactory.getSkill(4110012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage += eff.getPercentDamageRate();
                }
                bx = SkillFactory.getSkill(4110014);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.RecoveryUP += eff.getX() - 100;
                    this.BuffUP += eff.getY() - 100;
                }
                bx = SkillFactory.getSkill(4121014);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                }


                bx = SkillFactory.getSkill(4120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4111015, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(4111015, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(4120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(4111015, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(4120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(4121015, bx.getEffect(bof).getCooltimeReduceR());
                }
                bx = SkillFactory.getSkill(4120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4121013, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_boss_damage_rate += bx.getEffect(bof).getBossDamage();
                }
                bx = SkillFactory.getSkill(4120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(4121013, bx.getEffect(bof).getBulletCount());
                }

                break;
            case 420:
            case 421:
            case 422:
                bx = SkillFactory.getSkill(4200007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localluk += eff.getLukX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(4210013);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }
                bx = SkillFactory.getSkill(4200010);
                bof = chra.getTotalSkillLevel(bx);
                Item shield1 = chra.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
                if ((bof > 0) && (shield1 != null))
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getX();
                    this.percent_mdef += eff.getX();
                    this.dodgeChance += eff.getER();
                }
                bx = SkillFactory.getSkill(4210012);
                bof = chra.getTotalSkillLevel(bx);
                if ((bof > 0) && (bx != null))
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.mesoBuff *= (eff.getMesoRate() + 100.0D) / 100.0D;
                    this.pickRate += eff.getU();
                    this.mesoGuard -= eff.getV();
                    this.mesoGuardMeso -= eff.getW();
                    addDamageIncrease(4211006, eff.getX());
                }
                bx = SkillFactory.getSkill(4221013);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }
                bx = SkillFactory.getSkill(4221007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(4201012, eff.getDAMRate());
                    addDamageIncrease(4201004, eff.getDAMRate());
                    addDamageIncrease(4211002, eff.getDAMRate());
                    addDamageIncrease(4211011, eff.getDAMRate());
                }


                bx = SkillFactory.getSkill(4220043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4211006, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4220044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(4211006, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(4220046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4221007, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4220047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(4221007, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(4220048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(4221007, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(4220049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4221014, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4220050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(4221014, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 431:
            case 432:
            case 433:
            case 434:
                bx = SkillFactory.getSkill(4310006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localluk += eff.getLukX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(4330007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.hpRecoverProp += eff.getProb();
                    this.hpRecoverPercent += eff.getX();
                }
                bx = SkillFactory.getSkill(4330008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.ASR += eff.getASRRate();
                }
                bx = SkillFactory.getSkill(4330009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getER();
                }
                bx = SkillFactory.getSkill(4341006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getWDEFRate();
                    this.percent_mdef += eff.getMDEFRate();
                    this.dodgeChance += eff.getER();
                }
                bx = SkillFactory.getSkill(4341002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(4311002, eff.getDAMRate());
                    addDamageIncrease(4311003, eff.getDAMRate());
                    addDamageIncrease(4301004, eff.getDAMRate());
                    addDamageIncrease(4331000, eff.getDAMRate());
                    addDamageIncrease(4321004, eff.getDAMRate());
                    addDamageIncrease(4321006, eff.getDAMRate());
                }


                bx = SkillFactory.getSkill(4340043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4331000, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4340044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(4331000, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(4340045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(4331000, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(4340046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4341009, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4340047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(4341009, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(4340048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(4341009, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(4340049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(4341011, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(4340051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(4341011, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 510:
            case 511:
            case 512:
                bx = SkillFactory.getSkill(5100009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(5100010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(5121015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.ASR += eff.getASRRate();
                }
                bx = SkillFactory.getSkill(5120014);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getX();
                }
                double energyrate = (chra.getBuffedValue(MapleBuffStat.能量获得) != null) && (chra.getSpecialStat().isEnergyfull()) ? 1.0D : 0.5D;
                if (chra.getTotalSkillLevel(5120018) > 0)
                {
                    bx = SkillFactory.getSkill(5120018);
                    bof = chra.getTotalSkillLevel(bx);
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk = ((int) (this.watk + eff.getWatk() * energyrate));
                    this.wdef = ((int) (this.wdef + eff.getEnhancedWdef() * energyrate));
                    this.mdef = ((int) (this.mdef + eff.getEnhancedMdef() * energyrate));
                    this.speed = ((int) (this.speed + eff.getSpeed() * energyrate));
                    this.accuracy = ((int) (this.accuracy + eff.getAcc() * energyrate));
                }
                else if (chra.getTotalSkillLevel(5110014) > 0)
                {
                    bx = SkillFactory.getSkill(5110014);
                    bof = chra.getTotalSkillLevel(bx);
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk = ((int) (this.watk + eff.getWatk() * energyrate));
                    this.wdef = ((int) (this.wdef + eff.getEnhancedWdef() * energyrate));
                    this.mdef = ((int) (this.mdef + eff.getEnhancedMdef() * energyrate));
                    this.speed = ((int) (this.speed + eff.getSpeed() * energyrate));
                    this.accuracy = ((int) (this.accuracy + eff.getAcc() * energyrate));
                }
                else if (chra.getTotalSkillLevel(5100015) > 0)
                {
                    bx = SkillFactory.getSkill(5100015);
                    bof = chra.getTotalSkillLevel(bx);
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.wdef = ((int) (this.wdef + eff.getEnhancedWdef() * energyrate));
                    this.mdef = ((int) (this.mdef + eff.getEnhancedMdef() * energyrate));
                    this.speed = ((int) (this.speed + eff.getSpeed() * energyrate));
                    this.accuracy = ((int) (this.accuracy + eff.getAcc() * energyrate));
                }


                bx = SkillFactory.getSkill(5120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5121007, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(5121020, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBossDamageRate(5121007, bx.getEffect(bof).getBossDamage());
                    addBossDamageRate(5121020, bx.getEffect(bof).getBossDamage());
                }
                bx = SkillFactory.getSkill(5120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(5121007, bx.getEffect(bof).getAttackCount());
                    addAttackCount(5121020, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(5120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5121016, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(5121017, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(5121016, bx.getEffect(bof).getTargetPlus());
                    addTargetPlus(5121017, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(5120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(5121016, bx.getEffect(bof).getAttackCount());
                    addAttackCount(5121017, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 520:
            case 521:
            case 522:
                bx = SkillFactory.getSkill(5200009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }


                bx = SkillFactory.getSkill(5220043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5211008, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5220044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(5211008, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(5220045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(5211008, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(5220046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5221017, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5220047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(5221017, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(5220048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(5221017, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(5220049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5221004, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5220051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_boss_damage_rate += bx.getEffect(bof).getBossDamage();
                }

                break;
            case 501:
            case 530:
            case 531:
            case 532:
                bx = SkillFactory.getSkill(5010003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }
                bx = SkillFactory.getSkill(5300008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(5311001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5301001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5310007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.ASR += eff.getASRRate();
                    this.percent_wdef += eff.getWDEFRate();
                }
                bx = SkillFactory.getSkill(5310006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }
                bx = SkillFactory.getSkill(5320009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage_rate += eff.getDAMRate();
                    this.percent_boss_damage_rate += eff.getDAMRate();
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                }


                bx = SkillFactory.getSkill(5320043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(5321004, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(5320044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(5321004, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(5320046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5321000, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5320047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(5321000, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(5320048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(5321000, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(5320049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(5321012, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(5320051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(5321012, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 508:
            case 570:
            case 571:
            case 572:
                bx = SkillFactory.getSkill(5080000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.accuracy += eff.getAccX();
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getSpeedMax();
                }
                bx = SkillFactory.getSkill(5700003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }


                break;
            case 1100:
            case 1110:
            case 1111:
            case 1112:
                bx = SkillFactory.getSkill(11000023);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.percent_wdef += eff.getWdefX();
                    this.percent_mdef += eff.getMdefX();
                    this.speed += eff.getPassiveSpeed();
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getSpeedMax();
                }
                bx = SkillFactory.getSkill(11001022);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }
                bx = SkillFactory.getSkill(11100026);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(11110025);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }
                bx = SkillFactory.getSkill(11110026);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    this.localstr += eff.getStrX();
                }
                bx = SkillFactory.getSkill(11120008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }


                bx = SkillFactory.getSkill(11120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(11121103, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(11121203, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(11120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(11121103, bx.getEffect(bof).getTargetPlus());
                    addTargetPlus(11121203, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(11120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(11121103, bx.getEffect(bof).getAttackCount());
                    addAttackCount(11121203, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(11120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(11121101, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(11121102, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(11121201, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(11121202, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(11120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(11121101, bx.getEffect(bof).getIgnoreMob());
                    addIgnoreMobpdpRate(11121102, bx.getEffect(bof).getIgnoreMob());
                    addIgnoreMobpdpRate(11121201, bx.getEffect(bof).getIgnoreMob());
                    addIgnoreMobpdpRate(11121202, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(11120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_boss_damage_rate += bx.getEffect(bof).getBossDamage();
                }

                break;
            case 1200:
            case 1210:
            case 1211:
            case 1212:
                bx = SkillFactory.getSkill(12000005);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_mp += bx.getEffect(bof).getPercentMP();
                }
                bx = SkillFactory.getSkill(12100008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(12110001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.mpconPercent += eff.getX() - 100;
                    this.percent_damage_rate += eff.getY();
                    this.percent_boss_damage_rate += eff.getY();
                }


                break;
            case 1300:
            case 1310:
            case 1311:
            case 1312:
                bx = SkillFactory.getSkill(13001021);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(13000023);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    this.accuracy += eff.getAccX();

                    this.defRange += eff.getRange();
                }
                bx = SkillFactory.getSkill(13100026);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(13110025);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);

                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }
                bx = SkillFactory.getSkill(13110026);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getWdefX();
                    this.percent_mdef += eff.getMdefX();
                    this.dodgeChance += eff.getER();
                }


                bx = SkillFactory.getSkill(13120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(13100022, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(13100027, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(13110022, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(13110027, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(13120003, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(13120010, bx.getEffect(bof).getDAMRate());
                }


                bx = SkillFactory.getSkill(13120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(13121002, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(13121009, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(13120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(13121002, bx.getEffect(bof).getTargetPlus());
                    addTargetPlus(13121009, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(13120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(13121002, bx.getEffect(bof).getAttackCount());
                    addAttackCount(13121009, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(13120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(13121001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(13120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(13121001, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(13120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_boss_damage_rate += bx.getEffect(bof).getBossDamage();
                }

                break;
            case 1400:
            case 1410:
            case 1411:
            case 1412:
                bx = SkillFactory.getSkill(14110009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.ASR += eff.getASRRate();
                }
                bx = SkillFactory.getSkill(14100010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localluk += eff.getLukX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(14110011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.RecoveryUP += bx.getEffect(bof).getX() - 100;
                }


                break;
            case 1500:
            case 1510:
            case 1511:
            case 1512:
                bx = SkillFactory.getSkill(15000023);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.speed += eff.getPassiveSpeed();
                    this.jump += eff.getPassiveJump();
                    this.raidenCount += eff.getV();
                    this.raidenPorp += eff.getProb();
                }
                bx = SkillFactory.getSkill(15001022);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.raidenCount += eff.getV();
                    this.raidenPorp += eff.getProb();
                }
                bx = SkillFactory.getSkill(15100024);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                }
                bx = SkillFactory.getSkill(15100025);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    this.raidenCount += eff.getV();
                    this.raidenPorp += eff.getProb();
                }
                bx = SkillFactory.getSkill(15110026);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    this.raidenCount += eff.getV();
                    this.raidenPorp += eff.getProb();
                }
                bx = SkillFactory.getSkill(15120007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);

                    this.percent_hp += eff.getPercentHP();
                    this.ignore_mob_damage_rate += eff.getIgnoreMobDamR();
                }
                bx = SkillFactory.getSkill(15120008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.raidenCount += eff.getV();
                    this.raidenPorp += eff.getProb();
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                }


                bx = SkillFactory.getSkill(15120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(15111022, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(15120003, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(15120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(15111022, bx.getEffect(bof).getTargetPlus());
                    addTargetPlus(15120003, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(15120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(15111022, bx.getEffect(bof).getAttackCount());
                    addAttackCount(15120003, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(15120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(15121002, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(15120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(15121002, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(15120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(15121002, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(15120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(15121001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(15120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(15121001, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(15120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_boss_damage_rate += bx.getEffect(bof).getBossDamage();
                }

                break;
            case 2110:
            case 2111:
            case 2112:
                bx = SkillFactory.getSkill(21100008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(21101006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_damage += bx.getEffect(bof).getDAMRate();
                }
                bx = SkillFactory.getSkill(21110000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMax()));
                    this.ASR += eff.getASRRate();
                }
                bx = SkillFactory.getSkill(21110002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(21000004, eff.getDAMRate());
                    addDamageIncrease(21110007, eff.getW());
                    addDamageIncrease(21110008, eff.getY());
                }
                bx = SkillFactory.getSkill(21110010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    this.percent_boss_damage_rate += eff.getBossDamage();
                }
                bx = SkillFactory.getSkill(21120002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(21100007, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(21120004);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(21120012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    addDamageIncrease(21100010, eff.getDamage());
                }
                bx = SkillFactory.getSkill(21120011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(21101011, eff.getDAMRate());
                    addDamageIncrease(21111013, eff.getDAMRate());
                }


                bx = SkillFactory.getSkill(21120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(21111009, bx.getEffect(bof).getCooltimeReduceR());
                }
                bx = SkillFactory.getSkill(21120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(21121013, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(21120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(21121013, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(21120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(21121013, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(21120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(21120006, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(21120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(21120006, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(21120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(21120006, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(21110011, bx.getEffect(bof).getDAMRate());
                }

                break;
            case 2200:
            case 2210:
            case 2211:
            case 2212:
            case 2213:
            case 2214:
            case 2215:
            case 2216:
            case 2217:
            case 2218:
                this.magic += chra.getTotalSkillLevel(SkillFactory.getSkill(22000000));
                bx = SkillFactory.getSkill(22120001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(22131001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(22150000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.mpconPercent += eff.getX() - 100;
                    this.percent_damage_rate += eff.getY();
                    this.percent_boss_damage_rate += eff.getY();
                }
                bx = SkillFactory.getSkill(22160000);
                bof = chra.getTotalSkillLevel(bx);
                if ((bof > 0) && (bx != null))
                {
                    this.percent_damage += bx.getEffect(bof).getDamage();
                }
                bx = SkillFactory.getSkill(22170001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.magic += eff.getX();
                    this.trueMastery += eff.getMastery();
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                }


                bx = SkillFactory.getSkill(22170043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(22171002, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(22170045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(22171002, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(22170046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(22181002, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(22170047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(22181002, bx.getEffect(bof).getCooltimeReduceR());
                }
                bx = SkillFactory.getSkill(22170048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(22181002, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(22170049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(22181001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(22170050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(22181001, bx.getEffect(bof).getIgnoreMob());
                }

                break;
            case 2001:
            case 2300:
            case 2310:
            case 2311:
            case 2312:
                bx = SkillFactory.getSkill(20021110);
                bof = chra.getSkillLevel(bx);
                if (bof > 0)
                {
                }


                bx = SkillFactory.getSkill(20020112);
                bof = chra.getSkillLevel(bx);
                if (bof > 0)
                {
                    chra.getTrait(MapleTraitType.charm).addLocalExp(GameConstants.getTraitExpNeededForLevel(30));
                }
                bx = SkillFactory.getSkill(23000001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getER();
                }
                bx = SkillFactory.getSkill(23100008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(23100004);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getProb();
                }
                bx = SkillFactory.getSkill(23110006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23101001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(23121000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23111000, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(23121002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23111001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(23121004);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getProb();
                }
                bx = SkillFactory.getSkill(23120009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getX();
                    this.trueMastery += eff.getMastery();
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                }
                bx = SkillFactory.getSkill(23120010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getX();
                }
                bx = SkillFactory.getSkill(23120011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23101001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(23120012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    addDamageIncrease(23100005, eff.getDamage());
                }


                bx = SkillFactory.getSkill(23120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23121000, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(23120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23111000, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(23120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23121002, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(23120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(23111001, bx.getEffect(bof).getDAMRate());
                }

                break;
            case 2400:
            case 2410:
            case 2411:
            case 2412:
                bx = SkillFactory.getSkill(20030206);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localdex += eff.getDexX();
                    this.dodgeChance += eff.getER();
                    chra.getTrait(MapleTraitType.insight).addLocalExp(GameConstants.getTraitExpNeededForLevel(eff.getER()));
                    chra.getTrait(MapleTraitType.craft).addLocalExp(GameConstants.getTraitExpNeededForLevel(eff.getER()));
                }
                bx = SkillFactory.getSkill(24001002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.speed += eff.getPassiveSpeed();
                    this.jump += eff.getPassiveJump();
                }
                bx = SkillFactory.getSkill(24000003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.dodgeChance += eff.getX();
                }
                bx = SkillFactory.getSkill(24100006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localluk += eff.getLukX();
                }
                bx = SkillFactory.getSkill(24111002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localluk += eff.getLukX();
                }
                bx = SkillFactory.getSkill(24111006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(24101002, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(24120002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.dodgeChance += eff.getX();
                }
                bx = SkillFactory.getSkill(24121003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(24111006, eff.getDAMRate());
                    addDamageIncrease(24111008, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(24120006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    this.trueMastery += eff.getMastery();
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                }


                bx = SkillFactory.getSkill(24120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(24121005, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(24120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(24121005, bx.getEffect(bof).getCooltimeReduceR());
                }
                bx = SkillFactory.getSkill(24120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(24121005, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(24120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(24121000, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(24120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(24121000, bx.getEffect(bof).getTargetPlus());
                }

                break;
            case 2004:
            case 2700:
            case 2710:
            case 2711:
            case 2712:
                bx = SkillFactory.getSkill(20040218);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }
                bx = SkillFactory.getSkill(20040221);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localint_ += eff.getIntX();
                    chra.getTrait(MapleTraitType.will).addLocalExp(GameConstants.getTraitExpNeededForLevel(eff.getIntX()));
                    chra.getTrait(MapleTraitType.insight).addLocalExp(GameConstants.getTraitExpNeededForLevel(eff.getIntX()));
                }
                bx = SkillFactory.getSkill(27000003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getWdefX();
                    this.percent_mdef += eff.getMdefX();
                }
                bx = SkillFactory.getSkill(27001002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(27000207);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(27001100, eff.getMdRate());
                    addDamageIncrease(27101100, eff.getMdRate());
                    addDamageIncrease(27101101, eff.getMdRate());
                    addDamageIncrease(27111100, eff.getMdRate());
                    addDamageIncrease(27111101, eff.getMdRate());
                    addDamageIncrease(27111303, eff.getMdRate());
                    addDamageIncrease(27121100, eff.getMdRate());
                    addDamageIncrease(27120211, eff.getMdRate());
                    addDamageIncrease(27001201, eff.getMdRate());
                    addDamageIncrease(27101202, eff.getMdRate());
                    addDamageIncrease(27111202, eff.getMdRate());
                    addDamageIncrease(27111303, eff.getMdRate());
                    addDamageIncrease(27121202, eff.getMdRate());
                    addDamageIncrease(27121303, eff.getMdRate());
                }
                bx = SkillFactory.getSkill(27100006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(27111004);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.ASR += bx.getEffect(bof).getASRRate();
                    this.TER += bx.getEffect(bof).getTERRate();
                }
                bx = SkillFactory.getSkill(27120008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(27111303, eff.getDAMRate());
                }


                bx = SkillFactory.getSkill(27120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(27121100, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(27120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(27121202, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(27120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(27121202, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(27120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(27121303, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(27120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(27121303, bx.getEffect(bof).getTargetPlus());
                }

                break;
            case 3001:
            case 3100:
            case 3101:
            case 3110:
            case 3111:
            case 3112:
            case 3120:
            case 3121:
            case 3122:
                this.mpRecoverProp = 100;
                bx = SkillFactory.getSkill(30010112);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_boss_damage_rate += eff.getBossDamage();
                    this.mpRecover += eff.getX();
                    this.mpRecoverProp += eff.getBossDamage();
                }
                bx = SkillFactory.getSkill(30010185);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    chra.getTrait(MapleTraitType.will).addLocalExp(GameConstants.getTraitExpNeededForLevel(eff.getY()));
                    chra.getTrait(MapleTraitType.charisma).addLocalExp(GameConstants.getTraitExpNeededForLevel(eff.getZ()));
                }
                bx = SkillFactory.getSkill(30010241);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage += eff.getDAMRate();
                }

                if (GameConstants.is恶魔复仇者(chra.getJob()))
                {
                    bx = SkillFactory.getSkill(31010002);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.hpRecoverProp += eff.getProb();
                        this.hpRecoverPercent += eff.getX();
                    }
                    bx = SkillFactory.getSkill(31010003);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.jump += eff.getPassiveJump();
                        this.speed += eff.getPassiveSpeed();
                    }
                    bx = SkillFactory.getSkill(31200004);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_wdef += eff.getWDEFRate();
                        this.percent_mdef += eff.getMDEFRate();
                    }
                    bx = SkillFactory.getSkill(31200005);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.accuracy += eff.getAccX();
                    }
                    bx = SkillFactory.getSkill(31200006);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.localstr += eff.getStrX();
                        this.wdef += eff.getWdefX();
                        this.mdef += eff.getMdefX();
                    }
                    bx = SkillFactory.getSkill(31210005);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        addDamageIncrease(31011000, eff.getDAMRate());
                        addDamageIncrease(31011004, eff.getDAMRate());
                        addDamageIncrease(31011005, eff.getDAMRate());
                        addDamageIncrease(31011006, eff.getDAMRate());
                        addDamageIncrease(31201000, eff.getDAMRate());
                        addDamageIncrease(31201007, eff.getDAMRate());
                        addDamageIncrease(31201008, eff.getDAMRate());
                        addDamageIncrease(31201009, eff.getDAMRate());
                        addDamageIncrease(31201010, eff.getDAMRate());
                        addDamageIncrease(31211000, eff.getDAMRate());
                        addDamageIncrease(31211007, eff.getDAMRate());
                        addDamageIncrease(31211008, eff.getDAMRate());
                        addDamageIncrease(31211009, eff.getDAMRate());
                        addDamageIncrease(31211010, eff.getDAMRate());
                        addDamageIncrease(31221000, eff.getDAMRate());
                        addDamageIncrease(31221009, eff.getDAMRate());
                        addDamageIncrease(31221010, eff.getDAMRate());
                        addDamageIncrease(31221011, eff.getDAMRate());
                        addDamageIncrease(31221012, eff.getDAMRate());
                    }
                    bx = SkillFactory.getSkill(31210006);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.hpRecoverPercent += eff.getX();
                    }
                    bx = SkillFactory.getSkill(31220005);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                        addDamageIncrease(31211002, eff.getX());
                        addDamageIncrease(31211011, eff.getX());
                        addDamageIncrease(31221001, eff.getX());
                        addDamageIncrease(31221014, eff.getX());
                    }

                }
                else
                {
                    bx = SkillFactory.getSkill(31000003);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        this.percent_hp += bx.getEffect(bof).getPercentHP();
                    }
                    bx = SkillFactory.getSkill(31100007);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        addDamageIncrease(31000004, eff.getDAMRate());
                        addDamageIncrease(31001006, eff.getDAMRate());
                        addDamageIncrease(31001007, eff.getDAMRate());
                        addDamageIncrease(31001008, eff.getDAMRate());
                    }
                    bx = SkillFactory.getSkill(31100005);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.localstr += eff.getStrX();
                        this.localdex += eff.getDexX();
                    }
                    bx = SkillFactory.getSkill(31110010);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        addDamageIncrease(31000004, eff.getX());
                        addDamageIncrease(31001006, eff.getX());
                        addDamageIncrease(31001007, eff.getX());
                        addDamageIncrease(31001008, eff.getX());
                    }
                    bx = SkillFactory.getSkill(31110008);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.dodgeChance += eff.getX();
                        this.hpRecoverPercent += eff.getY();
                        this.hpRecoverProp += eff.getX();
                        this.mpRecover += eff.getY();
                        this.mpRecoverProp += eff.getX();
                    }
                    bx = SkillFactory.getSkill(31110009);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.mpRecover += 1;
                        this.mpRecoverProp += eff.getProb();
                    }
                    bx = SkillFactory.getSkill(31110006);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_damage_rate += eff.getX();
                        this.percent_boss_damage_rate += eff.getX();
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getY()));
                    }
                    bx = SkillFactory.getSkill(31121006);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                    }
                    bx = SkillFactory.getSkill(31110007);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        this.percent_damage += bx.getEffect(bof).getDAMRate();
                    }
                    bx = SkillFactory.getSkill(31120011);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        addDamageIncrease(31000004, eff.getX());
                        addDamageIncrease(31001006, eff.getX());
                        addDamageIncrease(31001007, eff.getX());
                        addDamageIncrease(31001008, eff.getX());
                    }
                    bx = SkillFactory.getSkill(31120008);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.watk += eff.getAttackX();
                        this.trueMastery += eff.getMastery();
                        this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                    }
                    bx = SkillFactory.getSkill(31120009);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        this.percent_wdef += bx.getEffect(bof).getT();
                    }
                    bx = SkillFactory.getSkill(30010111);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.hpRecoverPercent += eff.getX();
                        this.hpRecoverProp += eff.getProb();
                    }


                    bx = SkillFactory.getSkill(31120043);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        addDamageIncrease(31111005, bx.getEffect(bof).getDAMRate());
                    }
                    bx = SkillFactory.getSkill(31120044);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        addAttackCount(31111005, bx.getEffect(bof).getAttackCount());
                    }
                    bx = SkillFactory.getSkill(31120047);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        addDamageIncrease(31121005, bx.getEffect(bof).getDAMRate());
                    }
                    bx = SkillFactory.getSkill(31120049);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        addDamageIncrease(31121001, bx.getEffect(bof).getDAMRate());
                    }
                    bx = SkillFactory.getSkill(31120050);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        addAttackCount(31121001, bx.getEffect(bof).getAttackCount());
                    }
                }

                break;
            case 3200:
            case 3210:
            case 3211:
            case 3212:
                bx = SkillFactory.getSkill(32000015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.magic += eff.getMagicX();
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(32100007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(32100008);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(32110000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.ASR += bx.getEffect(bof).getASRRate();
                    this.TER += bx.getEffect(bof).getTERRate();
                }
                bx = SkillFactory.getSkill(32110001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage += eff.getDAMRate();
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                }
                bx = SkillFactory.getSkill(32111015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(32101001, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(32121004);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(32111003, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(32120000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.magic += bx.getEffect(bof).getMagicX();
                }
                bx = SkillFactory.getSkill(32120001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.dodgeChance += bx.getEffect(bof).getER();
                }
                bx = SkillFactory.getSkill(32121010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.percent_mp += eff.getPercentMP();
                    this.percent_wdef += eff.getWDEFRate();
                    this.percent_mdef += eff.getMDEFRate();
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                }


                bx = SkillFactory.getSkill(32120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(32111010, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(32120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(32111003, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(32120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(32111003, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(32120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(32111003, bx.getEffect(bof).getCooltimeReduceR());
                }
                bx = SkillFactory.getSkill(32120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(32121003, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(32120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(32121003, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(32120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(32121003, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 3300:
            case 3310:
            case 3311:
            case 3312:
                bx = SkillFactory.getSkill(33000005);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    this.accuracy += eff.getAccX();
                    this.percent_acc += eff.getPercentAcc();
                    this.defRange += eff.getRange();
                }
                bx = SkillFactory.getSkill(33100010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(33120000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getX();
                    this.trueMastery += eff.getMastery();
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                }
                bx = SkillFactory.getSkill(33110000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_damage_rate += eff.getDamage();
                    this.percent_boss_damage_rate += eff.getDamage();
                }
                bx = SkillFactory.getSkill(33120010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    this.dodgeChance += eff.getER();
                }
                bx = SkillFactory.getSkill(33120011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    addDamageIncrease(33100009, eff.getDamage());
                }


                bx = SkillFactory.getSkill(33120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(33121002, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(33120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(33121002, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(33120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(33121002, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(33120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(33121009, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(33120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(33121009, bx.getEffect(bof).getIgnoreMob());
                }

                break;
            case 3510:
            case 3511:
            case 3512:
                bx = SkillFactory.getSkill(35100011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(35100000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }
                bx = SkillFactory.getSkill(35120000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.trueMastery += bx.getEffect(bof).getMastery();
                }
                bx = SkillFactory.getSkill(35110014);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(35001003, eff.getDAMRate());
                    addDamageIncrease(35101003, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(35121006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(35111001, eff.getDAMRate());
                    addDamageIncrease(35111009, eff.getDAMRate());
                    addDamageIncrease(35111010, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(35120001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(35111005, eff.getX());
                    addDamageIncrease(35111011, eff.getX());
                    addDamageIncrease(35121009, eff.getX());
                    addDamageIncrease(35121010, eff.getX());
                    addDamageIncrease(35121011, eff.getX());
                    this.BuffUP_Summon += eff.getY();
                }


                bx = SkillFactory.getSkill(35120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(35121012, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(35120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(35121012, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(35120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(35121012, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 3002:
            case 3600:
            case 3610:
            case 3611:
            case 3612:
                bx = SkillFactory.getSkill(30020233);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr = ((int) (this.localstr + eff.getStrRate() / 100.0D * this.str));
                    this.localdex = ((int) (this.localdex + eff.getDexRate() / 100.0D * this.dex));
                    this.localluk = ((int) (this.localluk + eff.getLukRate() / 100.0D * this.luk));
                    this.localint_ = ((int) (this.localint_ + eff.getIntRate() / 100.0D * this.int_));
                }
                bx = SkillFactory.getSkill(36000003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.accuracy += eff.getPercentAcc();
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(36001002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                    this.localint_ += eff.getIntX();
                    this.localluk += eff.getLukX();
                }
                bx = SkillFactory.getSkill(36100005);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                    this.localint_ += eff.getIntX();
                    this.localluk += eff.getLukX();
                }
                bx = SkillFactory.getSkill(36100006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }
                bx = SkillFactory.getSkill(36100010);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(36001005, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(36101002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                    this.localint_ += eff.getIntX();
                    this.localluk += eff.getLukX();
                }
                bx = SkillFactory.getSkill(36110012);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    if (this.damageIncrease.containsKey(36001005))
                    {
                        addDamageIncrease(36001005, this.damageIncrease.get(36001005) + eff.getDAMRate());
                    }
                }
                bx = SkillFactory.getSkill(36111003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                    this.localint_ += eff.getIntX();
                    this.localluk += eff.getLukX();
                    this.percent_wdef += eff.getWdefX();
                    this.percent_mdef += eff.getMdefX();
                }
                bx = SkillFactory.getSkill(36120015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    if (this.damageIncrease.containsKey(36001005))
                    {
                        addDamageIncrease(36001005, this.damageIncrease.get(36001005) + eff.getDAMRate());
                    }
                }
                bx = SkillFactory.getSkill(36121003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                    this.localint_ += eff.getIntX();
                    this.localluk += eff.getLukX();
                }


                bx = SkillFactory.getSkill(36120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(36121000, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(36120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(36121000, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(36120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(36121001, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(36121011, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(36121012, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(36120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addIgnoreMobpdpRate(36121001, bx.getEffect(bof).getIgnoreMob());
                    addIgnoreMobpdpRate(36121011, bx.getEffect(bof).getIgnoreMob());
                    addIgnoreMobpdpRate(36121012, bx.getEffect(bof).getIgnoreMob());
                }
                bx = SkillFactory.getSkill(36120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(36121011, bx.getEffect(bof).getTargetPlus());
                    addTargetPlus(36121012, bx.getEffect(bof).getTargetPlus());
                }

                break;
            case 5100:
            case 5110:
            case 5111:
            case 5112:
                bx = SkillFactory.getSkill(51000001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getWdefX();
                    this.percent_mdef += eff.getMdefX();
                }
                bx = SkillFactory.getSkill(51000002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.accuracy += eff.getAccX();
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(51000000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(51100000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.localdex += eff.getDexX();
                }
                bx = SkillFactory.getSkill(51110001);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                }
                bx = SkillFactory.getSkill(51120000);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }
                bx = SkillFactory.getSkill(51120002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.watk += eff.getAttackX();
                    addDamageIncrease(51100002, eff.getDamage());
                }


                bx = SkillFactory.getSkill(51120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(51121008, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(51120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addTargetPlus(51121008, bx.getEffect(bof).getTargetPlus());
                }
                bx = SkillFactory.getSkill(51120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(51121008, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(51120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(51121007, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(51120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(51121007, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 6000:
            case 6100:
            case 6110:
            case 6111:
            case 6112:
                bx = SkillFactory.getSkill(60000222);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(61000003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_wdef += eff.getWdefX();
                    this.percent_mdef += eff.getMdefX();
                }
                bx = SkillFactory.getSkill(61001002);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.speed += bx.getEffect(bof).getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(61100009);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(61001000, eff.getDAMRate());
                    addDamageIncrease(61001004, eff.getDAMRate());
                    addDamageIncrease(61001005, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(61100007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.percent_hp += eff.getPercentHP();
                }
                bx = SkillFactory.getSkill(61110015);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(61001000, eff.getDAMRate());
                    addDamageIncrease(61001004, eff.getDAMRate());
                    addDamageIncrease(61001005, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(61110007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localstr += eff.getStrX();
                    this.percent_hp += eff.getPercentHP();
                }
                bx = SkillFactory.getSkill(61120007);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }
                bx = SkillFactory.getSkill(61120020);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    addDamageIncrease(61001000, eff.getDAMRate());
                    addDamageIncrease(61001004, eff.getDAMRate());
                    addDamageIncrease(61001005, eff.getDAMRate());
                }
                bx = SkillFactory.getSkill(61120011);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_ignore_mob_def_rate += bx.getEffect(bof).getIgnoreMob();
                }


                bx = SkillFactory.getSkill(61120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(61121100, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(61121201, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(61120044);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(61121100, bx.getEffect(bof).getDuration());
                    addBuffDuration(61121201, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(61120045);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(61121100, bx.getEffect(bof).getAttackCount());
                    addAttackCount(61121201, bx.getEffect(bof).getAttackCount());
                }
                bx = SkillFactory.getSkill(61120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(61121105, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(61120047);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(61120018, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(61120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(61120018, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(61120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(61111100, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(61111113, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(61111111, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(61120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addBuffDuration(61111100, bx.getEffect(bof).getDuration());
                    addBuffDuration(61111113, bx.getEffect(bof).getDuration());
                    addBuffDuration(61111111, bx.getEffect(bof).getDuration());
                }
                bx = SkillFactory.getSkill(61120051);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addAttackCount(61111100, bx.getEffect(bof).getAttackCount());
                    addAttackCount(61111113, bx.getEffect(bof).getAttackCount());
                    addAttackCount(61111111, bx.getEffect(bof).getAttackCount());
                }

                break;
            case 6001:
            case 6500:
            case 6510:
            case 6511:
            case 6512:
                bx = SkillFactory.getSkill(65000003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.jump += eff.getPassiveJump();
                    this.speed += eff.getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(65100003);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.watk += bx.getEffect(bof).getAttackX();
                }
                bx = SkillFactory.getSkill(65100004);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.localdex += bx.getEffect(bof).getDexX();
                }
                bx = SkillFactory.getSkill(65100005);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }
                bx = SkillFactory.getSkill(65110006);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localdex += eff.getDexX();
                    this.percent_damage += eff.getDAMRate();
                }


                bx = SkillFactory.getSkill(65120043);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(65111007, bx.getEffect(bof).getDAMRate());
                    addDamageIncrease(65111100, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(65120046);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(65121002, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(65120048);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(65121002, bx.getEffect(bof).getCooltimeReduceR());
                }
                bx = SkillFactory.getSkill(65120049);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addDamageIncrease(65121003, bx.getEffect(bof).getDAMRate());
                }
                bx = SkillFactory.getSkill(65120050);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    addCoolTimeReduce(65121003, bx.getEffect(bof).getCooltimeReduceR());
                }

                break;
            case 10000:
            case 10100:
            case 10110:
            case 10111:
            case 10112:
                if (chra.isZeroSecondLook())
                {
                    bx = SkillFactory.getSkill(101000103);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.watk += eff.getAttackX();
                        this.percent_damage_rate += eff.getMobCountDamage();
                        this.percent_boss_damage_rate += eff.getBossDamage();
                    }
                    bx = SkillFactory.getSkill(101100102);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.wdef += eff.getWdefX();
                        this.mdef += eff.getMdefX();
                        this.ASR += eff.getASRRate();
                        this.TER += eff.getTERRate();
                    }
                }
                else
                {
                    bx = SkillFactory.getSkill(101000203);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.watk += eff.getAttackX();
                        this.jump += eff.getPassiveJump();
                        this.speed += eff.getPassiveSpeed();
                        this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    }
                    bx = SkillFactory.getSkill(101100203);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_hp += eff.getPercentHP();
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                    }
                    bx = SkillFactory.getSkill(101120207);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + eff.getCriticalMax()));
                    }
                }

                break;
            case 11000:
            case 11200:
            case 11210:
            case 11211:
            case 11212:
                bx = SkillFactory.getSkill(110000800);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.percent_hp += eff.getPercentHP();
                    this.percent_mp += eff.getPercentMP();
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                    this.percent_boss_damage_rate += eff.getBossDamage();
                }
                bx = SkillFactory.getSkill(110000513);
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    MapleStatEffect eff = bx.getEffect(bof);
                    this.localint_ += eff.getIntX();
                    this.localluk += eff.getLukX();
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                    this.percent_hp += eff.getPercentHP();
                    this.percent_mp += eff.getPercentMP();
                    this.percent_boss_damage_rate += eff.getBossDamage();
                    this.percent_damage += eff.getMagicDamage();
                    this.wdef += eff.getWdefX();
                    this.mdef += eff.getMdefX();
                    this.ASR += eff.getASRRate();
                    this.TER += eff.getTERRate();
                }
                int buffSourceId = chra.getBuffSource(MapleBuffStat.模式变更);
                if (buffSourceId == 110001501)
                {
                    bx = SkillFactory.getSkill(112000011);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_hp += eff.getPercentHP();
                        this.localint_ += eff.getIntX();
                    }
                    bx = SkillFactory.getSkill(112000012);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    }

                    bx = SkillFactory.getSkill(112000003);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        addDamageIncrease(112000000, eff.getDAMRate());
                        addDamageIncrease(112000001, eff.getDAMRate());
                        addDamageIncrease(112000002, eff.getDAMRate());
                    }
                    bx = SkillFactory.getSkill(112000014);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.magic += eff.getMagicX();
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
                        this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + eff.getCriticalMax()));
                        this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
                    }
                    bx = SkillFactory.getSkill(112000013);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_damage += eff.getMagicDamage();
                    }
                    bx = SkillFactory.getSkill(112001009);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_hp += eff.getPercentHP();
                        this.percent_mp += eff.getPercentMP();
                        this.percent_wdef += eff.getWDEFRate();
                        this.percent_mdef += eff.getMDEFRate();
                        this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    }
                    bx = SkillFactory.getSkill(112000020);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.magic += eff.getMagicX();
                        addDamageIncrease(112001006, eff.getDAMRate());
                    }
                }
                else if (buffSourceId == 110001502)
                {
                    bx = SkillFactory.getSkill(112100013);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.localint_ += eff.getIntX();
                        this.magic += eff.getMagicX();
                        this.jump += eff.getPassiveJump();
                        this.speed += eff.getPassiveSpeed();
                    }
                    bx = SkillFactory.getSkill(112100003);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        addDamageIncrease(112100000, eff.getDAMRate());
                        addDamageIncrease(112100002, eff.getDAMRate());
                        addDamageIncrease(112100001, eff.getDAMRate());
                        addTargetPlus(112100000, eff.getTargetPlus());
                        addTargetPlus(112100002, eff.getTargetPlus());
                        addTargetPlus(112100001, eff.getTargetPlus());
                    }
                    bx = SkillFactory.getSkill(112100006);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        addDamageIncrease(112101004, eff.getDAMRate());
                        addDamageIncrease(112101005, eff.getDAMRate());
                        addTargetPlus(112101004, eff.getTargetPlus());
                        addTargetPlus(112101005, eff.getTargetPlus());
                    }
                    bx = SkillFactory.getSkill(112100010);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.magic += eff.getMagicX();
                    }
                    bx = SkillFactory.getSkill(112100014);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                        addDamageIncrease(112100000, eff.getDAMRate());
                        addDamageIncrease(112100002, eff.getDAMRate());
                        addDamageIncrease(112100003, eff.getDAMRate());
                        addDamageIncrease(112100001, eff.getDAMRate());
                        addDamageIncrease(112101004, eff.getDAMRate());
                        addDamageIncrease(112101005, eff.getDAMRate());
                        addDamageIncrease(112101007, eff.getDAMRate());
                        addDamageIncrease(112100012, eff.getDAMRate());
                        addTargetPlus(112100000, eff.getTargetPlus());
                        addTargetPlus(112100002, eff.getTargetPlus());
                        addTargetPlus(112100003, eff.getTargetPlus());
                        addTargetPlus(112100001, eff.getTargetPlus());
                        addTargetPlus(112101004, eff.getTargetPlus());
                        addTargetPlus(112101005, eff.getTargetPlus());
                        addTargetPlus(112101007, eff.getTargetPlus());
                        addTargetPlus(112100012, eff.getTargetPlus());
                    }
                    bx = SkillFactory.getSkill(112100015);
                    bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect eff = bx.getEffect(bof);
                        this.accuracy += eff.getAccX();
                        this.percent_damage += eff.getMagicDamage();
                        this.percent_ignore_mob_def_rate += eff.getIgnoreMob();
                    }
                }
                else if (buffSourceId != 110001503)
                {
                    if (buffSourceId == 110001504)
                    {
                        break;
                    }
                }
                break;
        }
        if (GameConstants.is反抗者(chra.getJob()))
        {
            bx = SkillFactory.getSkill(30000002);
            bof = chra.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                this.RecoveryUP += bx.getEffect(bof).getX() - 100;
            }
        }


        switch (chra.getJob())
        {
            case 112:
            case 122:
            case 132:
            case 212:
            case 222:
            case 232:
            case 312:
            case 322:
            case 412:
            case 422:
            case 434:
            case 512:
            case 522:
            case 532:
            case 2112:
            case 2217:
            case 2218:
            case 2312:
            case 2412:
            case 2712:
            case 3112:
            case 3122:
            case 3212:
            case 3312:
            case 3512:
            case 3612:
            case 5112:
            case 6112:
            case 6512:
                bx = SkillFactory.getSkill(getHyperSkillByJob(30, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.localstr += bx.getEffect(bof).getStrX();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(31, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.localdex += bx.getEffect(bof).getDexX();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(32, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.localint_ += bx.getEffect(bof).getIntX();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(33, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.localluk += bx.getEffect(bof).getLukX();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(34, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + bx.getEffect(bof).getCritical()));
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(35, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                }


                bx = SkillFactory.getSkill(getHyperSkillByJob(36, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if (bof > 0)
                {
                    this.percent_hp += bx.getEffect(bof).getPercentHP();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(37, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.percent_mp += bx.getEffect(bof).getPercentMP();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(38, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.incMaxDF = bx.getEffect(bof).getIndieMaxDF();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(39, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.percent_wdef += bx.getEffect(bof).getWdefX();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(40, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.percent_mdef += bx.getEffect(bof).getMdefX();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(41, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.speed += bx.getEffect(bof).getPassiveSpeed();
                }
                bx = SkillFactory.getSkill(getHyperSkillByJob(42, chra.getJob()));
                bof = chra.getTotalSkillLevel(bx);
                if ((bx.isHyperSkill()) && (bof > 0))
                {
                    this.jump += bx.getEffect(bof).getPassiveJump();
                }
                break;
        }
    }

    private void handleBuffStats(MapleCharacter chra)
    {
        MapleStatEffect effect = chra.getStatForBuff(MapleBuffStat.骑兽技能);
        if ((effect != null) && (effect.getSourceId() == 33001001))
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + effect.getW()));
            this.percent_hp += effect.getZ();
        }
        Integer buff = chra.getBuffedValue(MapleBuffStat.物理攻击);
        if (buff != null)
        {
            this.watk += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.物理防御);
        if (buff != null)
        {
            this.wdef += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.魔法防御);
        if (buff != null)
        {
            this.mdef += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.魔法攻击);
        if (buff != null)
        {
            this.magic += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.命中率);
        if (buff != null)
        {
            this.accuracy += buff;
        }

        buff = chra.getBuffedSkill_Y(MapleBuffStat.隐身术);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.移动速度);
        if (buff != null)
        {
            this.speed += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.跳跃力);
        if (buff != null)
        {
            this.jump += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.MAXHP);
        if (buff != null)
        {
            this.percent_hp += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.MAXMP);
        if (buff != null)
        {
            this.percent_mp += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.斗气集中);
        if (buff != null)
        {
            Skill combos = SkillFactory.getSkill(1110013);
            int comboslevel = chra.getTotalSkillLevel(combos);
            if (comboslevel > 0)
            {
                effect = combos.getEffect(comboslevel);
                this.percent_damage_rate += buff * effect.getX();
                this.percent_boss_damage_rate += buff * effect.getX();
            }
        }
        effect = chra.getStatForBuff(MapleBuffStat.召唤兽);
        if ((effect != null) && (effect.getSourceId() == 35121010))
        {
            this.percent_damage_rate += effect.getX();
            this.percent_boss_damage_rate += effect.getX();
        }

        effect = chra.getStatForBuff(MapleBuffStat.属性攻击);
        if (effect != null)
        {
            this.percent_damage_rate += effect.getDamage();
            this.percent_boss_damage_rate += effect.getDamage();
        }

        buff = chra.getBuffedValue(MapleBuffStat.聚财术);
        if (buff != null)
        {
            this.mesoBuff *= buff.doubleValue() / 100.0D;
        }
        effect = chra.getStatForBuff(MapleBuffStat.敛财术);
        if (effect != null)
        {
            this.pickRate = effect.getProb();
        }
        buff = chra.getBuffedValue(MapleBuffStat.金钱护盾);
        if (buff != null)
        {
            this.mesoGuardMeso += buff.doubleValue();
        }

        buff = chra.getBuffedValue(MapleBuffStat.冒险岛勇士);
        if (buff != null)
        {
            double d = buff.doubleValue() / 100.0D;
            this.localstr = ((int) (this.localstr + d * this.str));
            this.localdex = ((int) (this.localdex + d * this.dex));
            this.localluk = ((int) (this.localluk + d * this.luk));
            this.localint_ = ((int) (this.localint_ + d * this.int_));
        }
        buff = chra.getBuffedValue(MapleBuffStat.敏捷提升);
        if (buff != null)
        {
            double d = buff.doubleValue() / 100.0D;
            this.localdex = ((int) (this.localdex + d * this.dex));
        }
        effect = chra.getStatForBuff(MapleBuffStat.火眼晶晶);
        if (effect != null)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + effect.getX()));
            this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + effect.getCriticalMax()));
        }
        buff = chra.getBuffedValue(MapleBuffStat.终极无限);
        if (buff != null)
        {
            this.percent_matk += buff - 1;
        }
        buff = chra.getBuffedSkill_X(MapleBuffStat.集中精力);
        if (buff != null)
        {
            this.mpconReduce += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.英雄回声);
        if (buff != null)
        {
            double d = buff.doubleValue() / 100.0D;
            this.watk += (int) (this.watk * d);
            this.magic += (int) (this.magic * d);
        }
        buff = chra.getBuffedValue(MapleBuffStat.MESO_RATE);
        if (buff != null)
        {
            this.mesoBuff *= buff.doubleValue() / 100.0D;
        }
        buff = chra.getBuffedValue(MapleBuffStat.DROP_RATE);
        if (buff != null)
        {
            this.dropBuff *= buff.doubleValue() / 100.0D;
        }
        buff = chra.getBuffedValue(MapleBuffStat.EXPRATE);
        if (buff != null)
        {
            this.expBuff *= buff.doubleValue() / 100.0D;
        }
        buff = chra.getBuffedValue(MapleBuffStat.ACASH_RATE);
        if (buff != null)
        {
            this.cashBuff *= buff.doubleValue() / 100.0D;
        }
        buff = chra.getBuffedSkill_X(MapleBuffStat.狂暴战魂);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }

        effect = chra.getStatForBuff(MapleBuffStat.战神之盾);
        if (effect != null)
        {
            this.reduceDamageRate += effect.getT();
        }
        buff = chra.getBuffedValue(MapleBuffStat.魔法屏障);
        if (buff != null)
        {
            this.reduceDamageRate += buff;
        }
        effect = chra.getStatForBuff(MapleBuffStat.风影漫步);
        if (effect != null)
        {
            this.percent_damage_rate += effect.getDamage();
            this.percent_boss_damage_rate += effect.getDamage();
        }
        buff = chra.getBuffedValue(MapleBuffStat.矛连击强化);
        if (buff != null)
        {
            int level = Math.max(1, chra.getAranCombo() / 10);
            if (chra.getTotalSkillLevel(21110000) > 0)
            {
                effect = SkillFactory.getSkill(21110000).getEffect(chra.getTotalSkillLevel(21110000));
                int num = Math.min(effect.getX(), level);
                this.watk += num * effect.getW();
                this.ASR += num * effect.getW();
                this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + num * effect.getY()));
            }
            else if (chra.getTotalSkillLevel(21000000) > 0)
            {
                effect = SkillFactory.getSkill(21000000).getEffect(chra.getTotalSkillLevel(21000000));
                int num = Math.min(effect.getX(), level);
                this.watk += num * effect.getY();
            }
        }

        effect = chra.getStatForBuff(MapleBuffStat.抗魔领域);
        if (effect != null)
        {
            this.ASR += effect.getX();
        }
        effect = chra.getStatForBuff(MapleBuffStat.雷鸣冲击);
        if (effect != null)
        {
            this.percent_damage_rate += effect.getDamage();
            this.percent_boss_damage_rate += effect.getDamage();
        }
        buff = chra.getBuffedSkill_X(MapleBuffStat.葵花宝典);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedSkill_Y(MapleBuffStat.死亡猫头鹰);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedSkill_Y(MapleBuffStat.终极斩);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.DAMAGE_BUFF);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.ATTACK_BUFF);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.增强_物理攻击);
        if (buff != null)
        {
            this.watk += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.增强_魔法攻击);
        if (buff != null)
        {
            this.magic += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.增强_物理防御);
        if (buff != null)
        {
            this.wdef += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.增强_魔法防御);
        if (buff != null)
        {
            this.mdef += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.呼啸_爆击概率);
        if (buff != null)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + buff));
        }
        buff = chra.getBuffedValue(MapleBuffStat.呼啸_MaxMp增加);
        if (buff != null)
        {
            this.percent_mp += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.幻灵转化);
        if (buff != null)
        {
            this.percent_hp += buff;
        }

        effect = chra.getStatForBuff(MapleBuffStat.金属机甲);
        if (effect != null)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + effect.getCritical()));
        }
        effect = chra.getStatForBuff(MapleBuffStat.黑暗灵气);
        if (effect != null)
        {
            this.percent_damage_rate += effect.getX();
            this.percent_boss_damage_rate += effect.getX();
        }
        effect = chra.getStatForBuff(MapleBuffStat.蓝色灵气);
        if (effect != null)
        {
            this.percent_wdef += effect.getZ() + effect.getY();
            this.percent_mdef += effect.getZ() + effect.getY();
        }
        effect = chra.getStatForBuff(MapleBuffStat.幻灵霸体);
        if (effect != null)
        {
            this.percent_damage_rate += effect.getV();
            this.percent_boss_damage_rate += effect.getV();
        }
        buff = chra.getBuffedValue(MapleBuffStat.暴走形态);
        if (buff != null)
        {
            this.percent_hp += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.幸运骰子);
        if (buff != null)
        {
            this.percent_wdef += GameConstants.getDiceStat(buff, 2);
            this.percent_mdef += GameConstants.getDiceStat(buff, 2);
            this.percent_hp += GameConstants.getDiceStat(buff, 3);
            this.percent_mp += GameConstants.getDiceStat(buff, 3);
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + GameConstants.getDiceStat(buff, 4)));
            this.percent_damage_rate += GameConstants.getDiceStat(buff, 5);
            this.percent_boss_damage_rate += GameConstants.getDiceStat(buff, 5);
            this.expBuff *= (GameConstants.getDiceStat(buff, 6) + 100.0D) / 100.0D;
        }
        effect = chra.getStatForBuff(MapleBuffStat.祝福护甲);
        if (effect != null)
        {
            this.watk += effect.getEnhancedWatk();
        }
        effect = chra.getStatForBuff(MapleBuffStat.反制攻击);
        if (effect != null)
        {
            switch (effect.getSourceId())
            {
                case 5120011:
                case 5220012:
                case 5720012:
                case 51111003:
                    this.percent_damage_rate += effect.getIndieDamR();
                    this.percent_boss_damage_rate += effect.getIndieDamR();
                    break;
                case 5121015:
                    this.percent_damage_rate += effect.getX();
                    this.percent_boss_damage_rate += effect.getX();
                    break;
                case 31121005:
                    this.percent_damage += effect.getDAMRate();
                    break;
                default:
                    this.percent_damage_rate += effect.getDAMRate();
                    this.percent_boss_damage_rate += effect.getDAMRate();
            }

        }
        buff = chra.getBuffedSkill_X(MapleBuffStat.战斗命令);
        if (buff != null)
        {
            this.combatOrders += buff;
        }
        effect = chra.getStatForBuff(MapleBuffStat.灵魂助力);
        if (effect != null)
        {
            this.trueMastery += effect.getMastery();
        }
        buff = chra.getBuffedValue(MapleBuffStat.玛瑙的保佑);
        if (buff != null)
        {
            this.dodgeChance += buff;
        }
        effect = chra.getStatForBuff(MapleBuffStat.牧师祝福);
        if (effect != null)
        {
            this.watk += effect.getX();
            this.magic += effect.getY();
            this.accuracy += effect.getV();
        }

        buff = chra.getBuffedValue(MapleBuffStat.力量);
        if (buff != null)
        {
            this.localstr += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.敏捷);
        if (buff != null)
        {
            this.localdex += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.智力);
        if (buff != null)
        {
            this.localint_ += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.运气);
        if (buff != null)
        {
            this.localluk += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.indiePad);
        if (buff != null)
        {
            this.watk += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.indieMad);
        if (buff != null)
        {
            this.magic += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.indieAcc);
        if (buff != null)
        {
            this.accuracy += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.indieAllStat);
        if (buff != null)
        {
            this.localstr += buff;
            this.localdex += buff;
            this.localint_ += buff;
            this.localluk += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.PVP_DAMAGE);
        if (buff != null)
        {
            this.pvpDamage += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.PVP_ATTACK);
        if (buff != null)
        {
            this.pvpDamage += buff;
        }
        effect = chra.getStatForBuff(MapleBuffStat.潜力解放);
        if (effect != null)
        {
            this.passive_sharpeye_rate = 100;
            this.ASR = 100;
            this.wdef += effect.getX();
            this.mdef += effect.getX();
            this.watk += effect.getX();
            this.magic += effect.getX();
        }


        buff = chra.getBuffedValue(MapleBuffStat.伤害吸收);
        if (buff != null)
        {
            this.reduceDamageRate += buff;
        }
        buff = chra.getBuffedSkill_X(MapleBuffStat.属性抗性);
        if (buff != null)
        {
            this.BuffUP_Skill += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.物理防御增加);
        if (buff != null)
        {
            this.wdef += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.魔法防御增加);
        if (buff != null)
        {
            this.mdef += buff;
        }
        effect = chra.getStatForBuff(MapleBuffStat.精神连接);
        if ((effect != null) && (effect.is船员统帅()))
        {
            this.watk += effect.getWatk();
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + effect.getCritical()));
        }

        buff = chra.getBuffedValue(MapleBuffStat.爆击提升);
        if (buff != null)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + buff));
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }


        buff = chra.getBuffedValue(MapleBuffStat.百分比MaxHp);
        if (buff != null)
        {
            this.percent_hp += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.百分比MaxMp);
        if (buff != null)
        {
            this.percent_mp += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.百分比无视防御);
        if (buff != null)
        {
            this.percent_ignore_mob_def_rate += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.爆击概率增加);
        if (buff != null)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + buff));
        }
        buff = chra.getBuffedValue(MapleBuffStat.最小爆击伤害);
        if (buff != null)
        {
            this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + buff));
        }
        effect = chra.getStatForBuff(MapleBuffStat.卡牌审判);
        buff = chra.getBuffedValue(MapleBuffStat.卡牌审判);
        if ((effect != null) && (buff != null))
        {
            switch (buff)
            {
                case 1:
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + effect.getV()));
                    break;
                case 2:
                    this.dropBuff *= (effect.getW() + 100.0D) / 100.0D;
                    break;
                case 3:
                    this.ASR += effect.getX();
                    this.TER += effect.getY();
            }

        }
        buff = chra.getBuffedValue(MapleBuffStat.伤害增加);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.增加_物理攻击);
        if (buff != null)
        {
            this.watk += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.黑暗高潮);
        if (buff != null)
        {
            this.percent_damage += buff;
        }
        effect = chra.getStatForBuff(MapleBuffStat.生命潮汐);
        if (effect != null)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + effect.getProb()));
        }


        effect = chra.getStatForBuff(MapleBuffStat.黑暗祝福);
        if (effect != null)
        {
            buff = chra.getBuffedValue(MapleBuffStat.黑暗祝福);
            Skill bx = SkillFactory.getSkill(27100003);
            int bof = chra.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect skilleff = bx.getEffect(bof);
                if (buff == 1)
                {
                    this.magic += skilleff.getU();
                }
                else if (buff == 2)
                {
                    this.magic += skilleff.getV();
                }
                else if (buff == 3)
                {
                    this.magic += skilleff.getY();
                }
            }
        }
        effect = chra.getStatForBuff(MapleBuffStat.模式转换);
        if (effect != null)
        {
            if (effect.getSourceId() == 60001216)
            {
                this.percent_wdef += effect.getWdefX();
                this.percent_mdef += effect.getMdefX();
                this.accuracy += effect.getAccX();
                this.percent_hp += effect.getPercentHP();
                int[] skills = {61100005, 61110005, 61120010};
                for (int i : skills)
                {
                    Skill bx = SkillFactory.getSkill(i);
                    int bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect skilleff = bx.getEffect(bof);
                        this.percent_wdef += skilleff.getWdefX();
                        this.percent_mdef += skilleff.getMdefX();
                        this.accuracy += skilleff.getAccX();
                        this.percent_hp += skilleff.getPercentHP();
                    }
                }
            }
            else if (effect.getSourceId() == 60001217)
            {
                this.watk += effect.getAttackX();
                this.percent_boss_damage_rate += effect.getBossDamage();
                this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + effect.getCritical()));
                int[] skills = {61100008, 61110010, 61120013};
                for (int i : skills)
                {
                    Skill bx = SkillFactory.getSkill(i);
                    int bof = chra.getTotalSkillLevel(bx);
                    if (bof > 0)
                    {
                        MapleStatEffect skilleff = bx.getEffect(bof);
                        this.watk += skilleff.getAttackX();
                        this.percent_boss_damage_rate += skilleff.getBossDamage();
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + skilleff.getCritical()));
                    }
                }
            }
        }

        buff = chra.getBuffedValue(MapleBuffStat.灵魂凝视);
        if (buff != null)
        {
            this.percent_damage_rate += buff;
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.伤害上限);
        if (buff != null)
        {
            this.incMaxDamage += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.暴击概率);
        if (buff != null)
        {
            this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + buff));
        }
        buff = chra.getBuffedValue(MapleBuffStat.BOSS伤害);
        if (buff != null)
        {
            this.percent_boss_damage_rate += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.恶魔超越);
        if (buff != null)
        {
            this.hpRecoverPercent -= buff / 2;
            if (this.hpRecoverPercent < 0)
            {
                this.hpRecoverPercent = 0;
            }
        }


        buff = chra.getBuffedValue(MapleBuffStat.交叉锁链);
        if (buff != null)
        {
            this.percent_damage += buff;
        }

        buff = chra.getBuffedValue(MapleBuffStat.BOSS伤害增加);
        if (buff != null)
        {
            this.percent_boss_damage_rate += buff;
        }
        effect = chra.getStatForBuff(MapleBuffStat.极限射箭);
        if (effect != null)
        {
            switch (effect.getSourceId())
            {
                case 3111011:
                    this.watk += effect.getAttackX();
                    break;
            }

        }


        buff = chra.getBuffedValue(MapleBuffStat.疾驰速度);
        if (buff != null)
        {
            this.speed += buff;
        }
        buff = chra.getBuffedValue(MapleBuffStat.疾驰跳跃);
        if (buff != null)
        {
            this.jump += buff;
        }

        if (this.speed > 140)
        {
            this.speed = 140;
        }
        if (this.jump > 123)
        {
            this.jump = 123;
        }
        buff = chra.getBuffedValue(MapleBuffStat.骑兽技能);
        if (buff != null)
        {
            this.jump = 120;
            switch (buff)
            {
                case 1:
                    this.speed = 150;
                    break;
                case 2:
                    this.speed = 170;
                    break;
                case 3:
                    this.speed = 180;
                    break;
                default:
                    this.speed = 200;
            }
        }
    }

    public short getDex()
    {
        return this.dex;
    }

    public short getStr()
    {
        return this.str;
    }

    public short getInt()
    {
        return this.int_;
    }

    public short getLuk()
    {
        return this.luk;
    }

    private void calculateFame(MapleCharacter player)
    {
        player.getTrait(MapleTraitType.charm).addLocalExp(player.getFame());
        for (MapleTraitType t : MapleTraitType.values())
        {
            player.getTrait(t).recalcLevel();
        }
    }

    private void CalcPassive_SharpEye(MapleCharacter player)
    {
        if (GameConstants.is反抗者(player.getJob()))
        {
            Skill critSkill = SkillFactory.getSkill(30000022);
            int critlevel = player.getTotalSkillLevel(critSkill);
            if (critlevel > 0)
            {
                this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + critSkill.getEffect(critlevel).getProb()));
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
            }
            critSkill = SkillFactory.getSkill(30010022);
            critlevel = player.getTotalSkillLevel(critSkill);
            if (critlevel > 0)
            {
                this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + critSkill.getEffect(critlevel).getProb()));
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
            }
        }
        Skill critSkill;
        int critlevel;
        switch (player.getJob())
        {
            case 410:
            case 411:
            case 412:
                critSkill = SkillFactory.getSkill(4100001);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getProb()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 1410:
            case 1411:
            case 1412:
                critSkill = SkillFactory.getSkill(14100001);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getProb()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 3100:
            case 3110:
            case 3111:
            case 3112:
                critSkill = SkillFactory.getSkill(31100006);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.watk += critSkill.getEffect(critlevel).getAttackX();
                }

                break;
            case 2300:
            case 2310:
            case 2311:
            case 2312:
                critSkill = SkillFactory.getSkill(23000003);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 3210:
            case 3211:
            case 3212:
                critSkill = SkillFactory.getSkill(32100006);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 434:
                critSkill = SkillFactory.getSkill(4340010);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getProb()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 211:
            case 212:
                critSkill = SkillFactory.getSkill(2110009);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 221:
            case 222:
                critSkill = SkillFactory.getSkill(2210009);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 231:
            case 232:
                critSkill = SkillFactory.getSkill(2310010);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 1211:
            case 1212:
                critSkill = SkillFactory.getSkill(12110000);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 530:
            case 531:
            case 532:
                critSkill = SkillFactory.getSkill(5300004);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 300:
            case 310:
            case 311:
            case 312:
            case 320:
            case 321:
            case 322:
                critSkill = SkillFactory.getSkill(3000001);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getProb()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 2214:
            case 2215:
            case 2216:
            case 2217:
            case 2218:
                critSkill = SkillFactory.getSkill(22140000);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getProb()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }

                break;
            case 570:
            case 571:
            case 572:
                critSkill = SkillFactory.getSkill(5080004);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }
                critSkill = SkillFactory.getSkill(5710005);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                }
                critSkill = SkillFactory.getSkill(5720008);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                    this.percent_boss_damage_rate += critSkill.getEffect(critlevel).getProb();
                }

                break;
            case 2003:
            case 2410:
            case 2411:
            case 2412:
                critSkill = SkillFactory.getSkill(24110007);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                }
                critSkill = SkillFactory.getSkill(20030204);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                }

                break;
            case 3101:
            case 3120:
            case 3121:
            case 3122:
                critSkill = SkillFactory.getSkill(31010003);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                }

                break;
            case 500:
            case 510:
            case 511:
            case 512:
            case 520:
            case 521:
            case 522:
                critSkill = SkillFactory.getSkill(5000007);
                critlevel = player.getTotalSkillLevel(critSkill);
                if (critlevel > 0)
                {
                    this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                    this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                }
                if ((player.getJob() == 511) || (player.getJob() == 512))
                {
                    critSkill = SkillFactory.getSkill(5110011);
                    critlevel = player.getTotalSkillLevel(critSkill);
                    if (critlevel > 0)
                    {
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                        this.passive_sharpeye_max_percent = ((short) (this.passive_sharpeye_max_percent + critSkill.getEffect(critlevel).getCriticalMax()));
                        this.percent_boss_damage_rate += critSkill.getEffect(critlevel).getProb();
                    }
                    critSkill = SkillFactory.getSkill(5110000);
                    critlevel = player.getTotalSkillLevel(critSkill);
                    if (critlevel > 0)
                    {
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getProb()));
                        this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + critSkill.getEffect(critlevel).getCriticalMin()));
                    }
                }
                if ((player.getJob() == 521) || (player.getJob() == 522))
                {
                    critSkill = SkillFactory.getSkill(5210013);
                    critlevel = player.getTotalSkillLevel(critSkill);
                    if (critlevel > 0)
                    {
                        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + (short) critSkill.getEffect(critlevel).getCritical()));
                        this.percent_ignore_mob_def_rate += critSkill.getEffect(critlevel).getIgnoreMob();
                    }
                }
                break;
        }
    }

    private void CalcPassive_Mastery(MapleCharacter player)
    {
        if (player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11) == null)
        {
            this.passive_mastery = 0;
            return;
        }

        MapleWeaponType weaponType = constants.ItemConstants.getWeaponType(player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11).getItemId());
        boolean acc = true;
        int skil;
        switch (weaponType)
        {
            case 弓:
                skil = GameConstants.is风灵使者(player.getJob()) ? 13100025 : 3100000;
                break;
            case 拳套:
                skil = 4100000;
                break;
            case 手杖:
                skil = player.getTotalSkillLevel(24120006) > 0 ? 24120006 : 24100004;
                break;
            case 手持火炮:
                skil = 5300005;
                break;
            case 双刀副手:
            case 短刀:
                skil = (player.getJob() >= 430) && (player.getJob() <= 434) ? 4300000 : 4200000;
                break;
            case 弩:
                skil = GameConstants.is反抗者(player.getJob()) ? 33100000 : 3200000;
                break;
            case 单手斧:
            case 单手钝器:
                skil = (player.getJob() >= 110) && (player.getJob() <= 112) ? 1100000 : GameConstants.is米哈尔(player.getJob()) ? 51100001 : GameConstants.is魂骑士(player.getJob()) ? 11100025 :
                        GameConstants.is恶魔猎手(player.getJob()) ? 31100004 : 1200000;
                break;
            case 双手斧:
            case 单手剑:
            case 双手剑:
            case 双手钝器:
                skil = (player.getJob() >= 110) && (player.getJob() <= 112) ? 1100000 : GameConstants.is米哈尔(player.getJob()) ? 51100001 : GameConstants.is魂骑士(player.getJob()) ? 11100025 :
                        GameConstants.is狂龙战士(player.getJob()) ? 61100006 : 1200000;
                break;
            case 矛:
                skil = GameConstants.is战神(player.getJob()) ? 21100000 : 1300000;
                break;
            case 枪:
                skil = 1300000;
                break;
            case 指节:
                skil = GameConstants.is奇袭者(player.getJob()) ? 15100023 : 5100001;
                break;
            case 短枪:
                skil = GameConstants.is龙的传人(player.getJob()) ? 5700000 : GameConstants.is反抗者(player.getJob()) ? 35100000 : 5200000;
                break;
            case 双弩枪:
                skil = 23100005;
                break;
            case 短杖:
            case 长杖:
                acc = false;
                skil = player.getJob() <= 2000 ? 12100007 : player.getJob() <= 232 ? 2300006 : player.getJob() <= 222 ? 2200006 : player.getJob() <= 212 ? 2100006 :
                        GameConstants.is反抗者(player.getJob()) ? 32100006 : 22120002;
                break;
            case 双头杖:
                acc = false;
                skil = 27100005;
                break;
            case 灵魂手铳:
                skil = 65100003;
                break;
            case 亡命剑:
                skil = 31200005;
                break;
            case 能量剑:
                skil = 36100006;
                break;
            case 大剑:
                skil = 101000103;
                break;
            case 太刀:
                skil = 101000203;
                break;
            case 驯兽魔法棒:
                acc = false;
                skil = 110000515;
                break;
            default:
                this.passive_mastery = 0;
                return;
        }
        if (player.getSkillLevel(skil) <= 0)
        {
            this.passive_mastery = 0;
            return;
        }

        MapleStatEffect eff = SkillFactory.getSkill(skil).getEffect(player.getTotalSkillLevel(skil));
        if (acc)
        {
            this.accuracy += eff.getX();
        }
        else
        {
            this.magic += eff.getX();
        }
        this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff.getCritical()));
        this.passive_mastery = ((byte) eff.getMastery());
        this.trueMastery += eff.getMastery() + weaponType.getBaseMastery();
        if (player.getJob() == 132)
        {
            Skill bx = SkillFactory.getSkill(1320018);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.watk += eff2.getAttackX();
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
            }
        }
        else if ((player.getJob() == 231) || (player.getJob() == 232))
        {
            Skill bx = SkillFactory.getSkill(2310008);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff2.getCritical()));
                this.percent_acc += eff2.getArRate();
            }
        }
        else if (player.getJob() == 312)
        {
            Skill bx = SkillFactory.getSkill(3120005);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.watk += eff2.getX();
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
            }
        }
        else if (player.getJob() == 322)
        {
            Skill bx = SkillFactory.getSkill(3220004);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.watk += eff2.getX();
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff.getCriticalMin()));
            }
        }
        else if (player.getJob() == 412)
        {
            Skill bx = SkillFactory.getSkill(4120012);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.accuracy += eff2.getPercentAcc();
                this.dodgeChance += eff2.getPercentAvoid();
                this.watk += eff2.getX();
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
            }
        }
        else if (player.getJob() == 422)
        {
            Skill bx = SkillFactory.getSkill(4220012);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.accuracy += eff2.getPercentAcc();
                this.dodgeChance += eff2.getPercentAvoid();
                this.watk += eff2.getX();
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
            }
        }
        else if (player.getJob() == 434)
        {
            Skill bx = SkillFactory.getSkill(4340013);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.accuracy += eff2.getPercentAcc();
                this.dodgeChance += eff2.getPercentAvoid();
                this.watk += eff2.getX();
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
            }
        }
        else if (player.getJob() == 512)
        {
            Skill bx = SkillFactory.getSkill(5121015);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
            }
        }
        else if (player.getJob() == 522)
        {
            Skill bx = SkillFactory.getSkill(5220020);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
            }
        }
        else if (player.getJob() == 1112)
        {
            Skill bx = SkillFactory.getSkill(11120007);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 1312)
        {
            Skill bx = SkillFactory.getSkill(13120006);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 1512)
        {
            Skill bx = SkillFactory.getSkill(15120006);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 2112)
        {
            Skill bx = SkillFactory.getSkill(21120001);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 2712)
        {
            Skill bx = SkillFactory.getSkill(27120007);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.magic += eff2.getX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 3122)
        {
            Skill bx = SkillFactory.getSkill(31220006);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getAttackX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 3612)
        {
            Skill bx = SkillFactory.getSkill(36120006);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getAttackX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 5112)
        {
            Skill bx = SkillFactory.getSkill(51120001);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.watk += eff2.getAttackX();
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
            }
        }
        else if (player.getJob() == 6112)
        {
            Skill bx = SkillFactory.getSkill(61120012);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getAttackX();
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
        else if (player.getJob() == 6512)
        {
            Skill bx = SkillFactory.getSkill(65120005);
            int bof = player.getTotalSkillLevel(bx);
            if (bof > 0)
            {
                MapleStatEffect eff2 = bx.getEffect(bof);
                this.passive_mastery = ((byte) eff2.getMastery());
                this.trueMastery -= eff.getMastery();
                this.trueMastery += eff2.getMastery();
                this.watk += eff2.getAttackX();
                this.passive_sharpeye_rate = ((short) (this.passive_sharpeye_rate + eff2.getCritical()));
                this.passive_sharpeye_min_percent = ((short) (this.passive_sharpeye_min_percent + eff2.getCriticalMin()));
            }
        }
    }

    public void recalcPVPRank(MapleCharacter chra)
    {
        this.pvpRank = 10;
        this.pvpExp = chra.getTotalBattleExp();
        for (int i = 0; i < 10; i++)
        {
            if (this.pvpExp > GameConstants.getPVPExpNeededForLevel(i + 1))
            {
                this.pvpRank -= 1;
                this.pvpExp -= GameConstants.getPVPExpNeededForLevel(i + 1);
            }
        }
    }

    public void relocHeal(MapleCharacter chra)
    {
        int playerjob = chra.getJob();

        this.shouldHealHP = (10 + this.recoverHP);
        this.shouldHealMP = (GameConstants.isNotMpJob(chra.getJob()) ? 0.0F : 3 + this.mpRestore + this.recoverMP + this.localint_ / 10);
        this.mpRecoverTime = 0;
        this.hpRecoverTime = 0;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        if ((playerjob == 111) || (playerjob == 112))
        {
            Skill effect = SkillFactory.getSkill(1110000);
            int lvl = chra.getSkillLevel(effect);
            if (lvl > 0)
            {
                MapleStatEffect eff = effect.getEffect(lvl);
                this.shouldHealHP += eff.getHp();
                this.hpRecoverTime = 4000;
                this.shouldHealMP += eff.getMp();
                this.mpRecoverTime = 4000;
            }
        }
        else if ((playerjob == 1111) || (playerjob == 1112))
        {
            Skill effect = SkillFactory.getSkill(11110025);
            int lvl = chra.getSkillLevel(effect);
            if (lvl > 0)
            {
                MapleStatEffect eff = effect.getEffect(lvl);
                this.shouldHealHP += eff.getY() * this.localmaxhp / 100.0F;
                this.hpRecoverTime = (eff.getW() * 1000);
            }
        }
        else if ((playerjob == 510) || (playerjob == 511) || (playerjob == 512))
        {
            Skill effect = SkillFactory.getSkill(5100013);
            int lvl = chra.getSkillLevel(effect);
            if (lvl > 0)
            {
                MapleStatEffect eff = effect.getEffect(lvl);
                this.shouldHealHP += eff.getX() * this.localmaxhp / 100.0F;
                this.hpRecoverTime = eff.getY();
                this.shouldHealMP += eff.getX() * this.localmaxmp / 100.0F;
                this.mpRecoverTime = eff.getY();
            }
        }
        else if ((playerjob == 570) || (playerjob == 571) || (playerjob == 572))
        {
            Skill effect = SkillFactory.getSkill(5700005);
            int lvl = chra.getSkillLevel(effect);
            if (lvl > 0)
            {
                MapleStatEffect eff = effect.getEffect(lvl);
                this.shouldHealHP += eff.getX() * this.localmaxhp / 100.0F;
                this.hpRecoverTime = (eff.getY() * 1000);
                this.shouldHealMP += eff.getX() * this.localmaxmp / 100.0F;
                this.mpRecoverTime = (eff.getY() * 1000);
            }
        }
        else if (GameConstants.is双弩精灵(playerjob))
        {
            Skill effect = SkillFactory.getSkill(20020109);
            int lvl = chra.getSkillLevel(effect);
            if (lvl > 0)
            {
                MapleStatEffect eff = effect.getEffect(lvl);
                this.shouldHealHP += eff.getX() * this.localmaxhp / 100.0F;
                this.hpRecoverTime = 4000;
                this.shouldHealMP += eff.getX() * this.localmaxmp / 100.0F;
                this.mpRecoverTime = 4000;
            }
        }
        else if ((playerjob == 3111) || (playerjob == 3112))
        {
            Skill effect = SkillFactory.getSkill(31110009);
            int lvl = chra.getSkillLevel(effect);
            if (lvl > 0)
            {
                this.shouldHealMP += effect.getEffect(lvl).getY();
                this.mpRecoverTime = 4000;
            }
        }
        else if ((playerjob == 6111) || (playerjob == 6112))
        {
            Skill effect = SkillFactory.getSkill(61110006);
            int lvl = chra.getSkillLevel(effect);
            if (lvl > 0)
            {
                MapleStatEffect eff = effect.getEffect(lvl);
                this.shouldHealHP += eff.getX() * this.localmaxhp / 100.0F;
                this.hpRecoverTime = eff.getY();
                this.shouldHealMP += eff.getX() * this.localmaxmp / 100.0F;
                this.mpRecoverTime = eff.getY();
            }
        }
        if (chra.getChair() != 0)
        {
            Pair<Integer, Integer> ret = ii.getChairRecovery(chra.getChair());
            this.shouldHealHP += ret.getLeft();
            if (this.hpRecoverTime == 0)
            {
                this.hpRecoverTime = 4000;
            }
            this.shouldHealMP += (GameConstants.isNotMpJob(chra.getJob()) ? 0.0F : ret.getRight());
            if ((this.mpRecoverTime == 0) && (!GameConstants.isNotMpJob(chra.getJob())))
            {
                this.hpRecoverTime = 4000;
            }
        }
        else if (chra.getMap() != null)
        {
            float recvRate = chra.getMap().getRecoveryRate();
            if (recvRate > 0.0F)
            {
                this.shouldHealHP *= recvRate;
                this.shouldHealMP *= recvRate;
            }
        }
    }

    public void calculateMaxBaseDamage(int watk, int lv2damX, int pvpDamage, MapleCharacter chra)
    {
        if (watk <= 0)
        {
            this.localmaxbasedamage = 1.0F;
            this.localmaxbasepvpdamage = 1.0F;
        }
        else
        {
            Item weapon_item = chra.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
            Item weapon_item2 = chra.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
            int job = chra.getJob();
            MapleWeaponType weapon = weapon_item == null ? MapleWeaponType.没有武器 : constants.ItemConstants.getWeaponType(weapon_item.getItemId());
            MapleWeaponType weapon2 = weapon_item2 == null ? MapleWeaponType.没有武器 : constants.ItemConstants.getWeaponType(weapon_item2.getItemId());
            int thirdstat = 0;
            int thirdstatpvp = 0;
            boolean mage =
                    ((job >= 200) && (job <= 232)) || ((job >= 1200) && (job <= 1212)) || ((job >= 2200) && (job <= 2218)) || ((job >= 2700) && (job <= 2712)) || ((job >= 3200) && (job <= 3212)) || ((job >= 11200) && (job <= 11212));
            int secondarystatpvp;
            int mainstat;
            int secondarystat;
            int mainstatpvp;
            switch (weapon)
            {
                case 弓:
                case 弩:
                case 短枪:
                case 双弩枪:
                case 灵魂手铳:
                    mainstat = this.localdex;
                    secondarystat = this.localstr;
                    mainstatpvp = this.dex;
                    secondarystatpvp = this.str;
                    break;
                case 双刀副手:
                case 短刀:
                    mainstat = this.localluk;
                    secondarystat = this.localdex + this.localstr;
                    mainstatpvp = this.luk;
                    secondarystatpvp = this.dex + this.str;
                    break;
                case 拳套:
                case 手杖:
                    mainstat = this.localluk;
                    secondarystat = this.localdex;
                    mainstatpvp = this.luk;
                    secondarystatpvp = this.dex;
                    break;
                case 亡命剑:
                    mainstat = this.localstr;
                    secondarystat = this.maxhp;
                    mainstatpvp = this.localstr;
                    secondarystatpvp = this.maxhp;
                    break;
                case 能量剑:
                    mainstat = this.localstr;
                    secondarystat = this.localluk;
                    thirdstat = this.localdex;
                    mainstatpvp = this.str;
                    secondarystatpvp = this.luk;
                    thirdstatpvp = this.dex;
                    break;
                case 手持火炮:
                case 单手斧:
                case 单手钝器:
                case 双手斧:
                case 单手剑:
                case 双手剑:
                case 双手钝器:
                case 矛:
                case 枪:
                case 指节:
                case 短杖:
                case 长杖:
                case 双头杖:
                default:
                    if (mage)
                    {
                        mainstat = this.localint_;
                        secondarystat = this.localluk;
                        mainstatpvp = this.int_;
                        secondarystatpvp = this.luk;
                    }
                    else
                    {
                        mainstat = this.localstr;
                        secondarystat = this.localdex;
                        mainstatpvp = this.str;
                        secondarystatpvp = this.dex;
                    }
                    break;
            }
            if (GameConstants.is新手职业(job))
            {
                mainstat = this.localstr;
                secondarystat = this.localdex;
                mainstatpvp = this.str;
                secondarystatpvp = this.dex;
            }
            float weaponDamageMultiplier = weapon.getMaxDamageMultiplier();
            this.localmaxbasepvpdamage = (weaponDamageMultiplier * (4 * mainstatpvp + secondarystatpvp) * (100.0F + pvpDamage / 100.0F) + lv2damX);
            this.localmaxbasepvpdamageL = (weaponDamageMultiplier * (4 * mainstat + secondarystat) * (100.0F + pvpDamage / 100.0F) + lv2damX);
            if ((weapon2 != MapleWeaponType.没有武器) && (weapon_item != null) && (weapon_item2 != null) && (GameConstants.is恶魔复仇者(job)))
            {
                Equip we1 = (Equip) weapon_item;
                Equip we2 = (Equip) weapon_item2;
                int watk2 = mage ? we2.getMatk() : we2.getWatk();
                this.localmaxbasedamage = (weaponDamageMultiplier * (4 * mainstat + secondarystat) * ((watk - watk2) / 100.0F) + lv2damX);
                if (watk2 > 0)
                {
                    this.localmaxbasedamage += weapon2.getMaxDamageMultiplier() * (4 * mainstat + secondarystat) * (watk2 / 100.0F);
                    if (server.ServerProperties.ShowPacket())
                    {
                        System.out.println("副手武器: " + weapon2 + " 武器加成: " + weapon2.getMaxDamageMultiplier() + " 副手攻击: " + watk2);
                    }
                }
            }
            else if (GameConstants.is恶魔复仇者(job))
            {
                this.localmaxbasedamage = ((weaponDamageMultiplier * mainstat + secondarystat * 0.15888888F) * (watk / 100.0F) + lv2damX);
            }
            else if (GameConstants.is尖兵(job))
            {
                this.localmaxbasedamage = (weaponDamageMultiplier * (4 * (mainstat + secondarystat + thirdstat)) * (watk / 100.0F) + lv2damX);
            }
            else
            {
                if ((job == 110) || (job == 111) || (job == 112))
                {
                    weaponDamageMultiplier = (float) (weaponDamageMultiplier + 0.1D);
                }
                this.localmaxbasedamage = (weaponDamageMultiplier * (4 * mainstat + secondarystat) * (watk / 100.0F) + lv2damX);
            }

            if (server.ServerProperties.ShowPacket())
            {
                System.out.println("当前攻击: " + this.localmaxbasedamage + " 攻击加成: " + this.localmaxbasedamage * (this.percent_damage / 100.0D));
            }
            this.localmaxbasedamage = ((float) (this.localmaxbasedamage + this.localmaxbasedamage * (this.percent_damage / 100.0D)));
            if (server.ServerProperties.ShowPacket())
            {
                System.out.println("武器类型: " + weapon + " 攻击力: " + watk + " indieDamR: " + this.percent_damage / 100.0D);
                System.out.println("武器加成: " + weaponDamageMultiplier + " 主要属性: " + mainstat + " 次要属性: " + secondarystat + " 第三属性: " + thirdstat + " 攻击力加成: " + watk / 100.0F);
                System.out.println("最终攻击: " + this.localmaxbasedamage + " 武器突破极限: " + getLimitBreak(chra) + " BUFF或者潜能增加上限: " + this.incMaxDamage);
            }
        }
    }

    public boolean isRangedJob(int job)
    {
        return (GameConstants.is龙的传人(job)) || (GameConstants.is双弩精灵(job)) || (GameConstants.is火炮手(job)) || (job == 400) || (job / 10 == 52) || (job / 10 == 59) || (job / 100 == 3) || (job / 100 == 13) || (job / 100 == 14) || (job / 100 == 33) || (job / 100 == 35) || (job / 10 == 41);
    }

    public void addSkillProp(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_prop.containsKey(skillId))
        {
            int oldval = this.add_skill_prop.get(skillId);
            this.add_skill_prop.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_prop.put(skillId, val);
        }
    }

    public void addDamageIncrease(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.damageIncrease.containsKey(skillId))
        {
            int oldval = this.damageIncrease.get(skillId);
            this.damageIncrease.put(skillId, oldval + val);
        }
        else
        {
            this.damageIncrease.put(skillId, val);
        }
    }

    public void addTargetPlus(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_targetPlus.containsKey(skillId))
        {
            int oldval = this.add_skill_targetPlus.get(skillId);
            this.add_skill_targetPlus.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_targetPlus.put(skillId, val);
        }
    }

    public void addAttackCount(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_attackCount.containsKey(skillId))
        {
            int oldval = this.add_skill_attackCount.get(skillId);
            this.add_skill_attackCount.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_attackCount.put(skillId, val);
        }
    }

    public void addBuffDuration(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_duration.containsKey(skillId))
        {
            int oldval = this.add_skill_duration.get(skillId);
            this.add_skill_duration.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_duration.put(skillId, val);
        }
    }

    public void addCoolTimeReduce(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_coolTimeR.containsKey(skillId))
        {
            int oldval = this.add_skill_coolTimeR.get(skillId);
            this.add_skill_coolTimeR.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_coolTimeR.put(skillId, val);
        }
    }

    public void addIgnoreMobpdpRate(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_ignoreMobpdpR.containsKey(skillId))
        {
            int oldval = this.add_skill_ignoreMobpdpR.get(skillId);
            this.add_skill_ignoreMobpdpR.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_ignoreMobpdpR.put(skillId, val);
        }
    }

    public void addBossDamageRate(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_bossDamageRate.containsKey(skillId))
        {
            int oldval = this.add_skill_bossDamageRate.get(skillId);
            this.add_skill_bossDamageRate.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_bossDamageRate.put(skillId, val);
        }
    }

    public static int getHyperSkillByJob(int skillId, int job)
    {
        switch (job)
        {
            case 2217:
            case 2218:
                return 22170000 + skillId;
            case 112:
            case 122:
            case 132:
            case 212:
            case 222:
            case 232:
            case 312:
            case 322:
            case 412:
            case 422:
            case 434:
            case 512:
            case 522:
            case 532:
            case 1112:
            case 1312:
            case 1512:
            case 2112:
            case 2312:
            case 2412:
            case 2712:
            case 3112:
            case 3122:
            case 3212:
            case 3312:
            case 3512:
            case 3612:
            case 5112:
            case 6112:
            case 6512:
                return job * 10000 + skillId;
        }
        return skillId;
    }

    public int getLimitBreak(MapleCharacter chra)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();

        int limitBreak = 999999;
        Equip weapon = (Equip) chra.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
        if (weapon != null)
        {
            limitBreak = ii.getLimitBreak(weapon.getItemId()) + weapon.getLimitBreak();

            Equip subweapon = (Equip) chra.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
            if ((subweapon != null) && (constants.ItemConstants.isWeapon(subweapon.getItemId())))
            {
                int subWeaponLB = ii.getLimitBreak(subweapon.getItemId()) + subweapon.getLimitBreak();
                if (subWeaponLB > limitBreak)
                {
                    limitBreak = subWeaponLB;
                }
            }
        }
        return limitBreak;
    }

    public void setStr(short str, MapleCharacter chra)
    {
        this.str = str;
        recalcLocalStats(chra);
    }

    public void setDex(short dex, MapleCharacter chra)
    {
        this.dex = dex;
        recalcLocalStats(chra);
    }

    public void setLuk(short luk, MapleCharacter chra)
    {
        this.luk = luk;
        recalcLocalStats(chra);
    }

    public void setInt(short int_, MapleCharacter chra)
    {
        this.int_ = int_;
        recalcLocalStats(chra);
    }

    public int getHealHp()
    {
        return Math.max(this.localmaxhp - this.hp, 0);
    }

    public int getHealMp(int job)
    {
        if (GameConstants.isNotMpJob(job))
        {
            return 0;
        }
        return Math.max(this.localmaxmp - this.mp, 0);
    }

    public void setInfo(int maxhp, int maxmp, int hp, int mp)
    {
        this.maxhp = maxhp;
        this.maxmp = maxmp;
        this.hp = hp;
        this.mp = mp;
    }

    public void setMaxHp(int hp, MapleCharacter chra)
    {
        this.maxhp = hp;
        recalcLocalStats(chra);
    }

    public void setMaxMp(int mp, MapleCharacter chra)
    {
        this.maxmp = mp;
        recalcLocalStats(chra);
    }

    public int getHp()
    {
        return this.hp;
    }

    public int getMp()
    {
        return this.mp;
    }

    public int getTotalDex()
    {
        return this.localdex;
    }

    public int getTotalInt()
    {
        return this.localint_;
    }

    public int getTotalStr()
    {
        return this.localstr;
    }

    public int getTotalLuk()
    {
        return this.localluk;
    }

    public int getTotalMagic()
    {
        return this.magic;
    }

    public int getSpeed()
    {
        return this.speed;
    }

    public int getJump()
    {
        return this.jump;
    }

    public int getTotalWatk()
    {
        return this.watk;
    }

    public int getHands()
    {
        return this.hands;
    }

    public float getCurrentMaxBaseDamage()
    {
        return this.localmaxbasedamage;
    }

    public float getCurrentMaxBasePVPDamage()
    {
        return this.localmaxbasepvpdamage;
    }

    public float getCurrentMaxBasePVPDamageL()
    {
        return this.localmaxbasepvpdamageL;
    }

    public int getCoolTimeR()
    {
        if (this.coolTimeR > 5)
        {
            return 5;
        }
        return this.coolTimeR;
    }

    public int getReduceCooltime()
    {
        if (this.reduceCooltime > 5)
        {
            return 5;
        }
        return this.reduceCooltime;
    }

    public int getAttackCount(int skillId)
    {
        if (this.add_skill_attackCount.containsKey(skillId))
        {
            return this.add_skill_attackCount.get(skillId);
        }
        return 0;
    }

    public int getMobCount(int skillId)
    {
        if (this.add_skill_targetPlus.containsKey(skillId))
        {
            return this.add_skill_targetPlus.get(skillId);
        }
        return 0;
    }

    public int getReduceCooltimeRate(int skillId)
    {
        if (this.add_skill_coolTimeR.containsKey(skillId))
        {
            return this.add_skill_coolTimeR.get(skillId);
        }
        return 0;
    }

    public int getIgnoreMobpdpR(int skillId)
    {
        if (this.add_skill_ignoreMobpdpR.containsKey(skillId))
        {
            return this.add_skill_ignoreMobpdpR.get(skillId) + this.percent_ignore_mob_def_rate;
        }
        return this.percent_ignore_mob_def_rate;
    }

    public int getDamageRate()
    {
        return this.percent_damage_rate;
    }

    public int getBossDamageRate()
    {
        return this.percent_boss_damage_rate;
    }

    public int getBossDamageRate(int skillId)
    {
        if (this.add_skill_bossDamageRate.containsKey(skillId))
        {
            return this.add_skill_bossDamageRate.get(skillId) + this.percent_boss_damage_rate;
        }
        return this.percent_boss_damage_rate;
    }

    public int getDuration(int skillId)
    {
        if (this.add_skill_duration.containsKey(skillId))
        {
            return this.add_skill_duration.get(skillId);
        }
        return 0;
    }

    public void addDotTime(int skillId, int val)
    {
        if ((skillId < 0) || (val <= 0))
        {
            return;
        }
        if (this.add_skill_dotTime.containsKey(skillId))
        {
            int oldval = this.add_skill_dotTime.get(skillId);
            this.add_skill_dotTime.put(skillId, oldval + val);
        }
        else
        {
            this.add_skill_dotTime.put(skillId, val);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\PlayerStats.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */