package client;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import database.DatabaseConnection;
import tools.Pair;
import tools.data.output.MaplePacketLittleEndianWriter;


public class MapleQuickSlot implements Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private final List<Pair<Integer, Integer>> quickslot;
    private boolean changed = false;

    public MapleQuickSlot()
    {
        this.quickslot = new ArrayList<>();
    }

    public MapleQuickSlot(List<Pair<Integer, Integer>> quickslots)
    {
        this.quickslot = quickslots;
    }

    public List<Pair<Integer, Integer>> Layout()
    {
        this.changed = true;
        return this.quickslot;
    }

    public void unchanged()
    {
        this.changed = false;
    }

    public void resetQuickSlot()
    {
        this.changed = true;
        this.quickslot.clear();
    }

    public void addQuickSlot(int index, int key)
    {
        this.changed = true;
        this.quickslot.add(new Pair(index, key));
    }

    public int getKeyByIndex(int index)
    {
        for (Pair<Integer, Integer> p : this.quickslot)
        {
            if (p.getLeft() == index)
            {
                return p.getRight();
            }
        }
        return -1;
    }

    public void writeData(MaplePacketLittleEndianWriter mplew)
    {
        mplew.write(this.quickslot.isEmpty() ? 0 : 1);
        if (this.quickslot.isEmpty())
        {
            return;
        }
        Collections.sort(this.quickslot, new QuickSlotComparator());
        for (Pair<Integer, Integer> qs : this.quickslot)
        {
            mplew.writeInt(qs.getRight());
        }
    }

    public void saveQuickSlots(int charid) throws SQLException
    {
        if (!this.changed)
        {
            return;
        }
        Connection con = DatabaseConnection.getConnection();
        PreparedStatement ps = con.prepareStatement("DELETE FROM quickslot WHERE characterid = ?");
        ps.setInt(1, charid);
        ps.execute();
        ps.close();
        if (this.quickslot.isEmpty())
        {
            return;
        }
        boolean first = true;
        StringBuilder query = new StringBuilder();
        for (Pair<Integer, Integer> q : this.quickslot)
        {
            if (first)
            {
                first = false;
                query.append("INSERT INTO quickslot VALUES (");
            }
            else
            {
                query.append(",(");
            }
            query.append("DEFAULT,");
            query.append(charid).append(",");
            query.append(q.getLeft().intValue()).append(",");
            query.append(q.getRight().intValue()).append(")");
        }
        ps = con.prepareStatement(query.toString());
        ps.execute();
        ps.close();
    }

    public static class QuickSlotComparator implements Comparator<Pair<Integer, Integer>>, Serializable
    {
        public int compare(Pair<Integer, Integer> p1, Pair<Integer, Integer> p2)
        {
            int val1index = p1.getLeft();
            int val2index = p2.getLeft();
            if (val1index > val2index) return 1;
            if (val1index == val2index)
            {
                return 0;
            }
            return -1;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleQuickSlot.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */