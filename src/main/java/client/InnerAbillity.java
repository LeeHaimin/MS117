package client;

import server.Randomizer;


public class InnerAbillity
{
    private static InnerAbillity instance = null;

    public static InnerAbillity getInstance()
    {
        if (instance == null)
        {
            instance = new InnerAbillity();
        }
        return instance;
    }

    public InnerSkillEntry renewSkill(int rank, int position, int itemId)
    {
        return renewSkill(rank, position, itemId, false);
    }

    public InnerSkillEntry renewSkill(int rank, int position, int itemId, boolean ultimateCirculatorPos)
    {
        if ((ultimateCirculatorPos) && (itemId == 2701000))
        {
            int randomSkill = constants.ItemConstants.getInnerSkillbyRank(3)[((int) Math.floor(Math.random() * constants.ItemConstants.getInnerSkillbyRank(rank).length))];
            Skill skill = SkillFactory.getSkill(randomSkill);
            if (skill == null)
            {
                return null;
            }

            int random = Randomizer.nextInt(100);
            int skillLevel;
            if (random < 38)
            {
                skillLevel = Randomizer.rand(skill.getMaxLevel() / 2, skill.getMaxLevel());
            }
            else
            {
                if (random < 70)
                {
                    skillLevel = Randomizer.rand(skill.getMaxLevel() / 3, skill.getMaxLevel() / 2);
                }
                else skillLevel = Randomizer.rand(skill.getMaxLevel() / 4, skill.getMaxLevel() / 3);
            }
            if (skillLevel > skill.getMaxLevel())
            {
                skillLevel = skill.getMaxLevel();
            }
            return new InnerSkillEntry(randomSkill, skillLevel, (byte) position, (byte) 3);
        }
        int circulatorRate = 0;
        if (itemId == -1)
        {
            circulatorRate = 10;
        }
        else if ((itemId == 2702000) || (itemId == 2702001))
        {
            circulatorRate = 20;
        }
        if (isSuccess(3 + circulatorRate))
        {
            rank = 1;
        }
        else if (isSuccess(2 + circulatorRate / 5))
        {
            rank = 2;
        }
        else if (isSuccess(1 + circulatorRate / 10))
        {
            rank = 3;
        }
        else
        {
            rank = 0;
        }


        if (((itemId == 2702000) || (itemId == 2702001)) && (rank == 3))
        {
            rank = 2;
        }
        int randomSkill = constants.ItemConstants.getInnerSkillbyRank(rank)[((int) Math.floor(Math.random() * constants.ItemConstants.getInnerSkillbyRank(rank).length))];
        Skill skill = SkillFactory.getSkill(randomSkill);
        if (skill == null)
        {
            return null;
        }

        int random = Randomizer.nextInt(100);
        int skillLevel;
        if (random < 3 + circulatorRate / 2)
        {
            skillLevel = Randomizer.rand(skill.getMaxLevel() / 2, skill.getMaxLevel());
        }
        else
        {
            if (random < circulatorRate)
            {
                skillLevel = Randomizer.rand(skill.getMaxLevel() / 3, skill.getMaxLevel() / 2);
            }
            else skillLevel = Randomizer.rand(skill.getMaxLevel() / 4, skill.getMaxLevel() / 3);
        }
        if (skillLevel > skill.getMaxLevel())
        {
            skillLevel = skill.getMaxLevel();
        }
        return new InnerSkillEntry(randomSkill, skillLevel, (byte) position, (byte) rank);
    }

    public boolean isSuccess(int rate)
    {
        return rate > Randomizer.nextInt(100);
    }

    public int getCirculatorRank(int itemId)
    {
        return itemId % 1000 / 100 + 1;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\InnerAbillity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */