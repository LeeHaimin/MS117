package client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import client.inventory.Equip;
import client.inventory.MapleInventoryType;
import constants.GameConstants;
import server.MapleItemInformationProvider;
import server.quest.MapleQuest;
import tools.Pair;
import tools.Triple;
import tools.data.output.MaplePacketLittleEndianWriter;

public final class MonsterBook implements java.io.Serializable
{
    private static final long serialVersionUID = 7179541993413738569L;
    private final Map<Integer, Integer> cards;
    private final ArrayList<Integer> cardItems = new ArrayList<>();
    private final Map<Integer, Pair<Integer, Boolean>> sets = new HashMap<>();
    private boolean changed = false;
    private int currentSet = -1;
    private int level = 0;
    private int setScore;
    private int finishedSets;

    public MonsterBook(Map<Integer, Integer> cards, MapleCharacter chr)
    {
        this.cards = cards;
        calculateItem();
        calculateScore();

        MapleQuestStatus stat = chr.getQuestNoAdd(MapleQuest.getInstance(122800));
        if ((stat != null) && (stat.getCustomData() != null))
        {
            this.currentSet = Integer.parseInt(stat.getCustomData());
            if ((!this.sets.containsKey(this.currentSet)) || (!this.sets.get(this.currentSet).right))
            {
                this.currentSet = -1;
            }
        }
        applyBook(chr, true);
    }

    public void calculateItem()
    {
        this.cardItems.clear();
        for (Map.Entry<Integer, Integer> s : this.cards.entrySet())
        {
            addCardItem(s.getKey(), s.getValue());
        }
    }

    public byte calculateScore()
    {
        byte returnval = 0;
        this.sets.clear();
        int oldLevel = this.level;
        int oldSetScore = this.setScore;
        this.setScore = 0;
        this.finishedSets = 0;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        for (int i : this.cardItems)
        {
            Integer x = ii.getSetId(i);
            if ((x != null) && (x > 0))
            {
                Triple<Integer, List<Integer>, List<Integer>> set = ii.getMonsterBookInfo(x);
                if (set != null)
                {
                    Integer localInteger1;
                    if (!this.sets.containsKey(x))
                    {
                        this.sets.put(x, new Pair(1, Boolean.FALSE));
                    }
                    else
                    {
                        Pair localPair = this.sets.get(x);
                        localInteger1 = (Integer) localPair.left;
                        Integer localInteger2 = (Integer) (localPair.left = (Integer) localPair.left + 1);
                    }
                    if (this.sets.get(x).left == set.mid.size())
                    {
                        this.sets.get(x).right = Boolean.TRUE;
                        this.setScore += set.left;
                        if (this.currentSet == -1)
                        {
                            this.currentSet = x;
                            returnval = 2;
                        }
                        this.finishedSets += 1;
                    }
                }
            }
        }
        this.level = 10;
        for (byte i = 0; i < 10; i = (byte) (i + 1))
        {
            if (GameConstants.getSetExpNeededForLevel(i) > this.setScore)
            {
                this.level = i;
                break;
            }
        }
        if (this.level > oldLevel)
        {
            returnval = 2;
        }
        else if (this.setScore > oldSetScore)
        {
            returnval = 1;
        }
        return returnval;
    }

    public void applyBook(MapleCharacter chr, boolean first_login)
    {
        if (GameConstants.GMS)
        {
            Equip item = (Equip) chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -55);
            if (item == null)
            {
                item = (Equip) MapleItemInformationProvider.getInstance().getEquipById(1172000);
                item.setPosition((short) -55);
            }
            modifyBook(item);
            if (first_login)
            {
                chr.getInventory(MapleInventoryType.EQUIPPED).addFromDB(item);
            }
            else
            {
                chr.forceReAddItem_Book(item, MapleInventoryType.EQUIPPED);
                chr.equipChanged();
            }
        }
    }

    public void addCardItem(int key, int value)
    {
        if (value >= 2)
        {
            Integer x = MapleItemInformationProvider.getInstance().getItemIdByMob(key);
            if ((x != null) && (x > 0))
            {
                this.cardItems.add(x.intValue());
            }
        }
    }

    public void modifyBook(Equip eq)
    {
        eq.setStr((short) this.level);
        eq.setDex((short) this.level);
        eq.setInt((short) this.level);
        eq.setLuk((short) this.level);
        eq.setPotential1(0);
        eq.setPotential2(0);
        eq.setPotential3(0);
        if (this.currentSet > -1)
        {
            Triple<Integer, List<Integer>, List<Integer>> set = MapleItemInformationProvider.getInstance().getMonsterBookInfo(this.currentSet);
            if (set != null)
            {
                for (int i = 0; i < set.right.size(); i++)
                {
                    if (i == 0)
                    {
                        eq.setPotential1(((Integer) ((List) set.right).get(i)).shortValue());
                    }
                    else if (i == 1)
                    {
                        eq.setPotential2(((Integer) ((List) set.right).get(i)).shortValue());
                    }
                    else if (i == 2)
                    {
                        eq.setPotential3(((Integer) ((List) set.right).get(i)).shortValue());
                        break;
                    }
                }
            }
            else
            {
                this.currentSet = -1;
            }
        }
    }

    public static MonsterBook loadCards(int charid, MapleCharacter chr) throws java.sql.SQLException
    {
        PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("SELECT * FROM monsterbook WHERE charid = ? ORDER BY cardid ASC");
        ps.setInt(1, charid);
        ResultSet rs = ps.executeQuery();
        Map<Integer, Integer> cards = new LinkedHashMap<>();


        while (rs.next())
        {
            cards.put(rs.getInt("cardid"), rs.getInt("level"));
        }
        rs.close();
        ps.close();
        return new MonsterBook(cards, chr);
    }

    public void writeCharInfoPacket(MaplePacketLittleEndianWriter mplew)
    {
        List<Integer> cardSize = new ArrayList(10);
        for (int i = 0; i < 10; i++)
        {
            cardSize.add(0);
        }
        for (int x : this.cardItems)
        {
            cardSize.set(0, cardSize.get(0) + 1);
            cardSize.set(x / 1000 % 10 + 1, cardSize.get(x / 1000 % 10 + 1) + 1);
        }
        for (int i : cardSize)
        {
            mplew.writeInt(i);
        }
        mplew.writeInt(this.setScore);
        mplew.writeInt(this.currentSet);
        mplew.writeInt(this.finishedSets);
    }

    public void writeFinished(MaplePacketLittleEndianWriter mplew)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        mplew.write(1);
        mplew.writeShort(this.cardItems.size());
        List<Integer> mbList = new ArrayList(ii.getMonsterBookList());
        java.util.Collections.sort(mbList);
        int fullCards = mbList.size() / 8 + (mbList.size() % 8 > 0 ? 1 : 0);
        mplew.writeShort(fullCards);

        for (int i = 0; i < fullCards; i++)
        {
            int currentMask = 1;
            int maskToWrite = 0;
            for (int y = i * 8; y < i * 8 + 8; y++)
            {
                if (mbList.size() <= y)
                {
                    break;
                }
                if (this.cardItems.contains(mbList.get(y)))
                {
                    maskToWrite |= currentMask;
                }
                currentMask *= 2;
            }
            mplew.write(maskToWrite);
        }

        int fullSize = this.cardItems.size() / 2 + (this.cardItems.size() % 2 > 0 ? 1 : 0);
        mplew.writeShort(fullSize);
        for (int i = 0; i < fullSize; i++)
        {
            mplew.write(i == this.cardItems.size() / 2 ? 1 : 17);
        }
    }

    public void writeUnfinished(MaplePacketLittleEndianWriter mplew)
    {
        mplew.write(0);
        mplew.writeShort(this.cardItems.size());
        for (Integer cardItem : this.cardItems)
        {
            int i = cardItem;
            mplew.writeShort(i % 10000);
            mplew.write(1);
        }
    }

    public int getSetScore()
    {
        return this.setScore;
    }

    public int getLevel()
    {
        return this.level;
    }

    public int getSet()
    {
        return this.currentSet;
    }

    public boolean changeSet(int c)
    {
        if ((this.sets.containsKey(c)) && this.sets.get(c).right)
        {
            this.currentSet = c;
            return true;
        }
        return false;
    }

    public void changed()
    {
        this.changed = true;
    }

    public Map<Integer, Integer> getCards()
    {
        return this.cards;
    }

    public int getSeen()
    {
        return this.cards.size();
    }

    public int getCaught()
    {
        int ret = 0;
        for (Integer integer : this.cards.values())
        {
            int i = integer;
            if (i >= 2)
            {
                ret++;
            }
        }
        return ret;
    }

    public int getLevelByCard(int cardid)
    {
        return this.cards.get(cardid) == null ? 0 : this.cards.get(cardid);
    }

    public void saveCards(int charid) throws java.sql.SQLException
    {
        if (!this.changed)
        {
            return;
        }
        Connection con = database.DatabaseConnection.getConnection();
        PreparedStatement ps = con.prepareStatement("DELETE FROM monsterbook WHERE charid = ?");
        ps.setInt(1, charid);
        ps.execute();
        ps.close();
        this.changed = false;
        if (this.cards.isEmpty())
        {
            return;
        }

        boolean first = true;
        StringBuilder query = new StringBuilder();

        for (Map.Entry<Integer, Integer> all : this.cards.entrySet())
        {
            if (first)
            {
                first = false;
                query.append("INSERT INTO monsterbook VALUES (DEFAULT,");
            }
            else
            {
                query.append(",(DEFAULT,");
            }
            query.append(charid);
            query.append(",");
            query.append(all.getKey());
            query.append(",");
            query.append(all.getValue());
            query.append(")");
        }
        ps = con.prepareStatement(query.toString());
        ps.execute();
        ps.close();
    }

    public boolean monsterCaught(MapleClient c, int cardid, String cardname)
    {
        if ((!this.cards.containsKey(cardid)) || (this.cards.get(cardid) < 2))
        {
            this.changed = true;
            c.getPlayer().dropMessage(-6, "Book entry updated - " + cardname);

            this.cards.put(cardid, 2);
            if (GameConstants.GMS)
            {
                if (c.getPlayer().getQuestStatus(50195) != 1)
                {
                    MapleQuest.getInstance(50195).forceStart(c.getPlayer(), 9010000, "1");
                }
                if (c.getPlayer().getQuestStatus(50196) != 1)
                {
                    MapleQuest.getInstance(50196).forceStart(c.getPlayer(), 9010000, "1");
                }
                addCardItem(cardid, 2);
                byte rr = calculateScore();
                if (rr > 0)
                {
                    if (c.getPlayer().getQuestStatus(50197) != 1)
                    {
                        MapleQuest.getInstance(50197).forceStart(c.getPlayer(), 9010000, "1");
                    }

                    if (rr > 1)
                    {
                        applyBook(c.getPlayer(), false);
                    }
                }
            }
            return true;
        }
        return false;
    }

    public void monsterSeen(MapleClient c, int cardid, String cardname)
    {
        if (this.cards.containsKey(cardid))
        {
            return;
        }
        this.changed = true;

        c.getPlayer().dropMessage(-6, "New book entry - " + cardname);
        this.cards.put(cardid, 1);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MonsterBook.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */