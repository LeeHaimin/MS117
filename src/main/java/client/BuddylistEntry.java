package client;


public class BuddylistEntry
{
    private final String name;
    private final int cid;
    private String group;
    private int channel;

    private boolean visible;

    public BuddylistEntry(String name, int characterId, String group, int channel, boolean visible)
    {
        this.name = name;
        this.cid = characterId;
        this.group = group;
        this.channel = channel;
        this.visible = visible;
    }


    public int getChannel()
    {
        return this.channel;
    }

    public void setChannel(int channel)
    {
        this.channel = channel;
    }

    public boolean isOnline()
    {
        return this.channel >= 0;
    }

    public void setOffline()
    {
        this.channel = -1;
    }

    public String getName()
    {
        return this.name;
    }

    public int getCharacterId()
    {
        return this.cid;
    }

    public boolean isVisible()
    {
        return this.visible;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public String getGroup()
    {
        return this.group;
    }

    public void setGroup(String g)
    {
        this.group = g;
    }

    public int hashCode()
    {
        int prime = 31;
        int result = 1;
        result = prime * result + this.cid;
        return result;
    }

    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        BuddylistEntry other = (BuddylistEntry) obj;
        return this.cid == other.cid;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\BuddylistEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */