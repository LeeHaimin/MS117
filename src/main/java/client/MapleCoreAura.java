package client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.DatabaseConnection;
import server.Randomizer;


public class MapleCoreAura
{
    private final int id;
    private int level;
    private int str;
    private int dex;
    private int int_;
    private int luk;
    private int watk;
    private int magic;
    private long expiration;

    public MapleCoreAura(int chrId)
    {
        this.id = chrId;
    }

    public MapleCoreAura(int chrId, int chrlevel)
    {
        this.id = chrId;
        this.level = chrlevel;
    }

    public static MapleCoreAura loadFromDb(int chrId)
    {
        return loadFromDb(chrId, -1);
    }

    public static MapleCoreAura loadFromDb(int chrId, int chrlevel)
    {
        try
        {
            MapleCoreAura ret = new MapleCoreAura(chrId);
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM character_coreauras WHERE characterid = ?");
            ps.setInt(1, chrId);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                return null;
            }
            ret.setLevel(chrlevel > 0 ? chrlevel : rs.getInt("level"));
            long expire = rs.getLong("expiredate");
            if (System.currentTimeMillis() > expire)
            {
                ret.resetCoreAura();
                ret.saveToDb();
            }
            else
            {
                ret.setStr(rs.getInt("str"));
                ret.setDex(rs.getInt("dex"));
                ret.setInt(rs.getInt("int"));
                ret.setLuk(rs.getInt("luk"));
                ret.setWatk(rs.getInt("watk"));
                ret.setMagic(rs.getInt("magic"));
                ret.setExpiration(expire);
            }
            rs.close();
            ps.close();
            return ret;
        }
        catch (SQLException ex)
        {
            System.err.println("加载龙的传人宝盒信息出错" + ex);
        }
        return null;
    }

    public void resetCoreAura()
    {
        this.str = 5;
        this.dex = 5;
        this.int_ = 5;
        this.luk = 5;
        this.watk = 0;
        this.magic = 0;
        this.expiration = (System.currentTimeMillis() + 86400000L);
    }

    public void saveToDb()
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE `character_coreauras` SET `level` = ?, `str` = ?, `dex` = ?, `int` = ?, `luk` = ?, `watk` = ?, " +
                    "`magic`" + " = ?, `expiredate` = ? WHERE `characterid` = ?");
            ps.setInt(1, this.level);
            ps.setInt(2, this.str);
            ps.setInt(3, this.dex);
            ps.setInt(4, this.int_);
            ps.setInt(5, this.luk);
            ps.setInt(6, this.watk);
            ps.setInt(7, this.magic);
            ps.setLong(8, this.expiration);
            ps.setInt(9, this.id);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException ex)
        {
            System.err.println("保存龙的传人宝盒出错" + ex);
        }
    }

    public static MapleCoreAura createCoreAura(int chrId, int chrlevel)
    {
        MapleCoreAura ret = new MapleCoreAura(chrId, chrlevel);
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM character_coreauras WHERE characterid = ?");
            ps.setInt(1, chrId);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                ps.close();
                rs.close();
                return loadFromDb(chrId, chrlevel);
            }

            ps.close();
            rs.close();

            ret.resetCoreAura();
            ps = con.prepareStatement("INSERT INTO `character_coreauras` (`characterid`, `level`, `str`, `dex`, `int`, `luk`, `watk`, `magic`, `expiredate`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, chrId);
            ps.setInt(2, chrlevel);
            ps.setInt(3, ret.getStr());
            ps.setInt(4, ret.getDex());
            ps.setInt(5, ret.getInt());
            ps.setInt(6, ret.getLuk());
            ps.setInt(7, ret.getWatk());
            ps.setInt(8, ret.getMagic());
            ps.setLong(9, ret.getExpiration());
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException ex)
        {
            System.err.println("创建龙的传人宝盒信息出错 " + ex);
            return null;
        }
        return ret;
    }

    public int getStr()
    {
        return this.str;
    }

    public void setStr(int str)
    {
        this.str = str;
    }

    public int getDex()
    {
        return this.dex;
    }

    public void setDex(int dex)
    {
        this.dex = dex;
    }

    public int getInt()
    {
        return this.int_;
    }

    public void setInt(int int_)
    {
        this.int_ = int_;
    }

    public int getLuk()
    {
        return this.luk;
    }

    public void setLuk(int luk)
    {
        this.luk = luk;
    }

    public int getWatk()
    {
        return this.watk;
    }

    public void setWatk(int watk)
    {
        this.watk = watk;
    }

    public int getMagic()
    {
        return this.magic;
    }

    public void setMagic(int magic)
    {
        this.magic = magic;
    }

    public long getExpiration()
    {
        return this.expiration;
    }

    public void setExpiration(long expire)
    {
        this.expiration = expire;
    }

    public void randomCoreAura(int type)
    {
        int max = type == 2 ? 15 : type == 3 ? 20 : type == 4 ? 25 : Randomizer.nextBoolean() ? 15 : type == 2 ? 20 : type == 3 ? 25 : type == 4 ? 32 : 10;
        int min = Randomizer.nextBoolean() ? 5 : 1;
        this.str = Randomizer.rand(min, max);
        this.dex = Randomizer.rand(min, max);
        this.int_ = Randomizer.rand(min, max);
        this.luk = Randomizer.rand(min, max);
        if (Randomizer.nextInt(1000) == 1)
        {
            this.watk = Randomizer.rand(10, 32);
        }
        else if (Randomizer.nextInt(500) == 1)
        {
            this.watk = Randomizer.rand(10, 25);
        }
        else if (Randomizer.nextInt(200) == 1)
        {
            this.watk = Randomizer.rand(5, 20);
        }
        else if (Randomizer.nextInt(100) == 1)
        {
            this.watk = Randomizer.rand(5, 15);
        }
        else
        {
            this.watk = Randomizer.rand(0, 15);
        }
        if (Randomizer.nextInt(1000) == 1)
        {
            this.magic = Randomizer.rand(10, 32);
        }
        else if (Randomizer.nextInt(500) == 1)
        {
            this.magic = Randomizer.rand(10, 25);
        }
        else if (Randomizer.nextInt(200) == 1)
        {
            this.magic = Randomizer.rand(5, 20);
        }
        else if (Randomizer.nextInt(100) == 1)
        {
            this.magic = Randomizer.rand(5, 15);
        }
        else
        {
            this.magic = Randomizer.rand(0, 15);
        }
    }

    public int getId()
    {
        return this.id;
    }

    public int getLevel()
    {
        return this.level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    public int getCoreAuraLevel()
    {
        if ((this.level >= 30) && (this.level < 70)) return 1;
        if ((this.level >= 70) && (this.level < 120)) return 2;
        if ((this.level >= 120) && (this.level < 160)) return 3;
        if (this.level >= 160)
        {
            return 4;
        }
        return 1;
    }

    public int getTotal()
    {
        return this.str + this.dex + this.int_ + this.luk + this.watk + this.magic;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleCoreAura.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */