package client;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class LoginCrypto
{
    protected static final int extralength = 6;
    private static final String[] Alphabet = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private static final String[] Number = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private static final Random rand = new Random();

    public static String Generate_13DigitAsiasoftPassport()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(Alphabet[rand.nextInt(Alphabet.length)]);
        for (int i = 0; i < 11; i++)
        {
            sb.append(Number[rand.nextInt(Number.length)]);
        }
        sb.append(Alphabet[rand.nextInt(Alphabet.length)]);
        return sb.toString();
    }

    public static boolean checkSha1Hash(String hash, String password)
    {
        return hash.equals(hexSha1(password));
    }

    public static String hexSha1(String in)
    {
        return hashWithDigest(in, "SHA-1");
    }

    private static String hashWithDigest(String in, String digest)
    {
        try
        {
            MessageDigest Digester = MessageDigest.getInstance(digest);
            Digester.update(in.getBytes(StandardCharsets.UTF_8), 0, in.length());
            byte[] sha1Hash = Digester.digest();
            return toSimpleHexString(sha1Hash);
        }
        catch (NoSuchAlgorithmException ex)
        {
            throw new RuntimeException("Hashing the password failed", ex);
        }
    }

    private static String toSimpleHexString(byte[] bytes)
    {
        return tools.HexTool.toString(bytes).replace(" ", "").toLowerCase();
    }

    public static boolean checkSaltedSha512Hash(String hash, String password, String salt)
    {
        return hash.equals(makeSaltedSha512Hash(password, salt));
    }

    public static String makeSaltedSha512Hash(String password, String salt)
    {
        return hexSha512(password + salt);
    }

    public static String hexSha512(String in)
    {
        return hashWithDigest(in, "SHA-512");
    }

    public static String makeSalt()
    {
        byte[] salt = new byte[16];
        rand.nextBytes(salt);
        return toSimpleHexString(salt);
    }

    public static String rand_s(String in)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++)
        {
            sb.append(rand.nextBoolean() ? Alphabet[rand.nextInt(Alphabet.length)] : Number[rand.nextInt(Number.length)]);
        }
        return sb.toString() + in;
    }

    public static String rand_r(String in)
    {
        return in.substring(6, 134);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\LoginCrypto.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */