package client;

public class MapleCoolDownValueHolder
{
    public final int skillId;
    public final long startTime;
    public final long length;

    public MapleCoolDownValueHolder(int skillId, long startTime, long length)
    {
        this.skillId = skillId;
        this.startTime = startTime;
        this.length = length;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleCoolDownValueHolder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */