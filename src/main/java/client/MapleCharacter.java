package client;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.anticheat.CheatTracker;
import client.anticheat.ReportType;
import client.inventory.Equip;
import client.inventory.ImpFlag;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.ItemLoader;
import client.inventory.MapleImp;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryIdentifier;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePet;
import client.inventory.MapleRing;
import client.messages.PlayerGMRank;
import client.status.MonsterStatus;
import client.status.MonsterStatusEffect;
import constants.BattleConstants;
import constants.GameConstants;
import constants.ItemConstants;
import database.DatabaseConnection;
import handling.channel.ChannelServer;
import handling.login.JobType;
import handling.world.CharacterTransfer;
import handling.world.PlayerBuffValueHolder;
import handling.world.family.MapleFamily;
import handling.world.family.MapleFamilyBuff;
import handling.world.family.MapleFamilyCharacter;
import handling.world.guild.MapleGuild;
import handling.world.guild.MapleGuildCharacter;
import handling.world.messenger.MapleMessenger;
import handling.world.messenger.MapleMessengerCharacter;
import handling.world.party.MapleParty;
import handling.world.party.MaplePartyCharacter;
import handling.world.sidekick.MapleSidekick;
import server.MapleCarnivalChallenge;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.MaplePortal;
import server.MapleStatEffect;
import server.Randomizer;
import server.ServerProperties;
import server.StructSetItemStat;
import server.Timer;
import server.life.MapleMonster;
import server.maps.MapleDoor;
import server.maps.MapleMap;
import server.maps.MapleMapEffect;
import server.maps.MapleMapObject;
import server.maps.MapleSummon;
import server.maps.SavedLocationType;
import server.maps.events.Event_PyramidSubway;
import server.quest.MapleQuest;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.Triple;
import tools.packet.BuffPacket;
import tools.packet.MobPacket;
import tools.packet.UIPacket;

public class MapleCharacter extends server.maps.AnimatedMapleMapObject implements java.io.Serializable
{
    public static final double MIN_VIEW_RANGE_SQ = 480000.0D;
    public static final double MAX_VIEW_RANGE_SQ = 786432.0D;
    private static final Logger log = Logger.getLogger(MapleCharacter.class);
    private static final long serialVersionUID = 845748950829L;
    private final AtomicLong exp = new AtomicLong();
    private final MaplePet[] spawnPets;
    private final Map<MapleQuest, MapleQuestStatus> quests;
    private final Map<Skill, SkillEntry> skills;
    private final PlayerStats stats;
    private final MapleCharacterCards characterCard;
    private final EnumMap<MapleTraitType, MapleTrait> traits;
    //    public transient List<MapleSummon> summons;
    public transient List<MapleSummon> summons;
    private byte guildrank = 5;
    private byte allianceRank = 5;
    private int guildid = 0;
    private int rank = 1;
    private int rankMove = 0;
    private int jobRank = 1;
    private int jobRankMove = 0;
    private int guildContribution = 0;
    private int[] remainingSp = new int[10];
    private SkillMacro[] skillMacros = new SkillMacro[5];
    private Battler[] battlers = new Battler[6];
    private transient Event_PyramidSubway pyramidSubway = null;
    private transient List<Integer> pendingExpiration = null;
    private transient Map<Skill, SkillEntry> pendingSkills = null;
    private int aranCombo = 0;
    private boolean isbanned = false;
    private String name;
    private String chalktext;
    private String BlessOfFairy_Origin;
    private String BlessOfEmpress_Origin;
    private String teleportname;
    private long lastComboTime;
    private long lastfametime;
    private long keydown_skill;
    private long nextConsume;
    private long pqStartTime;
    private long lastDragonBloodTime;
    private long lastBerserkTime;
    private long lastRecoveryTime;
    private long lastSummonTime;
    private long mapChangeTime;
    private long lastFishingTime;
    private long lastFairyTime;
    private long lastHPTime;
    private long lastMPTime;
    private long lastFamiliarEffectTime;
    private long lastDOTTime;
    private long lastExpirationTime;
    private long lastBlessOfDarknessTime;
    private long lastRecoveryTimeEM;
    private byte gmLevel;
    private byte gender;
    private byte initialSpawnPoint;
    private byte skinColor;
    private byte world;
    private byte fairyExp;
    private byte subcategory;
    private short level;
    private short mulung_energy;
    private short availableCP;
    private short fatigue;
    private short totalCP;
    private short hpApUsed;
    private short job;
    private short remainingAp;
    private short scrolledPosition;
    private int accountid;
    private int id;
    private int meso;
    private int hair;
    private int face;
    private int mapid;
    private int fame;
    private int pvpExp;
    private int pvpPoints;
    private int totalWins;
    private int totalLosses;
    private int fallcounter;
    private int maplepoints;
    private int acash;
    private int chair;
    private int itemEffect;
    private int points;
    private int vpoints;
    private int marriageId;
    private int marriageItemId;
    private int dotHP;
    private int currentrep;
    private int totalrep;
    private int coconutteam;
    private int followid;
    private int gachexp;
    private int challenge;

    public int getBuffSource(MapleBuffStat stat)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return -1;
        }
        return mbsvh.effect.getSourceId();
    }

    private Point old;
    private MonsterFamiliar summonedFamiliar;
    private int[] wishlist;
    private int[] rocks;
    private int[] savedLocations;
    private int[] regrocks;
    private int[] hyperrocks;
    private transient java.util.concurrent.atomic.AtomicInteger inst;
    private transient java.util.concurrent.atomic.AtomicInteger insd;
    private transient List<server.movement.LifeMovementFragment> lastres;
    private List<Integer> lastmonthfameids;
    private List<Integer> lastmonthbattleids;
    private List<Integer> extendedSlots;
    private List<MapleDoor> doors;
    private List<server.maps.MechDoor> mechDoors;
    private List<server.shop.MapleShopItem> rebuy;
    private MapleImp[] imps;
    private transient Set<MapleMonster> controlled;
    private transient Set<MapleMapObject> visibleMapObjects;
    private transient ReentrantReadWriteLock visibleMapObjectsLock;
    private transient ReentrantReadWriteLock summonsLock;
    private transient ReentrantReadWriteLock controlledLock;
    private transient client.inventory.MapleAndroid android;
    private Map<Integer, String> questinfo;
    private Map<String, String> keyValue;
    private InnerSkillEntry[] innerSkills;
    private transient ArrayList<Pair<MapleBuffStat, MapleBuffStatValueHolder>> effects;
    private transient Map<Integer, MapleCoolDownValueHolder> coolDowns;
    private transient Map<MapleDisease, MapleDiseaseValueHolder> diseases;
    private Map<ReportType, Integer> reports;
    private server.cashshop.CashShop cs;
    private transient java.util.Deque<MapleCarnivalChallenge> pendingCarnivalRequests;
    private transient server.MapleCarnivalParty carnivalParty;
    private BuddyList buddylist;
    private MonsterBook monsterbook;
    private transient CheatTracker anticheat;
    private transient MapleLieDetector antiMacro;
    private MapleClient client;
    private transient MapleParty party;
    private transient MapleMap map;
    private transient server.shop.MapleShop shop;
    private transient server.maps.MapleDragon dragon;
    private transient server.maps.MapleExtractor extractor;
    private transient RockPaperScissors rps;
    private MapleSidekick sidekick;
    private Map<Integer, MonsterFamiliar> familiars;
    private server.MapleStorage storage;
    private transient server.MapleTrade trade;
    private client.inventory.MapleMount mount;
    private List<Integer> finishedAchievements;
    private MapleMessenger messenger;
    private byte[] petStore;
    private transient server.shops.IMaplePlayerShop playerShop;
    private boolean invincible;
    private boolean canTalk;
    private boolean followinitiator;
    private boolean followon;
    private boolean smega;
    private boolean hasSummon;
    private MapleGuildCharacter mgc;
    private MapleFamilyCharacter mfc;
    private transient scripting.event.EventInstanceManager eventInstance;
    private MapleInventory[] inventory;
    private ArrayList<Battler> boxed;
    private MapleKeyLayout keylayout;
    private MapleQuickSlot quickslot;
    private transient ScheduledFuture<?> mapTimeLimitTask;
    private transient Map<Integer, Integer> linkMobs;
    private transient server.PokemonBattle battle;
    private boolean changed_wishlist;
    private boolean changed_trocklocations;
    private boolean changed_skillmacros;
    private boolean changed_achievements;
    private boolean changed_savedlocations;
    private boolean changed_pokemon;
    private boolean changed_questinfo;
    private boolean changed_skills;
    private boolean changed_reports;
    private boolean changed_extendedSlots;
    private boolean changed_innerSkills;
    private boolean changed_keyValue;
    private int decorate;
    private int vip;
    private Timestamp viptime;
    private int titleEffect;
    private int beans;
    private int warning;
    private int dollars;
    private int shareLots;
    private int reborns;
    private int reborns1;
    private int reborns2;
    private int reborns3;
    private int apstorage;
    private int honorLevel;

    public boolean isHidden()
    {
        return getBuffSource(MapleBuffStat.隐身术) / 1000000 == 9;
    }

    private int honorExp;
    private Timestamp createDate;
    private int love;
    private long lastlovetime;
    private Map<Integer, Long> lastdayloveids;
    private int playerPoints;
    private int playerEnergy;
    private transient MaplePvpStats pvpStats;
    private int pvpDeaths;
    private int pvpKills;
    private int pvpVictory;
    private client.inventory.MaplePotionPot potionPot;
    private MapleCoreAura coreAura;
    private boolean isSaveing;
    private PlayerSpecialStats specialStats;

    private MapleCharacter(boolean ChannelServer)
    {
        setStance(0);
        setPosition(new Point(0, 0));
        this.inventory = new MapleInventory[MapleInventoryType.values().length];
        for (MapleInventoryType type : MapleInventoryType.values())
        {
            this.inventory[type.ordinal()] = new MapleInventory(type);
        }
        this.keyValue = new LinkedHashMap<>();
        this.questinfo = new LinkedHashMap<>();
        this.quests = new LinkedHashMap<>();
        this.skills = new LinkedHashMap<>();
        this.innerSkills = new InnerSkillEntry[3];
        this.stats = new PlayerStats();
        this.characterCard = new MapleCharacterCards();
        for (int i = 0; i < this.remainingSp.length; i++)
        {
            this.remainingSp[i] = 0;
        }
        this.traits = new EnumMap(MapleTraitType.class);
        for (MapleTraitType t : MapleTraitType.values())
        {
            this.traits.put(t, new MapleTrait(t));
        }
        this.spawnPets = new MaplePet[3];
        this.specialStats = new PlayerSpecialStats();
        this.specialStats.resetSpecialStats();
        if (ChannelServer)
        {
            this.isSaveing = false;
            this.changed_reports = false;
            this.changed_skills = false;
            this.changed_achievements = false;
            this.changed_wishlist = false;
            this.changed_trocklocations = false;
            this.changed_skillmacros = false;
            this.changed_savedlocations = false;
            this.changed_pokemon = false;
            this.changed_extendedSlots = false;
            this.changed_questinfo = false;
            this.changed_innerSkills = false;
            this.changed_keyValue = false;
            this.scrolledPosition = 0;
            this.lastComboTime = 0L;
            this.mulung_energy = 0;
            this.aranCombo = 0;
            this.keydown_skill = 0L;
            this.nextConsume = 0L;
            this.pqStartTime = 0L;
            this.fairyExp = 0;
            this.mapChangeTime = 0L;
            this.lastRecoveryTime = 0L;
            this.lastDragonBloodTime = 0L;
            this.lastBerserkTime = 0L;
            this.lastFishingTime = 0L;
            this.lastFairyTime = 0L;
            this.lastHPTime = 0L;
            this.lastMPTime = 0L;
            this.lastFamiliarEffectTime = 0L;
            this.lastExpirationTime = 0L;
            this.lastBlessOfDarknessTime = 0L;
            this.lastRecoveryTimeEM = 0L;
            this.old = new Point(0, 0);
            this.coconutteam = 0;
            this.followid = 0;
            this.marriageItemId = 0;
            this.fallcounter = 0;
            this.challenge = 0;
            this.dotHP = 0;
            this.lastSummonTime = 0L;
            this.hasSummon = false;
            this.invincible = false;
            this.canTalk = true;
            this.followinitiator = false;
            this.followon = false;
            this.rebuy = new ArrayList<>();
            this.linkMobs = new HashMap<>();
            this.finishedAchievements = new ArrayList<>();
            this.reports = new EnumMap<>(ReportType.class);
            this.teleportname = "";
            this.smega = true;
            this.petStore = new byte[3];
            for (int i = 0; i < this.petStore.length; i++)
            {
                this.petStore[i] = -1;
            }
            this.wishlist = new int[12];
            this.rocks = new int[10];
            this.regrocks = new int[5];
            this.hyperrocks = new int[13];
            this.imps = new MapleImp[3];
            this.boxed = new ArrayList<>();
            this.familiars = new LinkedHashMap<>();
            this.extendedSlots = new ArrayList<>();
            this.effects = new ArrayList<>();
            this.coolDowns = new LinkedHashMap<>();
            this.diseases = new tools.ConcurrentEnumMap(MapleDisease.class);
            this.inst = new AtomicInteger(0);
            this.insd = new AtomicInteger(-1);
            this.keylayout = new MapleKeyLayout();
            this.quickslot = new MapleQuickSlot();
            this.doors = new ArrayList<>();
            this.mechDoors = new ArrayList<>();
            this.controlled = new LinkedHashSet<>();
            this.controlledLock = new ReentrantReadWriteLock();
            this.summons = new LinkedList<>();
            this.summonsLock = new ReentrantReadWriteLock();
            this.visibleMapObjects = new LinkedHashSet<>();
            this.visibleMapObjectsLock = new ReentrantReadWriteLock();
            this.pendingCarnivalRequests = new LinkedList<>();

            this.savedLocations = new int[SavedLocationType.values().length];
            for (int i = 0; i < SavedLocationType.values().length; i++)
            {
                this.savedLocations[i] = -1;
            }
        }
    }

    public static MapleCharacter getDefault(MapleClient client, JobType type)
    {
        MapleCharacter ret = new MapleCharacter(false);
        ret.client = client;
        ret.map = null;
        ret.exp.set(0L);
        ret.gmLevel = 0;
        ret.job = ((short) type.jobId);
        ret.meso = 0;
        ret.level = 1;
        ret.remainingAp = 0;
        ret.fame = 0;
        ret.love = 0;
        ret.accountid = client.getAccID();
        ret.buddylist = new BuddyList((byte) 20);

        ret.stats.str = 12;
        ret.stats.dex = 5;
        ret.stats.int_ = 4;
        ret.stats.luk = 4;
        ret.stats.maxhp = 50;
        ret.stats.hp = 50;
        ret.stats.maxmp = 50;
        ret.stats.mp = 50;
        ret.gachexp = 0;
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM accounts WHERE id = ?");
            ps.setInt(1, ret.accountid);
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                ret.client.setAccountName(rs.getString("name"));
                ret.acash = rs.getInt("ACash");
                ret.maplepoints = rs.getInt("mPoints");
                ret.points = rs.getInt("points");
                ret.vpoints = rs.getInt("vpoints");
            }
            rs.close();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("Error getting character default" + e);
        }
        return ret;
    }

    public static MapleCharacter ReconstructChr(CharacterTransfer ct, MapleClient client, boolean isChannel)
    {
        MapleCharacter ret = new MapleCharacter(true);
        ret.client = client;
        if (!isChannel)
        {
            ret.client.setChannel(ct.channel);
        }
        ret.id = ct.characterid;
        ret.name = ct.name;
        ret.level = ct.level;
        ret.fame = ct.fame;
        ret.love = ct.love;

        ret.stats.str = ct.str;
        ret.stats.dex = ct.dex;
        ret.stats.int_ = ct.int_;
        ret.stats.luk = ct.luk;
        ret.stats.maxhp = ct.maxhp;
        ret.stats.maxmp = ct.maxmp;
        ret.stats.hp = ct.hp;
        ret.stats.mp = ct.mp;

        ret.characterCard.setCards(ct.cardsInfo);

        ret.chalktext = ct.chalkboard;
        ret.gmLevel = ct.gmLevel;
        ret.exp.set(ret.level > ret.getMaxLevelForSever() ? 0L : ct.exp);
        ret.hpApUsed = ct.hpApUsed;
        ret.remainingSp = ct.remainingSp;
        ret.remainingAp = ct.remainingAp;
        ret.meso = ct.meso;
        ret.skinColor = ct.skinColor;
        ret.gender = ct.gender;
        ret.job = ct.job;
        ret.hair = ct.hair;
        ret.face = ct.face;
        ret.accountid = ct.accountid;
        ret.totalWins = ct.totalWins;
        ret.totalLosses = ct.totalLosses;
        client.setAccID(ct.accountid);
        ret.mapid = ct.mapid;
        ret.initialSpawnPoint = ct.initialSpawnPoint;
        ret.world = ct.world;
        ret.guildid = ct.guildid;
        ret.guildrank = ct.guildrank;
        ret.guildContribution = ct.guildContribution;
        ret.allianceRank = ct.alliancerank;
        ret.points = ct.points;
        ret.vpoints = ct.vpoints;
        ret.fairyExp = ct.fairyExp;
        ret.marriageId = ct.marriageId;
        ret.currentrep = ct.currentrep;
        ret.totalrep = ct.totalrep;
        ret.gachexp = ct.gachexp;
        ret.pvpExp = ct.pvpExp;
        ret.pvpPoints = ct.pvpPoints;
        ret.decorate = ct.decorate;
        ret.beans = ct.beans;
        ret.warning = ct.warning;

        ret.dollars = ct.dollars;
        ret.shareLots = ct.shareLots;

        ret.reborns = ct.reborns;
        ret.reborns1 = ct.reborns1;
        ret.reborns2 = ct.reborns2;
        ret.reborns3 = ct.reborns3;
        ret.apstorage = ct.apstorage;

        ret.honorLevel = ct.honorLevel;
        ret.honorExp = ct.honorExp;

        ret.vip = ct.vip;
        ret.viptime = ct.viptime;
        ret.playerPoints = ct.playerPoints;
        ret.playerEnergy = ct.playerEnergy;
        ret.pvpDeaths = ct.pvpDeaths;
        ret.pvpKills = ct.pvpKills;
        ret.pvpVictory = ct.pvpVictory;
        ret.makeMFC(ct.familyid, ct.seniorid, ct.junior1, ct.junior2);
        if (ret.guildid > 0)
        {
            ret.mgc = new MapleGuildCharacter(ret);
        }
        ret.fatigue = ct.fatigue;
        ret.buddylist = new BuddyList(ct.buddysize);
        ret.subcategory = ct.subcategory;

        if (ct.sidekick > 0)
        {
            ret.sidekick = handling.world.WorldSidekickService.getInstance().getSidekick(ct.sidekick);
        }

        if (isChannel)
        {
            server.maps.MapleMapFactory mapFactory = ChannelServer.getInstance(client.getChannel()).getMapFactory();
            ret.map = mapFactory.getMap(ret.mapid);
            if (ret.map == null)
            {
                ret.map = mapFactory.getMap(100000000);
            }
            else if ((ret.map.getForcedReturnId() != 999999999) && (ret.map.getForcedReturnMap() != null))
            {
                ret.map = ret.map.getForcedReturnMap();
            }

            MaplePortal portal = ret.map.getPortal(ret.initialSpawnPoint);
            if (portal == null)
            {
                portal = ret.map.getPortal(0);
                ret.initialSpawnPoint = 0;
            }
            ret.setPosition(portal.getPosition());

            int messengerid = ct.messengerid;
            if (messengerid > 0)
            {
                ret.messenger = handling.world.WorldMessengerService.getInstance().getMessenger(messengerid);
            }
        }
        else
        {
            ret.messenger = null;
        }
        int partyid = ct.partyid;
        if (partyid >= 0)
        {
            MapleParty party = handling.world.WrodlPartyService.getInstance().getParty(partyid);
            if ((party != null) && (party.getMemberById(ret.id) != null))
            {
                ret.party = party;
            }
        }


        for (Map.Entry<Integer, Object> qs : ct.Quest.entrySet())
        {
            MapleQuestStatus queststatus_from = (MapleQuestStatus) qs.getValue();
            queststatus_from.setQuest(qs.getKey());
            ret.quests.put(queststatus_from.getQuest(), queststatus_from);
        }
        for (Map.Entry<Integer, SkillEntry> qs : ct.Skills.entrySet())
        {
            ret.skills.put(SkillFactory.getSkill(qs.getKey()), qs.getValue());
        }
        for (Integer zz : ct.finishedAchievements)
        {
            ret.finishedAchievements.add(zz);
        }
        for (Object zz : ct.boxed)
        {
            Battler zzz = (Battler) zz;
            zzz.setStats();
            ret.boxed.add(zzz);
        }
        for (Map.Entry<MapleTraitType, Integer> t : ct.traits.entrySet())
        {
            ret.traits.get(t.getKey()).setExp(t.getValue());
        }
        for (Entry<Byte, Integer> byteIntegerEntry : ct.reports.entrySet())
        {
            Iterator<Entry<Byte, Integer>> messengerid = (Iterator<Entry<Byte, Integer>>) byteIntegerEntry;
            if (messengerid.hasNext())
            {
                Entry qs = messengerid.next();
                ret.reports.put(ReportType.getById((Byte) qs.getKey()), (Integer) qs.getValue());
            }
        }
        ret.innerSkills = ((InnerSkillEntry[]) ct.innerSkills);
        ret.monsterbook = new MonsterBook(ct.mbook, ret);
        ret.inventory = ((MapleInventory[]) ct.inventorys);
        ret.BlessOfFairy_Origin = ct.BlessOfFairy;
        ret.BlessOfEmpress_Origin = ct.BlessOfEmpress;
        ret.skillMacros = ((SkillMacro[]) ct.skillmacro);
        ret.battlers = ((Battler[]) ct.battlers);
        Battler[] messengerid = ret.battlers;
//        Map.Entry<Byte, Integer>
        int qs = messengerid.length;
        for (Battler b : messengerid)
        {
            if (b != null)
            {
                b.setStats();
            }
        }
        ret.petStore = ct.petStore;
        ret.keylayout = new MapleKeyLayout(ct.keymap);
        ret.quickslot = new MapleQuickSlot(ct.quickslot);
        ret.keyValue = ct.KeyValue;
        ret.questinfo = ct.InfoQuest;
        ret.familiars = ct.familiars;
        ret.savedLocations = ct.savedlocation;
        ret.wishlist = ct.wishlist;
        ret.rocks = ct.rocks;
        ret.regrocks = ct.regrocks;
        ret.hyperrocks = ct.hyperrocks;
        ret.buddylist.loadFromTransfer(ct.buddies);


        ret.keydown_skill = 0L;
        ret.lastfametime = ct.lastfametime;
        ret.lastmonthfameids = ct.famedcharacters;
        ret.lastmonthbattleids = ct.battledaccs;
        ret.extendedSlots = ct.extendedSlots;
        ret.lastlovetime = ct.lastLoveTime;
        ret.lastdayloveids = ct.loveCharacters;
        ret.storage = ((server.MapleStorage) ct.storage);
        ret.pvpStats = ((MaplePvpStats) ct.pvpStats);
        ret.potionPot = ((client.inventory.MaplePotionPot) ct.potionPot);
        ret.coreAura = ((MapleCoreAura) ct.coreAura);
        ret.specialStats = ((PlayerSpecialStats) ct.SpecialStats);
        ret.cs = ((server.cashshop.CashShop) ct.cs);
        client.setAccountName(ct.accountname);
        ret.acash = ct.ACash;
        ret.maplepoints = ct.MaplePoints;
        ret.imps = ct.imps;
        ret.anticheat = ((CheatTracker) ct.anticheat);
        ret.anticheat.start(ret);
        ret.antiMacro = ((MapleLieDetector) ct.antiMacro);
        ret.rebuy = ct.rebuy;
        ret.mount = new client.inventory.MapleMount(ret, ct.mount_itemid, PlayerStats.getSkillByJob(1004, ret.job), ct.mount_Fatigue, ct.mount_level, ct.mount_exp);
        ret.stats.recalcLocalStats(true, ret);
        client.setTempIP(ct.tempIP);

        return ret;
    }

    public int getMaxLevelForSever()
    {
        if (((GameConstants.is炎术士(this.job)) || (GameConstants.is夜行者(this.job))) && (!isIntern()))
        {
            return ServerProperties.getMaxCygnusLevel();
        }
        return ServerProperties.getMaxLevel();
    }

    public void makeMFC(int familyid, int seniorid, int junior1, int junior2)
    {
        if (familyid > 0)
        {
            MapleFamily f = handling.world.WorldFamilyService.getInstance().getFamily(familyid);
            if (f == null)
            {
                this.mfc = null;
            }
            else
            {
                this.mfc = f.getMFC(this.id);
                if (this.mfc == null)
                {
                    this.mfc = f.addFamilyMemberInfo(this, seniorid, junior1, junior2);
                }
                if (this.mfc.getSeniorId() != seniorid)
                {
                    this.mfc.setSeniorId(seniorid);
                }
                if (this.mfc.getJunior1() != junior1)
                {
                    this.mfc.setJunior1(junior1);
                }
                if (this.mfc.getJunior2() != junior2)
                {
                    this.mfc.setJunior2(junior2);
                }
            }
        }
        else
        {
            this.mfc = null;
        }
    }

    public boolean isIntern()
    {
        return this.gmLevel >= PlayerGMRank.INTERN.getLevel();
    }

    public static MapleCharacter loadCharFromDB(int charid, MapleClient client, boolean channelserver)
    {
        return loadCharFromDB(charid, client, channelserver, null);
    }

    public static MapleCharacter loadCharFromDB(int charid, MapleClient client, boolean channelserver, Map<Integer, CardData> cads)
    {
        MapleCharacter ret = new MapleCharacter(channelserver);
        ret.client = client;
        ret.id = charid;

        Connection con = DatabaseConnection.getConnection();
        PreparedStatement ps = null;

        ResultSet rs = null;
        try
        {
            ps = con.prepareStatement("SELECT * FROM characters WHERE id = ?");
            ps.setInt(1, charid);
            rs = ps.executeQuery();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                throw new RuntimeException("加载角色失败原因(角色没有找到).");
            }
            ret.name = rs.getString("name");
            ret.level = rs.getShort("level");
            ret.fame = rs.getInt("fame");
            ret.love = rs.getInt("love");

            ret.stats.str = rs.getShort("str");
            ret.stats.dex = rs.getShort("dex");
            ret.stats.int_ = rs.getShort("int");
            ret.stats.luk = rs.getShort("luk");
            ret.stats.maxhp = rs.getInt("maxhp");
            ret.stats.maxmp = rs.getInt("maxmp");
            ret.stats.hp = rs.getInt("hp");
            ret.stats.mp = rs.getInt("mp");
            ret.job = rs.getShort("job");
            ret.gmLevel = rs.getByte("gm");
            ret.exp.set(ret.level >= ret.getMaxLevelForSever() ? 0L : rs.getLong("exp"));
            ret.hpApUsed = rs.getShort("hpApUsed");
            String[] sp = rs.getString("sp").split(",");
            for (int i = 0; i < ret.remainingSp.length; i++)
            {
                ret.remainingSp[i] = Integer.parseInt(sp[i]);
            }
            ret.remainingAp = rs.getShort("ap");
            ret.meso = rs.getInt("meso");
            ret.skinColor = rs.getByte("skincolor");
            ret.gender = rs.getByte("gender");

            ret.hair = rs.getInt("hair");
            ret.face = rs.getInt("face");
            ret.accountid = rs.getInt("accountid");
            if (client != null)
            {
                client.setAccID(ret.accountid);
            }
            ret.mapid = rs.getInt("map");
            ret.initialSpawnPoint = rs.getByte("spawnpoint");
            ret.world = rs.getByte("world");
            ret.guildid = rs.getInt("guildid");
            ret.guildrank = rs.getByte("guildrank");
            ret.allianceRank = rs.getByte("allianceRank");
            ret.guildContribution = rs.getInt("guildContribution");
            ret.totalWins = rs.getInt("totalWins");
            ret.totalLosses = rs.getInt("totalLosses");
            ret.currentrep = rs.getInt("currentrep");
            ret.totalrep = rs.getInt("totalrep");
            ret.makeMFC(rs.getInt("familyid"), rs.getInt("seniorid"), rs.getInt("junior1"), rs.getInt("junior2"));
            if ((ret.guildid > 0) && (client != null))
            {
                ret.mgc = new MapleGuildCharacter(ret);
            }
            ret.gachexp = rs.getInt("gachexp");
            ret.buddylist = new BuddyList(rs.getByte("buddyCapacity"));
            ret.subcategory = rs.getByte("subcategory");
            ret.mount = new client.inventory.MapleMount(ret, 0, PlayerStats.getSkillByJob(1004, ret.job), (byte) 0, (byte) 1, 0);

            ret.rank = rs.getInt("rank");
            ret.rankMove = rs.getInt("rankMove");
            ret.jobRank = rs.getInt("jobRank");
            ret.jobRankMove = rs.getInt("jobRankMove");

            ret.marriageId = rs.getInt("marriageId");

            ret.fatigue = rs.getShort("fatigue");

            ret.pvpExp = rs.getInt("pvpExp");
            ret.pvpPoints = rs.getInt("pvpPoints");

            for (MapleTrait t : ret.traits.values())
            {
                t.setExp(rs.getInt(t.getType().name()));
            }

            ret.decorate = rs.getInt("decorate");

            ret.beans = rs.getInt("beans");

            ret.warning = rs.getInt("warning");

            ret.dollars = rs.getInt("dollars");
            ret.shareLots = rs.getInt("sharelots");

            ret.reborns = rs.getInt("reborns");
            ret.reborns1 = rs.getInt("reborns1");
            ret.reborns2 = rs.getInt("reborns2");
            ret.reborns3 = rs.getInt("reborns3");
            ret.apstorage = rs.getInt("apstorage");

            ret.honorLevel = rs.getInt("honorLevel");
            ret.honorExp = rs.getInt("honorExp");

            ret.playerPoints = rs.getInt("playerPoints");
            ret.playerEnergy = rs.getInt("playerEnergy");

            ret.pvpDeaths = rs.getInt("pvpDeaths");
            ret.pvpKills = rs.getInt("pvpKills");
            ret.pvpVictory = rs.getInt("pvpVictory");

            ret.vip = rs.getInt("vip");
            Timestamp expiration = rs.getTimestamp("viptime");
            ret.viptime = (expiration);
            if ((channelserver) && (client != null))
            {
                ret.pvpStats = MaplePvpStats.loadOrCreateFromDB(ret.accountid);

                ret.anticheat = new CheatTracker(ret);

                ret.antiMacro = new MapleLieDetector(ret);

                server.maps.MapleMapFactory mapFactory = ChannelServer.getInstance(client.getChannel()).getMapFactory();
                ret.map = mapFactory.getMap(ret.mapid);
                if (ret.map == null)
                {
                    ret.map = mapFactory.getMap(100000000);
                }

                MaplePortal portal = ret.map.getPortal(ret.initialSpawnPoint);
                if (portal == null)
                {
                    portal = ret.map.getPortal(0);
                    ret.initialSpawnPoint = 0;
                }
                ret.setPosition(portal.getPosition());

                int partyid = rs.getInt("party");
                if (partyid >= 0)
                {
                    MapleParty party = handling.world.WrodlPartyService.getInstance().getParty(partyid);
                    if ((party != null) && (party.getMemberById(ret.id) != null))
                    {
                        ret.party = party;
                    }
                }

                String[] pets = rs.getString("pets").split(",");
                for (int i = 0; i < ret.petStore.length; i++)
                {
                    ret.petStore[i] = Byte.parseByte(pets[i]);
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT * FROM achievements WHERE accountid = ?");
                ps.setInt(1, ret.accountid);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    ret.finishedAchievements.add(rs.getInt("achievementid"));
                }
                ps.close();
                rs.close();

                ps = con.prepareStatement("SELECT * FROM reports WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    if (ReportType.getById(rs.getByte("type")) != null)
                    {
                        ret.reports.put(ReportType.getById(rs.getByte("type")), rs.getInt("count"));
                    }
                }
            }
            rs.close();
            ps.close();


            if (cads != null)
            {
                ret.characterCard.setCards(cads);
            }
            else if (client != null)
            {
                ret.characterCard.loadCards(client, channelserver);
            }


            ps = con.prepareStatement("SELECT * FROM character_keyvalue WHERE characterid = ?");
            ps.setInt(1, charid);
            rs = ps.executeQuery();
            while (rs.next())
            {
                if (rs.getString("key") != null)
                {

                    ret.keyValue.put(rs.getString("key"), rs.getString("value"));
                }
            }
            rs.close();
            ps.close();

            ps = con.prepareStatement("SELECT * FROM questinfo WHERE characterid = ?");
            ps.setInt(1, charid);
            rs = ps.executeQuery();
            while (rs.next())
            {
                ret.questinfo.put(rs.getInt("quest"), rs.getString("customData"));
            }
            rs.close();
            ps.close();

            ps = con.prepareStatement("SELECT * FROM queststatus WHERE characterid = ?");
            ps.setInt(1, charid);
            rs = ps.executeQuery();
            PreparedStatement pse = con.prepareStatement("SELECT * FROM queststatusmobs WHERE queststatusid = ?");
            int id;
            while (rs.next())
            {
                id = rs.getInt("quest");
                MapleQuest q = MapleQuest.getInstance(id);
                byte stat = rs.getByte("status");
                if (((stat != 1) && (stat != 2)) || (((!channelserver) || ((q != null) && (!q.isBlocked()))) && (


                        (stat != 1) || (!channelserver) || (q.canStart(ret, null)))))
                {

                    MapleQuestStatus status = new MapleQuestStatus(q, stat);
                    long cTime = rs.getLong("time");
                    if (cTime > -1L)
                    {
                        status.setCompletionTime(cTime * 1000L);
                    }
                    status.setForfeited(rs.getInt("forfeited"));
                    status.setCustomData(rs.getString("customData"));
                    ret.quests.put(q, status);
                    pse.setInt(1, rs.getInt("queststatusid"));
                    ResultSet rsMobs = pse.executeQuery();
                    while (rsMobs.next())
                    {
                        status.setMobKills(rsMobs.getInt("mob"), rsMobs.getInt("count"));
                    }
                    rsMobs.close();
                }
            }
            rs.close();
            ps.close();
            pse.close();
            Skill skil;
            if (channelserver)
            {
                ret.monsterbook = MonsterBook.loadCards(ret.accountid, ret);

                ps = con.prepareStatement("SELECT * FROM inventoryslot where characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                if (!rs.next())
                {
                    rs.close();
                    ps.close();
                    throw new RuntimeException("No Inventory slot column found in SQL. [inventoryslot]");
                }
                ret.getInventory(MapleInventoryType.EQUIP).setSlotLimit(rs.getByte("equip"));
                ret.getInventory(MapleInventoryType.USE).setSlotLimit(rs.getByte("use"));
                ret.getInventory(MapleInventoryType.SETUP).setSlotLimit(rs.getByte("setup"));
                ret.getInventory(MapleInventoryType.ETC).setSlotLimit(rs.getByte("etc"));
                ret.getInventory(MapleInventoryType.CASH).setSlotLimit(rs.getByte("cash"));

                ps.close();
                rs.close();

                for (Pair<Item, MapleInventoryType> mit : ItemLoader.装备道具.loadItems(false, charid).values())
                {
                    ret.getInventory(mit.getRight()).addFromDB(mit.getLeft());
                }

                ps = con.prepareStatement("SELECT * FROM accounts WHERE id = ?");
                ps.setInt(1, ret.accountid);
                rs = ps.executeQuery();
                if (rs.next())
                {
                    ret.getClient().setAccountName(rs.getString("name"));
                    ret.acash = rs.getInt("ACash");
                    ret.maplepoints = rs.getInt("mPoints");
                    ret.points = rs.getInt("points");
                    ret.vpoints = rs.getInt("vpoints");


                    if (rs.getTimestamp("lastlogon") != null)
                    {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(rs.getTimestamp("lastlogon").getTime());
                        if (cal.get(7) + 1 != Calendar.getInstance().get(7))
                        {
                        }
                    }


                    if (rs.getInt("banned") > 0)
                    {
                        rs.close();
                        ps.close();
                        ret.getClient().getSession().close(true);
                        throw new RuntimeException("加载的角色为封包状态，服务端断开这个连接...");
                    }
                    rs.close();
                    ps.close();

                    ps = con.prepareStatement("UPDATE accounts SET lastlogon = CURRENT_TIMESTAMP() WHERE id = ?");
                    ps.setInt(1, ret.accountid);
                    ps.executeUpdate();
                }
                else
                {
                    rs.close();
                }
                ps.close();

                ps = con.prepareStatement("SELECT skillid, skilllevel, masterlevel, expiration, teachId, position FROM skills WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();

                int phantom = 0;
                while (rs.next())
                {
                    int skid = rs.getInt("skillid");
                    skil = SkillFactory.getSkill(skid);
                    int skl = rs.getInt("skilllevel");
                    byte msl = rs.getByte("masterlevel");
                    int teachId = rs.getInt("teachId");
                    byte position = rs.getByte("position");
                    if ((skil != null) && (GameConstants.isApplicableSkill(skid)))
                    {
                        if (skil.is老技能())
                        {
                            ret.changed_skills = true;
                        }
                        else
                        {
                            if ((skl > skil.getMaxLevel()) && ((skid < 92000000) || (skid > 99999999)))
                            {
                                if ((!skil.isBeginnerSkill()) && (skil.canBeLearnedBy(ret.job)) && (!skil.isSpecialSkill()) && (!skil.isAdminSkill()))
                                {
                                    ret.remainingSp[GameConstants.getSkillBookBySkill(skid)] += skl - skil.getMaxLevel();
                                }
                                skl = (byte) skil.getMaxLevel();
                            }
                            if (msl > skil.getMaxLevel())
                            {
                                msl = (byte) skil.getMaxLevel();
                            }
                            if ((position >= 0) && (position < 13) && (phantom < 13))
                            {
                                if ((GameConstants.is幻影(ret.job)) && (skil.getSkillByJobBook() != -1))
                                {
                                    msl = skil.isFourthJob() ? (byte) skil.getMasterLevel() : 0;
                                    ret.skills.put(skil, new SkillEntry(skl, msl, -1L, teachId, position));
                                }
                                phantom++;
                            }
                            else
                            {
                                ret.skills.put(skil, new SkillEntry(skl, msl, rs.getLong("expiration"), teachId));
                            }
                        }
                    }
                    else if ((skil == null) && (!GameConstants.is新手职业(skid / 10000)) && (!GameConstants.isSpecialSkill(skid)) && (!GameConstants.isAdminSkill(skid)))
                    {
                        ret.remainingSp[GameConstants.getSkillBookBySkill(skid)] += skl;
                    }
                }

                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT skillid, skilllevel, position, rank FROM innerskills WHERE characterid = ? LIMIT 3");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    int skid = rs.getInt("skillid");
                    skil = SkillFactory.getSkill(skid);
                    int skl = rs.getInt("skilllevel");
                    byte position = rs.getByte("position");
                    byte rank = rs.getByte("rank");
                    if ((skil != null) && (skil.isInnerSkill()) && (position >= 1) && (position <= 3))
                    {
                        if (skl > skil.getMaxLevel())
                        {
                            skl = (byte) skil.getMaxLevel();
                        }
                        InnerSkillEntry InnerSkill = new InnerSkillEntry(skid, skl, position, rank);
                        ret.innerSkills[(position - 1)] = InnerSkill;
                    }
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT * FROM characters WHERE accountid = ? ORDER BY level DESC");
                ps.setInt(1, ret.accountid);
                rs = ps.executeQuery();
                int maxlevel_ = 0;
                int maxlevel_2 = 0;
                while (rs.next())
                {
                    if (rs.getInt("id") != charid)
                    {
                        if (GameConstants.is骑士团(rs.getShort("job")))
                        {
                            int maxlevel = rs.getShort("level") / 5;
                            if (maxlevel > 24)
                            {
                                maxlevel = 24;
                            }
                            if ((maxlevel > maxlevel_2) || (maxlevel_2 == 0))
                            {
                                maxlevel_2 = maxlevel;
                                ret.BlessOfEmpress_Origin = rs.getString("name");
                            }
                        }
                        int maxlevel = rs.getShort("level") / 10;
                        if (maxlevel > 20)
                        {
                            maxlevel = 20;
                        }
                        if ((maxlevel > maxlevel_) || (maxlevel_ == 0))
                        {
                            maxlevel_ = maxlevel;
                            ret.BlessOfFairy_Origin = rs.getString("name");
                        }
                    }
                }
                if (ret.BlessOfFairy_Origin == null)
                {
                    ret.BlessOfFairy_Origin = ret.name;
                }
                ret.skills.put(SkillFactory.getSkill(GameConstants.getBOF_ForJob(ret.job)), new SkillEntry(maxlevel_, (byte) 0, -1L, 0));
                if (SkillFactory.getSkill(GameConstants.getEmpress_ForJob(ret.job)) != null)
                {
                    if (ret.BlessOfEmpress_Origin == null)
                    {
                        ret.BlessOfEmpress_Origin = ret.BlessOfFairy_Origin;
                    }
                    ret.skills.put(SkillFactory.getSkill(GameConstants.getEmpress_ForJob(ret.job)), new SkillEntry(maxlevel_2, (byte) 0, -1L, 0));
                }
                ps.close();
                rs.close();


                ps = con.prepareStatement("SELECT * FROM skillmacros WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();

                while (rs.next())
                {
                    int position = rs.getInt("position");
                    SkillMacro macro = new SkillMacro(rs.getInt("skill1"), rs.getInt("skill2"), rs.getInt("skill3"), rs.getString("name"), rs.getInt("shout"), position);
                    ret.skillMacros[position] = macro;
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT * FROM familiars WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    if (rs.getLong("expiry") > System.currentTimeMillis())
                    {

                        ret.familiars.put(rs.getInt("familiar"), new MonsterFamiliar(charid, rs.getInt("id"), rs.getInt("familiar"), rs.getLong("expiry"), rs.getString("name"), rs.getInt("fatigue")
                                , rs.getByte("vitality")));
                    }
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT * FROM pokemon WHERE characterid = ? OR (accountid = ? AND active = 0)");
                ps.setInt(1, charid);
                ps.setInt(2, ret.accountid);
                rs = ps.executeQuery();
                int position = 0;
                while (rs.next())
                {
                    Battler b = new Battler(rs.getInt("level"), rs.getInt("exp"), charid, rs.getInt("monsterid"), rs.getString("name"), constants.BattleConstants.PokemonNature.values()[rs.getInt(
                            "nature")], rs.getInt("itemid"), rs.getByte("gender"), rs.getByte("hpiv"), rs.getByte("atkiv"), rs.getByte("defiv"), rs.getByte("spatkiv"), rs.getByte("spdefiv"),
                            rs.getByte("speediv"), rs.getByte("evaiv"), rs.getByte("acciv"), rs.getByte("ability"));
                    if (b.getFamily() != null)
                    {

                        if ((rs.getInt("active") > 0) && (position < 6) && (rs.getInt("characterid") == charid))
                        {
                            ret.battlers[position] = b;
                            position++;
                        }
                        else
                        {
                            ret.boxed.add(b);
                        }
                    }
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT `key`,`type`,`action` FROM keymap WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                Map<Integer, Pair<Byte, Integer>> keyb = ret.keylayout.Layout();
                while (rs.next())
                {
                    keyb.put(rs.getInt("key"), new Pair(rs.getByte("type"), rs.getInt("action")));
                }
                rs.close();
                ps.close();
                ret.keylayout.unchanged();

                ps = con.prepareStatement("SELECT `index`, `key` FROM quickslot WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                List<Pair<Integer, Integer>> quickslots = ret.quickslot.Layout();
                while (rs.next())
                {
                    quickslots.add(new Pair(rs.getInt("index"), rs.getInt("key")));
                }
                rs.close();
                ps.close();
                ret.quickslot.unchanged();

                ps = con.prepareStatement("SELECT `locationtype`,`map` FROM savedlocations WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    ret.savedLocations[rs.getInt("locationtype")] = rs.getInt("map");
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT `characterid_to`,`when` FROM famelog WHERE characterid = ? AND DATEDIFF(NOW(),`when`) < 30");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                ret.lastfametime = 0L;
                ret.lastmonthfameids = new ArrayList(31);
                while (rs.next())
                {
                    ret.lastfametime = Math.max(ret.lastfametime, rs.getTimestamp("when").getTime());
                    ret.lastmonthfameids.add(rs.getInt("characterid_to"));
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT `characterid_to`,`when` FROM lovelog WHERE characterid = ? AND DATEDIFF(NOW(),`when`) < 1");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                ret.lastlovetime = 0L;
                ret.lastdayloveids = new LinkedHashMap<>();
                while (rs.next())
                {
                    ret.lastlovetime = Math.max(ret.lastlovetime, rs.getTimestamp("when").getTime());
                    ret.lastdayloveids.put(rs.getInt("characterid_to"), rs.getTimestamp("when").getTime());
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT `accid_to`,`when` FROM battlelog WHERE accid = ? AND DATEDIFF(NOW(),`when`) < 30");
                ps.setInt(1, ret.accountid);
                rs = ps.executeQuery();
                ret.lastmonthbattleids = new ArrayList<>();
                while (rs.next())
                {
                    ret.lastmonthbattleids.add(rs.getInt("accid_to"));
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT `itemId` FROM extendedSlots WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    ret.extendedSlots.add(rs.getInt("itemId"));
                }
                rs.close();
                ps.close();

                ret.buddylist.loadFromDb(charid);

                ret.storage = server.MapleStorage.loadOrCreateFromDB(ret.accountid);

                ret.cs = new server.cashshop.CashShop(ret.accountid, charid, ret.getJob());

                ps = con.prepareStatement("SELECT sn FROM wishlist WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                int i = 0;
                while (rs.next())
                {
                    ret.wishlist[i] = rs.getInt("sn");
                    i++;
                }
                while (i < 12)
                {
                    ret.wishlist[i] = 0;
                    i++;
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT mapid,vip FROM trocklocations WHERE characterid = ? LIMIT 28");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                int r = 0;
                int reg = 0;
                int hyper = 0;
                while (rs.next())
                {
                    if (rs.getInt("vip") == 0)
                    {
                        ret.regrocks[reg] = rs.getInt("mapid");
                        reg++;
                    }
                    else if (rs.getInt("vip") == 1)
                    {
                        ret.rocks[r] = rs.getInt("mapid");
                        r++;
                    }
                    else if (rs.getInt("vip") == 2)
                    {
                        ret.hyperrocks[hyper] = rs.getInt("mapid");
                        hyper++;
                    }
                }
                while (reg < 5)
                {
                    ret.regrocks[reg] = 999999999;
                    reg++;
                }
                while (r < 10)
                {
                    ret.rocks[r] = 999999999;
                    r++;
                }
                while (hyper < 13)
                {
                    ret.hyperrocks[hyper] = 999999999;
                    hyper++;
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT * FROM imps WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                r = 0;
                while (rs.next())
                {
                    ret.imps[r] = new MapleImp(rs.getInt("itemid"));
                    ret.imps[r].setLevel(rs.getByte("level"));
                    ret.imps[r].setState(rs.getByte("state"));
                    ret.imps[r].setCloseness(rs.getShort("closeness"));
                    ret.imps[r].setFullness(rs.getShort("fullness"));
                    r++;
                }
                rs.close();
                ps.close();

                ps = con.prepareStatement("SELECT * FROM mountdata WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                if (!rs.next())
                {
                    throw new RuntimeException("在数据库中没有找到角色的坐骑信息...");
                }
                Item mount = ret.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -18);
                ret.mount = new client.inventory.MapleMount(ret, mount != null ? mount.getItemId() : 0, 80001000, rs.getByte("Fatigue"), rs.getByte("Level"), rs.getInt("Exp"));
                ps.close();
                rs.close();

                ps = con.prepareStatement("SELECT * FROM character_potionpots WHERE characterid = ?");
                ps.setInt(1, charid);
                rs = ps.executeQuery();
                if (rs.next())
                {
                    ret.potionPot = new client.inventory.MaplePotionPot(charid, rs.getInt("itemId"), rs.getInt("hp"), rs.getInt("mp"), rs.getInt("maxValue"), rs.getLong("startDate"), rs.getLong(
                            "endDate"));
                }
                ps.close();
                rs.close();

                if ((ret.getSkillLevel(1214) > 0) && (GameConstants.is龙的传人(ret.job)))
                {
                    ret.coreAura = MapleCoreAura.loadFromDb(charid, ret.level);
                    if (ret.coreAura == null)
                    {
                        ret.coreAura = MapleCoreAura.createCoreAura(charid, ret.level);
                    }
                }
                else if (ret.getSkillTeachId(80001151) > 0)
                {
                    ret.coreAura = MapleCoreAura.loadFromDb(ret.getSkillTeachId(80001151));
                }

                ret.stats.recalcLocalStats(true, ret);
            }
            else
            {
                for (Pair<Item, MapleInventoryType> mit : ItemLoader.装备道具.loadItems(true, charid).values())
                {
                    ret.getInventory(mit.getRight()).addFromDB(mit.getLeft());
                }
                ret.stats.recalcPVPRank(ret);
            }


            return ret;
        }
        catch (SQLException ess)
        {
            System.out.println("加载角色数据信息出错...");
            FileoutputUtil.outputFileError("log\\Packet_Except.log", ess);
            ret.getClient().getSession().close(true);
            throw new RuntimeException("加载角色数据信息出错.服务端断开这个连接...");
        }
        finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (rs != null)
                {
                    rs.close();
                }
            }
            catch (SQLException ignored)
            {
            }
        }
    }

    public MapleInventory getInventory(MapleInventoryType type)
    {
        return this.inventory[type.ordinal()];
    }

    public MapleClient getClient()
    {
        return this.client;
    }

    public void setClient(MapleClient client)
    {
        this.client = client;
    }

    public short getJob()
    {
        return this.job;
    }

    public int getSkillLevel(int skillid)
    {
        return getSkillLevel(SkillFactory.getSkill(skillid));
    }

    public int getSkillTeachId(int skillId)
    {
        return getSkillTeachId(SkillFactory.getSkill(skillId));
    }

    public int getSkillLevel(Skill skill)
    {
        if (skill == null)
        {
            return 0;
        }
        SkillEntry ret = this.skills.get(skill);
        if ((ret == null) || (ret.skillevel <= 0))
        {
            return 0;
        }
        return ret.skillevel;
    }

    public int getSkillTeachId(Skill skill)
    {
        if (skill == null)
        {
            return 0;
        }
        SkillEntry ret = this.skills.get(skill);
        if ((ret == null) || (ret.teachId == 0))
        {
            return 0;
        }
        return ret.teachId;
    }

    public void setJob(int jobId)
    {
        this.job = ((short) jobId);
    }

    public static void saveNewCharToDB(MapleCharacter chr, JobType type, short db, boolean oldkey)
    {
        Connection con = DatabaseConnection.getConnection();

        PreparedStatement ps = null;
        PreparedStatement pse = null;
        ResultSet rs = null;
        try
        {
            con.setTransactionIsolation(1);
            con.setAutoCommit(false);

            ps = con.prepareStatement("INSERT INTO characters (level, str, dex, luk, `int`, hp, mp, maxhp, maxmp, sp, ap, skincolor, gender, job, hair, face, map, meso, party, buddyCapacity, pets, "
                    + "decorate, subcategory, accountid, name, world) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 1);
            ps.setInt(1, chr.level);
            PlayerStats stat = chr.stats;
            ps.setShort(2, stat.getStr());
            ps.setShort(3, stat.getDex());
            ps.setShort(4, stat.getInt());
            ps.setShort(5, stat.getLuk());
            ps.setInt(6, stat.getHp());
            ps.setInt(7, stat.getMp());
            ps.setInt(8, stat.getMaxHp());
            ps.setInt(9, stat.getMaxMp());
            StringBuilder sps = new StringBuilder();
            for (int i = 0; i < chr.remainingSp.length; i++)
            {
                sps.append(chr.remainingSp[i]);
                sps.append(",");
            }
            String sp = sps.toString();
            ps.setString(10, sp.substring(0, sp.length() - 1));
            ps.setShort(11, chr.remainingAp);
            ps.setByte(12, chr.skinColor);
            ps.setByte(13, chr.gender);
            ps.setShort(14, chr.job);
            ps.setInt(15, chr.hair);
            ps.setInt(16, chr.face);
            if ((db < 0) || (db > 10))
            {
                db = 0;
            }
            ps.setInt(17, configs.ServerConfig.BEGINNER_SPAWN_MAP);
            ps.setInt(18, chr.meso);
            ps.setInt(19, -1);
            ps.setByte(20, chr.buddylist.getCapacity());
            ps.setString(21, "-1,-1,-1");
            ps.setInt(22, chr.decorate);
            ps.setInt(23, db);
            ps.setInt(24, chr.getAccountID());
            ps.setString(25, chr.name);
            ps.setByte(26, chr.world);
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next())
            {
                chr.id = rs.getInt(1);
            }
            else
            {
                ps.close();
                rs.close();
                throw new database.DatabaseException("生成新角色到数据库出错...");
            }
            ps.close();
            rs.close();


            ps = con.prepareStatement("INSERT INTO queststatus (`queststatusid`, `characterid`, `quest`, `status`, `time`, `forfeited`, `customData`) VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)", 1);
            pse = con.prepareStatement("INSERT INTO queststatusmobs VALUES (DEFAULT, ?, ?, ?)");
            ps.setInt(1, chr.id);
            for (MapleQuestStatus q : chr.quests.values())
            {
                ps.setInt(2, q.getQuest().getId());
                ps.setInt(3, q.getStatus());
                ps.setInt(4, (int) (q.getCompletionTime() / 1000L));
                ps.setInt(5, q.getForfeited());
                ps.setString(6, q.getCustomData());
                ps.execute();
                rs = ps.getGeneratedKeys();
                Iterator localIterator2;
                if (q.hasMobKills())
                {
                    rs.next();
                    for (localIterator2 = q.getMobKills().keySet().iterator(); localIterator2.hasNext(); )
                    {
                        int mob = (Integer) localIterator2.next();
                        pse.setInt(1, rs.getInt(1));
                        pse.setInt(2, mob);
                        pse.setInt(3, q.getMobKills(mob));
                        pse.execute();
                    }
                }
                rs.close();
            }
            ps.close();
            pse.close();


            ps = con.prepareStatement("INSERT INTO character_keyvalue (`characterid`, `key`, `value`) VALUES (?, ?, ?)");
            ps.setInt(1, chr.id);
            for (Map.Entry<String, String> key : chr.keyValue.entrySet())
            {
                ps.setString(2, key.getKey());
                ps.setString(3, key.getValue());
                ps.execute();
            }
            ps.close();


            ps = con.prepareStatement("INSERT INTO skills (characterid, skillid, skilllevel, masterlevel, expiration, teachId) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setInt(1, chr.id);
            for (Map.Entry<Skill, SkillEntry> skill : chr.skills.entrySet())
            {
                if (GameConstants.isApplicableSkill(skill.getKey().getId()))
                {
                    ps.setInt(2, skill.getKey().getId());
                    ps.setInt(3, skill.getValue().skillevel);
                    ps.setByte(4, skill.getValue().masterlevel);
                    ps.setLong(5, skill.getValue().expiration);
                    ps.setInt(6, skill.getValue().teachId);
                    ps.execute();
                }
            }
            ps.close();


            ps = con.prepareStatement("INSERT INTO inventoryslot (characterid, `equip`, `use`, `setup`, `etc`, `cash`) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setInt(1, chr.id);
            ps.setByte(2, (byte) 64);
            ps.setByte(3, (byte) 64);
            ps.setByte(4, (byte) 64);
            ps.setByte(5, (byte) 64);
            ps.setByte(6, (byte) 60);
            ps.execute();
            ps.close();


            ps = con.prepareStatement("INSERT INTO mountdata (characterid, `Level`, `Exp`, `Fatigue`) VALUES (?, ?, ?, ?)");
            ps.setInt(1, chr.id);
            ps.setByte(2, (byte) 1);
            ps.setInt(3, 0);
            ps.setByte(4, (byte) 0);
            ps.execute();
            ps.close();


            int[] array1 = {2, 3, 4, 5, 6, 7, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 31, 33, 34, 35, 37, 38, 39, 40, 41, 43, 44, 45, 46, 47, 48, 50, 51, 52, 56, 57, 59, 60, 61, 62, 63,
                    64, 65};
            int[] array2 = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 4, 4, 4, 4, 4, 4, 5, 5, 6, 6, 6, 6, 6, 6, 6};
            int[] array3 = {10, 12, 13, 18, 23, 28, 8, 5, 0, 4, 27, 30, 32, 1, 24, 19, 14, 15, 52, 2, 25, 17, 11, 3, 20, 26, 16, 22, 9, 50, 51, 6, 31, 29, 7, 33, 35, 53, 54, 100, 101, 102, 103, 104
                    , 105, 106};

            int[] new_array1 = {20, 21, 22, 23, 24, 25, 26, 27, 29, 34, 35, 36, 37, 38, 39, 40, 41, 43, 44, 45, 46, 47, 48, 49, 50, 52, 56, 57, 59, 60, 61, 63, 64, 65, 66, 71, 73, 79, 81, 82, 83};
            int[] new_array2 = {4, 4, 4, 4, 4, 4, 4, 4, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 4, 4, 4, 4, 4, 4, 5, 5, 6, 6, 6, 6, 6, 6, 6, 4, 4, 4, 4, 4, 4};
            int[] new_array3 = {27, 30, 0, 1, 24, 19, 14, 15, 52, 17, 11, 8, 3, 20, 26, 16, 22, 9, 50, 51, 2, 31, 29, 5, 7, 4, 53, 54, 100, 101, 102, 103, 104, 105, 106, 12, 13, 23, 28, 10, 18};

            ps = con.prepareStatement("INSERT INTO keymap (characterid, `key`, `type`, `action`) VALUES (?, ?, ?, ?)");
            ps.setInt(1, chr.id);
            int keylength = oldkey ? array1.length : new_array1.length;
            for (int i = 0; i < keylength; i++)
            {
                ps.setInt(2, oldkey ? array1[i] : new_array1[i]);
                ps.setInt(3, oldkey ? array2[i] : new_array2[i]);
                ps.setInt(4, oldkey ? array3[i] : new_array3[i]);
                ps.execute();
            }
            ps.close();

            List<Pair<Item, MapleInventoryType>> itemsWithType = new ArrayList<>();
            for (MapleInventory iv : chr.inventory)
            {
                for (Item item : iv.list())
                {
                    itemsWithType.add(new Pair(item, iv.getType()));
                }
            }
            ItemLoader.装备道具.saveItems(itemsWithType, chr.id);
            con.commit();
            return;
        }
        catch (Exception e)
        {
            FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
            System.err.println("[charsave] Error saving character data");
            try
            {
                con.rollback();
            }
            catch (SQLException ex)
            {
                FileoutputUtil.outputFileError("log\\Packet_Except.log", ex);
                System.err.println("[charsave] Error Rolling Back");
            }
        }
        finally
        {
            try
            {
                if (pse != null)
                {
                    pse.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (rs != null)
                {
                    rs.close();
                }
                con.setAutoCommit(true);
                con.setTransactionIsolation(4);
            }
            catch (SQLException e)
            {
                FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
                System.err.println("[charsave] Error going back to autocommit mode");
            }
        }
    }

    public int getAccountID()
    {
        return this.accountid;
    }

    public static void deleteWhereCharacterId_NoLock(Connection con, String sql, int id) throws SQLException
    {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, id);
        ps.execute();
        ps.close();
    }

    public static boolean ban(String id, String reason, boolean accountId, int gmlevel, boolean hellban)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();

            if (id.matches("/[0-9]{1,3}\\..*"))
            {
                PreparedStatement ps = con.prepareStatement("INSERT INTO ipbans VALUES (DEFAULT, ?)");
                ps.setString(1, id);
                ps.execute();
                ps.close();
                return true;
            }
            PreparedStatement ps;
            if (accountId)
            {
                ps = con.prepareStatement("SELECT id FROM accounts WHERE name = ?");
            }
            else
            {
                ps = con.prepareStatement("SELECT accountid FROM characters WHERE name = ?");
            }
            boolean ret = false;
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                int z = rs.getInt(1);
                PreparedStatement psb = con.prepareStatement("UPDATE accounts SET banned = 1, banreason = ? WHERE id = ? AND gm < ?");
                psb.setString(1, reason);
                psb.setInt(2, z);
                psb.setInt(3, gmlevel);
                psb.execute();
                psb.close();
                if (gmlevel > 100)
                {
                    PreparedStatement psa = con.prepareStatement("SELECT * FROM accounts WHERE id = ?");
                    psa.setInt(1, z);
                    ResultSet rsa = psa.executeQuery();
                    if (rsa.next())
                    {
                        String sessionIP = rsa.getString("sessionIP");
                        if ((sessionIP != null) && (sessionIP.matches("/[0-9]{1,3}\\..*")))
                        {
                            PreparedStatement psz = con.prepareStatement("INSERT INTO ipbans VALUES (DEFAULT, ?)");
                            psz.setString(1, sessionIP);
                            psz.execute();
                            psz.close();
                        }
                        String macData = rsa.getString("macs");
                        if ((macData != null) && (!macData.equalsIgnoreCase("00-00-00-00-00-00")) && (macData.length() >= 17))
                        {
                            PreparedStatement psm = con.prepareStatement("INSERT INTO macbans VALUES (DEFAULT, ?)");
                            psm.setString(1, macData);
                            psm.execute();
                            psm.close();
                        }
                        if (hellban)
                        {
                            PreparedStatement pss = con.prepareStatement("UPDATE accounts SET banned = 1, banreason = ? WHERE email = ?" + (sessionIP == null ? "" : " OR SessionIP = ?"));
                            pss.setString(1, reason);
                            pss.setString(2, rsa.getString("email"));
                            if (sessionIP != null)
                            {
                                pss.setString(3, sessionIP);
                            }
                            pss.execute();
                            pss.close();
                        }
                    }
                    rsa.close();
                    psa.close();
                }
                ret = true;
            }
            rs.close();
            ps.close();
            return ret;
        }
        catch (SQLException ex)
        {
            System.err.println("Error while banning" + ex);
        }
        return false;
    }

    public Map<Integer, MapleSummon> getSummons()
    {
        return (Map<Integer, MapleSummon>) summons;
    }

    public int getCoconutTeam()
    {
        return coconutteam;
    }

    public void setCoconutTeam(int team)
    {
        coconutteam = team;
    }

    public void saveToDB(boolean dc, boolean fromcs)
    {
        if (this.isSaveing)
        {
            log.info(MapleClient.getLogMessage(this, "正在保存数据，本次操作返回."));
            return;
        }
        Connection con = DatabaseConnection.getConnection();

        PreparedStatement ps = null;
        PreparedStatement pse = null;
        ResultSet rs = null;
        try
        {
            this.isSaveing = true;
            con.setTransactionIsolation(1);
            con.setAutoCommit(false);

            ps = con.prepareStatement("UPDATE characters SET level = ?, fame = ?, str = ?, dex = ?, luk = ?, `int` = ?, exp = ?, hp = ?, mp = ?, maxhp = ?, maxmp = ?, sp = ?, ap = ?, gm = ?, " +
                    "skincolor = ?, gender = ?, job = ?, hair = ?, face = ?, map = ?, meso = ?, hpApUsed = ?, spawnpoint = ?, party = ?, buddyCapacity = ?, pets = ?, subcategory = ?, marriageId = ?, " +
                    "currentrep = ?, totalrep = ?, gachexp = ?, fatigue = ?, charm = ?, charisma = ?, craft = ?, insight = ?, sense = ?, will = ?, totalwins = ?, totallosses = ?, pvpExp = ?, pvpPoints = " + "?, decorate = ?, beans = ?, warning = ?, dollars = ?, sharelots = ?, reborns = ?, reborns1 = ?, reborns2 = ?, reborns3 = ?, apstorage = ?, honorLevel = ?, honorExp = ?, love = ?, " + "playerPoints = ?, playerEnergy = ?, pvpDeaths = ?, pvpKills = ?, pvpVictory = ?, vip = ?, viptime = ?, name = ? WHERE id = ?", 1);
            ps.setInt(1, this.level);
            ps.setInt(2, this.fame);
            ps.setShort(3, this.stats.getStr());
            ps.setShort(4, this.stats.getDex());
            ps.setShort(5, this.stats.getLuk());
            ps.setShort(6, this.stats.getInt());
            ps.setLong(7, this.level >= getMaxLevelForSever() ? 0L : Math.abs(this.exp.get()));
            ps.setInt(8, this.stats.getHp() < 1 ? 50 : this.stats.getHp());
            ps.setInt(9, this.stats.getMp());
            ps.setInt(10, this.stats.getMaxHp());
            ps.setInt(11, this.stats.getMaxMp());
            StringBuilder sps = new StringBuilder();
            for (int element : this.remainingSp)
            {
                sps.append(element);
                sps.append(",");
            }
            String sp = sps.toString();
            ps.setString(12, sp.substring(0, sp.length() - 1));
            ps.setShort(13, this.remainingAp);
            ps.setByte(14, this.gmLevel);
            ps.setByte(15, this.skinColor);
            ps.setByte(16, this.gender);
            ps.setShort(17, this.job);
            ps.setInt(18, this.hair);
            ps.setInt(19, this.face);
            if ((!fromcs) && (this.map != null))
            {
                if (this.map.getId() == 180000001)
                {
                    ps.setInt(20, 180000001);
                }
                else if ((this.map.getForcedReturnId() != 999999999) && (this.map.getForcedReturnMap() != null))
                {
                    ps.setInt(20, this.map.getForcedReturnId());
                }
                else
                {
                    ps.setInt(20, this.stats.getHp() < 1 ? this.map.getReturnMapId() : this.map.getId());
                }
            }
            else
            {
                ps.setInt(20, this.mapid);
            }
            ps.setInt(21, this.meso);
            ps.setShort(22, this.hpApUsed);
            if (this.map == null)
            {
                ps.setByte(23, (byte) 0);
            }
            else
            {
                MaplePortal closest = this.map.findClosestSpawnpoint(getTruePosition());
                ps.setByte(23, (byte) (closest != null ? closest.getId() : 0));
            }
            ps.setInt(24, this.party == null ? -1 : this.party.getId());
            ps.setShort(25, this.buddylist.getCapacity());
            StringBuilder petz = new StringBuilder();
            for (int i = 0; i < 3; i++)
            {
                if ((this.spawnPets[i] != null) && (this.spawnPets[i].getSummoned()))
                {
                    this.spawnPets[i].saveToDb();
                    petz.append(this.spawnPets[i].getInventoryPosition());
                    petz.append(",");
                }
                else
                {
                    petz.append("-1,");
                }
            }
            String petstring = petz.toString();
            ps.setString(26, petstring.substring(0, petstring.length() - 1));
            ps.setByte(27, this.subcategory);
            ps.setInt(28, this.marriageId);
            ps.setInt(29, this.currentrep);
            ps.setInt(30, this.totalrep);
            ps.setInt(31, this.gachexp);
            ps.setShort(32, this.fatigue);
            ps.setInt(33, this.traits.get(MapleTraitType.charm).getTotalExp());
            ps.setInt(34, this.traits.get(MapleTraitType.charisma).getTotalExp());
            ps.setInt(35, this.traits.get(MapleTraitType.craft).getTotalExp());
            ps.setInt(36, this.traits.get(MapleTraitType.insight).getTotalExp());
            ps.setInt(37, this.traits.get(MapleTraitType.sense).getTotalExp());
            ps.setInt(38, this.traits.get(MapleTraitType.will).getTotalExp());
            ps.setInt(39, this.totalWins);
            ps.setInt(40, this.totalLosses);
            ps.setInt(41, this.pvpExp);
            ps.setInt(42, this.pvpPoints);

            ps.setInt(43, this.decorate);

            ps.setInt(44, this.beans);

            ps.setInt(45, this.warning);

            ps.setInt(46, this.dollars);
            ps.setInt(47, this.shareLots);

            ps.setInt(48, this.reborns);
            ps.setInt(49, this.reborns1);
            ps.setInt(50, this.reborns2);
            ps.setInt(51, this.reborns3);
            ps.setInt(52, this.apstorage);

            ps.setInt(53, this.honorLevel);
            ps.setInt(54, this.honorExp);

            ps.setInt(55, this.love);

            ps.setInt(56, this.playerPoints);
            ps.setInt(57, this.playerEnergy);

            ps.setInt(58, this.pvpDeaths);
            ps.setInt(59, this.pvpKills);
            ps.setInt(60, this.pvpVictory);

            ps.setInt(61, this.vip);
            ps.setTimestamp(62, getViptime());

            ps.setString(63, this.name);
            ps.setInt(64, this.id);
            if (ps.executeUpdate() < 1)
            {
                ps.close();
                throw new database.DatabaseException("Character not in database (" + this.id + ")");
            }
            ps.close();


            if (this.changed_skillmacros)
            {
                deleteWhereCharacterId(con, "DELETE FROM skillmacros WHERE characterid = ?");
                for (int i = 0; i < 5; i++)
                {
                    SkillMacro macro = this.skillMacros[i];
                    if (macro != null)
                    {
                        ps = con.prepareStatement("INSERT INTO skillmacros (characterid, skill1, skill2, skill3, name, shout, position) VALUES (?, ?, ?, ?, ?, ?, ?)");
                        ps.setInt(1, this.id);
                        ps.setInt(2, macro.getSkill1());
                        ps.setInt(3, macro.getSkill2());
                        ps.setInt(4, macro.getSkill3());
                        ps.setString(5, macro.getName());
                        ps.setInt(6, macro.getShout());
                        ps.setInt(7, i);
                        ps.execute();
                        ps.close();
                    }
                }
            }
            Battler macro;
            if (this.changed_pokemon)
            {
                ps = con.prepareStatement("DELETE FROM pokemon WHERE characterid = ? OR (accountid = ? AND active = 0)");
                ps.setInt(1, this.id);
                ps.setInt(2, this.accountid);
                ps.execute();
                ps.close();
                ps =
                        con.prepareStatement("INSERT INTO pokemon (characterid, level, exp, monsterid, name, nature, active, accountid, itemid, gender, hpiv, atkiv, defiv, spatkiv, spdefiv, speediv, " +
                                "evaiv, acciv, ability) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                for (Battler value : this.battlers)
                {
                    macro = value;
                    if (macro != null)
                    {
                        ps.setInt(1, this.id);
                        ps.setInt(2, macro.getLevel());
                        ps.setInt(3, macro.getExp());
                        ps.setInt(4, macro.getMonsterId());
                        ps.setString(5, macro.getName());
                        ps.setInt(6, macro.getNature().ordinal());
                        ps.setInt(7, 1);
                        ps.setInt(8, this.accountid);
                        ps.setInt(9, macro.getItem() == null ? 0 : macro.getItem().id);
                        ps.setByte(10, macro.getGender());
                        ps.setByte(11, macro.getIV(BattleConstants.PokemonStat.HP));
                        ps.setByte(12, macro.getIV(BattleConstants.PokemonStat.ATK));
                        ps.setByte(13, macro.getIV(BattleConstants.PokemonStat.DEF));
                        ps.setByte(14, macro.getIV(BattleConstants.PokemonStat.SPATK));
                        ps.setByte(15, macro.getIV(BattleConstants.PokemonStat.SPDEF));
                        ps.setByte(16, macro.getIV(BattleConstants.PokemonStat.SPEED));
                        ps.setByte(17, macro.getIV(BattleConstants.PokemonStat.EVA));
                        ps.setByte(18, macro.getIV(BattleConstants.PokemonStat.ACC));
                        ps.setByte(19, macro.getAbilityIndex());
                        ps.execute();
                    }
                }
                for (Battler battler : this.boxed)
                {
                    macro = battler;
                    ps.setInt(1, this.id);
                    ps.setInt(2, macro.getLevel());
                    ps.setInt(3, macro.getExp());
                    ps.setInt(4, macro.getMonsterId());
                    ps.setString(5, macro.getName());
                    ps.setInt(6, macro.getNature().ordinal());
                    ps.setInt(7, 0);
                    ps.setInt(8, this.accountid);
                    ps.setInt(9, macro.getItem() == null ? 0 : macro.getItem().id);
                    ps.setByte(10, macro.getGender());
                    ps.setByte(11, macro.getIV(BattleConstants.PokemonStat.HP));
                    ps.setByte(12, macro.getIV(BattleConstants.PokemonStat.ATK));
                    ps.setByte(13, macro.getIV(BattleConstants.PokemonStat.DEF));
                    ps.setByte(14, macro.getIV(BattleConstants.PokemonStat.SPATK));
                    ps.setByte(15, macro.getIV(BattleConstants.PokemonStat.SPDEF));
                    ps.setByte(16, macro.getIV(BattleConstants.PokemonStat.SPEED));
                    ps.setByte(17, macro.getIV(BattleConstants.PokemonStat.EVA));
                    ps.setByte(18, macro.getIV(BattleConstants.PokemonStat.ACC));
                    ps.setByte(19, macro.getAbilityIndex());
                    ps.execute();
                }
                ps.close();
            }


            deleteWhereCharacterId(con, "DELETE FROM inventoryslot WHERE characterid = ?");
            ps = con.prepareStatement("INSERT INTO inventoryslot (characterid, `equip`, `use`, `setup`, `etc`, `cash`) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setInt(1, this.id);
            ps.setByte(2, getInventory(MapleInventoryType.EQUIP).getSlotLimit());
            ps.setByte(3, getInventory(MapleInventoryType.USE).getSlotLimit());
            ps.setByte(4, getInventory(MapleInventoryType.SETUP).getSlotLimit());
            ps.setByte(5, getInventory(MapleInventoryType.ETC).getSlotLimit());
            ps.setByte(6, getInventory(MapleInventoryType.CASH).getSlotLimit());
            ps.execute();
            ps.close();


            List<Pair<Item, MapleInventoryType>> itemsWithType = new ArrayList<>();
            for (MapleInventory iv : this.inventory)
            {
                for (Item item : iv.list())
                {
                    itemsWithType.add(new Pair(item, iv.getType()));
                }
            }
            ItemLoader.装备道具.saveItems(itemsWithType, this.id);


            if (this.changed_keyValue)
            {
                deleteWhereCharacterId(con, "DELETE FROM character_keyvalue WHERE characterid = ?");
                ps = con.prepareStatement("INSERT INTO character_keyvalue (`characterid`, `key`, `value`) VALUES (?, ?, ?)");
                ps.setInt(1, this.id);
                for (Object key : this.keyValue.entrySet())
                {
                    ps.setString(2, (String) ((Map.Entry) key).getKey());
                    ps.setString(3, (String) ((Map.Entry) key).getValue());
                    ps.execute();
                }
                ps.close();
            }


            if (this.changed_questinfo)
            {
                deleteWhereCharacterId(con, "DELETE FROM questinfo WHERE characterid = ?");
                ps = con.prepareStatement("INSERT INTO questinfo (`characterid`, `quest`, `customData`) VALUES (?, ?, ?)");
                ps.setInt(1, this.id);
                for (Object q : this.questinfo.entrySet())
                {
                    ps.setInt(2, (Integer) ((Entry) q).getKey());
                    ps.setString(3, (String) ((Map.Entry) q).getValue());
                    ps.execute();
                }
                ps.close();
            }


            deleteWhereCharacterId(con, "DELETE FROM queststatus WHERE characterid = ?");
            ps = con.prepareStatement("INSERT INTO queststatus (`queststatusid`, `characterid`, `quest`, `status`, `time`, `forfeited`, `customData`) VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)", 1);
            pse = con.prepareStatement("INSERT INTO queststatusmobs VALUES (DEFAULT, ?, ?, ?)");
            ps.setInt(1, this.id);
            for (MapleQuestStatus q : this.quests.values())
            {
                ps.setInt(2, q.getQuest().getId());
                ps.setInt(3, q.getStatus());
                ps.setInt(4, (int) (q.getCompletionTime() / 1000L));
                ps.setInt(5, q.getForfeited());
                ps.setString(6, q.getCustomData());
                ps.execute();
                rs = ps.getGeneratedKeys();
                Iterator localIterator2;
                if (q.hasMobKills())
                {
                    rs.next();
                    for (localIterator2 = q.getMobKills().keySet().iterator(); localIterator2.hasNext(); )
                    {
                        int mob = (Integer) localIterator2.next();
                        pse.setInt(1, rs.getInt(1));
                        pse.setInt(2, mob);
                        pse.setInt(3, q.getMobKills(mob));
                        pse.execute();
                    }
                }
                rs.close();
            }
            int mob;
            ps.close();
            pse.close();


            if (this.changed_skills)
            {
                deleteWhereCharacterId(con, "DELETE FROM skills WHERE characterid = ?");
                ps = con.prepareStatement("INSERT INTO skills (characterid, skillid, skilllevel, masterlevel, expiration, teachId, position) VALUES (?, ?, ?, ?, ?, ?, ?)");
                ps.setInt(1, this.id);
                for (Object skill : this.skills.entrySet())
                {
                    if (GameConstants.isApplicableSkill(((Skill) ((Map.Entry) skill).getKey()).getId()))
                    {
                        ps.setInt(2, ((Skill) ((Map.Entry) skill).getKey()).getId());
                        ps.setInt(3, ((SkillEntry) ((Map.Entry) skill).getValue()).skillevel);
                        ps.setByte(4, ((SkillEntry) ((Map.Entry) skill).getValue()).masterlevel);
                        ps.setLong(5, ((SkillEntry) ((Map.Entry) skill).getValue()).expiration);
                        ps.setInt(6, ((SkillEntry) ((Map.Entry) skill).getValue()).teachId);
                        ps.setByte(7, ((SkillEntry) ((Map.Entry) skill).getValue()).position);
                        ps.execute();
                    }
                }
                ps.close();
            }


            if (this.changed_innerSkills)
            {
                deleteWhereCharacterId(con, "DELETE FROM innerskills WHERE characterid = ?");
                for (int i = 0; i < 3; i++)
                {
                    InnerSkillEntry InnerSkill = this.innerSkills[i];
                    if (InnerSkill != null)
                    {
                        ps = con.prepareStatement("INSERT INTO innerskills (characterid, skillid, skilllevel, position, rank) VALUES (?, ?, ?, ?, ?)");
                        ps.setInt(1, this.id);
                        ps.setInt(2, InnerSkill.getSkillId());
                        ps.setInt(3, InnerSkill.getSkillLevel());
                        ps.setByte(4, InnerSkill.getPosition());
                        ps.setByte(5, InnerSkill.getRank());
                        ps.execute();
                        ps.close();
                    }
                }
            }


            List<MapleCoolDownValueHolder> coolDownInfo = getCooldowns();
            MapleCoolDownValueHolder cooling;
            if ((dc) && (coolDownInfo.size() > 0))
            {
                ps = con.prepareStatement("INSERT INTO skills_cooldowns (charid, SkillID, StartTime, length) VALUES (?, ?, ?, ?)");
                ps.setInt(1, getId());
                for (MapleCoolDownValueHolder mapleCoolDownValueHolder : coolDownInfo)
                {
                    cooling = mapleCoolDownValueHolder;
                    ps.setInt(2, cooling.skillId);
                    ps.setLong(3, cooling.startTime);
                    ps.setLong(4, cooling.length);
                    ps.execute();
                }
                ps.close();
            }


            if (this.changed_savedlocations)
            {
                deleteWhereCharacterId(con, "DELETE FROM savedlocations WHERE characterid = ?");
                ps = con.prepareStatement("INSERT INTO savedlocations (characterid, `locationtype`, `map`) VALUES (?, ?, ?)");
                ps.setInt(1, this.id);
                for (SavedLocationType savedLocationType : SavedLocationType.values())
                {
                    if (this.savedLocations[savedLocationType.getValue()] != -1)
                    {
                        ps.setInt(2, savedLocationType.getValue());
                        ps.setInt(3, this.savedLocations[savedLocationType.getValue()]);
                        ps.execute();
                    }
                }
                ps.close();
            }


            if (this.changed_achievements)
            {
                ps = con.prepareStatement("DELETE FROM achievements WHERE accountid = ?");
                ps.setInt(1, this.accountid);
                ps.executeUpdate();
                ps.close();
                ps = con.prepareStatement("INSERT INTO achievements(charid, achievementid, accountid) VALUES(?, ?, ?)");
                for (Integer achid : this.finishedAchievements)
                {
                    ps.setInt(1, this.id);
                    ps.setInt(2, achid);
                    ps.setInt(3, this.accountid);
                    ps.execute();
                }
                ps.close();
            }


            if (this.changed_reports)
            {
                deleteWhereCharacterId(con, "DELETE FROM reports WHERE characterid = ?");
                ps = con.prepareStatement("INSERT INTO reports VALUES(DEFAULT, ?, ?, ?)");
                for (Object achid : this.reports.entrySet())
                {
                    ps.setInt(1, this.id);
                    ps.setByte(2, ((ReportType) ((Entry) achid).getKey()).i);
                    ps.setInt(3, (Integer) ((Entry) achid).getValue());
                    ps.execute();
                }
                ps.close();
            }


            if (this.buddylist.changed())
            {
                deleteWhereCharacterId(con, "DELETE FROM buddies WHERE characterid = ?");
                ps = con.prepareStatement("INSERT INTO buddies (characterid, `buddyid`, `pending`) VALUES (?, ?, ?)");
                ps.setInt(1, this.id);
                for (BuddylistEntry entry : this.buddylist.getBuddies())
                {
                    ps.setInt(2, entry.getCharacterId());
                    ps.setInt(3, entry.isVisible() ? 0 : 1);
                    ps.execute();
                }
                ps.close();
                this.buddylist.setChanged(false);
            }


            ps = con.prepareStatement("UPDATE accounts SET `ACash` = ?, `mPoints` = ?, `points` = ?, `vpoints` = ? WHERE id = ?");
            ps.setInt(1, this.acash);
            ps.setInt(2, this.maplepoints);
            ps.setInt(3, this.points);
            ps.setInt(4, this.vpoints);
            ps.setInt(5, this.client.getAccID());
            ps.executeUpdate();
            ps.close();


            if (this.storage != null)
            {
                this.storage.saveToDB();
            }


            if (this.cs != null)
            {
                this.cs.save();
            }
            server.life.PlayerNPC.updateByCharId(this);


            this.keylayout.saveKeys(this.id);


            this.quickslot.saveQuickSlots(this.id);


            this.mount.saveMount(this.id);


            if (this.android != null)
            {
                this.android.saveToDb();
            }


            this.monsterbook.saveCards(this.accountid);


            this.pvpStats.saveToDb(this.accountid);


            if (this.potionPot != null)
            {
                this.potionPot.saveToDb();
            }


            if ((this.coreAura != null) && (this.coreAura.getId() == this.id))
            {
                this.coreAura.saveToDb();
            }


            deleteWhereCharacterId(con, "DELETE FROM familiars WHERE characterid = ?");
            ps = con.prepareStatement("INSERT INTO familiars (characterid, expiry, name, fatigue, vitality, familiar) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setInt(1, this.id);
            for (Object InnerSkill = this.familiars.values().iterator(); ((Iterator) InnerSkill).hasNext(); )
            {
                MonsterFamiliar f = (MonsterFamiliar) ((Iterator) InnerSkill).next();
                ps.setLong(2, f.getExpiry());
                ps.setString(3, f.getName());
                ps.setInt(4, f.getFatigue());
                ps.setByte(5, f.getVitality());
                ps.setInt(6, f.getFamiliar());
                ps.executeUpdate();
            }
            ps.close();


            deleteWhereCharacterId(con, "DELETE FROM imps WHERE characterid = ?");
            ps = con.prepareStatement("INSERT INTO imps (characterid, itemid, closeness, fullness, state, level) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setInt(1, this.id);
            for (MapleImp imp : this.imps)
            {
                if (imp != null)
                {
                    ps.setInt(2, imp.getItemId());
                    ps.setShort(3, imp.getCloseness());
                    ps.setShort(4, imp.getFullness());
                    ps.setByte(5, imp.getState());
                    ps.setByte(6, imp.getLevel());
                    ps.executeUpdate();
                }
            }
            ps.close();


            if (this.changed_wishlist)
            {
                deleteWhereCharacterId(con, "DELETE FROM wishlist WHERE characterid = ?");
                for (int i = 0; i < getWishlistSize(); i++)
                {
                    ps = con.prepareStatement("INSERT INTO wishlist(characterid, sn) VALUES(?, ?) ");
                    ps.setInt(1, getId());
                    ps.setInt(2, this.wishlist[i]);
                    ps.execute();
                    ps.close();
                }
            }

            if (this.changed_trocklocations)
            {


                deleteWhereCharacterId(con, "DELETE FROM trocklocations WHERE characterid = ?");
                for (int regrock : this.regrocks)
                {
                    if (regrock != 999999999)
                    {
                        ps = con.prepareStatement("INSERT INTO trocklocations(characterid, mapid, vip) VALUES (?, ?, 0)");
                        ps.setInt(1, getId());
                        ps.setInt(2, regrock);
                        ps.execute();
                        ps.close();
                    }
                }
                for (int rock : this.rocks)
                {
                    if (rock != 999999999)
                    {
                        ps = con.prepareStatement("INSERT INTO trocklocations(characterid, mapid, vip) VALUES (?, ?, 1)");
                        ps.setInt(1, getId());
                        ps.setInt(2, rock);
                        ps.execute();
                        ps.close();
                    }
                }
                for (int hyperrock : this.hyperrocks)
                {
                    if (hyperrock != 999999999)
                    {
                        ps = con.prepareStatement("INSERT INTO trocklocations(characterid, mapid, vip) VALUES (?, ?, 2)");
                        ps.setInt(1, getId());
                        ps.setInt(2, hyperrock);
                        ps.execute();
                        ps.close();
                    }
                }
            }

            Iterator localIterator1;

            if (this.changed_extendedSlots)
            {
                deleteWhereCharacterId(con, "DELETE FROM extendedSlots WHERE characterid = ?");
                for (localIterator1 = this.extendedSlots.iterator(); localIterator1.hasNext(); )
                {
                    int i = (Integer) localIterator1.next();
                    if (getInventory(MapleInventoryType.ETC).findById(i) != null)
                    {
                        ps = con.prepareStatement("INSERT INTO extendedSlots(characterid, itemId) VALUES(?, ?) ");
                        ps.setInt(1, getId());
                        ps.setInt(2, i);
                        ps.execute();
                        ps.close();
                    }
                }
            }
            this.isSaveing = false;
            this.changed_wishlist = false;
            this.changed_trocklocations = false;
            this.changed_skillmacros = false;
            this.changed_savedlocations = false;
            this.changed_pokemon = false;
            this.changed_questinfo = false;
            this.changed_achievements = false;
            this.changed_extendedSlots = false;
            this.changed_skills = false;
            this.changed_reports = false;
            this.changed_keyValue = false;
            con.commit();
            return;
        }
        catch (Exception e)
        {
            FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
            log.error(MapleClient.getLogMessage(this, "[charsave] 保存角色数据出现错误 .") + e);
            try
            {
                con.rollback();
            }
            catch (SQLException ex)
            {
                FileoutputUtil.outputFileError("log\\Packet_Except.log", ex);
                log.error(MapleClient.getLogMessage(this, "[charsave] Error Rolling Back") + ex);
            }
        }
        finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (pse != null)
                {
                    pse.close();
                }
                if (rs != null)
                {
                    rs.close();
                }
                con.setAutoCommit(true);
                con.setTransactionIsolation(4);
            }
            catch (SQLException e)
            {
                FileoutputUtil.outputFileError("log\\Packet_Except.log", e);
                log.error(MapleClient.getLogMessage(this, "[charsave] Error going back to autocommit mode") + e);
            }
        }
    }

    public PlayerSpecialStats getSpecialStat()
    {
        return this.specialStats;
    }

    public void cancelMapTimeLimitTask()
    {
        if (this.mapTimeLimitTask != null)
        {
            this.mapTimeLimitTask.cancel(false);
            this.mapTimeLimitTask = null;
        }
    }

    public void startMapTimeLimitTask(int time, final MapleMap to)
    {
        if (time <= 0)
        {
            time = 1;
        }
        this.client.getSession().write(MaplePacketCreator.getClock(time));
        final MapleMap ourMap = getMap();
        time *= 1000;
        this.mapTimeLimitTask = server.Timer.MapTimer.getInstance().register(new Runnable()
        {
            public void run()
            {
                if (ourMap.getId() == 180000001)
                {
                    MapleCharacter.this.getQuestNAdd(MapleQuest.getInstance(123455)).setCustomData(String.valueOf(System.currentTimeMillis()));
                    MapleCharacter.this.getQuestNAdd(MapleQuest.getInstance(123456)).setCustomData("0");
                }
                MapleCharacter.this.changeMap(to, to.getPortal(0));
            }
        }, time, time);
    }

    public MapleMap getMap()
    {
        return this.map;
    }

    public MapleQuestStatus getQuestNAdd(MapleQuest quest)
    {
        if (!this.quests.containsKey(quest))
        {
            MapleQuestStatus status = new MapleQuestStatus(quest, 0);
            this.quests.put(quest, status);
            return status;
        }
        return this.quests.get(quest);
    }

    public void changeMap(MapleMap to, MaplePortal pto)
    {
        changeMapInternal(to, pto.getPosition(), MaplePacketCreator.getWarpToMap(to, pto.getId(), this), null);
    }

    private void changeMapInternal(MapleMap to, Point pos, byte[] warpPacket, MaplePortal pto)
    {
        if (to == null)
        {
            return;
        }
        if (getAntiMacro().inProgress())
        {
            dropMessage(5, "被使用测谎仪时无法操作。");
            return;
        }
        int nowmapid = this.map.getId();
        if (this.eventInstance != null)
        {
            this.eventInstance.changedMap(this, to.getId());
        }
        boolean pyramid = this.pyramidSubway != null;
        if (this.map.getId() == nowmapid)
        {
            this.client.getSession().write(warpPacket);
            boolean shouldChange = this.client.getChannelServer().getPlayerStorage().getCharacterById(getId()) != null;
            boolean shouldState = this.map.getId() == to.getId();
            if ((shouldChange) && (shouldState))
            {
                to.setCheckStates(false);
            }
            this.map.removePlayer(this);
            if (shouldChange)
            {
                this.map = to;
                setPosition(pos);
                to.addPlayer(this);
                this.stats.relocHeal(this);
                if (shouldState)
                {
                    to.setCheckStates(true);
                }
            }
        }
        if ((pyramid) && (this.pyramidSubway != null))
        {
            this.pyramidSubway.onChangeMap(this, to.getId());
        }
    }

    public MapleLieDetector getAntiMacro()
    {
        return this.antiMacro;
    }

    public void dropMessage(int type, String message)
    {
        if (type == -1)
        {
            this.client.getSession().write(UIPacket.getTopMsg(message));
        }
        else if (type == -2)
        {
            this.client.getSession().write(tools.packet.PlayerShopPacket.shopChat(message, 0));
        }
        else if (type == -3)
        {
            this.client.getSession().write(MaplePacketCreator.getChatText(getId(), message, isSuperGM(), 0));
        }
        else if (type == -4)
        {
            this.client.getSession().write(MaplePacketCreator.getChatText(getId(), message, isSuperGM(), 1));
        }
        else if (type == -5)
        {
            this.client.getSession().write(MaplePacketCreator.spouseMessage(message, false));
        }
        else if (type == -6)
        {
            this.client.getSession().write(MaplePacketCreator.spouseMessage(message, true));
        }
        else if (type == -7)
        {
            this.client.getSession().write(UIPacket.getMidMsg(message, false, 0));
        }
        else if (type == -8)
        {
            this.client.getSession().write(UIPacket.getMidMsg(message, true, 0));
        }
        else if (type == -9)
        {
            this.client.getSession().write(MaplePacketCreator.showQuestMessage(message));
        }
        else if (type == -10)
        {
            this.client.getSession().write(MaplePacketCreator.getFollowMessage(message));
        }
        else if (type == -11)
        {
            this.client.getSession().write(MaplePacketCreator.yellowChat(message));
        }
        else
        {
            this.client.getSession().write(MaplePacketCreator.serverNotice(type, message));
        }
    }

    public int getId()
    {
        return this.id;
    }

    public boolean isSuperGM()
    {
        return this.gmLevel >= PlayerGMRank.SUPERGM.getLevel();
    }

    public void setMap(MapleMap newmap)
    {
        this.map = newmap;
    }

    public void setMap(int PmapId)
    {
        this.mapid = PmapId;
    }

    public Map<String, String> getKeyValue_Map()
    {
        return this.keyValue;
    }

    public int getInfoQuestStat(int id, String stat)
    {
        String statz = getInfoQuestStatS(id, stat);
        return (statz == null) || ("".equals(statz)) ? 0 : Integer.parseInt(statz);
    }

    public String getInfoQuestStatS(int id, String stat)
    {
        String info = getInfoQuest(id);
        if ((info != null) && (info.length() > 0) && (info.contains(stat)))
        {
            int startIndex = info.indexOf(stat) + stat.length() + 1;
            int until = info.indexOf(";", startIndex);
            return info.substring(startIndex, until != -1 ? until : info.length());
        }
        return "";
    }

    public String getInfoQuest(int questid)
    {
        if (this.questinfo.containsKey(questid))
        {
            return this.questinfo.get(questid);
        }
        return "";
    }

    public void setInfoQuestStat(int id, String stat, int statData)
    {
        setInfoQuestStat(id, stat, statData);
    }

    public void setInfoQuestStat(int id, String stat, String statData)
    {
        String info = getInfoQuest(id);
        if ((info.length() == 0) || (!info.contains(stat)))
        {
            updateInfoQuest(id, stat + "=" + statData + (info.length() == 0 ? "" : ";") + info);
        }
        else
        {
            String newInfo = stat + "=" + statData;
            String beforeStat = info.substring(0, info.indexOf(stat));
            int from = info.indexOf(";", info.indexOf(stat) + stat.length());
            String afterStat = from == -1 ? "" : info.substring(from + 1);
            updateInfoQuest(id, beforeStat + newInfo + (afterStat.length() != 0 ? ";" + afterStat : ""));
        }
    }

    public void updateInfoQuest(int questid, String data)
    {
        updateInfoQuest(questid, data, true);
    }

    public void updateInfoQuest(int questid, String data, boolean show)
    {
        this.questinfo.put(questid, data);
        this.changed_questinfo = true;
        if (show)
        {
            this.client.getSession().write(MaplePacketCreator.updateInfoQuest(questid, data));
        }
    }

    public int getNumQuest()
    {
        int i = 0;
        for (MapleQuestStatus q : this.quests.values())
        {
            if ((q.getStatus() == 2) && (!q.isCustom()))
            {
                i++;
            }
        }
        return i;
    }

    public byte getQuestStatus(int questId)
    {
        MapleQuest qq = MapleQuest.getInstance(questId);
        if (getQuestNoAdd(qq) == null)
        {
            return 0;
        }
        return getQuestNoAdd(qq).getStatus();
    }

    public MapleQuestStatus getQuestNoAdd(MapleQuest quest)
    {
        return this.quests.get(quest);
    }

    public MapleQuestStatus getQuest(MapleQuest quest)
    {
        if (!this.quests.containsKey(quest))
        {
            return new MapleQuestStatus(quest, 0);
        }
        return this.quests.get(quest);
    }

    public boolean needQuestItem(int questId, int itemId)
    {
        if (questId <= 0)
        {
            return true;
        }
        MapleQuest quest = MapleQuest.getInstance(questId);
        return getInventory(ItemConstants.getInventoryType(itemId)).countById(itemId) < quest.getAmountofItems(itemId);
    }

    public void setQuestAdd(MapleQuest quest, byte status, String customData)
    {
        if (!this.quests.containsKey(quest))
        {
            MapleQuestStatus stat = new MapleQuestStatus(quest, status);
            stat.setCustomData(customData);
            this.quests.put(quest, stat);
        }
    }

    public MapleQuestStatus getQuestRemove(MapleQuest quest)
    {
        return this.quests.remove(quest);
    }

    public void updateQuest(MapleQuestStatus quest)
    {
        updateQuest(quest, false);
    }

    public void updateQuest(MapleQuestStatus quest, boolean update)
    {
        this.quests.put(quest.getQuest(), quest);
        if (!quest.isCustom())
        {
            this.client.getSession().write(MaplePacketCreator.updateQuest(quest));
            if ((quest.getStatus() == 1) && (!update))
            {
                this.client.getSession().write(MaplePacketCreator.updateQuestInfo(quest.getQuest().getId(), quest.getNpc(), quest.getStatus() == 1));
            }
        }
    }

    public Map<Integer, String> getInfoQuest_Map()
    {
        return this.questinfo;
    }

    public Map<MapleQuest, MapleQuestStatus> getQuest_Map()
    {
        return this.quests;
    }

    public void startFishingTask()
    {
        cancelFishingTask();
        this.lastFishingTime = System.currentTimeMillis();
    }

    public void cancelFishingTask()
    {
        this.lastFishingTime = 0L;
    }

    public boolean canFish(long now)
    {
        return (this.lastFishingTime > 0L) && (this.lastFishingTime + GameConstants.getFishingTime(this.stats.canFishVIP, isGM()) < now);
    }

    public boolean isGM()
    {
        return this.gmLevel >= PlayerGMRank.GM.getLevel();
    }

    public void doFish(long now)
    {
        this.lastFishingTime = now;
        boolean expMulti = haveItem(2300001, 1, false, true);
        if ((this.client == null) || (this.client.getPlayer() == null) || (!this.client.isReceiving()) || ((!expMulti) && (!haveItem(2300000, 1, false, true))) || (!GameConstants.isFishingMap(getMapId())) || (!this.stats.canFish) || (this.chair <= 0))
        {
            cancelFishingTask();
            return;
        }
        MapleInventoryManipulator.removeById(this.client, MapleInventoryType.USE, expMulti ? 2300001 : 2300000, 1, false, false);
        boolean passed = false;
        while (!passed)
        {
            int randval = server.RandomRewards.getFishingReward();
            switch (randval)
            {
                case 0:
                    int money = Randomizer.rand(expMulti ? 15 : 10, expMulti ? 75000 : 50000);
                    gainMeso(money, true);
                    passed = true;
                    break;
                case 1:
                    int experi = Math.min(Randomizer.nextInt((int) Math.abs(getExpNeededForLevel() / 200L) + 1), 500000);
                    gainExp(expMulti ? experi * 3 / 2 : experi, true, false, true);
                    passed = true;
                    break;
                default:
                    if (MapleItemInformationProvider.getInstance().itemExists(randval))
                    {
                        MapleInventoryManipulator.addById(this.client, randval, (short) 1, "钓鱼 时间 " + FileoutputUtil.CurrentReadable_Date());
                        passed = true;
                    }
                    break;
            }
        }
        this.map.broadcastMessage(UIPacket.fishingCaught(this.id));
    }

    public Integer getBuffedSkill_X(MapleBuffStat stat)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return null;
        }
        return mbsvh.effect.getX();
    }

    public MapleBuffStatValueHolder getBuffStatValueHolder(MapleBuffStat stat)
    {
        MapleBuffStatValueHolder mbsvh = null;
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            if (buffs.getLeft() == stat)
            {
                mbsvh = buffs.getRight();
            }
        }
        return mbsvh;
    }

    public ArrayList<Pair<MapleBuffStat, MapleBuffStatValueHolder>> getAllEffects()
    {
        return new ArrayList(this.effects);
    }

    public Integer getBuffedSkill_Y(MapleBuffStat stat)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return null;
        }
        return mbsvh.effect.getY();
    }

    public boolean isBuffFrom(MapleBuffStat stat, Skill skill)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if ((mbsvh == null) || (mbsvh.effect == null) || (skill == null))
        {
            return false;
        }
        return (mbsvh.effect.isSkill()) && (mbsvh.effect.getSourceId() == skill.getId());
    }

    public boolean hasBuffSkill(int skillId)
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> mapleBuffStatMapleBuffStatValueHolderPair : getAllEffects())
        {
            Pair buffs = mapleBuffStatMapleBuffStatValueHolderPair;
            allBuffs.add((MapleBuffStatValueHolder) buffs.getRight());
        }
        Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs;
        boolean find = false;
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            if ((mbsvh.effect.isSkill()) && (mbsvh.effect.getSourceId() == skillId))
            {
                find = true;
                break;
            }
        }
        allBuffs.clear();
        return find;
    }

    public int getTrueBuffSource(MapleBuffStat stat)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return -1;
        }
        return mbsvh.effect.isSkill() ? mbsvh.effect.getSourceId() : -mbsvh.effect.getSourceId();
    }

    public void setBuffedValue(MapleBuffStat stat, int value)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return;
        }
        mbsvh.value = value;
    }

    public void setSchedule(MapleBuffStat stat, ScheduledFuture<?> sched)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return;
        }
        mbsvh.schedule.cancel(false);
        mbsvh.schedule = sched;
    }

    public Long getBuffedStartTime(MapleBuffStat stat)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return null;
        }
        return mbsvh.startTime;
    }

    public void registerEffect(MapleStatEffect effect, long starttime, ScheduledFuture<?> schedule, int from)
    {
        registerEffect(effect, starttime, schedule, effect.getStatups(), false, effect.getDuration(), from);
    }

    public void registerEffect(MapleStatEffect effect, long starttime, ScheduledFuture<?> schedule, List<Pair<MapleBuffStat, Integer>> statups, boolean silent, int localDuration, int from)
    {
        if (effect.is隐藏术())
        {
            this.map.broadcastMessage(this, MaplePacketCreator.removePlayerFromMap(getId()), false);
        }
        else if (effect.is龙之力())
        {
            prepareDragonBlood();
        }
        else if (effect.is团队治疗())
        {
            prepareRecovery();
        }
        else if (effect.is重生契约())
        {
            checkBerserk();
        }
        else if (effect.is骑兽技能_())
        {
            getMount().startSchedule();
        }
        else if (effect.is恶魔恢复())
        {
            prepareRecoveryEM();
        }
        for (Pair<MapleBuffStat, Integer> statup : statups)
        {
            int value = statup.getRight();
            this.effects.add(new Pair(statup.getLeft(), new MapleBuffStatValueHolder(effect, starttime, schedule, value, localDuration, from)));
        }
        if (!silent)
        {
            this.stats.recalcLocalStats(this);
        }
        if (isShowPacket())
        {
            dropSpouseMessage(25, "注册BUFF效果 - 当前BUFF总数: " + this.effects.size() + " 技能: " + effect.getSourceId());
        }
    }

    private void prepareDragonBlood()
    {
        this.lastDragonBloodTime = System.currentTimeMillis();
    }

    private void prepareRecovery()
    {
        this.lastRecoveryTime = System.currentTimeMillis();
    }

    public void checkBerserk()
    {
        if ((this.job != 132) || (this.lastBerserkTime < 0L) || (this.lastBerserkTime + 10000L > System.currentTimeMillis()))
        {
            return;
        }
        int skillId = 1320016;
        Skill BerserkX = SkillFactory.getSkill(skillId);
        int skilllevel = getTotalSkillLevel(BerserkX);
        if ((skilllevel >= 1) && (this.map != null))
        {
            this.lastBerserkTime = System.currentTimeMillis();
            MapleStatEffect ampStat = BerserkX.getEffect(skilllevel);
            this.stats.Berserk = (this.stats.getHp() * 100 / this.stats.getCurrentMaxHp() >= ampStat.getX());
            this.client.getSession().write(MaplePacketCreator.showOwnBuffEffect(skillId, 1, getLevel(), skilllevel, (byte) (this.stats.Berserk ? 1 : 0)));
            this.map.broadcastMessage(this, MaplePacketCreator.showBuffeffect(getId(), skillId, 1, getLevel(), skilllevel, (byte) (this.stats.Berserk ? 1 : 0)), false);
        }
        else
        {
            this.lastBerserkTime = -1L;
        }
    }

    public client.inventory.MapleMount getMount()
    {
        return this.mount;
    }

    private void prepareRecoveryEM()
    {
        this.lastRecoveryTimeEM = System.currentTimeMillis();
    }

    public boolean isShowPacket()
    {
        return (isAdmin()) && (ServerProperties.ShowPacket());
    }

    public void dropSpouseMessage(int type, String message)
    {
        if ((type == 0) || (type == 1) || ((type >= 6) && (type <= 32)))
        {
            this.client.getSession().write(MaplePacketCreator.spouseMessage(type, message));
        }
        else
        {
            this.client.getSession().write(MaplePacketCreator.serverNotice(5, message));
        }
    }

    public int getTotalSkillLevel(Skill skill)
    {
        if (skill == null)
        {
            return 0;
        }
        SkillEntry ret = this.skills.get(skill);
        if ((ret == null) || (ret.skillevel <= 0))
        {
            return 0;
        }
        return Math.min(skill.getTrueMax(), ret.skillevel + (skill.isBeginnerSkill() ? 0 :
                this.stats.combatOrders + (skill.getMaxLevel() > 10 ? this.stats.incAllskill : 0) + this.stats.getSkillIncrement(skill.getId())));
    }

    public short getLevel()
    {
        return this.level;
    }

    public boolean isAdmin()
    {
        return this.gmLevel >= PlayerGMRank.ADMIN.getLevel();
    }

    public void setLevel(short newLevel)
    {
        if (newLevel <= 0)
        {
            newLevel = 1;
        }
        this.level = newLevel;
    }

    public void checkPartyEffect()
    {
        Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs;
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> mapleBuffStatMapleBuffStatValueHolderPair : getAllEffects())
        {
            buffs = mapleBuffStatMapleBuffStatValueHolderPair;
            allBuffs.add(buffs.getRight());
        }
        for (Iterator localIterator = allBuffs.iterator(); localIterator.hasNext(); buffs = (Pair<MapleBuffStat, MapleBuffStatValueHolder>) localIterator.next())
        {
        }
    }

    public List<MapleBuffStat> getBuffStats(MapleStatEffect effect, long startTime)
    {
        List<MapleBuffStat> ret = new ArrayList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> stateffect : getAllEffects())
        {
            MapleBuffStatValueHolder mbsvh = stateffect.getRight();
            if ((mbsvh.effect.sameSource(effect)) && ((startTime == -1L) || (startTime == mbsvh.startTime)))
            {
                ret.add(stateffect.getLeft());
            }
        }
        return ret;
    }

    public List<SpecialBuffInfo> getSpecialBuffInfo(MapleBuffStat stat)
    {
        List<SpecialBuffInfo> ret = new ArrayList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> stateffect : getAllEffects())
        {
            MapleBuffStatValueHolder mbsvh = stateffect.getRight();
            int skillId = mbsvh.effect.getSourceId();
            if (stateffect.getLeft() == stat)
            {
                ret.add(new SpecialBuffInfo(mbsvh.effect.isSkill() ? skillId : -skillId, mbsvh.value, mbsvh.localDuration));
            }
        }
        return ret;
    }

    public List<SpecialBuffInfo> getSpecialBuffInfo(MapleBuffStat stat, int buffid, int value, int bufflength)
    {
        List<SpecialBuffInfo> ret = new ArrayList<>();
        ret.add(new SpecialBuffInfo(buffid, value, bufflength));
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> stateffect : getAllEffects())
        {
            MapleBuffStatValueHolder mbsvh = stateffect.getRight();
            int skillId = mbsvh.effect.getSourceId();
            if ((stateffect.getLeft() == stat) && (skillId != Math.abs(buffid)))
            {
                ret.add(new SpecialBuffInfo(mbsvh.effect.isSkill() ? skillId : -skillId, mbsvh.value, mbsvh.localDuration));
            }
        }
        return ret;
    }

    private void deregisterBuffStats(List<MapleBuffStat> stats, MapleStatEffect effect, boolean overwrite)
    {
        int effectSize = this.effects.size();
        ArrayList<Pair<MapleBuffStat, MapleBuffStatValueHolder>> effectsToRemove = new ArrayList<>();
        for (MapleBuffStat stat : stats)
        {
            for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
                if ((buffs.getLeft() == stat) && ((effect == null) || (buffs.getRight().effect.sameSource(effect))))
                {
                    effectsToRemove.add(buffs);
                    MapleBuffStatValueHolder mbsvh = buffs.getRight();
                    if ((stat == MapleBuffStat.召唤兽) || (stat == MapleBuffStat.替身术) || (stat == MapleBuffStat.幻灵重生) || (stat == MapleBuffStat.灵魂助力) || (stat == MapleBuffStat.DAMAGE_BUFF) || (stat == MapleBuffStat.地雷) || (stat == MapleBuffStat.indiePad))
                    {
                        int summonId = mbsvh.effect.getSourceId();
                        List<MapleSummon> toRemove = new ArrayList<>();
                        this.visibleMapObjectsLock.writeLock().lock();
                        this.summonsLock.writeLock().lock();
                        try
                        {
                            for (MapleSummon summon : this.summons)
                            {
                                if ((summon.getSkillId() == summonId) || ((stat == MapleBuffStat.地雷) && (summonId == 33101008)) || ((summonId == 35121009) && (summon.getSkillId() == 35121011)) || (((summonId != 86) && (summonId != 88) && (summonId != 91)) || ((summon.getSkillId() == summonId + 999) || (((summonId == 1085) || (summonId == 1087) || (summonId == 1090)) && (summon.getSkillId() == summonId - 999)))))
                                {
                                    this.map.broadcastMessage(tools.packet.SummonPacket.removeSummon(summon, overwrite));
                                    this.map.removeMapObject(summon);
                                    this.visibleMapObjects.remove(summon);
                                    toRemove.add(summon);
                                }
                            }
                            for (MapleSummon ss : toRemove)
                            {
                                this.summons.remove(ss);
                            }
                        }
                        finally
                        {
                            this.summonsLock.writeLock().unlock();
                            this.visibleMapObjectsLock.writeLock().unlock();
                        }
                        if ((summonId == 3111005) || (summonId == 3211005))
                        {
                            cancelEffectFromBuffStat(MapleBuffStat.精神连接);
                        }
                    }
                    else if (stat == MapleBuffStat.龙之力)
                    {
                        this.lastDragonBloodTime = 0L;
                    }
                    else if ((stat == MapleBuffStat.恢复效果) || (mbsvh.effect.getSourceId() == 35121005))
                    {
                        this.lastRecoveryTime = 0L;
                    }
                    else if ((stat == MapleBuffStat.导航辅助) || (stat == MapleBuffStat.神秘瞄准术))
                    {
                        this.linkMobs.clear();
                    }
                    else if (stat == MapleBuffStat.恶魔恢复)
                    {
                        this.lastRecoveryTimeEM = 0L;
                    }
                }
        }
        MapleBuffStat stat;
        int toRemoveSize = effectsToRemove.size();
        for (Object toRemove : effectsToRemove)
        {
            if (this.effects.contains(toRemove))
            {
                if (((MapleBuffStatValueHolder) ((Pair) toRemove).getRight()).schedule != null)
                {
                    ((MapleBuffStatValueHolder) ((Pair) toRemove).getRight()).schedule.cancel(false);
                    ((MapleBuffStatValueHolder) ((Pair) toRemove).getRight()).schedule = null;
                }
                this.effects.remove(toRemove);
            }
        }
        effectsToRemove.clear();
        boolean ok = effectSize - this.effects.size() == toRemoveSize;
        if (isShowPacket())
        {
            dropSpouseMessage(20, "取消注册的BUFF效果 - 以前BUFF总数: " + effectSize + " 现在BUFF总数 " + this.effects.size() + " 取消的BUFF数量: " + toRemoveSize + " 是否相同: " + ok);
        }
        if (!ok)
        {
            FileoutputUtil.log("取消BUFF错误.txt", getName() + " - " + getJob() + " 取消BUFF出现错误 技能ID: " + (effect != null ? Integer.valueOf(effect.getSourceId()) : "???"), true);
        }
    }

    public void cancelEffect(MapleStatEffect effect, boolean overwrite, long startTime)
    {
        if (effect == null)
        {
            return;
        }
        cancelEffect(effect, overwrite, startTime, effect.getStatups());
    }

    public void cancelEffect(MapleStatEffect effect, boolean overwrite, long startTime, List<Pair<MapleBuffStat, Integer>> statups)
    {
        if (effect == null) return;
        List<MapleBuffStat> buffstats;
        if (!overwrite)
        {
            buffstats = getBuffStats(effect, startTime);
        }
        else
        {
            buffstats = new ArrayList(statups.size());
            for (Pair<MapleBuffStat, Integer> statup : statups)
            {
                buffstats.add(statup.getLeft());
            }
        }
        if (ServerProperties.ShowPacket())
        {
            log.info("取消技能BUFF: - buffstats.size() " + buffstats.size());
        }
        if (buffstats.size() <= 0)
        {
            if (effect.is黑暗灵气())
            {
                cancelEffectFromBuffStat(MapleBuffStat.黑暗灵气);
            }
            else if (effect.is黄色灵气())
            {
                cancelEffectFromBuffStat(MapleBuffStat.黄色灵气);
            }
            else if (effect.is蓝色灵气())
            {
                cancelEffectFromBuffStat(MapleBuffStat.蓝色灵气);
            }
            return;
        }
        if (ServerProperties.ShowPacket())
        {
            log.info("开始取消技能BUFF: - 1");
        }
        if ((effect.is终极无限()) && (getBuffedValue(MapleBuffStat.终极无限) != null))
        {
            int duration = Math.max(effect.getDuration(), effect.alchemistModifyVal(this, effect.getDuration(), false));
            long start = getBuffedStartTime(MapleBuffStat.终极无限);
            duration += (int) (start - System.currentTimeMillis());
            if (duration > 0)
            {
                int neworbcount = getBuffedValue(MapleBuffStat.终极无限) + effect.getDamage();
                List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.终极无限, neworbcount));
                setBuffedValue(MapleBuffStat.终极无限, neworbcount);
                this.client.getSession().write(BuffPacket.giveBuff(effect.getSourceId(), duration, stat, effect, this));
                addHP((int) (effect.getHpR() * this.stats.getCurrentMaxHp()));
                addMP((int) (effect.getMpR() * this.stats.getCurrentMaxMp(getJob())));
                setSchedule(MapleBuffStat.终极无限, server.Timer.BuffTimer.getInstance().schedule(new server.MapleStatEffect.CancelEffectAction(this, effect, start, stat),
                        effect.alchemistModifyVal(this, 4000, false)));
                return;
            }
        }

        deregisterBuffStats(buffstats, effect, overwrite);
        if (effect.is时空门())
        {
            if (!getDoors().isEmpty())
            {
                removeDoor();
                silentPartyUpdate();
            }
        }
        else if (effect.is机械传送门())
        {
            if (!getMechDoors().isEmpty())
            {
                removeMechDoor();
            }
        }
        else if (effect.is骑兽技能_())
        {
            getMount().cancelSchedule();
        }
        else if (effect.is机械骑兽())
        {
            cancelEffectFromBuffStat(MapleBuffStat.金属机甲);
        }
        else if ((effect.is矛连击强化()) && (!overwrite))
        {
            this.aranCombo = 0;
        }
        else if (effect.is战法灵气())
        {
            cancelEffectFromBuffStat(MapleBuffStat.幻灵霸体);
        }

        cancelPlayerBuffs(buffstats, overwrite);
        if ((!overwrite) && (effect.is隐藏术()) && (this.client.getChannelServer().getPlayerStorage().getCharacterById(getId()) != null))
        {
            this.map.broadcastMessage(this, MaplePacketCreator.spawnPlayerMapobject(this), false);
        }

        if ((effect.getSourceId() == 35121013) && (!overwrite))
        {
            SkillFactory.getSkill(35001002).getEffect(getTotalSkillLevel(35001002)).applyTo(this);
        }
        if (isShowPacket())
        {
            dropMessage(5, "取消BUFF效果 - 当前BUFF总数: " + this.effects.size() + " 技能: " + effect.getSourceId());
        }
    }

    public void cancelBuffStats(MapleBuffStat... stat)
    {
        List<MapleBuffStat> buffStatList = java.util.Arrays.asList(stat);
        deregisterBuffStats(buffStatList, null, false);
        cancelPlayerBuffs(buffStatList, false);
    }

    public void cancelEffectFromBuffStat(MapleBuffStat stat)
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            if (buffs.getLeft() == stat)
            {
                allBuffs.add(buffs.getRight());
            }
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            cancelEffect(mbsvh.effect, false, -1L);
        }
        allBuffs.clear();
    }

    public void cancelEffectFromBuffStat(MapleBuffStat stat, int from)
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            if ((buffs.getLeft() == stat) && (buffs.getRight().fromChrId == from))
            {
                allBuffs.add(buffs.getRight());
            }
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            cancelEffect(mbsvh.effect, false, -1L);
        }
        allBuffs.clear();
    }

    private void cancelPlayerBuffs(List<MapleBuffStat> buffstats, boolean overwrite)
    {
        boolean write = (this.client != null) && (this.client.getChannelServer() != null) && (this.client.getChannelServer().getPlayerStorage().getCharacterById(getId()) != null);
        if (write)
        {
            if (buffstats.contains(MapleBuffStat.导航辅助))
            {
                this.client.getSession().write(BuffPacket.cancelBuff(MapleBuffStat.导航辅助));
                return;
            }
            if (buffstats.contains(MapleBuffStat.召唤兽))
            {
                buffstats.remove(MapleBuffStat.召唤兽);
                if (buffstats.size() <= 0)
                {
                    return;
                }
            }
            if (overwrite)
            {
                List<MapleBuffStat> buffStatX = new ArrayList<>();
                for (MapleBuffStat stat : buffstats)
                {
                    if (stat.canStack())
                    {
                        buffStatX.add(stat);
                    }
                }
                if ((buffStatX.size() <= 0) || (buffstats.contains(MapleBuffStat.击杀点数)))
                {
                    if (isShowPacket())
                    {
                        dropMessage(5, "取消BUFF效果 - 不发送封包");
                    }
                    return;
                }
                buffstats = buffStatX;
            }

            if (isShowPacket())
            {
                dropMessage(5, "取消BUFF效果 - 发送封包 - 是否注册BUFF时: " + overwrite);
            }
            this.client.getSession().write(BuffPacket.cancelBuff(buffstats, this));
            this.map.broadcastMessage(this, BuffPacket.cancelForeignBuff(getId(), buffstats), false);
            this.stats.recalcLocalStats(this);
        }
    }

    public void dispel()
    {
        if (!isHidden())
        {
            LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
            for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
            {
                allBuffs.add(buffs.getRight());
            }
            for (MapleBuffStatValueHolder mbsvh : allBuffs)
            {
                if ((mbsvh.effect.isSkill()) && (mbsvh.schedule != null) && (!mbsvh.effect.isMorph()) && (!mbsvh.effect.isGmBuff()) && (!mbsvh.effect.is骑兽技能()) && (!mbsvh.effect.isMechChange()) && (!mbsvh.effect.is能量获得()) && (!mbsvh.effect.is矛连击强化()) && (!mbsvh.effect.is尖兵支援()) && (!mbsvh.effect.isNotRemoved()))
                {
                    cancelEffect(mbsvh.effect, false, mbsvh.startTime);
                }
            }
        }
    }

    public void dispelSkill(int skillId)
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            if ((mbsvh.effect.isSkill()) && (mbsvh.effect.getSourceId() == skillId))
            {
                cancelEffect(mbsvh.effect, false, mbsvh.startTime);
                break;
            }
        }
    }

    public void dispelSummons()
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            if (mbsvh.effect.getSummonMovementType() != null)
            {
                cancelEffect(mbsvh.effect, false, mbsvh.startTime);
            }
        }
    }

    public void dispelSummons(int skillId)
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            if ((mbsvh.effect.getSummonMovementType() != null) && (mbsvh.effect.getSourceId() == skillId))
            {
                cancelEffect(mbsvh.effect, false, mbsvh.startTime);
                break;
            }
        }
    }

    public void dispelBuff(int buffId)
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            if (mbsvh.effect.getSourceId() == buffId)
            {
                cancelEffect(mbsvh.effect, false, mbsvh.startTime);
                break;
            }
        }
    }

    public void cancelAllBuffs_()
    {
        this.effects.clear();
    }

    public void cancelAllBuffs()
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            cancelEffect(mbsvh.effect, false, mbsvh.startTime);
        }
    }

    public void cancelMorphs()
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            switch (mbsvh.effect.getSourceId())
            {
                case 61111008:
                case 61120008:
                    return;
            }
            if (mbsvh.effect.isMorph())
            {
                cancelEffect(mbsvh.effect, false, mbsvh.startTime);
            }
        }
    }

    public int getMorphState()
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            if (mbsvh.effect.isMorph())
            {
                return mbsvh.effect.getSourceId();
            }
        }
        return -1;
    }

    public void silentGiveBuffs(List<PlayerBuffValueHolder> buffs)
    {
        if (buffs == null)
        {
            return;
        }
        for (PlayerBuffValueHolder mbsvh : buffs)
        {
            mbsvh.effect.silentApplyBuff(this, mbsvh.startTime, mbsvh.localDuration, mbsvh.statup, mbsvh.fromChrId);
        }
    }

    public List<PlayerBuffValueHolder> getAllBuffs()
    {
        List<PlayerBuffValueHolder> ret = new ArrayList<>();
        Map<Pair<Integer, Byte>, Integer> alreadyDone = new HashMap<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> mbsvh : getAllEffects())
        {
            Pair<Integer, Byte> key = new Pair(mbsvh.getRight().effect.getSourceId(), mbsvh.getRight().effect.getLevel());
            if (alreadyDone.containsKey(key))
            {
                ret.get(alreadyDone.get(key)).statup.add(new Pair(mbsvh.getLeft(), mbsvh.getRight().value));
            }
            else
            {
                alreadyDone.put(key, ret.size());
                ArrayList<Pair<MapleBuffStat, Integer>> list = new ArrayList<>();
                list.add(new Pair(mbsvh.getLeft(), mbsvh.getRight().value));
                ret.add(new PlayerBuffValueHolder(mbsvh.getRight().startTime, mbsvh.getRight().effect, list, mbsvh.getRight().localDuration, mbsvh.getRight().fromChrId));
            }
        }
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> mbsvh : getAllEffects())
        {
            if (mbsvh.getRight().schedule != null)
            {
                mbsvh.getRight().schedule.cancel(false);
                mbsvh.getRight().schedule = null;
            }
        }
        return ret;
    }

    public void cancelMagicDoor()
    {
        LinkedList<MapleBuffStatValueHolder> allBuffs = new LinkedList<>();
        for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
        {
            allBuffs.add(buffs.getRight());
        }
        for (MapleBuffStatValueHolder mbsvh : allBuffs)
        {
            if (mbsvh.effect.is时空门())
            {
                cancelEffect(mbsvh.effect, false, mbsvh.startTime);
                break;
            }
        }
    }

    public int getGMLevel()
    {
        return this.gmLevel;
    }

    public boolean canDOT(long now)
    {
        return (this.lastDOTTime > 0L) && (this.lastDOTTime + 8000L < now);
    }

    public boolean hasDOT()
    {
        return this.dotHP > 0;
    }

    public void doDOT()
    {
        addHP(-(this.dotHP * 4));
        this.dotHP = 0;
        this.lastDOTTime = 0L;
    }

    public void addHP(int delta)
    {
        if (this.stats.setHp(this.stats.getHp() + delta, this))
        {
            updateSingleStat(MapleStat.HP, this.stats.getHp());
        }
    }

    public void updateSingleStat(MapleStat stat, long newval)
    {
        updateSingleStat(stat, newval, false);
    }

    public void updateSingleStat(MapleStat stat, long newval, boolean itemReaction)
    {
        Map<MapleStat, Long> statup = new EnumMap(MapleStat.class);
        statup.put(stat, newval);
        this.client.getSession().write(MaplePacketCreator.updatePlayerStats(statup, itemReaction, this));
    }

    public void setDOT(int d, int source, int sourceLevel)
    {
        this.dotHP = d;
        addHP(-(this.dotHP * 4));
        this.map.broadcastMessage(MaplePacketCreator.getPVPMist(this.id, source, sourceLevel, d));
        this.lastDOTTime = System.currentTimeMillis();
    }

    public void doDragonBlood()
    {
        MapleStatEffect bloodEffect = getStatForBuff(MapleBuffStat.龙之力);
        if (bloodEffect == null)
        {
            this.lastDragonBloodTime = 0L;
            return;
        }
        prepareDragonBlood();
        if (this.stats.getHp() - bloodEffect.getStr() <= 1)
        {
            cancelBuffStats(MapleBuffStat.龙之力);
        }
        else
        {
            addHP(-bloodEffect.getStr());
            this.client.getSession().write(MaplePacketCreator.showOwnBuffEffect(bloodEffect.getSourceId(), 7, getLevel(), bloodEffect.getLevel()));
            this.map.broadcastMessage(this, MaplePacketCreator.showBuffeffect(getId(), bloodEffect.getSourceId(), 7, getLevel(), bloodEffect.getLevel()), false);
        }
    }

    public boolean canBlood(long now)
    {
        return (this.lastDragonBloodTime > 0L) && (this.lastDragonBloodTime + 4000L < now);
    }

    public void doRecovery()
    {
        MapleStatEffect bloodEffect = getStatForBuff(MapleBuffStat.恢复效果);
        if (bloodEffect == null)
        {
            bloodEffect = getStatForBuff(MapleBuffStat.金属机甲);
            if (bloodEffect == null)
            {
                this.lastRecoveryTime = 0L;
            }
            else if (bloodEffect.getSourceId() == 35121005)
            {
                prepareRecovery();
                if (this.stats.getMp() < bloodEffect.getU())
                {
                    cancelEffectFromBuffStat(MapleBuffStat.骑兽技能);
                    cancelEffectFromBuffStat(MapleBuffStat.金属机甲);
                }
                else
                {
                    addMP(-bloodEffect.getU());
                }
            }
        }
        else
        {
            prepareRecovery();
            if (this.stats.getHp() >= this.stats.getCurrentMaxHp())
            {
                cancelEffectFromBuffStat(MapleBuffStat.恢复效果);
            }
            else
            {
                healHP(bloodEffect.getX());
            }
        }
    }

    public boolean canRecover(long now)
    {
        return (this.lastRecoveryTime > 0L) && (this.lastRecoveryTime + 5000L < now);
    }

    public void doRecoveryEM()
    {
        MapleStatEffect bloodEffect = getStatForBuff(MapleBuffStat.恶魔恢复);
        if (bloodEffect == null)
        {
            this.lastRecoveryTimeEM = 0L;
            return;
        }
        prepareRecoveryEM();
        if (this.stats.getHp() < this.stats.getCurrentMaxHp())
        {
            healHP(bloodEffect.getX() * (this.stats.getCurrentMaxHp() / 100));
        }
    }

    public MapleStatEffect getStatForBuff(MapleBuffStat stat)
    {
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return null;
        }
        return mbsvh.effect;
    }

    public void healHP(int delta)
    {
        addHP(delta);
        this.client.getSession().write(MaplePacketCreator.showOwnHpHealed(delta));
        getMap().broadcastMessage(this, MaplePacketCreator.showHpHealed(getId(), delta), false);
    }

    public boolean canRecoverEM(long now)
    {
        MapleStatEffect bloodEffect = getStatForBuff(MapleBuffStat.恶魔恢复);
        if (bloodEffect == null)
        {
            return false;
        }
        int time = bloodEffect.getY();
        return (this.lastRecoveryTimeEM > 0L) && (this.lastRecoveryTimeEM + time < now);
    }

    public void startPower()
    {
        if (this.specialStats.isUsePower())
        {
            this.specialStats.changePower(false);
        }
        else
        {
            this.specialStats.changePower(true);
        }
        this.client.getSession().write(BuffPacket.startPower(this.specialStats.isUsePower()));
        this.client.getSession().write(MaplePacketCreator.showSpecialEffect(this.specialStats.isUsePower() ? 59 : 60));
    }

    public void doRecoveryPower()
    {
        if (!GameConstants.is尖兵(getJob()))
        {
            return;
        }
        this.specialStats.prepareRecoveryPowerTime();
        addPowerCount(1);
    }

    public void addPowerCount(int delta)
    {
        Skill skill = SkillFactory.getSkill(30020232);
        int skilllevel = getTotalSkillLevel(skill);
        if ((!GameConstants.is尖兵(getJob())) || (skill == null) || (skilllevel <= 0))
        {
            return;
        }
        if (setPowerCount(this.specialStats.getPowerCount() + delta))
        {
            this.stats.recalcLocalStats(this);
            this.client.getSession().write(BuffPacket.updatePowerCount(30020232, this.specialStats.getPowerCount()));
        }
    }

    public boolean setPowerCount(int count)
    {
        int oldPower = this.specialStats.getPowerCount();
        int tempPower = count;
        if (tempPower < 0)
        {
            tempPower = 0;
        }
        if (tempPower > getPowerCountByJob())
        {
            tempPower = getPowerCountByJob();
        }
        this.specialStats.setPowerCount(tempPower);
        return this.specialStats.getPowerCount() != oldPower;
    }

    public int getPowerCountByJob()
    {
        switch (getJob())
        {
            case 3610:
                return 10;
            case 3611:
                return 15;
            case 3612:
                return 20;
        }
        return 5;
    }

    public boolean canRecoverPower(long now)
    {
        Skill skill = SkillFactory.getSkill(30020232);
        int skilllevel = getTotalSkillLevel(skill);
        if ((!GameConstants.is尖兵(getJob())) || (skill == null) || (skilllevel <= 0))
        {
            return false;
        }
        return (this.specialStats.getLastRecoveryPowerTime() > 0L) && (this.specialStats.getLastRecoveryPowerTime() + 4000L < now);
    }

    public int getEnergyCount()
    {
        if (getBuffedValue(MapleBuffStat.能量获得) == null)
        {
            return 0;
        }
        return getBuffedValue(MapleBuffStat.能量获得);
    }

    public Integer getBuffedValue(MapleBuffStat stat)
    {
        if (stat.canStack())
        {
            int value = 0;
            boolean find = false;
            for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
            {
                if (buffs.getLeft() == stat)
                {
                    find = true;
                    value += buffs.getRight().value;
                }
            }
            return find ? value : null;
        }
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return null;
        }
        return mbsvh.value;
    }

    public void handleEnergyCharge(int targets)
    {
        Skill echskill_2 = SkillFactory.getSkill(5100015);
        Skill echskill_3 = SkillFactory.getSkill(5110014);
        Skill echskill_4 = SkillFactory.getSkill(5120018);
        MapleStatEffect effect;
        if (getSkillLevel(echskill_4) > 0)
        {
            effect = echskill_4.getEffect(getTotalSkillLevel(echskill_4));
        }
        else
        {
            if (getSkillLevel(echskill_3) > 0)
            {
                effect = echskill_3.getEffect(getTotalSkillLevel(echskill_3));
            }
            else
            {
                if (getSkillLevel(echskill_2) > 0) effect = echskill_2.getEffect(getTotalSkillLevel(echskill_2));
                else return;
            }
        }
        if ((targets > 0) && (!this.specialStats.isEnergyfull()))
        {
            if (getBuffedValue(MapleBuffStat.能量获得) == null)
            {
                effect.applyEnergyBuff(this);
                return;
            }

            int energy = getBuffedIntValue(MapleBuffStat.能量获得);
            if (energy < 10000)
            {
                energy += effect.getX() * targets;
                if (energy >= 10000)
                {
                    energy = 10000;
                    this.specialStats.changeEnergyfull(true);
                }
                setBuffedValue(MapleBuffStat.能量获得, energy);

                this.client.getSession().write(MaplePacketCreator.showOwnBuffEffect(effect.getSourceId(), 2, getLevel(), effect.getLevel()));
                this.client.getSession().write(BuffPacket.giveEnergyCharge(energy, effect.getSourceId(), this.specialStats.isEnergyfull(), false));

                this.map.broadcastMessage(this, MaplePacketCreator.showBuffeffect(this.id, effect.getSourceId(), 2, getLevel(), effect.getLevel()), false);
                this.map.broadcastMessage(this, BuffPacket.showEnergyCharge(this.id, energy, effect.getSourceId(), this.specialStats.isEnergyfull(), false), false);
            }
        }
    }

    public void handleEnergyConsume(int mobCount, int skillId)
    {
        MapleStatEffect echeffect = getStatForBuff(MapleBuffStat.能量获得);
        if ((skillId == 0) || (!this.specialStats.isEnergyfull()) || (echeffect == null))
        {
            return;
        }
        Skill skill = SkillFactory.getSkill(skillId);
        int skillLevel = getTotalSkillLevel(GameConstants.getLinkedAttackSkill(skillId));
        MapleStatEffect effect = skill.getEffect(skillLevel);
        if ((effect == null) || (effect.getForceCon() <= 0))
        {
            return;
        }

        int energy = getBuffedIntValue(MapleBuffStat.能量获得);
        energy -= effect.getForceCon() * mobCount;
        if (energy <= 0)
        {
            energy = 0;
            this.specialStats.changeEnergyfull(false);
        }
        setBuffedValue(MapleBuffStat.能量获得, energy);
        this.client.getSession().write(BuffPacket.giveEnergyCharge(energy, echeffect.getSourceId(), this.specialStats.isEnergyfull(), true));
        this.map.broadcastMessage(this, BuffPacket.showEnergyCharge(this.id, energy, echeffect.getSourceId(), this.specialStats.isEnergyfull(), true), false);
    }

    public int getOrbCount()
    {
        if (getBuffedValue(MapleBuffStat.斗气集中) == null)
        {
            return 0;
        }
        return getBuffedValue(MapleBuffStat.斗气集中);
    }

    public void handleOrbgain()
    {
        int orbcount = getBuffedValue(MapleBuffStat.斗气集中);
        Skill combos = SkillFactory.getSkill(1101013);
        Skill advcombo = SkillFactory.getSkill(1120003);

        int advComboSkillLevel = getTotalSkillLevel(advcombo);
        MapleStatEffect ceffect;
        if (advComboSkillLevel > 0)
        {
            ceffect = advcombo.getEffect(advComboSkillLevel);
        }
        else
        {
            if (getSkillLevel(combos) > 0) ceffect = combos.getEffect(getTotalSkillLevel(combos));
            else return;
        }
        if (orbcount < ceffect.getX() + 1)
        {
            int neworbcount = orbcount + 1;
            if ((advComboSkillLevel > 0) && (ceffect.makeChanceResult()) && (neworbcount < ceffect.getX() + 1))
            {
                neworbcount++;
            }

            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.斗气集中, neworbcount));
            setBuffedValue(MapleBuffStat.斗气集中, neworbcount);
            int duration = ceffect.getDuration();
            duration += (int) (getBuffedStartTime(MapleBuffStat.斗气集中) - System.currentTimeMillis());
            this.client.getSession().write(BuffPacket.giveBuff(combos.getId(), duration, stat, ceffect, this));
            this.map.broadcastMessage(this, BuffPacket.giveForeignBuff(getId(), stat, ceffect), false);
        }
    }

    public void handleOrbconsume(int howmany)
    {
        Skill combos = SkillFactory.getSkill(1101013);
        if (getSkillLevel(combos) <= 0)
        {
            return;
        }
        MapleStatEffect effect = getStatForBuff(MapleBuffStat.斗气集中);
        if (effect == null)
        {
            return;
        }
        List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.斗气集中, Math.max(1, getBuffedValue(MapleBuffStat.斗气集中).intValue() - howmany)));
        setBuffedValue(MapleBuffStat.斗气集中, Math.max(1, getBuffedValue(MapleBuffStat.斗气集中) - howmany));
        int duration = effect.getDuration();
        duration += (int) (getBuffedStartTime(MapleBuffStat.斗气集中) - System.currentTimeMillis());
        this.client.getSession().write(BuffPacket.giveBuff(combos.getId(), duration, stat, effect, this));
        this.map.broadcastMessage(this, BuffPacket.giveForeignBuff(getId(), stat, effect), false);
    }

    public int getLightTotal()
    {
        return this.specialStats.getLightTotal();
    }

    public int getLightType()
    {
        return this.specialStats.getLightType();
    }

    public int getDarkTotal()
    {
        return this.specialStats.getDarkTotal();
    }

    public int getDarkType()
    {
        return this.specialStats.getDarkType();
    }

    public void handleLuminous(int skillId)
    {
        int skillMode = constants.SkillConstants.getLuminousSkillMode(skillId);
        if ((skillMode < 0) || (skillMode == 20040219) || (getSkillLevel(skillMode) <= 0))
        {
            return;
        }
        boolean isLightSkill = skillMode == 20040216;
        if (isLightSkill)
        {
            this.specialStats.gainLightTotal(Randomizer.nextInt(200) + 100);
            if (this.specialStats.getLightTotal() > 10000)
            {
                if (this.specialStats.getLightType() < 5)
                {
                    this.specialStats.setLightTotal(0);
                    this.specialStats.gainLightType(1);
                    if (this.specialStats.getLightType() > 5)
                    {
                        this.specialStats.setLightType(5);
                    }
                }
                else
                {
                    this.specialStats.setLightTotal(10000);
                    this.specialStats.setLightType(5);
                }
            }
        }
        else
        {
            this.specialStats.gainDarkTotal(Randomizer.nextInt(200) + 100);
            if (this.specialStats.getDarkTotal() > 10000)
            {
                if (this.specialStats.getDarkType() < 5)
                {
                    this.specialStats.setDarkTotal(0);
                    this.specialStats.gainDarkType(1);
                    if (this.specialStats.getDarkType() > 5)
                    {
                        this.specialStats.setDarkType(5);
                    }
                }
                else
                {
                    this.specialStats.setDarkTotal(10000);
                    this.specialStats.setDarkType(5);
                }
            }
        }
        MapleStatEffect effect = getStatForBuff(MapleBuffStat.光暗转换);
        if (effect == null)
        {
            effect = SkillFactory.getSkill(skillMode).getEffect(getSkillLevel(skillMode));
            effect.applyTo(this);
        }
        else if (effect.getSourceId() == skillMode)
        {
            addHP(this.stats.getMaxHp() / 100);
        }
        this.client.getSession().write(BuffPacket.updateLuminousGauge(this));
    }

    public void changeLuminousMode(int skillid)
    {
        boolean isLightSkill = skillid == 20040216;
        boolean isDarkSkill = skillid == 20040217;
        if (isLightSkill)
        {
            this.specialStats.gainLightType(-1);
        }
        else if (isDarkSkill)
        {
            this.specialStats.gainDarkType(-1);
        }
        else
        {
            return;
        }
        if ((this.specialStats.getLightType() < 0) || (this.specialStats.getDarkType() < 0))
        {
            return;
        }
        MapleStatEffect effect = SkillFactory.getSkill(skillid).getEffect(getSkillLevel(skillid));
        if (effect != null)
        {
            effect.applyTo(this);
        }
        this.client.getSession().write(BuffPacket.updateLuminousGauge(this));
    }

    public void handleBlackBless()
    {
        if (this.lastBlessOfDarknessTime == 0L)
        {
            this.lastBlessOfDarknessTime = System.currentTimeMillis();
        }
        Skill skill = SkillFactory.getSkill(27100003);
        int skilllevel = getTotalSkillLevel(skill);
        if (skilllevel <= 0)
        {
            return;
        }
        MapleStatEffect effect = skill.getEffect(skilllevel);
        if (getStatForBuff(MapleBuffStat.黑暗祝福) == null)
        {
            effect.applyTo(this);
            return;
        }
        if (this.lastBlessOfDarknessTime + effect.getDuration() < System.currentTimeMillis())
        {
            int count = getBuffedValue(MapleBuffStat.黑暗祝福);
            if (count < 3)
            {
                count++;
            }
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.黑暗祝福, count));
            setBuffedValue(MapleBuffStat.黑暗祝福, count);
            int duration = 2100000000;
            this.client.getSession().write(MaplePacketCreator.showBlessOfDarkness(skill.getId()));
            this.client.getSession().write(BuffPacket.giveBuff(skill.getId(), duration, stat, effect, this));
            this.lastBlessOfDarknessTime = System.currentTimeMillis();
        }
    }

    public void handleBlackBlessLost(int howmany)
    {
        Skill skill = SkillFactory.getSkill(27100003);
        if (getSkillLevel(skill) <= 0)
        {
            cancelEffectFromBuffStat(MapleBuffStat.黑暗祝福);
            return;
        }
        MapleStatEffect effect = getStatForBuff(MapleBuffStat.黑暗祝福);
        if (effect == null)
        {
            return;
        }
        this.lastBlessOfDarknessTime = System.currentTimeMillis();
        int count = getBuffedValue(MapleBuffStat.黑暗祝福);
        count -= howmany;
        if (count <= 0)
        {
            cancelEffectFromBuffStat(MapleBuffStat.黑暗祝福);
        }
        else
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.黑暗祝福, count));
            setBuffedValue(MapleBuffStat.黑暗祝福, count);
            int duration = 2100000000;
            this.client.getSession().write(MaplePacketCreator.showBlessOfDarkness(skill.getId()));
            this.client.getSession().write(BuffPacket.giveBuff(skill.getId(), duration, stat, effect, this));
        }
    }

    public void handleDarkCrescendo()
    {
        MapleStatEffect dkeffect = getStatForBuff(MapleBuffStat.黑暗高潮);
        if ((dkeffect != null) && (dkeffect.getSourceId() == 27121005))
        {
            int orbcount = getBuffedValue(MapleBuffStat.黑暗高潮);
            Skill skill = SkillFactory.getSkill(27121005);
            if (getSkillLevel(skill) > 0)
            {
                MapleStatEffect effect = skill.getEffect(getTotalSkillLevel(skill));
                if ((orbcount < effect.getX()) && (effect.makeChanceResult()))
                {
                    int neworbcount = orbcount + 1;
                    setBuffedValue(MapleBuffStat.黑暗高潮, neworbcount);
                    int duration = effect.getDuration();
                    duration += (int) (getBuffedStartTime(MapleBuffStat.黑暗高潮) - System.currentTimeMillis());
                    this.client.getSession().write(BuffPacket.giveDarkCrescendo(skill.getId(), duration, neworbcount));
                }
            }
        }
    }

    public boolean canMorphLost(long now)
    {
        if ((getJob() >= 6100) && (getJob() <= 6112))
        {
            return (this.specialStats.getLastMorphLostTime() > 0L) && (this.specialStats.getMorphCount() > 0) && (this.specialStats.getLastMorphLostTime() + 20000L < now);
        }
        return false;
    }

    public void morphLostTask()
    {
        if ((getJob() >= 6100) && (getJob() <= 6112))
        {
            if (this.specialStats.getMorphCount() > 0)
            {
                this.specialStats.gainMorphCount(-1);
                this.client.getSession().write(BuffPacket.give狂龙变形值(this.specialStats.getMorphCount()));
            }
            this.specialStats.prepareMorphLostTime();
        }
    }

    public int getMorphCount()
    {
        return this.specialStats.getMorphCount();
    }

    public void setMorphCount(int amount)
    {
        this.specialStats.setMorphCount(amount);
        if (this.specialStats.getMorphCount() <= 0)
        {
            this.specialStats.setMorphCount(0);
            this.client.getSession().write(BuffPacket.cancelBuff(MapleBuffStat.变形值));
        }
    }

    public void handleMorphCharge(int targets)
    {
        Skill mchskill = SkillFactory.getSkill(60000219);
        int skilllevel = getTotalSkillLevel(mchskill);
        if (skilllevel > 0)
        {
            MapleStatEffect mcheff = mchskill.getEffect(skilllevel);
            if ((targets > 0) && (mcheff != null))
            {
                this.specialStats.prepareMorphLostTime();
                int maxCount = getJob() == 6110 ? 400 : getJob() == 6100 ? 100 : 700;
                if (this.specialStats.getMorphCount() < maxCount)
                {
                    this.specialStats.gainMorphCount(targets);
                    if (this.specialStats.getMorphCount() >= maxCount)
                    {
                        this.specialStats.setMorphCount(maxCount);
                    }
                    this.client.getSession().write(BuffPacket.give狂龙变形值(this.specialStats.getMorphCount()));
                    if (isAdmin())
                    {
                        this.map.broadcastMessage(this, BuffPacket.show狂龙变形值(getId(), this.specialStats.getMorphCount()), false);
                    }
                }
            }
        }
    }

    public void silentEnforceMaxHpMp()
    {
        this.stats.setMp(this.stats.getMp(), this);
        this.stats.setHp(this.stats.getHp(), true, this);
    }

    public void enforceMaxHpMp()
    {
        Map<MapleStat, Long> statups = new EnumMap(MapleStat.class);
        if (this.stats.getMp() > this.stats.getCurrentMaxMp(getJob()))
        {
            this.stats.setMp(this.stats.getMp(), this);
            statups.put(MapleStat.MP, (long) this.stats.getMp());
        }
        if (this.stats.getHp() > this.stats.getCurrentMaxHp())
        {
            this.stats.setHp(this.stats.getHp(), this);
            statups.put(MapleStat.HP, (long) this.stats.getHp());
        }
        if (statups.size() > 0)
        {
            this.client.getSession().write(MaplePacketCreator.updatePlayerStats(statups, this));
        }
    }

    public byte getInitialSpawnpoint()
    {
        return this.initialSpawnPoint;
    }

    public String getBlessOfFairyOrigin()
    {
        return this.BlessOfFairy_Origin;
    }

    public String getBlessOfEmpressOrigin()
    {
        return this.BlessOfEmpress_Origin;
    }

    public int getFallCounter()
    {
        return this.fallcounter;
    }

    public void setFallCounter(int fallcounter)
    {
        this.fallcounter = fallcounter;
    }

    public long getExp()
    {
        return this.exp.get();
    }

    public void setExp(long exp)
    {
        this.exp.set(exp);
    }

    public int getRemainingSp()
    {
        return this.remainingSp[GameConstants.getSkillBookByJob(this.job)];
    }

    public void setRemainingSp(int remainingSp)
    {
        this.remainingSp[GameConstants.getSkillBookByJob(this.job)] = remainingSp;
    }

    public int getRemainingSp(int skillbook)
    {
        return this.remainingSp[skillbook];
    }

    public int[] getRemainingSps()
    {
        return this.remainingSp;
    }

    public int getRemainingSpSize()
    {
        int ret = 0;
        for (int value : this.remainingSp)
        {
            if (value > 0)
            {
                ret++;
            }
        }
        return ret;
    }

    public byte getSkinColor()
    {
        return this.skinColor;
    }

    public void setSkinColor(byte skinColor)
    {
        this.skinColor = skinColor;
    }

    public byte getGender()
    {
        return this.gender;
    }

    public void setGender(byte gender)
    {
        this.gender = gender;
    }

    public byte getSecondGender()
    {
        if (GameConstants.is神之子(this.job))
        {
            return 1;
        }
        return this.gender;
    }

    public void changeZeroLook()
    {
        if (!GameConstants.is神之子(this.job))
        {
            return;
        }
        setKeyValue("Zero_Look", isZeroSecondLook() ? "0" : "1");
        this.map.broadcastMessage(this, MaplePacketCreator.updateZeroLook(this), false);
        this.stats.recalcLocalStats(this);
    }

    public void setKeyValue(String key, String values)
    {
        this.keyValue.put(key, values);
        this.changed_keyValue = true;
    }

    public boolean isZeroSecondLook()
    {
        return getZeroLook() == 1;
    }

    public byte getZeroLook()
    {
        if (!GameConstants.is神之子(this.job))
        {
            return -1;
        }
        if (getKeyValue("Zero_Look") == null)
        {
            setKeyValue("Zero_Look", "0");
        }
        return Byte.parseByte(getKeyValue("Zero_Look"));
    }

    public String getKeyValue(String key)
    {
        if (this.keyValue.containsKey(key))
        {
            return this.keyValue.get(key);
        }
        return null;
    }

    public int getHair()
    {
        return this.hair;
    }

    public void setHair(int hair)
    {
        this.hair = hair;
    }

    public int getSecondHair()
    {
        if (GameConstants.is神之子(this.job))
        {
            if (getKeyValue("Second_Hair") == null)
            {
                setKeyValue("Second_Hair", "37623");
            }
            return Integer.parseInt(getKeyValue("Second_Hair"));
        }
        return this.hair;
    }

    public void setSecondHair(int hair)
    {
        setKeyValue("Second_Hair", String.valueOf(hair));
    }

    public int getFace()
    {
        return this.face;
    }

    public void setFace(int face)
    {
        this.face = face;
    }

    public int getSecondFace()
    {
        if (GameConstants.is神之子(this.job))
        {
            if (getKeyValue("Second_Face") == null)
            {
                setKeyValue("Second_Face", "21290");
            }
            return Integer.parseInt(getKeyValue("Second_Face"));
        }
        return this.face;
    }

    public void setSecondFace(int face)
    {
        setKeyValue("Second_Face", String.valueOf(face));
    }

    public Point getOldPosition()
    {
        return this.old;
    }

    public void setOldPosition(Point x)
    {
        this.old = x;
    }

    public boolean isInvincible()
    {
        return this.invincible;
    }

    public void setInvincible(boolean invinc)
    {
        this.invincible = invinc;
    }

    public BuddyList getBuddylist()
    {
        return this.buddylist;
    }

    public void updateFame()
    {
        updateSingleStat(MapleStat.人气, this.fame);
    }

    public void gainFame(int famechange, boolean show)
    {
        this.fame += famechange;
        updateSingleStat(MapleStat.人气, this.fame);
        if ((show) && (famechange != 0))
        {
            this.client.getSession().write(MaplePacketCreator.getShowFameGain(famechange));
        }
    }

    public void updateHair(int hair)
    {
        setHair(hair);
        updateSingleStat(MapleStat.发型, hair);
        equipChanged();
    }

    public void updateFace(int face)
    {
        setFace(face);
        updateSingleStat(MapleStat.脸型, face);
        equipChanged();
    }

    public void equipChanged()
    {
        if (this.map == null)
        {
            return;
        }
        this.map.broadcastMessage(this, MaplePacketCreator.updateCharLook(this), false);
        this.stats.recalcLocalStats(this);
        if (getMessenger() != null) handling.world.WorldMessengerService.getInstance().updateMessenger(getMessenger().getId(), getName(), this.client.getChannel());
    }

    public MapleMessenger getMessenger()
    {
        return this.messenger;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getObjectId()
    {
        return getId();
    }

    public void setMessenger(MapleMessenger messenger)
    {
        this.messenger = messenger;
    }

    public void changeMapBanish(int mapid, String portal, String msg)
    {
        dropMessage(5, msg);
        MapleMap maps = this.client.getChannelServer().getMapFactory().getMap(mapid);
        changeMap(maps, maps.getPortal(portal));
    }

    public void setObjectId(int id)
    {
        throw new UnsupportedOperationException();
    }

    public void changeMap(MapleMap to, Point pos)
    {
        changeMapInternal(to, pos, MaplePacketCreator.getWarpToMap(to, 128, this), null);
    }

    public void changeMap(MapleMap to)
    {
        changeMapInternal(to, to.getPortal(0).getPosition(), MaplePacketCreator.getWarpToMap(to, 0, this), to.getPortal(0));
    }

    public void changeMapPortal(MapleMap to, MaplePortal pto)
    {
        changeMapInternal(to, pto.getPosition(), MaplePacketCreator.getWarpToMap(to, pto.getId(), this), pto);
    }

    public void cancelChallenge()
    {
        if ((this.challenge != 0) && (this.client.getChannelServer() != null))
        {
            MapleCharacter chr = this.client.getChannelServer().getPlayerStorage().getCharacterById(this.challenge);
            if (chr != null)
            {
                chr.dropMessage(6, getName() + " 拒绝了您的请求.");
                chr.setChallenge(0);
            }
            dropMessage(6, "您的请求被拒绝.");
            this.challenge = 0;
        }
    }

    public void leaveMap(MapleMap map)
    {
        this.controlledLock.writeLock().lock();
        this.visibleMapObjectsLock.writeLock().lock();
        try
        {
            for (MapleMonster mons : this.controlled)
            {
                if (mons != null)
                {
                    mons.setController(null);
                    mons.setControllerHasAggro(false);
                    map.updateMonsterController(mons);
                }
            }
            this.controlled.clear();
            this.visibleMapObjects.clear();
        }
        finally
        {
            this.controlledLock.writeLock().unlock();
            this.visibleMapObjectsLock.writeLock().unlock();
        }
        if (this.chair != 0)
        {
            this.chair = 0;
        }
        clearLinkMid();
        cancelFishingTask();
        cancelChallenge();
        if (getBattle() != null)
        {
            getBattle().forfeit(this, true);
        }
        if (!getMechDoors().isEmpty())
        {
            removeMechDoor();
        }
        cancelMapTimeLimitTask();
        if (getTrade() != null)
        {
            server.MapleTrade.cancelTrade(getTrade(), this.client, this);
        }
    }

    public void changeJob(int newJob)
    {
        try
        {
            cancelEffectFromBuffStat(MapleBuffStat.影分身);
            this.job = ((short) newJob);
            updateSingleStat(MapleStat.职业, newJob);
            if ((!GameConstants.is新手职业(newJob)) && (!GameConstants.is神之子(newJob)))
            {
                if (GameConstants.isSeparatedSpJob(newJob))
                {
                    int changeSp = (newJob == 2200) || (newJob == 2210) || (newJob == 2211) || (newJob == 2213) ? 3 : 5;
                    if (!GameConstants.is龙神(newJob))
                    {
                        if (GameConstants.getSkillBookByJob(newJob) == 3)
                        {
                            changeSp = 3;
                        }
                        else
                        {
                            changeSp = 4;
                        }
                    }
                    this.remainingSp[GameConstants.getSkillBookByJob(newJob)] += changeSp;
                    this.client.getSession().write(UIPacket.getSPMsg((byte) changeSp, (short) newJob));
                }
                else
                {
                    this.remainingSp[GameConstants.getSkillBookByJob(newJob)] += 1;
                    if (newJob % 10 >= 2)
                    {
                        this.remainingSp[GameConstants.getSkillBookByJob(newJob)] += 2;
                    }
                }
                if ((newJob % 10 >= 1) && (this.level >= 70))
                {
                    this.remainingAp = ((short) (this.remainingAp + 5));
                    updateSingleStat(MapleStat.AVAILABLEAP, this.remainingAp);
                    Skill skil = SkillFactory.getSkill(PlayerStats.getSkillByJob(1007, getJob()));
                    if ((skil != null) && (getSkillLevel(skil) <= 0))
                    {
                        dropMessage(-1, "恭喜你获得锻造技能。");
                        changeSingleSkillLevel(skil, skil.getMaxLevel(), (byte) skil.getMaxLevel());
                    }
                }
                if (!isGM())
                {
                    resetStatsByJob(true);
                    if (!GameConstants.is龙神(newJob))
                    {
                        if (getLevel() > (newJob == 200 ? 8 : 10)) if ((newJob % 100 == 0) && (newJob % 1000 / 100 > 0))
                        {
                            this.remainingSp[GameConstants.getSkillBookByJob(newJob)] += 3 * (getLevel() - (newJob == 200 ? 8 : 10));
                        }
                    }
                    else if (newJob == 2200)
                    {
                        MapleQuest.getInstance(22100).forceStart(this, 0, null);
                        MapleQuest.getInstance(22100).forceComplete(this, 0);
                        expandInventory((byte) 1, 4);
                        expandInventory((byte) 2, 4);
                        expandInventory((byte) 3, 4);
                        expandInventory((byte) 4, 4);
                        this.client.getSession().write(tools.packet.NPCPacket.getEvanTutorial("UI/tutorial/evan/14/0"));
                        dropMessage(5, "孵化器里的蛋中孵化出了幼龙，获得了可以提升龙的技能的3点SP，幼龙好像想说话。点击幼龙，和它说话吧！");
                    }
                }
                updateSingleStat(MapleStat.AVAILABLESP, 0L);
            }

            int maxhp = this.stats.getMaxHp();
            int maxmp = this.stats.getMaxMp();

            switch (this.job)
            {
                case 100:
                case 1100:
                case 2100:
                case 3200:
                case 5100:
                case 6100:
                case 3100:
                    maxhp += Randomizer.rand(200, 250);
                    break;
                case 3110:
                case 110:
                case 120:
                case 130:
                case 1110:
                case 2110:
                case 3210:
                case 5110:
                    maxhp += Randomizer.rand(300, 350);
                    break;
                case 3101:
                case 3120:
                    maxhp += Randomizer.rand(500, 800);
                    break;
                case 200:
                case 2200:
                case 2210:
                case 2700:
                    maxmp += Randomizer.rand(100, 150);
                    break;
                case 300:
                case 400:
                case 500:
                case 501:
                case 509:
                case 2300:
                case 2400:
                case 3300:
                case 3500:
                case 3600:
                    maxhp += Randomizer.rand(100, 150);
                    maxmp += Randomizer.rand(25, 50);
                    break;
                case 6110:
                    maxhp += Randomizer.rand(350, 400);
                    maxmp += Randomizer.rand(120, 180);
                    break;
                case 210:
                case 220:
                case 230:
                case 2710:
                    maxmp += Randomizer.rand(400, 450);
                    break;
                case 310:
                case 320:
                case 410:
                case 420:
                case 430:
                case 510:
                case 520:
                case 530:
                case 570:
                case 580:
                case 590:
                case 1310:
                case 1410:
                case 2310:
                case 2410:
                case 3310:
                case 3510:
                case 3610:
                    maxhp += Randomizer.rand(200, 250);
                    maxhp += Randomizer.rand(150, 200);
                    break;
                case 800:
                case 900:
                    maxhp += 99999;
                    maxmp += 99999;
            }

            if (maxhp >= getMaxHpForSever())
            {
                maxhp = getMaxHpForSever();
            }
            if (maxmp >= getMaxMpForSever())
            {
                maxmp = getMaxMpForSever();
            }
            if (GameConstants.isNotMpJob(this.job))
            {
                maxmp = GameConstants.getMPByJob(this.job);
            }
            this.stats.setInfo(maxhp, maxmp, maxhp, maxmp);
            Map<MapleStat, Long> statup = new EnumMap(MapleStat.class);
            statup.put(MapleStat.MAXHP, (long) maxhp);
            statup.put(MapleStat.MAXMP, (long) maxmp);
            statup.put(MapleStat.HP, (long) maxhp);
            statup.put(MapleStat.MP, (long) maxmp);
            this.characterCard.recalcLocalStats(this);
            this.stats.recalcLocalStats(this);
            this.client.getSession().write(MaplePacketCreator.updatePlayerStats(statup, this));
            this.map.broadcastMessage(this, MaplePacketCreator.showForeignEffect(getId(), 12), false);
            this.map.broadcastMessage(this, MaplePacketCreator.updateCharLook(this), false);
            silentPartyUpdate();
            guildUpdate();
            familyUpdate();
            sidekickUpdate();
            if (this.dragon != null)
            {
                this.map.broadcastMessage(MaplePacketCreator.removeDragon(this.id));
                this.dragon = null;
            }
            baseSkills();
            giveSubWeaponItem();
            if ((newJob >= 2200) && (newJob <= 2218))
            {
                if (getBuffedValue(MapleBuffStat.骑兽技能) != null)
                {
                    cancelBuffStats(MapleBuffStat.骑兽技能);
                }
                makeDragon();
            }
            if ((newJob >= 3300) && (newJob <= 3312))
            {
                this.client.getSession().write(MaplePacketCreator.updateJaguar(this));
            }
        }
        catch (Exception e)
        {
            FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", e);
        }
    }

    public void giveSubWeaponItem()
    {
        Item toRemove = getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
        switch (this.job)
        {
            case 3100:
                if ((toRemove == null) || (toRemove.getItemId() != 1099001))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099001, (short) -10);
                }
                break;
            case 3110:
                if ((toRemove != null) && (toRemove.getItemId() != 1099002) && (toRemove.getItemId() == 1099001))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099002, (short) -10);
                }
                break;
            case 3111:
                if ((toRemove != null) && toRemove.getItemId() == 1099002)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099003, (short) -10);
                }
                break;
            case 3112:
                if ((toRemove != null) && toRemove.getItemId() == 1099003)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099004, (short) -10, 1);
                }
                break;
            case 3101:
                if ((toRemove == null) || (toRemove.getItemId() != 1099006))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1050249, (short) -5, false);
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1070029, (short) -7, false);
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1102505, (short) -9, false);
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099006, (short) -10);
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1232001, (short) -11);
                    updateHair(getGender() == 0 ? 36460 : 37450);
                }
                if (!haveItem(1142553))
                {
                    MapleInventoryManipulator.addById(this.client, 1142553, (short) 1, "恶魔复仇者1转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 3120:
                if ((toRemove != null) && toRemove.getItemId() == 1099006)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099007, (short) -10, 1);
                }
                if (!haveItem(1142554))
                {
                    MapleInventoryManipulator.addById(this.client, 1142554, (short) 1, "恶魔复仇者2转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 3121:
                if ((toRemove != null) && toRemove.getItemId() == 1099007)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099008, (short) -10, 1);
                }
                if (!haveItem(1142555))
                {
                    MapleInventoryManipulator.addById(this.client, 1142555, (short) 1, "恶魔复仇者3转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 3122:
                if ((toRemove != null) && toRemove.getItemId() == 1099008)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1099009, (short) -10, 1);
                }
                if (!haveItem(1142556))
                {
                    MapleInventoryManipulator.addById(this.client, 1142556, (short) 1, "恶魔复仇者4转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 3600:
                if ((toRemove == null) || (toRemove.getItemId() != 1353001))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1353001, (short) -10);
                }
                toRemove = getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
                if ((toRemove == null) || (toRemove.getItemId() != 1242001))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1242001, (short) -11);
                }
                removeAll(1242000, false, false);
                if (!haveItem(1142575))
                {
                    MapleInventoryManipulator.addById(this.client, 1142575, (short) 1, "尖兵1转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 3610:
                if ((toRemove != null) && toRemove.getItemId() == 1353001)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1353002, (short) -10);
                }
                if (!haveItem(1142576))
                {
                    MapleInventoryManipulator.addById(this.client, 1142576, (short) 1, "尖兵2转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 3611:
                if ((toRemove != null) && toRemove.getItemId() == 1353002)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1353003, (short) -10);
                }
                if (!haveItem(1142577))
                {
                    MapleInventoryManipulator.addById(this.client, 1142577, (short) 1, "尖兵3转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 3612:
                if ((toRemove != null) && toRemove.getItemId() == 1353003)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1353004, (short) -10, 1);
                }
                if (!haveItem(1142578))
                {
                    MapleInventoryManipulator.addById(this.client, 1142578, (short) 1, "尖兵4转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 5100:
                if ((toRemove == null) || (toRemove.getItemId() != 1098000))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1098000, (short) -10);
                }
                break;
            case 5110:
                if ((toRemove != null) && toRemove.getItemId() == 1098000)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1098001, (short) -10);
                }
                break;
            case 5111:
                if ((toRemove != null) && toRemove.getItemId() == 1098001)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1098002, (short) -10);
                }
                break;
            case 5112:
                if ((toRemove != null) && toRemove.getItemId() == 1098002)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1098003, (short) -10, 1);
                }
                break;
            case 6100:
                if ((toRemove == null) || (toRemove.getItemId() != 1352500))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352500, (short) -10);
                }
                if (!haveItem(1142484))
                {
                    MapleInventoryManipulator.addById(this.client, 1142484, (short) 1, "狂龙1转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 6110:
                if ((toRemove != null) && toRemove.getItemId() == 1352500)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352501, (short) -10);
                }
                if (!haveItem(1142485))
                {
                    MapleInventoryManipulator.addById(this.client, 1142485, (short) 1, "狂龙2转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 6111:
                if ((toRemove != null) && toRemove.getItemId() == 1352501)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352502, (short) -10);
                }
                if (!haveItem(1142486))
                {
                    MapleInventoryManipulator.addById(this.client, 1142486, (short) 1, "狂龙3转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 6112:
                if ((toRemove != null) && toRemove.getItemId() == 1352502)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352503, (short) -10, 1);
                }
                if (!haveItem(1142487))
                {
                    MapleInventoryManipulator.addById(this.client, 1142487, (short) 1, "狂龙4转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 6500:
                if ((toRemove == null) || (toRemove.getItemId() != 1352601))
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352601, (short) -10);
                }
                if (!haveItem(1142495))
                {
                    MapleInventoryManipulator.addById(this.client, 1142495, (short) 1, "萝莉1转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 6510:
                if ((toRemove != null) && toRemove.getItemId() == 1352601)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352602, (short) -10);
                }
                if (!haveItem(1142496))
                {
                    MapleInventoryManipulator.addById(this.client, 1142496, (short) 1, "萝莉2转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 6511:
                if ((toRemove != null) && toRemove.getItemId() == 1352602)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352603, (short) -10);
                }
                if (!haveItem(1142497))
                {
                    MapleInventoryManipulator.addById(this.client, 1142497, (short) 1, "萝莉3转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
            case 6512:
                if ((toRemove != null) && toRemove.getItemId() == 1352603)
                {
                    MapleInventoryManipulator.addItemAndEquip(this.client, 1352604, (short) -10, 1);
                }
                if (!haveItem(1142498))
                {
                    MapleInventoryManipulator.addById(this.client, 1142498, (short) 1, "萝莉4转赠送 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                break;
        }
    }

    public void checkZeroItem()
    {
        if ((this.job != 10112) || (this.level < 100))
        {
            return;
        }
        if (getKeyValue("Zero_Item") == null)
        {
            MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();

            int[] toRemovePos = {-9, -5, -7};
            for (int pos : toRemovePos)
            {
                Item toRemove = getInventory(MapleInventoryType.EQUIPPED).getItem((short) pos);
                if (toRemove != null)
                {
                    MapleInventoryManipulator.removeFromSlot(this.client, MapleInventoryType.EQUIPPED, toRemove.getPosition(), toRemove.getQuantity(), false);
                }
            }

            int[][] equips = {{1003840, -1}, {1032202, -4}, {1052606, -5}, {1072814, -7}, {1082521, -8}, {1102552, -9}, {1113059, -12}, {1113060, -13}, {1113061, -15}, {1113062, -16}, {1122260,
                    -17}, {1132231, -29}, {1152137, -30}};


            for (int[] i : equips)
            {
                if (ii.itemExists(i[0]))
                {
                    Equip equip = (Equip) ii.getEquipById(i[0]);
                    equip.setPosition((byte) i[1]);
                    equip.setQuantity((short) 1);
                    if ((i[1] != -12) && (i[1] != -13) && (i[1] != -15) && (i[1] != -16) && (i[1] != -30))
                    {
                        equip.resetPotential();
                    }
                    equip.setGMLog("系统赠送");
                    forceReAddItem_NoUpdate(equip, MapleInventoryType.EQUIPPED);
                    this.client.getSession().write(tools.packet.InventoryPacket.modifyInventory(false, Collections.singletonList(new client.inventory.ModifyInventory(0, equip))));
                }
            }
            equipChanged();
            MapleInventoryManipulator.addById(this.client, 1142634, (short) 1, "系统赠送");
            MapleInventoryManipulator.addById(this.client, 2001530, (short) 100, "系统赠送");

            Object list = new HashMap<>();
            int[] skillIds = {101000103, 101000203};
            for (int skillId : skillIds)
            {
                Skill skil = SkillFactory.getSkill(skillId);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    ((Map) list).put(skil, new SkillEntry(8, (byte) skil.getMaxLevel(), -1L));
                }
            }
            if (!((Map) list).isEmpty())
            {
                changeSkillsLevel((Map) list);
            }
            setKeyValue("Zero_Item", "True");
        }
    }

    public void checkZeroWeapon()
    {
        if ((this.job != 10112) || (this.level < 100))
        {
            return;
        }
        Item lazuliItem = getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
        Item lapisItem = getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
        int lazuli = lazuliItem != null ? lazuliItem.getItemId() : 0;
        int lapis = lapisItem != null ? lapisItem.getItemId() : 0;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        if (lazuli != getZeroWeapon(false))
        {
            int itemId = getZeroWeapon(false);
            Equip equip = (Equip) ii.getEquipById(itemId);
            equip.setPosition((short) -11);
            equip.setQuantity((short) 1);
            equip.setGMLog("神之子升级赠送");
            equip.resetPotential();
            forceReAddItem_NoUpdate(equip, MapleInventoryType.EQUIPPED);
            this.client.getSession().write(tools.packet.InventoryPacket.modifyInventory(false, Collections.singletonList(new client.inventory.ModifyInventory(0, equip))));
        }
        if (lapis != getZeroWeapon(true))
        {
            int itemId = getZeroWeapon(true);
            Equip equip = (Equip) ii.getEquipById(itemId);
            equip.setPosition((short) -10);
            equip.setQuantity((short) 1);
            equip.setGMLog("神之子升级赠送");
            equip.resetPotential();
            forceReAddItem_NoUpdate(equip, MapleInventoryType.EQUIPPED);
            this.client.getSession().write(tools.packet.InventoryPacket.modifyInventory(false, Collections.singletonList(new client.inventory.ModifyInventory(0, equip))));
        }
        equipChanged();
    }

    public void sendDestroyData(MapleClient client)
    {
        client.getSession().write(MaplePacketCreator.removePlayerFromMap(getObjectId()));
    }

    public int getZeroWeapon(boolean lapis)
    {
        int weapon = lapis ? 1562000 : 1572000;
        if (this.level < 110)
        {
            return weapon;
        }
        if ((this.level >= 110) && (this.level < 120))
        {
            weapon = lapis ? 1562001 : 1572001;
        }
        else if ((this.level >= 120) && (this.level < 130))
        {
            weapon = lapis ? 1562002 : 1572002;
        }
        else if ((this.level >= 130) && (this.level < 140))
        {
            weapon = lapis ? 1562003 : 1572003;
        }
        else if ((this.level >= 140) && (this.level < 160))
        {
            weapon = lapis ? 1562004 : 1572004;
        }
        else if ((this.level >= 160) && (this.level < 180))
        {
            weapon = lapis ? 1562005 : 1572005;
        }
        else if ((this.level >= 180) && (this.level < 200))
        {
            weapon = lapis ? 1562006 : 1572006;
        }
        else if (this.level >= 200)
        {
            weapon = lapis ? 1562007 : 1572007;
        }
        return weapon;
    }

    public void baseSkills()
    {
        checkZeroItem();
        checkInnerSkill();
        checkZeroWeapon();
        checkBeastTamerSkill();
        fixTeachSkillLevel();
        Map<Skill, SkillEntry> list = new HashMap<>();
        Iterator localIterator;
        if (GameConstants.getJobNumber(this.job) >= 3)
        {
            List<Integer> baseSkills = SkillFactory.getSkillsByJob(this.job);
            if (baseSkills != null)
            {
                for (localIterator = baseSkills.iterator(); localIterator.hasNext(); )
                {
                    int i = (Integer) localIterator.next();
                    Skill skil = SkillFactory.getSkill(i);

                    if ((skil != null) && (!skil.isInvisible()) && (skil.isFourthJob()) && (getSkillLevel(skil) <= 0) && (getMasterLevel(skil) <= 0) && (skil.getMasterLevel() > 0))
                    {
                        list.put(skil, new SkillEntry(0, (byte) skil.getMasterLevel(), SkillFactory.getDefaultSExpiry(skil)));
                    }
                    else if ((skil != null) && (skil.getName() != null) && (skil.getName().contains("冒险岛勇士")) && (getSkillLevel(skil) <= 0) && (getMasterLevel(skil) <= 0))
                    {
                        list.put(skil, new SkillEntry(0, (byte) 10, SkillFactory.getDefaultSExpiry(skil)));
                    }
                    else if ((skil != null) && (skil.getName() != null) && (skil.getName().contains("希纳斯的骑士")) && (getSkillLevel(skil) <= 0) && (getMasterLevel(skil) <= 0))
                        list.put(skil, new SkillEntry(0, (byte) 30, SkillFactory.getDefaultSExpiry(skil)));
                }
            }
        }
        Skill skil;
        if ((this.job >= 3300) && (this.job <= 3312))
        {
            skil = SkillFactory.getSkill(30001061);
            if ((skil != null) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry(skil.getMaxLevel(), (byte) skil.getMaxLevel(), -1L));
            }
        }
        if ((GameConstants.is火炮手(this.job)) && (this.level >= 10))
        {
            skil = SkillFactory.getSkill(110);
            if ((skil != null) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry(1, (byte) 1, -1L));
            }
        }
        if ((GameConstants.is恶魔猎手(this.job)) && (this.level >= 10) && (this.job != 3001))
        {
            int[] ss = {30010110, 30010111, 30010112, 30011159, 30010185, 30010183, 30010184, 30010186};
            for (int s : ss)
            {
                skil = SkillFactory.getSkill(s);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }
        }
        if ((GameConstants.is恶魔复仇者(this.job)) && (this.level >= 10))
        {
            int[] ss = {30011109, 30010110, 30010185, 30010241, 30010242, 30010230, 30010231};
            for (int s : ss)
            {
                skil = SkillFactory.getSkill(s);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }
        }
        if ((GameConstants.is尖兵(this.job)) && (this.level >= 10))
        {
            int[] ss = {30020232, 30020233, 30020234, 30021237, 30020240};
            for (int s : ss)
            {
                skil = SkillFactory.getSkill(s);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }
        }
        if ((GameConstants.is双弩精灵(this.job)) && (this.level >= 10))
        {
            int[] ss = {20021110, 20020111, 20020112, 20020109, 20021160, 20021161};
            for (int s : ss)
            {
                skil = SkillFactory.getSkill(s);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }
        }
        if ((GameConstants.is龙的传人(this.job)) && (this.level >= 10))
        {
            skil = SkillFactory.getSkill(228);
            if ((skil != null) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry(1, (byte) 1, -1L));
            }
            skil = SkillFactory.getSkill(1214);
            if ((skil != null) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry(1, (byte) 1, -1L));
            }
        }
        if ((GameConstants.is幻影(this.job)) && (this.level >= 10))
        {
            int[] ss = {20031203, 20030204, 20031205, 20030206, 20031207, 20031208, 20031160, 20031161};
            for (int s : ss)
            {
                skil = SkillFactory.getSkill(s);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }
            if (this.job == 2412)
            {
                skil = SkillFactory.getSkill(20031210);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
                skil = SkillFactory.getSkill(20031209);
                if ((skil != null) && (getSkillLevel(skil) > 0))
                {
                    list.put(skil, new SkillEntry(0, (byte) 0, -1L));
                }
            }
            else
            {
                skil = SkillFactory.getSkill(20031209);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
                skil = SkillFactory.getSkill(20031210);
                if ((skil != null) && (getSkillLevel(skil) > 0)) list.put(skil, new SkillEntry(0, (byte) 0, -1L));
            }
        }
        int[] fixskills;
        if ((GameConstants.is夜光(this.job)) && (this.level >= 10))
        {
            int[] ss = {20040216, 20040217, 20040218, 20040219, 20040220, 20040221, 20041222};
            for (int s : ss)
            {
                skil = SkillFactory.getSkill(s);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }

            fixskills = new int[]{27001100, 27001201, 27000207};
            for (int f : fixskills)
            {
                skil = SkillFactory.getSkill(f);
                if ((skil != null) && (getSkillLevel(skil) <= 0) && (getMasterLevel(skil) <= 0) && (skil.getMasterLevel() > 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) skil.getMasterLevel(), SkillFactory.getDefaultSExpiry(skil)));
                }
            }
        }
        if ((this.job >= 432) && (this.job <= 434))
        {
            fixskills = new int[]{4311003, 4321006, 4330009, 4341009, 4341002};
            for (int i : fixskills)
            {
                skil = SkillFactory.getSkill(i);
                if ((skil != null) && (!skil.isInvisible()) && (skil.isFourthJob()) && (skil.getMasterLevel() > 0))
                {
                    if ((getSkillLevel(skil) <= 0) && (getMasterLevel(skil) <= 0))
                    {
                        list.put(skil, new SkillEntry(0, (byte) skil.getMaxLevel(), SkillFactory.getDefaultSExpiry(skil)));
                    }
                    else if (getMasterLevel(skil) <= skil.getMaxLevel())
                    {
                        list.put(skil, new SkillEntry((byte) getSkillLevel(skil), (byte) skil.getMaxLevel(), SkillFactory.getDefaultSExpiry(skil)));
                    }
                }
            }
        }
        if ((GameConstants.is米哈尔(this.job)) && (this.level >= 10))
        {
            int[] ss = {50001214};
            for (int i : ss)
            {
                skil = SkillFactory.getSkill(i);
                if ((skil != null) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }
        }
        if ((GameConstants.is狂龙战士(this.job)) && (this.level >= 10))
        {
            int[] ss = {60001216, 60001217, 60001218, 60000219, 60000222};
            for (int i : ss)
            {
                skil = SkillFactory.getSkill(i);
                if ((skil != null) && (getSkillLevel(skil) <= 0)) list.put(skil, new SkillEntry(1, (byte) 1, -1L));
            }
        }
        if ((GameConstants.is爆莉萌天使(this.job)) && (this.level >= 10))
        {
            int[] ss = {60011216, 60011218, 60011219, 60011221, 60011222};
            for (int sid : ss)
            {
                skil = SkillFactory.getSkill(sid);
                if ((skil != null) && (getSkillLevel(skil) <= 0)) list.put(skil, new SkillEntry(1, (byte) 1, -1L));
            }
        }
        Object baseSkills;
        int[] skillIds;
        if ((this.job == 10112) && (this.level >= 100))
        {
            baseSkills = SkillFactory.getSkillsByJob(10100);
            ((List) baseSkills).addAll(SkillFactory.getSkillsByJob(10110));
            ((List) baseSkills).addAll(SkillFactory.getSkillsByJob(10111));
            ((List) baseSkills).addAll(SkillFactory.getSkillsByJob(10112));
            for (Object o : (List) baseSkills)
            {
                int i = (Integer) o;
                skil = SkillFactory.getSkill(i);
                if ((skil != null) && (!skil.isInvisible()) && (skil.isFourthJob()) && (getSkillLevel(skil) <= 0) && (getMasterLevel(skil) <= 0) && (skil.getMasterLevel() > 0))
                {
                    list.put(skil, new SkillEntry(0, (byte) skil.getMaxLevel(), SkillFactory.getDefaultSExpiry(skil)));
                }
            }
            skillIds = new int[]{100001273, 100000275, 100000278, 100001263, 100001264, 100001265, 100001266, 100000267, 100001268, 100000282};
            for (int i : skillIds)
            {
                skil = SkillFactory.getSkill(i);
                if ((skil != null) && (skil.canBeLearnedBy(getJob())) && (getSkillLevel(skil) <= 0))
                {
                    list.put(skil, new SkillEntry(1, (byte) 1, -1L));
                }
            }
        }
        for (int i : constants.SkillConstants.defaultskills)
        {
            skil = SkillFactory.getSkill(i);
            if ((skil != null) && (skil.canBeLearnedBy(getJob())) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry(1, (byte) 1, -1L));
            }
        }
        if (!list.isEmpty())
        {
            changeSkillsLevel(list);
        }
    }

    public void sendSpawnData(MapleClient client)
    {
        if (client.getPlayer().allowedToTarget(this))
        {
            client.getSession().write(MaplePacketCreator.spawnPlayerMapobject(this));

            if (this.dragon != null)
            {
                client.getSession().write(MaplePacketCreator.spawnDragon(this.dragon));
            }

            if (this.android != null)
            {
                client.getSession().write(tools.packet.AndroidPacket.spawnAndroid(this, this.android));
            }
            if (this.summonedFamiliar != null)
            {
                client.getSession().write(MaplePacketCreator.spawnFamiliar(this.summonedFamiliar, true));
            }
            if ((this.summons != null) && (this.summons.size() > 0))
            {
                this.summonsLock.readLock().lock();
                try
                {
                    for (MapleSummon summon : this.summons)
                    {
                        client.getSession().write(tools.packet.SummonPacket.spawnSummon(summon, false));
                    }
                }
                finally
                {
                    this.summonsLock.readLock().unlock();
                }
            }
            if ((this.followid > 0) && (this.followon))
            {
                client.getSession().write(MaplePacketCreator.followEffect(this.followinitiator ? this.followid : this.id, this.followinitiator ? this.id : this.followid, null));
            }
        }
    }

    public void makeDragon()
    {
        this.dragon = new server.maps.MapleDragon(this);
        this.map.broadcastMessage(MaplePacketCreator.spawnDragon(this.dragon));
    }

    public server.maps.MapleDragon getDragon()
    {
        return this.dragon;
    }

    public void setDragon(server.maps.MapleDragon d)
    {
        this.dragon = d;
    }

    public void gainAp(short ap)
    {
        this.remainingAp = ((short) (this.remainingAp + ap));
        updateSingleStat(MapleStat.AVAILABLEAP, this.remainingAp);
    }

    public void gainSP(int sp)
    {
        this.remainingSp[GameConstants.getSkillBookByJob(this.job)] += sp;
        updateSingleStat(MapleStat.AVAILABLESP, 0L);
        this.client.getSession().write(UIPacket.getSPMsg((byte) sp, this.job));
    }

    public void gainSP(int sp, int skillbook)
    {
        this.remainingSp[skillbook] += sp;
        updateSingleStat(MapleStat.AVAILABLESP, 0L);
        this.client.getSession().write(UIPacket.getSPMsg((byte) sp, (short) 0));
    }

    public void resetSP(int sp)
    {
        for (int i = 0; i < this.remainingSp.length; i++)
        {
            this.remainingSp[i] = sp;
        }
        updateSingleStat(MapleStat.AVAILABLESP, 0L);
    }

    public void resetAPSP()
    {
        resetSP(0);
        gainAp((short) -this.remainingAp);
    }

    public List<Integer> getProfessions()
    {
        List<Integer> prof = new ArrayList<>();
        for (int i = 9200; i <= 9204; i++)
        {
            if (getProfessionLevel(i * 10000) > 0)
            {
                prof.add(i);
            }
        }
        return prof;
    }

    public byte getProfessionLevel(int id)
    {
        int ret = getSkillLevel(id);
        if (ret <= 0)
        {
            return 0;
        }
        return (byte) (ret >>> 24 & 0xFF);
    }

    public short getProfessionExp(int id)
    {
        int ret = getSkillLevel(id);
        if (ret <= 0)
        {
            return 0;
        }
        return (short) (ret & 0xFFFF);
    }

    public boolean addProfessionExp(int id, int expGain)
    {
        int ret = getProfessionLevel(id);
        if ((ret <= 0) || (ret >= 10))
        {
            return false;
        }
        int newExp = getProfessionExp(id) + expGain;
        if (newExp >= GameConstants.getProfessionEXP(ret))
        {
            changeProfessionLevelExp(id, ret + 1, newExp - GameConstants.getProfessionEXP(ret));
            int traitGain = (int) Math.pow(2.0D, ret + 1);
            switch (id)
            {
                case 92000000:
                    this.traits.get(MapleTraitType.sense).addExp(traitGain, this);
                    break;
                case 92010000:
                    this.traits.get(MapleTraitType.will).addExp(traitGain, this);
                    break;
                case 92020000:
                case 92030000:
                case 92040000:
                    this.traits.get(MapleTraitType.craft).addExp(traitGain, this);
            }

            return true;
        }
        changeProfessionLevelExp(id, ret, newExp);
        return false;
    }

    public void changeProfessionLevelExp(int id, int level, int exp)
    {
        changeSingleSkillLevel(SkillFactory.getSkill(id), ((level & 0xFF) << 24) + (exp & 0xFFFF), (byte) 10);
    }

    public void changeSingleSkillLevel(Skill skill, int newLevel, byte newMasterlevel)
    {
        if (skill == null)
        {
            return;
        }
        changeSingleSkillLevel(skill, newLevel, newMasterlevel, SkillFactory.getDefaultSExpiry(skill));
    }

    public void changeSingleSkillLevel(Skill skill, int newLevel, byte newMasterlevel, long expiration)
    {
        Map<Skill, SkillEntry> list = new HashMap<>();
        boolean hasRecovery = false;
        boolean recalculate = false;
        if (changeSkillData(skill, newLevel, newMasterlevel, expiration))
        {
            list.put(skill, new SkillEntry(newLevel, newMasterlevel, expiration, getSkillTeachId(skill), getSkillPosition(skill)));
            if (GameConstants.isRecoveryIncSkill(skill.getId()))
            {
                hasRecovery = true;
            }
            if (skill.getId() < 80000000)
            {
                recalculate = true;
            }
        }
        if (list.isEmpty())
        {
            return;
        }
        this.client.getSession().write(MaplePacketCreator.updateSkills(list));
        reUpdateStat(hasRecovery, recalculate);
    }

    public void changeSkillLevel_Skip(Map<Skill, SkillEntry> skill)
    {
        changeSkillLevel_Skip(skill, false);
    }

    public void changeSkillLevel_Skip(Map<Skill, SkillEntry> skill, boolean write)
    {
        if (skill.isEmpty())
        {
            return;
        }
        Map<Skill, SkillEntry> newlist = new HashMap<>();
        for (Map.Entry<Skill, SkillEntry> date : skill.entrySet())
        {
            if (date.getKey() != null)
            {


                newlist.put(date.getKey(), date.getValue());
                if ((date.getValue().skillevel == 0) && (date.getValue().masterlevel == 0))
                {
                    this.skills.remove(date.getKey());

                }
                else this.skills.put(date.getKey(), date.getValue());
            }
        }
        if ((write) && (!newlist.isEmpty()))
        {
            this.client.getSession().write(MaplePacketCreator.updateSkills(newlist));
        }
    }

    public void changeTeachSkill(int skillId, int toChrId)
    {
        Skill skill = SkillFactory.getSkill(skillId);
        if (skill == null)
        {
            return;
        }
        this.client.getSession().write(MaplePacketCreator.updateSkill(skillId, toChrId, 1, -1L));
        this.skills.put(skill, new SkillEntry(1, (byte) 1, -1L, toChrId));
        this.changed_skills = true;
    }

    public void playerDead()
    {
        cancelEffectFromBuffStat(MapleBuffStat.影分身);
        cancelEffectFromBuffStat(MapleBuffStat.变身术);
        cancelEffectFromBuffStat(MapleBuffStat.飞行骑乘);
        cancelEffectFromBuffStat(MapleBuffStat.骑兽技能);
        cancelEffectFromBuffStat(MapleBuffStat.金属机甲);
        cancelEffectFromBuffStat(MapleBuffStat.恢复效果);
        cancelEffectFromBuffStat(MapleBuffStat.indieMaxHp);
        cancelEffectFromBuffStat(MapleBuffStat.indieMaxMp);
        cancelEffectFromBuffStat(MapleBuffStat.增强_MAXHP);
        cancelEffectFromBuffStat(MapleBuffStat.增强_MAXMP);
        cancelEffectFromBuffStat(MapleBuffStat.MAXHP);
        cancelEffectFromBuffStat(MapleBuffStat.MAXMP);
        cancelEffectFromBuffStat(MapleBuffStat.精神连接);
        cancelEffectFromBuffStat(MapleBuffStat.剑刃之壁);
        cancelEffectFromBuffStat(MapleBuffStat.尖兵飞行);
        cancelEffectFromBuffStat(MapleBuffStat.击杀点数);
        dispelSummons();
        checkFollow();
        MapleStatEffect statss = getStatForBuff(MapleBuffStat.灵魂之石);
        if (statss != null)
        {
            dropMessage(5, "由于灵魂之石的效果发动，本次死亡经验您的经验值不会减少。");
            getStat().setHp(getStat().getMaxHp() / 100 * statss.getX(), this);
            setStance(0);

            cancelEffectFromBuffStat(MapleBuffStat.灵魂之石);
            return;
        }
        statss = getStatForBuff(MapleBuffStat.神秘运气);
        if (statss != null)
        {
            dropMessage(5, "由于神秘的运气的效果发动，本次死亡经验您的经验值不会减少。");
            getStat().setHp(getStat().getMaxHp() / 100 * statss.getX(), this);
            setStance(0);
            cancelEffectFromBuffStat(MapleBuffStat.神秘运气);
            return;
        }
        if ((getSkillLevel(1320016) > 0) && (!skillisCooling(1320019)))
        {
            getStat().setHp(getStat().getCurrentMaxHp(), this);
            getStat().setMp(getStat().getCurrentMaxMp(getJob()), this);
            updateSingleStat(MapleStat.HP, getStat().getHp());
            updateSingleStat(MapleStat.MP, getStat().getMp());
            setStance(0);
            Skill skill = SkillFactory.getSkill(1320019);
            skill.getEffect(getSkillLevel(1320016)).applyTo(this);
            return;
        }
        if (getEventInstance() != null)
        {
            getEventInstance().playerKilled(this);
        }
        setPowerCount(0);
        this.dotHP = 0;
        this.lastDOTTime = 0L;
        this.specialStats.resetSpecialStats();
        if ((!GameConstants.is新手职业(this.job)) && (!inPVP()))
        {
            int charms = getItemQuantity(5130000, false);
            if (charms > 0)
            {
                MapleInventoryManipulator.removeById(this.client, MapleInventoryType.CASH, 5130000, 1, true, false);
                charms--;
                if (charms > 255)
                {
                    charms = 255;
                }
                this.client.getSession().write(tools.packet.MTSCSPacket.useCharm((byte) charms, (byte) 0));
            }
            else
            {
                long expforlevel = getExpNeededForLevel();
                float diepercentage;
                if ((this.map.isTown()) || (server.maps.FieldLimitType.RegularExpLoss.check(this.map.getFieldLimit())))
                {
                    diepercentage = 0.01F;
                }
                else
                {
                    diepercentage = 0.1F - this.traits.get(MapleTraitType.charisma).getLevel() / 20 / 100.0F;
                }
                long v10 = (long) (this.exp.get() - (expforlevel * diepercentage));
                if (v10 < 0L)
                {
                    v10 = 0L;
                }
                this.exp.set(v10);
            }
            updateSingleStat(MapleStat.经验, this.exp.get());
        }
        if (!this.stats.checkEquipDurabilitys(this, -100))
        {
            dropMessage(5, "An item has run out of durability but has no inventory room to go to.");
        }
        if (this.pyramidSubway != null)
        {
            this.stats.setHp(50, this);
            this.pyramidSubway.fail(this);
        }
    }

    public void healMP(int delta)
    {
        addMP(delta);
        this.client.getSession().write(MaplePacketCreator.showOwnHpHealed(delta));
        getMap().broadcastMessage(this, MaplePacketCreator.showHpHealed(getId(), delta), false);
    }

    public void addMP(int delta)
    {
        addMP(delta, false);
    }

    public void addDemonMp(int delta)
    {
        if ((delta > 0) && ((getJob() == 3111) || (getJob() == 3112)) && (this.stats.setMp(this.stats.getMp() + delta, this))) updateSingleStat(MapleStat.MP, this.stats.getMp());
    }

    public void addMPHP(int hpDiff, int mpDiff)
    {
        Map<MapleStat, Long> statups = new EnumMap(MapleStat.class);
        if (this.stats.setHp(this.stats.getHp() + hpDiff, this))
        {
            statups.put(MapleStat.HP, (long) this.stats.getHp());
        }
        if (((mpDiff < 0) && (GameConstants.is恶魔猎手(getJob()))) || ((!GameConstants.is恶魔猎手(getJob())) && (this.stats.setMp(this.stats.getMp() + mpDiff, this))))
        {
            statups.put(MapleStat.MP, (long) this.stats.getMp());
        }

        if (statups.size() > 0)
        {
            this.client.getSession().write(MaplePacketCreator.updatePlayerStats(statups, this));
        }
    }

    public void gainExp(int total, boolean show, boolean inChat, boolean white)
    {
        try
        {
            long prevexp = getExp();
            long needed = getExpNeededForLevel();
            if (total > 0)
            {
                this.stats.checkEquipLevels(this, total);
            }
            if (this.level >= getMaxLevelForSever())
            {
                setExp(0L);
            }
            else
            {
                boolean leveled = false;
                long tot = this.exp.get() + total;
                if (tot >= needed)
                {
                    this.exp.addAndGet(total);
                    levelUp();
                    leveled = true;
                    if (this.level >= getMaxLevelForSever())
                    {
                        setExp(0L);
                    }
                    else
                    {
                        needed = getExpNeededForLevel();
                        if (this.exp.get() >= needed)
                        {
                            setExp(needed - 1L);
                        }
                    }
                }
                else
                {
                    this.exp.addAndGet(total);
                }
                if (total > 0)
                {
                    familyRep((int) prevexp, (int) needed, leveled);
                }
            }
            if (total != 0)
            {
                if (this.exp.get() < 0L)
                {
                    if (total > 0)
                    {
                        setExp(needed);
                    }
                    else if (total < 0)
                    {
                        setExp(0L);
                    }
                }
                updateSingleStat(MapleStat.经验, getExp());
                if (show)
                {
                    this.client.getSession().write(MaplePacketCreator.GainEXP_Others(total, inChat, white));
                }
            }
        }
        catch (Exception e)
        {
            FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", e);
        }
    }

    public void familyRep(int prevexp, int needed, boolean leveled)
    {
        if (this.mfc != null)
        {
            int onepercent = needed / 100;
            if (onepercent <= 0)
            {
                return;
            }
            int percentrep = (int) (getExp() / onepercent - prevexp / onepercent);
            if (leveled)
            {
                percentrep = 100 - percentrep + this.level / 2;
            }
            if (percentrep > 0)
            {
                int sensen = handling.world.WorldFamilyService.getInstance().setRep(this.mfc.getFamilyId(), this.mfc.getSeniorId(), percentrep * 10, this.level, this.name);
                if (sensen > 0)
                {
                    handling.world.WorldFamilyService.getInstance().setRep(this.mfc.getFamilyId(), sensen, percentrep * 5, this.level, this.name);
                }
            }
        }
    }

    public void gainExpMonster(long gain, boolean show, boolean white, int numExpSharers, boolean partyBonusMob, int partyBonusRate)
    {
        long totalExp = gain;
        int 密友经验 = 0;
        if (this.sidekick != null)
        {
            MapleCharacter side = this.map.getCharacterById(this.sidekick.getCharacter(this.sidekick.getCharacter(0).getId() == getId() ? 1 : 0).getId());
            if (side != null)
            {
                密友经验 = (int) (gain / 2L);
                totalExp += 密友经验;
            }
        }
        int 网吧经验 = 0;
        if (haveItem(5420008))
        {
            网吧经验 = (int) (gain / 100.0D * 25.0D);
            totalExp += 网吧经验;
        }
        int 精灵祝福 = 0;
        if (get精灵祝福() > 0)
        {
            精灵祝福 = (int) (gain / 100.0D * 10.0D);
            totalExp += 精灵祝福;
        }
        int 结婚经验 = 0;
        if (this.marriageId > 0)
        {
            MapleCharacter marrChr = this.map.getCharacterById(this.marriageId);
            if (marrChr != null)
            {
                结婚经验 = (int) (gain / 100.0D * 10.0D);
                totalExp += 结婚经验;
            }
        }
        int 组队经验 = 0;
        if (numExpSharers > 1)
        {
            double rate = (this.map == null) || (!partyBonusMob) || (this.map.getPartyBonusRate() <= 0) ? 0.05D : partyBonusRate > 0 ? partyBonusRate / 100.0D : this.map.getPartyBonusRate() / 100.0D;
            组队经验 = (int) ((float) (gain * rate) * (numExpSharers + (rate > 0.05D ? -1 : 1)));
            totalExp += 组队经验;
        }
        int 道具佩戴经验 = 0;
        int 召回戒指经验 = 0;
        if (this.stats.equippedWelcomeBackRing)
        {
            召回戒指经验 = (int) (gain / 100.0D * 80.0D);
            totalExp += 召回戒指经验;
        }
        if ((gain > 0L) && (totalExp < gain))
        {
            totalExp = 2147483647L;
        }
        if (totalExp > 0L)
        {
            this.stats.checkEquipLevels(this, (int) totalExp);
            gainBeastTamerSkillExp((int) totalExp);
        }
        long prevexp = getExp();
        long needed = getExpNeededForLevel();
        if (this.level >= getMaxLevelForSever())
        {
            setExp(0L);
        }
        else
        {
            boolean leveled = false;
            if ((this.exp.get() + totalExp >= needed) || (this.exp.get() >= needed))
            {
                this.exp.addAndGet(totalExp);
                levelUp();
                leveled = true;
                if (this.level >= getMaxLevelForSever())
                {
                    setExp(0L);
                }
                else
                {
                    needed = getExpNeededForLevel();
                    if (this.exp.get() >= needed)
                    {
                        setExp(needed);
                    }
                }
            }
            else
            {
                this.exp.addAndGet(totalExp);
            }
            if (totalExp > 0L)
            {
                familyRep((int) prevexp, (int) needed, leveled);
            }
        }
        if (gain != 0L)
        {
            if (this.exp.get() < 0L)
            {
                if (gain > 0L)
                {
                    setExp(getExpNeededForLevel());
                }
                else if (gain < 0L)
                {
                    setExp(0L);
                }
            }
            updateSingleStat(MapleStat.经验, getExp());
            if (show)
            {
                Map<MapleExpStat, Integer> expStatup = new EnumMap(MapleExpStat.class);
                if (组队经验 > 0)
                {
                    expStatup.put(MapleExpStat.组队经验, 组队经验);
                }
                if (结婚经验 > 0)
                {
                    expStatup.put(MapleExpStat.结婚奖励经验, 结婚经验);
                }
                if (道具佩戴经验 > 0)
                {
                    expStatup.put(MapleExpStat.道具佩戴经验, 道具佩戴经验);
                }
                if (网吧经验 > 0)
                {
                    expStatup.put(MapleExpStat.网吧特别经验, 网吧经验);
                }
                if (精灵祝福 > 0)
                {
                    expStatup.put(MapleExpStat.精灵祝福经验, 精灵祝福);
                }
                this.client.getSession().write(MaplePacketCreator.showGainExpFromMonster((int) gain, white, expStatup, 召回戒指经验));
            }
        }
    }

    public void forceReAddItem(Item item, MapleInventoryType type)
    {
        forceReAddItem_NoUpdate(item, type);
        if (type != MapleInventoryType.UNDEFINED)
        {
            this.client.getSession().write(tools.packet.InventoryPacket.modifyInventory(false, Collections.singletonList(new client.inventory.ModifyInventory(0, item))));
        }
    }

    public void forceReAddItem_NoUpdate(Item item, MapleInventoryType type)
    {
        getInventory(type).removeSlot(item.getPosition());
        getInventory(type).addFromDB(item);
    }

    public void forceReAddItem_Book(Item item, MapleInventoryType type)
    {
        getInventory(type).removeSlot(item.getPosition());
        getInventory(type).addFromDB(item);
        if (type != MapleInventoryType.UNDEFINED)
        {
            this.client.getSession().write(MaplePacketCreator.upgradeBook(item, this));
        }
    }

    public void silentPartyUpdate()
    {
        if (this.party != null)
        {
            handling.world.WrodlPartyService.getInstance().updateParty(this.party.getId(), handling.world.PartyOperation.更新队伍, new MaplePartyCharacter(this));
        }
    }

    public boolean hasGmLevel(int level)
    {
        return this.gmLevel >= level;
    }

    public void setGmLevel(int level)
    {
        this.gmLevel = ((byte) level);
    }

    public MapleInventory[] getInventorys()
    {
        return this.inventory;
    }

    public boolean canExpiration(long now)
    {
        return (this.lastExpirationTime > 0L) && (this.lastExpirationTime + 60000L < now);
    }

    public void expirationTask()
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        MapleQuestStatus stat = getQuestNoAdd(MapleQuest.getInstance(122700));

        List<Integer> ret = new ArrayList<>();
        long currenttime = System.currentTimeMillis();
        List<Triple<MapleInventoryType, Item, Boolean>> tobeRemoveItem = new ArrayList<>();
        List<Item> tobeUnlockItem = new ArrayList<>();
        for (MapleInventoryType inv : MapleInventoryType.values())
        {
            for (Item item : getInventory(inv))
            {
                long expiration = item.getExpiration();
                if (((expiration != -1L) && (!ItemConstants.isPet(item.getItemId())) && (currenttime > expiration)) || (ii.isLogoutExpire(item.getItemId())))
                {
                    if (ItemFlag.LOCK.check(item.getFlag()))
                    {
                        tobeUnlockItem.add(item);
                    }
                    else if (currenttime > expiration)
                    {
                        tobeRemoveItem.add(new Triple(inv, item, Boolean.TRUE));
                    }
                }
                else if ((item.getItemId() == 5000054) && (item.getPet() != null) && (item.getPet().getSecondsLeft() <= 0))
                {
                    tobeRemoveItem.add(new Triple(inv, item, Boolean.FALSE));
                }
                else if ((item.getPosition() == -38) && ((stat == null) || (stat.getCustomData() == null) || (Long.parseLong(stat.getCustomData()) < currenttime)))
                {
                    tobeRemoveItem.add(new Triple(inv, item, Boolean.FALSE));
                }
            }
        }
        for (Triple<MapleInventoryType, Item, Boolean> itemz : tobeRemoveItem)
        {
            Item item = (Item) ((Triple) itemz).getMid();
            if (item == null)
            {
                FileoutputUtil.log("道具到期.txt", getName() + " 检测道具已经过期，但道具为空，无法继续执行。", true);

            }
            else if ((Boolean) ((Triple) itemz).getRight())
            {
                if (MapleInventoryManipulator.removeFromSlot(this.client, (MapleInventoryType) ((Triple) itemz).getLeft(), item.getPosition(), item.getQuantity(), false))
                {
                    ret.add(item.getItemId());
                }
                if (((Triple) itemz).getLeft() == MapleInventoryType.EQUIPPED)
                {
                    equipChanged();
                }
            }
            else if (item.getPosition() == -38)
            {
                short slot = getInventory(MapleInventoryType.EQUIP).getNextFreeSlot();
                if (slot > -1)
                {
                    MapleInventoryManipulator.unequip(this.client, item.getPosition(), slot);
                }
            }
        }
        for (Item itemz : tobeUnlockItem)
        {
            itemz.setExpiration(-1L);
            forceUpdateItem(itemz);
        }

        this.pendingExpiration = ret;


        Object tobeRemoveSkill = new ArrayList<>();
        Object tobeRemoveList = new HashMap<>();
        for (Object item = this.skills.entrySet().iterator(); ((Iterator) item).hasNext(); )
        {
            Map.Entry<Skill, SkillEntry> skil = (Map.Entry) ((Iterator) item).next();
            if ((skil.getValue().expiration != -1L) && (currenttime > skil.getValue().expiration))
            {
                ((List) tobeRemoveSkill).add(skil.getKey());
            }
        }
        for (Object o : (List) tobeRemoveSkill)
        {
            Skill skil = (Skill) o;
            ((Map) tobeRemoveList).put(skil, new SkillEntry(0, (byte) 0, -1L));
            this.skills.remove(skil);
            this.changed_skills = true;
        }
        this.pendingSkills = ((Map) tobeRemoveList);
        if ((stat != null) && (stat.getCustomData() != null) && (Long.parseLong(stat.getCustomData()) < currenttime))
        {
            this.quests.remove(MapleQuest.getInstance(7830));
            this.quests.remove(MapleQuest.getInstance(122700));
            this.client.getSession().write(MaplePacketCreator.pendantSlot(false));
        }


        if ((this.coreAura != null) && (currenttime > this.coreAura.getExpiration()))
        {
            this.coreAura.resetCoreAura();
            this.coreAura.saveToDb();
            updataCoreAura();
            dropMessage(5, "宝盒属性时间到期，属性已经重置。");
        }


        Item itemFix = getInventory(MapleInventoryType.EQUIPPED).getItem((short) -37);
        if ((itemFix != null) && (itemFix.getItemId() / 10000 != 119))
        {
            short slot = getInventory(MapleInventoryType.EQUIP).getNextFreeSlot();
            if (slot > -1)
            {
                MapleInventoryManipulator.unequip(this.client, itemFix.getPosition(), slot);
                dropMessage(5, "装备道具[" + ii.getName(itemFix.getItemId()) + "]由于装备的位置错误已自动取下。");
            }
        }


        Timestamp currentVipTime = new Timestamp(System.currentTimeMillis());
        Object expirationVip;
        if (getVip() != 0)
        {
            expirationVip = getViptime();
            if ((expirationVip != null) && (currentVipTime.after((Timestamp) expirationVip)))
            {
                setVip(0);
                setViptime(null);
                dropMessage(-11, "您的月卡已经到期，当前月卡等级为 " + getVip());
            }
            else if (expirationVip == null)
            {
                setVip(0);
                setViptime(null);
            }
        }


        if (!this.pendingExpiration.isEmpty())
        {
            for (expirationVip = this.pendingExpiration.iterator(); ((Iterator) expirationVip).hasNext(); )
            {
                Integer itemId = (Integer) ((Iterator) expirationVip).next();
                if (ii.isCash(itemId))
                {
                    this.client.getSession().write(MaplePacketCreator.showCashItemExpired(itemId));
                }
                else
                {
                    this.client.getSession().write(MaplePacketCreator.showItemExpired(itemId));
                }
            }
        }
        this.pendingExpiration = null;


        if (!this.pendingSkills.isEmpty())
        {
            this.client.getSession().write(MaplePacketCreator.updateSkills(this.pendingSkills));
            this.client.getSession().write(MaplePacketCreator.showSkillExpired(this.pendingSkills));
        }
        this.pendingSkills = null;
        this.lastExpirationTime = System.currentTimeMillis();
    }

    public void forceUpdateItem(Item item)
    {
        forceUpdateItem(item, false);
    }

    public void updataCoreAura()
    {
        if (this.coreAura != null)
        {
            this.client.getSession().write(tools.packet.InventoryPacket.updataCoreAura(this));
        }
    }

    public int getVip()
    {
        return this.vip;
    }

    public Timestamp getViptime()
    {
        if (getVip() == 0)
        {
            return null;
        }
        return this.viptime;
    }

    public void forceUpdateItem(Item item, boolean updateTick)
    {
        List<client.inventory.ModifyInventory> mods = new LinkedList<>();
        mods.add(new client.inventory.ModifyInventory(3, item));
        mods.add(new client.inventory.ModifyInventory(0, item));
        this.client.getSession().write(tools.packet.InventoryPacket.modifyInventory(updateTick, mods, this));
    }

    public void setViptime(long period)
    {
        if (period > 0L)
        {
            Timestamp expiration = new Timestamp(System.currentTimeMillis() + period * 24L * 60L * 60L * 1000L);
            setViptime(expiration);
        }
        else
        {
            setViptime(null);
        }
    }

    public void setViptime(Timestamp expire)
    {
        this.viptime = expire;
    }

    public server.shop.MapleShop getShop()
    {
        return this.shop;
    }

    public void setShop(server.shop.MapleShop shop)
    {
        this.shop = shop;
    }

    public int[] getSavedLocations()
    {
        return this.savedLocations;
    }

    public int getSavedLocation(SavedLocationType type)
    {
        return this.savedLocations[type.getValue()];
    }

    public void saveLocation(SavedLocationType type)
    {
        this.savedLocations[type.getValue()] = getMapId();
        this.changed_savedlocations = true;
    }

    public int getMapId()
    {
        if (this.map != null)
        {
            return this.map.getId();
        }
        return this.mapid;
    }

    public void saveLocation(SavedLocationType type, int mapz)
    {
        this.savedLocations[type.getValue()] = mapz;
        this.changed_savedlocations = true;
    }

    public void clearSavedLocation(SavedLocationType type)
    {
        this.savedLocations[type.getValue()] = -1;
        this.changed_savedlocations = true;
    }

    public int getMeso()
    {
        return this.meso;
    }

    public void gainMeso(int gain, boolean show)
    {
        gainMeso(gain, show, false);
    }

    public void gainMeso(int gain, boolean show, boolean inChat)
    {
        if (this.meso + gain < 0)
        {
            this.client.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        this.meso += gain;
        if (this.meso >= 1000000)
        {
            finishAchievement(31);
        }
        if (this.meso >= 10000000)
        {
            finishAchievement(32);
        }
        if (this.meso >= 100000000)
        {
            finishAchievement(33);
        }
        if (this.meso >= 1000000000)
        {
            finishAchievement(34);
        }
        updateSingleStat(MapleStat.金币, this.meso, false);
        this.client.getSession().write(MaplePacketCreator.enableActions());
        if (show)
        {
            this.client.getSession().write(MaplePacketCreator.showMesoGain(gain, inChat));
        }
    }

    public void controlMonster(MapleMonster monster, boolean aggro)
    {
        monster.setController(this);
        controlled.add(monster);
        client.getSession().write(MobPacket.controlMonster(monster, false, aggro));
    }

    public void stopControllingMonster(MapleMonster monster)
    {
        if (monster != null)
        {
            controlled.remove(monster);
        }
    }

    public void checkMonsterAggro(MapleMonster monster)
    {
        if (monster == null)
        {
            return;
        }
        if (monster.getController() == this)
        {
            monster.setControllerHasAggro(true);
        }
        else
        {
            monster.switchController(this, true);
        }
    }

    public int getControlledSize()
    {
        return this.controlled.size();
    }

    public List<MapleQuestStatus> getStartedQuests()
    {
        List<MapleQuestStatus> ret = new LinkedList<>();
        for (MapleQuestStatus q : this.quests.values())
        {
            if ((q.getStatus() == 1) && (!q.isCustom()) && (!q.getQuest().isBlocked()))
            {
                ret.add(q);
            }
        }
        return ret;
    }

    public server.maps.MapleMapObjectType getType()
    {
        return server.maps.MapleMapObjectType.PLAYER;
    }

    public List<MapleQuestStatus> getCompletedQuests()
    {
        List<MapleQuestStatus> ret = new LinkedList<>();
        for (MapleQuestStatus q : this.quests.values())
        {
            if ((q.getStatus() == 2) && (!q.isCustom()) && (!q.getQuest().isBlocked()))
            {
                ret.add(q);
            }
        }
        return ret;
    }

    public List<Pair<Integer, Long>> getCompletedMedals()
    {
        List<Pair<Integer, Long>> ret = new ArrayList<>();
        for (MapleQuestStatus q : this.quests.values())
        {
            if ((q.getStatus() == 2) && (!q.isCustom()) && (!q.getQuest().isBlocked()) && (q.getQuest().getMedalItem() > 0) && (ItemConstants.getInventoryType(q.getQuest().getMedalItem()) == MapleInventoryType.EQUIP))
            {
                ret.add(new Pair(q.getQuest().getId(), q.getCompletionTime()));
            }
        }
        return ret;
    }

    public void mobKilled(int id, int skillID)
    {
        for (MapleQuestStatus q : this.quests.values())
            if ((q.getStatus() == 1) && (q.hasMobKills()))
            {

                if (q.mobKilled(id, skillID))
                {
                    this.client.getSession().write(MaplePacketCreator.updateQuestMobKills(q));
                    if (q.getQuest().canComplete(this, null)) this.client.getSession().write(MaplePacketCreator.getShowQuestCompletion(q.getQuest().getId()));
                }
            }
    }

    public Map<Skill, SkillEntry> getSkills(boolean packet)
    {
        if (!packet)
        {
            return Collections.unmodifiableMap(this.skills);
        }
        Map<Skill, SkillEntry> oldlist = new LinkedHashMap(this.skills);
        Map<Skill, SkillEntry> newlist = new LinkedHashMap<>();
        for (Map.Entry<Skill, SkillEntry> skill : oldlist.entrySet())
        {
            if ((!skill.getKey().isAngelSkill()) && (!skill.getKey().isLinkedAttackSkill()) && (!skill.getKey().isDefaultSkill()))
            {


                newlist.put(skill.getKey(), skill.getValue());
            }
        }
        return newlist;
    }

    public int getAllSkillLevels()
    {
        int rett = 0;
        for (Map.Entry<Skill, SkillEntry> ret : this.skills.entrySet())
        {
            if ((!ret.getKey().isBeginnerSkill()) && (!ret.getKey().isSpecialSkill()) && (ret.getValue().skillevel > 0))
            {
                rett += ret.getValue().skillevel;
            }
        }
        return rett;
    }

    public long getSkillExpiry(Skill skill)
    {
        if (skill == null)
        {
            return 0L;
        }
        SkillEntry ret = this.skills.get(skill);
        if ((ret == null) || (ret.skillevel <= 0))
        {
            return 0L;
        }
        return ret.expiration;
    }

    public byte getMasterLevel(int skillId)
    {
        return getMasterLevel(SkillFactory.getSkill(skillId));
    }

    public byte getMasterLevel(Skill skill)
    {
        SkillEntry ret = this.skills.get(skill);
        if (ret == null)
        {
            return 0;
        }
        return ret.masterlevel;
    }

    public byte getSkillPosition(int skillId)
    {
        return getSkillPosition(SkillFactory.getSkill(skillId));
    }

    public byte getSkillPosition(Skill skill)
    {
        if (skill == null)
        {
            return -1;
        }
        SkillEntry ret = this.skills.get(skill);
        if ((ret == null) || (ret.position == -1))
        {
            return -1;
        }
        return ret.position;
    }

    public void levelUp()
    {
        levelUp(false);
    }

    public void levelUp(boolean takeexp)
    {
        int vipAp = getVip() > 1 ? getVip() - 1 : 0;
        if ((GameConstants.is炎术士(this.job)) || (GameConstants.is夜行者(this.job)))
        {
            if (this.level <= 70)
            {
                this.remainingAp = ((short) (this.remainingAp + 6));
            }
            else
            {
                this.remainingAp = ((short) (this.remainingAp + 5));
            }
        }
        else
        {
            this.remainingAp = ((short) (this.remainingAp + 5));
        }
        int maxhp = this.stats.getMaxHp();
        int maxmp = this.stats.getMaxMp();

        if (GameConstants.is新手职业(this.job))
        {
            maxhp += Randomizer.rand(12, 16);
            maxmp += Randomizer.rand(10, 12);
        }
        else if ((this.job >= 3100) && (this.job <= 3112))
        {
            maxhp += Randomizer.rand(48, 52);
        }
        else if ((this.job == 3101) || (this.job == 3120) || (this.job == 3121) || (this.job == 3122))
        {
            maxhp += Randomizer.rand(30, 40);
        }
        else if (((this.job >= 100) && (this.job <= 132)) || ((this.job >= 1100) && (this.job <= 1112)) || ((this.job >= 5100) && (this.job <= 5112)))
        {

            maxhp += Randomizer.rand(48, 52);
            maxmp += Randomizer.rand(4, 6);
        }
        else if (((this.job >= 200) && (this.job <= 232)) || ((this.job >= 1200) && (this.job <= 1212)) || ((this.job >= 2700) && (this.job <= 2712)))
        {

            maxhp += Randomizer.rand(10, 14);
            maxmp += Randomizer.rand(48, 52);
        }
        else if ((this.job >= 3200) && (this.job <= 3212))
        {
            maxhp += Randomizer.rand(20, 24);
            maxmp += Randomizer.rand(42, 44);
        }
        else if (((this.job >= 300) && (this.job <= 322)) || ((this.job >= 400) && (this.job <= 434)) || ((this.job >= 1300) && (this.job <= 1312)) || ((this.job >= 1400) && (this.job <= 1412)) || ((this.job >= 2300) && (this.job <= 2312)) || ((this.job >= 2400) && (this.job <= 2412)) || ((this.job >= 3300) && (this.job <= 3312)) || ((this.job >= 3600) && (this.job <= 3612)))
        {


            maxhp += Randomizer.rand(20, 24);
            maxmp += Randomizer.rand(14, 16);
        }
        else if (((this.job >= 510) && (this.job <= 512)) || ((this.job >= 580) && (this.job <= 582)) || ((this.job >= 1510) && (this.job <= 1512)) || ((this.job >= 6500) && (this.job <= 6512)))
        {


            maxhp += Randomizer.rand(37, 41);
            maxmp += Randomizer.rand(18, 22);
        }
        else if (((this.job >= 500) && (this.job <= 532)) || ((this.job >= 570) && (this.job <= 572)) || (this.job == 508) || ((this.job >= 590) && (this.job <= 592)) || ((this.job >= 3500) && (this.job <= 3512)) || (this.job == 1500))
        {


            maxhp += Randomizer.rand(22, 26);
            maxmp += Randomizer.rand(18, 22);
        }
        else if ((this.job >= 2100) && (this.job <= 2112))
        {
            maxhp += Randomizer.rand(50, 52);
            maxmp += Randomizer.rand(4, 6);
        }
        else if ((this.job >= 2200) && (this.job <= 2218))
        {
            maxhp += Randomizer.rand(12, 16);
            maxmp += Randomizer.rand(50, 52);
        }
        else if ((this.job >= 6100) && (this.job <= 6112))
        {
            maxhp += Randomizer.rand(68, 74);
            maxmp += Randomizer.rand(4, 6);
        }
        else if ((this.job >= 10100) && (this.job <= 10112))
        {
            maxhp += Randomizer.rand(48, 52);
        }
        else if ((this.job >= 11200) && (this.job <= 11212))
        {
            maxhp += Randomizer.rand(38, 42);
            maxmp += Randomizer.rand(20, 24);
        }
        else
        {
            maxhp += Randomizer.rand(24, 38);
            maxmp += Randomizer.rand(12, 24);
            if ((this.job != 800) && (this.job != 900) && (this.job != 910))
            {
                System.err.println("出现未处理的角色升级加血职业: " + this.job);
            }
        }
        maxmp += (GameConstants.isNotMpJob(getJob()) ? 0 : this.stats.getTotalInt() / 10);
        if ((GameConstants.is夜光(this.job)) && (getSkillLevel(20040221) > 0))
        {
            maxmp += Randomizer.rand(18, 22);
        }
        if (takeexp)
        {
            this.exp.addAndGet(getExpNeededForLevel());
            if (this.exp.get() < 0L)
            {
                this.exp.set(0L);
            }
        }
        else
        {
            setExp(0L);
        }
        this.level = ((short) (this.level + 1));
        if (this.level >= getMaxLevelForSever())
        {
            setExp(0L);
        }
        maxhp = Math.min(getMaxHpForSever(), Math.abs(maxhp));
        maxmp = Math.min(getMaxMpForSever(), Math.abs(maxmp));
        if (GameConstants.is恶魔猎手(this.job))
        {
            maxmp = GameConstants.getMPByJob(this.job);
        }
        else if (GameConstants.is神之子(this.job))
        {
            maxmp = 100;
            checkZeroWeapon();
        }
        else if (GameConstants.isNotMpJob(this.job))
        {
            maxmp = 10;
        }
        if (this.level == 30)
        {
            finishAchievement(2);
        }
        if (this.level == 70)
        {
            finishAchievement(3);
        }
        if (this.level == 120)
        {
            finishAchievement(4);
        }
        if (this.level == 200)
        {
            finishAchievement(5);
        }
        if ((this.level == 250) && (!isGM()))
        {
            String sb = "[祝贺] " + getMedalText() + getName() + "终于达到了250级.大家一起祝贺下吧。";
            handling.world.WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(6, sb));
        }
        Map<MapleStat, Long> statup = new EnumMap(MapleStat.class);
        statup.put(MapleStat.MAXHP, (long) maxhp);
        statup.put(MapleStat.MAXMP, (long) maxmp);
        statup.put(MapleStat.HP, (long) maxhp);
        statup.put(MapleStat.MP, (long) maxmp);
        statup.put(MapleStat.经验, this.exp.get());
        statup.put(MapleStat.等级, (long) this.level);

        if ((isGM()) || (!GameConstants.is新手职业(this.job)))
        {
            if ((GameConstants.is神之子(this.job)) && (this.level >= 100))
            {
                this.remainingSp[0] += 3;
                this.remainingSp[1] += 3;
            }
            else
            {
                this.remainingSp[GameConstants.getSkillBookByLevel(this.job, this.level)] += 3;
            }
        }
        else if (this.level < 10)
        {
            PlayerStats tmp1705_1702 = this.stats;
            tmp1705_1702.str = ((short) (tmp1705_1702.str + this.remainingAp));
            this.remainingAp = 0;
            statup.put(MapleStat.力量, (long) this.stats.getStr());
        }
        else if (this.level == 10)
        {
            resetStats(4, 4, 4, 4);
        }


        statup.put(MapleStat.AVAILABLEAP, (long) this.remainingAp);
        statup.put(MapleStat.AVAILABLESP, (long) this.remainingSp[GameConstants.getSkillBookByLevel(this.job, this.level)]);
        this.stats.setInfo(maxhp, maxmp, maxhp, maxmp);
        this.client.getSession().write(MaplePacketCreator.updatePlayerStats(statup, this));
        this.map.broadcastMessage(this, MaplePacketCreator.showForeignEffect(getId(), 0), false);
        this.characterCard.recalcLocalStats(this);
        this.stats.recalcLocalStats(this);
        silentPartyUpdate();
        guildUpdate();
        sidekickUpdate();
        familyUpdate();
        checkBeastTamerSkill();

        if ((!GameConstants.is龙神(this.job)) && (!GameConstants.is暗影双刀(this.job)))
        {
            autoChangeJob();
        }
        else if (GameConstants.is龙神(this.job))
        {
            int oldJobId = getJob();
            switch (this.level)
            {
                case 10:
                    changeJob(2200);
                    break;
                case 20:
                    changeJob(2210);
                    break;
                case 30:
                    changeJob(2211);
                    break;
                case 40:
                    changeJob(2212);
                    break;
                case 50:
                    changeJob(2213);
                    break;
                case 60:
                    changeJob(2214);
                    break;
                case 80:
                    changeJob(2215);
                    break;
                case 100:
                    changeJob(2216);
                    break;
                case 120:
                    changeJob(2217);
                    break;
                case 160:
                    changeJob(2218);
            }

            if ((oldJobId != getJob()) && (isGM()))
            {

                dropMessage(5, "[转职提示] 恭喜您等级达到 " + this.level + " 级，系统自动为您转职为: " + MapleCarnivalChallenge.getJobNameByIdNull(getJob()));
            }
        }
        else if (GameConstants.is暗影双刀(this.job))
        {
            int oldJobId = getJob();
            switch (this.level)
            {
                case 20:
                    changeJob(430);
                    break;
                case 30:
                    changeJob(431);
                    break;
                case 55:
                    changeJob(432);
                    break;
                case 70:
                    changeJob(433);
                    break;
                case 100:
                    changeJob(434);
            }

            if ((oldJobId != getJob()) && (isGM()))
            {

                dropMessage(5, "[转职提示] 恭喜您等级达到 " + this.level + " 级，系统自动为您转职为: " + MapleCarnivalChallenge.getJobNameByIdNull(getJob()));
            }
        }
        else if ((getSubcategory() == 10) && (getJob() == 0) && (this.level == 10))
        {
            changeJob(508);
            if (isGM())
            {
            }
        }


        if ((GameConstants.is龙的传人(getJob())) && (getSkillLevel(1214) > 0))
        {
            if (this.coreAura == null)
            {
                this.coreAura = MapleCoreAura.createCoreAura(this.id, this.level);
            }
            else
            {
                this.coreAura.setLevel(this.level);
            }
            updataCoreAura();
        }
    }

    public void autoChangeJob()
    {
        int oldJobId = getJob();
        if (this.level == 10)
        {
            switch (oldJobId)
            {
                case 2000:
                    changeJob(2100);
                    break;
                case 2001:
                    changeJob(2200);
                    break;
                case 2002:
                    changeJob(2300);
                    break;
                case 2003:
                    changeJob(2400);
                    break;
                case 2004:
                    changeJob(2700);
                    break;
                case 3001:
                    break;

                case 3002:
                    changeJob(3600);
                    break;
                case 5000:
                    changeJob(5100);
                    break;
                case 6000:
                    changeJob(6100);
                    break;
                case 6001:
                    changeJob(6500);
            }

        }
        else if (this.level == 30)
        {
            switch (oldJobId)
            {
                case 501:
                    changeJob(530);
                    break;


                case 1100:
                case 1200:
                case 1300:
                case 1400:
                case 1500:
                case 2100:
                case 2300:
                case 2400:
                case 2700:
                case 3200:
                case 3300:
                case 3500:
                case 3600:
                case 5100:
                case 6100:
                case 6500:
                    if (isValidJob(oldJobId + 10))
                    {
                        changeJob(oldJobId + 10);
                    }
                    break;
            }
        }
        else if (this.level == 60)
        {
            switch (oldJobId)
            {

                case 110:
                case 120:
                case 130:
                case 210:
                case 220:
                case 230:
                case 310:
                case 320:
                case 410:
                case 420:
                case 510:
                case 520:
                case 530:
                case 570:
                case 1110:
                case 1310:
                case 1510:
                case 2110:
                case 2310:
                case 2410:
                case 2710:
                case 3210:
                case 3310:
                case 3510:
                case 3610:
                case 5110:
                case 6110:
                case 6510:
                    if (isValidJob(oldJobId + 1)) changeJob(oldJobId + 1);
                    break;
            }

        }
        else if (this.level == 70)
        {
            switch (oldJobId)
            {
                case 1210:
                case 1410:
                    if (isValidJob(oldJobId + 1))
                    {
                        changeJob(oldJobId + 1);
                    }
                    break;
            }
        }
        else if (this.level == 100)
        {
            switch (oldJobId)
            {

                case 111:
                case 121:
                case 131:
                case 211:
                case 221:
                case 231:
                case 311:
                case 321:
                case 411:
                case 421:
                case 511:
                case 521:
                case 531:
                case 571:
                case 1111:
                case 1311:
                case 1511:
                case 2111:
                case 2311:
                case 2411:
                case 2711:
                case 3211:
                case 3311:
                case 3511:
                case 3611:
                case 5111:
                case 6111:
                case 6511:
                    if (isValidJob(oldJobId + 1)) changeJob(oldJobId + 1);
                    break;
            }

        }
        else if (this.level == 120)
        {
            switch (oldJobId)
            {
                case 1211:
                case 1411:
                    if (isValidJob(oldJobId + 1))
                    {
                        changeJob(oldJobId + 1);
                    }
                    break;
            }
        }
        if ((oldJobId != getJob()) && (isGM()))
        {

            dropMessage(5, "[转职提示] 恭喜您等级达到 " + this.level + " 级，系统自动为您转职为: " + MapleCarnivalChallenge.getJobNameByIdNull(getJob()));
        }
    }

    public boolean isValidJob(int id)
    {
        return MapleCarnivalChallenge.getJobNameByIdNull(id) != null;
    }

    public void changeKeybinding(int key, byte type, int action)
    {
        if (type != 0)
        {
            this.keylayout.Layout().put(key, new Pair(type, action));
        }
        else
        {
            this.keylayout.Layout().remove(key);
        }
    }

    public void sendMacros()
    {
        this.client.getSession().write(MaplePacketCreator.getMacros(this.skillMacros));
    }

    public void updateMacros(int position, SkillMacro updateMacro)
    {
        this.skillMacros[position] = updateMacro;
        this.changed_skillmacros = true;
    }

    public SkillMacro[] getMacros()
    {
        return this.skillMacros;
    }

    public void tempban(String reason, Calendar duration, int greason, boolean IPMac)
    {
        if (IPMac)
        {
            this.client.banMacs();
        }
        this.client.getSession().write(MaplePacketCreator.GMPoliceMessage());
        try
        {
            Connection con = DatabaseConnection.getConnection();

            if (IPMac)
            {
                PreparedStatement ps = con.prepareStatement("INSERT INTO ipbans VALUES (DEFAULT, ?)");
                ps.setString(1, this.client.getSession().getRemoteAddress().toString().split(":")[0]);
                ps.execute();
                ps.close();
            }
            this.client.disconnect(true, false);
            this.client.getSession().close(true);
            PreparedStatement ps = con.prepareStatement("UPDATE accounts SET tempban = ?, banreason = ?, greason = ? WHERE id = ?");
            Timestamp TS = new Timestamp(duration.getTimeInMillis());
            ps.setTimestamp(1, TS);
            ps.setString(2, reason);
            ps.setInt(3, greason);
            ps.setInt(4, this.accountid);
            ps.execute();
            ps.close();
        }
        catch (SQLException ex)
        {
            System.err.println("Error while tempbanning" + ex);
        }
    }

    public boolean ban(String reason, boolean IPMac, boolean autoban, boolean hellban)
    {
        gainWarning(false);
        this.client.getSession().write(MaplePacketCreator.GMPoliceMessage());
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE accounts SET banned = ?, banreason = ? WHERE id = ?");
            ps.setInt(1, autoban ? 2 : 1);
            ps.setString(2, reason);
            ps.setInt(3, this.accountid);
            ps.execute();
            ps.close();
            if (IPMac)
            {
                this.client.banMacs();
                ps = con.prepareStatement("INSERT INTO ipbans VALUES (DEFAULT, ?)");
                ps.setString(1, this.client.getSessionIPAddress());
                ps.execute();
                ps.close();
                if (hellban)
                {
                    PreparedStatement psa = con.prepareStatement("SELECT * FROM accounts WHERE id = ?");
                    psa.setInt(1, this.accountid);
                    ResultSet rsa = psa.executeQuery();
                    if (rsa.next())
                    {
                        PreparedStatement pss = con.prepareStatement("UPDATE accounts SET banned = ?, banreason = ? WHERE email = ? OR SessionIP = ?");
                        pss.setInt(1, autoban ? 2 : 1);
                        pss.setString(2, reason);
                        pss.setString(3, rsa.getString("email"));
                        pss.setString(4, this.client.getSessionIPAddress());
                        pss.execute();
                        pss.close();
                    }
                    rsa.close();
                    psa.close();
                }
            }
        }
        catch (SQLException ex)
        {
            System.err.println("Error while banning" + ex);
            return false;
        }
        this.client.disconnect(true, false);
        this.client.getSession().close(true);
        return true;
    }

    public server.MapleStorage getStorage()
    {
        return this.storage;
    }

    public void removeVisibleMapObject(MapleMapObject mo)
    {
        visibleMapObjectsLock.writeLock().lock();
        try
        {
            visibleMapObjects.remove(mo);
        }
        finally
        {
            visibleMapObjectsLock.writeLock().unlock();
        }
    }

    public boolean isMapObjectVisible(MapleMapObject mo)
    {
        this.visibleMapObjectsLock.readLock().lock();
        try
        {
            return this.visibleMapObjects.contains(mo);
        }
        finally
        {
            this.visibleMapObjectsLock.readLock().unlock();
        }
    }

    public java.util.Collection<MapleMapObject> getAndWriteLockVisibleMapObjects()
    {
        this.visibleMapObjectsLock.writeLock().lock();
        return this.visibleMapObjects;
    }

    public void unlockWriteVisibleMapObjects()
    {
        this.visibleMapObjectsLock.writeLock().unlock();
    }

    public void checkCopyItems()
    {
        List<Integer> equipOnlyIds = new ArrayList<>();
        Map<Integer, Integer> checkItems = new HashMap<>();

        for (Item item : getInventory(MapleInventoryType.EQUIP).list())
        {
            int equipOnlyId = item.getEquipOnlyId();
            if (equipOnlyId > 0)
            {
                if (checkItems.containsKey(equipOnlyId))
                {
                    if (checkItems.get(equipOnlyId) == item.getItemId())
                    {
                        equipOnlyIds.add(equipOnlyId);
                    }
                }
                else
                {
                    checkItems.put(equipOnlyId, item.getItemId());
                }
            }
        }
        for (Item item : getInventory(MapleInventoryType.EQUIPPED).list())
        {
            int equipOnlyId = item.getEquipOnlyId();
            if (equipOnlyId > 0)
            {
                if (checkItems.containsKey(equipOnlyId))
                {
                    if (checkItems.get(equipOnlyId) == item.getItemId())
                    {
                        equipOnlyIds.add(equipOnlyId);
                    }
                }
                else
                {
                    checkItems.put(equipOnlyId, item.getItemId());
                }
            }
        }
        Item item;
        boolean autoban = false;
        for (Integer equipOnlyId : equipOnlyIds)
        {
            MapleInventoryManipulator.removeAllByEquipOnlyId(this.client, equipOnlyId);
            autoban = true;
        }
        if (autoban)
        {
            server.AutobanManager.getInstance().autoban(this.client, "无理由.");
        }
        checkItems.clear();
        equipOnlyIds.clear();
    }

    public List<MaplePet> getPets()
    {
        List<MaplePet> ret = new ArrayList<>();
        for (Item item : getInventory(MapleInventoryType.CASH).newList())
        {
            if (item.getPet() != null)
            {
                ret.add(item.getPet());
            }
        }
        return ret;
    }

    public MaplePet[] getSpawnPets()
    {
        return this.spawnPets;
    }

    public byte getPetIndex(int petId)
    {
        for (byte i = 0; i < 3; i = (byte) (i + 1))
        {
            if ((this.spawnPets[i] != null) && (this.spawnPets[i].getUniqueId() == petId))
            {
                return i;
            }
        }

        return -1;
    }

    public byte getPetByItemId(int petItemId)
    {
        for (byte i = 0; i < 3; i = (byte) (i + 1))
        {
            if ((this.spawnPets[i] != null) && (this.spawnPets[i].getPetItemId() == petItemId))
            {
                return i;
            }
        }

        return -1;
    }

    public int getNextEmptyPetIndex()
    {
        for (int i = 0; i < 3; i++)
        {
            if (this.spawnPets[i] == null)
            {
                return i;
            }
        }
        return 3;
    }

    public int getNoPets()
    {
        int ret = 0;
        for (int i = 0; i < 3; i++)
        {
            if (this.spawnPets[i] != null)
            {
                ret++;
            }
        }
        return ret;
    }

    public List<MaplePet> getSummonedPets()
    {
        List<MaplePet> ret = new ArrayList<>();
        for (byte i = 0; i < 3; i = (byte) (i + 1))
        {
            if ((this.spawnPets[i] != null) && (this.spawnPets[i].getSummoned()))
            {
                ret.add(this.spawnPets[i]);
            }
        }
        return ret;
    }

    public void addSpawnPet(MaplePet pet)
    {
        for (int i = 0; i < 3; i++)
        {
            if (this.spawnPets[i] == null)
            {
                this.spawnPets[i] = pet;
                pet.setSummoned((byte) (i + 1));
                return;
            }
        }
    }

    public void unequipAllSpawnPets()
    {
        for (int i = 0; i < 3; i++)
        {
            if (this.spawnPets[i] != null)
            {
                unequipSpawnPet(this.spawnPets[i], true, false);
            }
        }
    }

    public void unequipSpawnPet(MaplePet pet, boolean shiftLeft, boolean hunger)
    {
        if (getSpawnPet(getPetIndex(pet)) != null)
        {
            getSpawnPet(getPetIndex(pet)).setSummoned(0);
            getSpawnPet(getPetIndex(pet)).saveToDb();
        }
        this.client.getSession().write(tools.packet.PetPacket.updatePet(pet, getInventory(MapleInventoryType.CASH).getItem((byte) pet.getInventoryPosition()), false));
        if (this.map != null)
        {
            this.map.broadcastMessage(this, tools.packet.PetPacket.showPet(this, pet, true, hunger), true);
        }
        removeSpawnPet(pet, shiftLeft);
        checkPetSkill();


        this.client.getSession().write(MaplePacketCreator.enableActions());
    }

    public MaplePet getSpawnPet(int index)
    {
        return this.spawnPets[index];
    }

    public byte getPetIndex(MaplePet pet)
    {
        for (byte i = 0; i < 3; i = (byte) (i + 1))
        {
            if ((this.spawnPets[i] != null) && (this.spawnPets[i].getUniqueId() == pet.getUniqueId()))
            {
                return i;
            }
        }

        return -1;
    }

    public void removeSpawnPet(MaplePet pet, boolean shiftLeft)
    {
        for (int i = 0; i < 3; i++)
        {
            if ((this.spawnPets[i] != null) && (this.spawnPets[i].getUniqueId() == pet.getUniqueId()))
            {
                this.spawnPets[i] = null;
                break;
            }
        }
    }

    public void checkPetSkill()
    {
        Map<Integer, Integer> setHandling = new HashMap<>();
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int value;
        for (int i = 0; i < 3; i++)
        {
            if (this.spawnPets[i] != null)
            {
                int set = ii.getPetSetItemID(this.spawnPets[i].getPetItemId());
                if (set > 0)
                {
                    value = 1;
                    if (setHandling.containsKey(set))
                    {
                        value += setHandling.get(set);
                    }
                    setHandling.put(set, value);
                }
            }
        }
        if (setHandling.isEmpty())
        {
            Map<Skill, SkillEntry> chrSkill = new HashMap(getSkills());
            Map<Skill, SkillEntry> petSkill = new HashMap<>();
            for (Map.Entry<Skill, SkillEntry> skill : chrSkill.entrySet())
            {
                if (skill.getKey().isPetPassive())
                {
                    petSkill.put(skill.getKey(), new SkillEntry(0, (byte) 0, -1L));
                }
            }
            if (!petSkill.isEmpty())
            {
                changePetSkillLevel(petSkill);
            }
            return;
        }
        Map<Skill, SkillEntry> petSkillData = new HashMap<>();
        Iterator<Map.Entry<Integer, Integer>> iter = setHandling.entrySet().iterator();
        Map.Entry<Integer, Integer> entry;
        while (iter.hasNext())
        {
            entry = iter.next();
            server.StructSetItem setItem = ii.getSetItem(entry.getKey());
            if (setItem != null)
            {
                Map<Integer, StructSetItemStat> setItemStats = setItem.getSetItemStats();
                for (Map.Entry<Integer, StructSetItemStat> ent : setItemStats.entrySet())
                {
                    StructSetItemStat setItemStat = ent.getValue();
                    if (ent.getKey() <= entry.getValue())
                    {
                        if ((setItemStat.skillId > 0) && (setItemStat.skillLevel > 0) && (getSkillLevel(setItemStat.skillId) <= 0))
                        {
                            petSkillData.put(SkillFactory.getSkill(setItemStat.skillId), new SkillEntry((byte) setItemStat.skillLevel, (byte) 0, -1L));
                        }
                    }
                    else if ((setItemStat.skillId > 0) && (setItemStat.skillLevel > 0) && (getSkillLevel(setItemStat.skillId) > 0))
                    {
                        petSkillData.put(SkillFactory.getSkill(setItemStat.skillId), new SkillEntry(0, (byte) 0, -1L));
                    }
                }
            }
        }

        if (!petSkillData.isEmpty())
        {
            changePetSkillLevel(petSkillData);
        }
    }

    public Map<Skill, SkillEntry> getSkills()
    {
        return Collections.unmodifiableMap(this.skills);
    }

    public void changePetSkillLevel(Map<Skill, SkillEntry> skill)
    {
        if (skill.isEmpty())
        {
            return;
        }
        Map<Skill, SkillEntry> newlist = new HashMap<>();
        for (Map.Entry<Skill, SkillEntry> date : skill.entrySet())
        {
            if (date.getKey() != null)
            {


                if ((date.getValue().skillevel == 0) && (date.getValue().masterlevel == 0))
                {
                    if (this.skills.containsKey(date.getKey()))
                    {
                        this.skills.remove(date.getKey());
                        newlist.put(date.getKey(), date.getValue());
                    }

                }
                else if (getSkillLevel(date.getKey()) != date.getValue().skillevel)
                {
                    this.skills.put(date.getKey(), date.getValue());
                    newlist.put(date.getKey(), date.getValue());
                }
            }
        }
        if (!newlist.isEmpty())
        {
            for (Map.Entry<Skill, SkillEntry> date : newlist.entrySet())
            {
                this.client.getSession().write(MaplePacketCreator.updatePetSkill(date.getKey().getId(), date.getValue().skillevel, date.getValue().masterlevel, date.getValue().expiration));
            }
            reUpdateStat(false, true);
        }
    }

    private void reUpdateStat(boolean hasRecovery, boolean recalculate)
    {
        this.changed_skills = true;
        if (hasRecovery)
        {
            this.stats.relocHeal(this);
        }
        if (recalculate)
        {
            this.stats.recalcLocalStats(this);
        }
    }

    public void spawnPet(byte slot)
    {
        spawnPet(slot, false, true);
    }

    public void spawnPet(byte slot, boolean lead)
    {
        spawnPet(slot, lead, true);
    }

    public void spawnPet(byte slot, boolean lead, boolean broadcast)
    {
        Item item = getInventory(MapleInventoryType.CASH).getItem(slot);
        if ((item == null) || (!ItemConstants.isPet(item.getItemId())))
        {
            this.client.getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        MaplePet pet;
        switch (item.getItemId())
        {
            case 5000028:
            case 5000047:
                pet = MaplePet.createPet(item.getItemId() + 1, MapleInventoryIdentifier.getInstance());
                if (pet != null)
                {
                    MapleInventoryManipulator.addById(this.client, item.getItemId() + 1, (short) 1, item.getOwner(), pet, 90L,
                            "双击宠物获得: " + item.getItemId() + " 时间: " + FileoutputUtil.CurrentReadable_Date());
                    MapleInventoryManipulator.removeFromSlot(this.client, MapleInventoryType.CASH, slot, (short) 1, false);
                }

                break;
            default:
                pet = item.getPet();
                if ((pet != null) && ((item.getItemId() != 5000054) || (pet.getSecondsLeft() > 0)) && ((item.getExpiration() == -1L) || (item.getExpiration() > System.currentTimeMillis())))
                    if (getPetIndex(pet) != -1)
                    {
                        unequipSpawnPet(pet, true, false);
                    }
                    else
                    {
                        int leadid = 8;
                        if (GameConstants.is骑士团(getJob()))
                        {
                            leadid = 10000018;
                        }
                        else if (GameConstants.is战神(getJob()))
                        {
                            leadid = 20000024;
                        }
                        else if (GameConstants.is龙神(getJob()))
                        {
                            leadid = 20011024;
                        }
                        if (((getSkillLevel(SkillFactory.getSkill(leadid)) == 0) || (getNoPets() == 3)) && (getSpawnPet(0) != null))
                        {
                            unequipSpawnPet(getSpawnPet(0), false, false);
                        }
                        else if ((!lead) || (getSkillLevel(SkillFactory.getSkill(leadid)) <= 0))
                        {
                        }


                        Point pos = getPosition();
                        pet.setPos(pos);
                        try
                        {
                            pet.setFh(getMap().getFootholds().findBelow(pos).getId());
                        }
                        catch (NullPointerException e)
                        {
                            pet.setFh(0);
                        }
                        pet.setStance(0);
                        if (getSkillLevel(pet.getBuffSkill()) == 0)
                        {
                            pet.setBuffSkill(0);
                        }
                        pet.setCanPickup(getIntRecord(122902) > 0);
                        addSpawnPet(pet);
                        if (getMap() != null)
                        {
                            this.client.getSession().write(tools.packet.PetPacket.updatePet(pet, getInventory(MapleInventoryType.CASH).getItem((byte) pet.getInventoryPosition()), true));
                            getMap().broadcastMessage(this, tools.packet.PetPacket.showPet(this, pet, false, false), true);
                            this.client.getSession().write(tools.packet.PetPacket.loadExceptionList(this, pet));

                            checkPetSkill();
                        }
                    }
                break;
        }


        this.client.getSession().write(MaplePacketCreator.enableActions());
    }

    public long getLastFameTime()
    {
        return this.lastfametime;
    }

    public List<Integer> getFamedCharacters()
    {
        return this.lastmonthfameids;
    }

    public List<Integer> getBattledCharacters()
    {
        return this.lastmonthbattleids;
    }

    public FameStatus canGiveFame(MapleCharacter from)
    {
        if (this.lastfametime >= System.currentTimeMillis() - 86400000L) return FameStatus.NOT_TODAY;
        if ((from == null) || (this.lastmonthfameids == null) || (this.lastmonthfameids.contains(from.getId())))
        {
            return FameStatus.NOT_THIS_MONTH;
        }
        return FameStatus.OK;
    }

    public void hasGivenFame(MapleCharacter to)
    {
        this.lastfametime = System.currentTimeMillis();
        this.lastmonthfameids.add(to.getId());
        Connection con = DatabaseConnection.getConnection();
        try
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO famelog (characterid, characterid_to) VALUES (?, ?)");
            ps.setInt(1, getId());
            ps.setInt(2, to.getId());
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("ERROR writing famelog for char " + getName() + " to " + to.getName() + e);
        }
    }

    public boolean canBattle(MapleCharacter to)
    {
        return (to != null) && (this.lastmonthbattleids != null) && (!this.lastmonthbattleids.contains(to.getAccountID()));
    }

    public void hasBattled(MapleCharacter to)
    {
        this.lastmonthbattleids.add(to.getAccountID());
        Connection con = DatabaseConnection.getConnection();
        try
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO battlelog (accid, accid_to) VALUES (?, ?)");
            ps.setInt(1, getAccountID());
            ps.setInt(2, to.getAccountID());
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("ERROR writing battlelog for char " + getName() + " to " + to.getName() + e);
        }
    }

    public MapleKeyLayout getKeyLayout()
    {
        return this.keylayout;
    }

    public MapleQuickSlot getQuickSlot()
    {
        return this.quickslot;
    }

    public void addDoor(MapleDoor door)
    {
        this.doors.add(door);
    }

    public void clearDoors()
    {
        this.doors.clear();
    }

    public List<MapleDoor> getDoors()
    {
        return new ArrayList(this.doors);
    }

    public void addMechDoor(server.maps.MechDoor door)
    {
        this.mechDoors.add(door);
    }

    public void clearMechDoors()
    {
        this.mechDoors.clear();
    }

    public List<server.maps.MechDoor> getMechDoors()
    {
        return new ArrayList(this.mechDoors);
    }

    public void setSmega()
    {
        if (this.smega)
        {
            this.smega = false;
            dropMessage(5, "You have set megaphone to disabled mode");
        }
        else
        {
            this.smega = true;
            dropMessage(5, "You have set megaphone to enabled mode");
        }
    }

    public boolean getSmega()
    {
        return this.smega;
    }

    public int getChair()
    {
        return this.chair;
    }

    public void setChair(int chair)
    {
        this.chair = chair;
        this.stats.relocHeal(this);
    }

    public int getItemEffect()
    {
        return this.itemEffect;
    }

    public void setItemEffect(int itemEffect)
    {
        this.itemEffect = itemEffect;
    }

    public int getTitleEffect()
    {
        return this.titleEffect;
    }

    public void setTitleEffect(int titleEffect)
    {
        this.titleEffect = titleEffect;
    }

    public int getFamilyId()
    {
        if (this.mfc == null)
        {
            return 0;
        }
        return this.mfc.getFamilyId();
    }

    public int getSeniorId()
    {
        if (this.mfc == null)
        {
            return 0;
        }
        return this.mfc.getSeniorId();
    }

    public int getJunior1()
    {
        if (this.mfc == null)
        {
            return 0;
        }
        return this.mfc.getJunior1();
    }

    public int getJunior2()
    {
        if (this.mfc == null)
        {
            return 0;
        }
        return this.mfc.getJunior2();
    }

    public int getCurrentRep()
    {
        return this.currentrep;
    }

    public void setCurrentRep(int newRank)
    {
        this.currentrep = newRank;
        if (this.mfc != null)
        {
            this.mfc.setCurrentRep(newRank);
        }
    }

    public int getTotalRep()
    {
        return this.totalrep;
    }

    public void setTotalRep(int newRank)
    {
        this.totalrep = newRank;
        if (this.mfc != null)
        {
            this.mfc.setTotalRep(newRank);
        }
    }

    public int getTotalWins()
    {
        return this.totalWins;
    }

    public int getTotalLosses()
    {
        return this.totalLosses;
    }

    public void increaseTotalWins()
    {
        this.totalWins += 1;
    }

    public void increaseTotalLosses()
    {
        this.totalLosses += 1;
    }

    public byte getGuildRank()
    {
        return this.guildrank;
    }

    public void setGuildRank(byte newRank)
    {
        this.guildrank = newRank;
        if (this.mgc != null)
        {
            this.mgc.setGuildRank(newRank);
        }
    }

    public int getGuildContribution()
    {
        return this.guildContribution;
    }

    public void setGuildContribution(int newContribution)
    {
        this.guildContribution = newContribution;
        if (this.mgc != null)
        {
            this.mgc.setGuildContribution(newContribution);
        }
    }

    public MapleGuildCharacter getMGC()
    {
        return this.mgc;
    }

    public byte getAllianceRank()
    {
        return this.allianceRank;
    }

    public void setAllianceRank(byte newRank)
    {
        this.allianceRank = newRank;
        if (this.mgc != null)
        {
            this.mgc.setAllianceRank(newRank);
        }
    }

    public MapleGuild getGuild()
    {
        if (getGuildId() <= 0)
        {
            return null;
        }
        return handling.world.WorldGuildService.getInstance().getGuild(getGuildId());
    }

    public int getGuildId()
    {
        return this.guildid;
    }

    public void setGuildId(int newGuildId)
    {
        this.guildid = newGuildId;
        if (this.guildid > 0)
        {
            if (this.mgc == null)
            {
                this.mgc = new MapleGuildCharacter(this);
            }
            else
            {
                this.mgc.setGuildId(this.guildid);
            }
        }
        else
        {
            this.mgc = null;
            this.guildContribution = 0;
        }
    }

    public void sidekickUpdate()
    {
        if (this.sidekick == null)
        {
            return;
        }
        this.sidekick.getCharacter(this.sidekick.getCharacter(0).getId() == getId() ? 0 : 1).update(this);
        if (!MapleSidekick.checkLevels(getLevel(), this.sidekick.getCharacter(this.sidekick.getCharacter(0).getId() == getId() ? 1 : 0).getLevel()))
        {
            this.sidekick.eraseToDB();
        }
    }

    public void guildUpdate()
    {
        if (this.guildid <= 0)
        {
            return;
        }
        this.mgc.setLevel(this.level);
        this.mgc.setJobId(this.job);
        handling.world.WorldGuildService.getInstance().memberLevelJobUpdate(this.mgc);
    }

    public void saveGuildStatus()
    {
        handling.world.guild.MapleGuild.setOfflineGuildStatus(this.guildid, this.guildrank, this.guildContribution, this.allianceRank, this.id);
    }

    public void familyUpdate()
    {
        if (this.mfc == null)
        {
            return;
        }
        handling.world.WorldFamilyService.getInstance().memberFamilyUpdate(this.mfc, this);
    }

    public void saveFamilyStatus()
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE characters SET familyid = ?, seniorid = ?, junior1 = ?, junior2 = ? WHERE id = ?");
            if (this.mfc == null)
            {
                ps.setInt(1, 0);
                ps.setInt(2, 0);
                ps.setInt(3, 0);
                ps.setInt(4, 0);
            }
            else
            {
                ps.setInt(1, this.mfc.getFamilyId());
                ps.setInt(2, this.mfc.getSeniorId());
                ps.setInt(3, this.mfc.getJunior1());
                ps.setInt(4, this.mfc.getJunior2());
            }
            ps.setInt(5, this.id);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException se)
        {
            System.out.println("SQLException: " + se.getLocalizedMessage());
        }
    }

    public void modifyCSPoints(int type, int quantity)
    {
        modifyCSPoints(type, quantity, false);
    }

    public void modifyCSPoints(int type, int quantity, boolean show)
    {
        switch (type)
        {
            case 1:
                if (this.acash + quantity < 0)
                {
                    if (show)
                    {
                        dropMessage(-1, "You have gained the max cash. No cash will be awarded.");
                    }
                    ban(getName() + " 点卷数量为负", false, true, false);
                    return;
                }
                this.acash += quantity;
                break;
            case 2:
                if (this.maplepoints + quantity < 0)
                {
                    if (show)
                    {
                        dropMessage(-1, "You have gained the max maple points. No cash will be awarded.");
                    }
                    ban(getName() + " 抵用卷数量为负", false, true, false);
                    return;
                }
                this.maplepoints += quantity;
                break;
        }


        if ((show) && (quantity != 0))
        {
            dropMessage(-1, "您" + (quantity > 0 ? "获得了 " : "消耗了 ") + Math.abs(quantity) + (type == 1 ? " 点券." : " 抵用券."));
        }

        this.client.getSession().write(MaplePacketCreator.showCharCash(this));
    }

    public int getCSPoints(int type)
    {
        switch (type)
        {
            case 1:
                return this.acash;
            case 2:
                return this.maplepoints;
            case -1:
                return this.acash + this.maplepoints;
        }
        return 0;
    }

    public boolean hasEquipped(int itemid)
    {
        return this.inventory[MapleInventoryType.EQUIPPED.ordinal()].countById(itemid) >= 1;
    }

    public boolean haveItem(int itemid, int quantity)
    {
        return haveItem(itemid, quantity, true, true);
    }

    public boolean haveItem(int itemid, int quantity, boolean checkEquipped, boolean greaterOrEquals)
    {
        MapleInventoryType type = ItemConstants.getInventoryType(itemid);
        int possesed = this.inventory[type.ordinal()].countById(itemid);
        if ((checkEquipped) && (type == MapleInventoryType.EQUIP))
        {
            possesed += this.inventory[MapleInventoryType.EQUIPPED.ordinal()].countById(itemid);
        }
        if (greaterOrEquals)
        {
            return possesed >= quantity;
        }
        return possesed == quantity;
    }

    public boolean haveItem(int itemid)
    {
        return haveItem(itemid, 1, true, true);
    }

    public int getItemQuantity(int itemid)
    {
        MapleInventoryType type = ItemConstants.getInventoryType(itemid);
        return getInventory(type).countById(itemid);
    }

    public int getItemQuantity(int itemid, boolean checkEquipped)
    {
        int possesed = this.inventory[ItemConstants.getInventoryType(itemid).ordinal()].countById(itemid);
        if (checkEquipped)
        {
            possesed += this.inventory[MapleInventoryType.EQUIPPED.ordinal()].countById(itemid);
        }
        return possesed;
    }

    public int getEquipId(byte slot)
    {
        MapleInventory equip = getInventory(MapleInventoryType.EQUIP);
        return equip.getItem(slot).getItemId();
    }

    public int getUseId(byte slot)
    {
        MapleInventory use = getInventory(MapleInventoryType.USE);
        return use.getItem(slot).getItemId();
    }

    public int getSetupId(byte slot)
    {
        MapleInventory setup = getInventory(MapleInventoryType.SETUP);
        return setup.getItem(slot).getItemId();
    }

    public int getCashId(byte slot)
    {
        MapleInventory cash = getInventory(MapleInventoryType.CASH);
        return cash.getItem(slot).getItemId();
    }

    public int getEtcId(byte slot)
    {
        MapleInventory etc = getInventory(MapleInventoryType.ETC);
        return etc.getItem(slot).getItemId();
    }

    public byte getBuddyCapacity()
    {
        return this.buddylist.getCapacity();
    }

    public void setBuddyCapacity(byte capacity)
    {
        this.buddylist.setCapacity(capacity);
        this.client.getSession().write(tools.packet.BuddyListPacket.updateBuddyCapacity(capacity));
    }

    public List<MapleSummon> getSummonsReadLock()
    {
        this.summonsLock.readLock().lock();
        return this.summons;
    }

    public int getSummonsSize()
    {
        return this.summons.size();
    }

    public void unlockSummonsReadLock()
    {
        this.summonsLock.readLock().unlock();
    }

    public void addSummon(MapleSummon s)
    {
        summons.add(s);
    }

    public void removeSummon(MapleSummon s)
    {
        summons.remove(s);
    }

    public boolean skillisCooling(int skillId)
    {
        return this.coolDowns.containsKey(skillId);
    }

    public void giveCoolDowns(List<MapleCoolDownValueHolder> cooldowns)
    {
        if (cooldowns != null)
        {
            for (MapleCoolDownValueHolder cooldown : cooldowns)
            {
                this.coolDowns.put(cooldown.skillId, cooldown);
            }
        }
        else
        {
            try
            {
                Connection con = DatabaseConnection.getConnection();
                PreparedStatement ps = con.prepareStatement("SELECT SkillID,StartTime,length FROM skills_cooldowns WHERE charid = ?");
                ps.setInt(1, getId());
                ResultSet rs = ps.executeQuery();
                while (rs.next())
                {
                    if (rs.getLong("length") + rs.getLong("StartTime") - System.currentTimeMillis() > 0L)
                    {

                        giveCoolDowns(rs.getInt("SkillID"), rs.getLong("StartTime"), rs.getLong("length"));
                    }
                }
                ps.close();
                rs.close();
                deleteWhereCharacterId(con, "DELETE FROM skills_cooldowns WHERE charid = ?");
            }
            catch (SQLException e)
            {
                System.err.println("Error while retriving cooldown from SQL storage");
            }
        }
    }

    public void giveCoolDowns(int skillid, long starttime, long length)
    {
        addCooldown(skillid, starttime, length);
    }

    private void deleteWhereCharacterId(Connection con, String sql) throws SQLException
    {
        deleteWhereCharacterId(con, sql, this.id);
    }

    public void addCooldown(int skillId, long startTime, long length)
    {
        this.coolDowns.put(skillId, new MapleCoolDownValueHolder(skillId, startTime, length));
    }

    public static void deleteWhereCharacterId(Connection con, String sql, int id) throws SQLException
    {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, id);
        ps.executeUpdate();
        ps.close();
    }

    public int getCooldownSize()
    {
        return this.coolDowns.size();
    }

    public List<MapleDiseaseValueHolder> getAllDiseases()
    {
        return new ArrayList(this.diseases.values());
    }

    public void giveDebuff(MapleDisease disease, server.life.MobSkill skill)
    {
        giveDebuff(disease, skill.getX(), skill.getDuration(), skill.getSkillId(), skill.getSkillLevel());
    }

    public void giveDebuff(MapleDisease disease, int x, long duration, int skillid, int level)
    {
        if ((this.map != null) && (!hasDisease(disease)))
        {
            if ((disease != MapleDisease.诱惑) && (disease != MapleDisease.眩晕) && (disease != MapleDisease.FLAG) && (getBuffedValue(MapleBuffStat.进阶祝福) != null))
            {
                return;
            }

            int mC = getBuffSource(MapleBuffStat.金属机甲);
            if ((mC > 0) && (mC != 35121005))
            {
                return;
            }
            MapleStatEffect effect = getStatForBuff(MapleBuffStat.抵抗之魔法盾);
            if (effect != null)
            {
                int count = getBuffedValue(MapleBuffStat.抵抗之魔法盾);
                if (count > 0)
                {
                    int newcount = count - 1;
                    if (newcount > 0)
                    {
                        setBuffedValue(MapleBuffStat.抵抗之魔法盾, newcount);
                        List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.抵抗之魔法盾, newcount));
                        int buffduration = 2100000000;
                        this.client.getSession().write(BuffPacket.giveBuff(effect.getSourceId(), buffduration, stat, effect, this));
                    }
                    else
                    {
                        cancelEffectFromBuffStat(MapleBuffStat.抵抗之魔法盾);
                        if (effect.getSourceId() == 27111004)
                        {
                            this.client.getSession().write(MaplePacketCreator.skillCooldown(effect.getSourceId(), effect.getCooldown(this)));
                            addCooldown(effect.getSourceId(), System.currentTimeMillis(), effect.getCooldown(this) * 1000);
                        }
                    }
                    return;
                }
                cancelEffectFromBuffStat(MapleBuffStat.抵抗之魔法盾);
                if (effect.getSourceId() == 27111004)
                {
                    this.client.getSession().write(MaplePacketCreator.skillCooldown(effect.getSourceId(), effect.getCooldown(this)));
                    addCooldown(effect.getSourceId(), System.currentTimeMillis(), effect.getCooldown(this) * 1000);
                }
            }

            if ((this.stats.ASR > 0) && (Randomizer.nextInt(100) < this.stats.ASR))
            {
                return;
            }
            this.diseases.put(disease, new MapleDiseaseValueHolder(disease, System.currentTimeMillis(), duration - this.stats.decreaseDebuff));
            this.client.getSession().write(BuffPacket.giveDebuff(disease, x, skillid, level, (int) duration));
            this.map.broadcastMessage(this, BuffPacket.giveForeignDebuff(this.id, disease, skillid, level, x), false);
            if ((x > 0) && (disease == MapleDisease.中毒))
            {
                addHP((int) -(x * ((duration - this.stats.decreaseDebuff) / 1000L)));
            }
        }
    }

    public void giveSilentDebuff(List<MapleDiseaseValueHolder> ld)
    {
        if (ld != null)
        {
            for (MapleDiseaseValueHolder disease : ld)
            {
                this.diseases.put(disease.disease, disease);
            }
        }
    }

    public void dispelDebuffs()
    {
        List<MapleDisease> diseasess = new ArrayList(this.diseases.keySet());
        for (MapleDisease d : diseasess)
        {
            dispelDebuff(d);
        }
    }

    public void dispelDebuff(MapleDisease debuff)
    {
        if (hasDisease(debuff))
        {
            this.client.getSession().write(BuffPacket.cancelDebuff(debuff));
            this.map.broadcastMessage(this, BuffPacket.cancelForeignDebuff(this.id, debuff), false);
            this.diseases.remove(debuff);
        }
    }

    public boolean hasDisease(MapleDisease dis)
    {
        return this.diseases.containsKey(dis);
    }

    public void cancelAllDebuffs()
    {
        this.diseases.clear();
    }

    public int getDiseaseSize()
    {
        return this.diseases.size();
    }

    public void sendNote(String to, String msg)
    {
        sendNote(to, msg, 0);
    }

    public void sendNote(String to, String msg, int fame)
    {
        MapleCharacterUtil.sendNote(to, getName(), msg, fame);
    }

    public void showNote()
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM notes WHERE `to`=?", 1005, 1008);
            ps.setString(1, getName());
            ResultSet rs = ps.executeQuery();
            rs.last();
            int count = rs.getRow();
            rs.first();
            this.client.getSession().write(tools.packet.MTSCSPacket.showNotes(rs, count));
            rs.close();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("Unable to show note" + e);
        }
    }

    public void deleteNote(int id, int fame)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT gift FROM notes WHERE `id`=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if ((rs.next()) && (rs.getInt("gift") == fame) && (fame > 0))
            {
                addFame(fame);
                updateSingleStat(MapleStat.人气, getFame());
                this.client.getSession().write(MaplePacketCreator.getShowFameGain(fame));
            }

            rs.close();
            ps.close();
            ps = con.prepareStatement("DELETE FROM notes WHERE `id`=?");
            ps.setInt(1, id);
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("Unable to delete note" + e);
        }
    }

    public void addFame(int famechange)
    {
        this.fame += famechange;
        getTrait(MapleTraitType.charm).addLocalExp(famechange);
        if (this.fame >= 50)
        {
            finishAchievement(7);
        }
    }

    public int getFame()
    {
        return this.fame;
    }

    public void setFame(int fame)
    {
        this.fame = fame;
    }

    public MapleTrait getTrait(MapleTraitType t)
    {
        return this.traits.get(t);
    }

    public void finishAchievement(int id)
    {
        if ((!achievementFinished(id)) && (isAlive()))
        {
            server.achievement.MapleAchievements.getInstance().getById(id).finishAchievement(this);
        }
    }

    public boolean achievementFinished(int achievementid)
    {
        return this.finishedAchievements.contains(achievementid);
    }

    public boolean isAlive()
    {
        return this.stats.getHp() > 0;
    }

    public int getMulungEnergy()
    {
        return this.mulung_energy;
    }

    public void mulung_EnergyModify(boolean inc)
    {
        if (inc)
        {
            if (this.mulung_energy + 100 > 10000)
            {
                this.mulung_energy = 10000;
            }
            else
            {
                this.mulung_energy = ((short) (this.mulung_energy + 100));
            }
        }
        else
        {
            this.mulung_energy = 0;
        }
        this.client.getSession().write(MaplePacketCreator.MulungEnergy(this.mulung_energy));
    }

    public void writeMulungEnergy()
    {
        this.client.getSession().write(MaplePacketCreator.MulungEnergy(this.mulung_energy));
    }

    public void writeEnergy(String type, String inc)
    {
        this.client.getSession().write(MaplePacketCreator.sendPyramidEnergy(type, inc));
    }

    public void writeStatus(String type, String inc)
    {
        this.client.getSession().write(MaplePacketCreator.sendGhostStatus(type, inc));
    }

    public void writePoint(String type, String inc)
    {
        this.client.getSession().write(MaplePacketCreator.sendGhostPoint(type, inc));
    }

    public int getAranCombo()
    {
        return this.aranCombo;
    }

    public void gainAranCombo(int count, boolean show)
    {
        int oldCombo = this.aranCombo;
        oldCombo += count;
        if (oldCombo < 0)
        {
            oldCombo = 0;
        }
        this.aranCombo = Math.min(30000, oldCombo);
        if (show)
        {
            this.client.getSession().write(MaplePacketCreator.ShowAranCombo(this.aranCombo));
        }
    }

    public long getLastComboTime()
    {
        return this.lastComboTime;
    }

    public void setLastComboTime(long time)
    {
        this.lastComboTime = time;
    }

    public long getKeyDownSkill_Time()
    {
        return this.keydown_skill;
    }

    public void setKeyDownSkill_Time(long keydown_skill)
    {
        this.keydown_skill = keydown_skill;
    }

    public void check生命潮汐()
    {
        if ((this.job != 2711) && (this.job != 2712))
        {
            return;
        }
        Skill skill = SkillFactory.getSkill(27110007);
        int skilllevel = getTotalSkillLevel(skill);
        MapleStatEffect effect = getStatForBuff(MapleBuffStat.生命潮汐);
        if ((skilllevel >= 1) && (this.map != null) && ((effect == null) || (effect.getLevel() < skilllevel)))
        {
            skill.getEffect(skilllevel).applyTo(this);
        }
    }

    public void check血之契约()
    {
        if (!GameConstants.is恶魔复仇者(this.job))
        {
            return;
        }
        Skill skill = SkillFactory.getSkill(30010242);
        int skilllevel = getTotalSkillLevel(skill);
        if ((skilllevel >= 1) && (this.map != null))
        {
            this.client.getSession().write(BuffPacket.give血之契约(this));
        }
    }

    public String getChalkboard()
    {
        return this.chalktext;
    }

    public void setChalkboard(String text)
    {
        this.chalktext = text;
        if (this.map != null)
        {
            this.map.broadcastMessage(tools.packet.MTSCSPacket.useChalkboard(getId(), text));
        }
    }

    public int[] getWishlist()
    {
        return this.wishlist;
    }

    public void setWishlist(int[] wl)
    {
        this.wishlist = wl;
        this.changed_wishlist = true;
    }

    public void clearWishlist()
    {
        for (int i = 0; i < 12; i++)
        {
            this.wishlist[i] = 0;
        }
        this.changed_wishlist = true;
    }

    public int getWishlistSize()
    {
        int ret = 0;
        for (int i = 0; i < 12; i++)
        {
            if (this.wishlist[i] > 0)
            {
                ret++;
            }
        }
        return ret;
    }

    public int[] getRocks()
    {
        return this.rocks;
    }

    public void deleteFromRocks(int map)
    {
        for (int i = 0; i < 10; i++)
        {
            if (this.rocks[i] == map)
            {
                this.rocks[i] = 999999999;
                this.changed_trocklocations = true;
                break;
            }
        }
    }

    public void addRockMap()
    {
        if (getRockSize() >= 10)
        {
            return;
        }
        this.rocks[getRockSize()] = getMapId();
        this.changed_trocklocations = true;
    }

    public int getRockSize()
    {
        int ret = 0;
        for (int i = 0; i < 10; i++)
        {
            if (this.rocks[i] != 999999999)
            {
                ret++;
            }
        }
        return ret;
    }

    public boolean isRockMap(int id)
    {
        for (int i = 0; i < 10; i++)
        {
            if (this.rocks[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    public int[] getRegRocks()
    {
        return this.regrocks;
    }

    public void deleteFromRegRocks(int map)
    {
        for (int i = 0; i < 5; i++)
        {
            if (this.regrocks[i] == map)
            {
                this.regrocks[i] = 999999999;
                this.changed_trocklocations = true;
                break;
            }
        }
    }

    public void addRegRockMap()
    {
        if (getRegRockSize() >= 5)
        {
            return;
        }
        this.regrocks[getRegRockSize()] = getMapId();
        this.changed_trocklocations = true;
    }

    public int getRegRockSize()
    {
        int ret = 0;
        for (int i = 0; i < 5; i++)
        {
            if (this.regrocks[i] != 999999999)
            {
                ret++;
            }
        }
        return ret;
    }

    public boolean isRegRockMap(int id)
    {
        for (int i = 0; i < 5; i++)
        {
            if (this.regrocks[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    public int[] getHyperRocks()
    {
        return this.hyperrocks;
    }

    public void deleteFromHyperRocks(int map)
    {
        for (int i = 0; i < 13; i++)
        {
            if (this.hyperrocks[i] == map)
            {
                this.hyperrocks[i] = 999999999;
                this.changed_trocklocations = true;
                break;
            }
        }
    }

    public void addHyperRockMap()
    {
        if (getRegRockSize() >= 13)
        {
            return;
        }
        this.hyperrocks[getHyperRockSize()] = getMapId();
        this.changed_trocklocations = true;
    }

    public int getHyperRockSize()
    {
        int ret = 0;
        for (int i = 0; i < 13; i++)
        {
            if (this.hyperrocks[i] != 999999999)
            {
                ret++;
            }
        }
        return ret;
    }

    public boolean isHyperRockMap(int id)
    {
        for (int i = 0; i < 13; i++)
        {
            if (this.hyperrocks[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    public client.inventory.MaplePotionPot getPotionPot()
    {
        return this.potionPot;
    }

    public void setPotionPot(client.inventory.MaplePotionPot p)
    {
        this.potionPot = p;
    }

    public MapleCoreAura getCoreAura()
    {
        return this.coreAura;
    }

    public List<server.movement.LifeMovementFragment> getLastRes()
    {
        return this.lastres;
    }

    public void setLastRes(List<server.movement.LifeMovementFragment> lastres)
    {
        this.lastres = lastres;
    }

    public server.MapleCarnivalParty getCarnivalParty()
    {
        return this.carnivalParty;
    }

    public void setCarnivalParty(server.MapleCarnivalParty party)
    {
        this.carnivalParty = party;
    }

    public void addCP(int ammount)
    {
        this.totalCP = ((short) (this.totalCP + ammount));
        this.availableCP = ((short) (this.availableCP + ammount));
    }

    public void useCP(int ammount)
    {
        this.availableCP = ((short) (this.availableCP - ammount));
    }

    public int getAvailableCP()
    {
        return this.availableCP;
    }

    public int getTotalCP()
    {
        return this.totalCP;
    }

    public void resetCP()
    {
        this.totalCP = 0;
        this.availableCP = 0;
    }

    public void addCarnivalRequest(MapleCarnivalChallenge request)
    {
        this.pendingCarnivalRequests.add(request);
    }

    public MapleCarnivalChallenge getNextCarnivalRequest()
    {
        return this.pendingCarnivalRequests.pollLast();
    }

    public void clearCarnivalRequests()
    {
        this.pendingCarnivalRequests = new LinkedList<>();
    }

    public void startMonsterCarnival(int enemyavailable, int enemytotal)
    {
        this.client.getSession().write(tools.packet.MonsterCarnivalPacket.startMonsterCarnival(this, enemyavailable, enemytotal));
    }

    public void CPUpdate(boolean party, int available, int total, int team)
    {
        this.client.getSession().write(tools.packet.MonsterCarnivalPacket.CPUpdate(party, available, total, team));
    }

    public void playerDiedCPQ(String name, int lostCP, int team)
    {
        this.client.getSession().write(tools.packet.MonsterCarnivalPacket.playerDiedMessage(name, lostCP, team));
    }

    public void setAchievementFinished(int id)
    {
        if (!this.finishedAchievements.contains(id))
        {
            this.finishedAchievements.add(id);
            this.changed_achievements = true;
        }
    }

    public List<Integer> getFinishedAchievements()
    {
        return this.finishedAchievements;
    }

    public boolean getCanTalk()
    {
        return this.canTalk;
    }

    public void canTalk(boolean talk)
    {
        this.canTalk = talk;
    }

    public double getEXPMod()
    {
        return hasEXPCard();
    }

    public double hasEXPCard()
    {
        int[] expCards = {5210000, 5210001, 5210002, 5210003, 5210004, 5210005, 5210006, 5211047, 5211060, 5211000, 5211001, 5211002, 5211063, 5211064, 5211065, 5211066, 5211069, 5211070, 5211084,
                5211085, 5211108, 5211109};


        MapleInventory iv = getInventory(MapleInventoryType.CASH);
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        double canuse = 1.0D;
        int[] arrayOfInt1 = expCards;
        int i = arrayOfInt1.length;
        for (int value : arrayOfInt1)
        {
            Integer ids = value;
            if ((iv.countById(ids) > 0) && (ii.isExpOrDropCardTime(ids)))
            {
                switch (ids)
                {
                    case 5210000:
                    case 5210001:
                    case 5210002:
                    case 5210003:
                    case 5210004:
                    case 5210005:
                    case 5210006:
                    case 5211000:
                    case 5211001:
                    case 5211002:
                    case 5211047:
                    case 5211084:
                    case 5211085:
                    case 5211108:
                    case 5211109:
                        canuse = 2.0D;
                        break;
                    case 5211060:
                        canuse = 3.0D;
                        break;
                    case 5211063:
                    case 5211064:
                    case 5211065:
                    case 5211066:
                    case 5211069:
                    case 5211070:
                        canuse = 1.5D;
                }

            }
        }

        return canuse;
    }

    public int getDropMod()
    {
        return hasDropCard();
    }

    public int hasDropCard()
    {
        int[] dropCards = {5360000, 5360014, 5360015, 5360016};
        MapleInventory iv = getInventory(MapleInventoryType.CASH);
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int[] arrayOfInt1 = dropCards;
        int i = arrayOfInt1.length;
        for (int value : arrayOfInt1)
        {
            Integer id3 = value;
            if ((iv.countById(id3) > 0) && (ii.isExpOrDropCardTime(id3)))
            {
                return 2;
            }
        }

        return 1;
    }

    public int getCashMod()
    {
        return this.stats.cashMod;
    }

    public int getPoints()
    {
        return this.points;
    }

    public void setPoints(int p)
    {
        this.points = p;
        if (this.points >= 1)
        {
            finishAchievement(1);
        }
    }

    public int getVPoints()
    {
        return this.vpoints;
    }

    public void setVPoints(int p)
    {
        this.vpoints = p;
    }

    public server.cashshop.CashShop getCashInventory()
    {
        return this.cs;
    }

    public void removeItem(int id, int quantity)
    {
        MapleInventoryManipulator.removeById(this.client, ItemConstants.getInventoryType(id), id, quantity, true, false);
        this.client.getSession().write(MaplePacketCreator.getShowItemGain(id, (short) -quantity, true));
    }

    public void removeAll(int id)
    {
        removeAll(id, true, false);
    }

    public void removeAll(int itemId, boolean show, boolean checkEquipped)
    {
        MapleInventoryType type = ItemConstants.getInventoryType(itemId);
        int possessed = getInventory(type).countById(itemId);
        if (possessed > 0)
        {
            MapleInventoryManipulator.removeById(getClient(), type, itemId, possessed, true, false);
            if (show)
            {
                getClient().getSession().write(MaplePacketCreator.getShowItemGain(itemId, (short) -possessed, true));
            }
        }
        if ((checkEquipped) && (type == MapleInventoryType.EQUIP))
        {
            type = MapleInventoryType.EQUIPPED;
            possessed = getInventory(type).countById(itemId);
            if (possessed > 0)
            {
                MapleInventoryManipulator.removeById(getClient(), type, itemId, possessed, true, false);
                if (show)
                {
                    getClient().getSession().write(MaplePacketCreator.getShowItemGain(itemId, (short) -possessed, true));
                }
                equipChanged();
            }
        }
    }

    public void removeItem(int itemId)
    {
        removeItem(itemId, false);
    }

    public void removeItem(int itemId, boolean show)
    {
        MapleInventoryType type = ItemConstants.getInventoryType(itemId);
        if (type == MapleInventoryType.EQUIP)
        {
            type = MapleInventoryType.EQUIPPED;
            int possessed = getInventory(type).countById(itemId);
            if (possessed > 0)
            {
                MapleInventoryManipulator.removeById(getClient(), type, itemId, possessed, true, false);
                if (show)
                {
                    getClient().getSession().write(MaplePacketCreator.getShowItemGain(itemId, (short) -possessed, true));
                }
                equipChanged();
            }
        }
    }

    public MapleRing getMarriageRing()
    {
        MapleInventory iv = getInventory(MapleInventoryType.EQUIPPED);
        List<Item> equipped = iv.newList();
        Collections.sort(equipped);
        MapleRing mrings = null;
        for (Item ite : equipped)
        {
            Equip item = (Equip) ite;
            if (item.getRing() != null)
            {
                MapleRing ring = item.getRing();
                ring.setEquipped(true);
                if ((mrings == null) && (ItemConstants.is结婚戒指(item.getItemId())))
                {
                    mrings = ring;
                }
            }
        }
        if (mrings == null)
        {
            iv = getInventory(MapleInventoryType.EQUIP);
            for (Item ite : iv.list())
            {
                Equip item = (Equip) ite;
                if (item.getRing() != null)
                {
                    MapleRing ring = item.getRing();
                    ring.setEquipped(false);
                    if ((mrings == null) && (ItemConstants.is结婚戒指(item.getItemId())))
                    {
                        mrings = ring;
                    }
                }
            }
        }
        return mrings;
    }

    public Triple<List<MapleRing>, List<MapleRing>, List<MapleRing>> getRings(boolean equip)
    {
        MapleInventory iv = getInventory(MapleInventoryType.EQUIPPED);
        List<Item> equipped = iv.newList();
        Collections.sort(equipped);
        List<MapleRing> crings = new ArrayList<>();
        List<MapleRing> frings = new ArrayList<>();
        List<MapleRing> mrings = new ArrayList<>();

        for (Item ite : equipped)
        {
            Equip item = (Equip) ite;
            if ((item.getRing() != null) && (ItemConstants.isEffectRing(item.getItemId())))
            {
                MapleRing ring = item.getRing();
                ring.setEquipped(true);
                if (equip)
                {
                    if (ItemConstants.is恋人戒指(item.getItemId()))
                    {
                        crings.add(ring);
                    }
                    else if (ItemConstants.is好友戒指(item.getItemId()))
                    {
                        frings.add(ring);
                    }
                    else if (ItemConstants.is结婚戒指(item.getItemId()))
                    {
                        mrings.add(ring);
                    }
                }
                else if ((crings.isEmpty()) && (ItemConstants.is恋人戒指(item.getItemId())))
                {
                    crings.add(ring);
                }
                else if ((frings.isEmpty()) && (ItemConstants.is好友戒指(item.getItemId())))
                {
                    frings.add(ring);
                }
                else if ((mrings.isEmpty()) && (ItemConstants.is结婚戒指(item.getItemId())))
                {
                    mrings.add(ring);
                }
            }
        }

        if (equip)
        {
            iv = getInventory(MapleInventoryType.EQUIP);
            for (Item ite : iv.list())
            {
                Equip item = (Equip) ite;
                if ((item.getRing() != null) && (ItemConstants.isEffectRing(item.getItemId())))
                {
                    MapleRing ring = item.getRing();
                    ring.setEquipped(false);
                    if (ItemConstants.is恋人戒指(item.getItemId()))
                    {
                        crings.add(ring);
                    }
                    else if (ItemConstants.is好友戒指(item.getItemId()))
                    {
                        frings.add(ring);
                    }
                    else if (ItemConstants.is结婚戒指(item.getItemId()))
                    {
                        mrings.add(ring);
                    }
                }
            }
        }
        Collections.sort(frings, new client.inventory.MapleRing.RingComparator());
        Collections.sort(crings, new client.inventory.MapleRing.RingComparator());
        Collections.sort(mrings, new client.inventory.MapleRing.RingComparator());
        return new Triple(crings, frings, mrings);
    }

    public void startFairySchedule(boolean exp)
    {
        startFairySchedule(exp, false);
    }

    public void startFairySchedule(boolean exp, boolean equipped)
    {
        cancelFairySchedule((exp) || (this.stats.equippedFairy == 0));
        if (this.fairyExp <= 0)
        {
            this.fairyExp = ((byte) this.stats.equippedFairy);
        }
        if ((equipped) && (this.fairyExp < this.stats.equippedFairy * 3) && (this.stats.equippedFairy > 0))
        {
            dropMessage(5, "您装备了精灵吊坠在1小时后经验获取将增加到 " + (this.fairyExp + this.stats.equippedFairy) + " %.");
        }
        this.lastFairyTime = System.currentTimeMillis();
    }

    public void cancelFairySchedule(boolean exp)
    {
        this.lastFairyTime = 0L;
        if (exp)
        {
            this.fairyExp = 0;
        }
    }

    public boolean canFairy(long now)
    {
        return (this.lastFairyTime > 0L) && (this.lastFairyTime + 3600000L < now);
    }

    public boolean canHP(long now)
    {
        if (this.lastHPTime + 5000L < now)
        {
            this.lastHPTime = now;
            return true;
        }
        return false;
    }

    public boolean canMP(long now)
    {
        if (this.lastMPTime + 5000L < now)
        {
            this.lastMPTime = now;
            return true;
        }
        return false;
    }

    public boolean canHPRecover(long now)
    {
        if ((this.stats.hpRecoverTime > 0) && (this.lastHPTime + this.stats.hpRecoverTime < now))
        {
            this.lastHPTime = now;
            return true;
        }
        return false;
    }

    public boolean canMPRecover(long now)
    {
        if ((this.stats.mpRecoverTime > 0) && (this.lastMPTime + this.stats.mpRecoverTime < now))
        {
            this.lastMPTime = now;
            return true;
        }
        return false;
    }

    public void doFairy()
    {
        if ((this.fairyExp < this.stats.equippedFairy * 3) && (this.stats.equippedFairy > 0))
        {
            this.fairyExp = ((byte) (this.fairyExp + this.stats.equippedFairy));
            dropMessage(5, "精灵吊坠经验获取增加到 " + this.fairyExp + " %.");
        }
        if (getGuildId() > 0)
        {
            handling.world.WorldGuildService.getInstance().gainGP(getGuildId(), 20, this.id);
            this.client.getSession().write(UIPacket.getGPContribution(20));
        }
        this.traits.get(MapleTraitType.will).addExp(5, this);
        startFairySchedule(false, true);
    }

    public byte getFairyExp()
    {
        return this.fairyExp;
    }

    public int getTeam()
    {
        return this.coconutteam;
    }

    public void setTeam(int v)
    {
        this.coconutteam = v;
    }

    public void clearLinkMid()
    {
        this.linkMobs.clear();
        cancelEffectFromBuffStat(MapleBuffStat.导航辅助);
        cancelEffectFromBuffStat(MapleBuffStat.神秘瞄准术);
    }

    public int getFirstLinkMid()
    {
        Iterator localIterator = this.linkMobs.keySet().iterator();
        if (localIterator.hasNext())
        {
            Integer lm = (Integer) localIterator.next();
            return lm;
        }
        return 0;
    }

    public Map<Integer, Integer> getAllLinkMid()
    {
        return this.linkMobs;
    }

    public void setLinkMid(int lm, int x)
    {
        this.linkMobs.put(lm, x);
    }

    public int getDamageIncrease(int lm)
    {
        if (this.linkMobs.containsKey(lm))
        {
            return this.linkMobs.get(lm);
        }
        return 0;
    }

    public server.maps.MapleExtractor getExtractor()
    {
        return this.extractor;
    }

    public boolean allowedToTarget(MapleCharacter other)
    {
        return (other != null) && ((!other.isHidden()) || (getGMLevel() >= other.getGMLevel()));
    }

    public void setExtractor(server.maps.MapleExtractor me)
    {
        removeExtractor();
        this.extractor = me;
    }

    public void removeExtractor()
    {
        if (this.extractor != null)
        {
            this.map.broadcastMessage(MaplePacketCreator.removeExtractor(this.id));
            this.map.removeMapObject(this.extractor);
            this.extractor = null;
        }
    }

    public void spawnSavedPets()
    {
        for (byte b : this.petStore)
        {
            if (b > -1)
            {
                spawnPet(b, false, false);
            }
        }
        this.petStore = new byte[]{-1, -1, -1};
    }

    public byte[] getPetStores()
    {
        return this.petStore;
    }

    public byte getSubcategory()
    {
        if ((this.job >= 430) && (this.job <= 434)) return 1;
        if (GameConstants.is火炮手(this.job)) return 2;
        if (GameConstants.is龙的传人(this.job)) return 10;
        if (this.job == 0)
        {
            return this.subcategory;
        }
        return 0;
    }

    public void setSubcategory(int z)
    {
        this.subcategory = ((byte) z);
    }

    public int itemQuantity(int itemid)
    {
        return getInventory(ItemConstants.getInventoryType(itemid)).countById(itemid);
    }

    public RockPaperScissors getRPS()
    {
        return this.rps;
    }

    public void setRPS(RockPaperScissors rps)
    {
        this.rps = rps;
    }

    public long getNextConsume()
    {
        return this.nextConsume;
    }

    public void setNextConsume(long nc)
    {
        this.nextConsume = nc;
    }

    public int getRank()
    {
        return this.rank;
    }

    public int getRankMove()
    {
        return this.rankMove;
    }

    public int getJobRank()
    {
        return this.jobRank;
    }

    public int getJobRankMove()
    {
        return this.jobRankMove;
    }

    public void changeChannel(int channel)
    {
        ChannelServer toch = ChannelServer.getInstance(channel);
        if ((channel == this.client.getChannel()) || (toch == null) || (toch.isShutdown()))
        {
            this.client.getSession().write(MaplePacketCreator.serverBlocked(1));
            return;
        }
        changeRemoval();
        ChannelServer ch = ChannelServer.getInstance(this.client.getChannel());
        if (getMessenger() != null)
        {
            handling.world.WorldMessengerService.getInstance().silentLeaveMessenger(getMessenger().getId(), new MapleMessengerCharacter(this));
        }
        handling.world.PlayerBuffStorage.addBuffsToStorage(getId(), getAllBuffs());
        handling.world.PlayerBuffStorage.addCooldownsToStorage(getId(), getCooldowns());
        handling.world.PlayerBuffStorage.addDiseaseToStorage(getId(), getAllDiseases());
        handling.world.World.ChannelChange_Data(new CharacterTransfer(this), getId(), channel);
        ch.removePlayer(this);
        this.client.updateLoginState(3, this.client.getSessionIPAddress());
        String s = this.client.getSessionIPAddress();
        handling.login.LoginServer.addIPAuth(s.substring(s.indexOf('/') + 1));
        this.client.getSession().write(MaplePacketCreator.getChannelChange(this.client, Integer.parseInt(toch.getIP().split(":")[1])));
        saveToDB(false, false);
        getMap().removePlayer(this);
        this.client.setPlayer(null);
        this.client.setReceiving(false);
    }

    public void expandInventory(byte type, int amount)
    {
        MapleInventory inv = getInventory(MapleInventoryType.getByType(type));
        inv.addSlot((byte) amount);
        this.client.getSession().write(tools.packet.InventoryPacket.updateInventorySlotLimit(type, inv.getSlotLimit()));
    }

    public int getFollowId()
    {
        return this.followid;
    }

    public void setFollowId(int fi)
    {
        this.followid = fi;
        if (fi == 0)
        {
            this.followinitiator = false;
            this.followon = false;
        }
    }

    public boolean isFollowOn()
    {
        return this.followon;
    }

    public void setFollowOn(boolean fi)
    {
        this.followon = fi;
    }

    public boolean isFollowInitiator()
    {
        return this.followinitiator;
    }

    public void setFollowInitiator(boolean fi)
    {
        this.followinitiator = fi;
    }

    public void checkFollow()
    {
        if (this.followid <= 0)
        {
            return;
        }
        if (this.followon)
        {
            this.map.broadcastMessage(MaplePacketCreator.followEffect(this.id, 0, null));
            this.map.broadcastMessage(MaplePacketCreator.followEffect(this.followid, 0, null));
        }
        MapleCharacter tt = this.map.getCharacterById(this.followid);
        this.client.getSession().write(MaplePacketCreator.getFollowMessage("已停止跟随。"));
        if (tt != null)
        {
            tt.setFollowId(0);
            tt.getClient().getSession().write(MaplePacketCreator.getFollowMessage("已停止跟随。"));
        }
        setFollowId(0);
    }

    public int getMarriageId()
    {
        return this.marriageId;
    }

    public void setMarriageId(int mi)
    {
        this.marriageId = mi;
    }

    public int getMarriageItemId()
    {
        return this.marriageItemId;
    }

    public void setMarriageItemId(int mi)
    {
        this.marriageItemId = mi;
    }

    public boolean isStaff()
    {
        return this.gmLevel >= PlayerGMRank.INTERN.getLevel();
    }

    public boolean isDonator()
    {
        return this.gmLevel >= PlayerGMRank.DONATOR.getLevel();
    }

    public boolean startPartyQuest(int questid)
    {
        boolean ret = false;
        MapleQuest q = MapleQuest.getInstance(questid);
        if ((q == null) || (!q.isPartyQuest()))
        {
            return false;
        }
        if ((!this.quests.containsKey(q)) || (!this.questinfo.containsKey(questid)))
        {
            MapleQuestStatus status = getQuestNAdd(q);
            status.setStatus((byte) 1);
            updateQuest(status);
            switch (questid)
            {
                case 1300:
                case 1301:
                case 1302:
                    updateInfoQuest(questid, "min=0;sec=0;date=0000-00-00;have=0;rank=F;try=0;cmp=0;CR=0;VR=0;gvup=0;vic=0;lose=0;draw=0");
                    break;
                case 1303:
                    updateInfoQuest(questid, "min=0;sec=0;date=0000-00-00;have=0;have1=0;rank=F;try=0;cmp=0;CR=0;VR=0;vic=0;lose=0");
                    break;
                case 1204:
                    updateInfoQuest(questid, "min=0;sec=0;date=0000-00-00;have0=0;have1=0;have2=0;have3=0;rank=F;try=0;cmp=0;CR=0;VR=0");
                    break;
                case 1206:
                    updateInfoQuest(questid, "min=0;sec=0;date=0000-00-00;have0=0;have1=0;rank=F;try=0;cmp=0;CR=0;VR=0");
                    break;
                default:
                    updateInfoQuest(questid, "min=0;sec=0;date=0000-00-00;have=0;rank=F;try=0;cmp=0;CR=0;VR=0");
            }

            ret = true;
        }
        return ret;
    }

    public String getOneInfo(int questid, String key)
    {
        if ((!this.questinfo.containsKey(questid)) || (key == null) || (MapleQuest.getInstance(questid) == null) || (!MapleQuest.getInstance(questid).isPartyQuest()))
        {
            return null;
        }
        String[] split = this.questinfo.get(questid).split(";");
        for (String x : split)
        {
            String[] split2 = x.split("=");
            if ((split2.length == 2) && (split2[0].equals(key)))
            {
                return split2[1];
            }
        }
        return null;
    }

    public void updateOneInfo(int questid, String key, String value)
    {
        if ((!this.questinfo.containsKey(questid)) || (key == null) || (value == null) || (MapleQuest.getInstance(questid) == null) || (!MapleQuest.getInstance(questid).isPartyQuest()))
        {
            return;
        }
        String[] split = this.questinfo.get(questid).split(";");
        boolean changed = false;
        StringBuilder newQuest = new StringBuilder();
        for (String x : split)
        {
            String[] split2 = x.split("=");
            if (split2.length == 2)
            {

                if (split2[0].equals(key))
                {
                    newQuest.append(key).append("=").append(value);
                }
                else
                {
                    newQuest.append(x);
                }
                newQuest.append(";");
                changed = true;
            }
        }
        updateInfoQuest(questid, changed ? newQuest.toString().substring(0, newQuest.toString().length() - 1) : newQuest.toString());
    }

    public void recalcPartyQuestRank(int questid)
    {
        if ((MapleQuest.getInstance(questid) == null) || (!MapleQuest.getInstance(questid).isPartyQuest()))
        {
            return;
        }
        if (!startPartyQuest(questid))
        {
            String oldRank = getOneInfo(questid, "rank");
            if ((oldRank == null) || (oldRank.equals("S")))
            {
                return;
            }
            String newRank;
            if (oldRank.equals("A"))
            {
                newRank = "S";
            }
            else
            {
                if (oldRank.equals("B"))
                {
                    newRank = "A";
                }
                else
                {
                    if (oldRank.equals("C"))
                    {
                        newRank = "B";
                    }
                    else
                    {
                        if (oldRank.equals("D"))
                        {
                            newRank = "C";
                        }
                        else
                        {
                            if (oldRank.equals("F")) newRank = "D";
                            else return;
                        }
                    }
                }
            }
            List<Pair<String, Pair<String, Integer>>> questInfo = MapleQuest.getInstance(questid).getInfoByRank(newRank);
            if (questInfo == null)
            {
                return;
            }
            for (Pair<String, Pair<String, Integer>> q : questInfo)
            {
                int vall;
                boolean found = false;
                String val = getOneInfo(questid, q.right.left);
                if (val == null)
                {
                    return;
                }
                try
                {
                    vall = Integer.parseInt(val);
                }
                catch (NumberFormatException e)
                {
                    return;
                }
                if (q.left.equals("less"))
                {
                    found = vall < q.right.right;
                }
                else if (q.left.equals("more"))
                {
                    found = vall > q.right.right;
                }
                else if (q.left.equals("equal"))
                {
                    found = vall == q.right.right;
                }
                if (!found)
                {
                    return;
                }
            }
            updateOneInfo(questid, "rank", newRank);
        }
    }

    public void tryPartyQuest(int questid)
    {
        if ((MapleQuest.getInstance(questid) == null) || (!MapleQuest.getInstance(questid).isPartyQuest()))
        {
            return;
        }
        try
        {
            startPartyQuest(questid);
            this.pqStartTime = System.currentTimeMillis();
            updateOneInfo(questid, "try", String.valueOf(Integer.parseInt(getOneInfo(questid, "try")) + 1));
        }
        catch (Exception e)
        {
            System.out.println("tryPartyQuest error");
        }
    }

    public void endPartyQuest(int questid)
    {
        if ((MapleQuest.getInstance(questid) == null) || (!MapleQuest.getInstance(questid).isPartyQuest()))
        {
            return;
        }
        try
        {
            startPartyQuest(questid);
            if (this.pqStartTime > 0L)
            {
                long changeTime = System.currentTimeMillis() - this.pqStartTime;
                int mins = (int) (changeTime / 1000L / 60L);
                int secs = (int) (changeTime / 1000L % 60L);
                int mins2 = Integer.parseInt(getOneInfo(questid, "min"));
                if ((mins2 <= 0) || (mins < mins2))
                {
                    updateOneInfo(questid, "min", String.valueOf(mins));
                    updateOneInfo(questid, "sec", String.valueOf(secs));
                    updateOneInfo(questid, "date", FileoutputUtil.CurrentReadable_Date());
                }
                int newCmp = Integer.parseInt(getOneInfo(questid, "cmp")) + 1;
                updateOneInfo(questid, "cmp", String.valueOf(newCmp));
                updateOneInfo(questid, "CR", String.valueOf((int) Math.ceil(newCmp * 100.0D / Integer.parseInt(getOneInfo(questid, "try")))));
                recalcPartyQuestRank(questid);
                this.pqStartTime = 0L;
            }
        }
        catch (Exception e)
        {
            System.out.println("endPartyQuest error");
        }
    }

    public void havePartyQuest(int itemId)
    {
        int index = -1;
        int questid;
//        int questid;
//        int questid;
//        int questid;
//        int questid;
//        int questid;
//        int questid;
//        int questid;
//        int questid;
        switch (itemId)
        {
            case 1002798:
                questid = 1200;
                break;
            case 1072369:
                questid = 1201;
                break;
            case 1022073:
                questid = 1202;
                break;
            case 1082232:
                questid = 1203;
                break;
            case 1002571:
            case 1002572:
            case 1002573:
            case 1002574:
                questid = 1204;
                index = itemId - 1002571;
                break;
            case 1102226:
                questid = 1303;
                break;
            case 1102227:
                questid = 1303;
                index = 0;
                break;
            case 1122010:
                questid = 1205;
                break;
            case 1032060:
            case 1032061:
                questid = 1206;
                index = itemId - 1032060;
                break;
            case 3010018:
                questid = 1300;
                break;
            case 1122007:
                questid = 1301;
                break;
            case 1122058:
                questid = 1302;
                break;
            default:
                return;
        }
        if ((MapleQuest.getInstance(questid) == null) || (!MapleQuest.getInstance(questid).isPartyQuest()))
        {
            return;
        }
        startPartyQuest(questid);
        updateOneInfo(questid, "have" + (index == -1 ? "" : Integer.valueOf(index)), "1");
    }

    public void resetStatsByJob(boolean beginnerJob)
    {
        int baseJob = beginnerJob ? this.job % 1000 : this.job % 1000 / 100 * 100;
        boolean UA = getQuestNoAdd(MapleQuest.getInstance(111111)) != null;
        if (baseJob == 100)
        {
            resetStats(UA ? 4 : 35, 4, 4, 4);
        }
        else if (baseJob == 200)
        {
            resetStats(4, 4, UA ? 4 : 20, 4);
        }
        else if ((baseJob == 300) || (baseJob == 400))
        {
            resetStats(4, UA ? 4 : 25, 4, 4);
        }
        else if (baseJob == 500)
        {
            resetStats(4, UA ? 4 : 20, 4, 4);
        }
        else if (baseJob == 0)
        {
            resetStats(4, 4, 4, 4);
        }
    }

    public boolean hasSummon()
    {
        return this.hasSummon;
    }

    public void setHasSummon(boolean summ)
    {
        this.hasSummon = summ;
    }

    public void removeDoor()
    {
        MapleDoor door = getDoors().iterator().next();
        for (MapleCharacter chr : door.getTarget().getCharactersThreadsafe())
        {
            door.sendDestroyData(chr.getClient());
        }
        for (MapleCharacter chr : door.getTown().getCharactersThreadsafe())
        {
            door.sendDestroyData(chr.getClient());
        }
        for (MapleDoor destroyDoor : getDoors())
        {
            door.getTarget().removeMapObject(destroyDoor);
            door.getTown().removeMapObject(destroyDoor);
        }
        clearDoors();
    }

    public void removeMechDoor()
    {
        for (server.maps.MechDoor destroyDoor : getMechDoors())
        {
            for (MapleCharacter chr : getMap().getCharactersThreadsafe())
            {
                destroyDoor.sendDestroyData(chr.getClient());
            }
            getMap().removeMapObject(destroyDoor);
        }
        clearMechDoors();
    }

    public void changeRemoval()
    {
        changeRemoval(false);
    }

    public void changeRemoval(boolean dc)
    {
        if ((getCheatTracker() != null) && (dc))
        {
            getCheatTracker().dispose();
        }
        removeFamiliar();
        dispelSummons();
        if (!dc)
        {
            cancelEffectFromBuffStat(MapleBuffStat.飞行骑乘);
            cancelEffectFromBuffStat(MapleBuffStat.骑兽技能);
            cancelEffectFromBuffStat(MapleBuffStat.金属机甲);
            cancelEffectFromBuffStat(MapleBuffStat.恢复效果);
            cancelEffectFromBuffStat(MapleBuffStat.精神连接);
            cancelEffectFromBuffStat(MapleBuffStat.尖兵飞行);
        }
        if (getPyramidSubway() != null)
        {
            getPyramidSubway().dispose(this);
        }
        if ((this.playerShop != null) && (!dc))
        {
            this.playerShop.removeVisitor(this);
            if (this.playerShop.isOwner(this))
            {
                this.playerShop.setOpen(true);
            }
        }
        if (!getDoors().isEmpty())
        {
            removeDoor();
        }
        if (!getMechDoors().isEmpty())
        {
            removeMechDoor();
        }
        scripting.npc.NPCScriptManager.getInstance().dispose(this.client);
        cancelFairySchedule(false);
    }

    public void updateTick(int newTick)
    {
        this.anticheat.updateTick(newTick);
    }

    public void useFamilyBuff(MapleFamilyBuff buff)
    {
        MapleQuestStatus stat = getQuestNAdd(MapleQuest.getInstance(buff.questID));
        stat.setCustomData(String.valueOf(System.currentTimeMillis()));
    }

    public List<Integer> usedBuffs()
    {
        List<Integer> used = new ArrayList<>();
        MapleFamilyBuff[] z = handling.world.family.MapleFamilyBuff.values();
        for (int i = 0; i < z.length; i++)
        {
            if (!canUseFamilyBuff(z[i]))
            {
                used.add(i);
            }
        }
        return used;
    }

    public boolean canUseFamilyBuff(MapleFamilyBuff buff)
    {
        MapleQuestStatus stat = getQuestNoAdd(MapleQuest.getInstance(buff.questID));
        if (stat == null)
        {
            return true;
        }
        if (stat.getCustomData() == null)
        {
            stat.setCustomData("0");
        }
        return Long.parseLong(stat.getCustomData()) + 86400000L < System.currentTimeMillis();
    }

    public String getTeleportName()
    {
        return this.teleportname;
    }

    public void setTeleportName(String tname)
    {
        this.teleportname = tname;
    }

    public int getNoJuniors()
    {
        if (this.mfc == null)
        {
            return 0;
        }
        return this.mfc.getNoJuniors();
    }

    public MapleFamilyCharacter getMFC()
    {
        return this.mfc;
    }

    public void setFamily(int newf, int news, int newj1, int newj2)
    {
        if ((this.mfc == null) || (newf != this.mfc.getFamilyId()) || (news != this.mfc.getSeniorId()) || (newj1 != this.mfc.getJunior1()) || (newj2 != this.mfc.getJunior2()))
        {
            makeMFC(newf, news, newj1, newj2);
        }
    }

    public int getGachExp()
    {
        return this.gachexp;
    }

    public void setGachExp(int ge)
    {
        this.gachexp = ge;
    }

    public boolean isInBlockedMap()
    {
        if ((!isAlive()) || (getPyramidSubway() != null) || (getMap().getSquadByMap() != null) || (getEventInstance() != null) || (getMap().getEMByMap() != null))
        {
            return true;
        }
        if (((getMapId() >= 680000210) && (getMapId() <= 680000502)) || ((getMapId() / 10000 == 92502) && (getMapId() >= 925020100)) || (getMapId() / 10000 == 92503) || (getMapId() == 180000001))
        {
            return true;
        }
        for (int i : GameConstants.blockedMaps)
        {
            if (getMapId() == i)
            {
                return true;
            }
        }
        return false;
    }

    public server.maps.events.Event_PyramidSubway getPyramidSubway()
    {
        return this.pyramidSubway;
    }

    public scripting.event.EventInstanceManager getEventInstance()
    {
        return this.eventInstance;
    }

    public void setEventInstance(scripting.event.EventInstanceManager eventInstance)
    {
        this.eventInstance = eventInstance;
    }

    public void setPyramidSubway(server.maps.events.Event_PyramidSubway ps)
    {
        this.pyramidSubway = ps;
    }

    public boolean isInTownMap()
    {
        if ((hasBlockedInventory()) || (!getMap().isTown()) || (server.maps.FieldLimitType.VipRock.check(getMap().getFieldLimit())) || (getEventInstance() != null))
        {
            return false;
        }
        for (int i : GameConstants.blockedMaps)
        {
            if (getMapId() == i)
            {
                return false;
            }
        }
        return true;
    }

    public boolean hasBlockedInventory()
    {
        return (!isAlive()) || (getTrade() != null) || (getConversation() > 0) || (getDirection() >= 0) || (getPlayerShop() != null) || (getBattle() != null) || (this.map == null);
    }

    public server.MapleTrade getTrade()
    {
        return this.trade;
    }

    public void setTrade(server.MapleTrade trade)
    {
        this.trade = trade;
    }

    public int getConversation()
    {
        return this.inst.get();
    }

    public void setConversation(int inst)
    {
        this.inst.set(inst);
    }

    public int getDirection()
    {
        return this.insd.get();
    }

    public server.shops.IMaplePlayerShop getPlayerShop()
    {
        return this.playerShop;
    }

    public void setPlayerShop(server.shops.IMaplePlayerShop playerShop)
    {
        this.playerShop = playerShop;
    }

    public server.PokemonBattle getBattle()
    {
        return this.battle;
    }

    public void setBattle(server.PokemonBattle b)
    {
        this.battle = b;
    }

    public void setDirection(int inst)
    {
        this.insd.set(inst);
    }

    public void startPartySearch(List<Integer> jobs, int maxLevel, int minLevel, int membersNeeded)
    {
        for (MapleCharacter chr : this.map.getCharacters())
            if ((chr.getId() != this.id) && (chr.getParty() == null) && (chr.getLevel() >= minLevel) && (chr.getLevel() <= maxLevel) && ((jobs.isEmpty()) || (jobs.contains((int) chr.getJob()))) && ((isGM()) || (!chr.isGM())))
            {
                if ((this.party == null) || (this.party.getMembers().size() >= 6) || (this.party.getMembers().size() >= membersNeeded)) break;
                chr.setParty(this.party);
                handling.world.WrodlPartyService.getInstance().updateParty(this.party.getId(), handling.world.PartyOperation.加入队伍, new MaplePartyCharacter(chr));
                chr.receivePartyMemberHP();
                chr.updatePartyMemberHP();
            }
    }

    public MapleParty getParty()
    {
        if (this.party == null) return null;
        if (this.party.isDisbanded())
        {
            this.party = null;
        }
        return this.party;
    }

    public void receivePartyMemberHP()
    {
        if (this.party == null)
        {
            return;
        }
        int channel = this.client.getChannel();
        for (MaplePartyCharacter partychar : this.party.getMembers())
        {
            if ((partychar != null) && (partychar.getMapid() == getMapId()) && (partychar.getChannel() == channel))
            {
                MapleCharacter other = this.client.getChannelServer().getPlayerStorage().getCharacterByName(partychar.getName());
                if (other != null)
                {
                    this.client.getSession().write(tools.packet.PartyPacket.updatePartyMemberHP(other.getId(), other.getStat().getHp(), other.getStat().getCurrentMaxHp()));
                }
            }
        }
    }

    public void updatePartyMemberHP()
    {
        int channel;
        if ((this.party != null) && (this.client.getChannelServer() != null))
        {
            channel = this.client.getChannel();
            for (MaplePartyCharacter partychar : this.party.getMembers())
            {
                if ((partychar != null) && (partychar.getMapid() == getMapId()) && (partychar.getChannel() == channel))
                {
                    MapleCharacter other = this.client.getChannelServer().getPlayerStorage().getCharacterByName(partychar.getName());
                    if (other != null)
                    {
                        other.getClient().getSession().write(tools.packet.PartyPacket.updatePartyMemberHP(getId(), this.stats.getHp(), this.stats.getCurrentMaxHp()));
                    }
                }
            }
        }
    }

    public PlayerStats getStat()
    {
        return this.stats;
    }

    public void setParty(MapleParty party)
    {
        this.party = party;
    }

    public Battler getBattler(int pos)
    {
        return this.battlers[pos];
    }

    public Battler[] getBattlers()
    {
        return this.battlers;
    }

    public List<Battler> getBoxed()
    {
        return this.boxed;
    }

    public void changedBattler()
    {
        this.changed_pokemon = true;
    }

    public void makeBattler(int index, int monsterId)
    {
        server.life.MapleMonsterStats mons = server.life.MapleLifeFactory.getMonsterStats(monsterId);
        this.battlers[index] = new Battler(mons);
        this.battlers[index].setCharacterId(this.id);
        this.changed_pokemon = true;
        getMonsterBook().monsterCaught(this.client, monsterId, mons.getName());
    }

    public MonsterBook getMonsterBook()
    {
        return this.monsterbook;
    }

    public boolean removeBattler(int ind)
    {
        if (countBattlers() <= 1)
        {
            return false;
        }
        if (ind == this.battlers.length)
        {
            this.battlers[ind] = null;
        }
        else
        {
            for (int i = ind; i < this.battlers.length; i++)
            {
                this.battlers[i] = (i + 1 == this.battlers.length ? null : this.battlers[(i + 1)]);
            }
        }
        this.changed_pokemon = true;
        return true;
    }

    public int countBattlers()
    {
        int ret = 0;
        for (Battler battler : this.battlers)
        {
            if (battler != null)
            {
                ret++;
            }
        }
        return ret;
    }

    public int getChallenge()
    {
        return this.challenge;
    }

    public void setChallenge(int c)
    {
        this.challenge = c;
    }

    public short getFatigue()
    {
        return this.fatigue;
    }

    public void setFatigue(int j)
    {
        this.fatigue = ((short) Math.max(0, j));
        updateSingleStat(MapleStat.疲劳, this.fatigue);
    }

    public void fakeRelog()
    {
        this.client.getSession().write(MaplePacketCreator.getCharInfo(this));
        MapleMap mapp = getMap();
        mapp.setCheckStates(false);
        mapp.removePlayer(this);
        mapp.addPlayer(this);
        mapp.setCheckStates(true);
        if (GameConstants.GMS)
        {
            this.client.getSession().write(MaplePacketCreator.getFamiliarInfo(this));
        }
        this.client.getSession().write(MaplePacketCreator.serverNotice(5, "刷新人数据完成..."));
    }

    public boolean canSummon()
    {
        return canSummon(5000);
    }

    public boolean canSummon(int g)
    {
        if (this.lastSummonTime + g < System.currentTimeMillis())
        {
            this.lastSummonTime = System.currentTimeMillis();
            return true;
        }
        return false;
    }

    public int getIntNoRecord(int questID)
    {
        MapleQuestStatus stat = getQuestNoAdd(MapleQuest.getInstance(questID));
        if ((stat == null) || (stat.getCustomData() == null))
        {
            return 0;
        }
        return Integer.parseInt(stat.getCustomData());
    }

    public void updatePetAuto()
    {
        this.client.getSession().write(MaplePacketCreator.petAutoHP(getIntRecord(122221)));
        this.client.getSession().write(MaplePacketCreator.petAutoMP(getIntRecord(122223)));
        this.client.getSession().write(MaplePacketCreator.petAutoBuff(getIntRecord(122224)));
    }

    public int getIntRecord(int questID)
    {
        MapleQuestStatus stat = getQuestNAdd(MapleQuest.getInstance(questID));
        if (stat.getCustomData() == null)
        {
            stat.setCustomData("0");
        }
        return Integer.parseInt(stat.getCustomData());
    }

    public void sendEnglishQuiz(String msg)
    {
        this.client.getSession().write(MaplePacketCreator.englishQuizMsg(msg));
    }

    public long getChangeTime()
    {
        return this.mapChangeTime;
    }

    public void setChangeTime(boolean changeMap)
    {
        this.mapChangeTime = System.currentTimeMillis();
        if (changeMap)
        {
            getCheatTracker().resetInMapIimeCount();
        }
    }

    public CheatTracker getCheatTracker()
    {
        return this.anticheat;
    }

    public Map<ReportType, Integer> getReports()
    {
        return this.reports;
    }

    public void addReport(ReportType type)
    {
        Integer value = this.reports.get(type);
        this.reports.put(type, value == null ? 1 : value.intValue() + 1);
        this.changed_reports = true;
    }

    public void clearReports(ReportType type)
    {
        this.reports.remove(type);
        this.changed_reports = true;
    }

    public void clearReports()
    {
        this.reports.clear();
        this.changed_reports = true;
    }

    public int getReportPoints()
    {
        int ret = 0;
        for (Integer entry : this.reports.values())
        {
            ret += entry;
        }
        return ret;
    }

    public String getReportSummary()
    {
        StringBuilder ret = new StringBuilder();
        List<Pair<ReportType, Integer>> offenseList = new ArrayList<>();
        for (Map.Entry<ReportType, Integer> entry : this.reports.entrySet())
        {
            offenseList.add(new Pair(entry.getKey(), entry.getValue()));
        }
        Collections.sort(offenseList, (Comparator) (o1, o2) -> {
            int thisVal = ((Pair<ReportType, Integer>) o1).getRight();
            int anotherVal = ((Pair<ReportType, Integer>) o2).getRight();
            return thisVal == anotherVal ? 0 : thisVal < anotherVal ? 1 : -1;
        });
        for (Pair<ReportType, Integer> reportTypeIntegerPair : offenseList)
        {
            ret.append(tools.StringUtil.makeEnumHumanReadable(reportTypeIntegerPair.left.name()));
            ret.append(": ");
            ret.append(reportTypeIntegerPair.right);
            ret.append(" ");
        }
        return ret.toString();
    }

    public short getScrolledPosition()
    {
        return this.scrolledPosition;
    }

    public void setScrolledPosition(short s)
    {
        this.scrolledPosition = s;
    }

    public void forceCompleteQuest(int id)
    {
        MapleQuest.getInstance(id).forceComplete(this, 9270035);
    }

    public List<Integer> getExtendedSlots()
    {
        return this.extendedSlots;
    }

    public int getExtendedSlot(int index)
    {
        if ((this.extendedSlots.size() <= index) || (index < 0))
        {
            return -1;
        }
        return this.extendedSlots.get(index);
    }

    public void changedExtended()
    {
        this.changed_extendedSlots = true;
    }

    public client.inventory.MapleAndroid getAndroid()
    {
        return this.android;
    }

    public void setAndroid(client.inventory.MapleAndroid a)
    {
        if (checkHearts())
        {
            this.android = a;
            if ((this.map != null) && (a != null))
            {
                this.map.broadcastMessage(tools.packet.AndroidPacket.spawnAndroid(this, a));
                this.map.broadcastMessage(tools.packet.AndroidPacket.showAndroidEmotion(getId(), Randomizer.nextInt(17) + 1));
            }
        }
    }

    public boolean checkHearts()
    {
        return getInventory(MapleInventoryType.EQUIPPED).getItem((short) -35) != null;
    }

    public void removeAndroid()
    {
        if (this.map != null)
        {
            this.map.broadcastMessage(tools.packet.AndroidPacket.deactivateAndroid(this.id));
        }
        if (this.android != null)
        {
            this.android.saveToDb();
        }
        this.android = null;
    }

    public void updateAndroid(int size, int itemId)
    {
        if (this.map != null)
        {
            this.map.broadcastMessage(tools.packet.AndroidPacket.updateAndroidLook(getId(), size, itemId));
        }
    }

    public MapleSidekick getSidekick()
    {
        return this.sidekick;
    }

    public void setSidekick(MapleSidekick s)
    {
        this.sidekick = s;
    }

    public List<server.shop.MapleShopItem> getRebuy()
    {
        return this.rebuy;
    }

    public Map<Integer, MonsterFamiliar> getFamiliars()
    {
        return this.familiars;
    }

    public MonsterFamiliar getSummonedFamiliar()
    {
        return this.summonedFamiliar;
    }

    public void removeFamiliar()
    {
        if ((this.summonedFamiliar != null) && (this.map != null))
        {
            removeVisibleFamiliar();
        }
        this.summonedFamiliar = null;
    }

    public void removeVisibleFamiliar()
    {
        getMap().removeMapObject(this.summonedFamiliar);
        removeVisibleMapObject(this.summonedFamiliar);
        getMap().broadcastMessage(MaplePacketCreator.removeFamiliar(getId()));
        this.anticheat.resetFamiliarAttack();
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        cancelEffect(ii.getItemEffect(ii.getFamiliar(this.summonedFamiliar.getFamiliar()).passive), false, System.currentTimeMillis());
    }

    public void spawnFamiliar(MonsterFamiliar mf)
    {
        this.summonedFamiliar = mf;
        mf.setStance(0);
        mf.setPosition(getPosition());
        mf.setFh(getFH());
        addVisibleMapObject(mf);
        getMap().spawnFamiliar(mf);
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        MapleStatEffect eff = ii.getItemEffect(ii.getFamiliar(this.summonedFamiliar.getFamiliar()).passive);
        if ((eff != null) && (eff.getInterval() <= 0) && (eff.makeChanceResult()))
        {
            eff.applyTo(this);
        }
        this.lastFamiliarEffectTime = System.currentTimeMillis();
    }

    public int getFH()
    {
        server.maps.MapleFoothold fh = getMap().getFootholds().findBelow(getTruePosition());
        if (fh != null)
        {
            return fh.getId();
        }
        return 0;
    }

    public void addVisibleMapObject(MapleMapObject mo)
    {
        visibleMapObjectsLock.writeLock().lock();
        try
        {
            visibleMapObjects.add(mo);
        }
        finally
        {
            visibleMapObjectsLock.writeLock().unlock();
        }
    }

    public void doFamiliarSchedule(long now)
    {
        for (MonsterFamiliar mf : this.familiars.values())
        {
            if ((this.summonedFamiliar != null) && (this.summonedFamiliar.getId() == mf.getId()))
            {
                mf.addFatigue(this, 5);
                MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
                MapleStatEffect eff = ii.getItemEffect(ii.getFamiliar(this.summonedFamiliar.getFamiliar()).passive);
                if ((eff != null) && (eff.getInterval() > 0) && (canFamiliarEffect(now, eff)) && (eff.makeChanceResult()))
                {
                    eff.applyTo(this);
                }
            }
            else if (mf.getFatigue() > 0)
            {
                mf.setFatigue(Math.max(0, mf.getFatigue() - 5));
            }
        }
    }

    public boolean canFamiliarEffect(long now, MapleStatEffect eff)
    {
        return (this.lastFamiliarEffectTime > 0L) && (this.lastFamiliarEffectTime + eff.getInterval() < now);
    }

    public MapleImp[] getImps()
    {
        return this.imps;
    }

    public void sendImp()
    {
        for (int i = 0; i < this.imps.length; i++)
        {
            if (this.imps[i] != null)
            {
                this.client.getSession().write(MaplePacketCreator.updateImp(this.imps[i], ImpFlag.SUMMONED.getValue(), i, true));
            }
        }
    }

    public int getBattlePoints()
    {
        return this.pvpPoints;
    }

    public void setBattlePoints(int p)
    {
        if (p != this.pvpPoints)
        {
            this.client.getSession().write(UIPacket.getBPMsg(p - this.pvpPoints));
            updateSingleStat(MapleStat.BATTLE_POINTS, p);
        }
        this.pvpPoints = p;
    }

    public int getTotalBattleExp()
    {
        return this.pvpExp;
    }

    public void setTotalBattleExp(int p)
    {
        int previous = this.pvpExp;
        this.pvpExp = p;
        if (p != previous)
        {
            this.stats.recalcPVPRank(this);
            updateSingleStat(MapleStat.BATTLE_EXP, this.stats.pvpExp);
            updateSingleStat(MapleStat.BATTLE_RANK, this.stats.pvpRank);
        }
    }

    public void changeTeam(int newTeam)
    {
        this.coconutteam = newTeam;
        if (!inPVP())
        {


            this.client.getSession().write(MaplePacketCreator.showEquipEffect(newTeam));
        }
    }

    public boolean inPVP()
    {
        return (this.eventInstance != null) && (this.eventInstance.getName().startsWith("PVP")) && (this.client.getChannelServer().isCanPvp());
    }

    public void disease(int type, int level)
    {
        if (MapleDisease.getBySkill(type) == null)
        {
            return;
        }
        this.chair = 0;
        this.client.getSession().write(MaplePacketCreator.cancelChair(-1));
        this.map.broadcastMessage(this, MaplePacketCreator.showChair(this.id, 0), false);
        giveDebuff(MapleDisease.getBySkill(type), server.life.MobSkillFactory.getInstance().getMobSkill(type, level));
    }

    public void clearAllCooldowns()
    {
        for (MapleCoolDownValueHolder m : getCooldowns())
        {
            int skil = m.skillId;
            removeCooldown(skil);
            this.client.getSession().write(MaplePacketCreator.skillCooldown(skil, 0));
        }
    }

    public List<MapleCoolDownValueHolder> getCooldowns()
    {
        List<MapleCoolDownValueHolder> ret = new ArrayList<>();
        for (MapleCoolDownValueHolder mc : this.coolDowns.values())
        {
            if (mc != null)
            {
                ret.add(mc);
            }
        }
        return ret;
    }

    public void removeCooldown(int skillId)
    {
        this.coolDowns.remove(skillId);
    }

    public Pair<Double, Boolean> modifyDamageTaken(double damage, MapleMapObject attacke)
    {
        Pair<Double, Boolean> ret = new Pair(damage, Boolean.FALSE);
        if (damage < 0.0D)
        {
            return ret;
        }
        if ((this.stats.ignoreDAMr > 0) && (Randomizer.nextInt(100) < this.stats.ignoreDAMr_rate))
        {
            damage -= Math.floor(this.stats.ignoreDAMr * damage / 100.0D);
        }
        if ((this.stats.ignoreDAM > 0) && (Randomizer.nextInt(100) < this.stats.ignoreDAM_rate))
        {
            damage -= this.stats.ignoreDAM;
        }
        Integer div = getBuffedValue(MapleBuffStat.祝福护甲);
        Integer div2 = getBuffedValue(MapleBuffStat.神圣魔法盾);
        if (div2 != null)
        {
            if (div2 <= 0)
            {
                cancelEffectFromBuffStat(MapleBuffStat.神圣魔法盾);
            }
            else
            {
                setBuffedValue(MapleBuffStat.神圣魔法盾, div2 - 1);
                damage = 0.0D;
            }
        }
        else if (div != null)
        {
            if (div <= 0)
            {
                cancelEffectFromBuffStat(MapleBuffStat.祝福护甲);
            }
            else
            {
                setBuffedValue(MapleBuffStat.祝福护甲, div - 1);
                damage = 0.0D;
            }
        }
        List<Integer> attack = ((attacke instanceof MapleMonster)) || (attacke == null) ? null : new ArrayList();
        if (damage > 0.0D)
        {
            MapleCharacter attacker;
            if ((getJob() == 122) && (!skillisCooling(1210016)))
            {
                Skill divine = SkillFactory.getSkill(1210016);
                if (getTotalSkillLevel(divine) > 0)
                {
                    MapleStatEffect divineShield = divine.getEffect(getTotalSkillLevel(divine));
                    if (divineShield.makeChanceResult())
                    {
                        divineShield.applyTo(this);
                        this.client.getSession().write(MaplePacketCreator.skillCooldown(1210016, divineShield.getCooldown(this)));
                        addCooldown(1210016, System.currentTimeMillis(), divineShield.getCooldown(this) * 1000);
                    }
                }
            }
            else if (getJob() == 2112)
            {
                Skill achilles = SkillFactory.getSkill(21120004);
                if (getTotalSkillLevel(achilles) > 0)
                {
                    MapleStatEffect multiplier = achilles.getEffect(getTotalSkillLevel(achilles));
                    damage = multiplier.getX() / 1000.0D * damage;
                }
            }
            else if ((getJob() == 2710) || (getJob() == 2711) || (getJob() == 2712))
            {
                this.lastBlessOfDarknessTime = System.currentTimeMillis();
                Skill skill = SkillFactory.getSkill(27100003);
                if ((getTotalSkillLevel(skill) > 0) && (getBuffedValue(MapleBuffStat.黑暗祝福) != null))
                {
                    MapleStatEffect effect = skill.getEffect(getTotalSkillLevel(skill));
                    handleBlackBlessLost(1);
                    damage = effect.getX() / 100.0D * damage;
                }
            }
            else if (getJob() == 3112)
            {
                Skill divine = SkillFactory.getSkill(31120009);
                if (getTotalSkillLevel(divine) > 0)
                {
                    MapleStatEffect eff = divine.getEffect(getTotalSkillLevel(divine));
                    damage = eff.getX() / 1000.0D * damage;
                }
            }
            else if (getJob() == 5112)
            {
                Skill divine = SkillFactory.getSkill(51120003);
                if (getTotalSkillLevel(divine) > 0)
                {
                    MapleStatEffect eff = divine.getEffect(getTotalSkillLevel(divine));
                    damage = eff.getX() / 1000.0D * damage;
                }
            }
            else if ((getBuffedValue(MapleBuffStat.卫星防护_PROC) != null) && (getBuffedValue(MapleBuffStat.卫星防护_吸收) != null) && (getBuffedValue(MapleBuffStat.替身术) != null))
            {
                double buff = getBuffedValue(MapleBuffStat.卫星防护_PROC).doubleValue();
                double buffz = getBuffedValue(MapleBuffStat.卫星防护_吸收).doubleValue();
                if ((int) (buff / 100.0D * getStat().getMaxHp()) <= damage)
                {
                    damage -= buffz / 100.0D * damage;
                    cancelEffectFromBuffStat(MapleBuffStat.替身术);
                }
            }
            else if ((getJob() == 433) || (getJob() == 434))
            {
                Skill divine = SkillFactory.getSkill(4330001);
                if ((getTotalSkillLevel(divine) > 0) && (getBuffedValue(MapleBuffStat.隐身术) == null) && (!skillisCooling(divine.getId())))
                {
                    MapleStatEffect divineShield = divine.getEffect(getTotalSkillLevel(divine));
                    if (Randomizer.nextInt(100) < divineShield.getX())
                    {
                        divineShield.applyTo(this);
                    }
                }
            }
            else if (((getJob() == 512) || (getJob() == 522) || (getJob() == 582) || (getJob() == 592) || (getJob() == 572)) && (getBuffedValue(MapleBuffStat.反制攻击) == null))
            {
                Skill divine = SkillFactory.getSkill(getJob() == 522 ? 5220012 : getJob() == 512 ? 5120011 : 5720012);
                if ((getTotalSkillLevel(divine) > 0) && (!skillisCooling(divine.getId())))
                {
                    MapleStatEffect divineShield = divine.getEffect(getTotalSkillLevel(divine));
                    if (divineShield.makeChanceResult())
                    {
                        divineShield.applyTo(this);
                        this.client.getSession().write(MaplePacketCreator.skillCooldown(divine.getId(), divineShield.getX()));
                        addCooldown(divine.getId(), System.currentTimeMillis(), divineShield.getX() * 1000);
                    }
                }
            }
            else if (((getJob() == 531) || (getJob() == 532)) && (attacke != null))
            {

                Skill divine = SkillFactory.getSkill(5310009);
                if (getTotalSkillLevel(divine) > 0)
                {
                    MapleStatEffect divineShield = divine.getEffect(getTotalSkillLevel(divine));
                    if (divineShield.makeChanceResult())
                    {
                        if ((attacke instanceof MapleMonster))
                        {
                            MapleMonster monsterr = (MapleMonster) attacke;
                            int theDmg = (int) (divineShield.getDamage() * getStat().getCurrentMaxBaseDamage() / 100.0D);
                            monsterr.damage(this, theDmg, true);
                            getMap().broadcastMessage(tools.packet.MobPacket.damageMonster(monsterr.getObjectId(), theDmg));
                        }
                        else
                        {
                            attacker = (MapleCharacter) attacke;
                            attacker.addHP(-divineShield.getDamage());
                            attack.add(divineShield.getDamage());
                        }
                    }
                }
            }
            else if ((getJob() == 132) && (attacke != null))
            {
                Skill divine = SkillFactory.getSkill(1320011);
                if ((getTotalSkillLevel(divine) > 0) && (!skillisCooling(divine.getId())) && (getBuffSource(MapleBuffStat.灵魂助力) == 1301013))
                {
                    MapleStatEffect divineShield = divine.getEffect(getTotalSkillLevel(divine));
                    if (divineShield.makeChanceResult())
                    {
                        this.client.getSession().write(MaplePacketCreator.skillCooldown(divine.getId(), divineShield.getCooldown(this)));
                        addCooldown(divine.getId(), System.currentTimeMillis(), divineShield.getCooldown(this) * 1000);
                        if ((attacke instanceof MapleMonster))
                        {
                            MapleMonster monsterrr = (MapleMonster) attacke;
                            int theDmg = (int) (divineShield.getDamage() * getStat().getCurrentMaxBaseDamage() / 100.0D);
                            monsterrr.damage(this, theDmg, true);
                            getMap().broadcastMessage(tools.packet.MobPacket.damageMonster(monsterrr.getObjectId(), theDmg));
                        }
                        else
                        {
                            attacker = (MapleCharacter) attacke;
                            attacker.addHP(-divineShield.getDamage());
                            attack.add(divineShield.getDamage());
                        }
                    }
                }
            }
            if (attacke != null)
            {
                int damr = (Randomizer.nextInt(100) < getStat().DAMreflect_rate ? getStat().DAMreflect : 0) + (getBuffedValue(MapleBuffStat.伤害反击) != null ? getBuffedValue(MapleBuffStat.伤害反击) : 0);
                if (damr > 0)
                {
                    long bouncedamage = (long) (damage * damr / 100.0D);
                    if ((attacke instanceof MapleMonster))
                    {
                        MapleMonster monster = (MapleMonster) attacke;
                        bouncedamage = Math.min(bouncedamage, monster.getMobMaxHp() / 10L);
                        monster.damage(this, bouncedamage, true);
                        getMap().broadcastMessage(this, tools.packet.MobPacket.damageMonster(monster.getObjectId(), bouncedamage), getTruePosition());
                        if (getBuffSource(MapleBuffStat.伤害反击) == 31101003)
                        {
                            MapleStatEffect eff = getStatForBuff(MapleBuffStat.伤害反击);
                            monster.applyStatus(this, new MonsterStatusEffect(MonsterStatus.眩晕, 1, eff.getSourceId(), null, false), false, 5000L, true, eff);
                        }
                    }
                    else
                    {
                        MapleCharacter monster1 = (MapleCharacter) attacke;
                        bouncedamage = Math.min(bouncedamage, monster1.getStat().getCurrentMaxHp() / 10);
                        monster1.addHP(-(int) bouncedamage);

                        attack.add((int) bouncedamage);
                        if (getBuffSource(MapleBuffStat.伤害反击) == 31101003)
                        {
                            monster1.disease(MapleDisease.眩晕.getDisease(), 1);
                        }
                    }
                    ret.right = Boolean.TRUE;
                }
                if (((getJob() == 411) || (getJob() == 412) || (getJob() == 421) || (getJob() == 422) || (getJob() == 1411) || (getJob() == 1412)) && (getBuffedValue(MapleBuffStat.召唤兽) != null))
                {
                    List<MapleSummon> ss = getSummonsReadLock();
                    try
                    {
                        for (MapleSummon sum : ss)
                        {
                            if ((sum.getTruePosition().distanceSq(getTruePosition()) < 400000.0D) && ((sum.getSkillId() == 4111007) || (sum.getSkillId() == 4211007) || (sum.getSkillId() == 14111010)))
                            {
                                if ((attacke instanceof MapleMonster))
                                {
                                    List<Pair<Integer, Boolean>> allDamageNumbers = new ArrayList<>();
                                    List<tools.AttackPair> allDamage = new ArrayList<>();
                                    MapleMonster monster2 = (MapleMonster) attacke;
                                    int theDmg = (int) (SkillFactory.getSkill(sum.getSkillId()).getEffect(sum.getSkillLevel()).getX() * damage / 100.0D);
                                    allDamageNumbers.add(new Pair(theDmg, Boolean.FALSE));
                                    allDamage.add(new tools.AttackPair(monster2.getObjectId(), allDamageNumbers));
                                    getMap().broadcastMessage(tools.packet.SummonPacket.summonAttack(sum.getOwnerId(), sum.getObjectId(), (byte) -124, (byte) 17, allDamage, getLevel(), true));
                                    monster2.damage(this, theDmg, true);
                                    checkMonsterAggro(monster2);
                                    if (!monster2.isAlive())
                                    {
                                        getClient().getSession().write(tools.packet.MobPacket.killMonster(monster2.getObjectId(), 1));
                                    }
                                }
                                else
                                {
                                    MapleCharacter chr = (MapleCharacter) attacke;
                                    int dmg = SkillFactory.getSkill(sum.getSkillId()).getEffect(sum.getSkillLevel()).getX();
                                    chr.addHP(-dmg);
                                    attack.add(dmg);
                                }
                            }
                        }
                    }
                    finally
                    {
                        unlockSummonsReadLock();
                    }
                }
            }
        }
        else if (damage == 0.0D)
        {
            if (((getJob() == 433) || (getJob() == 434)) && (attacke != null))
            {
                Skill divine = SkillFactory.getSkill(4330009);
                if (getTotalSkillLevel(divine) > 0)
                {
                    MapleStatEffect divineShield = divine.getEffect(getTotalSkillLevel(divine));
                    int prop = getTotalSkillLevel(divine) + 10;
                    if (Randomizer.nextInt(100) < prop)
                    {
                        divineShield.applyTo(this);
                        getCheatTracker().resetTakeDamage();
                    }
                }
            }
            else if (((getJob() == 3611) || (getJob() == 3612)) && (attacke != null))
            {
                Skill AegisSystem = SkillFactory.getSkill(36110004);
                if (getTotalSkillLevel(AegisSystem) > 0)
                {
                    MapleStatEffect effect = AegisSystem.getEffect(getTotalSkillLevel(AegisSystem));
                    if ((effect != null) && (effect.makeChanceResult()) && ((attacke instanceof MapleMonster)))
                    {
                        MapleMonster attacker = (MapleMonster) attacke;
                        int attackCount = effect.getX();
                        this.specialStats.gainForceCounter();
                        getClient().getSession().write(MaplePacketCreator.showAegisSystemAttack(getId(), 36110004, this.specialStats.getForceCounter(), attacker.getObjectId(), attackCount));
                        if (attackCount - 1 > 0)
                        {
                            this.specialStats.gainForceCounter(attackCount - 1);
                        }
                        getCheatTracker().resetTakeDamage();
                    }
                }
            }
        }

        if ((attack != null) && (attack.size() > 0) && (attacke != null))
        {
            getMap().broadcastMessage(MaplePacketCreator.pvpCool(attacke.getObjectId(), attack));
        }
        ret.left = damage;
        return ret;
    }

    public void onAttack(long maxhp, int maxmp, int skillid, int oid, int totDamage)
    {
        if ((this.stats.hpRecoverProp > 0) && (Randomizer.nextInt(100) <= this.stats.hpRecoverProp))
        {
            if (this.stats.hpRecover > 0)
            {
                healHP(this.stats.hpRecover);
            }
            if (this.stats.hpRecoverPercent > 0)
            {
                addHP((int) Math.min(maxhp, Math.min((int) (totDamage * this.stats.hpRecoverPercent / 100.0D), this.stats.getMaxHp() / 2)));
            }
        }

        if ((this.stats.mpRecoverProp > 0) && (!GameConstants.isNotMpJob(getJob())) && (Randomizer.nextInt(100) <= this.stats.mpRecoverProp) && (this.stats.mpRecover > 0))
        {
            healMP(this.stats.mpRecover);
        }


        if (getBuffedValue(MapleBuffStat.连环吸血) != null)
        {
            addHP((int) Math.min(maxhp, Math.min((int) (totDamage * getStatForBuff(MapleBuffStat.连环吸血).getX() / 100.0D), this.stats.getMaxHp() / 2)));
        }
        if (getBuffSource(MapleBuffStat.连环吸血) == 23101003)
        {
            addMP(Math.min(maxmp, Math.min((int) (totDamage * getStatForBuff(MapleBuffStat.连环吸血).getX() / 100.0D), this.stats.getMaxMp() / 2)));
        }
        if ((getBuffedValue(MapleBuffStat.幻灵重生) != null) && (getBuffedValue(MapleBuffStat.召唤兽) == null) && (getSummonsSize() < 4) && (canSummon()))
        {
            MapleStatEffect eff = getStatForBuff(MapleBuffStat.幻灵重生);
            if (eff.makeChanceResult())
            {
                eff.applyTo(this, this, false, null, eff.getDuration());
            }
        }
        if ((getJob() == 212) || (getJob() == 222) || (getJob() == 232))
        {
            int[] skillIds = {2120010, 2220010, 2320011};
            for (int i : skillIds)
            {
                Skill skill = SkillFactory.getSkill(i);
                if (getTotalSkillLevel(skill) > 0)
                {
                    MapleStatEffect venomEffect = skill.getEffect(getTotalSkillLevel(skill));
                    if (ServerProperties.ShowPacket())
                    {
                        log.info("神秘瞄准术: " + skill.getId() + " - " + skill.getName() + " getAllLinkMid " + getAllLinkMid().size() + " Y " + venomEffect.getY());
                    }
                    if ((!venomEffect.makeChanceResult()) || (getAllLinkMid().size() >= venomEffect.getY())) break;
                    setLinkMid(oid, venomEffect.getX());
                    venomEffect.applyTo(this);
                    if (!ServerProperties.ShowPacket()) break;
                    log.info("神秘瞄准术: 开始加BUFF");
                    break;
                }
            }
        }


        if (getBuffSource(MapleBuffStat.模式变更) == 110001501)
        {
            Skill skil = SkillFactory.getSkill(112100011);
            if (getTotalSkillLevel(skil) > 0)
            {
                MapleStatEffect effect = skil.getEffect(getTotalSkillLevel(skil));
                if ((effect != null) && (Randomizer.nextInt(100) <= effect.getProb()))
                {
                    addHP((int) Math.min(maxhp, Math.min((int) (totDamage * effect.getX() / 100.0D), this.stats.getMaxHp() / 10)));
                }
            }
        }
        if (skillid > 0)
        {
            Skill skil = SkillFactory.getSkill(skillid);
            MapleStatEffect effect = skil.getEffect(getTotalSkillLevel(skil));
            switch (skillid)
            {
                case 14101006:
                case 31111003:
                case 33111006:
                    addHP((int) Math.min(maxhp, Math.min((int) (totDamage * effect.getX() / 100.0D), this.stats.getMaxHp() / 2)));
                    break;

                case 5221015:
                case 5721003:
                case 22151002:
                    setLinkMid(oid, effect.getX());
            }

        }

        switch (getJob())
        {
            case 2410:
            case 2411:
            case 2412:
                if ((skillid != 24120002) || (skillid != 24100003))
                {
                    handleCarteGain(oid);
                }
                break;
            case 2700:
            case 2710:
            case 2711:
            case 2712:
                if (skillid > 0)
                {
                    handleLuminous(skillid);
                }
                if (getJob() == 2712)
                {
                    handleDarkCrescendo();
                }
                handleBlackBless();
                break;
            case 3610:
            case 3611:
            case 3612:
                addPowerCount(1);
        }

    }

    public void afterAttack(int mobCount, int attackCount, int skillid)
    {
        switch (getJob())
        {
            case 510:
            case 511:
            case 512:
                if (!this.specialStats.isEnergyfull())
                {
                    handleEnergyCharge(mobCount * 2);
                }
                else
                {
                    handleEnergyConsume(mobCount, skillid);
                }
                break;
            case 110:
            case 111:
            case 112:
            case 2411:
            case 2412:
                if (((skillid != 1111008 ? 1 : 0) & (getBuffedValue(MapleBuffStat.斗气集中) != null ? 1 : 0)) != 0)
                {
                    handleOrbgain();
                }
                break;
            case 6100:
            case 6110:
            case 6111:
            case 6112:
                handleMorphCharge(isAdmin() ? mobCount * attackCount * 10 : 1);
        }

        if (!isIntern())
        {
            cancelEffectFromBuffStat(MapleBuffStat.风影漫步);
            cancelEffectFromBuffStat(MapleBuffStat.潜入);
            MapleStatEffect ds = getStatForBuff(MapleBuffStat.隐身术);
            if ((ds != null) && ((ds.getSourceId() != 4330001) || (!ds.makeChanceResult())))
            {
                cancelEffectFromBuffStat(MapleBuffStat.隐身术);
            }
        }
    }

    public void applyIceGage(int x)
    {
        updateSingleStat(MapleStat.ICE_GAGE, x);
    }

    public java.awt.Rectangle getBounds()
    {
        return new java.awt.Rectangle(getTruePosition().x - 25, getTruePosition().y - 75, 50, 75);
    }

    public void handleCritStorage()
    {
        if ((getJob() != 420) && (getJob() != 421) && (getJob() != 422))
        {
            return;
        }

        Skill critskill_2 = SkillFactory.getSkill(4200013);
        Skill critskill_4 = SkillFactory.getSkill(4220015);
        MapleStatEffect skilleffect;
        if (getSkillLevel(critskill_4) > 0)
        {
            skilleffect = critskill_4.getEffect(getTotalSkillLevel(critskill_4));
        }
        else
        {
            if (getSkillLevel(critskill_2) > 0) skilleffect = critskill_2.getEffect(getTotalSkillLevel(critskill_2));
            else return;
        }
        MapleBuffStat buffStat = MapleBuffStat.暴击概率;

        MapleStatEffect effect = getStatForBuff(buffStat);
        if ((effect == null) || (getBuffedValue(buffStat) == null))
        {
            skilleffect.applyTo(this);
            return;
        }

        int crit = getBuffedIntValue(buffStat);
        int newcrit = crit;

        if (newcrit < 100)
        {
            newcrit += skilleffect.getX();
            if (newcrit > 100)
            {
                newcrit = 100;
            }
        }

        if (crit != newcrit)
        {
            Map<MapleBuffStat, Integer> stat = Collections.singletonMap(buffStat, newcrit);
            setBuffedValue(buffStat, newcrit);
            int duration = effect.getDuration();
            duration += (int) (getBuffedStartTime(buffStat) - System.currentTimeMillis());
        }
    }

    public boolean getCygnusBless()
    {
        int jobid = getJob();
        return ((getSkillLevel(12) > 0) && (jobid >= 0) && (jobid < 1000)) || ((getSkillLevel(10000012) > 0) && (jobid >= 1000) && (jobid < 2000)) || ((getSkillLevel(20000012) > 0) && ((jobid == 2000) || ((jobid >= 2100) && (jobid <= 2112)))) || ((getSkillLevel(20010012) > 0) && ((jobid == 2001) || ((jobid >= 2200) && (jobid <= 2218)))) || ((getSkillLevel(20020012) > 0) && ((jobid == 2002) || ((jobid >= 2300) && (jobid <= 2312)))) || ((getSkillLevel(20030012) > 0) && ((jobid == 2003) || ((jobid >= 2400) && (jobid <= 2412)))) || ((getSkillLevel(20040012) > 0) && ((jobid == 2004) || ((jobid >= 2700) && (jobid <= 2712)))) || ((getSkillLevel(30000012) > 0) && ((jobid == 3000) || ((jobid >= 3200) && (jobid <= 3512)))) || ((getSkillLevel(30010012) > 0) && (GameConstants.is恶魔职业(jobid))) || ((getSkillLevel(30020012) > 0) && ((jobid == 3002) || ((jobid >= 3600) && (jobid <= 3612)))) || ((getSkillLevel(50000012) > 0) && ((jobid == 5000) || ((jobid >= 5100) && (jobid <= 5112)))) || ((getSkillLevel(60000012) > 0) && ((jobid == 6000) || ((jobid >= 6100) && (jobid <= 6112)))) || ((getSkillLevel(60010012) > 0) && ((jobid == 6001) || ((jobid >= 6500) && (jobid <= 6512)))) || ((getSkillLevel(100000012) > 0) && ((jobid == 10000) || ((jobid >= 10100) && (jobid <= 10112)))) || ((getSkillLevel(110000012) > 0) && ((jobid == 11000) || ((jobid >= 11200) && (jobid <= 11212))));
    }

    public byte get精灵祝福()
    {
        int jobid = getJob();
        if (((getSkillLevel(20021110) > 0) && ((jobid == 2002) || ((jobid >= 2300) && (jobid <= 2312)))) || (getSkillLevel(80001040) > 0))
        {
            return 10;
        }
        return 0;
    }

    public void handleForceGain(int oid, int skillid)
    {
        handleForceGain(oid, skillid, 0);
    }

    public void handleForceGain(int moboid, int skillid, int extraForce)
    {
        if ((!GameConstants.isForceIncrease(skillid)) && (extraForce <= 0))
        {
            return;
        }
        int forceGain = 1;
        if ((getLevel() >= 30) && (getLevel() < 70))
        {
            forceGain = 2;
        }
        else if ((getLevel() >= 70) && (getLevel() < 120))
        {
            forceGain = 3;
        }
        else if (getLevel() >= 120)
        {
            forceGain = 4;
        }
        this.specialStats.gainForceCounter();
        addMP(extraForce > 0 ? extraForce : forceGain, true);
        getClient().getSession().write(MaplePacketCreator.showForce(this, moboid, this.specialStats.getForceCounter(), forceGain));
        if ((this.stats.mpRecoverProp > 0) && (extraForce <= 0) && (Randomizer.nextInt(100) <= this.stats.mpRecoverProp))
        {
            this.specialStats.gainForceCounter();
            addMP(this.stats.mpRecover, true);
            getClient().getSession().write(MaplePacketCreator.showForce(this, moboid, this.specialStats.getForceCounter(), this.stats.mpRecoverProp));
        }
    }

    public void addMP(int delta, boolean ignore)
    {
        if ((GameConstants.isNotMpJob(getJob())) && (GameConstants.getMPByJob(getJob()) <= 0))
        {
            return;
        }
        if (((delta < 0) && (GameConstants.is恶魔猎手(getJob()))) || (((!GameConstants.is恶魔猎手(getJob())) || (ignore)) && (this.stats.setMp(this.stats.getMp() + delta, this))))
            updateSingleStat(MapleStat.MP, this.stats.getMp());
    }

    public void gainForce(int moboid, int forceColor, int skillid)
    {
        Skill effect = SkillFactory.getSkill(31110009);
        int maxFuryLevel = getSkillLevel(effect);
        this.specialStats.gainForceCounter();
        if (Randomizer.nextInt(100) >= 60)
        {
            addMP(forceColor, true);
        }
        getClient().getSession().write(MaplePacketCreator.showForce(this, moboid, this.specialStats.getForceCounter(), forceColor));
        if ((maxFuryLevel > 0) && ((skillid == 31000004) || (skillid == 31001006) || (skillid == 31001007) || (skillid == 31001008)))
        {
            this.specialStats.gainForceCounter();
            int rand = Randomizer.nextInt(100);
            if (rand >= 40)
            {
                addMP(forceColor, true);
                getClient().getSession().write(MaplePacketCreator.showForce(this, moboid, this.specialStats.getForceCounter(), 2));
            }
            else
            {
                getClient().getSession().write(MaplePacketCreator.showForce(this, moboid, this.specialStats.getForceCounter(), 3));
            }
        }
    }

    public int getCardStack()
    {
        return this.specialStats.getCardStack();
    }

    public void setCardStack(int amount)
    {
        this.specialStats.setCardStack(amount);
    }

    public int getCarteByJob()
    {
        if (getSkillLevel(20031210) > 0) return 40;
        if (getSkillLevel(20031209) > 0)
        {
            return 20;
        }
        return 0;
    }

    public void handleCarteGain(int oid)
    {
        int[] skillIds = {24120002, 24100003};
        for (int i : skillIds)
        {
            Skill skill = SkillFactory.getSkill(i);
            if (getSkillLevel(skill) > 0)
            {
                MapleStatEffect effect = skill.getEffect(getSkillLevel(skill));
                if ((!effect.makeChanceResult()) || (Randomizer.nextInt(100) > 50)) break;
                this.specialStats.gainForceCounter();
                getClient().getSession().write(MaplePacketCreator.gainCardStack(this, oid, skill.getId(), this.specialStats.getForceCounter(), skill.getId() == 24120002 ? 2 : 1));
                if (this.specialStats.getCardStack() >= getCarteByJob()) break;
                this.specialStats.gainCardStack();
                getClient().getSession().write(MaplePacketCreator.updateCardStack(this.specialStats.getCardStack()));
                break;
            }
        }
    }

    public void handleCardStack(int oid, int skillId)
    {
        Skill skill = SkillFactory.getSkill(skillId);
        if (getSkillLevel(skill) > 0)
        {
            MapleStatEffect effect = skill.getEffect(getSkillLevel(skill));
            if (effect != null)
            {
                this.specialStats.gainForceCounter();
                int color = skillId == 36001005 ? 6 : 2;
                getClient().getSession().write(MaplePacketCreator.gainCardStack(this, oid, skillId, this.specialStats.getForceCounter(), color, effect.getBulletCount()));
                this.specialStats.gainForceCounter(effect.getBulletCount() - 1);
            }
        }
    }

    public void handleAssassinStack(MapleMonster mob, int visProjectile)
    {
        Skill skill_2 = SkillFactory.getSkill(4100011);
        Skill skill_4 = SkillFactory.getSkill(4120018);

        MapleStatEffect effect;
        if (getSkillLevel(skill_4) > 0)
        {
            boolean isAssassin = false;
            effect = skill_4.getEffect(getTotalSkillLevel(skill_4));
        }
        else
        {
            if (getSkillLevel(skill_2) > 0)
            {
                boolean isAssassin = true;
                effect = skill_2.getEffect(getTotalSkillLevel(skill_2));
            }
            else
            {
                return;
            }
        }
        boolean isAssassin = false;
        if ((effect != null) && (effect.makeChanceResult()) && (mob != null))
        {
            int mobCount = effect.getMobCount();
            java.awt.Rectangle bounds = effect.calculateBoundingBox(mob.getTruePosition(), mob.isFacingLeft());
            List<MapleMapObject> affected = this.map.getMapObjectsInRect(bounds, java.util.Arrays.asList(server.maps.MapleMapObjectType.MONSTER));
            List<Integer> moboids = new ArrayList<>();
            for (MapleMapObject mo : affected)
            {
                if ((moboids.size() < mobCount) && (mo.getObjectId() != mob.getObjectId()))
                {
                    moboids.add(mo.getObjectId());
                }
            }
            this.specialStats.gainForceCounter();
            getClient().getSession().write(MaplePacketCreator.gainAssassinStack(getId(), mob.getObjectId(), this.specialStats.getForceCounter(), isAssassin, moboids, visProjectile,
                    mob.getTruePosition()));
            this.specialStats.gainForceCounter(moboids.size());
        }
    }

    public int get超越数值()
    {
        if (getBuffedValue(MapleBuffStat.恶魔超越) == null)
        {
            return 0;
        }
        return getBuffedValue(MapleBuffStat.恶魔超越);
    }

    public void handle超越状态(int skillId)
    {
        if ((skillId == 31011000) || (skillId == 31221000) || (skillId == 31201000) || (skillId == 31211000))
        {
            Skill skill = SkillFactory.getSkill(30010230);
            int skilllevel = getSkillLevel(skill);
            if (skilllevel > 0)
            {
                MapleStatEffect effect = skill.getEffect(skilllevel);
                if (getBuffedValue(MapleBuffStat.恶魔超越) == null)
                {
                    effect.apply超越状态(this);
                }
                else
                {
                    Integer combos = getBuffedValue(MapleBuffStat.恶魔超越);
                    if (combos < 20)
                    {
                        combos = combos.intValue() + 1;
                        if (combos >= 20)
                        {
                            combos = 20;
                        }
                        setBuffedValue(MapleBuffStat.恶魔超越, Integer.valueOf(combos));
                        getClient().getSession().write(BuffPacket.give恶魔复仇者超越(combos, 30010230));
                    }
                }
            }
        }
    }

    public int getDecorate()
    {
        return this.decorate;
    }

    public void setDecorate(int id)
    {
        if (((id >= 1012276) && (id <= 1012280)) || (id == 1012361) || (id == 1012363) || (id == 1012455) || (id == 1012456) || (id == 1012457) || (id == 1012458))
        {
            this.decorate = id;
        }
        else
        {
            this.decorate = 0;
        }
    }

    public boolean hasDecorate()
    {
        return (GameConstants.is恶魔职业(getJob())) || (GameConstants.is尖兵(getJob())) || (GameConstants.is林之灵(getJob()));
    }

    public void checkTailAndEar()
    {
        if (!GameConstants.is林之灵(getJob()))
        {
            return;
        }
        if (!this.questinfo.containsKey(59300))
        {
            updateInfoQuest(59300, "bTail=1;bEar=1;TailID=5010119;EarID=5010116", false);
        }
    }

    public void changeElfEar()
    {
        updateInfoQuest(7784, containsInfoQuest(7784, "sw=") ? "sw=1" : containsInfoQuest(7784, "sw=1") ? "sw=0" : "sw=1");
        if (containsInfoQuest(7784, GameConstants.is双弩精灵(getJob()) ? "sw=0" : "sw=1"))
        {
            getClient().getSession().write(MaplePacketCreator.showOwnJobChangedElf("Effect/BasicEff.img/JobChangedElf", 2, 5155000));
            getMap().broadcastMessage(this, MaplePacketCreator.showJobChangedElf(getId(), "Effect/BasicEff.img/JobChangedElf", 2, 5155000), false);
        }
        else
        {
            getClient().getSession().write(MaplePacketCreator.showOwnJobChangedElf("Effect/BasicEff.img/JobChanged", 2, 5155000));
            getMap().broadcastMessage(this, MaplePacketCreator.showJobChangedElf(getId(), "Effect/BasicEff.img/JobChanged", 2, 5155000), false);
        }
        equipChanged();
    }

    public boolean containsInfoQuest(int questid, String data)
    {
        if (this.questinfo.containsKey(questid))
        {
            return this.questinfo.get(questid).contains(data);
        }
        return false;
    }

    public boolean isElfEar()
    {
        if (containsInfoQuest(7784, "sw="))
        {
            return containsInfoQuest(7784, GameConstants.is双弩精灵(getJob()) ? "sw=0" : "sw=1");
        }
        return GameConstants.is双弩精灵(getJob());
    }

    public boolean isVip()
    {
        return this.vip > 0;
    }

    public void setVip(int vip)
    {
        if (vip >= 5)
        {
            this.vip = 5;
        }
        else if (vip < 0)
        {
            this.vip = 0;
        }
        else
        {
            this.vip = vip;
        }
    }

    public int getBossLog(String boss)
    {
        return getBossLog(boss, 0);
    }

    public int getBossLog(String boss, int type)
    {
        try
        {
            int count = 0;

            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM bosslog WHERE characterid = ? AND bossid = ?");
            ps.setInt(1, this.id);
            ps.setString(2, boss);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {


                count = rs.getInt("count");
                Timestamp bossTime = rs.getTimestamp("time");
                rs.close();
                ps.close();
                if (type == 0)
                {
                    Calendar sqlcal = Calendar.getInstance();
                    if (bossTime != null)
                    {
                        sqlcal.setTimeInMillis(bossTime.getTime());
                    }
                    if ((sqlcal.get(5) + 1 <= Calendar.getInstance().get(5)) || (sqlcal.get(2) + 1 <= Calendar.getInstance().get(2)) || (sqlcal.get(1) + 1 <= Calendar.getInstance().get(1)))
                    {
                        count = 0;
                        ps = con.prepareStatement("UPDATE bosslog SET count = 0, time = CURRENT_TIMESTAMP() WHERE characterid = ? AND bossid = ?");
                        ps.setInt(1, this.id);
                        ps.setString(2, boss);
                        ps.executeUpdate();
                    }
                }
            }
            else
            {
                PreparedStatement psu = con.prepareStatement("INSERT INTO bosslog (characterid, bossid, count, type) VALUES (?, ?, ?, ?)");
                psu.setInt(1, this.id);
                psu.setString(2, boss);
                psu.setInt(3, 0);
                psu.setInt(4, type);
                psu.executeUpdate();
                psu.close();
            }
            rs.close();
            ps.close();
            return count;
        }
        catch (Exception Ex)
        {
            log.error("获取BOSS挑战次数.", Ex);
        }
        return -1;
    }

    public void setBossLog(String boss)
    {
        setBossLog(boss, 0);
    }

    public void setBossLog(String boss, int type)
    {
        setBossLog(boss, type, 1);
    }

    public void setBossLog(String boss, int type, int count)
    {
        int bossCount = getBossLog(boss, type);
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE bosslog SET count = ?, type = ?, time = CURRENT_TIMESTAMP() WHERE characterid = ? AND bossid = ?");
            ps.setInt(1, bossCount + count);
            ps.setInt(2, type);
            ps.setInt(3, this.id);
            ps.setString(4, boss);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception Ex)
        {
            log.error("Error while set bosslog.", Ex);
        }
    }

    public void resetBossLog(String boss)
    {
        resetBossLog(boss, 0);
    }

    public void resetBossLog(String boss, int type)
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE bosslog SET count = ?, type = ?, time = CURRENT_TIMESTAMP() WHERE characterid = ? AND bossid = ?");
            ps.setInt(1, 0);
            ps.setInt(2, type);
            ps.setInt(3, this.id);
            ps.setString(4, boss);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception Ex)
        {
            log.error("重置BOSS次数失败.", Ex);
        }
    }

    public void updateCash()
    {
        this.client.getSession().write(MaplePacketCreator.showCharCash(this));
    }

    public short getSpace(int type)
    {
        return getInventory(MapleInventoryType.getByType((byte) type)).getNumFreeSlot();
    }

    public boolean haveSpace(int type)
    {
        short slot = getInventory(MapleInventoryType.getByType((byte) type)).getNextFreeSlot();
        return slot != -1;
    }

    public boolean haveSpaceForId(int itemid)
    {
        short slot = getInventory(ItemConstants.getInventoryType(itemid)).getNextFreeSlot();
        return slot != -1;
    }

    public boolean canHold()
    {
        for (int i = 1; i <= 5; i++)
        {
            if (getInventory(MapleInventoryType.getByType((byte) i)).getNextFreeSlot() <= -1)
            {
                return false;
            }
        }
        return true;
    }

    public boolean canHoldSlots(int slot)
    {
        for (int i = 1; i <= 5; i++)
        {
            if (getInventory(MapleInventoryType.getByType((byte) i)).isFull(slot))
            {
                return false;
            }
        }
        return true;
    }

    public boolean canHold(int itemid)
    {
        return getInventory(ItemConstants.getInventoryType(itemid)).getNextFreeSlot() > -1;
    }

    public int getMerchantMeso()
    {
        int mesos = 0;
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * from hiredmerch where characterid = ?");
            ps.setInt(1, this.id);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                mesos = rs.getInt("Mesos");
            }
            rs.close();
            ps.close();
        }
        catch (SQLException se)
        {
            log.error("获取雇佣商店金币发生错误", se);
        }
        return mesos;
    }

    public int addHyPay(int hypay)
    {
        int pay = getHyPay(1);
        int payUsed = getHyPay(2);
        int payReward = getHyPay(4);
        if (hypay > pay)
        {
            return -1;
        }
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE hypay SET pay = ? ,payUsed = ? ,payReward = ? where accname = ?");
            ps.setInt(1, pay - hypay);
            ps.setInt(2, payUsed + hypay);
            ps.setInt(3, payReward + hypay);
            ps.setString(4, getClient().getAccountName());
            ps.executeUpdate();
            ps.close();
            return 1;
        }
        catch (SQLException ex)
        {
            log.error("加减充值信息发生错误", ex);
        }
        return -1;
    }

    public int getHyPay(int type)
    {
        int pay = 0;
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from hypay where accname = ?");
            ps.setString(1, getClient().getAccountName());
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                if (type == 1)
                {
                    pay = rs.getInt("pay");
                }
                else if (type == 2)
                {
                    pay = rs.getInt("payUsed");
                }
                else if (type == 3)
                {
                    pay = rs.getInt("pay") + rs.getInt("payUsed");
                }
                else if (type == 4)
                {
                    pay = rs.getInt("payReward");
                }
                else
                {
                    pay = 0;
                }
            }
            else
            {
                PreparedStatement psu = con.prepareStatement("insert into hypay (accname, pay, payUsed, payReward) VALUES (?, ?, ?, ?)");
                psu.setString(1, getClient().getAccountName());
                psu.setInt(2, 0);
                psu.setInt(3, 0);
                psu.setInt(4, 0);
                psu.executeUpdate();
                psu.close();
            }
            ps.close();
            rs.close();
        }
        catch (SQLException ex)
        {
            log.error("获取充值信息发生错误", ex);
        }
        return pay;
    }

    public int delPayReward(int pay)
    {
        int payReward = getHyPay(4);
        if (pay <= 0)
        {
            return -1;
        }
        if (pay > payReward)
        {
            return -1;
        }
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE hypay SET payReward = ? where accname = ?");
            ps.setInt(1, payReward - pay);
            ps.setString(2, getClient().getAccountName());
            ps.executeUpdate();
            ps.close();
            return 1;
        }
        catch (SQLException ex)
        {
            log.error("加减消费奖励信息发生错误", ex);
        }
        return -1;
    }

    public void autoban(String reason, int greason)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(1), cal.get(2), cal.get(5) + 3, cal.get(11), cal.get(12));
        Timestamp TS = new Timestamp(cal.getTimeInMillis());
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE accounts SET banreason = ?, tempban = ?, greason = ? WHERE id = ?");
            ps.setString(1, reason);
            ps.setTimestamp(2, TS);
            ps.setInt(3, greason);
            ps.setInt(4, this.accountid);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException e)
        {
            log.error("Error while autoban" + e);
        }
    }

    public boolean isBanned()
    {
        return this.isbanned;
    }

    public void sendPolice(int greason, String reason, int duration)
    {
        this.isbanned = true;
        server.Timer.WorldTimer.getInstance().schedule(new Runnable()
        {

            public void run()
            {
                MapleCharacter.this.client.disconnect(true, false);
            }
        }, duration);
    }

    public void startCheck()
    {
        if (!this.client.hasCheck(getAccountID()))
        {
            log.info("[作弊] 检测到玩家 " + getName() + " 登录器关闭，系统对其进行断开连接处理。");
            sendPolice("检测到登录器关闭，游戏即将断开。");
        }
    }

    public void sendPolice(String text)
    {
        this.client.getSession().write(MaplePacketCreator.sendPolice(text));
        this.isbanned = true;
        server.Timer.WorldTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                MapleCharacter.this.client.disconnect(true, false);
                if (MapleCharacter.this.client.getSession().isConnected()) MapleCharacter.this.client.getSession().close(true);
            }
        }, 6000L);
    }

    public Timestamp getChrCreated()
    {
        if (this.createDate != null)
        {
            return this.createDate;
        }
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT createdate FROM characters WHERE id = ?");
            ps.setInt(1, getId());
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                return null;
            }
            this.createDate = rs.getTimestamp("createdate");
            rs.close();
            ps.close();
            return this.createDate;
        }
        catch (SQLException e)
        {
            throw new database.DatabaseException("获取角色创建日期出错", e);
        }
    }

    public boolean isInJailMap()
    {
        return (getMapId() == 180000001) && (getGMLevel() == 0);
    }

    public int getWarning()
    {
        return this.warning;
    }

    public void setWarning(int warning)
    {
        this.warning = warning;
    }

    public void gainWarning(boolean warningEnabled)
    {
        this.warning += 1;
        handling.world.WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                "[GM Message] 截至目前玩家: " + getName() + " (等级 " + getLevel() + ") 该用户已被警告: " + this.warning + " 次！"));
        if (warningEnabled == true)
        {
            if (this.warning == 1)
            {
                dropMessage(5, "这是你的第一次警告！请注意在游戏中勿使用非法程序！");
            }
            else if (this.warning == 2)
            {
                dropMessage(5, "警告现在是第 " + this.warning + " 次。如果你再得到一次警告就会封号处理！");
            }
            else if (this.warning >= 3)
            {
                ban(getName() + " 由于警告次数超过: " + this.warning + " 次，系统对其封号处理！", false, true, false);
                handling.world.WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(0, " 玩家 " + getName() + " (等级 " + getLevel() + ") 由于警告次数过多，系统对其封号处理！"));
            }
        }
    }

    public int getBeans()
    {
        return this.beans;
    }

    public void setBeans(int i)
    {
        this.beans = i;
    }

    public void gainBeans(int i, boolean show)
    {
        this.beans += i;
        if ((show) && (i != 0))
        {
            dropMessage(-1, "您" + (i > 0 ? "获得了 " : "消耗了 ") + Math.abs(i) + " 个豆豆.");
        }
    }

    public int teachSkill(int skillId, int toChrId)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("DELETE FROM skills WHERE skillid = ? AND teachId = ?");
            ps.setInt(1, skillId);
            ps.setInt(2, this.id);
            ps.executeUpdate();
            ps.close();
            ps = con.prepareStatement("SELECT * FROM skills WHERE skillid = ? AND characterid = ?");
            ps.setInt(1, skillId);
            ps.setInt(2, toChrId);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                PreparedStatement psskills = con.prepareStatement("INSERT INTO skills (characterid, skillid, skilllevel, masterlevel, expiration, teachId) VALUES (?, ?, ?, ?, ?, ?)");
                psskills.setInt(1, toChrId);
                psskills.setInt(2, skillId);
                psskills.setInt(3, 1);
                psskills.setByte(4, (byte) 1);
                psskills.setLong(5, -1L);
                psskills.setInt(6, this.id);
                psskills.executeUpdate();
                psskills.close();
                return 1;
            }
            rs.close();
            ps.close();
            return -1;
        }
        catch (Exception Ex)
        {
            log.error("传授技能失败.", Ex);
        }
        return -1;
    }

    public void startLieDetector(boolean isItem)
    {
        if (!getAntiMacro().inProgress())
        {
            getAntiMacro().startLieDetector(getName(), isItem, false);
        }
    }

    public int getDollars()
    {
        return this.dollars;
    }

    public int getShareLots()
    {
        return this.shareLots;
    }

    public void addDollars(int n)
    {
        this.dollars += n;
    }

    public void addShareLots(int n)
    {
        this.shareLots += n;
    }

    public int getAPS()
    {
        return this.apstorage;
    }

    public void gainAPS(int aps)
    {
        this.apstorage += aps;
    }

    public void doReborn(int type)
    {
        Map<MapleStat, Long> stat = new EnumMap(MapleStat.class);
        clearSkills();

        gainReborns(type);

        setLevel((short) 1);
        setExp(0L);
        setJob(0);
        setRemainingAp((short) 0);
        int str = 12;
        int dex = 5;
        int int_ = 4;
        int luk = 4;
        int total = getReborns() * 5 + getReborns1() * 10 + getReborns2() * 15 + getReborns3() * 30;
        this.stats.str = ((short) str);
        this.stats.dex = ((short) dex);
        this.stats.int_ = ((short) int_);
        this.stats.luk = ((short) luk);

        this.stats.setInfo(50, 50, 50, 50);
        setRemainingAp((short) total);
        this.stats.recalcLocalStats(this);
        stat.put(MapleStat.力量, (long) str);
        stat.put(MapleStat.敏捷, (long) dex);
        stat.put(MapleStat.智力, (long) int_);
        stat.put(MapleStat.运气, (long) luk);
        stat.put(MapleStat.AVAILABLEAP, (long) total);
        stat.put(MapleStat.MAXHP, 50L);
        stat.put(MapleStat.MAXMP, 50L);
        stat.put(MapleStat.HP, 50L);
        stat.put(MapleStat.MP, 50L);
        stat.put(MapleStat.等级, 1L);
        stat.put(MapleStat.职业, 0L);
        stat.put(MapleStat.经验, 0L);
        this.client.getSession().write(MaplePacketCreator.updatePlayerStats(stat, false, this));
    }

    public void clearSkills()
    {
        Map<Skill, SkillEntry> chrSkill = new HashMap(getSkills());
        Map<Skill, SkillEntry> newList = new HashMap<>();
        for (Map.Entry<Skill, SkillEntry> skill : chrSkill.entrySet())
        {
            newList.put(skill.getKey(), new SkillEntry(0, (byte) 0, -1L));
        }
        changeSkillsLevel(newList);
        newList.clear();
        chrSkill.clear();
    }

    public void gainReborns(int type)
    {
        if (type == 0)
        {
            this.reborns += 1;
        }
        else if (type == 1)
        {
            this.reborns1 += 1;
        }
        else if (type == 2)
        {
            this.reborns2 += 1;
        }
        else if (type == 3)
        {
            this.reborns3 += 1;
        }
    }

    public int getReborns()
    {
        return this.reborns;
    }

    public int getReborns1()
    {
        return this.reborns1;
    }

    public int getReborns2()
    {
        return this.reborns2;
    }

    public int getReborns3()
    {
        return this.reborns3;
    }

    public void changeSkillsLevel(Map<Skill, SkillEntry> skills)
    {
        if (skills.isEmpty())
        {
            return;
        }
        Map<Skill, SkillEntry> list = new HashMap<>();
        boolean hasRecovery = false;
        boolean recalculate = false;
        for (Map.Entry<Skill, SkillEntry> data : skills.entrySet())
        {
            if (changeSkillData(data.getKey(), data.getValue().skillevel, data.getValue().masterlevel, data.getValue().expiration))
            {
                list.put(data.getKey(), data.getValue());
                if (GameConstants.isRecoveryIncSkill(data.getKey().getId()))
                {
                    hasRecovery = true;
                }
                if (data.getKey().getId() < 80000000)
                {
                    recalculate = true;
                }
            }
        }
        if (list.isEmpty())
        {
            return;
        }
        this.client.getSession().write(MaplePacketCreator.updateSkills(list));
        reUpdateStat(hasRecovery, recalculate);
    }

    public boolean changeSkillData(Skill skill, int newLevel, byte newMasterlevel, long expiration)
    {
        if ((skill == null) || ((!GameConstants.isApplicableSkill(skill.getId())) && (!GameConstants.isApplicableSkill_(skill.getId()))))
        {
            return false;
        }
        if ((newLevel == 0) && (newMasterlevel == 0))
        {
            if (this.skills.containsKey(skill))
            {
                this.skills.remove(skill);
            }
            else
            {
                return false;
            }
        }
        else
        {
            this.skills.put(skill, new SkillEntry(newLevel, newMasterlevel, expiration, getSkillTeachId(skill), getSkillPosition(skill)));
        }
        return true;
    }

    public int 获取幻影装备技能(Skill skill)
    {
        if (skill == null)
        {
            return 0;
        }
        SkillEntry ret = this.skills.get(skill);
        if ((ret == null) || (ret.teachId == 0))
        {
            return 0;
        }
        return this.skills.get(SkillFactory.getSkill(ret.teachId)) != null ? ret.teachId : 0;
    }

    public void 修改幻影装备技能(int skillId, int teachId)
    {
        Skill skill = SkillFactory.getSkill(skillId);
        if (skill == null)
        {
            return;
        }
        SkillEntry ret = this.skills.get(skill);
        if (ret != null)
        {
            Skill theskill = SkillFactory.getSkill(ret.teachId);
            if ((theskill != null) && (theskill.isBuffSkill()))
            {
                cancelEffect(theskill.getEffect(1), false, -1L);
            }
            ret.teachId = teachId;
            this.changed_skills = true;
            this.client.getSession().write(MaplePacketCreator.修改幻影装备技能(skillId, teachId));
        }
    }

    public void 幻影删除技能(int skillId)
    {
        Skill skill = SkillFactory.getSkill(skillId);
        if (skill == null)
        {
            return;
        }
        SkillEntry ret = this.skills.get(skill);
        if (ret != null)
        {
            this.skills.remove(skill);
            this.client.getSession().write(MaplePacketCreator.幻影删除技能(ret.position));
            this.changed_skills = true;
        }
    }

    public void 幻影技能复制(int skillBook, int skillId, int level)
    {
        int skillLevel = 0;
        if (skillBook == 1)
        {
            skillLevel = getSkillLevel(24001001);
        }
        else if (skillBook == 2)
        {
            skillLevel = getSkillLevel(24101001);
        }
        else if (skillBook == 3)
        {
            skillLevel = getSkillLevel(24111001);
        }
        else if (skillBook == 4)
        {
            skillLevel = getSkillLevel(24121001);
        }
        Skill theskill = SkillFactory.getSkill(skillId);
        if (level > skillLevel)
        {
            skillLevel = level;
        }
        if (skillLevel > theskill.getMaxLevel())
        {
            skillLevel = theskill.getMaxLevel();
        }
        SkillEntry ret = this.skills.get(theskill);
        if (ret != null)
        {
            ret.skillevel = skillLevel;
            this.changed_skills = true;
            this.client.getSession().write(MaplePacketCreator.幻影复制技能(ret.position, skillId, skillLevel));
        }
        else if (skillBook == 1)
        {
            for (int i = 0; i < 4; i++)
            {
                if (幻影复制技能(i) == 0)
                {
                    this.skills.put(theskill, new SkillEntry(skillLevel, (byte) theskill.getMasterLevel(), -1L, 0, (byte) i));
                    this.changed_skills = true;
                    this.client.getSession().write(MaplePacketCreator.幻影复制技能(i, skillId, skillLevel));
                    break;
                }
            }
        }
        else if (skillBook == 2)
        {
            for (int i = 4; i < 8; i++)
            {
                if (幻影复制技能(i) == 0)
                {
                    this.skills.put(theskill, new SkillEntry(skillLevel, (byte) theskill.getMasterLevel(), -1L, 0, (byte) i));
                    this.changed_skills = true;
                    this.client.getSession().write(MaplePacketCreator.幻影复制技能(i, skillId, skillLevel));
                    break;
                }
            }
        }
        else if (skillBook == 3)
        {
            for (int i = 8; i < 11; i++)
            {
                if (幻影复制技能(i) == 0)
                {
                    this.skills.put(theskill, new SkillEntry(skillLevel, (byte) theskill.getMasterLevel(), -1L, 0, (byte) i));
                    this.changed_skills = true;
                    this.client.getSession().write(MaplePacketCreator.幻影复制技能(i, skillId, skillLevel));
                    break;
                }
            }
        }
        else if (skillBook == 4)
        {
            for (int i = 11; i < 13; i++)
            {
                if (幻影复制技能(i) == 0)
                {
                    this.skills.put(theskill, new SkillEntry(skillLevel, (byte) theskill.getMasterLevel(), -1L, 0, (byte) i));
                    this.changed_skills = true;
                    this.client.getSession().write(MaplePacketCreator.幻影复制技能(i, skillId, skillLevel));
                    break;
                }
            }
        }
    }

    public int 幻影复制技能(int position)
    {
        int skillId = 0;
        Map<Integer, Byte> skillsWithPos = getSkillsWithPos();
        for (Map.Entry<Integer, Byte> x : skillsWithPos.entrySet())
        {
            if (x.getValue() == position)
            {
                skillId = x.getKey();
                break;
            }
        }
        return skillId;
    }

    public Map<Integer, Byte> getSkillsWithPos()
    {
        Map<Skill, SkillEntry> chrskills = new HashMap(getSkills());
        Map<Integer, Byte> skillsWithPos = new LinkedHashMap<>();
        for (Map.Entry<Skill, SkillEntry> skill : chrskills.entrySet())
        {
            if ((skill.getValue().position >= 0) && (skill.getValue().position < 13))
            {
                skillsWithPos.put(skill.getKey().getId(), skill.getValue().position);
            }
        }
        return skillsWithPos;
    }

    public MapleCharacterCards getCharacterCard()
    {
        return this.characterCard;
    }

    public InnerSkillEntry[] getInnerSkills()
    {
        return this.innerSkills;
    }

    public int getInnerSkillSize()
    {
        int ret = 0;
        for (int i = 0; i < 3; i++)
        {
            if (this.innerSkills[i] != null)
            {
                ret++;
            }
        }
        return ret;
    }

    public int getInnerSkillIdByPos(int position)
    {
        if (this.innerSkills[position] != null)
        {
            return this.innerSkills[position].getSkillId();
        }
        return 0;
    }

    public void gainHonorExp(int amount)
    {
        if (amount <= 0)
        {
            return;
        }
        dropMessage(-9, "名声值 " + amount + " 获得了。");
        boolean levelup = false;
        if (getHonorExp() + amount >= getHonourNextExp())
        {
            this.honorLevel += 1;
            setHonorExp(0);
            levelup = true;
        }
        else
        {
            this.honorExp += amount;
        }
        this.client.getSession().write(MaplePacketCreator.updateInnerStats(this, levelup));
        this.client.getSession().write(MaplePacketCreator.updateSpecialStat("honorLeveling", 0, getHonorLevel(), true, getHonourNextExp()));
    }

    public int getHonorExp()
    {
        return this.honorExp;
    }

    public void setHonorExp(int exp)
    {
        this.honorExp = exp;
    }

    public int getHonourNextExp()
    {
        if (getHonorLevel() < 1)
        {
            setHonorLevel(1);
        }
        return getHonorLevel() * 500;
    }

    public int getHonorLevel()
    {
        return this.honorLevel;
    }

    public void setHonorLevel(int level)
    {
        this.honorLevel = level;
    }

    public void checkInnerSkill()
    {
        if ((this.level >= 30) && (this.innerSkills[0] == null))
        {
            changeInnerSkill(new InnerSkillEntry(70000015, 1, (byte) 1, (byte) 0));
        }
        if ((this.level >= 50) && (this.innerSkills[1] == null))
        {
            changeInnerSkill(new InnerSkillEntry(70000015, 3, (byte) 2, (byte) 0));
        }
        if ((this.level >= 70) && (this.innerSkills[2] == null))
        {
            changeInnerSkill(new InnerSkillEntry(70000015, 5, (byte) 3, (byte) 0));
        }
    }

    public void changeInnerSkill(InnerSkillEntry data)
    {
        changeInnerSkill(data.getSkillId(), data.getSkillLevel(), data.getPosition(), data.getRank());
    }

    public void changeInnerSkill(int skillId, int skillevel, byte position, byte rank)
    {
        Skill skill = SkillFactory.getSkill(skillId);
        if ((skill == null) || (!skill.isInnerSkill()) || (skillevel <= 0) || (position > 3) || (position < 1))
        {
            return;
        }
        if (skillevel > skill.getMaxLevel())
        {
            skillevel = skill.getMaxLevel();
        }
        InnerSkillEntry InnerSkill = new InnerSkillEntry(skillId, skillevel, position, rank);
        this.innerSkills[(position - 1)] = InnerSkill;
        this.client.getSession().write(MaplePacketCreator.updateInnerSkill(skillId, skillevel, position, rank));
        this.changed_innerSkills = true;
    }

    public void handleKillSpreeGain()
    {
        Skill skill = SkillFactory.getSkill(4221013);
        int skillLevel = getSkillLevel(skill);
        int killSpree = getBuffedIntValue(MapleBuffStat.击杀点数);
        if ((skillLevel <= 0) || (killSpree >= 5))
        {
            return;
        }
        if (Randomizer.nextInt(100) <= 20)
        {
            skill.getEffect(skillLevel).applyTo(this, true);
        }
    }

    public int getBuffedIntValue(MapleBuffStat stat)
    {
        if (stat.canStack())
        {
            int value = 0;
            for (Pair<MapleBuffStat, MapleBuffStatValueHolder> buffs : getAllEffects())
            {
                if (buffs.getLeft() == stat)
                {
                    value += buffs.getRight().value;
                }
            }
            return value;
        }
        MapleBuffStatValueHolder mbsvh = getBuffStatValueHolder(stat);
        if (mbsvh == null)
        {
            return 0;
        }
        return mbsvh.value;
    }

    public void handle元素雷电(int skillId)
    {
        if ((skillId == 15111022) || (skillId == 15121001) || (skillId == 15120003))
        {
            return;
        }
        Skill skill = SkillFactory.getSkill(15001022);
        int skillLevel = getSkillLevel(skill);
        if ((skillLevel <= 0) || (getBuffedValue(MapleBuffStat.元素属性) == null))
        {
            return;
        }
        if (Randomizer.nextInt(100) <= getStat().raidenPorp)
        {
            skill.getEffect(skillLevel).applyTo(this, true);
        }
    }

    public String getMedalText()
    {
        String medal = "";
        Item medalItem = getInventory(MapleInventoryType.EQUIPPED).getItem((short) -26);
        if (medalItem != null)
        {
            medal = "<" + MapleItemInformationProvider.getInstance().getName(medalItem.getItemId()) + "> ";
        }
        return medal;
    }

    public void gainGamePoints(int amount)
    {
        int gamePoints = getGamePoints() + amount;
        updateGamePoints(gamePoints);
    }

    public int getGamePoints()
    {
        try
        {
            int gamePoints = 0;
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts_info WHERE accId = ? AND worldId = ?");
            ps.setInt(1, getClient().getAccID());
            ps.setInt(2, getWorld());
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                gamePoints = rs.getInt("gamePoints");
                Timestamp updateTime = rs.getTimestamp("updateTime");
                Calendar sqlcal = Calendar.getInstance();
                if (updateTime != null)
                {
                    sqlcal.setTimeInMillis(updateTime.getTime());
                }
                if ((sqlcal.get(5) + 1 <= Calendar.getInstance().get(5)) || (sqlcal.get(2) + 1 <= Calendar.getInstance().get(2)) || (sqlcal.get(1) + 1 <= Calendar.getInstance().get(1)))
                {
                    gamePoints = 0;
                    PreparedStatement psu = con.prepareStatement("UPDATE accounts_info SET gamePoints = 0, updateTime = CURRENT_TIMESTAMP() WHERE accId = ? AND worldId = ?");
                    psu.setInt(1, getClient().getAccID());
                    psu.setInt(2, getWorld());
                    psu.executeUpdate();
                    psu.close();
                }
            }
            else
            {
                PreparedStatement psu = con.prepareStatement("INSERT INTO accounts_info (accId, worldId, gamePoints) VALUES (?, ?, ?)");
                psu.setInt(1, getClient().getAccID());
                psu.setInt(2, getWorld());
                psu.setInt(3, 0);
                psu.executeUpdate();
                psu.close();
            }
            rs.close();
            ps.close();
            return gamePoints;
        }
        catch (Exception Ex)
        {
            log.error("获取角色帐号的在线时间点出现错误 - 数据库查询失败", Ex);
        }
        return -1;
    }

    public void updateGamePoints(int amount)
    {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE accounts_info SET gamePoints = ?, updateTime = CURRENT_TIMESTAMP() WHERE accId = ? AND worldId = ?");
            ps.setInt(1, amount);
            ps.setInt(2, getClient().getAccID());
            ps.setInt(3, getWorld());
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception Ex)
        {
            log.error("更新角色帐号的在线时间出现错误 - 数据库更新失败.", Ex);
        }
    }

    public byte getWorld()
    {
        return this.world;
    }

    public void setWorld(byte world)
    {
        this.world = world;
    }

    public void resetGamePoints()
    {
        updateGamePoints(0);
    }

    public int getMaxHpForSever()
    {
        return ServerProperties.getMaxHp();
    }

    public int getMaxMpForSever()
    {
        return ServerProperties.getMaxMp();
    }

    public int getMaxDamageOver(int skillId)
    {
        int maxDamage = 999999;

        int limitBreak = getStat().getLimitBreak(this);
        int incMaxDamage = getStat().incMaxDamage;
        if (skillId != 0)
        {
            Skill skill = SkillFactory.getSkill(skillId);
            if (skill != null)
            {
                int skillMaxDamage = skill.getMaxDamageOver();
                if ((skillId == 4221014) && (getTotalSkillLevel(4220051) > 0))
                {
                    skillMaxDamage = 60000000;
                }
                return (limitBreak > skillMaxDamage ? limitBreak : skillMaxDamage) + incMaxDamage;
            }
        }
        return (limitBreak > maxDamage ? limitBreak : maxDamage) + incMaxDamage;
    }

    public int getTotalSkillLevel(int skillid)
    {
        return getTotalSkillLevel(SkillFactory.getSkill(skillid));
    }

    public void addTraitExp(int exp)
    {
        this.traits.get(MapleTraitType.craft).addExp(exp, this);
    }

    public void setTraitExp(int exp)
    {
        this.traits.get(MapleTraitType.craft).addExp(exp, this);
    }

    public int getLove()
    {
        return this.love;
    }

    public void setLove(int love)
    {
        this.love = love;
    }

    public void addLove(int loveChange)
    {
        this.love += loveChange;
        handling.world.messenger.MessengerRankingWorker.getInstance().updateRankFromPlayer(this);
    }

    public long getLastLoveTime()
    {
        return this.lastlovetime;
    }

    public Map<Integer, Long> getLoveCharacters()
    {
        return this.lastdayloveids;
    }

    public int canGiveLove(MapleCharacter from)
    {
        if ((from == null) || (this.lastdayloveids == null))
        {
            return 1;
        }
        if (this.lastdayloveids.containsKey(from.getId()))
        {
            long lastTime = this.lastdayloveids.get(from.getId());
            if (lastTime >= System.currentTimeMillis() - 86400000L)
            {
                return 2;
            }
            return 0;
        }

        return 0;
    }

    public void hasGiveLove(MapleCharacter to)
    {
        this.lastlovetime = System.currentTimeMillis();
        this.lastdayloveids.remove(to.getId());
        this.lastdayloveids.put(to.getId(), System.currentTimeMillis());
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("INSERT INTO lovelog (characterid, characterid_to) VALUES (?, ?)");
            ps.setInt(1, getId());
            ps.setInt(2, to.getId());
            ps.execute();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("ERROR writing lovelog for char " + getName() + " to " + to.getName() + e);
        }
    }

    public long getExpNeededForLevel()
    {
        return GameConstants.getExpNeededForLevel(this.level);
    }

    public boolean checkMaxStat()
    {
        if ((getGMLevel() > 0) || (getLevel() < 10))
        {
            return false;
        }
        return getPlayerStats() > getMaxStats(true) + 15;
    }

    public int getPlayerStats()
    {
        return getHpApUsed() + this.stats.getStr() + this.stats.getDex() + this.stats.getLuk() + this.stats.getInt() + getRemainingAp();
    }

    public int getMaxStats(boolean hpap)
    {
        int total = 29;

        if (!GameConstants.is新手职业(this.job))
        {
            int jobSp = 0;
            if (GameConstants.isSeparatedSpJob(this.job))
            {
                jobSp = GameConstants.getSkillBookByJob(this.job) * (GameConstants.is龙神(this.job) ? 3 : 5);
            }
            else
            {
                jobSp += (hpap ? 15 : 0);
            }
            total += jobSp;
        }

        if ((GameConstants.is炎术士(this.job)) || (GameConstants.is夜行者(this.job)))
        {
            if (this.level <= 70)
            {
                total += getLevel() * 6;
            }
            else
            {
                total += 420;
                total += (this.level - 70) * 5;
            }
        }
        else
        {
            total += this.level * 5;
        }

        if (hpap)
        {
            total += getHpApUsed();
        }
        else
        {
            total -= getHpApUsed();
        }
        return total;
    }

    public short getHpApUsed()
    {
        return this.hpApUsed;
    }

    public short getRemainingAp()
    {
        return this.remainingAp;
    }

    public void setRemainingAp(short remainingAp)
    {
        this.remainingAp = remainingAp;
    }

    public void setHpApUsed(short hpApUsed)
    {
        this.hpApUsed = hpApUsed;
    }

    public void resetStats(int str, int dex, int int_, int luk)
    {
        resetStats(str, dex, int_, luk, false);
    }

    public void resetStats(int str, int dex, int int_, int luk, boolean resetAll)
    {
        Map<MapleStat, Long> stat = new EnumMap(MapleStat.class);
        int total = this.stats.getStr() + this.stats.getDex() + this.stats.getLuk() + this.stats.getInt() + getRemainingAp();
        if (resetAll)
        {
            total = getMaxStats(false);
        }
        total -= str;
        this.stats.str = ((short) str);
        total -= dex;
        this.stats.dex = ((short) dex);
        total -= int_;
        this.stats.int_ = ((short) int_);
        total -= luk;
        this.stats.luk = ((short) luk);

        setRemainingAp((short) total);
        this.stats.recalcLocalStats(this);
        stat.put(MapleStat.力量, (long) str);
        stat.put(MapleStat.敏捷, (long) dex);
        stat.put(MapleStat.智力, (long) int_);
        stat.put(MapleStat.运气, (long) luk);
        stat.put(MapleStat.AVAILABLEAP, (long) total);

        this.client.getSession().write(MaplePacketCreator.updatePlayerStats(stat, false, this));
    }

    public void SpReset()
    {
        Map<Skill, SkillEntry> oldList = new HashMap(getSkills());
        Map<Skill, SkillEntry> newList = new HashMap<>();
        for (Map.Entry<Skill, SkillEntry> toRemove : oldList.entrySet())
        {
            if ((!toRemove.getKey().isBeginnerSkill()) && (!toRemove.getKey().isSpecialSkill()) && (!toRemove.getKey().isHyperSkill()))
            {
                int skillLevel = getSkillLevel(toRemove.getKey());
                if (skillLevel > 0)
                {
                    if (toRemove.getKey().canBeLearnedBy(getJob()))
                    {
                        newList.put(toRemove.getKey(), new SkillEntry(0, toRemove.getValue().masterlevel, toRemove.getValue().expiration));
                    }
                    else
                    {
                        newList.put(toRemove.getKey(), new SkillEntry(0, (byte) 0, -1L));
                    }
                }
            }
        }
        if (!newList.isEmpty())
        {
            changeSkillsLevel(newList);
        }
        oldList.clear();
        newList.clear();
        int[] spToGive;
        if (GameConstants.is龙神(getJob()))
        {
            spToGive = new int[10];
            if (getLevel() > 160)
            {
                spToGive[9] = ((getLevel() - 160) * 3 + 4);
                spToGive[8] = 124;
                spToGive[7] = 64;
                spToGive[6] = 64;
                spToGive[5] = 64;
                spToGive[4] = 34;
                spToGive[3] = 34;
                spToGive[2] = 34;
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 120) && (getLevel() <= 160))
            {
                spToGive[8] = ((getLevel() - 120) * 3 + 4);
                spToGive[7] = 64;
                spToGive[6] = 64;
                spToGive[5] = 64;
                spToGive[4] = 34;
                spToGive[3] = 34;
                spToGive[2] = 34;
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 100) && (getLevel() <= 120))
            {
                spToGive[7] = ((getLevel() - 100) * 3 + 4);
                spToGive[6] = 64;
                spToGive[5] = 64;
                spToGive[4] = 34;
                spToGive[3] = 34;
                spToGive[2] = 34;
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 80) && (getLevel() <= 100))
            {
                spToGive[6] = ((getLevel() - 80) * 3 + 4);
                spToGive[5] = 64;
                spToGive[4] = 34;
                spToGive[3] = 34;
                spToGive[2] = 34;
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 60) && (getLevel() <= 80))
            {
                spToGive[5] = ((getLevel() - 60) * 3 + 4);
                spToGive[4] = 34;
                spToGive[3] = 34;
                spToGive[2] = 34;
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 50) && (getLevel() <= 60))
            {
                spToGive[4] = ((getLevel() - 50) * 3 + 4);
                spToGive[3] = 34;
                spToGive[2] = 34;
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 40) && (getLevel() <= 50))
            {
                spToGive[3] = ((getLevel() - 40) * 3 + 4);
                spToGive[2] = 34;
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 30) && (getLevel() <= 40))
            {
                spToGive[2] = ((getLevel() - 30) * 3 + 4);
                spToGive[1] = 34;
                spToGive[0] = 34;
            }
            else if ((getLevel() > 20) && (getLevel() <= 30))
            {
                spToGive[1] = ((getLevel() - 20) * 3 + 4);
                spToGive[0] = 34;
            }
            else if ((getLevel() > 10) && (getLevel() <= 20))
            {
                spToGive[0] = ((getLevel() - 10) * 3 + 4);
            }
        }
        else if (GameConstants.is暗影双刀(getJob()))
        {
            spToGive = new int[6];
            if (getLevel() > 100)
            {
                spToGive[5] = ((getLevel() - 100) * 3 + 4);
                spToGive[4] = 100;
                spToGive[3] = 55;
                spToGive[2] = 65;
                spToGive[1] = 35;
                spToGive[0] = 35;
            }
            else if ((getLevel() > 70) && (getLevel() <= 100))
            {
                spToGive[4] = ((getLevel() - 70) * 3 + 4);
                spToGive[3] = 49;
                spToGive[2] = 49;
                spToGive[1] = 34;
                spToGive[0] = 35;
            }
            else if ((getLevel() > 55) && (getLevel() <= 70))
            {
                spToGive[3] = ((getLevel() - 55) * 3 + 4);
                spToGive[2] = 49;
                spToGive[1] = 34;
                spToGive[0] = 35;
            }
            else if ((getLevel() > 30) && (getLevel() <= 55))
            {
                spToGive[2] = ((getLevel() - 30) * 3 + 4);
                spToGive[1] = 34;
                spToGive[0] = 35;
            }
            else if ((getLevel() > 20) && (getLevel() <= 30))
            {
                spToGive[1] = ((getLevel() - 20) * 3 + 4);
                spToGive[0] = 35;
            }
            else if ((getLevel() > 10) && (getLevel() <= 20))
            {
                spToGive[0] = ((getLevel() - 10) * 3 + 5);
            }
        }
        else if (GameConstants.is神之子(getJob()))
        {
            spToGive = new int[2];
            if (getLevel() >= 100)
            {
                spToGive[0] = ((getLevel() - 100) * 3 + 8);
                spToGive[1] = ((getLevel() - 100) * 3 + 8);
            }
        }
        else if (GameConstants.isSeparatedSpJob(getJob()))
        {
            spToGive = new int[4];
            boolean isMagicJob = (getJob() >= 200) && (getJob() <= 232);
            if (getLevel() > 100)
            {
                spToGive[3] = ((getLevel() - 100) * 3 + 4);
                spToGive[2] = 124;
                spToGive[1] = 95;
                spToGive[0] = (65 + (isMagicJob ? 6 : 0));
            }
            else if ((getLevel() > 60) && (getLevel() <= 100))
            {
                spToGive[2] = ((getLevel() - 60) * 3 + 4);
                spToGive[1] = 95;
                spToGive[0] = (65 + (isMagicJob ? 6 : 0));
            }
            else if ((getLevel() > 30) && (getLevel() <= 60))
            {
                spToGive[1] = ((getLevel() - 30) * 3 + 5);
                spToGive[0] = (65 + (isMagicJob ? 6 : 0));
            }
            else if ((getLevel() > 10) && (getLevel() <= 30))
            {
                spToGive[0] = ((getLevel() - 10) * 3 + 5 + (isMagicJob ? 6 : 0));
            }
        }
        else
        {
            spToGive = new int[1];
            spToGive[0] += ((getJob() % 100 != 0) && (getJob() % 100 != 1) ? getJob() % 10 + 3 : 0);
            if (getJob() % 10 >= 2)
            {
                spToGive[0] += 3;
            }
            spToGive[0] += (getLevel() - 10) * 3;
        }
        for (int i = 0; i < spToGive.length; i++)
        {
            setRemainingSp(spToGive[i], i);
        }
        updateSingleStat(MapleStat.AVAILABLESP, 0L);
        this.client.getSession().write(MaplePacketCreator.enableActions());
    }

    public void setRemainingSp(int remainingSp, int skillbook)
    {
        this.remainingSp[skillbook] = remainingSp;
    }

    public int getPlayerPoints()
    {
        return this.playerPoints;
    }

    public void setPlayerPoints(int gain)
    {
        this.playerPoints = gain;
    }

    public void gainPlayerPoints(int gain)
    {
        if (this.playerPoints + gain < 0)
        {
            return;
        }
        this.playerPoints += gain;
    }

    public int getPlayerEnergy()
    {
        return this.playerEnergy;
    }

    public void setPlayerEnergy(int gain)
    {
        this.playerEnergy = gain;
    }

    public void gainPlayerEnergy(int gain)
    {
        if (this.playerEnergy + gain < 0)
        {
            return;
        }
        this.playerEnergy += gain;
    }

    public MaplePvpStats getPvpStats()
    {
        return this.pvpStats;
    }

    public int getPvpKills()
    {
        return this.pvpKills;
    }

    public void gainPvpKill()
    {
        this.pvpKills += 1;
        this.pvpVictory += 1;
        if (this.pvpVictory == 5)
        {
            this.map.broadcastMessage(MaplePacketCreator.spouseMessage(10, "[Pvp] 玩家 " + getName() + " 已经达到 5 连斩。"));
        }
        else if (this.pvpVictory == 10)
        {
            this.client.getChannelServer().broadcastMessage(MaplePacketCreator.spouseMessage(10, "[Pvp] 玩家 " + getName() + " 已经达到 10 连斩。"));
        }
        else if (this.pvpVictory >= 20)
        {
            handling.world.WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.spouseMessage(10,
                    "[Pvp] 玩家 " + getName() + " 已经达到 " + this.pvpVictory + " 连斩。他(她)在频道 " + this.client.getChannel() + " 地图 " + this.map.getMapName() + " 中喊道谁能赐我一死."));
        }
        else
        {
            dropMessage(6, "当前: " + this.pvpVictory + " 连斩.");
        }
    }

    public int getPvpDeaths()
    {
        return this.pvpDeaths;
    }

    public void gainPvpDeath()
    {
        this.pvpDeaths += 1;
        this.pvpVictory = 0;
    }

    public int getPvpVictory()
    {
        return this.pvpVictory;
    }

    public void dropTopMsg(String message)
    {
        this.client.getSession().write(UIPacket.getTopMsg(message));
    }

    public void dropMidMsg(String message)
    {
        this.client.getSession().write(UIPacket.clearMidMsg());
        this.client.getSession().write(UIPacket.getMidMsg(message, true, 0));
    }

    public void clearMidMsg()
    {
        this.client.getSession().write(UIPacket.clearMidMsg());
    }

    public int getEventCount(String eventId)
    {
        return getEventCount(eventId, 0);
    }

    public int getEventCount(String eventId, int type)
    {
        try
        {
            int count = 0;
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts_event WHERE accId = ? AND eventId = ?");
            ps.setInt(1, getClient().getAccID());
            ps.setString(2, eventId);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {


                count = rs.getInt("count");
                Timestamp updateTime = rs.getTimestamp("updateTime");
                if (type == 0)
                {
                    Calendar sqlcal = Calendar.getInstance();
                    if (updateTime != null)
                    {
                        sqlcal.setTimeInMillis(updateTime.getTime());
                    }
                    if ((sqlcal.get(5) + 1 <= Calendar.getInstance().get(5)) || (sqlcal.get(2) + 1 <= Calendar.getInstance().get(2)) || (sqlcal.get(1) + 1 <= Calendar.getInstance().get(1)))
                    {
                        count = 0;
                        PreparedStatement psu = con.prepareStatement("UPDATE accounts_event SET count = 0, updateTime = CURRENT_TIMESTAMP() WHERE accId = ? AND eventId = ?");
                        psu.setInt(1, getClient().getAccID());
                        psu.setString(2, eventId);
                        psu.executeUpdate();
                        psu.close();
                    }
                }
            }
            else
            {
                PreparedStatement psu = con.prepareStatement("INSERT INTO accounts_event (accId, eventId, count, type) VALUES (?, ?, ?, ?)");
                psu.setInt(1, getClient().getAccID());
                psu.setString(2, eventId);
                psu.setInt(3, 0);
                psu.setInt(4, type);
                psu.executeUpdate();
                psu.close();
            }
            rs.close();
            ps.close();
            return count;
        }
        catch (Exception Ex)
        {
            log.error("获取 EventCount 次数.", Ex);
        }
        return -1;
    }

    public void setEventCount(String eventId)
    {
        setEventCount(eventId, 0);
    }

    public void setEventCount(String eventId, int type)
    {
        setEventCount(eventId, type, 1);
    }

    public void setEventCount(String eventId, int type, int count)
    {
        int eventCount = getEventCount(eventId, type);
        try
        {
            PreparedStatement ps =
                    DatabaseConnection.getConnection().prepareStatement("UPDATE accounts_event SET count = ?, type = ?, updateTime = CURRENT_TIMESTAMP() WHERE accId = ? AND eventId " + "=" + " ?");
            ps.setInt(1, eventCount + count);
            ps.setInt(2, type);
            ps.setInt(3, getClient().getAccID());
            ps.setString(4, eventId);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception Ex)
        {
            log.error("增加 EventCount 次数失败.", Ex);
        }
    }

    public void resetEventCount(String eventId)
    {
        resetBossLog(eventId, 0);
    }

    public void resetEventCount(String eventId, int type)
    {
        try
        {
            PreparedStatement ps =
                    DatabaseConnection.getConnection().prepareStatement("UPDATE accounts_event SET count = 0, type = ?, updateTime = CURRENT_TIMESTAMP() WHERE accId = ? AND eventId " + "=" + " ?");
            ps.setInt(1, type);
            ps.setInt(2, getClient().getAccID());
            ps.setString(3, eventId);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception Ex)
        {
            log.error("重置 EventCount 次数失败.", Ex);
        }
    }

    public void checkBeastTamerSkill()
    {
        if ((this.job != 11212) || (this.level < 10))
        {
            return;
        }

        Map<Skill, SkillEntry> list = new HashMap<>();

        int[] skillIds = {110001500, 110001501, 110001502, 110001506, 110001510, 110000800};
        for (int i : skillIds)
        {
            Skill skil = SkillFactory.getSkill(i);
            if ((skil != null) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry(1, (byte) 1, -1L));
            }
        }

        Skill skil = SkillFactory.getSkill(110000515);
        if ((skil != null) && (this.level >= 30))
        {
            int oldskilllevel = getSkillLevel(skil);
            int newskilllevel = this.level / 10 - 2;
            if (newskilllevel > skil.getMaxLevel())
            {
                newskilllevel = skil.getMaxLevel();
            }
            if ((newskilllevel > 0) && (newskilllevel > oldskilllevel) && (oldskilllevel < skil.getMaxLevel()))
            {
                list.put(skil, new SkillEntry((byte) newskilllevel, (byte) skil.getMaxLevel(), -1L));
            }
        }

        if (this.level >= 120)
        {
            skil = SkillFactory.getSkill(110001511);
            if ((skil != null) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry((byte) skil.getMaxLevel(), (byte) skil.getMaxLevel(), -1L));
            }
        }

        if (this.level >= 150)
        {
            skil = SkillFactory.getSkill(110001512);
            if ((skil != null) && (getSkillLevel(skil) <= 0))
            {
                list.put(skil, new SkillEntry((byte) skil.getMaxLevel(), (byte) skil.getMaxLevel(), -1L));
            }
        }

        if (!list.isEmpty())
        {
            changeSkillsLevel(list);
        }
    }

    public int getBeastTamerSkillLevels(int skillId)
    {
        int ret = 0;
        int mod = skillId / 10000;
        if ((mod == 11200) || (mod == 11210) || (mod == 11211) || (mod == 11212))
        {
            Map<Skill, SkillEntry> chrSkills = new HashMap(getSkills());
            for (Map.Entry<Skill, SkillEntry> list : chrSkills.entrySet())
            {
                if ((list.getKey().getId() / 10000 == mod) && (!list.getKey().isLinkedAttackSkill()))
                {
                    ret += getSkillLevel(list.getKey());
                }
            }
        }
        return ret;
    }

    public void gainBeastTamerSkillExp(int amount)
    {
        if ((this.job != 11212) || (this.level < 60) || (amount < 0))
        {
            return;
        }
        Skill skil = SkillFactory.getSkill(110000513);
        if (skil == null)
        {
            return;
        }

        MapleQuestStatus levelStat = getQuestNAdd(MapleQuest.getInstance(59340));
        if (levelStat.getCustomData() == null)
        {
            levelStat.setCustomData("1");
        }
        if (levelStat.getStatus() != 1)
        {
            levelStat.setStatus((byte) 1);
        }

        MapleQuestStatus expStat = getQuestNAdd(MapleQuest.getInstance(59341));
        if (expStat.getCustomData() == null)
        {
            expStat.setCustomData("0");
        }
        if (expStat.getStatus() != 1)
        {
            expStat.setStatus((byte) 1);
        }
        int skillLevel = Integer.parseInt(levelStat.getCustomData());

        if (getSkillLevel(skil) <= skillLevel)
        {
            Map<Skill, SkillEntry> sDate = new HashMap<>();
            sDate.put(skil, new SkillEntry((byte) skillLevel, (byte) skil.getMaxLevel(), -1L));
            changeSkillLevel_Skip(sDate, false);
            updateQuest(levelStat, true);
            updateQuest(expStat, true);
            this.changed_skills = true;
            return;
        }
        int skillExp = Integer.parseInt(expStat.getCustomData());
        int needed = skil.getBonusExpInfo(skillLevel);
        int newExp = skillExp + amount;
        if (newExp >= needed)
        {
            if (skillLevel < skil.getMaxLevel())
            {
                int newLevel = skillLevel + 1;
                if (newLevel > skil.getMaxLevel())
                {
                    newLevel = skil.getMaxLevel();
                }
                Map<Skill, SkillEntry> sDate = new HashMap<>();
                sDate.put(skil, new SkillEntry((byte) newLevel, (byte) skil.getMaxLevel(), -1L));
                changeSkillLevel_Skip(sDate, false);
                levelStat.setCustomData(String.valueOf(newLevel));
                expStat.setCustomData("0");
                this.changed_skills = true;
            }
            else
            {
                levelStat.setCustomData("30");
                expStat.setCustomData("10000");
            }
        }
        else
        {
            expStat.setCustomData(String.valueOf(newExp));
        }
        updateQuest(levelStat, true);
        updateQuest(expStat, true);
    }

    public void fixTeachSkillLevel()
    {
        int skillId = 0;
        if (GameConstants.is火炮手(getJob()))
        {
            skillId = 110;
        }
        else if (GameConstants.is龙的传人(getJob()))
        {
            skillId = 1214;
        }
        else if (GameConstants.is双弩精灵(getJob()))
        {
            skillId = 20021110;
        }
        else if (GameConstants.is幻影(getJob()))
        {
            skillId = 20030204;
        }
        else if (GameConstants.is夜光(getJob()))
        {
            skillId = 20040218;
        }
        else if (GameConstants.is恶魔猎手(getJob()))
        {
            skillId = 30010112;
        }
        else if (GameConstants.is恶魔复仇者(getJob()))
        {
            skillId = 30010241;
        }
        else if (GameConstants.is尖兵(getJob()))
        {
            skillId = 30020233;
        }
        else if (GameConstants.is米哈尔(getJob()))
        {
            skillId = 50001214;
        }
        else if (GameConstants.is狂龙战士(getJob()))
        {
            skillId = 60000222;
        }
        else if (GameConstants.is爆莉萌天使(getJob()))
        {
            skillId = 60011219;
        }
        else if (GameConstants.is林之灵(getJob()))
        {
            skillId = 110000800;
        }

        if (skillId <= 0)
        {
            return;
        }
        Skill skill = SkillFactory.getSkill(skillId);

        if ((skill == null) || (skill.getMaxLevel() <= 1))
        {
            return;
        }

        if (skill.canBeLearnedBy(getJob()))
        {
            int oldlevel = getSkillLevel(skill);

            int newlevel = this.level >= 120 ? 2 : this.level >= 210 ? 3 : 1;

            int maxlevel = skill.getMaxLevel();

            if (newlevel > maxlevel)
            {
                newlevel = maxlevel;
            }

            if (newlevel != oldlevel)
            {
                Map<Skill, SkillEntry> sDate = new HashMap<>();
                sDate.put(skill, new SkillEntry((byte) newlevel, (byte) skill.getMaxLevel(), -1L));
                changeSkillsLevel(sDate);
            }
        }
    }

    public void startMapEffect(String msg, int itemId)
    {
        startMapEffect(msg, itemId, 30000);
    }

    public void startMapEffect(String msg, int itemId, int duration)
    {
        final MapleMapEffect mapEffect = new MapleMapEffect(msg, itemId);
        getClient().getSession().write(mapEffect.makeStartData());
        Timer.BuffTimer.getInstance().schedule(new Runnable()
        {
            @Override
            public void run()
            {
                getClient().getSession().write(mapEffect.makeDestroyData());
            }
        }, duration);
    }

    public void startMapEffect1(String msg, int itemId)
    {
        startMapEffect(msg, itemId, 20000);
    }

    public enum FameStatus
    {
        OK, NOT_TODAY, NOT_THIS_MONTH;

        FameStatus()
        {
        }
    }


}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleCharacter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */