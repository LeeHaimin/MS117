package client;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import constants.GameConstants;
import server.CharacterCardFactory;
import tools.Pair;
import tools.Triple;
import tools.data.output.MaplePacketLittleEndianWriter;


public class MapleCharacterCards
{
    private final List<Pair<Integer, Integer>> skills = new ArrayList<>();
    private Map<Integer, CardData> cards = new LinkedHashMap<>();

    public Map<Integer, CardData> getCards()
    {
        return this.cards;
    }

    public void setCards(Map<Integer, CardData> cads)
    {
        this.cards = cads;
    }

    public List<Pair<Integer, Integer>> getCardEffects()
    {
        return this.skills;
    }

    public void recalcLocalStats(MapleCharacter chr)
    {
        int pos = -1;
        for (Map.Entry<Integer, CardData> x : this.cards.entrySet())
        {
            if (x.getValue().chrId == chr.getId())
            {
                pos = x.getKey();
                break;
            }
        }
        if (pos != -1)
        {
            if (!CharacterCardFactory.getInstance().canHaveCard(chr.getLevel(), chr.getJob()))
            {
                this.cards.remove(pos);
            }
            else
            {
                this.cards.put(pos, new CardData(chr.getId(), chr.getLevel(), chr.getJob()));
            }
        }
        calculateEffects();
    }

    public void calculateEffects()
    {
        this.skills.clear();
        int deck1amount = 0;
        int deck2amount = 0;
        int deck3amount = 0;
        int reqRank1 = 0;
        int reqRank2 = 0;
        int reqRank3 = 0;
        List<Integer> cardSkillIds1 = new LinkedList<>();
        List<Integer> cardSkillIds2 = new LinkedList<>();
        List<Integer> cardSkillIds3 = new LinkedList<>();
        CharacterCardFactory cardFactory = CharacterCardFactory.getInstance();
        for (Map.Entry<Integer, CardData> integerCardDataEntry : this.cards.entrySet())
        {
            Map.Entry cardInfo = integerCardDataEntry;
            if (((CardData) cardInfo.getValue()).chrId > 0)
            {
                Triple<Integer, Integer, Integer> skillData = cardFactory.getCardSkill(((CardData) cardInfo.getValue()).job, ((CardData) cardInfo.getValue()).level);
                if ((Integer) cardInfo.getKey() < 4)
                {
                    if (skillData != null)
                    {
                        cardSkillIds1.add(skillData.getLeft());
                        this.skills.add(new Pair(skillData.getMid(), skillData.getRight()));
                    }
                    deck1amount++;
                    if ((reqRank1 == 0) || (reqRank1 > ((CardData) cardInfo.getValue()).level))
                    {
                        reqRank1 = ((CardData) cardInfo.getValue()).level;
                    }
                }
                else if (((Integer) cardInfo.getKey() > 3) && ((Integer) cardInfo.getKey() < 7))
                {
                    if (skillData != null)
                    {
                        cardSkillIds2.add(skillData.getLeft());
                        this.skills.add(new Pair(skillData.getMid(), skillData.getRight()));
                    }
                    deck2amount++;
                    if ((reqRank2 == 0) || (reqRank2 > ((CardData) cardInfo.getValue()).level))
                    {
                        reqRank2 = ((CardData) cardInfo.getValue()).level;
                    }
                }
                else
                {
                    if (skillData != null)
                    {
                        cardSkillIds3.add(skillData.getLeft());
                        this.skills.add(new Pair(skillData.getMid(), skillData.getRight()));
                    }
                    deck3amount++;
                    if ((reqRank3 == 0) || (reqRank3 > ((CardData) cardInfo.getValue()).level))
                    {
                        reqRank3 = ((CardData) cardInfo.getValue()).level;
                    }
                }
            }
        }
        if ((deck1amount == 3) && (cardSkillIds1.size() == 3))
        {
            List<Integer> uid = cardFactory.getUniqueSkills(cardSkillIds1);
            for (int i : uid)
            {
                this.skills.add(new Pair(i, GameConstants.getCardSkillLevel(reqRank1)));
            }
            this.skills.add(new Pair(cardFactory.getRankSkill(reqRank1), 1));
        }
        if ((deck2amount == 3) && (cardSkillIds2.size() == 3))
        {
            List<Integer> uid = cardFactory.getUniqueSkills(cardSkillIds2);
            for (int i : uid)
            {
                this.skills.add(new Pair(i, GameConstants.getCardSkillLevel(reqRank2)));
            }
            this.skills.add(new Pair(cardFactory.getRankSkill(reqRank2), 1));
        }
        if ((deck3amount == 3) && (cardSkillIds3.size() == 3))
        {
            List<Integer> uid = cardFactory.getUniqueSkills(cardSkillIds3);
            for (Integer integer : uid)
            {
                int i = integer;
                this.skills.add(new Pair(i, GameConstants.getCardSkillLevel(reqRank3)));
            }
            this.skills.add(new Pair(cardFactory.getRankSkill(reqRank3), 1));
        }
    }

    public void loadCards(MapleClient c, boolean channelserver) throws SQLException
    {
        this.cards = CharacterCardFactory.getInstance().loadCharacterCards(c.getAccID(), c.getWorld());
        if (channelserver)
        {
            calculateEffects();
        }
    }

    public void connectData(MaplePacketLittleEndianWriter mplew)
    {
        if (this.cards.isEmpty())
        {
            mplew.writeZeroBytes(81);
            return;
        }
        int poss = 0;
        for (CardData i : this.cards.values())
        {
            poss++;
            if (poss > 9)
            {
                return;
            }
            mplew.writeInt(i.chrId);
            mplew.write(i.level);
            mplew.writeInt(i.job);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleCharacterCards.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */