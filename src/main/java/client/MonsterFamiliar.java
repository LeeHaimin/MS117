package client;

import java.util.List;

import server.MapleItemInformationProvider;
import server.life.MapleLifeFactory;
import server.life.MapleMonsterStats;
import server.maps.MapleMapObjectType;
import server.movement.AbsoluteLifeMovement;
import server.movement.LifeMovementFragment;
import tools.MaplePacketCreator;
import tools.data.output.MaplePacketLittleEndianWriter;
import tools.packet.PacketHelper;

public final class MonsterFamiliar extends server.maps.AnimatedMapleMapObject implements java.io.Serializable
{
    private static final long serialVersionUID = 795419937713738569L;
    private final int id;
    private final int familiar;
    private final int characterid;
    private int fatigue;
    private String name;
    private long expiry;
    private short fh = 0;
    private byte vitality;

    public MonsterFamiliar(int characterid, int id, int familiar, long expiry, String name, int fatigue, byte vitality)
    {
        this.familiar = familiar;
        this.characterid = characterid;
        this.expiry = expiry;
        this.vitality = vitality;
        this.id = id;
        this.name = name;
        this.fatigue = fatigue;
        setStance(0);
        setPosition(new java.awt.Point(0, 0));
    }

    public MonsterFamiliar(int characterid, int familiar, long expiry)
    {
        this.familiar = familiar;
        this.characterid = characterid;
        this.expiry = expiry;
        this.fatigue = 0;
        this.vitality = 1;
        this.name = getOriginalName();
        this.id = server.Randomizer.nextInt();
    }

    public String getOriginalName()
    {
        return getOriginalStats().getName();
    }

    public MapleMonsterStats getOriginalStats()
    {
        return MapleLifeFactory.getMonsterStats(MapleItemInformationProvider.getInstance().getFamiliar(this.familiar).mob);
    }

    public void addFatigue(MapleCharacter owner)
    {
        addFatigue(owner, 1);
    }

    public void addFatigue(MapleCharacter owner, int f)
    {
        this.fatigue = Math.min(this.vitality * 300, Math.max(0, this.fatigue + f));
        owner.getClient().getSession().write(MaplePacketCreator.updateFamiliar(this));
        if (this.fatigue >= this.vitality * 300)
        {
            owner.removeFamiliar();
        }
    }

    public int getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String n)
    {
        this.name = n;
    }

    public short getFh()
    {
        return this.fh;
    }

    public void setFh(int f)
    {
        this.fh = ((short) f);
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.FAMILIAR;
    }

    public void sendSpawnData(MapleClient client)
    {
        client.getSession().write(MaplePacketCreator.spawnFamiliar(this, true));
    }

    public void sendDestroyData(MapleClient client)
    {
        client.getSession().write(MaplePacketCreator.spawnFamiliar(this, false));
    }

    public void updatePosition(List<LifeMovementFragment> movement)
    {
        for (LifeMovementFragment move : movement)
        {
            if (((move instanceof AbsoluteLifeMovement)))
            {
                setFh(((AbsoluteLifeMovement) move).getNewFH());
            }
        }
    }

    public void writeRegisterPacket(MaplePacketLittleEndianWriter mplew, boolean chr)
    {
        mplew.writeInt(getCharacterId());
        mplew.writeInt(getFamiliar());
        mplew.writeZeroBytes(13);
        mplew.write(chr ? 1 : 0);
        mplew.writeShort(getVitality());
        mplew.writeInt(getFatigue());
        mplew.writeLong(PacketHelper.getTime(getVitality() >= 3 ? System.currentTimeMillis() : -2L));
        mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        mplew.writeLong(PacketHelper.getTime(getExpiry()));
        mplew.writeShort(getVitality());
    }

    public int getCharacterId()
    {
        return this.characterid;
    }

    public int getFamiliar()
    {
        return this.familiar;
    }

    public byte getVitality()
    {
        return this.vitality;
    }

    public int getFatigue()
    {
        return this.fatigue;
    }

    public long getExpiry()
    {
        return this.expiry;
    }

    public void setExpiry(long e)
    {
        this.expiry = e;
    }

    public void setFatigue(int f)
    {
        this.fatigue = f;
    }

    public void setVitality(int v)
    {
        this.vitality = ((byte) v);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MonsterFamiliar.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */