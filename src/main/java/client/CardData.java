package client;

import java.io.Serializable;


public class CardData implements Serializable
{
    private static final long serialVersionUID = 2550550428979893978L;
    public final int chrId;
    public final short job;
    public final short level;

    public CardData(int cid, short level, short job)
    {
        this.chrId = cid;
        this.level = level;
        this.job = job;
    }

    public String toString()
    {
        return "角色ID: " + this.chrId + " 职业ID: " + this.job + " 等级: " + this.level;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\CardData.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */