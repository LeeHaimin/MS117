package client;

import java.io.Serializable;


public class InnerSkillEntry implements Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private final int skillId;
    private final int skillevel;
    private final byte position;
    private final byte rank;

    public InnerSkillEntry(int skillId, int skillevel, byte position, byte rank)
    {
        this.skillId = skillId;
        this.skillevel = skillevel;
        this.position = position;
        this.rank = rank;
    }

    public int getSkillId()
    {
        return this.skillId;
    }

    public int getSkillLevel()
    {
        return this.skillevel;
    }

    public byte getPosition()
    {
        return this.position;
    }

    public byte getRank()
    {
        return this.rank;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\InnerSkillEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */