package client.inventory;


public enum MapleInventoryType
{
    UNDEFINED(0), EQUIP(1), USE(2), SETUP(3), ETC(4), CASH(5), EQUIPPED(-1);

    final byte type;

    MapleInventoryType(int type)
    {
        this.type = ((byte) type);
    }

    public static MapleInventoryType getByType(byte type)
    {
        for (MapleInventoryType l : values())
        {
            if (l.getType() == type)
            {
                return l;
            }
        }
        return null;
    }

    public byte getType()
    {
        return this.type;
    }

    public static MapleInventoryType getByWZName(String name)
    {
        if (name.equals("Install")) return SETUP;
        if (name.equals("Consume")) return USE;
        if (name.equals("Etc")) return ETC;
        if (name.equals("Eqp")) return EQUIP;
        if (name.equals("Cash")) return CASH;
        if (name.equals("Pet"))
        {
            return CASH;
        }
        return UNDEFINED;
    }

    public short getBitfieldEncoding()
    {
        return (short) (2 << this.type);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\MapleInventoryType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */