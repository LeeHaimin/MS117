package client.inventory;

public enum ItemFlag
{
    LOCK(1), 鞋子防滑(2), 披风防寒(4), UNTRADEABLE(8), KARMA_EQ(16), KARMA_USE(2), CHARM_EQUIPPED(32), ANDROID_ACTIVATED(64), CRAFTED(128), CRAFTED_USE(16), 装备防爆(256), 幸运卷轴(512), KARMA_ACC_USE(1024),
    KARMA_ACC(4096), 保护升级次数(8192), 卷轴防护(16384);

    private final int i;

    ItemFlag(int i)
    {
        this.i = i;
    }

    public int getValue()
    {
        return this.i;
    }

    public boolean check(int flag)
    {
        return (flag & this.i) == this.i;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\ItemFlag.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */