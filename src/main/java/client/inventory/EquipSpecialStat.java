package client.inventory;


public enum EquipSpecialStat
{
    总伤害(1), 全属性(2), 剪刀次数(4), UNK8(8), UNK10(16);

    private final int value;

    EquipSpecialStat(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\EquipSpecialStat.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */