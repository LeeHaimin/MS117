package client.inventory;

public enum EquipAdditions
{
    elemboost("elemVol", "elemVol", true), mobcategory("category", "damage"), critical("prob", "damage"), boss("prob", "damage"), mobdie("hpIncOnMobDie", "mpIncOnMobDie"), hpmpchange(
            "hpChangerPerTime", "mpChangerPerTime"), skill("id", "level");

    private final String i1;
    private final String i2;
    private final boolean element;

    EquipAdditions(String i1, String i2)
    {
        this.i1 = i1;
        this.i2 = i2;
        this.element = false;
    }

    EquipAdditions(String i1, String i2, boolean element)
    {
        this.i1 = i1;
        this.i2 = i2;
        this.element = element;
    }

    public static EquipAdditions fromString(String str)
    {
        for (EquipAdditions s : values())
        {
            if (s.name().equalsIgnoreCase(str))
            {
                return s;
            }
        }
        return null;
    }

    public String getValue1()
    {
        return this.i1;
    }

    public String getValue2()
    {
        return this.i2;
    }

    public boolean isElement()
    {
        return this.element;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\EquipAdditions.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */