package client.inventory;


public enum EnhanctBuff
{
    UPGRADE_TIER(1), NO_DESTROY(2), SCROLL_SUCCESS(4);

    private final int value;

    EnhanctBuff(int value)
    {
        this.value = value;
    }

    public byte getValue()
    {
        return (byte) this.value;
    }

    public boolean check(int flag)
    {
        return (flag & this.value) != 0;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\EnhanctBuff.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */