package client.inventory;

public enum MapleWeaponType
{
    没有武器(1.43F, 20, 0), 双头杖(1.2F, 25, 21), 灵魂手铳(1.7F, 25, 22), 亡命剑(1.43F, 25, 23), 能量剑(1.5F, 25, 24), 驯兽魔法棒(1.34F, 25, 25), 单手剑(1.2F, 20, 30), 单手斧(1.2F, 20, 31), 单手钝器(1.2F, 20, 32), 短刀(1.3F, 20,
        33), 双刀副手(1.3F, 20, 34), 特殊副手(2.0F, 15, 35), 手杖(1.3F, 15, 36), 短杖(1.2F, 25, 37), 长杖(1.2F, 25, 38), 双手斧(1.34F, 20, 40), 双手剑(1.34F, 20, 41), 双手钝器(1.34F, 20, 42), 枪(1.49F, 20, 43), 矛(1.49F, 20
        , 44), 弓(1.3F, 15, 45), 弩(1.35F, 15, 46), 拳套(1.75F, 15, 47), 指节(1.7F, 20, 48), 短枪(1.5F, 15, 49), 双弩枪(1.3F, 15, 52), 手持火炮(1.35F, 15, 53), 大剑(1.49F, 15, 56), 太刀(1.34F, 15, 57);

    private final float damageMultiplier;
    private final int baseMastery;
    private final int weaponType;

    MapleWeaponType(float maxDamageMultiplier, int baseMastery, int weaponType)
    {
        this.damageMultiplier = maxDamageMultiplier;
        this.baseMastery = baseMastery;
        this.weaponType = weaponType;
    }

    public float getMaxDamageMultiplier()
    {
        return this.damageMultiplier;
    }


    public int getBaseMastery()
    {
        return this.baseMastery;
    }

    public int getWeaponType()
    {
        return this.weaponType;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\MapleWeaponType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */