package client.inventory;


public enum ImpFlag
{
    REMOVED(1), SUMMONED(2), TYPE(4), STATE(8), FULLNESS(16), CLOSENESS(32), CLOSENESS_LEFT(64), MINUTES_LEFT(128), LEVEL(256), FULLNESS_2(512), UPDATE_TIME(1024), CREATE_TIME(2048),
    AWAKE_TIME(4096), SLEEP_TIME(8192), MAX_CLOSENESS(16384), MAX_DELAY(32768), MAX_FULLNESS(65536), MAX_ALIVE(131072), MAX_MINUTES(262144);

    private final int i;

    ImpFlag(int i)
    {
        this.i = i;
    }

    public int getValue()
    {
        return this.i;
    }

    public boolean check(int flag)
    {
        return (flag & this.i) == this.i;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\ImpFlag.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */