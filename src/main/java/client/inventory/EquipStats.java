package client.inventory;


public enum EquipStats
{
    可升级次数(1), 已升级次数(2), 力量(4), 敏捷(8), 智力(16), 运气(32), Hp(64), Mp(128), 物攻(256), 魔攻(512), 物防(1024), 魔防(2048), 命中(4096), 回避(8192), 手技(16384), 速度(32768), 跳跃(65536), 状态(131072), 技能(262144),
    道具等级(524288), 道具经验(1048576), 耐久度(2097152), 金锤子(4194304), 大乱斗攻击力(8388608), DOWNLEVEL(16777216), ENHANCT_BUFF(33554432), DURABILITY_SPECIAL(67108864), REQUIRED_LEVEL(134217728),
    YGGDRASIL_WISDOM(268435456), FINAL_STRIKE(536870912), BOSS伤害(1073741824), 无视防御(Integer.MIN_VALUE);

    private final int value;

    EquipStats(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\EquipStats.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */