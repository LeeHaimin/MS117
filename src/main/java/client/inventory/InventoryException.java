package client.inventory;


public class InventoryException extends RuntimeException
{
    private static final long serialVersionUID = 1L;


    public InventoryException()
    {
    }


    public InventoryException(String msg)
    {
        super(msg);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\InventoryException.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */