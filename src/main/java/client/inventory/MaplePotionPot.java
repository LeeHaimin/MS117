package client.inventory;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import database.DatabaseConnection;


public class MaplePotionPot
{
    private final int chrId;
    private final int itemId;
    private final int maxlimit = 10000000;
    private final long startTime;
    private final long endTime;
    private int hp;
    private int mp;
    private int maxValue;

    public MaplePotionPot(int chrId, int itemId, int hp, int mp, int maxValue, long start, long end)
    {
        this.chrId = chrId;
        this.itemId = itemId;
        this.hp = hp;
        this.mp = mp;
        this.maxValue = maxValue;
        this.startTime = start;
        this.endTime = end;
    }


    public static MaplePotionPot createPotionPot(int chrId, int itemId, long endTime)
    {
        if (itemId != 5820000)
        {
            return null;
        }
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("INSERT INTO `character_potionpots` (`characterid`, `itemId`, `hp`, `mp`, `maxValue`, `startDate`, `endDate`) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, chrId);
            ps.setInt(2, itemId);
            ps.setInt(3, 0);
            ps.setInt(4, 0);
            ps.setInt(5, 1000000);
            ps.setLong(6, System.currentTimeMillis());
            ps.setLong(7, endTime);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException ex)
        {
            System.err.println("创建药剂罐信息出错 " + ex);
            return null;
        }
        MaplePotionPot ret = new MaplePotionPot(chrId, itemId, 0, 0, 1000000, System.currentTimeMillis(), endTime);
        return ret;
    }


    public void saveToDb()
    {
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE `character_potionpots` SET `hp` = ?, `mp` = ?, `maxValue` = ? WHERE `characterid` = ?");
            ps.setInt(1, this.hp);
            ps.setInt(2, this.mp);
            ps.setInt(3, this.maxValue);
            ps.setInt(4, this.chrId);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException ex)
        {
            System.err.println("保存药剂罐信息出错" + ex);
        }
    }


    public int getChrId()
    {
        return this.chrId;
    }


    public int getItmeId()
    {
        return this.itemId;
    }

    public void addHp(int value)
    {
        if (value <= 0)
        {
            return;
        }
        this.hp += value;
        if (this.hp > this.maxValue)
        {
            this.hp = this.maxValue;
        }
    }

    public void addMp(int value)
    {
        if (value <= 0)
        {
            return;
        }
        this.mp += value;
        if (this.mp > this.maxValue)
        {
            this.mp = this.maxValue;
        }
    }

    public boolean addMaxValue()
    {
        if (this.maxValue + 1000000 > this.maxlimit)
        {
            return false;
        }
        this.maxValue += 1000000;
        return true;
    }

    public long getStartDate()
    {
        return this.startTime;
    }

    public long getEndDate()
    {
        return this.endTime;
    }

    public boolean isFull(int addHp, int addMp)
    {
        if ((addHp > 0) && (addMp > 0)) return (isFullHp()) && (isFullMp());
        if ((addHp > 0) && (addMp == 0)) return isFullHp();
        if ((addHp == 0) && (addMp >= 0))
        {
            return isFullMp();
        }
        return true;
    }

    public boolean isFullHp()
    {
        return getHp() >= getMaxValue();
    }

    public boolean isFullMp()
    {
        return getMp() >= getMaxValue();
    }

    public int getHp()
    {
        if (this.hp < 0)
        {
            this.hp = 0;
        }
        else if (this.hp > this.maxValue)
        {
            this.hp = this.maxValue;
        }
        return this.hp;
    }

    public void setHp(int value)
    {
        this.hp = value;
    }

    public int getMaxValue()
    {
        if (this.maxValue > this.maxlimit)
        {
            this.maxValue = this.maxlimit;
        }
        return this.maxValue;
    }

    public int getMp()
    {
        if (this.mp < 0)
        {
            this.mp = 0;
        }
        else if (this.mp > this.maxValue)
        {
            this.mp = this.maxValue;
        }
        return this.mp;
    }

    public void setMp(int value)
    {
        this.mp = value;
    }

    public void setMaxValue(int value)
    {
        this.maxValue = value;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\MaplePotionPot.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */