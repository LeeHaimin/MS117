package client.inventory;

import java.io.Serializable;

import constants.GameConstants;
import constants.ItemConstants;
import server.MapleItemInformationProvider;
import server.Randomizer;

public class Equip extends Item implements Serializable
{
    public static final int ARMOR_RATIO = 350000;
    public static final int WEAPON_RATIO = 700000;
    private byte upgradeSlots = 0;
    private byte level = 0;
    private byte vicioushammer = 0;
    private byte state = 0;
    private byte enhance = 0;
    private short enhanctBuff = 0;
    private short reqLevel = 0;
    private short yggdrasilWisdom = 0;
    private short bossDamage = 0;
    private short ignorePDR = 0;
    private short totalDamage = 0;
    private short allStat = 0;
    private short karmaCount = -1;
    private boolean finalStrike = false;
    private short str = 0;
    private short dex = 0;
    private short _int = 0;
    private short luk = 0;
    private short hp = 0;
    private short mp = 0;
    private short watk = 0;
    private short matk = 0;
    private short wdef = 0;
    private short mdef = 0;
    private short acc = 0;
    private short avoid = 0;
    private short hands = 0;
    private short speed = 0;
    private short jump = 0;
    private short charmExp = 0;
    private short pvpDamage = 0;
    private int itemEXP = 0;
    private int durability = -1;
    private int incSkill = -1;
    private int statemsg = 0;
    private int potential1 = 0;
    private int potential2 = 0;
    private int potential3 = 0;
    private int potential4 = 0;
    private int potential5 = 0;
    private int potential6 = 0;
    private int socket1 = -1;
    private int socket2 = -1;
    private int socket3 = -1;
    private int itemSkin = 0;
    private int limitBreak = 0;
    private MapleRing ring = null;
    private MapleAndroid android = null;

    public Equip(int id, short position, byte flag)
    {
        super(id, position, (short) 1, flag);
    }

    public Equip(int id, short position, int uniqueid, short flag)
    {
        super(id, position, (short) 1, flag, uniqueid);
    }

    public Item copy()
    {
        Equip ret = new Equip(getItemId(), getPosition(), getUniqueId(), getFlag());
        ret.str = this.str;
        ret.dex = this.dex;
        ret._int = this._int;
        ret.luk = this.luk;
        ret.hp = this.hp;
        ret.mp = this.mp;
        ret.matk = this.matk;
        ret.mdef = this.mdef;
        ret.watk = this.watk;
        ret.wdef = this.wdef;
        ret.acc = this.acc;
        ret.avoid = this.avoid;
        ret.hands = this.hands;
        ret.speed = this.speed;
        ret.jump = this.jump;
        ret.upgradeSlots = this.upgradeSlots;
        ret.level = this.level;
        ret.itemEXP = this.itemEXP;
        ret.durability = this.durability;
        ret.vicioushammer = this.vicioushammer;
        ret.state = this.state;
        ret.enhance = this.enhance;
        ret.potential1 = this.potential1;
        ret.potential2 = this.potential2;
        ret.potential3 = this.potential3;
        ret.potential4 = this.potential4;
        ret.potential5 = this.potential5;
        ret.potential6 = this.potential6;
        ret.charmExp = this.charmExp;
        ret.pvpDamage = this.pvpDamage;
        ret.incSkill = this.incSkill;
        ret.statemsg = this.statemsg;
        ret.socket1 = this.socket1;
        ret.socket2 = this.socket2;
        ret.socket3 = this.socket3;
        ret.itemSkin = this.itemSkin;
        ret.limitBreak = this.limitBreak;


        ret.enhanctBuff = this.enhanctBuff;
        ret.reqLevel = this.reqLevel;
        ret.yggdrasilWisdom = this.yggdrasilWisdom;
        ret.finalStrike = this.finalStrike;
        ret.bossDamage = this.bossDamage;
        ret.ignorePDR = this.ignorePDR;
        ret.totalDamage = this.totalDamage;
        ret.allStat = this.allStat;
        ret.karmaCount = this.karmaCount;

        ret.setGMLog(getGMLog());
        ret.setGiftFrom(getGiftFrom());
        ret.setOwner(getOwner());
        ret.setQuantity(getQuantity());
        ret.setExpiration(getExpiration());
        ret.setInventoryId(getInventoryId());
        ret.setEquipOnlyId(getEquipOnlyId());
        return ret;
    }

    public byte getType()
    {
        return 1;
    }

    public void setQuantity(short quantity)
    {
        if ((quantity < 0) || (quantity > 1))
        {
            throw new RuntimeException("设置装备的数量错误 欲设置的数量： " + quantity + " (道具ID: " + getItemId() + ")");
        }
        super.setQuantity(quantity);
    }

    public int getItemEXP()
    {
        return this.itemEXP;
    }

    public void setItemEXP(int itemEXP)
    {
        if (itemEXP < 0)
        {
            itemEXP = 0;
        }
        this.itemEXP = itemEXP;
    }

    public byte getEnhance()
    {
        return this.enhance;
    }

    public void setEnhance(byte en)
    {
        this.enhance = en;
    }

    public int getPotential1()
    {
        return this.potential1;
    }

    public void setPotential1(int en)
    {
        this.potential1 = en;
    }

    public int getPotential2()
    {
        return this.potential2;
    }

    public void setPotential2(int en)
    {
        this.potential2 = en;
    }

    public int getPotential4()
    {
        return this.potential4;
    }

    public void setPotential4(int en)
    {
        this.potential4 = en;
    }

    public int getPotential5()
    {
        return this.potential5;
    }

    public void setPotential5(int en)
    {
        this.potential5 = en;
    }

    public int getPotential6()
    {
        return this.potential6;
    }

    public void setPotential6(int en)
    {
        this.potential6 = en;
    }

    public void resetPotential_Fuse(boolean half, int potentialState)
    {
        potentialState = -potentialState;
        if (Randomizer.nextInt(100) < 4)
        {
            potentialState -= (Randomizer.nextInt(100) < 4 ? 2 : 1);
        }
        setPotential1(potentialState);
        setPotential2(Randomizer.nextInt(half ? 5 : 10) == 0 ? potentialState : 0);
        setPotential3(0);
    }

    public void resetPotential()
    {
        int rank = Randomizer.nextInt(100) < 4 ? -18 : Randomizer.nextInt(100) < 4 ? -19 : -17;
        setPotential1(rank);
        setPotential2(Randomizer.nextInt(10) == 0 ? rank : 0);
        setPotential3(0);
    }

    public void renewPotential(int type)
    {
        renewPotential(type, 4);
    }

    public void renewPotential(int type, int rate)
    {
        if (Randomizer.nextInt(100) < rate)
        {
        }
        int rank = getState() != (type == 3 ? 20 : 19) ? -(getState() + 1) : type == 2 ? -18 : type == 4 ? -19 : -getState();
        setPotential1(rank);
        if (getPotential3() > 0)
        {
            setPotential2(rank);
        }
        else
        {
            switch (type)
            {
                case 1:
                    setPotential2(Randomizer.nextInt(10) == 0 ? rank : 0);
                    break;
                case 2:
                case 4:
                    setPotential2(Randomizer.nextInt(10) <= 1 ? rank : 0);
                    break;
                case 3:
                    setPotential2(Randomizer.nextInt(10) <= 2 ? rank : 0);
                    break;
                default:
                    setPotential2(0);
            }

        }
        setPotential3(0);
    }

    public byte getState()
    {
        return getState(false);
    }

    public int getPotential3()
    {
        return this.potential3;
    }

    public void setPotential3(int en)
    {
        this.potential3 = en;
    }

    public byte getState(boolean useAddPot)
    {
        byte ret = 0;
        if ((this.potential1 >= 40000) || (this.potential2 >= 40000) || (this.potential3 >= 40000))
        {
            ret = 20;
        }
        else if ((this.potential1 >= 30000) || (this.potential2 >= 30000) || (this.potential3 >= 30000))
        {
            ret = 19;
        }
        else if ((this.potential1 >= 20000) || (this.potential2 >= 20000) || (this.potential3 >= 20000))
        {
            ret = 18;
        }
        else if ((this.potential1 >= 1) || (this.potential2 >= 1) || (this.potential3 >= 1))
        {
            ret = 17;
        }
        else if ((this.potential1 < 0) || (this.potential2 < 0) || (this.potential3 < 0))
        {
            ret = 1;
        }
        if ((useAddPot) && ((this.potential4 < 0) || (this.potential5 < 0) || (this.potential6 < 0)))
        {
            ret = (byte) (ret | 0x20);
        }
        return ret;
    }

    public void setState(byte en)
    {
        this.state = en;
    }

    public byte getAddState()
    {
        byte ret = 0;
        if ((this.potential4 < 0) || (this.potential5 < 0) || (this.potential6 < 0))
        {
            ret = 17;
        }
        return ret;
    }

    public void renewAddPotential(int rate, int lines)
    {
        int rank = (Randomizer.nextInt(100) < rate) && (getAddState(lines) != 20) ? -(getAddState(lines) + 1) : -getAddState(lines);
        if (lines == 6)
        {
            setPotential6(rank);
        }
        else if (lines == 5)
        {
            setPotential5(rank);
        }
        else
        {
            setPotential4(rank);
        }
    }

    public byte getAddState(int lines)
    {
        byte ret = 0;
        int potential = lines == 5 ? this.potential5 : lines == 6 ? this.potential6 : this.potential4;
        if (potential >= 40000)
        {
            ret = 20;
        }
        else if (potential >= 30000)
        {
            ret = 19;
        }
        else if (potential >= 20000)
        {
            ret = 18;
        }
        else if (potential >= 1)
        {
            ret = 17;
        }
        return ret;
    }

    public short getCharmEXP()
    {
        return this.charmExp;
    }

    public void setCharmEXP(short s)
    {
        this.charmExp = s;
    }

    public MapleRing getRing()
    {
        if ((!ItemConstants.isEffectRing(getItemId())) || (getUniqueId() <= 0))
        {
            return null;
        }
        if (this.ring == null)
        {
            this.ring = MapleRing.loadFromDb(getUniqueId(), getPosition() < 0);
        }
        return this.ring;
    }

    public void setRing(MapleRing ring)
    {
        this.ring = ring;
    }

    public MapleAndroid getAndroid()
    {
        if ((getItemId() / 10000 != 166) || (getUniqueId() <= 0))
        {
            return null;
        }
        if (this.android == null)
        {
            this.android = MapleAndroid.loadFromDb(getItemId(), getUniqueId());
        }
        return this.android;
    }

    public void setAndroid(MapleAndroid ring)
    {
        this.android = ring;
    }

    public int getStateMsg()
    {
        return this.statemsg;
    }

    public void setStateMsg(int en)
    {
        if (en >= 3)
        {
            this.statemsg = 3;
        }
        else if (en < 0)
        {
            this.statemsg = 0;
        }
        else
        {
            this.statemsg = en;
        }
    }

    public short getSocketState()
    {
        short flag = 0;
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        boolean isSocketItem = ii.isActivatedSocketItem(getItemId());
        if (isSocketItem)
        {
            flag = (short) (flag | SocketFlag.可以镶嵌.getValue());
            if ((this.socket1 == -1) && (isSocketItem))
            {
                setSocket1(0);
            }
            if (this.socket1 != -1)
            {
                flag = (short) (flag | SocketFlag.已打孔01.getValue());
            }
            if (this.socket2 != -1)
            {
                flag = (short) (flag | SocketFlag.已打孔02.getValue());
            }
            if (this.socket3 != -1)
            {
                flag = (short) (flag | SocketFlag.已打孔03.getValue());
            }
            if (this.socket1 > 0)
            {
                flag = (short) (flag | SocketFlag.已镶嵌01.getValue());
            }
            if (this.socket2 > 0)
            {
                flag = (short) (flag | SocketFlag.已镶嵌02.getValue());
            }
            if (this.socket3 > 0)
            {
                flag = (short) (flag | SocketFlag.已镶嵌03.getValue());
            }
        }
        return flag;
    }

    public int getSocket1()
    {
        return this.socket1;
    }

    public void setSocket1(int socket)
    {
        this.socket1 = socket;
    }

    public int getSocket2()
    {
        return this.socket2;
    }

    public void setSocket2(int socket)
    {
        this.socket2 = socket;
    }

    public int getSocket3()
    {
        return this.socket3;
    }

    public void setSocket3(int socket)
    {
        this.socket3 = socket;
    }

    public int getItemSkin()
    {
        return this.itemSkin;
    }

    public void setItemSkin(int id)
    {
        this.itemSkin = id;
    }

    public short getKarmaCount()
    {
        return this.karmaCount;
    }

    public void setKarmaCount(short karmaCount)
    {
        this.karmaCount = karmaCount;
    }

    public int getEquipFlag()
    {
        int flag = 0;
        if (getUpgradeSlots() > 0)
        {
            flag |= EquipStats.可升级次数.getValue();
        }
        if (getLevel() > 0)
        {
            flag |= EquipStats.已升级次数.getValue();
        }
        if (getStr() > 0)
        {
            flag |= EquipStats.力量.getValue();
        }
        if (getDex() > 0)
        {
            flag |= EquipStats.敏捷.getValue();
        }
        if (getInt() > 0)
        {
            flag |= EquipStats.智力.getValue();
        }
        if (getLuk() > 0)
        {
            flag |= EquipStats.运气.getValue();
        }
        if (getHp() > 0)
        {
            flag |= EquipStats.Hp.getValue();
        }
        if (getMp() > 0)
        {
            flag |= EquipStats.Mp.getValue();
        }
        if (getWatk() > 0)
        {
            flag |= EquipStats.物攻.getValue();
        }
        if (getMatk() > 0)
        {
            flag |= EquipStats.魔攻.getValue();
        }
        if (getWdef() > 0)
        {
            flag |= EquipStats.物防.getValue();
        }
        if (getMdef() > 0)
        {
            flag |= EquipStats.魔防.getValue();
        }
        if (getAcc() > 0)
        {
            flag |= EquipStats.命中.getValue();
        }
        if (getAvoid() > 0)
        {
            flag |= EquipStats.回避.getValue();
        }
        if (getHands() > 0)
        {
            flag |= EquipStats.手技.getValue();
        }
        if (getSpeed() > 0)
        {
            flag |= EquipStats.速度.getValue();
        }
        if (getJump() > 0)
        {
            flag |= EquipStats.跳跃.getValue();
        }
        if (getFlag() > 0)
        {
            flag |= EquipStats.状态.getValue();
        }
        if (getIncSkill() > 0)
        {
            flag |= EquipStats.技能.getValue();
        }
        if (getEquipLevel() > 0)
        {
            flag |= EquipStats.道具等级.getValue();
        }
        if (getExpPercentage() > 0)
        {
            flag |= EquipStats.道具经验.getValue();
        }
        if (getDurability() > 0)
        {
            flag |= EquipStats.耐久度.getValue();
        }
        if (getViciousHammer() > 0)
        {
            flag |= EquipStats.金锤子.getValue();
        }
        if (getPVPDamage() > 0)
        {
            flag |= EquipStats.大乱斗攻击力.getValue();
        }
        if (getEnhanctBuff() > 0)
        {
            flag |= EquipStats.ENHANCT_BUFF.getValue();
        }
        if (getReqLevel() > 0)
        {
            flag |= EquipStats.REQUIRED_LEVEL.getValue();
        }
        if (getYggdrasilWisdom() > 0)
        {
            flag |= EquipStats.YGGDRASIL_WISDOM.getValue();
        }
        if (getFinalStrike())
        {
            flag |= EquipStats.FINAL_STRIKE.getValue();
        }
        if (getBossDamage() > 0)
        {
            flag |= EquipStats.BOSS伤害.getValue();
        }
        if (getIgnorePDR() > 0)
        {
            flag |= EquipStats.无视防御.getValue();
        }
        return flag;
    }

    public byte getUpgradeSlots()
    {
        return this.upgradeSlots;
    }

    public void setUpgradeSlots(byte upgradeSlots)
    {
        this.upgradeSlots = upgradeSlots;
    }

    public byte getLevel()
    {
        return this.level;
    }

    public short getStr()
    {
        return this.str;
    }

    public short getDex()
    {
        return this.dex;
    }

    public short getInt()
    {
        return this._int;
    }

    public short getLuk()
    {
        return this.luk;
    }

    public short getHp()
    {
        return this.hp;
    }

    public short getMp()
    {
        return this.mp;
    }

    public short getWatk()
    {
        return this.watk;
    }

    public short getMatk()
    {
        return this.matk;
    }

    public short getWdef()
    {
        return this.wdef;
    }

    public short getMdef()
    {
        return this.mdef;
    }

    public short getAcc()
    {
        return this.acc;
    }

    public short getAvoid()
    {
        return this.avoid;
    }

    public short getHands()
    {
        return this.hands;
    }

    public short getSpeed()
    {
        return this.speed;
    }

    public short getJump()
    {
        return this.jump;
    }

    public void setJump(short jump)
    {
        if (jump < 0)
        {
            jump = 0;
        }
        this.jump = jump;
    }

    public int getIncSkill()
    {
        return this.incSkill;
    }

    public int getEquipLevel()
    {
        if (GameConstants.getMaxLevel(getItemId()) <= 0) return 0;
        if (getEquipExp() <= 0)
        {
            return getBaseLevel();
        }
        int levelz = getBaseLevel();
        int expz = getEquipExp();
        for (int i = levelz; GameConstants.getStatFromWeapon(getItemId()) == null ? i <= GameConstants.getMaxLevel(getItemId()) : i < GameConstants.getMaxLevel(getItemId()); i++)
        {
            if (expz < GameConstants.getExpForLevel(i, getItemId())) break;
            levelz++;
            expz -= GameConstants.getExpForLevel(i, getItemId());
        }


        return levelz;
    }

    public int getExpPercentage()
    {
        if ((getEquipLevel() < getBaseLevel()) || (getEquipLevel() > GameConstants.getMaxLevel(getItemId())) || (GameConstants.getExpForLevel(getEquipLevel(), getItemId()) <= 0))
        {
            return 0;
        }
        return getEquipExpForLevel() * 100 / GameConstants.getExpForLevel(getEquipLevel(), getItemId());
    }

    public int getDurability()
    {
        return this.durability;
    }

    public byte getViciousHammer()
    {
        return this.vicioushammer;
    }

    public void setViciousHammer(byte ham)
    {
        this.vicioushammer = ham;
    }

    public short getPVPDamage()
    {
        return this.pvpDamage;
    }

    public void setPVPDamage(short p)
    {
        this.pvpDamage = p;
    }

    public short getEnhanctBuff()
    {
        return this.enhanctBuff;
    }

    public void setEnhanctBuff(short enhanctBuff)
    {
        if (enhanctBuff < 0)
        {
            enhanctBuff = 0;
        }
        this.enhanctBuff = enhanctBuff;
    }

    public short getReqLevel()
    {
        return this.reqLevel;
    }

    public void setReqLevel(short reqLevel)
    {
        if (reqLevel < 0)
        {
            reqLevel = 0;
        }
        this.reqLevel = reqLevel;
    }

    public short getYggdrasilWisdom()
    {
        return this.yggdrasilWisdom;
    }

    public void setYggdrasilWisdom(short yggdrasilWisdom)
    {
        if (yggdrasilWisdom < 0)
        {
            yggdrasilWisdom = 0;
        }
        this.yggdrasilWisdom = yggdrasilWisdom;
    }

    public boolean getFinalStrike()
    {
        return this.finalStrike;
    }

    public void setFinalStrike(boolean finalStrike)
    {
        this.finalStrike = finalStrike;
    }

    public short getBossDamage()
    {
        return this.bossDamage;
    }

    public void setBossDamage(short bossDamage)
    {
        if (bossDamage < 0)
        {
            bossDamage = 0;
        }
        this.bossDamage = bossDamage;
    }

    public short getIgnorePDR()
    {
        return this.ignorePDR;
    }

    public int getEquipExp()
    {
        if (this.itemEXP <= 0)
        {
            return 0;
        }

        if (ItemConstants.isWeapon(getItemId()))
        {
            return this.itemEXP / 700000;
        }
        return this.itemEXP / 350000;
    }

    public int getBaseLevel()
    {
        return GameConstants.getStatFromWeapon(getItemId()) == null ? 1 : 0;
    }

    public int getEquipExpForLevel()
    {
        if (getEquipExp() <= 0)
        {
            return 0;
        }
        int expz = getEquipExp();
        for (int i = getBaseLevel(); i <= GameConstants.getMaxLevel(getItemId()); i++)
        {
            if (expz < GameConstants.getExpForLevel(i, getItemId())) break;
            expz -= GameConstants.getExpForLevel(i, getItemId());
        }


        return expz;
    }

    public void setIgnorePDR(short ignorePDR)
    {
        if (ignorePDR < 0)
        {
            ignorePDR = 0;
        }
        this.ignorePDR = ignorePDR;
    }

    public void setDurability(int dur)
    {
        this.durability = dur;
    }

    public void setIncSkill(int inc)
    {
        this.incSkill = inc;
    }

    public void setSpeed(short speed)
    {
        if (speed < 0)
        {
            speed = 0;
        }
        this.speed = speed;
    }

    public void setHands(short hands)
    {
        if (hands < 0)
        {
            hands = 0;
        }
        this.hands = hands;
    }

    public void setAvoid(short avoid)
    {
        if (avoid < 0)
        {
            avoid = 0;
        }
        this.avoid = avoid;
    }

    public void setAcc(short acc)
    {
        if (acc < 0)
        {
            acc = 0;
        }
        this.acc = acc;
    }

    public void setMdef(short mdef)
    {
        if (mdef < 0)
        {
            mdef = 0;
        }
        this.mdef = mdef;
    }

    public void setWdef(short wdef)
    {
        if (wdef < 0)
        {
            wdef = 0;
        }
        this.wdef = wdef;
    }

    public void setMatk(short matk)
    {
        if (matk < 0)
        {
            matk = 0;
        }
        this.matk = matk;
    }

    public void setWatk(short watk)
    {
        if (watk < 0)
        {
            watk = 0;
        }
        this.watk = watk;
    }

    public void setMp(short mp)
    {
        if (mp < 0)
        {
            mp = 0;
        }
        this.mp = mp;
    }

    public void setHp(short hp)
    {
        if (hp < 0)
        {
            hp = 0;
        }
        this.hp = hp;
    }

    public void setLuk(short luk)
    {
        if (luk < 0)
        {
            luk = 0;
        }
        this.luk = luk;
    }

    public void setInt(short _int)
    {
        if (_int < 0)
        {
            _int = 0;
        }
        this._int = _int;
    }

    public void setDex(short dex)
    {
        if (dex < 0)
        {
            dex = 0;
        }
        this.dex = dex;
    }

    public void setStr(short str)
    {
        if (str < 0)
        {
            str = 0;
        }
        this.str = str;
    }

    public void setLevel(byte level)
    {
        this.level = level;
    }

    public int getEquipSpecialFlag()
    {
        int flag = 0;
        if (getTotalDamage() > 0)
        {
            flag |= EquipSpecialStat.总伤害.getValue();
        }
        if (getAllStat() > 0)
        {
            flag |= EquipSpecialStat.全属性.getValue();
        }
        flag |= EquipSpecialStat.剪刀次数.getValue();
        return flag;
    }

    public short getTotalDamage()
    {
        return this.totalDamage;
    }

    public void setTotalDamage(short totalDamage)
    {
        if (totalDamage < 0)
        {
            totalDamage = 0;
        }
        this.totalDamage = totalDamage;
    }

    public short getAllStat()
    {
        return this.allStat;
    }

    public void setAllStat(short allStat)
    {
        if (allStat < 0)
        {
            allStat = 0;
        }
        this.allStat = allStat;
    }

    public int getLimitBreak()
    {
        return Math.min(this.limitBreak, 100000000);
    }

    public void setLimitBreak(int lb)
    {
        this.limitBreak = lb;
    }

    public void setPotentials(int potline, int potId)
    {
        if (potline == 1)
        {
            setPotential1(potId);
        }
        else if (potline == 2)
        {
            setPotential2(potId);
        }
        else if (potline == 3)
        {
            setPotential3(potId);
        }
        else if (potline == 4)
        {
            setPotential4(potId);
        }
        else if (potline == 5)
        {
            setPotential5(potId);
        }
        else if (potline == 6)
        {
            setPotential6(potId);
        }
    }

    public enum ScrollResult
    {
        失败, 成功, 消失;

        ScrollResult()
        {
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\Equip.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */