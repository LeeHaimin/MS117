package client.inventory;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import database.DatabaseConnection;
import server.MapleItemInformationProvider;
import server.Randomizer;
import server.StructAndroid;
import server.movement.AbsoluteLifeMovement;
import server.movement.LifeMovement;
import server.movement.LifeMovementFragment;

public class MapleAndroid implements java.io.Serializable
{
    private static final Logger log = Logger.getLogger(MapleAndroid.class);
    private static final long serialVersionUID = 9179541993413738569L;
    private final int uniqueid;
    private final int itemid;
    private int Fh = 0;
    private int stance = 0;
    private int skin;
    private int hair;
    private int face;
    private int gender;
    private int type;
    private String name;
    private Point pos = new Point(0, 0);
    private boolean changed = false;

    private MapleAndroid(int itemid, int uniqueid)
    {
        this.itemid = itemid;
        this.uniqueid = uniqueid;
    }


    public static MapleAndroid loadFromDb(int itemid, int uniqueid)
    {
        try
        {
            MapleAndroid ret = new MapleAndroid(itemid, uniqueid);
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM androids WHERE uniqueid = ?");
            ps.setInt(1, uniqueid);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                return null;
            }
            int type = rs.getInt("type");
            int gender = rs.getInt("gender");
            boolean fix = false;
            if (type < 1)
            {
                MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
                type = ii.getAndroidType(itemid);
                StructAndroid aInfo = ii.getAndroidInfo(type);
                if (aInfo == null)
                {
                    return null;
                }
                gender = aInfo.gender;
                fix = true;
            }
            ret.setType(type);
            ret.setGender(gender);
            ret.setSkin(rs.getInt("skin"));
            ret.setHair(rs.getInt("hair"));
            ret.setFace(rs.getInt("face"));
            ret.setName(rs.getString("name"));
            ret.changed = fix;
            rs.close();
            ps.close();
            return ret;
        }
        catch (SQLException ex)
        {
            log.error("加载安卓信息出错", ex);
        }
        return null;
    }

    public static MapleAndroid create(int itemid, int uniqueid)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        int type = ii.getAndroidType(itemid);
        StructAndroid aInfo = ii.getAndroidInfo(type);
        if (aInfo == null)
        {
            return null;
        }
        int gender = aInfo.gender;
        int skin = aInfo.skin.get(Randomizer.nextInt(aInfo.skin.size()));
        int hair = aInfo.hair.get(Randomizer.nextInt(aInfo.hair.size()));
        int face = aInfo.face.get(Randomizer.nextInt(aInfo.face.size()));
        if (uniqueid <= -1)
        {
            uniqueid = MapleInventoryIdentifier.getInstance();
        }
        try
        {
            PreparedStatement pse = DatabaseConnection.getConnection().prepareStatement("INSERT INTO androids (uniqueid, type, gender, skin, hair, face, name) VALUES (?, ?, ?, ?, ?, ?, ?)");
            pse.setInt(1, uniqueid);
            pse.setInt(2, type);
            pse.setInt(3, gender);
            pse.setInt(4, skin);
            pse.setInt(5, hair);
            pse.setInt(6, face);
            pse.setString(7, "智能机器人");
            pse.executeUpdate();
            pse.close();
        }
        catch (SQLException ex)
        {
            log.error("创建安卓信息出错", ex);
            return null;
        }
        MapleAndroid and = new MapleAndroid(itemid, uniqueid);
        and.setType(type);
        and.setGender(gender);
        and.setSkin(skin);
        and.setHair(hair);
        and.setFace(face);
        and.setName("智能机器人");
        return and;
    }

    public void saveToDb()
    {
        if (!this.changed)
        {
            return;
        }
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE androids SET type = ?, gender = ?, skin = ?, hair = ?, face = ?, name = ? WHERE uniqueid = ?");
            ps.setInt(1, this.type);
            ps.setInt(2, this.gender);
            ps.setInt(3, this.skin);
            ps.setInt(4, this.hair);
            ps.setInt(5, this.face);
            ps.setString(6, this.name);
            ps.setInt(7, this.uniqueid);
            ps.executeUpdate();
            ps.close();
            this.changed = false;
        }
        catch (SQLException ex)
        {
            log.error("保存安卓信息出错", ex);
        }
    }

    public int getItemId()
    {
        return this.itemid;
    }

    public int getUniqueId()
    {
        return this.uniqueid;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String n)
    {
        this.name = n;
        this.changed = true;
    }

    public int getType()
    {
        return this.type;
    }

    public void setType(int t)
    {
        this.type = t;
        this.changed = true;
    }

    public int getGender()
    {
        return this.gender;
    }

    public void setGender(int g)
    {
        this.gender = g;
        this.changed = true;
    }

    public int getSkin()
    {
        return this.skin;
    }

    public void setSkin(int s)
    {
        this.skin = s;
        this.changed = true;
    }

    public int getHair()
    {
        return this.hair;
    }

    public void setHair(int h)
    {
        this.hair = h;
        this.changed = true;
    }

    public int getFace()
    {
        return this.face;
    }

    public void setFace(int f)
    {
        this.face = f;
        this.changed = true;
    }

    public int getFh()
    {
        return this.Fh;
    }

    public void setFh(int Fh)
    {
        this.Fh = Fh;
    }

    public Point getPos()
    {
        return this.pos;
    }

    public void setPos(Point pos)
    {
        this.pos = pos;
    }

    public int getStance()
    {
        return this.stance;
    }

    public void setStance(int stance)
    {
        this.stance = stance;
    }

    public void updatePosition(List<LifeMovementFragment> movement)
    {
        for (LifeMovementFragment move : movement)
        {
            if ((move instanceof LifeMovement))
            {
                if ((move instanceof AbsoluteLifeMovement))
                {
                    setPos(move.getPosition());
                    setFh(((AbsoluteLifeMovement) move).getNewFH());
                }
                setStance(((LifeMovement) move).getNewstate());
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\inventory\MapleAndroid.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */