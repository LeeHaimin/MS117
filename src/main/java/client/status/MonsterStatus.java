package client.status;

import java.io.Serializable;

import client.MapleDisease;
import constants.GameConstants;
import handling.Buffstat;

public enum MonsterStatus implements Serializable, Buffstat
{
    物攻(1, 1), 物防(2, 1), 魔攻(4, 1), 魔防(8, 1), 命中(16, 1), 回避(32, 1), 速度(64, 1), 眩晕(128, 1), 结冰(256, 1), 中毒(512, 1), 封印(1024, 1), 挑衅(2048, 1), 物攻提升(4096, 1), 物防提升(8192, 1), 魔攻提升(16384, 1), 魔防提升(32768,
        1), 巫毒(65536, 1), 影网(131072, 1), 免疫物攻(262144, 1), 免疫魔攻(524288, 1), 标飞挑衅(1048576, 1), 免疫伤害(2097152, 1), 忍者伏击(4194304, 1), 烈焰喷射(16777216, 1), 恐慌(33554432, 1), 模糊领域(134217728, 1),
    心灵控制(268435456, 1), 反射物攻(536870912, 1), 反射魔攻(1073741824, 1), 抗压(2, 2), 鬼刻符(4, 2), 怪物炸弹(8, 2), 魔击无效(16, 2), 被爆击伤害增加(2048, 2),

    空白BUFF(134217728, 1, true), 召唤怪物(Integer.MIN_VALUE, 1, true), EMPTY_1(32, 2, !GameConstants.GMS), EMPTY_2(64, 2, true), EMPTY_3(128, 2, true), EMPTY_4(256, 2, GameConstants.GMS), EMPTY_5(512, 2
        , GameConstants.GMS), EMPTY_6(1024, 2, GameConstants.GMS);

    static final long serialVersionUID = 0L;
    private final int i;
    private final int first;
    private final boolean end;

    MonsterStatus(int i, int first)
    {
        this.i = i;
        this.first = first;
        this.end = false;
    }

    MonsterStatus(int i, int first, boolean end)
    {
        this.i = i;
        this.first = first;
        this.end = end;
    }

    public static MonsterStatus getBySkill_Pokemon(int skill)
    {
        switch (skill)
        {
            case 120:
                return 封印;
            case 121:
                return 恐慌;
            case 123:
                return 眩晕;
            case 125:
                return 中毒;
            case 126:
                return 速度;
            case 137:
                return 结冰;
        }
        return null;
    }

    public static MapleDisease getLinkedDisease(MonsterStatus stat)
    {
        switch (stat)
        {
            case 眩晕:
            case 影网:
                return MapleDisease.眩晕;
            case 中毒:
            case 烈焰喷射:
                return MapleDisease.中毒;
            case 封印:
            case 魔击无效:
                return MapleDisease.封印;
            case 结冰:
                return MapleDisease.FREEZE;
            case 恐慌:
                return MapleDisease.黑暗;
            case 速度:
                return MapleDisease.缓慢;
        }
        return null;
    }

    public static int genericSkill(MonsterStatus stat)
    {
        switch (stat)
        {
            case 眩晕:
                return 90001001;
            case 速度:
                return 90001002;
            case 中毒:
                return 90001003;
            case 恐慌:
                return 90001004;
            case 封印:
                return 90001005;
            case 结冰:
                return 2221011;
            case 魔击无效:
                return 1111007;
            case 挑衅:
                return 4121003;
            case 鬼刻符:
                return 22161002;
            case 影网:
                return 4111003;
            case 烈焰喷射:
                return 5211004;
            case 巫毒:
                return 2311005;
            case 忍者伏击:
                return 4121004;
        }
        return 0;
    }

    public boolean isEmpty()
    {
        return this.end;
    }

    public int getValue()
    {
        return this.i;
    }

    public int getPosition()
    {
        return this.first;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\status\MonsterStatus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */