package client.status;

import java.lang.ref.WeakReference;

import client.MapleCharacter;
import server.life.MapleMonster;
import server.life.MobSkill;

public class MonsterStatusEffect
{
    private final int skill;
    private final MobSkill mobskill;
    private final boolean monsterSkill;
    private MonsterStatus stati;
    private WeakReference<MapleCharacter> weakChr = null;
    private Integer x;
    private int poisonSchedule = 0;
    private boolean reflect = false;
    private long cancelTime = 0L;
    private long dotTime = 0L;

    public MonsterStatusEffect(MonsterStatus stat, Integer x, int skillId, MobSkill mobskill, boolean monsterSkill)
    {
        this.stati = stat;
        this.x = x;
        this.skill = skillId;
        this.mobskill = mobskill;
        this.monsterSkill = monsterSkill;
    }

    public MonsterStatusEffect(MonsterStatus stat, Integer x, int skillId, MobSkill mobskill, boolean monsterSkill, boolean reflect)
    {
        this.stati = stat;
        this.x = x;
        this.skill = skillId;
        this.mobskill = mobskill;
        this.monsterSkill = monsterSkill;
        this.reflect = reflect;
    }

    public MonsterStatus getStati()
    {
        return this.stati;
    }

    public Integer getX()
    {
        return this.x;
    }

    public void setValue(MonsterStatus status, Integer newVal)
    {
        this.stati = status;
        this.x = newVal;
    }

    public int getSkill()
    {
        return this.skill;
    }

    public MobSkill getMobSkill()
    {
        return this.mobskill;
    }

    public boolean isMonsterSkill()
    {
        return this.monsterSkill;
    }

    public long getCancelTask()
    {
        return this.cancelTime;
    }

    public void setCancelTask(long cancelTask)
    {
        this.cancelTime = (System.currentTimeMillis() + cancelTask);
    }

    public long getDotTime()
    {
        return this.dotTime;
    }

    public void setDotTime(long duration)
    {
        this.dotTime = duration;
    }

    public void setPoisonSchedule(int poisonSchedule, MapleCharacter chrr)
    {
        this.poisonSchedule = poisonSchedule;
        this.weakChr = new WeakReference(chrr);
    }

    public int getPoisonSchedule()
    {
        return this.poisonSchedule;
    }

    public boolean shouldCancel(long now)
    {
        return (this.cancelTime > 0L) && (this.cancelTime <= now);
    }

    public void cancelTask()
    {
        this.cancelTime = 0L;
    }

    public boolean isReflect()
    {
        return this.reflect;
    }

    public int getFromID()
    {
        return (this.weakChr == null) || (this.weakChr.get() == null) ? 0 : this.weakChr.get().getId();
    }

    public void cancelPoisonSchedule(MapleMonster mm)
    {
        mm.doPoison(this, this.weakChr);
        this.poisonSchedule = 0;
        this.weakChr = null;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\status\MonsterStatusEffect.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */