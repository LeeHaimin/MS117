package client;

import java.io.Serializable;

import handling.Buffstat;
import server.Randomizer;

public enum MapleDisease implements Serializable, Buffstat
{
    眩晕(131072, 1, 123), 中毒(262144, 1, 125), 封印(524288, 1, 120), 黑暗(1048576, 1, 121), 虚弱(1073741824, 1, 122), 诅咒(Integer.MIN_VALUE, 1, 124), 缓慢(1, 2, 126), 变身(2, 2, 172), 诱惑(128, 2, 128),
    ZOMBIFY(16384, 2, 133), REVERSE_DIRECTION(524288, 2, 132), POTION(512, 3, 134), SHADOW(1024, 3, 135), BLIND(2048, 3, 136), FREEZE(131072, 3, 137), DISABLE_POTENTIAL(16777216, 4, 138),
    TORNADO(268435456, 4, 173), FLAG(Integer.MIN_VALUE, 5, 799);

    private static final long serialVersionUID = 0L;
    private final int i;
    private final int first;
    private final int disease;

    MapleDisease(int i, int first, int disease)
    {
        this.i = i;
        this.first = first;
        this.disease = disease;
    }

    public static MapleDisease getRandom()
    {
        for (; ; )
        {
            for (MapleDisease dis : values())
            {
                if (Randomizer.nextInt(values().length) == 0)
                {
                    return dis;
                }
            }
        }
    }

    public static MapleDisease getBySkill(int skill)
    {
        for (MapleDisease d : values())
        {
            if (d.getDisease() == skill)
            {
                return d;
            }
        }
        return null;
    }

    public int getDisease()
    {
        return this.disease;
    }

    public int getValue()
    {
        return this.i;
    }

    public int getPosition()
    {
        return this.first;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleDisease.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */