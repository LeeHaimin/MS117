package client;


public class SpecialBuffInfo
{
    public final int buffid;


    public final int value;


    public final int bufflength;


    public SpecialBuffInfo(int buffid, int value, int bufflength)
    {
        this.buffid = buffid;
        this.value = value;
        this.bufflength = bufflength;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\SpecialBuffInfo.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */