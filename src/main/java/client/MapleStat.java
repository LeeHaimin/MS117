package client;

public enum MapleStat
{
    皮肤(1L), 脸型(2L), 发型(4L), 等级(16L), 职业(32L), 力量(64L), 敏捷(128L), 智力(256L), 运气(512L), HP(1024L), MAXHP(2048L), MP(4096L), MAXMP(8192L), AVAILABLEAP(16384L), AVAILABLESP(32768L), 经验(65536L),
    人气(131072L), 金币(262144L), 宠物(1572872L), GACHAPONEXP(524288L), 疲劳(524288L), 领袖(1048576L), 洞察(2097152L), 意志(4194304L), 手技(8388608L), 感性(16777216L), 魅力(33554432L), TODAYS_TRAITS(67108864L),
    TRAIT_LIMIT(134217728L), BATTLE_EXP(268435456L), BATTLE_RANK(536870912L), BATTLE_POINTS(1073741824L), ICE_GAGE(2147483648L), VIRTUE(4294967296L), 性别(8589934592L);

    private final long i;

    MapleStat(long i)
    {
        this.i = i;
    }

    public static MapleStat getByValue(long value)
    {
        for (MapleStat stat : values())
        {
            if (stat.i == value)
            {
                return stat;
            }
        }
        return null;
    }

    public long getValue()
    {
        return this.i;
    }

    public enum Temp
    {
        力量(1), 敏捷(2), 智力(4), 运气(8), 物攻(16), 魔攻(32), 物防(64), 魔防(128), 命中(256), 回避(512), 速度(1024), 跳跃(2048);

        private final int i;

        Temp(int i)
        {
            this.i = i;
        }

        public int getValue()
        {
            return this.i;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleStat.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */