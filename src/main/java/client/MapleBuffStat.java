package client;

import java.io.Serializable;

import handling.Buffstat;

public enum MapleBuffStat implements Serializable, Buffstat
{
    物理攻击(1, 1), 物理防御(2, 1), 魔法攻击(4, 1), 魔法防御(8, 1), 命中率(16, 1), 回避率(32, 1), 手技(64, 1), 移动速度(128, 1), 跳跃力(256, 1), 魔法盾(512, 1), 隐身术(1024, 1), 攻击加速(2048, 1), 伤害反击(4096, 1), MAXHP(8192, 1),
    MAXMP(16384, 1), 神之保护(32768, 1), 无形箭弩(65536, 1),


    斗气集中(2097152, 1), 召唤兽(2097152, 1), 属性攻击(4194304, 1), 龙之力(8388608, 1), 神圣祈祷(16777216, 1), 聚财术(33554432, 1), 影分身(67108864, 1), 敛财术(134217728, 1), 替身术(134217728, 1),

    金钱护盾(268435456, 1), HP_LOSS_GUARD(536870912, 1), 变身术(2, 2), 恢复效果(4, 2), 冒险岛勇士(8, 2), 稳如泰山(16, 2), 火眼晶晶(32, 2, false, true), 魔法反击(64, 2),


    暗器伤人(256, 2), 终极无限(512, 2), 进阶祝福(1024, 2, false, true), 敏捷提升(2048, 2), 致盲(4096, 2), 集中精力(8192, 2),

    英雄回声(32768, 2), MESO_RATE(65536, 2), GHOST_MORPH(131072, 2), ARIANT_COSS_IMU(262144, 2), DROP_RATE(1048576, 2), EXPRATE(4194304, 2), ACASH_RATE(8388608, 2), ILLUSION(16777216, 2), 狂暴战魂(33554432
        , 2), 金刚霸体(67108864, 2), 闪光击(134217728, 2), ARIANT_COSS_IMU2(268435456, 2), 终极弓剑(536870912, 2), 自然力重置(Integer.MIN_VALUE, 2), 风影漫步(1, 3), 矛连击强化(4, 3), 连环吸血(8, 3), 战神之盾(16, 3), 战神抗压(32, 3),
    战神威势(64, 3), 天使状态(128, 3), 缓速术(4096, 3), 魔法屏障(8192, 3), 抗魔领域(16384, 3), 灵魂之石(32768, 3), 飞行骑乘(65536, 3), 雷鸣冲击(262144, 3), 葵花宝典(524288, 3), 死亡猫头鹰(1048576, 3), 终极斩(4194304, 3), DAMAGE_BUFF(8388608
        , 3), ATTACK_BUFF(16777216, 3), 地雷(33554432, 3), 增强_MAXHP(67108864, 3), 增强_MAXMP(134217728, 3), 增强_物理攻击(268435456, 3), 增强_魔法攻击(536870912, 3), 增强_物理防御(1073741824, 3),
    增强_魔法防御(Integer.MIN_VALUE, 3), 完美机甲(1, 4), 卫星防护_PROC(2, 4), 卫星防护_吸收(4, 4), 幻灵飓风(8, 4), 呼啸_爆击概率(16, 4), 呼啸_MaxMp增加(32, 4), 呼啸_伤害减少(64, 4), 呼啸_回避概率(128, 4), 幻灵转化(256, 4), 幻灵重生(512, 4), 潜入(1024,
        4), 金属机甲(2048, 4),

    黑暗灵气(8192, 4), 蓝色灵气(16384, 4), 黄色灵气(32768, 4), 幻灵霸体(65536, 4), 暴走形态(131072, 4), 幸运骰子(262144, 4), 祝福护甲(524288, 4), 反制攻击(1048576, 4), 快速移动精通(2097152, 4), 战斗命令(4194304, 4), 灵魂助力(8388608, 4),
    GIANT_POTION(33554432, 4), 玛瑙的保佑(67108864, 4), 玛瑙的意志(134217728, 4), 牧师祝福(536870912, 4), 压制术(1, 5), 冰骑士(2, 5),

    力量(16, 5), 智力(32, 5), 敏捷(64, 5), 运气(128, 5), indiePad(1024, 5, true), indieMad(2048, 5, true), indieMaxHp(4096, 5, true), indieMaxMp(8192, 5, true), indieAcc(16384, 5, true), indieEva(32768, 5,
        true), indieJump(65536, 5, true), indieSpeed(131072, 5, true), indieAllStat(262144, 5, true), PVP_DAMAGE(16384, 5), PVP_ATTACK(32768, 5), INVINCIBILITY(65536, 5), 潜力解放(131072, 5),
    精灵弱化(262144, 5), 拳手索命(524288, 5), FROZEN(1048576, 5),

    ICE_SKILL(4194304, 5),


    无限精气(536870912, 5),


    神圣魔法盾(1, 6),


    神秘瞄准术(4, 6), 异常抗性(16, 6), 属性抗性(32, 6),

    伤害吸收(64, 6), 黑暗变形(128, 6),

    随机橡木桶(256, 6), 精神连接(512, 6), 物理防御增加(8192, 6, true), 魔法防御增加(16384, 6, true), 神圣拯救者的祝福(16384, 6), 爆击提升(32768, 6),


    NO_SLIP(262144, 6), FAMILIAR_SHADOW(524288, 6), SIDEKICK_PASSIVE(1048576, 6), 吸血鬼之触(33554432, 6), 黑暗忍耐(67108864, 6),


    百分比MaxHp(8, 7, true), 攻击速度提升(256, 7, true), 取消天使(65536, 7, true), 击杀点数(131072, 7), 子弹数量(262144, 7), 百分比MaxMp(524288, 7, true), 百分比无视防御(33554432, 7, false, true), 神秘运气(67108864, 7),
    幻影屏障(134217728, 7), 爆击概率增加(268435456, 7), 最小爆击伤害(536870912, 7), 卡牌审判(1073741824, 7), 伤害增加(Integer.MIN_VALUE, 7, true),


    增加_物理攻击(64, 8), 光暗转换(512, 8), 黑暗高潮(1024, 8), 黑暗祝福(2048, 8), 抵抗之魔法盾(4096, 8), 生命潮汐(8192, 8), 变形值(32768, 8), 强健护甲(65536, 8), 模式转换(131072, 8), 剑刃之壁(1048576, 8), 灵魂凝视(2097152, 8), 伤害置换(8388608, 8,
        false, true), 伤害上限(1073741824, 8, true),


    状态异常抗性(16, 9, true), 所有属性抗性(32, 9, true), 天使复仇(256, 9), 暴击概率(262144, 9, true), BOSS伤害(16777216, 9), 超越攻击(67108864, 9), 恶魔恢复(134217728, 9), 恶魔超越(1073741824, 9), 伤害减少(Integer.MIN_VALUE, 9, true),


    尖兵电力(2, 10), 尖兵飞行(16, 10), 元素属性(128, 10), 命中增加(4096, 10), 回避增加(8192, 10), 月光转换(65536, 10), 灵魂融入(131072, 10), 元素灵魂(262144, 10), 元气恢复(67108864, 10), 交叉锁链(134217728, 10, false, true),


    极限射箭(16, 11, false, true), 进阶箭筒(128, 11), 圣洁之力(4096, 11), 神圣迅捷(8192, 11), BOSS伤害增加(65536, 11, true), 尖兵能量(Integer.MIN_VALUE, 11),


    模式变更(4, 12), 能量获得(16777216, 12, true), 疾驰速度(33554432, 12, true), 疾驰跳跃(67108864, 12, true), 骑兽技能(134217728, 12, true), 极速领域(268435456, 12, true), 导航辅助(536870912, 12, true),
    DEFAULT_BUFFSTAT(Integer.MIN_VALUE, 12);

    private static final long serialVersionUID = 0L;
    private final int buffstat;
    private final int first;
    private boolean stacked = false;
    private boolean special = false;

    MapleBuffStat(int buffstat, int first)
    {
        this.buffstat = buffstat;
        this.first = first;
    }

    MapleBuffStat(int buffstat, int first, boolean stacked)
    {
        this.buffstat = buffstat;
        this.first = first;
        this.stacked = stacked;
    }

    MapleBuffStat(int buffstat, int first, boolean stacked, boolean special)
    {
        this.buffstat = buffstat;
        this.first = first;
        this.stacked = stacked;
        this.special = special;
    }

    public int getPosition(boolean fromZero)
    {
        if (!fromZero)
        {
            return this.first;
        }
        if ((this.first > 0) && (this.first <= 12))
        {
            return 12 - this.first;
        }
        return 0;
    }

    public int getValue()
    {
        return this.buffstat;
    }

    public int getPosition()
    {
        return this.first;
    }

    public boolean canStack()
    {
        return this.stacked;
    }

    public boolean isSpecial()
    {
        return this.special;
    }
}