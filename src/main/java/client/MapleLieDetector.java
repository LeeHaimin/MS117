package client;

import handling.world.WorldBroadcastService;
import scripting.lieDetector.LieDetectorScript;
import server.Timer;
import server.maps.MapleMap;
import server.quest.MapleQuest;
import tools.HexTool;
import tools.MaplePacketCreator;
import tools.Pair;


public class MapleLieDetector
{
    public final MapleCharacter chr;
    public byte type;
    public int attempt;
    public String tester;
    public String answer;
    public boolean inProgress;
    public boolean passed;

    public MapleLieDetector(MapleCharacter c)
    {
        this.chr = c;
        reset();
    }

    public final void reset()
    {
        this.tester = "";
        this.answer = "";
        this.attempt = 0;
        this.inProgress = false;
        this.passed = false;
    }

    public final boolean startLieDetector(final String tester, final boolean isItem, boolean anotherAttempt)
    {
        if ((!anotherAttempt) && (((isPassed()) && (isItem)) || (inProgress()) || (this.attempt == 3)))
        {

            return false;
        }
        Pair<String, String> captcha = LieDetectorScript.getImageBytes();
        if (captcha == null)
        {
            return false;
        }
        byte[] image = HexTool.getByteArrayFromHexString(captcha.getLeft());
        this.answer = captcha.getRight();
        this.tester = tester;
        this.inProgress = true;
        this.type = ((byte) (isItem ? 0 : 1));
        this.attempt += 1;

        this.chr.getClient().getSession().write(MaplePacketCreator.sendLieDetector(image, this.attempt));
        Timer.EtcTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                if ((!MapleLieDetector.this.isPassed()) && (MapleLieDetector.this.chr != null)) if (MapleLieDetector.this.attempt >= 3)
                {
                    MapleCharacter search_chr = MapleLieDetector.this.chr.getMap().getCharacterByName(tester);
                    if ((search_chr != null) && (search_chr.getId() != MapleLieDetector.this.chr.getId()))
                    {
                        search_chr.dropMessage(5, MapleLieDetector.this.chr.getName() + " 没有通过测谎仪的检测，恭喜你获得7000的金币.");
                        search_chr.gainMeso(7000, true);
                    }
                    MapleLieDetector.this.end();
                    MapleLieDetector.this.chr.getClient().getSession().write(MaplePacketCreator.LieDetectorResponse((byte) 10, (byte) 4));
                    MapleMap map = MapleLieDetector.this.chr.getClient().getChannelServer().getMapFactory().getMap(180000001);
                    MapleLieDetector.this.chr.getQuestNAdd(MapleQuest.getInstance(123456)).setCustomData(String.valueOf(1800));
                    MapleLieDetector.this.chr.changeMap(map, map.getPortal(0));
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                            "[GM Message] 玩家: " + MapleLieDetector.this.chr.getName() + " (等级 " + MapleLieDetector.this.chr.getLevel() + ") 未通过测谎仪检测，系统将其监禁30分钟！"));
                }
                else
                {
                    MapleLieDetector.this.startLieDetector(tester, isItem, true);
                }
            }
        }, 60000L);


        return true;
    }

    public final int getAttempt()
    {
        return this.attempt;
    }

    public final byte getLastType()
    {
        return this.type;
    }

    public final String getTester()
    {
        return this.tester;
    }

    public final String getAnswer()
    {
        return this.answer;
    }

    public final boolean inProgress()
    {
        return this.inProgress;
    }

    public final boolean isPassed()
    {
        return this.passed;
    }

    public final void end()
    {
        this.inProgress = false;
        this.passed = true;
        this.attempt = 0;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleLieDetector.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */