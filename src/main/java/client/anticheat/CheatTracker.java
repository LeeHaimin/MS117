package client.anticheat;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.lang.ref.WeakReference;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import client.MapleCharacterUtil;
import client.SkillFactory;
import handling.world.WorldBroadcastService;
import server.AutobanManager;
import server.Timer;
import tools.MaplePacketCreator;
import tools.StringUtil;

public class CheatTracker
{
    private static final Logger log = Logger.getLogger(CheatTracker.class);
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock rL = this.lock.readLock();
    private final Lock wL = this.lock.writeLock();
    private final Map<CheatingOffense, CheatingOffenseEntry> offenses = new EnumMap(CheatingOffense.class);
    private WeakReference<MapleCharacter> chr;
    private long lastAttackTime = 0L;
    private int lastAttackTickCount = 0;
    private byte Attack_tickResetCount = 0;
    private long Server_ClientAtkTickDiff = 0L;
    private long lastDamage = 0L;
    private long takingDamageSince;
    private int numSequentialDamage = 0;
    private long lastDamageTakenTime = 0L;
    private byte numZeroDamageTaken = 0;
    private int numSequentialSummonAttack = 0;
    private long summonSummonTime = 0L;
    private int numSameDamage = 0;
    private Point lastMonsterMove;
    private int monsterMoveCount;
    private int attacksWithoutHit = 0;
    private byte dropsPerSecond = 0;
    private long lastDropTime = 0L;
    private byte msgsPerSecond = 0;
    private long lastMsgTime = 0L;
    private java.util.concurrent.ScheduledFuture<?> invalidationTask;
    private int gm_message = 0;
    private int lastTickCount = 0;
    private int tickSame = 0;
    private int inMapIimeCount = 0;
    private int lastPickupkCount = 0;
    private long lastSmegaTime = 0L;
    private long lastBBSTime = 0L;
    private long lastASmegaTime = 0L;
    private long lastMZDTime = 0L;
    private long lastCraftTime = 0L;
    private long lastSaveTime = 0L;
    private long lastLieDetectorTime = 0L;
    private long lastPickupkTime = 0L;
    private long lastlogonTime;
    private int numSequentialFamiliarAttack = 0;
    private long familiarSummonTime = 0L;

    public CheatTracker(MapleCharacter chr)
    {
        start(chr);
    }

    public final void start(MapleCharacter chr)
    {
        this.chr = new WeakReference(chr);
        this.invalidationTask = Timer.CheatTimer.getInstance().register(new InvalidationTask(), 60000L);
        this.takingDamageSince = System.currentTimeMillis();
        this.lastSaveTime = System.currentTimeMillis();
    }

    public void checkAttack(int skillId, int tickcount)
    {
        int AtkDelay = constants.GameConstants.getAttackDelay(skillId, skillId == 0 ? null : SkillFactory.getSkill(skillId));
        if (tickcount - this.lastAttackTickCount < AtkDelay)
        {
        }


        this.lastAttackTime = System.currentTimeMillis();
        if ((this.chr.get() != null) && (this.lastAttackTime - this.chr.get().getChangeTime() > 600000L))
        {
            this.chr.get().setChangeTime(false);

            if ((!this.chr.get().isInTownMap()) && (this.chr.get().getEventInstance() == null) && (this.chr.get().getMap().getMobsSize() >= 2))
            {
                this.inMapIimeCount += 1;
                if (this.inMapIimeCount >= 6)
                {
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                            "[GM Message] " + this.chr.get().getName() + " ID: " + this.chr.get().getId() + " (等级 " + this.chr.get().getLevel() + ") 在地图: " + this.chr.get().getMapId() + " 打怪时间超过1" + "小时，该玩家可能是在挂机打怪。"));
                }
                if (this.inMapIimeCount >= 8)
                {
                    this.inMapIimeCount = 0;
                    this.chr.get().startLieDetector(false);

                    log.info("[作弊] " + this.chr.get().getName() + " (等级 " + this.chr.get().getLevel() + ") 在地图: " + this.chr.get().getMapId() + " 打怪时间超过 80 分钟，系统启动测谎仪系统。");
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                            "[GM Message] " + this.chr.get().getName() + " ID: " + this.chr.get().getId() + " (等级 " + this.chr.get().getLevel() + ") 在地图: " + this.chr.get().getMapId() + " 打怪时间超过 " +
                                    "80" + " 分钟，系统启动测谎仪系统。"));
                }
            }
        }
        long STime_TC = this.lastAttackTime - tickcount;
        if (this.Server_ClientAtkTickDiff - STime_TC > 1000L)
        {
            if (this.chr.get().isAdmin())
            {
                this.chr.get().dropMessage(-5, "攻击速度异常2 技能: " + skillId + " 当前: " + (this.Server_ClientAtkTickDiff - STime_TC));
            }
            registerOffense(CheatingOffense.FASTATTACK2, "攻击速度异常.");
        }


        this.Attack_tickResetCount = ((byte) (this.Attack_tickResetCount + 1));
        if (this.Attack_tickResetCount >= (AtkDelay <= 200 ? 1 : 4))
        {
            this.Attack_tickResetCount = 0;
            this.Server_ClientAtkTickDiff = STime_TC;
        }
        updateTick(tickcount);
        this.lastAttackTickCount = tickcount;
    }

    public void registerOffense(CheatingOffense offense, String param)
    {
        MapleCharacter chrhardref = this.chr.get();
        if ((chrhardref == null) || (!offense.isEnabled()) || (chrhardref.isGM()))
        {
            return;
        }
        CheatingOffenseEntry entry = null;
        this.rL.lock();
        try
        {
            entry = this.offenses.get(offense);
        }
        finally
        {
            this.rL.unlock();
        }
        if ((entry != null) && (entry.isExpired()))
        {
            expireEntry(entry);
            entry = null;
            this.gm_message = 0;
        }
        if (entry == null)
        {
            entry = new CheatingOffenseEntry(offense, chrhardref.getId());
        }
        if (param != null)
        {
            entry.setParam(param);
        }
        entry.incrementCount();
        if (offense.shouldAutoban(entry.getCount()))
        {
            byte type = offense.getBanType();
            if (type == 1)
            {
                AutobanManager.getInstance().autoban(chrhardref.getClient(), StringUtil.makeEnumHumanReadable(offense.name()));
            }
            else if (type == 2)
            {
                chrhardref.getClient().disconnect(true, false);
                if (chrhardref.getClient().getSession().isConnected())
                {
                    chrhardref.getClient().getSession().close(true);
                }
                log.info("[作弊] " + chrhardref.getName() + " (等级:" + chrhardref.getLevel() + " 职业:" + chrhardref.getJob() + ") 服务器断开他的连接. 原因: " + StringUtil.makeEnumHumanReadable(offense.name()) + (param == null ? "" : " - " + param));
            }
            this.gm_message = 0;
            return;
        }
        this.wL.lock();
        try
        {
            this.offenses.put(offense, entry);
        }
        finally
        {
            this.wL.unlock();
        }
        switch (offense)
        {


            case SAME_DAMAGE:
                this.gm_message += 1;
                if (this.gm_message % 100 == 0)
                {
                    log.info("[作弊] " + MapleCharacterUtil.makeMapleReadable(chrhardref.getName()) + " ID: " + chrhardref.getId() + " (等级 " + chrhardref.getLevel() + ") 使用非法程序! " + StringUtil.makeEnumHumanReadable(offense.name()) + (param == null ? "" : " - " + param));
                    WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                            "[GM Message] " + MapleCharacterUtil.makeMapleReadable(chrhardref.getName()) + " ID: " + chrhardref.getId() + " (等级 " + chrhardref.getLevel() + ") 使用非法程序! " + StringUtil.makeEnumHumanReadable(offense.name()) + (param == null ? "" : " - " + param)));
                }
                if (this.gm_message >= 20) if (chrhardref.getLevel() < (offense == CheatingOffense.SAME_DAMAGE ? 180 : 190))
                {
                    Timestamp chrCreated = chrhardref.getChrCreated();
                    long time = System.currentTimeMillis();
                    if (chrCreated != null)
                    {
                        time = chrCreated.getTime();
                    }
                    if (time + 1296000000L >= System.currentTimeMillis())
                    {
                        AutobanManager.getInstance().autoban(chrhardref.getClient(), StringUtil.makeEnumHumanReadable(offense.name()) + " over 500 times " + (param == null ? "" : " - " + param));
                    }
                    else
                    {
                        this.gm_message = 0;
                        WorldBroadcastService.getInstance().broadcastGMMessage(MaplePacketCreator.serverNotice(6,
                                "[GM Message] " + MapleCharacterUtil.makeMapleReadable(chrhardref.getName()) + " " + "ID: " + chrhardref.getId() + " (等级 " + chrhardref.getLevel() + ") 使用非法程序! " + StringUtil.makeEnumHumanReadable(offense.name()) + (param == null ? "" : " - " + param)));
                        tools.FileoutputUtil.log("log\\Hacker.log",
                                "[GM Message] " + MapleCharacterUtil.makeMapleReadable(chrhardref.getName()) + " ID: " + chrhardref.getId() + " (等级 " + chrhardref.getLevel() + ") 使用非法程序! " + StringUtil.makeEnumHumanReadable(offense.name()) + (param == null ? "" : " - " + param));
                    }
                }
                break;
        }

        CheatingOffensePersister.getInstance().persistEntry(entry);
    }

    public void updateTick(int newTick)
    {
        if (newTick <= this.lastTickCount)
        {
            if ((this.tickSame >= 30) && (this.chr.get() != null) && (!(this.chr.get()).isGM()))
            {
                (this.chr.get()).getClient().disconnect(true, false);
                if ((this.chr.get()).getClient().getSession().isConnected())
                {
                    (this.chr.get()).getClient().getSession().close(true);
                }
                log.info("[作弊] " + (this.chr.get()).getName() + " (等级 " + (this.chr.get()).getLevel() + ") updateTick 次数: " + this.tickSame + " 服务器断开他的连接。");
            }
            else
            {
                this.tickSame += 1;
            }
        }
        else
        {
            this.tickSame = 0;
        }
        this.lastTickCount = newTick;
    }

    public void expireEntry(CheatingOffenseEntry coe)
    {
        wL.lock();
        try
        {
            offenses.remove(coe.getOffense());
        }
        finally
        {
            wL.unlock();
        }
    }

    public void resetInMapIimeCount()
    {
        this.inMapIimeCount = 0;
    }

    public void checkPVPAttack(int skillId)
    {
        int AtkDelay = constants.GameConstants.getAttackDelay(skillId, skillId == 0 ? null : SkillFactory.getSkill(skillId));
        long STime_TC = System.currentTimeMillis() - this.lastAttackTime;
        if (STime_TC < AtkDelay)
        {
            registerOffense(CheatingOffense.FASTATTACK, "攻击速度异常.");
        }
        this.lastAttackTime = System.currentTimeMillis();
    }

    public long getLastAttack()
    {
        return this.lastAttackTime;
    }

    public void checkTakeDamage(int damage)
    {
        this.numSequentialDamage += 1;
        this.lastDamageTakenTime = System.currentTimeMillis();


        if (this.lastDamageTakenTime - this.takingDamageSince / 500L < this.numSequentialDamage)
        {
            registerOffense(CheatingOffense.FAST_TAKE_DAMAGE, "掉血次数异常.");
        }
        if (this.lastDamageTakenTime - this.takingDamageSince > 4500L)
        {
            this.takingDamageSince = this.lastDamageTakenTime;
            this.numSequentialDamage = 0;
        }


        if (damage == 0)
        {
            this.numZeroDamageTaken = ((byte) (this.numZeroDamageTaken + 1));
            if (this.numZeroDamageTaken >= 50)
            {
                this.numZeroDamageTaken = 0;
                registerOffense(CheatingOffense.HIGH_AVOID, "回避率过高.");
            }
        }
        else if (damage != -1)
        {
            this.numZeroDamageTaken = 0;
        }
    }

    public void resetTakeDamage()
    {
        this.numZeroDamageTaken = 0;
    }

    public void checkSameDamage(int dmg, double expected)
    {
        if ((dmg > 2000) && (this.lastDamage == dmg) && (this.chr.get() != null) && ((this.chr.get().getLevel() < 190) || (dmg > expected * 2.0D)))
        {
            this.numSameDamage += 1;
            if (this.numSameDamage > 5)
            {
                registerOffense(CheatingOffense.SAME_DAMAGE,
                        this.numSameDamage + " times, 攻击伤害 " + dmg + ", 预期伤害 " + expected + " [等级: " + this.chr.get().getLevel() + ", 职业: " + this.chr.get().getJob() + "]");
                this.numSameDamage = 0;
            }
        }
        else
        {
            this.lastDamage = dmg;
            this.numSameDamage = 0;
        }
    }

    public void checkHighDamage(int eachd, double maxDamagePerHit, int mobId, int skillId)
    {
        if ((eachd > maxDamagePerHit) && (maxDamagePerHit > 2000.0D) && (this.chr.get() != null))
        {
            registerOffense(CheatingOffense.HIGH_DAMAGE,
                    "[伤害: " + eachd + ", 预计伤害: " + maxDamagePerHit + ", 怪物ID: " + mobId + "] [职业: " + this.chr.get().getJob() + ", 等级: " + this.chr.get().getLevel() + ", 技能: " + skillId + "]");
            if (eachd > maxDamagePerHit * 2.0D)
            {
                registerOffense(CheatingOffense.HIGH_DAMAGE_2,
                        "[伤害: " + eachd + ", 预计伤害: " + maxDamagePerHit + ", 怪物ID: " + mobId + "] [职业: " + this.chr.get().getJob() + ", 等级: " + this.chr.get().getLevel() + ", 技能: " + skillId + "]");
            }
        }
    }

    public void checkMoveMonster(Point pos)
    {
        if (pos.equals(this.lastMonsterMove))
        {
            this.monsterMoveCount += 1;
            if (this.monsterMoveCount > 10)
            {
                registerOffense(CheatingOffense.MOVE_MONSTERS, "吸怪 坐标: " + pos.x + ", " + pos.y);
                this.monsterMoveCount = 0;
            }
        }
        else
        {
            this.lastMonsterMove = pos;
            this.monsterMoveCount = 1;
        }
    }

    public void resetSummonAttack()
    {
        this.summonSummonTime = System.currentTimeMillis();
        this.numSequentialSummonAttack = 0;
    }

    public boolean checkSummonAttack()
    {
        this.numSequentialSummonAttack += 1;
        if ((System.currentTimeMillis() - this.summonSummonTime) / 1001L < this.numSequentialSummonAttack)
        {
            registerOffense(CheatingOffense.FAST_SUMMON_ATTACK, "召唤兽攻击速度过快.");
            return false;
        }
        return true;
    }

    public void resetFamiliarAttack()
    {
        this.familiarSummonTime = System.currentTimeMillis();
        this.numSequentialFamiliarAttack = 0;
    }

    public boolean checkFamiliarAttack(MapleCharacter chr)
    {
        this.numSequentialFamiliarAttack += 1;


        if ((System.currentTimeMillis() - this.familiarSummonTime) / 601L < this.numSequentialFamiliarAttack)
        {
            registerOffense(CheatingOffense.FAST_SUMMON_ATTACK, "召唤兽攻击速度异常.");
            return false;
        }
        return true;
    }

    public void checkPickup(int count, boolean pet)
    {
        if (System.currentTimeMillis() - this.lastPickupkTime < 1000L)
        {
            this.lastPickupkCount += 1;
            if ((this.lastPickupkCount >= count) && (this.chr.get() != null) && (!this.chr.get().isGM()))
            {
                log.info("[作弊] " + this.chr.get().getName() + " (等级 " + this.chr.get().getLevel() + ") " + (pet ? "宠物" : "角色") + "捡取 checkPickup 次数: " + this.lastPickupkCount + " 服务器断开他的连接。");
                this.chr.get().getClient().disconnect(true, false);
                if ((this.chr.get() != null) && (this.chr.get().getClient() != null) && (this.chr.get().getClient().getSession().isConnected()))
                {
                    this.chr.get().getClient().getSession().close(true);
                }
            }
        }
        else
        {
            this.lastPickupkCount = 0;
        }
        this.lastPickupkTime = System.currentTimeMillis();
    }

    public void checkDrop()
    {
        checkDrop(false);
    }

    public void checkDrop(boolean dc)
    {
        if (System.currentTimeMillis() - this.lastDropTime < 1000L)
        {
            this.dropsPerSecond = ((byte) (this.dropsPerSecond + 1));
            if (this.dropsPerSecond >= (dc ? 32 : 16)) if ((this.chr.get() != null) && (!this.chr.get().isGM()))
            {
                if (dc)
                {
                    this.chr.get().getClient().disconnect(true, false);
                    if (this.chr.get().getClient().getSession().isConnected())
                    {
                        this.chr.get().getClient().getSession().close(true);
                    }
                    log.info("[作弊] " + this.chr.get().getName() + " (等级 " + this.chr.get().getLevel() + ") checkDrop 次数: " + this.dropsPerSecond + " 服务器断开他的连接。");
                }
                else
                {
                    this.chr.get().getClient().setMonitored(true);
                }
            }
        }
        else
        {
            this.dropsPerSecond = 0;
        }
        this.lastDropTime = System.currentTimeMillis();
    }

    public void checkMsg()
    {
        if (System.currentTimeMillis() - this.lastMsgTime < 1000L)
        {
            this.msgsPerSecond = ((byte) (this.msgsPerSecond + 1));
            if ((this.msgsPerSecond > 10) && (this.chr.get() != null) && (!this.chr.get().isGM()))
            {
                this.chr.get().getClient().disconnect(true, false);
                if (this.chr.get().getClient().getSession().isConnected())
                {
                    this.chr.get().getClient().getSession().close(true);
                }
                log.info("[作弊] " + this.chr.get().getName() + " (等级 " + this.chr.get().getLevel() + ") checkMsg 次数: " + this.msgsPerSecond + " 服务器断开他的连接。");
            }
        }
        else
        {
            this.msgsPerSecond = 0;
        }
        this.lastMsgTime = System.currentTimeMillis();
    }

    public int getAttacksWithoutHit()
    {
        return this.attacksWithoutHit;
    }

    public void setAttacksWithoutHit(boolean increase)
    {
        if (increase)
        {
            this.attacksWithoutHit += 1;
        }
        else
        {
            this.attacksWithoutHit = 0;
        }
    }

    public void registerOffense(CheatingOffense offense)
    {
        registerOffense(offense, null);
    }

    public boolean canSmega()
    {
        if ((this.lastSmegaTime > System.currentTimeMillis()) && (this.chr.get() != null) && (!(this.chr.get()).isGM()))
        {
            return false;
        }
        this.lastSmegaTime = System.currentTimeMillis();
        return true;
    }

    public boolean canAvatarSmega()
    {
        if ((this.lastASmegaTime > System.currentTimeMillis()) && (this.chr.get() != null) && (!(this.chr.get()).isGM()))
        {
            return false;
        }
        this.lastASmegaTime = System.currentTimeMillis();
        return true;
    }

    public boolean canBBS()
    {
        if ((this.lastBBSTime + 60000L > System.currentTimeMillis()) && (this.chr.get() != null) && (!(this.chr.get()).isGM()))
        {
            return false;
        }
        this.lastBBSTime = System.currentTimeMillis();
        return true;
    }

    public boolean canMZD()
    {
        if ((this.lastMZDTime > System.currentTimeMillis()) && (this.chr.get() != null) && (!(this.chr.get()).isGM()))
        {
            return false;
        }
        this.lastMZDTime = System.currentTimeMillis();
        return true;
    }

    public boolean canCraftMake()
    {
        if ((this.lastCraftTime + 3000L > System.currentTimeMillis()) && (this.chr.get() != null) && (!(this.chr.get()).isGM()))
        {
            return false;
        }
        this.lastCraftTime = System.currentTimeMillis();
        return true;
    }

    public boolean canSaveDB()
    {
        if ((this.lastSaveTime + 180000L > System.currentTimeMillis()) && (this.chr.get() != null))
        {
            return false;
        }
        this.lastSaveTime = System.currentTimeMillis();
        return true;
    }

    public int getlastSaveTime()
    {
        if (this.lastSaveTime <= 0L)
        {
            this.lastSaveTime = System.currentTimeMillis();
        }
        int seconds = (int) ((this.lastSaveTime + 180000L - System.currentTimeMillis()) / 1000L);
        return seconds;
    }

    public boolean canLieDetector()
    {
        if ((this.lastLieDetectorTime + 300000L > System.currentTimeMillis()) && (this.chr.get() != null))
        {
            return false;
        }
        this.lastLieDetectorTime = System.currentTimeMillis();
        return true;
    }

    public long getLastlogonTime()
    {
        if ((this.lastlogonTime <= 0L) || (this.chr.get() == null))
        {
            this.lastlogonTime = System.currentTimeMillis();
        }
        return this.lastlogonTime;
    }

    public int getPoints()
    {
        int ret = 0;
        CheatingOffenseEntry[] offenses_copy;
        rL.lock();
        try
        {
            offenses_copy = offenses.values().toArray(new CheatingOffenseEntry[offenses.size()]);
        }
        finally
        {
            rL.unlock();
        }
        for (final CheatingOffenseEntry entry : offenses_copy)
        {
            if (entry.isExpired())
            {
                expireEntry(entry);
            }
            else
            {
                ret += entry.getPoints();
            }
        }
        return ret;
    }

    public Map<CheatingOffense, CheatingOffenseEntry> getOffenses()
    {
        return java.util.Collections.unmodifiableMap(this.offenses);
    }

    public String getSummary()
    {
        StringBuilder ret = new StringBuilder();
        List<CheatingOffenseEntry> offenseList = new ArrayList<>();
        this.rL.lock();
        try
        {
            for (CheatingOffenseEntry entry : this.offenses.values())
            {
                if (!entry.isExpired())
                {
                    offenseList.add(entry);
                }
            }
        }
        finally
        {
            this.rL.unlock();
        }
        Collections.sort(offenseList, (Comparator) (o1, o2) -> {
            int thisVal = ((CheatingOffenseEntry) o1).getPoints();
            int anotherVal = ((CheatingOffenseEntry) o2).getPoints();
            return thisVal == anotherVal ? 0 : thisVal < anotherVal ? 1 : -1;
        });
        int to = Math.min(offenseList.size(), 4);
        for (int x = 0; x < to; x++)
        {
            ret.append(StringUtil.makeEnumHumanReadable(offenseList.get(x).getOffense().name()));
            ret.append(": ");
            ret.append(offenseList.get(x).getCount());
            if (x != to - 1)
            {
                ret.append(" ");
            }
        }
        return ret.toString();
    }

    public void dispose()
    {
        if (this.invalidationTask != null)
        {
            this.invalidationTask.cancel(false);
        }
        this.invalidationTask = null;
        this.chr = new WeakReference(null);
    }

    private class InvalidationTask implements Runnable
    {
        private InvalidationTask()
        {
        }

        public void run()
        {
            CheatingOffenseEntry[] offenses_copy;
            rL.lock();
            try
            {
                offenses_copy = offenses.values().toArray(new CheatingOffenseEntry[offenses.size()]);
            }
            finally
            {
                rL.unlock();
            }
            for (CheatingOffenseEntry offense : offenses_copy)
            {
                if (offense.isExpired())
                {
                    expireEntry(offense);
                }
            }
            if (chr.get() == null)
            {
                dispose();
            }
        }
    }
}