package client.anticheat;

public class CheatingOffenseEntry
{
    private final CheatingOffense offense;
    private final int characterid;
    private int count = 0;
    private long lastOffense;
    private String param;
    private int dbid = -1;

    public CheatingOffenseEntry(CheatingOffense offense, int characterid)
    {
        this.offense = offense;
        this.characterid = characterid;
    }

    public CheatingOffense getOffense()
    {
        return this.offense;
    }

    public int getCount()
    {
        return this.count;
    }

    public int getChrfor()
    {
        return this.characterid;
    }

    public void incrementCount()
    {
        this.count += 1;
        this.lastOffense = System.currentTimeMillis();
    }

    public boolean isExpired()
    {
        return this.lastOffense < System.currentTimeMillis() - this.offense.getValidityDuration();
    }

    public int getPoints()
    {
        return this.count * this.offense.getPoints();
    }

    public String getParam()
    {
        return this.param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public long getLastOffenseTime()
    {
        return this.lastOffense;
    }

    public int getDbId()
    {
        return this.dbid;
    }

    public void setDbId(int dbid)
    {
        this.dbid = dbid;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\anticheat\CheatingOffenseEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */