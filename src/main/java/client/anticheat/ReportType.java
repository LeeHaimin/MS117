package client.anticheat;

public enum ReportType
{
    Hacking(0, "hack"), Botting(1, "bot"), Scamming(2, "scam"), FakeGM(3, "fake"),

    Advertising(5, "ad");

    public final byte i;
    public final String theId;

    ReportType(int i, String theId)
    {
        this.i = ((byte) i);
        this.theId = theId;
    }

    public static ReportType getById(int z)
    {
        for (ReportType t : values())
        {
            if (t.i == z)
            {
                return t;
            }
        }
        return null;
    }

    public static ReportType getByString(String z)
    {
        for (ReportType t : values())
        {
            if (z.contains(t.theId))
            {
                return t;
            }
        }
        return null;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\anticheat\ReportType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */