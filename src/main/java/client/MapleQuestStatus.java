package client;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import constants.GameConstants;
import server.life.MapleLifeFactory;
import server.quest.MapleQuest;

public final class MapleQuestStatus implements Serializable
{
    private static final long serialVersionUID = 91795419934134L;
    private transient MapleQuest quest;
    private byte status;
    private Map<Integer, Integer> killedMobs = null;
    private int npc;
    private long completionTime;
    private int forfeited = 0;

    private String customData;


    public MapleQuestStatus(MapleQuest quest, int status)
    {
        this.quest = quest;
        setStatus((byte) status);
        this.completionTime = System.currentTimeMillis();
        if ((status == 1) && (!quest.getRelevantMobs().isEmpty()))
        {
            registerMobs();
        }
    }

    private void registerMobs()
    {
        this.killedMobs = new LinkedHashMap<>();
        for (Integer integer : this.quest.getRelevantMobs().keySet())
        {
            int i = integer;
            this.killedMobs.put(i, 0);
        }
    }

    public MapleQuestStatus(MapleQuest quest, byte status, int npc)
    {
        this.quest = quest;
        setStatus(status);
        setNpc(npc);
        this.completionTime = System.currentTimeMillis();
        if ((status == 1) && (!quest.getRelevantMobs().isEmpty()))
        {
            registerMobs();
        }
    }

    public MapleQuest getQuest()
    {
        return this.quest;
    }

    public void setQuest(int qid)
    {
        this.quest = MapleQuest.getInstance(qid);
    }

    public byte getStatus()
    {
        return this.status;
    }

    public void setStatus(byte status)
    {
        this.status = status;
    }

    public int getNpc()
    {
        return this.npc;
    }

    public void setNpc(int npc)
    {
        this.npc = npc;
    }

    public boolean isCustom()
    {
        return GameConstants.isCustomQuest(this.quest.getId());
    }

    public boolean mobKilled(int id, int skillID)
    {
        if ((this.quest != null) && (this.quest.getSkillID() > 0) && (this.quest.getSkillID() != skillID))
        {
            return false;
        }

        Integer mob = this.killedMobs.get(id);
        if (mob != null)
        {
            int mo = maxMob(id);
            if (mob >= mo)
            {
                return false;
            }
            this.killedMobs.put(id, Math.min(mob.intValue() + 1, mo));
            return true;
        }
        for (Map.Entry<Integer, Integer> mo : this.killedMobs.entrySet())
        {
            if (questCount(mo.getKey(), id))
            {
                int mobb = maxMob(mo.getKey());
                if (mo.getValue() >= mobb)
                {
                    return false;
                }
                this.killedMobs.put(mo.getKey(), Math.min(mo.getValue().intValue() + 1, mobb));
                return true;
            }
        }
        return false;
    }

    private int maxMob(int mobid)
    {
        for (Map.Entry<Integer, Integer> qs : this.quest.getRelevantMobs().entrySet())
        {
            if (qs.getKey() == mobid)
            {
                return qs.getValue();
            }
        }
        return 0;
    }

    private boolean questCount(int mo, int id)
    {
        Iterator localIterator;
        if (MapleLifeFactory.getQuestCount(mo) != null)
        {
            for (localIterator = MapleLifeFactory.getQuestCount(mo).iterator(); localIterator.hasNext(); )
            {
                int i = (Integer) localIterator.next();
                if (i == id)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void setMobKills(int id, int count)
    {
        if (this.killedMobs == null)
        {
            registerMobs();
        }
        this.killedMobs.put(id, count);
    }

    public boolean hasMobKills()
    {
        if (this.killedMobs == null)
        {
            return false;
        }
        return this.killedMobs.size() > 0;
    }

    public int getMobKills(int id)
    {
        Integer mob = this.killedMobs.get(id);
        if (mob == null)
        {
            return 0;
        }
        return mob;
    }

    public Map<Integer, Integer> getMobKills()
    {
        return this.killedMobs;
    }

    public long getCompletionTime()
    {
        return this.completionTime;
    }

    public void setCompletionTime(long completionTime)
    {
        this.completionTime = completionTime;
    }

    public int getForfeited()
    {
        return this.forfeited;
    }

    public void setForfeited(int forfeited)
    {
        if (forfeited >= this.forfeited)
        {
            this.forfeited = forfeited;
        }
        else
        {
            throw new IllegalArgumentException("Can't set forfeits to something lower than before.");
        }
    }

    public String getCustomData()
    {
        return this.customData;
    }

    public void setCustomData(String customData)
    {
        this.customData = customData;
    }

    public boolean isDailyQuest()
    {
        switch (this.quest.getId())
        {
            case 11463:
            case 11464:
            case 11465:
            case 11468:
                return true;
        }
        return false;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleQuestStatus.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */