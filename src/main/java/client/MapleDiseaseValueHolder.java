package client;

import java.io.Serializable;

public class MapleDiseaseValueHolder implements Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    public final long startTime;
    public final long length;
    public final MapleDisease disease;

    public MapleDiseaseValueHolder(MapleDisease disease, long startTime, long length)
    {
        this.disease = disease;
        this.startTime = startTime;
        this.length = length;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleDiseaseValueHolder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */