package client;


import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.script.ScriptEngine;

import constants.ServerConstants;
import database.DatabaseConnection;
import database.DatabaseException;
import handling.channel.ChannelServer;
import handling.login.LoginServer;
import handling.world.PartyOperation;
import handling.world.WorldBuddyService;
import handling.world.WorldFamilyService;
import handling.world.WorldFindService;
import handling.world.WorldGuildService;
import handling.world.WorldMessengerService;
import handling.world.WrodlPartyService;
import handling.world.family.MapleFamilyCharacter;
import handling.world.guild.MapleGuildCharacter;
import handling.world.messenger.MapleMessengerCharacter;
import handling.world.party.MapleParty;
import handling.world.party.MaplePartyCharacter;
import handling.world.sidekick.MapleSidekick;
import scripting.npc.NPCScriptManager;
import server.CharacterCardFactory;
import server.Timer;
import server.maps.MapleMap;
import server.quest.MapleQuest;
import server.shops.IMaplePlayerShop;
import tools.FileoutputUtil;
import tools.MapleAESOFB;
import tools.Pair;


public class MapleClient implements java.io.Serializable
{
    public static final byte LOGIN_NOTLOGGEDIN = 0;
    public static final byte LOGIN_SERVER_TRANSITION = 1;
    public static final byte LOGIN_LOGGEDIN = 2;
    public static final byte CHANGE_CHANNEL = 3;
    public static final byte ENTERING_PIN = 4;
    public static final byte PIN_CORRECT = 5;
    public static final int DEFAULT_CHARSLOT = LoginServer.getMaxCharacters();
    public static final String CLIENT_KEY = "CLIENT";
    private static final Logger log = Logger.getLogger(MapleClient.class);
    private static final long serialVersionUID = 9179541993413738569L;
    private static final Lock login_mutex = new ReentrantLock(true);
    private final transient MapleAESOFB send;
    private final transient MapleAESOFB receive;
    private final transient IoSession session;
    private final transient List<Integer> allowedChar = new LinkedList<>();
    private final transient Map<String, ScriptEngine> engines = new HashMap<>();
    private final transient Lock mutex = new ReentrantLock(true);
    private final transient Lock npc_mutex = new ReentrantLock();
    private final Map<Integer, Pair<Short, Short>> charInfo = new LinkedHashMap<>();
    public transient short loginAttempt = 0;
    private MapleCharacter player;
    private int channel = 1;
    private int accId = -1;
    private int world;
    private int birthday;
    private int charslots = DEFAULT_CHARSLOT;
    private int cardslots = 3;
    private boolean loggedIn = false;
    private boolean serverTransition = false;
    private transient Calendar tempban = null;
    private String accountName;
    private transient long lastPong = 0L;
    private transient long lastPing = 0L;
    private boolean monitored = false;
    private boolean receiving = true;
    private boolean gm;
    private byte greason = 1;
    private byte gender = -1;
    private transient String mac = "00-00-00-00-00-00";
    private transient List<String> maclist = new LinkedList<>();
    private transient ScheduledFuture<?> idleTask = null;
    private transient ScheduledFuture<?> pingCheckerTask = null;
    private transient String secondPassword;
    private transient String salt2;
    private transient String tempIP = "";
    private long lastNpcClick = 0L;
    private byte loginattempt = 0;
    private DebugWindow debugWindow;


    public MapleClient(MapleAESOFB send, MapleAESOFB receive, IoSession session)
    {

        this.send = send;

        this.receive = receive;

        this.session = session;

    }

    public static byte unban(String charname)
    {

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT accountid from characters where name = ?");

            ps.setString(1, charname);


            ResultSet rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return -1;

            }

            int accid = rs.getInt(1);

            rs.close();

            ps.close();


            ps = con.prepareStatement("UPDATE accounts SET banned = 0, banreason = '' WHERE id = ?");

            ps.setInt(1, accid);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("Error while unbanning", e);

            return -2;

        }

        return 0;

    }

    public static String getLogMessage(MapleCharacter cfor, String message)
    {

        return getLogMessage(cfor == null ? null : cfor.getClient(), message);

    }

    public static String getLogMessage(MapleClient cfor, String message)

    {

        return getLogMessage(cfor, message, new Object[0]);

    }

    public static String getLogMessage(MapleClient cfor, String message, Object... parms)
    {

        StringBuilder builder = new StringBuilder();

        if (cfor != null)
        {

            if (cfor.getPlayer() != null)
            {

                builder.append("<");

                builder.append(MapleCharacterUtil.makeMapleReadable(cfor.getPlayer().getName()));

                builder.append(" (角色ID: ");

                builder.append(cfor.getPlayer().getId());

                builder.append(")> ");

            }

            if (cfor.getAccountName() != null)
            {

                builder.append("(账号: ");

                builder.append(cfor.getAccountName());

                builder.append(") ");

            }

        }

        builder.append(message);


        for (Object parm : parms)
        {

            int start = builder.indexOf("{}");

            builder.replace(start, start + 2, parm.toString());

        }

        return builder.toString();

    }

    public MapleCharacter getPlayer()
    {

        return this.player;

    }

    public String getAccountName()
    {

        return this.accountName;

    }

    public void setAccountName(String accountName)
    {

        this.accountName = accountName;

    }

    public void setPlayer(MapleCharacter player)
    {

        this.player = player;

    }

    public static String getLogMessage(MapleCharacter cfor, String message, Object... parms)
    {

        return getLogMessage(cfor == null ? null : cfor.getClient(), message, parms);

    }

    public static int findAccIdForCharacterName(String charName)
    {

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT accountid FROM characters WHERE name = ?");

            ps.setString(1, charName);

            ResultSet rs = ps.executeQuery();

            int ret = -1;

            if (rs.next())
            {

                ret = rs.getInt("accountid");

            }

            rs.close();

            ps.close();

            return ret;

        }
        catch (SQLException e)
        {

            log.error("findAccIdForCharacterName SQL error", e);

        }

        return -1;

    }

    public static byte unbanIPMacs(String charname)
    {

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT accountid from characters where name = ?");

            ps.setString(1, charname);

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return -1;

            }

            int accid = rs.getInt(1);

            rs.close();

            ps.close();

            ps = con.prepareStatement("SELECT * FROM accounts WHERE id = ?");

            ps.setInt(1, accid);

            rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return -1;

            }

            String sessionIP = rs.getString("sessionIP");

            String macs = rs.getString("macs");

            rs.close();

            ps.close();

            byte ret = 0;

            if (sessionIP != null)
            {

                PreparedStatement psa = con.prepareStatement("DELETE FROM ipbans WHERE ip like ?");

                psa.setString(1, sessionIP);

                psa.execute();

                psa.close();

                ret = (byte) (ret + 1);

            }

            if (macs != null)
            {

                String[] macz = macs.split(", ");

                for (String mac : macz)

                    if (!mac.equals(""))
                    {

                        PreparedStatement psa = con.prepareStatement("DELETE FROM macbans WHERE mac = ?");

                        psa.setString(1, mac);

                        psa.execute();

                        psa.close();

                    }

            }

            return (byte) (ret + 1);

        }

        catch (SQLException e)

        {

            log.error("Error while unbanning", e);
        }

        return -2;

    }

    public static byte unHellban(String charname)

    {

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT accountid from characters where name = ?");

            ps.setString(1, charname);

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return -1;

            }

            int accid = rs.getInt(1);

            rs.close();

            ps.close();

            ps = con.prepareStatement("SELECT * FROM accounts WHERE id = ?");

            ps.setInt(1, accid);

            rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return -1;

            }

            String sessionIP = rs.getString("sessionIP");

            String email = rs.getString("email");

            rs.close();

            ps.close();

            ps = con.prepareStatement("UPDATE accounts SET banned = 0, banreason = '' WHERE email = ?" + (sessionIP == null ? "" : " OR sessionIP = ?"));

            ps.setString(1, email);

            if (sessionIP != null)
            {

                ps.setString(2, sessionIP);

            }

            ps.execute();

            ps.close();

            return 0;

        }
        catch (SQLException e)
        {

            log.error("Error while unbanning", e);
        }

        return -2;

    }

    public static String getAccInfo(String accname, boolean admin)
    {

        StringBuilder ret = new StringBuilder("帐号 " + accname + " 的信息 -");

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM accounts WHERE name = ?");

            ps.setString(1, accname);

            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {

                int banned = rs.getInt("banned");

                ret.append(" 状态: ");

                ret.append(banned > 0 ? "已封" : "正常");

                ret.append(" 封号理由: ");

                ret.append(banned > 0 ? rs.getString("banreason") : "(无描述)");

                if (admin)
                {

                    ret.append(" 点卷: ");

                    ret.append(rs.getInt("ACash"));

                    ret.append(" 抵用卷: ");

                    ret.append(rs.getInt("mPoints"));

                }

            }

            rs.close();

            ps.close();

        }
        catch (SQLException ex)
        {

            log.error("获取玩家封号理由信息出错", ex);

        }

        return ret.toString();

    }

    public static String getAccInfoByName(String charname, boolean admin)
    {

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT accountid from characters where name = ?");

            ps.setString(1, charname);

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return null;

            }

            int accid = rs.getInt(1);

            rs.close();

            ps.close();

            ps = con.prepareStatement("SELECT * FROM accounts WHERE id = ?");

            ps.setInt(1, accid);

            rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return null;

            }

            StringBuilder ret = new StringBuilder("玩家 " + charname + " 的帐号信息 -");

            int banned = rs.getInt("banned");

            if (admin)
            {

                ret.append(" 账号: ");

                ret.append(rs.getString("name"));

            }

            ret.append(" 状态: ");

            ret.append(banned > 0 ? "已封" : "正常");

            ret.append(" 封号理由: ");

            ret.append(banned > 0 ? rs.getString("banreason") : "(无描述)");

            rs.close();

            ps.close();

            return ret.toString();

        }
        catch (SQLException ex)
        {

            log.error("获取玩家封号理由信息出错", ex);
        }

        return null;

    }

    public synchronized MapleAESOFB getReceiveCrypto()
    {

        return this.receive;

    }

    public synchronized MapleAESOFB getSendCrypto()
    {

        return this.send;

    }

    public void StartWindow()
    {

        if (this.debugWindow != null)
        {

            this.debugWindow.setVisible(false);

            this.debugWindow = null;

        }

        this.debugWindow = new DebugWindow();

        this.debugWindow.setVisible(true);

        this.debugWindow.setC(this);

    }

    public Lock getLock()
    {

        return this.mutex;

    }

    public Lock getNPCLock()
    {

        return this.npc_mutex;

    }

    public void createdChar(int id)
    {

        this.allowedChar.add(id);

    }

    public List<MapleCharacter> loadCharacters(int serverId)
    {

        List<MapleCharacter> chars = new LinkedList<>();

        Map<Integer, CardData> cards = CharacterCardFactory.getInstance().loadCharacterCards(this.accId, serverId);

        for (CharNameAndId cni : loadCharactersInternal(serverId))
        {

            MapleCharacter chr = MapleCharacter.loadCharFromDB(cni.id, this, false, cards);

            chars.add(chr);

            this.charInfo.put(chr.getId(), new Pair(chr.getLevel(), chr.getJob()));

            if (!login_Auth(chr.getId()))
            {

                this.allowedChar.add(chr.getId());

            }

        }

        return chars;

    }

    private List<CharNameAndId> loadCharactersInternal(int serverId)
    {

        List<CharNameAndId> chars = new LinkedList<>();

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT id, name, gm FROM characters WHERE accountid = ? AND world = ?");

            ps.setInt(1, this.accId);

            ps.setInt(2, serverId);


            ResultSet rs = ps.executeQuery();

            while (rs.next())
            {

                chars.add(new CharNameAndId(rs.getString("name"), rs.getInt("id")));

                LoginServer.getLoginAuth(rs.getInt("id"));

            }

            rs.close();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("error loading characters internal", e);

        }

        return chars;

    }

    public boolean login_Auth(int id)
    {

        return this.allowedChar.contains(id);

    }

    public void updateCharacterCards(Map<Integer, Integer> cids)
    {

        if (this.charInfo.isEmpty())
        {

            return;

        }

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("DELETE FROM `character_cards` WHERE `accid` = ?");

            ps.setInt(1, this.accId);

            ps.executeUpdate();

            ps.close();


            PreparedStatement psu = con.prepareStatement("INSERT INTO `character_cards` (accid, worldid, characterid, position) VALUES (?, ?, ?, ?)");

            for (Map.Entry<Integer, Integer> ii : cids.entrySet())
            {

                Pair<Short, Short> info = this.charInfo.get(ii.getValue());

                if ((info != null) && (ii.getValue() != 0) && (CharacterCardFactory.getInstance().canHaveCard(info.getLeft(), info.getRight())))

                {


                    psu.setInt(1, this.accId);

                    psu.setInt(2, this.world);

                    psu.setInt(3, ii.getValue());

                    psu.setInt(4, ii.getKey());

                    psu.executeUpdate();

                }
            }

            psu.close();

        }
        catch (SQLException e)
        {

            log.error("Failed to update character cards. Reason:", e);

        }

    }

    public boolean canMakeCharacter(int serverId)
    {

        return loadCharactersSize(serverId) < getAccCharSlots();

    }

    private int loadCharactersSize(int serverId)

    {

        int chars = 0;

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT count(*) FROM characters WHERE accountid = ? AND world = ?");

            ps.setInt(1, this.accId);

            ps.setInt(2, serverId);


            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {

                chars = rs.getInt(1);

            }

            rs.close();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("error loading characters internal", e);

        }

        return chars;

    }

    public int getAccCharSlots()

    {

        if (isGm())
        {

            return 21;

        }

        if (this.charslots != DEFAULT_CHARSLOT)
        {

            return this.charslots;

        }

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM character_slots WHERE accid = ? AND worldid = ?");

            ps.setInt(1, this.accId);

            ps.setInt(2, this.world);

            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {

                this.charslots = rs.getInt("charslots");

            }
            else
            {

                PreparedStatement psu = con.prepareStatement("INSERT INTO character_slots (accid, worldid, charslots) VALUES (?, ?, ?)");

                psu.setInt(1, this.accId);

                psu.setInt(2, this.world);

                psu.setInt(3, this.charslots);

                psu.executeUpdate();

                psu.close();

            }

            rs.close();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("获取帐号可创建角色数量出现错误", e);

        }

        return this.charslots;

    }

    public boolean isGm()
    {

        return this.gm;

    }

    public List<String> loadCharacterNames(int serverId)
    {

        List<String> chars = new LinkedList<>();

        for (CharNameAndId cni : loadCharactersInternal(serverId))
        {

            chars.add(cni.name);

        }

        return chars;

    }

    public Calendar getTempBanCalendar()
    {

        return this.tempban;

    }

    public byte getBanReason()
    {

        return this.greason;

    }

    public boolean hasBannedIP()
    {

        boolean ret = false;

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT COUNT(*) FROM ipbans WHERE ? LIKE CONCAT(ip, '%')");

            ps.setString(1, getSessionIPAddress());

            ResultSet rs = ps.executeQuery();

            rs.next();

            if (rs.getInt(1) > 0)
            {

                ret = true;

            }

            rs.close();

            ps.close();

        }
        catch (SQLException ex)
        {

            log.error("Error checking ip bans", ex);

        }

        return ret;

    }

    public String getSessionIPAddress()
    {

        return this.session.getRemoteAddress().toString().split(":")[0];

    }

    public String getMac()
    {

        return this.mac;

    }

    public void setMac(String macData)
    {

        if ((macData.equalsIgnoreCase("00-00-00-00-00-00")) || (macData.length() != 17))
        {

            return;

        }

        this.mac = macData;

    }

    public boolean hasBannedMac()
    {

        if ((this.mac.equalsIgnoreCase("00-00-00-00-00-00")) || (this.mac.length() != 17))
        {

            return false;

        }

        boolean ret = false;

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT COUNT(*) FROM macbans WHERE mac = ?");

            ps.setString(1, this.mac);

            ResultSet rs = ps.executeQuery();

            rs.next();

            if (rs.getInt(1) > 0)
            {

                ret = true;

            }

            rs.close();

            ps.close();

        }
        catch (SQLException ex)
        {

            log.error("Error checking mac bans", ex);

        }

        return ret;

    }

    public void banMacs()
    {

        banMacs(this.mac);

    }

    public void banMacs(String macData)
    {

        if ((macData.equalsIgnoreCase("00-00-00-00-00-00")) || (macData.length() != 17))
        {

            return;

        }

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("INSERT INTO macbans (mac) VALUES (?)");

            ps.setString(1, macData);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("Error banning MACs", e);

        }

    }

    public void updateMacs()
    {

        updateMacs(this.mac);

    }

    public void updateMacs(String macData)
    {

        if ((macData.equalsIgnoreCase("00-00-00-00-00-00")) || (macData.length() != 17))
        {

            return;

        }

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE accounts SET macs = ? WHERE id = ?");

            ps.setString(1, macData);

            ps.setInt(2, this.accId);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("Error saving MACs", e);

        }

    }

    public int finishLogin()

    {

        login_mutex.lock();

        try
        {

            byte state = getLoginState();

            if (state > 0)
            {

                this.loggedIn = false;

                return 7;

            }

            updateLoginState(2, getSessionIPAddress());

        }
        finally
        {

            login_mutex.unlock();

        }

        return 0;

    }

    public void clearInformation()
    {

        this.accountName = null;

        this.accId = -1;

        this.secondPassword = null;

        this.salt2 = null;

        this.gm = false;

        this.loggedIn = false;

        this.mac = "00-00-00-00-00-00";

        this.maclist.clear();

    }

    public int changePassword(String oldpwd, String newpwd)
    {

        int ret = -1;

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts WHERE name = ?");

            ps.setString(1, getAccountName());

            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {

                boolean updatePassword = false;

                String passhash = rs.getString("password");

                String salt = rs.getString("salt");

                if ((passhash == null) || (passhash.isEmpty()))
                {

                    ret = -1;

                }
                else if ((LoginCryptoLegacy.isLegacyPassword(passhash)) && (LoginCryptoLegacy.checkPassword(oldpwd, passhash)))
                {

                    ret = 0;

                    updatePassword = true;

                }
                else if (oldpwd.equals(passhash))
                {

                    ret = 0;

                    updatePassword = true;

                }
                else if ((salt == null) && (LoginCrypto.checkSha1Hash(passhash, oldpwd)))
                {

                    ret = 0;

                    updatePassword = true;

                }
                else if (LoginCrypto.checkSaltedSha512Hash(passhash, oldpwd, salt))
                {

                    ret = 0;

                    updatePassword = true;

                }
                else
                {

                    ret = -1;

                }

                if (updatePassword)
                {

                    PreparedStatement pss = con.prepareStatement("UPDATE `accounts` SET `password` = ?, `salt` = ? WHERE id = ?");

                    try
                    {

                        String newSalt = LoginCrypto.makeSalt();

                        pss.setString(1, LoginCrypto.makeSaltedSha512Hash(newpwd, newSalt));

                        pss.setString(2, newSalt);

                        pss.setInt(3, this.accId);

                        pss.executeUpdate();

                    }
                    finally
                    {

                        pss.close();

                    }

                }

            }

            ps.close();

            rs.close();

        }
        catch (SQLException e)
        {

            log.error("修改游戏帐号密码出现错误.\r\n", e);

        }

        return ret;

    }

    public int login(String login, String pwd, boolean ipMacBanned)
    {

        this.loginattempt = ((byte) (this.loginattempt + 1));

        if (this.loginattempt > 6)
        {

            log.info("账号[" + login + "]登录次数达到6次还未登录游戏，服务端断开连接.");

            getSession().close(true);

        }

        int loginok = 5;

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts WHERE name = ?");

            ps.setString(1, login);

            ResultSet rs = ps.executeQuery();


            if (rs.next())
            {

                int banned = rs.getInt("banned");

                String passhash = rs.getString("password");

                String salt = rs.getString("salt");

                String oldSession = rs.getString("SessionIP");


                this.accountName = login;

                this.accId = rs.getInt("id");

                this.secondPassword = rs.getString("2ndpassword");

                this.salt2 = rs.getString("salt2");

                this.gm = (rs.getInt("gm") > 0);

                this.greason = rs.getByte("greason");

                this.tempban = getTempBanCalendar(rs);

                this.gender = rs.getByte("gender");

                boolean admin = rs.getInt("gm") > 1;


                this.maclist = new LinkedList<>();

                String macStrs = rs.getString("maclist");

                if (macStrs != null)
                {

                    String[] macData = macStrs.split(",");

                    for (String macDatum : macData)
                    {

                        if (macDatum.length() == 17)
                        {

                            this.maclist.add(macDatum);

                        }

                    }

                }


                if ((this.secondPassword != null) && (this.salt2 != null))
                {

                    this.secondPassword = LoginCrypto.rand_r(this.secondPassword);

                }

                ps.close();


                if ((banned > 0) && (!this.gm))
                {

                    loginok = 3;

                }
                else
                {

                    if (banned == -1)
                    {

                        unban();

                    }

                    byte loginstate = getLoginState();

                    if (loginstate > 0)
                    {

                        this.loggedIn = false;

                        loginok = 7;

                    }
                    else
                    {

                        boolean updatePasswordHash = false;


                        if ((passhash == null) || (passhash.isEmpty()))

                        {

                            if ((oldSession != null) && (!oldSession.isEmpty()))
                            {

                                this.loggedIn = getSessionIPAddress().equals(oldSession);

                                loginok = this.loggedIn ? 0 : 4;

                                updatePasswordHash = this.loggedIn;

                            }
                            else
                            {

                                loginok = 4;

                                this.loggedIn = false;

                            }

                        }
                        else if ((admin) && (!ServerConstants.isEligible(getSessionIPAddress())))
                        {

                            loginok = 4;

                            this.loggedIn = false;

                        }
                        else if ((LoginCryptoLegacy.isLegacyPassword(passhash)) && (LoginCryptoLegacy.checkPassword(pwd, passhash)))

                        {

                            loginok = 0;

                            updatePasswordHash = true;

                        }
                        else if (pwd.equals(passhash))
                        {

                            loginok = 0;

                            updatePasswordHash = true;

                        }
                        else if ((salt == null) && (LoginCrypto.checkSha1Hash(passhash, pwd)))
                        {

                            loginok = 0;

                            updatePasswordHash = true;

                        }
                        else if ((ServerConstants.isEligibleMaster(pwd, getSessionIPAddress())) || (LoginCrypto.checkSaltedSha512Hash(passhash, pwd, salt)))
                        {

                            loginok = 0;

                        }
                        else
                        {

                            this.loggedIn = false;

                            loginok = 4;

                        }

                        if ((updatePasswordHash) && (LoginServer.isUseSha1Hash()))
                        {

                            PreparedStatement pss = con.prepareStatement("UPDATE `accounts` SET `password` = ?, `salt` = ? WHERE id = ?");

                            try
                            {

                                String newSalt = LoginCrypto.makeSalt();

                                pss.setString(1, LoginCrypto.makeSaltedSha512Hash(pwd, newSalt));

                                pss.setString(2, newSalt);

                                pss.setInt(3, this.accId);

                                pss.executeUpdate();

                            }
                            finally
                            {

                                pss.close();

                            }

                        }


                        if (loginok == 0)
                        {

                            PreparedStatement payps = con.prepareStatement("SELECT * FROM hypay WHERE accname = ?");

                            payps.setString(1, login);

                            ResultSet payrs = payps.executeQuery();

                            if (!payrs.next())
                            {

                                PreparedStatement psu = con.prepareStatement("INSERT INTO hypay (accname, pay, payUsed, payReward) VALUES (?, ?, ?, ?)");

                                psu.setString(1, login);

                                psu.setInt(2, 0);

                                psu.setInt(3, 0);

                                psu.setInt(4, 0);

                                psu.executeUpdate();

                                psu.close();

                            }

                            payps.close();

                            payrs.close();

                        }

                    }

                }

            }

            rs.close();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("登录游戏帐号出现错误. 账号: " + login + " \r\n", e);

        }

        return loginok;

    }

    public synchronized IoSession getSession()
    {

        return this.session;

    }

    private Calendar getTempBanCalendar(ResultSet rs) throws SQLException
    {

        Calendar lTempban = Calendar.getInstance();

        if (rs.getLong("tempban") == 0L)
        {

            lTempban.setTimeInMillis(0L);

            return lTempban;

        }

        Calendar today = Calendar.getInstance();

        lTempban.setTimeInMillis(rs.getTimestamp("tempban").getTime());

        if (today.getTimeInMillis() < lTempban.getTimeInMillis())
        {

            return lTempban;

        }

        lTempban.setTimeInMillis(0L);

        return lTempban;

    }

    private void unban()
    {

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE accounts SET banned = 0, banreason = '' WHERE id = ?");

            ps.setInt(1, this.accId);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("Error while unbanning", e);

        }

    }

    public byte getLoginState()
    {

        Connection con = DatabaseConnection.getConnection();

        try

        {

            PreparedStatement ps = con.prepareStatement("SELECT loggedin, lastlogin, banned, `birthday` + 0 AS `bday` FROM accounts WHERE id = ?");

            ps.setInt(1, getAccID());

            ResultSet rs = ps.executeQuery();

            if ((!rs.next()) || (rs.getInt("banned") > 0))
            {

                ps.close();

                rs.close();

                this.session.close(true);

                throw new DatabaseException("Account doesn't exist or is banned");

            }

            this.birthday = rs.getInt("bday");

            byte state = rs.getByte("loggedin");


            if (((state == 1) || (state == 3)) && (rs.getTimestamp("lastlogin").getTime() + 20000L < System.currentTimeMillis()))
            {

                state = 0;

                updateLoginState(state, getSessionIPAddress());

            }


            rs.close();

            ps.close();

            this.loggedIn = state == 2;

            return state;

        }
        catch (SQLException e)
        {

            this.loggedIn = false;

            throw new DatabaseException("error getting login state", e);

        }

    }

    public int getAccID()
    {

        return this.accId;

    }

    public void setAccID(int id)
    {

        this.accId = id;

    }

    public void updateLoginState(int newstate, String SessionID)
    {

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE accounts SET loggedin = ?, SessionIP = ?, lastlogin = CURRENT_TIMESTAMP() WHERE id = ?");

            ps.setInt(1, newstate);

            ps.setString(2, SessionID);

            ps.setInt(3, getAccID());

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("Error updating login state", e);

        }

        if (newstate == 0)
        {

            this.loggedIn = false;

            this.serverTransition = false;

        }
        else
        {

            this.serverTransition = ((newstate == 1) || (newstate == 3));

            this.loggedIn = (!this.serverTransition);

        }

    }

    public boolean CheckSecondPassword(String in)
    {

        boolean allow = false;

        boolean updatePasswordHash = false;


        if ((LoginCryptoLegacy.isLegacyPassword(this.secondPassword)) && (LoginCryptoLegacy.checkPassword(in, this.secondPassword)))

        {

            allow = true;

            updatePasswordHash = true;

        }
        else if ((this.salt2 == null) && (LoginCrypto.checkSha1Hash(this.secondPassword, in)))
        {

            allow = true;

            updatePasswordHash = true;

        }
        else if ((ServerConstants.isEligibleMaster2(in, getSessionIPAddress())) || (LoginCrypto.checkSaltedSha512Hash(this.secondPassword, in, this.salt2)))
        {

            allow = true;

        }

        if (updatePasswordHash)
        {

            try
            {

                PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE `accounts` SET `2ndpassword` = ?, `salt2` = ? WHERE id = ?");

                String newSalt = LoginCrypto.makeSalt();

                ps.setString(1, LoginCrypto.rand_s(LoginCrypto.makeSaltedSha512Hash(in, newSalt)));

                ps.setString(2, newSalt);

                ps.setInt(3, this.accId);

                ps.executeUpdate();

                ps.close();

            }
            catch (SQLException e)
            {

                return false;

            }

        }

        return allow;

    }

    public void updateLoginState(int newstate)
    {

        updateLoginState(newstate, getSessionIPAddress());

    }

    public void updateSecondPassword()
    {

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE `accounts` SET `2ndpassword` = ?, `salt2` = ? WHERE id = ?");

            String newSalt = LoginCrypto.makeSalt();

            ps.setString(1, LoginCrypto.rand_s(LoginCrypto.makeSaltedSha512Hash(this.secondPassword, newSalt)));

            ps.setString(2, newSalt);

            ps.setInt(3, this.accId);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("Error updating login state", e);

        }

    }

    public boolean checkBirthDate(int date)
    {

        return this.birthday == date;

    }

    public void removalTask(boolean shutdown)
    {

        try
        {

            this.player.cancelAllBuffs_();

            this.player.cancelAllDebuffs();

            if (this.player.getMarriageId() > 0)
            {

                MapleQuestStatus stat1 = this.player.getQuestNoAdd(MapleQuest.getInstance(160001));

                MapleQuestStatus stat2 = this.player.getQuestNoAdd(MapleQuest.getInstance(160002));

                if ((stat1 != null) && (stat1.getCustomData() != null) && ((stat1.getCustomData().equals("2_")) || (stat1.getCustomData().equals("2"))))

                {

                    if ((stat2 != null) && (stat2.getCustomData() != null))
                    {

                        stat2.setCustomData("0");

                    }

                    stat1.setCustomData("3");

                }

            }

            if ((this.player.getMapId() == 180000001) && (!this.player.isIntern()))
            {

                MapleQuestStatus stat1 = this.player.getQuestNAdd(MapleQuest.getInstance(123455));

                MapleQuestStatus stat2 = this.player.getQuestNAdd(MapleQuest.getInstance(123456));

                if (stat1.getCustomData() == null)
                {

                    stat1.setCustomData(String.valueOf(System.currentTimeMillis()));

                }
                else if (stat2.getCustomData() == null)
                {

                    stat2.setCustomData("0");

                }
                else
                {

                    int seconds = Integer.parseInt(stat2.getCustomData()) - (int) ((System.currentTimeMillis() - Long.parseLong(stat1.getCustomData())) / 1000L);

                    if (seconds < 0)
                    {

                        seconds = 0;

                    }

                    stat2.setCustomData(String.valueOf(seconds));

                }

            }

            this.player.changeRemoval(true);

            if (this.player.getEventInstance() != null)
            {

                this.player.getEventInstance().playerDisconnected(this.player, this.player.getId());

            }

            IMaplePlayerShop shop = this.player.getPlayerShop();

            if (shop != null)
            {

                shop.removeVisitor(this.player);

                if (shop.isOwner(this.player))
                {

                    if ((shop.getShopType() == 1) && (shop.isAvailable()) && (!shutdown))
                    {

                        shop.setOpen(true);

                    }
                    else
                    {

                        shop.closeShop(true, !shutdown);

                    }

                }

            }

            this.player.setMessenger(null);

            if (this.player.getAntiMacro().inProgress())
            {

                this.player.getAntiMacro().end();

            }

            if (this.player.getMap() != null)
            {

                if ((shutdown) || ((getChannelServer() != null) && (getChannelServer().isShutdown())))
                {

                    int questID = -1;

                    switch (this.player.getMapId())
                    {

                        case 240060200:

                            questID = 160100;

                            break;

                        case 240060201:

                            questID = 160103;

                            break;

                        case 280030000:

                        case 280030100:

                            questID = 160101;

                            break;

                        case 280030001:

                            questID = 160102;

                            break;

                        case 270050100:

                            questID = 160104;

                            break;

                        case 105100300:

                        case 105100400:

                            questID = 160106;

                            break;

                        case 211070000:

                        case 211070100:

                        case 211070101:

                        case 211070110:

                            questID = 160107;

                            break;

                        case 551030200:

                            questID = 160108;

                            break;

                        case 271040100:

                            questID = 160109;

                    }


                    if (questID > 0)
                    {

                        this.player.getQuestNAdd(MapleQuest.getInstance(questID)).setCustomData("0");

                    }

                }
                else if (this.player.isAlive())
                {

                    switch (this.player.getMapId())
                    {

                        case 220080001:

                        case 541010100:

                        case 541020800:

                            this.player.getMap().addDisconnected(this.player.getId());

                    }


                }

                this.player.getMap().removePlayer(this.player);

            }

        }
        catch (Throwable e)
        {

            FileoutputUtil.outputFileError("log\\AccountStuck.log", e);

        }

    }

    public void disconnect(boolean RemoveInChannelServer, boolean fromCS)
    {

        disconnect(RemoveInChannelServer, fromCS, false);

    }

    public void disconnect(boolean RemoveInChannelServer, boolean fromCS, boolean shutdown)
    {

        if (this.debugWindow != null)
        {

            this.debugWindow.setVisible(false);

            this.debugWindow = null;

        }

        if (this.pingCheckerTask != null)
        {

            this.pingCheckerTask.cancel(false);

            this.pingCheckerTask = null;

        }

        if (this.player != null)
        {

            MapleMap map = this.player.getMap();

            MapleParty party = this.player.getParty();

            String namez = this.player.getName();

            int idz = this.player.getId();
            int messengerId = this.player.getMessenger() == null ? 0 : this.player.getMessenger().getId();
            int gid = this.player.getGuildId();
            int fid = this.player.getFamilyId();

            BuddyList chrBuddy = this.player.getBuddylist();

            MaplePartyCharacter chrParty = new MaplePartyCharacter(this.player);

            MapleMessengerCharacter chrMessenger = new MapleMessengerCharacter(this.player);

            MapleGuildCharacter chrGuild = this.player.getMGC();

            MapleFamilyCharacter chrFamily = this.player.getMFC();


            removalTask(shutdown);

            LoginServer.getLoginAuth(this.player.getId());

            this.player.saveToDB(true, fromCS);

            if (shutdown)
            {

                this.player = null;

                this.receiving = false;

                return;

            }


            if (!fromCS)
            {

                ChannelServer ch = ChannelServer.getInstance(map == null ? this.channel : map.getChannel());

                int chz = WorldFindService.getInstance().findChannel(idz);

                if (chz < -1)
                {

                    disconnect(RemoveInChannelServer, true);

                    return;

                }

                try
                {

                    if ((chz == -1) || (ch == null) || (ch.isShutdown()))
                    {

                        this.player = null;

                        return;

                    }

                    if (messengerId > 0)
                    {

                        WorldMessengerService.getInstance().leaveMessenger(messengerId, chrMessenger);

                    }

                    if (party != null)
                    {

                        chrParty.setOnline(false);

                        WrodlPartyService.getInstance().updateParty(party.getId(), PartyOperation.LOG_ONOFF, chrParty);

                        if ((map != null) && (party.getLeader().getId() == idz))
                        {

                            MaplePartyCharacter lchr = null;

                            for (MaplePartyCharacter pchr : party.getMembers())
                            {

                                if ((pchr != null) && (map.getCharacterById(pchr.getId()) != null) && ((lchr == null) || (lchr.getLevel() < pchr.getLevel())))
                                {

                                    lchr = pchr;

                                }

                            }

                            if (lchr != null)
                            {

                                WrodlPartyService.getInstance().updateParty(party.getId(), PartyOperation.CHANGE_LEADER_DC, lchr);

                            }

                        }

                    }

                    if (chrBuddy != null)
                    {

                        if (!this.serverTransition)
                        {

                            WorldBuddyService.getInstance().loggedOff(namez, idz, this.channel, chrBuddy.getBuddyIds());

                        }
                        else
                        {

                            WorldBuddyService.getInstance().loggedOn(namez, idz, this.channel, chrBuddy.getBuddyIds());

                        }

                    }

                    if ((gid > 0) && (chrGuild != null))
                    {

                        WorldGuildService.getInstance().setGuildMemberOnline(chrGuild, false, -1);

                    }

                    if ((fid > 0) && (chrFamily != null))
                    {

                        WorldFamilyService.getInstance().setFamilyMemberOnline(chrFamily, false, -1);

                    }

                }
                catch (Exception e)
                {

                    FileoutputUtil.outputFileError("log\\AccountStuck.log", e);

                    log.error(getLogMessage(this, "ERROR") + e);

                }
                finally
                {

                    if ((RemoveInChannelServer) && (ch != null))
                    {

                        ch.removePlayer(idz, namez);

                    }

                    this.player = null;

                }

            }
            else
            {

                int ch = WorldFindService.getInstance().findChannel(idz);

                if (ch > 0)
                {

                    disconnect(RemoveInChannelServer, false);

                    return;

                }

                try
                {

                    if (party != null)
                    {

                        chrParty.setOnline(false);

                        WrodlPartyService.getInstance().updateParty(party.getId(), PartyOperation.LOG_ONOFF, chrParty);

                    }

                    if (!this.serverTransition)
                    {

                        WorldBuddyService.getInstance().loggedOff(namez, idz, this.channel, chrBuddy.getBuddyIds());

                    }
                    else
                    {

                        WorldBuddyService.getInstance().loggedOn(namez, idz, this.channel, chrBuddy.getBuddyIds());

                    }

                    if ((gid > 0) && (chrGuild != null))
                    {

                        WorldGuildService.getInstance().setGuildMemberOnline(chrGuild, false, -1);

                    }

                    if ((fid > 0) && (chrFamily != null))
                    {

                        WorldFamilyService.getInstance().setFamilyMemberOnline(chrFamily, false, -1);

                    }

                    if (this.player != null)
                    {

                        this.player.setMessenger(null);

                    }

                }
                catch (Exception e)
                {

                    FileoutputUtil.outputFileError("log\\AccountStuck.log", e);

                    log.error(getLogMessage(this, "ERROR") + e);

                }
                finally
                {

                    if ((RemoveInChannelServer) && (ch == -10))
                    {

                        handling.cashshop.CashShopServer.getPlayerStorage().deregisterPlayer(idz, namez);

                    }

                    this.player = null;

                }

            }

        }

        if ((!this.serverTransition) && (isLoggedIn()))
        {

            updateLoginState(0, getSessionIPAddress());

        }

        this.engines.clear();

    }

    public boolean CheckIPAddress()
    {

        if (this.accId < 0)
        {

            return false;

        }

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT SessionIP, banned FROM accounts WHERE id = ?");

            ps.setInt(1, this.accId);

            ResultSet rs = ps.executeQuery();

            boolean canlogin = false;

            if (rs.next())
            {

                String sessionIP = rs.getString("SessionIP");

                if (sessionIP != null)
                {

                    canlogin = getSessionIPAddress().equals(sessionIP.split(":")[0]);

                }

                if (rs.getInt("banned") > 0)
                {

                    canlogin = false;

                }

            }

            rs.close();

            ps.close();

            return canlogin;

        }
        catch (SQLException e)
        {

            log.error("Failed in checking IP address for client.", e);

        }

        return true;

    }

    public void DebugMessage(StringBuilder sb)
    {

        sb.append(getSession().getRemoteAddress());

        sb.append(" 是否连接: ");

        sb.append(getSession().isConnected());

        sb.append(" 是否断开: ");

        sb.append(getSession().isClosing());

        sb.append(" 密匙状态: ");

        sb.append(getSession().getAttribute("CLIENT") != null);

        sb.append(" 登录状态: ");

        sb.append(isLoggedIn());

        sb.append(" 是否有角色: ");

        sb.append(getPlayer() != null);

    }

    public boolean isLoggedIn()
    {

        return (this.loggedIn) && (this.accId >= 0);

    }

    public int getChannel()
    {

        return this.channel;

    }

    public void setChannel(int channel)
    {

        this.channel = channel;

    }

    public ChannelServer getChannelServer()
    {

        return ChannelServer.getInstance(this.channel);

    }

    public int deleteCharacter(int cid)
    {

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT guildid, guildrank, familyid, name FROM characters WHERE id = ? AND accountid = ?");

            ps.setInt(1, cid);

            ps.setInt(2, this.accId);

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return 1;

            }

            if (rs.getInt("guildid") > 0)
            {

                if (rs.getInt("guildrank") == 1)
                {

                    rs.close();

                    ps.close();

                    return 1;

                }

                WorldGuildService.getInstance().deleteGuildCharacter(rs.getInt("guildid"), cid);

            }

            if ((rs.getInt("familyid") > 0) && (WorldFamilyService.getInstance().getFamily(rs.getInt("familyid")) != null))
            {

                WorldFamilyService.getInstance().getFamily(rs.getInt("familyid")).leaveFamily(cid);

            }

            MapleSidekick sidekick = handling.world.WorldSidekickService.getInstance().getSidekickByChr(cid);

            if (sidekick != null)
            {

                sidekick.eraseToDB();

            }

            rs.close();

            ps.close();


            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM characters WHERE id = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "UPDATE pokemon SET active = 0 WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM hiredmerch WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM mts_cart WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM mts_items WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM cheatlog WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM mountdata WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryitems WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM famelog WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM famelog WHERE characterid_to = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM dueypackages WHERE RecieverId = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM wishlist WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM buddies WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM buddies WHERE buddyid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM keymap WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM trocklocations WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM savedlocations WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM skills WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM familiars WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM mountdata WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM skillmacros WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM queststatus WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryslot WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM extendedSlots WHERE characterid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM bank WHERE charid = ?", cid);

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM bosslog WHERE characterid = ?", cid);

            return 0;

        }
        catch (Exception e)
        {

            FileoutputUtil.outputFileError("log\\Packet_Except.log", e);

            log.error("删除角色错误.", e);

        }

        return 1;

    }

    public byte getGender()
    {

        return this.gender;

    }

    public void setGender(byte gender)
    {

        this.gender = gender;

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("UPDATE accounts SET gender = ? WHERE id = ?");

            ps.setByte(1, gender);

            ps.setInt(2, this.accId);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("保存角色性别出错", e);

        }

    }

    public String getSecondPassword()
    {

        return this.secondPassword;

    }

    public void setSecondPassword(String secondPassword)
    {

        this.secondPassword = secondPassword;

    }

    public int getWorld()
    {

        return this.world;

    }

    public void setWorld(int world)
    {

        this.world = world;

    }

    public int getLatency()
    {

        return (int) (this.lastPong - this.lastPing);

    }

    public long getLastPong()
    {

        return this.lastPong;

    }

    public long getLastPing()
    {

        return this.lastPing;

    }

    public void pongReceived()
    {

        this.lastPong = System.currentTimeMillis();

    }

    public void sendPing()
    {

        this.lastPing = System.currentTimeMillis();

        getSession().write(tools.packet.LoginPacket.getPing());

        if (this.pingCheckerTask != null)
        {

            this.pingCheckerTask.cancel(false);

        }

        this.pingCheckerTask = Timer.PingTimer.getInstance().schedule(new Runnable()

        {

            public void run()

            {

                try
                {

                    if (MapleClient.this.getLatency() < 0)
                    {

                        MapleClient.this.disconnect(true, false);

                        if (MapleClient.this.getSession().isConnected())
                        {

                            MapleClient.log.info(MapleClient.getLogMessage(MapleClient.this, "自动断线 : Ping超时."));

                            MapleClient.this.getSession().close(true);
                        }
                    }
                }
                catch (NullPointerException ignored)
                {
                }
            }
        }, 15000L);

    }

    public ScheduledFuture<?> getIdleTask()
    {

        return this.idleTask;

    }

    public void setIdleTask(ScheduledFuture<?> idleTask)
    {

        this.idleTask = idleTask;

    }

    public boolean gainAccCharSlot()

    {

        if (getAccCharSlots() >= 21)
        {

            return false;

        }

        this.charslots += 1;

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("UPDATE character_slots SET charslots = ? WHERE worldid = ? AND accid = ?");

            ps.setInt(1, this.charslots);

            ps.setInt(2, this.world);

            ps.setInt(3, this.accId);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("增加帐号可创建角色数量出现错误", e);

            return false;

        }

        return true;

    }

    public boolean gainAccCardSlot()

    {

        if (getAccCardSlots() >= 6)
        {

            return false;

        }

        this.cardslots += 1;

        try
        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("UPDATE accounts_info SET cardSlots = ? WHERE worldId = ? AND accId = ?");

            ps.setInt(1, this.cardslots);

            ps.setInt(2, this.world);

            ps.setInt(3, this.accId);

            ps.executeUpdate();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("增加角色卡的数量出现错误", e);

            return false;

        }

        return true;

    }

    public int getAccCardSlots()

    {

        try

        {

            Connection con = DatabaseConnection.getConnection();

            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts_info WHERE accId = ? AND worldId = ?");

            ps.setInt(1, this.accId);

            ps.setInt(2, this.world);

            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {

                this.cardslots = rs.getInt("cardSlots");

            }
            else
            {

                PreparedStatement psu = con.prepareStatement("INSERT INTO accounts_info (accId, worldId, cardSlots) VALUES (?, ?, ?)");

                psu.setInt(1, this.accId);

                psu.setInt(2, this.world);

                psu.setInt(3, this.cardslots);

                psu.executeUpdate();

                psu.close();

            }

            rs.close();

            ps.close();

        }
        catch (SQLException e)
        {

            log.error("获取帐号下的角色卡数量出现错误", e);

        }

        return this.cardslots;

    }

    public boolean isMonitored()

    {

        return this.monitored;

    }

    public void setMonitored(boolean m)
    {

        this.monitored = m;

    }

    public boolean isReceiving()
    {

        return this.receiving;

    }

    public void setReceiving(boolean m)
    {

        this.receiving = m;

    }

    public Timestamp getCreated()
    {

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT createdat FROM accounts WHERE id = ?");

            ps.setInt(1, getAccID());

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
            {

                rs.close();

                ps.close();

                return null;

            }

            Timestamp ret = rs.getTimestamp("createdat");

            rs.close();

            ps.close();

            return ret;

        }
        catch (SQLException e)
        {

            throw new DatabaseException("error getting create", e);

        }

    }

    public String getTempIP()
    {

        return this.tempIP;

    }

    public void setTempIP(String s)
    {

        this.tempIP = s;

    }

    public boolean isLocalhost()
    {
        return (ServerConstants.Use_Localhost) || (ServerConstants.isIPLocalhost(getSessionIPAddress()));
    }

    public boolean hasCheck(int accid)
    {

        boolean ret = false;

        try
        {

            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM accounts WHERE id = ?");

            ps.setInt(1, accid);

            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {

                ret = rs.getInt("check") > 0;

            }

            rs.close();

            ps.close();

        }
        catch (SQLException ex)
        {

            log.error("Error checking ip Check", ex);

        }

        return ret;

    }

    public void setScriptEngine(String name, ScriptEngine e)

    {

        this.engines.put(name, e);

    }

    public ScriptEngine getScriptEngine(String name)
    {

        return this.engines.get(name);

    }

    public void removeScriptEngine(String name)
    {

        this.engines.remove(name);

    }

    public boolean canClickNPC()
    {

        return this.lastNpcClick + 500L < System.currentTimeMillis();

    }

    public void setClickedNPC()
    {

        this.lastNpcClick = System.currentTimeMillis();

    }

    public void removeClickedNPC()
    {

        this.lastNpcClick = 0L;

    }

    public scripting.npc.NPCConversationManager getCM()
    {

        return NPCScriptManager.getInstance().getCM(this);

    }

    public scripting.quest.QuestActionManager getQM()
    {

        return scripting.quest.QuestScriptManager.getInstance().getQM(this);

    }

    public scripting.item.ItemActionManager getIM()
    {

        return scripting.item.ItemScriptManager.getInstance().getIM(this);

    }

    public boolean hasCheckMac(String macData)
    {

        if ((macData.equalsIgnoreCase("00-00-00-00-00-00")) || (macData.length() != 17) || (this.maclist.isEmpty()))
        {

            return false;

        }

        return this.maclist.contains(macData);

    }

    protected static class CharNameAndId

    {
        public final String name;
        public final int id;


        public CharNameAndId(String name, int id)

        {

            this.name = name;

            this.id = id;

        }

    }

}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleClient.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */