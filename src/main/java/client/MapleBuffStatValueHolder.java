package client;

import server.MapleStatEffect;

public class MapleBuffStatValueHolder
{
    public final MapleStatEffect effect;
    public final long startTime;
    public final int localDuration;
    public final int fromChrId;
    public int value;
    public java.util.concurrent.ScheduledFuture<?> schedule;

    public MapleBuffStatValueHolder(MapleStatEffect effect, long startTime, java.util.concurrent.ScheduledFuture<?> schedule, int value, int localDuration, int fromChrId)
    {
        this.effect = effect;
        this.startTime = startTime;
        this.schedule = schedule;
        this.value = value;
        this.localDuration = localDuration;
        this.fromChrId = fromChrId;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleBuffStatValueHolder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */