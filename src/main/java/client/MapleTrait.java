package client;

import constants.GameConstants;
import tools.MaplePacketCreator;

public class MapleTrait
{
    private final MapleTraitType type;
    private int totalExp = 0;
    private int localTotalExp = 0;
    private short exp = 0;
    private byte level = 0;

    public MapleTrait(MapleTraitType t)
    {
        this.type = t;
    }

    public void addExp(int exp)
    {
        this.totalExp += exp;
        this.localTotalExp += exp;
        if (exp != 0)
        {
            recalcLevel();
        }
    }

    public boolean recalcLevel()
    {
        if (this.totalExp < 0)
        {
            this.totalExp = 0;
            this.localTotalExp = 0;
            this.level = 0;
            this.exp = 0;
            return false;
        }
        int oldLevel = this.level;
        for (byte i = 0; i < 100; i = (byte) (i + 1))
        {
            if (GameConstants.getTraitExpNeededForLevel(i) > this.localTotalExp)
            {
                this.exp = ((short) (GameConstants.getTraitExpNeededForLevel(i) - this.localTotalExp));
                this.level = ((byte) (i - 1));
                return this.level > oldLevel;
            }
        }
        this.exp = 0;
        this.level = 100;
        this.totalExp = GameConstants.getTraitExpNeededForLevel(this.level);
        this.localTotalExp = this.totalExp;
        return this.level > oldLevel;
    }

    public void addExp(int exp, MapleCharacter chr)
    {
        addTrueExp(exp * chr.getClient().getChannelServer().getTraitRate(), chr);
    }

    public void addTrueExp(int exp, MapleCharacter chr)
    {
        if (exp != 0)
        {
            this.totalExp += exp;
            this.localTotalExp += exp;
            chr.updateSingleStat(this.type.getStat(), this.totalExp);
            chr.getClient().getSession().write(MaplePacketCreator.showTraitGain(this.type, exp));
            recalcLevel();
        }
    }

    public int getLevel()
    {
        return this.level;
    }

    public int getExp()
    {
        return this.exp;
    }

    public void setExp(int exp)
    {
        this.totalExp = exp;
        this.localTotalExp = exp;
        recalcLevel();
    }

    public int getTotalExp()
    {
        return this.totalExp;
    }

    public int getLocalTotalExp()
    {
        return this.localTotalExp;
    }

    public void addLocalExp(int exp)
    {
        this.localTotalExp += exp;
    }

    public void clearLocalExp()
    {
        this.localTotalExp = this.totalExp;
    }

    public MapleTraitType getType()
    {
        return this.type;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\client\MapleTrait.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */