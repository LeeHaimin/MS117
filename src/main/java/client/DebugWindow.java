package client;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.UIManager;

public class DebugWindow extends javax.swing.JFrame
{
    private MapleClient c;
    private javax.swing.JButton jButton1;
    private JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private JTextArea jTextArea1;

    public DebugWindow()
    {
        initComponents();
        setLocationRelativeTo(null);
    }

    private void initComponents()
    {
        this.jScrollPane1 = new javax.swing.JScrollPane();
        this.jTextArea1 = new JTextArea();
        this.jButton1 = new javax.swing.JButton();
        this.jLabel1 = new JLabel();

        setDefaultCloseOperation(2);
        setTitle("调试窗口");
        setResizable(false);

        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setRows(5);
        this.jScrollPane1.setViewportView(this.jTextArea1);

        this.jButton1.setText("测试封包");
        this.jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                DebugWindow.this.jButton1ActionPerformed(evt);
            }

        });
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 446, 32767).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addComponent(this.jLabel1, -1, -1, 32767).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton1))).addContainerGap()));

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 253, 32767).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.jButton1).addGroup(layout.createSequentialGroup().addGap(0, 0, 0).addComponent(this.jLabel1, -1, -1, 32767))).addContainerGap()));


        pack();
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
    {
        if (this.c == null)
        {
            this.jLabel1.setText("发送失败，客户为空.");
            return;
        }
        byte[] data = tools.HexTool.getByteArrayFromHexString(this.jTextArea1.getText());
        this.jTextArea1.setText(null);
        this.jLabel1.setText(null);
        if ((this.c != null) && (data.length >= 2))
        {
            this.c.getSession().write(tools.MaplePacketCreator.testPacket(data));
            this.jLabel1.setText("发送成功，发送的封包长度: " + data.length);
        }
        else
        {
            this.jLabel1.setText("发送失败，发送的封包长度: " + data.length);
        }
    }

    public static void main(String[] args)
    {
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex)
        {
            Logger.getLogger(DebugWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            Logger.getLogger(DebugWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            Logger.getLogger(DebugWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            Logger.getLogger(DebugWindow.class.getName()).log(Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new DebugWindow().setVisible(true);
            }
        });
    }

    public MapleClient getC()
    {
        return this.c;
    }

    public void setC(MapleClient c)
    {
        this.c = c;
        if (c.getPlayer() != null)
        {
            setTitle("玩家: " + c.getPlayer().getName() + " - 封包测试");
        }
    }
}