package GUI.panel;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import GUI.IPanel;
import GUI.KinMS;
import client.MapleCharacter;
import handling.channel.ChannelServer;

/**
 * 创建者 李海民
 * 文件说明：
 * 日期 2019/12/4 21:24
 **/
public class RewardPanel extends JPanel implements IPanel
{
    JTextField tfCount;
    JTextField tfType;
    JButton jButton15;
    JButton btnCallMonster;
    private int num = 0, type = 0;
    private String sCount, sType;

    @Override
    public void init()
    {
        tfCount = new JTextField();
        tfType = new JTextField();
        jButton15 = new JButton();
        btnCallMonster = new JButton();
        btnCallMonster.setText("召唤怪物");


//        tfCount.setText("输入数量");

//        tfType.setText("1点卷/2抵用/3金币/4经验");

        jButton15.setText("发放点卷");
        jButton15.addActionListener(this::sendStamps);

        btnCallMonster.addActionListener(this::callMonster);
        GroupLayout jPanel2Layout = new GroupLayout(this);
        this.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(tfCount,
                GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(tfType, GroupLayout.PREFERRED_SIZE,
                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jButton15).addComponent(btnCallMonster).addContainerGap(117, Short.MAX_VALUE)));

        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(tfCount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(tfType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jButton15).addComponent(btnCallMonster)).addContainerGap(203, Short.MAX_VALUE)));

    }

    public boolean isEmpty(String s)
    {
        return s == null || s.length() == 0;
    }

    private void getInput()
    {
        sCount = tfCount.getText();
        sType = tfType.getText();
        if (isEmpty(sCount) || isEmpty(sType))
        {
            return;
        }
        try
        {
            num = Integer.parseInt(sCount);
            type = Integer.parseInt(sType);
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "错误!\r\n" + ex);
        }
    }

    private void sendStamps(java.awt.event.ActionEvent evt)
    {
        int sendCount = 0;
        getInput();
        for (ChannelServer cSer : ChannelServer.getAllInstances())
        {
            for (MapleCharacter mch : cSer.getPlayerStorage().getAllCharacters())
            {
                switch (type)
                {
                    case 1:
                    case 2:
                        mch.modifyCSPoints(type, num);
                        String cash = type == 1 ? "点卷" : "抵用卷";
                        mch.startMapEffect("管理员发放" + num + cash + "给在线的所有玩家！快感谢管理员吧！", 5121009);
                        break;
                    case 3:
                        mch.gainMeso(num, true);
                        mch.startMapEffect("管理员发放" + num + "冒险币给在线的所有玩家！快感谢管理员吧！", 5121009);
                        break;
                    case 4:
                        mch.gainExp(num, true, false, true);
                        mch.startMapEffect("管理员发放" + num + "经验给在线的所有玩家！快感谢管理员吧！", 5121009);
                        break;
                }
                sendCount++;
            }
        }
        String sendType = type == 1 ? "点卷" : type == 2 ? "抵用卷" : type == 3 ? "金币" : "经验";
        String output = "一共发放[" + num * sendCount + "]." + sendType + "!一共发放给了" + sendCount + "人！";
        KinMS.getInstance().printChatLog(output);
    }

    private void callMonster(java.awt.event.ActionEvent evt)
    {
        getInput();
        ChannelServer channelServer = ChannelServer.getInstance(1);
        if (null == channelServer)
        {
            return;
        }

        for (MapleCharacter mch : channelServer.getPlayerStorage().getAllCharacters())
        {
            for (int i = 0; i < num; i++)
            {
                ChannelServer.getInstance(1).createBoss(mch.getMap().getId(), sType, 0, 0, 10);
            }
        }
        //8130100
    }
}
