package GUI.panel;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import GUI.IPanel;
import GUI.KinMS;
import client.MapleCharacter;
import client.inventory.Equip;
import client.inventory.ItemFlag;
import client.inventory.MapleInventoryType;
import constants.GameConstants;
import handling.channel.ChannelServer;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import tools.MaplePacketCreator;

/**
 * 创建者 李海民
 * 文件说明：
 * 日期 2019/12/3 19:45
 **/
public class SendGoodsPanel extends JPanel implements IPanel
{
    final JTextField jTextField5;
    final JTextField jTextField6;
    final JTextField jTextField7;
    final JTextField jTextField8;
    final JTextField jTextField9;
    final JTextField jTextField10;
    final JTextField jTextField11;
    final JTextField jTextField12;
    final JTextField jTextField13;
    final JTextField jTextField14;
    final JTextField jTextField15;
    final JTextField jTextField16;
    final JTextField jTextField17;
    final JTextField jTextField18;
    final JTextField jTextField19;
    final JButton jButton14;


    final JTextField jTextField3;
    final JTextField jTextField4;


    final JButton btnSendBroadcast;
    final JTextField tfBroadcast;

    public SendGoodsPanel()
    {
        jTextField5 = new JTextField();
        jTextField6 = new JTextField();
        jTextField7 = new JTextField();
        jTextField8 = new JTextField();
        jTextField9 = new JTextField();
        jTextField10 = new JTextField();
        jTextField11 = new JTextField();
        jTextField12 = new JTextField();
        jTextField13 = new JTextField();
        jTextField14 = new JTextField();
        jTextField15 = new JTextField();
        jTextField16 = new JTextField();
        jTextField17 = new JTextField();
        jTextField18 = new JTextField();
        jTextField19 = new JTextField();
        jTextField3 = new JTextField();
        jTextField4 = new JTextField();
        jButton14 = new JButton();
        btnSendBroadcast = new JButton();
        jButton14.addActionListener(this::jButton14ActionPerformed);


        tfBroadcast = new JTextField();
        btnSendBroadcast.addActionListener(this::btnSendBroadcastAction);

        jTextField3.setText("05010013");
        jTextField3.setText("LLLLL");
        jTextField5.setText("1");
        jTextField6.setText("32767");
        jTextField7.setText("32767");
        jTextField8.setText("32767");
        jTextField9.setText("32767");
        jTextField10.setText("30000");
        jTextField11.setText("30000");
        jTextField12.setText("9");
        jTextField13.setText("GM");
        jTextField14.setText("0");
        jTextField15.setText("可以交易");
        jTextField16.setText("32767");
        jTextField17.setText("32767");
        jTextField18.setText("30000");
        jTextField19.setText("30000");
        jButton14.setText("给予物品");


        btnSendBroadcast.setText("公告发布");
    }

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt)
    {
        createGoods();
    }

    private void btnSendBroadcastAction(java.awt.event.ActionEvent evt)
    {
        sendNoticeBroadcast();
    }

    private void createGoods()
    {
        String goodName, id, count, str, agi, head, luk, hp, mp, upgradeCount, creatorName, sendTime, attackNum, magicNum, physicDefence, magicDefence;

        try
        {
            String 名字;
            if ("玩家名字".equals(jTextField3.getText()))
            {
                名字 = "";
            }
            else
            {
                名字 = jTextField3.getText();
            }
//            名字 = "LLLLL";
            int 物品ID;
            if ("物品ID".equals(jTextField4.getText()))
            {
                物品ID = 0;
            }
            else
            {
                物品ID = Integer.parseInt(jTextField4.getText());
            }
//            物品ID = 1432030;
            int 数量;
            if ("数量".equals(jTextField5.getText()))
            {
                数量 = 0;
            }
            else
            {
                数量 = Integer.parseInt(jTextField5.getText());
            }

            int 力量;
            if ("力量".equals(jTextField6.getText()))
            {
                力量 = 0;
            }
            else
            {
                力量 = Integer.parseInt(jTextField6.getText());
            }

            int 敏捷;
            if ("敏捷".equals(jTextField7.getText()))
            {
                敏捷 = 0;
            }
            else
            {
                敏捷 = Integer.parseInt(jTextField7.getText());
            }

            int 智力;
            if ("智力".equals(jTextField8.getText()))
            {
                智力 = 0;
            }
            else
            {
                智力 = Integer.parseInt(jTextField8.getText());
            }

            int 运气;
            if ("运气".equals(jTextField9.getText()))
            {
                运气 = 0;
            }
            else
            {
                运气 = Integer.parseInt(jTextField9.getText());
            }

            int HP;
            if ("HP设置".equals(jTextField10.getText()))
            {
                HP = 0;
            }
            else
            {
                HP = Integer.parseInt(jTextField10.getText());
            }

            int MP;
            if ("MP设置".equals(jTextField11.getText()))
            {
                MP = 0;
            }
            else
            {
                MP = Integer.parseInt(jTextField11.getText());
            }
            int 可加卷次数;
            if ("加卷次数".equals(jTextField12.getText()))
            {
                可加卷次数 = 0;
            }
            else
            {
                可加卷次数 = Integer.parseInt(jTextField12.getText());
            }

            String 制作人名字;
            if ("制作人".equals(jTextField13.getText()))
            {
                制作人名字 = "";
            }
            else
            {
                制作人名字 = jTextField13.getText();
            }

            int 给予时间;
            if ("给予物品时间".equals(jTextField14.getText()))
            {
                给予时间 = 0;
            }
            else
            {
                给予时间 = Integer.parseInt(jTextField14.getText());
            }

            String 是否可以交易 = jTextField15.getText();

            int 攻击力;
            if ("攻击力".equals(jTextField16.getText()))
            {
                攻击力 = 0;
            }
            else
            {
                攻击力 = Integer.parseInt(jTextField16.getText());
            }

            int 魔法力;
            if ("魔法力".equals(jTextField17.getText()))
            {
                魔法力 = 0;
            }
            else
            {
                魔法力 = Integer.parseInt(jTextField17.getText());
            }

            int 物理防御;
            if ("物理防御".equals(jTextField18.getText()))
            {
                物理防御 = 0;
            }
            else
            {
                物理防御 = Integer.parseInt(jTextField18.getText());
            }

            int 魔法防御;
            if ("魔法防御".equals(jTextField19.getText()))
            {
                魔法防御 = 0;
            }
            else
            {
                魔法防御 = Integer.parseInt(jTextField19.getText());
            }
            MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
            MapleInventoryType type = GameConstants.getInventoryType(物品ID);
            System.out.print("要发送的物品type:" + type);
            String outputA = "";
            String output =
                    "玩家名字：" + 名字 + " 物品ID：" + 物品ID + " 数量：" + 数量 + " 力量:" + 力量 + " 敏捷:" + 敏捷 + " 智力:" + 智力 + " 运气:" + 运气 + " HP:" + HP + " MP:" + MP + " 可加卷次数:" + 可加卷次数 + " 制作人名字:" + 制作人名字 + " " +
                            "给予时间:" + 给予时间 + " 是否可以交易:" + 是否可以交易 + " " + "攻击力:" + 攻击力 + " 魔法力:" + 魔法力 + " 物理防御:" + 物理防御 + " 魔法防御:" + 魔法防御 + "\r\n";
            for (ChannelServer cserv1 : ChannelServer.getAllInstances())
            {
//                ChannelServer cserv1 = ChannelServer.getInstance(1);
                for (MapleCharacter mch : cserv1.getPlayerStorage().getAllCharacters())
                {
                    if (mch.getName().equals(名字))
                    {
                        System.out.println("获取到角色实例");
                        if (数量 >= 0)
                        {
                            //检查背包是否有空间
                            if (!MapleInventoryManipulator.checkSpace(mch.getClient(), 物品ID, 数量, ""))
                            {
                                System.out.println("背包空间不足");
                                return;
                            }
                            if (type.equals(MapleInventoryType.EQUIP) && !GameConstants.isThrowingStar(物品ID) && !GameConstants.isBullet(物品ID) || type.equals(MapleInventoryType.CASH) && 物品ID >= 5000000 && 物品ID <= 5000100)
                            {
                                System.out.println("可以进行武器的发送");
                                final Equip item = (Equip) (ii.getEquipById(物品ID));
                                if (ii.isCash(物品ID))
                                {
                                    item.setUniqueId(1);
                                }
                                if (力量 > 0 && 力量 <= 32767)
                                {
                                    item.setStr((short) (力量));
                                }
                                if (敏捷 > 0 && 敏捷 <= 32767)
                                {
                                    item.setDex((short) (敏捷));
                                }
                                if (智力 > 0 && 智力 <= 32767)
                                {
                                    item.setInt((short) (智力));
                                }
                                if (运气 > 0 && 运气 <= 32767)
                                {
                                    item.setLuk((short) (运气));
                                }
                                if (攻击力 > 0 && 攻击力 <= 32767)
                                {
                                    item.setWatk((short) (攻击力));
                                }
                                if (魔法力 > 0 && 魔法力 <= 32767)
                                {
                                    item.setMatk((short) (魔法力));
                                }
                                if (物理防御 > 0 && 物理防御 <= 32767)
                                {
                                    item.setWdef((short) (物理防御));
                                }
                                if (魔法防御 > 0 && 魔法防御 <= 32767)
                                {
                                    item.setMdef((short) (魔法防御));
                                }
                                if (HP > 0 && HP <= 30000)
                                {
                                    item.setHp((short) (HP));
                                }
                                if (MP > 0 && MP <= 30000)
                                {
                                    item.setMp((short) (MP));
                                }
                                if ("可以交易".equals(是否可以交易))
                                {
                                    byte flag = (byte) item.getFlag();
                                    if (item.getType() == MapleInventoryType.EQUIP.getType())
                                    {
                                        flag |= ItemFlag.KARMA_EQ.getValue();
                                    }
                                    else
                                    {
                                        flag |= ItemFlag.KARMA_USE.getValue();
                                    }
                                    item.setFlag(flag);
                                }
                                if (给予时间 > 0)
                                {
                                    item.setExpiration(System.currentTimeMillis() + (给予时间 * 24 * 60 * 60 * 1000));
                                }
                                if (可加卷次数 > 0)
                                {
                                    item.setUpgradeSlots((byte) (可加卷次数));
                                }
                                if (制作人名字 != null)
                                {
                                    item.setOwner(制作人名字);
                                }
                                final String name = ii.getName(物品ID);
                                if (物品ID / 10000 == 114 && name != null && name.length() > 0)
                                { //medal
                                    final String msg = "你已获得称号 <" + name + ">";
                                    mch.getClient().getPlayer().dropMessage(5, msg);
                                    mch.getClient().getPlayer().dropMessage(5, msg);
                                }

                                MapleInventoryManipulator.addbyItem(mch.getClient(), item.copy());
                            }
                            else
                            {
                                System.out.println("武器类型判定失败，通过ID发送");
                                MapleInventoryManipulator.addById(mch.getClient(), 物品ID, (short) 数量, "", null, 给予时间, "0");
                            }
                        }
                        else
                        {
                            MapleInventoryManipulator.removeById(mch.getClient(), GameConstants.getInventoryType(物品ID), 物品ID, -数量, true, false);
                        }
                        mch.getClient().getSession().write(MaplePacketCreator.getShowItemGain(物品ID, (short) 数量, true));
                        outputA = "[createGoods]:" + output;
                    }
                }
            }
//            jTextField3.setText("玩家名字");
//            jTextField4.setText("物品ID");
//            jTextField5.setText("1");
            jTextField6.setText("32767");
            jTextField7.setText("32767");
            jTextField8.setText("32767");
            jTextField9.setText("32767");
            jTextField10.setText("30000");
            jTextField11.setText("30000");
            jTextField12.setText("9");
            jTextField13.setText("GM");
//            jTextField14.setText(new Date(System.currentTimeMillis()).toString());
            jTextField15.setText("可以交易");
            jTextField16.setText("32767");
            jTextField17.setText("32767");
            jTextField18.setText("30000");
            jTextField19.setText("30000");
            KinMS.getInstance().printChatLog(outputA);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "错误!\r\n" + e);
        }
    }

    private void sendNoticeBroadcast()
    {
        try
        {
            String str = tfBroadcast.getText();
            String output = "";
            for (ChannelServer cserv1 : ChannelServer.getAllInstances())
            {
                for (MapleCharacter mch : cserv1.getPlayerStorage().getAllCharacters())
                {
                    mch.startMapEffect(str, 5121009);
                    output = "[公告]:" + str;
                }
            }
            tfBroadcast.setText("");
            KinMS.getInstance().printChatLog(output);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "错误!\r\n" + e);
        }
    }

    @Override
    public void init()
    {
        GroupLayout jPanel1Layout = new GroupLayout(this);
        this.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addComponent(tfBroadcast, GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE).addGap(18, 18, 18).addComponent(btnSendBroadcast)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(jTextField3, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jTextField4, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jTextField5, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)).addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(jPanel1Layout.createSequentialGroup().addComponent(jTextField9, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jTextField13)).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(jTextField8).addComponent(jTextField7)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(jTextField11, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE).addComponent(jTextField12, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))).addGroup(jPanel1Layout.createSequentialGroup().addComponent(jTextField6, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jTextField10, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(jTextField16).addComponent(jTextField15).addComponent(jTextField14).addComponent(jTextField17)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(jButton14, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(jTextField18).addComponent(jTextField19)))).addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(tfBroadcast, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(btnSendBroadcast)).addGap(18, 18, 18).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jTextField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jTextField6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jTextField7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jTextField8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jTextField9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jTextField17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jButton14)).addContainerGap(50, Short.MAX_VALUE)));

    }
}
