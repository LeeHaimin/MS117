package GUI.panel;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import GUI.KinMS;
import handling.world.WorldBroadcastService;
import server.ShutdownServer;
import server.Start;
import server.Timer;
import tools.MaplePacketCreator;

/**
 * 创建者 李海民
 * 文件说明：
 * 日期 2019/12/3 17:09
 **/
public class ServerPanel extends JPanel
{
    private JButton btnStop;
    private JButton btnStart;
    private JTextField etStopTime;

    public ServerPanel()
    {

    }

    public void init()
    {
        initBtn();
        GroupLayout jPanel5Layout = new GroupLayout(this);
        this.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel5Layout.createSequentialGroup().addContainerGap().addGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(btnStart).addGroup(jPanel5Layout.createSequentialGroup().addComponent(etStopTime, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18).addComponent(btnStop))).addContainerGap(343, Short.MAX_VALUE)));
        jPanel5Layout.setVerticalGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel5Layout.createSequentialGroup().addContainerGap().addComponent(btnStart).addGap(18, 18, 18).addGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(etStopTime, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(btnStop)).addContainerGap(162, Short.MAX_VALUE)));
    }

    private void initBtn()
    {
        btnStart = new JButton();
        btnStop = new JButton();
        etStopTime = new JTextField();

        btnStart.setText("启动服务端");
        btnStart.addActionListener(this::startServer);
        btnStop.setText("关闭服务器");
        btnStop.addActionListener(this::stopServer);
    }

    private void startServer(java.awt.event.ActionEvent evt)
    {
        try
        {
            Start.instance.run();
            KinMS.getInstance().printChatLog("[服务器] 服务器启动成功！");
        }
        catch (InterruptedException ex)
        {
            Logger.getLogger(KinMS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void stopServer(java.awt.event.ActionEvent evt)
    {
        try
        {
            String 输出 = "关闭服务器倒数时间";
            KinMS.getInstance().minutesLeft = Integer.parseInt(etStopTime.getText());
            if (KinMS.ts == null && (KinMS.t == null || !KinMS.t.isAlive()))
            {
                KinMS.t = new Thread(ShutdownServer.getInstance());
                KinMS.ts = Timer.EventTimer.getInstance().register(() -> {
                    if (KinMS.getInstance().minutesLeft == 0)
                    {
                        ShutdownServer.getInstance();
                        KinMS.t.start();
                        KinMS.ts.cancel(false);
                        return;
                    }
                    WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(0, "本私服器將在 " + KinMS.getInstance().minutesLeft + "分鐘後關閉. 請盡速關閉雇佣商人 並下線."));
                    System.out.println("本私服器將在 " + KinMS.getInstance().minutesLeft + "分鐘後關閉.");
                    KinMS.getInstance().minutesLeft--;
                }, 60000);
            }
            etStopTime.setText("关闭服务器倒数时间");
            KinMS.getInstance().printChatLog(输出);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "错误!\r\n" + e);
        }
    }
}
