package GUI.panel;

import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;

import GUI.IPanel;
import GUI.KinMS;
import handling.RecvPacketOpcode;
import handling.SendPacketOpcode;
import handling.channel.ChannelServer;
import scripting.portal.PortalScriptManager;
import scripting.reactor.ReactorScriptManager;
import server.life.MapleMonsterInformationProvider;
import server.shop.MapleShopFactory;

/**
 * 创建者 李海民
 * 文件说明：
 * 日期 2019/12/3 19:14
 **/
public class ReloadPanel extends JPanel implements IPanel
{
    private JButton btnReloadBurstRate;
    private JButton btnReloadDungeon;
    private JButton btnReloadPile;
    private JButton btnReloadShop;
    private JLabel jLabel1;
    private JButton btnReloadLink;
    private JButton btnReloadMarket;
    private JButton btnReloadMission;
    private JButton btnReloadPackage;

    @Override
    public void init()
    {

        btnReloadMission = new JButton();
        btnReloadDungeon = new JButton();
        btnReloadBurstRate = new JButton();
        btnReloadShop = new JButton();
        btnReloadLink = new JButton();
        btnReloadPile = new JButton();
        jLabel1 = new JLabel();
        btnReloadMarket = new JButton();
        btnReloadPackage = new JButton();


        btnReloadMission.setText("重载任务");
        btnReloadMission.addActionListener(this::jButton9ActionPerformed);

        btnReloadDungeon.setText("重载事件");
        btnReloadDungeon.addActionListener(this::onReloadEvent);

        btnReloadBurstRate.setText("重载爆率");
        btnReloadBurstRate.addActionListener(this::jButton5ActionPerformed);

        btnReloadShop.setText("重载商店");
        btnReloadShop.addActionListener(this::jButton4ActionPerformed);

        btnReloadLink.setText("重载传送门");
        btnReloadLink.addActionListener(this::jButton3ActionPerformed);

        btnReloadPile.setText("重载反应堆");
        btnReloadPile.addActionListener(this::jButton2ActionPerformed);

        jLabel1.setText("重载系列：");

        btnReloadMarket.setText("重载商城");
        btnReloadMarket.addActionListener(this::jButton12ActionPerformed);

        btnReloadPackage.setText("重载包头");
        btnReloadPackage.addActionListener(this::jButton6ActionPerformed);

        GroupLayout jPanel6Layout = new GroupLayout(this);
        this.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel6Layout.createSequentialGroup().addContainerGap().addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jLabel1).addGroup(jPanel6Layout.createSequentialGroup().addComponent(btnReloadDungeon).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(btnReloadBurstRate).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(btnReloadPile).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(btnReloadLink)).addGroup(jPanel6Layout.createSequentialGroup().addComponent(btnReloadMission).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(btnReloadShop)).addGroup(jPanel6Layout.createSequentialGroup().addComponent(btnReloadPackage).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(btnReloadMarket))).addContainerGap(202, Short.MAX_VALUE)));

        jPanel6Layout.setVerticalGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel6Layout.createSequentialGroup().addContainerGap().addComponent(jLabel1).addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnReloadLink).addComponent(btnReloadPile)).addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnReloadDungeon).addComponent(btnReloadBurstRate))).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnReloadMission).addComponent(btnReloadShop)).addGap(10, 10, 10).addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnReloadPackage).addComponent(btnReloadMarket)).addContainerGap(122, Short.MAX_VALUE)));
    }

    private void jButton9ActionPerformed(ActionEvent evt)
    {
//        MapleQuest.clearQuests();
        String output = "[重载系统] 任务重载成功。";
        JOptionPane.showMessageDialog(null, "任务重载成功。");
        KinMS.getInstance().printChatLog(output);
    }

    private void onReloadEvent(ActionEvent evt)
    {
        for (ChannelServer instance1 : ChannelServer.getAllInstances())
        {
            if (instance1 != null)
            {
                instance1.reloadEvents();
            }
        }
        String output = "[重载系统] 副本重载成功。";
        JOptionPane.showMessageDialog(null, "副本重载成功。");
        KinMS.getInstance().printChatLog(output);
    }

    private void jButton5ActionPerformed(ActionEvent evt)
    {
        MapleMonsterInformationProvider.getInstance().clearDrops();
        String output = "[重载系统] 爆率重载成功。";
        JOptionPane.showMessageDialog(null, "爆率重载成功。");
        KinMS.getInstance().printChatLog(output);
    }

    private void jButton4ActionPerformed(ActionEvent evt)
    {
        MapleShopFactory.getInstance().clear();
        String output = "[重载系统] 商店重载成功。";
        JOptionPane.showMessageDialog(null, "商店重载成功。");
        KinMS.getInstance().printChatLog(output);
    }

    private void jButton3ActionPerformed(ActionEvent evt)
    {
        PortalScriptManager.getInstance().clearScripts();
        String output = "[重载系统] 传送门重载成功。";
        JOptionPane.showMessageDialog(null, "传送门重载成功。");
        KinMS.getInstance().printChatLog(output);
    }

    private void jButton2ActionPerformed(ActionEvent evt)
    {
        ReactorScriptManager.getInstance().clearDrops();
        String output = "[重载系统] 反应堆重载成功。";
        JOptionPane.showMessageDialog(null, "反应堆重载成功。");
        KinMS.getInstance().printChatLog(output);
    }

    private void jButton12ActionPerformed(ActionEvent evt)
    {
//        CashItemFactory.getInstance().clearCashShop();
        String output = "[重载系统] 商城重载成功。";
        JOptionPane.showMessageDialog(null, "商城重载成功。");
        KinMS.getInstance().printChatLog(output);
    }

    private void jButton6ActionPerformed(ActionEvent evt)
    {
        SendPacketOpcode.reloadValues();
        RecvPacketOpcode.reloadValues();
        String output = "[重载系统] 包头重载成功。";
        JOptionPane.showMessageDialog(null, "包头重载成功。");
        KinMS.getInstance().printChatLog(output);


//        ChannelServer.getInstance(1).createBoss(910000001, 8130100, 0, 0, 10);
    }

}
