package GUI;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import provider.MapleData;
import provider.MapleDataDirectoryEntry;
import provider.MapleDataFileEntry;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;

/**
 * 创建者 李海民
 * 文件说明：
 * 日期 2019/12/5 19:19
 **/
class WZTest
{
    public static void main(String[] args)
    {
        File stringFile = MapleDataProviderFactory.fileInWZPath("Character.wz");
        MapleDataProvider root = MapleDataProviderFactory.getDataProvider(stringFile);
        List<String> string = new ArrayList<String>();
        for (MapleDataDirectoryEntry topDir : root.getRoot().getSubdirectories())
        {
            for (MapleDataFileEntry ifile : topDir.getFiles())
            {
                MapleData iz = root.getData(topDir.getName() + "/" + ifile.getName());
                MapleData dat = iz.getChildByPath("info");
                if (dat != null)
                {
                    for (MapleData data : dat.getChildren())
                    {
                        if (!string.contains(data.getName()))
                        {
                            string.add(data.getName());
                        }
                    }
                }
            }
        }
        Collections.sort(string);
        for (String i : string)
        {
//            FileoutputUtil.log("装备属性.txt", i, true);
        }
    }
}
