package GUI;

import server.Start;

/**
 * 创建者 李海民
 * 文件说明：
 * 日期 2019/12/7 23:41
 **/
class Test
{
    public static void main(String[] args)
    {
        System.setProperty("wzpath", "D:\\MapleStory\\MS117\\WZ");
        try
        {
            Start.instance.run();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
