/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Canvas;
import java.net.URL;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UnsupportedLookAndFeelException;

import GUI.panel.ReloadPanel;
import GUI.panel.RewardPanel;
import GUI.panel.SaveDataPanel;
import GUI.panel.SendGoodsPanel;
import GUI.panel.ServerPanel;
import client.MapleCharacter;
import handling.channel.ChannelServer;

/**
 * @author Administrator
 */
public class KinMS extends FrameModel
{
    private static final KinMS instance = new KinMS();
    public static ScheduledFuture<?> ts = null;
    public static Thread t = null;
    public int minutesLeft = 0;

    public KinMS()
    {
        initWZPath();
        FontClass.setUIFont();
        setSize(920, 480);
        setLocationRelativeTo(null);
        URL imgUrl = getClass().getClassLoader().getResource("Icon.png");
//        URL imgUrl = getClass().getClassLoader().getResource("D:\\MapleStory\\Project\\lee\\src\\main\\java\\gui\\Icon.png");
        if (null != imgUrl)
        {
            System.out.println(imgUrl.getPath());
            ImageIcon icon = new ImageIcon(imgUrl);
            setIconImage(icon.getImage());
        }
        setTitle("KinMS服务端后台管理工具    服务端版本为：Ver.0.79");
        initComponents();
    }

    public static void initWZPath()
    {
        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");//办公室
//        System.setProperty("wzpath","D:\\MapleStory\\MS117\\WZ");//家里
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        canvas1 = new Canvas();
        jScrollPane1 = new JScrollPane();
        chatLog = new JTextPane();
        TabBar = new JTabbedPane();
        serverPanel = new JPanel();
//        jTextField22 = new JTextField();
        jPanel7 = new JPanel();
        jButton7 = new JButton();
        jButton8 = new JButton();
        jLabel2 = new JLabel();
        jPanel6 = new JPanel();
        jPanel8 = new JPanel();
        jButton11 = new JButton();
        jTextField1 = new JTextField();
        jPanel1 = new JPanel();
        jButton13 = new JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane1.setViewportView(chatLog);


        jTextField22 = new JTextField();
        jTextField22.setText("关闭服务器倒数时间");


        ServerPanel serPanel = new ServerPanel();
        serPanel.init();
        TabBar.addTab("服务器配置", serPanel);


        SaveDataPanel dataPanel = new SaveDataPanel();
        dataPanel.init();
        TabBar.addTab("保存数据", dataPanel);


        ReloadPanel reloadPanel = new ReloadPanel();
        reloadPanel.init();
        TabBar.addTab("重载系列", reloadPanel);


        SendGoodsPanel sendGoodsPanel = new SendGoodsPanel();
        sendGoodsPanel.init();
        TabBar.addTab("指令/公告", sendGoodsPanel);


        RewardPanel rewardPanel = new RewardPanel();
        rewardPanel.init();
        TabBar.addTab("奖励系列", rewardPanel);

        jLabel2.setText("保存系列：");

        jButton11.setText("解卡玩家");
        jButton11.addActionListener(this::jButton11ActionPerformed);

        jTextField1.setText("输入玩家名字");

        GroupLayout jPanel8Layout = new GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(jPanel8Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel8Layout.createSequentialGroup().addContainerGap().addComponent(jTextField1,
                GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jButton11).addContainerGap(357,
                Short.MAX_VALUE)));
        jPanel8Layout.setVerticalGroup(jPanel8Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel8Layout.createSequentialGroup().addContainerGap().addGroup(jPanel8Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jTextField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(jButton11)).addContainerGap(203, Short.MAX_VALUE)));

        TabBar.addTab("卡号处理", jPanel8);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jScrollPane1).addGroup(layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(canvas1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)).addComponent(TabBar, GroupLayout.PREFERRED_SIZE, 583, GroupLayout.PREFERRED_SIZE));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(TabBar).addGap(5, 5, 5).addComponent(canvas1,
                GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20).addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 93,
                GroupLayout.PREFERRED_SIZE).addContainerGap()));

        pack();
    }

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt)
    {
        sendNotice(0);
    }

    private void sendNotice(int type)
    {
        try
        {
            String str = jTextField1.getText();
            byte[] p = null;
            String output = "";
            if (type == 0)
            {
                for (ChannelServer cserv : ChannelServer.getAllInstances())
                {
                    for (MapleCharacter chr : cserv.getPlayerStorage().getAllCharacters())
                    {
                        if (chr.getName().equals(str) && chr.getMapId() != 0)
                        {
                            chr.getClient().getSession().close();
                            chr.getClient().disconnect(true, false);
                            output = "[解卡系统] 成功断开" + str + "玩家！";
                        }
                        else
                        {
                            output = "[解卡系统] 玩家名字输入错误或者该玩家没有在线！";
                        }
                    }
                }
            }
            jTextField1.setText("");
            printChatLog(output);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "错误!\r\n" + e);
        }
    }

    public void printChatLog(String str)
    {
        chatLog.setText(chatLog.getText() + str + "\r\n");
    }

    /**
     * Creates new form KinMS
     */
    public static final KinMS getInstance()
    {
        return instance;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        initWZPath();
//        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");//办公室
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex)
        {
            Logger.getLogger(KinMS.class.getName()).log(Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(() -> new KinMS().setVisible(true));
    }

    private void initListeners()
    {

    }

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt)
    {
        int p = 0;
        for (ChannelServer cserv : ChannelServer.getAllInstances())
        {
            p++;
            cserv.closeAllMerchants();
        }
        String output = "[保存雇佣商人系统] 雇佣商人保存" + p + "个频道成功。";
        JOptionPane.showMessageDialog(null, "雇佣商人保存" + p + "个频道成功。");
        printChatLog(output);
    }

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt)
    {
        // TODO add your handling code here:
        int p = 0;
        for (ChannelServer cserv : ChannelServer.getAllInstances())
        {
            for (MapleCharacter chr : cserv.getPlayerStorage().getAllCharacters())
            {
                p++;
                chr.saveToDB(true, true);
            }
        }
        String output = "[保存数据系统] 保存" + p + "个成功。";
        JOptionPane.showMessageDialog(null, output);
        printChatLog(output);
    }
}
