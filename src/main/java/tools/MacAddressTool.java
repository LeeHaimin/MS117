package tools;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class MacAddressTool
{
    public static String getMacAddress(boolean ipAddress)
    {
        String macs = null;
        String localip = null;
        try
        {
            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();

            boolean finded = false;
            while ((netInterfaces.hasMoreElements()) && (!finded))
            {
                NetworkInterface ni = netInterfaces.nextElement();
                Enumeration<InetAddress> address = ni.getInetAddresses();
                while (address.hasMoreElements())
                {
                    InetAddress inetAddress = address.nextElement();
                    String ip = inetAddress.getHostAddress();
                    if ((!ip.contains(":")) && (!ip.startsWith("221.231.")) && (!ip.equalsIgnoreCase("127.0.0.1")))
                    {


                        if ((!inetAddress.isSiteLocalAddress()) && (!inetAddress.isLoopbackAddress()))
                        {
                            localip = inetAddress.getHostAddress();
                            byte[] mac = ni.getHardwareAddress();
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < mac.length; i++)
                            {
                                if (i != 0)
                                {
                                    sb.append("-");
                                }
                                String str = Integer.toHexString(mac[i] & 0xFF);
                                sb.append(str.length() == 1 ? 0 + str : str);
                            }
                            macs = sb.toString().toUpperCase();
                            System.out.println("外网 - localip: " + localip);
                            System.out.println("外网 - macs: " + macs);
                            finded = true;
                            break;
                        }
                        if ((inetAddress.isSiteLocalAddress()) && (!inetAddress.isLoopbackAddress()))
                        {
                            localip = inetAddress.getHostAddress();
                            byte[] mac = ni.getHardwareAddress();
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < mac.length; i++)
                            {
                                if (i != 0)
                                {
                                    sb.append("-");
                                }
                                String str = Integer.toHexString(mac[i] & 0xFF);
                                sb.append(str.length() == 1 ? 0 + str : str);
                            }
                            macs = sb.toString().toUpperCase();
                            System.out.println("内网 - localip: " + localip);
                            System.out.println("内网 - macs: " + macs);
                            finded = true;
                            break;
                        }
                    }
                }
            }
        }
        catch (SocketException e)
        {
            e.printStackTrace();
        }
        return ipAddress ? localip : macs;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\MacAddressTool.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */