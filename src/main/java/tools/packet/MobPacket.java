package tools.packet;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import client.status.MonsterStatus;
import client.status.MonsterStatusEffect;
import handling.SendPacketOpcode;
import server.ServerProperties;
import server.life.MapleMonster;
import server.life.MobSkill;
import server.maps.MapleMap;
import server.maps.MapleNodes;
import server.movement.LifeMovementFragment;
import tools.HexTool;
import tools.MaplePacketCreator;
import tools.data.output.MaplePacketLittleEndianWriter;


public class MobPacket
{
    private static final Logger log = Logger.getLogger(MobPacket.class);


    public static byte[] damageMonster(int oid, long damage)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(0);
        if (damage > 2147483647L)
        {
            mplew.writeInt(Integer.MAX_VALUE);
        }
        else
        {
            mplew.writeInt((int) damage);
        }

        return mplew.getPacket();
    }


    public static byte[] damageFriendlyMob(MapleMonster mob, long damage, boolean display)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_MONSTER.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.write(display ? 1 : 2);
        if (damage > 2147483647L)
        {
            mplew.writeInt(Integer.MAX_VALUE);
        }
        else
        {
            mplew.writeInt((int) damage);
        }
        if (mob.getHp() > 2147483647L)
        {
            mplew.writeInt((int) (mob.getHp() / mob.getMobMaxHp() * 2.147483647E9D));
        }
        else
        {
            mplew.writeInt((int) mob.getHp());
        }
        if (mob.getMobMaxHp() > 2147483647L)
        {
            mplew.writeInt(Integer.MAX_VALUE);
        }
        else
        {
            mplew.writeInt((int) mob.getMobMaxHp());
        }

        return mplew.getPacket();
    }


    public static byte[] killMonster(int oid, int animation)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.KILL_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(animation);
        if (animation == 4)
        {
            mplew.writeInt(-1);
        }

        return mplew.getPacket();
    }


    public static byte[] suckMonster(int oid, int chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.KILL_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(4);
        mplew.writeInt(chr);

        return mplew.getPacket();
    }


    public static byte[] healMonster(int oid, int heal)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(0);
        mplew.writeInt(-heal);

        return mplew.getPacket();
    }


    public static byte[] MobToMobDamage(int oid, int dmg, int mobid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOB_TO_MOB_DAMAGE.getValue());
        mplew.writeInt(oid);
        mplew.write(0);
        mplew.writeInt(dmg);
        mplew.writeInt(mobid);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] getMobSkillEffect(int oid, int skillid, int cid, int skilllevel)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SKILL_EFFECT_MOB.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(skillid);
        mplew.writeInt(cid);
        mplew.writeShort(skilllevel);

        return mplew.getPacket();
    }


    public static byte[] getMobCoolEffect(int oid, int itemid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ITEM_EFFECT_MOB.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }


    public static byte[] showMonsterHP(int oid, int remhppercentage)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_MONSTER_HP.getValue());
        mplew.writeInt(oid);
        mplew.write(remhppercentage);

        return mplew.getPacket();
    }


    public static byte[] showCygnusAttack(int oid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CYGNUS_ATTACK.getValue());
        mplew.writeInt(oid);

        return mplew.getPacket();
    }


    public static byte[] showMonsterResist(int oid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MONSTER_RESIST.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(0);
        mplew.writeShort(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] showBossHP(MapleMonster mob)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOSS_ENV.getValue());
        mplew.write(6);
        mplew.writeInt(mob.getId() == 9400589 ? 9300184 : mob.getId());
        if (mob.getHp() > 2147483647L)
        {
            mplew.writeInt((int) (mob.getHp() / mob.getMobMaxHp() * 2.147483647E9D));
        }
        else
        {
            mplew.writeInt((int) mob.getHp());
        }
        if (mob.getMobMaxHp() > 2147483647L)
        {
            mplew.writeInt(Integer.MAX_VALUE);
        }
        else
        {
            mplew.writeInt((int) mob.getMobMaxHp());
        }
        mplew.write(mob.getStats().getTagColor());
        mplew.write(mob.getStats().getTagBgColor());

        return mplew.getPacket();
    }


    public static byte[] showBossHP(int monsterId, long currentHp, long maxHp)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOSS_ENV.getValue());
        mplew.write(6);
        mplew.writeInt(monsterId);
        if (currentHp > 2147483647L)
        {
            mplew.writeInt((int) (currentHp / maxHp * 2.147483647E9D));
        }
        else
        {
            mplew.writeInt((int) (currentHp <= 0L ? -1L : currentHp));
        }
        if (maxHp > 2147483647L)
        {
            mplew.writeInt(Integer.MAX_VALUE);
        }
        else
        {
            mplew.writeInt((int) maxHp);
        }
        mplew.write(6);
        mplew.write(5);


        return mplew.getPacket();
    }


    public static byte[] moveMonster(boolean useskill, int action, int skillId, int skillLevel, int delay, int oid, Point startPos, List<LifeMovementFragment> moves)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(useskill ? 1 : 0);
        mplew.write(action);
        mplew.write(skillId);
        mplew.write(skillLevel);
        mplew.writeShort(delay);
        mplew.writeZeroBytes(6);
        mplew.writePos(startPos);
        mplew.writeInt(0);
        PacketHelper.serializeMovementList(mplew, moves);

        return mplew.getPacket();
    }

    public static byte[] controlMonster(MapleMonster life, boolean newSpawn, boolean aggro)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_MONSTER_CONTROL.getValue());
        mplew.write(aggro ? 2 : 1);
        mplew.writeInt(life.getObjectId());
        mplew.write(1);
        mplew.writeInt(life.getId());
        addMonsterStatus(mplew, life);
        mplew.writePos(life.getTruePosition());
        mplew.write(life.getStance());
        mplew.writeShort(0);
        mplew.writeShort(life.getFh());
        mplew.write(newSpawn ? -2 : life.isFake() ? -4 : -1);
        mplew.write(life.getCarnivalTeam());
        mplew.writeLong(life.getMobMaxHp() > 2147483647L ? 2147483647L : life.getMobMaxHp());
        mplew.writeZeroBytes(16);
        for (int i = 0; i < 5; i++)
        {
            mplew.write(-1);
        }
        mplew.writeZeroBytes(9);

        return mplew.getPacket();
    }

    public static void addMonsterStatus(MaplePacketLittleEndianWriter mplew, MapleMonster life)
    {
        if (life.getStati().size() <= 1)
        {
            life.addEmpty();
        }
        boolean writeChangedStats = false;
        mplew.write((writeChangedStats) && (life.getChangedStats() != null) ? 1 : 0);
        if ((writeChangedStats) && (life.getChangedStats() != null))
        {
            mplew.writeInt(life.getChangedStats().hp > 2147483647L ? Integer.MAX_VALUE : (int) life.getChangedStats().hp);
            mplew.writeInt(life.getChangedStats().mp);
            mplew.writeInt(life.getChangedStats().exp);
            mplew.writeInt(life.getChangedStats().watk);
            mplew.writeInt(life.getChangedStats().matk);
            mplew.writeInt(life.getChangedStats().PDRate);
            mplew.writeInt(life.getChangedStats().MDRate);
            mplew.writeInt(life.getChangedStats().acc);
            mplew.writeInt(life.getChangedStats().eva);
            mplew.writeInt(life.getChangedStats().pushed);
            mplew.writeInt(0);
            mplew.writeInt(life.getChangedStats().level);
        }
        mplew.writeZeroBytes(40);
        mplew.write(HexTool.getByteArrayFromHexString("E0 13 48 00 00 00 00 88"));
        for (int i = 1; i <= 4; i++)
        {
            mplew.writeLong(0L);
            mplew.writeShort(Integer.valueOf((int) System.currentTimeMillis()).shortValue());
        }
        mplew.writeZeroBytes(19);
    }

    public static byte[] stopControllingMonster(int oid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_MONSTER_CONTROL.getValue());
        mplew.write(0);
        mplew.writeInt(oid);

        return mplew.getPacket();
    }

    public static byte[] makeMonsterReal(MapleMonster life)
    {
        return spawnMonster(life, -1, 0);
    }

    public static byte[] spawnMonster(MapleMonster life, int spawnType, int link)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_MONSTER.getValue());
        mplew.write(0);
        mplew.writeInt(life.getObjectId());
        mplew.write(1);
        mplew.writeInt(life.getId());
        addMonsterStatus(mplew, life);
        mplew.writePos(life.getTruePosition());
        mplew.write(life.getStance());
        mplew.writeShort(0);
        mplew.writeShort(life.getFh());
        mplew.write(spawnType);
        if ((spawnType == -3) || (spawnType >= 0))
        {
            mplew.writeInt(link);
        }
        mplew.write(life.getCarnivalTeam());
        mplew.writeLong(life.getMobMaxHp() > 2147483647L ? 2147483647L : life.getMobMaxHp());
        mplew.writeZeroBytes(16);
        for (int i = 0; i < 5; i++)
        {
            mplew.write(-1);
        }
        mplew.writeZeroBytes(9);

        return mplew.getPacket();
    }

    public static byte[] makeMonsterFake(MapleMonster life)
    {
        return spawnMonster(life, -4, 0);
    }

    public static byte[] makeMonsterEffect(MapleMonster life, int effect)
    {
        return spawnMonster(life, effect, 0);
    }

    public static byte[] moveMonsterResponse(int objectid, short moveid, int currentMp, boolean useSkills, int skillId, int skillLevel)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_MONSTER_RESPONSE.getValue());
        mplew.writeInt(objectid);
        mplew.writeShort(moveid);
        mplew.write(useSkills ? 1 : 0);
        mplew.writeShort(currentMp);
        mplew.write(skillId);
        mplew.write(skillLevel);
        mplew.writeInt(0);
        mplew.writeShort(0);

        return mplew.getPacket();
    }

    private static void getLongMask_NoRef(MaplePacketLittleEndianWriter mplew, Collection<MonsterStatusEffect> ss, boolean ignore_imm)
    {
        int[] mask = new int[12];
        for (MonsterStatusEffect statup : ss)
        {
            if ((statup != null) && (statup.getStati() != MonsterStatus.反射物攻) && (statup.getStati() != MonsterStatus.反射魔攻) && ((!ignore_imm) || ((statup.getStati() != MonsterStatus.免疫物攻) && (statup.getStati() != MonsterStatus.免疫魔攻) && (statup.getStati() != MonsterStatus.免疫伤害))))
            {
                mask[(statup.getStati().getPosition() - 1)] |= statup.getStati().getValue();
            }
        }
        for (int i = mask.length; i >= 1; i--)
        {
            mplew.writeInt(mask[(i - 1)]);
        }
    }


    public static byte[] applyMonsterStatus(int oid, MonsterStatus mse, int x, MobSkill skil)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.APPLY_MONSTER_STATUS.getValue());
        mplew.writeInt(oid);
        PacketHelper.writeSingleMask(mplew, mse);
        mplew.writeInt(x);
        mplew.writeShort(skil.getSkillId());
        mplew.writeShort(skil.getSkillLevel());
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.write(1);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] applyMonsterStatus(MapleMonster mons, MonsterStatusEffect ms)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.APPLY_MONSTER_STATUS.getValue());
        mplew.writeInt(mons.getObjectId());
        PacketHelper.writeSingleMask(mplew, ms.getStati());
        mplew.writeInt(ms.getX());
        if (ms.isMonsterSkill())
        {
            mplew.writeShort(ms.getMobSkill().getSkillId());
            mplew.writeShort(ms.getMobSkill().getSkillLevel());
        }
        else if (ms.getSkill() > 0)
        {
            mplew.writeInt(ms.getSkill());
        }
        mplew.writeShort(0);
        if (ms.getSkill() == 31121003)
        {
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.write(1);
        }
        else if ((ms.getSkill() == 35111005) || (ms.getSkill() == 1211013) || (ms.getSkill() == 23121002))
        {
            mplew.writeInt(0);
            mplew.writeShort(0);
        }
        else
        {
            mplew.writeShort(0);
        }
        if (ms.getStati() == MonsterStatus.速度)
        {
            mplew.write(0);
        }
        mplew.write(1);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] applyMonsterPoisonStatus(MapleMonster mons, List<MonsterStatusEffect> mse)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        if ((mse.size() <= 0) || (mse.get(0) == null))
        {
            return MaplePacketCreator.enableActions();
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.APPLY_MONSTER_STATUS.getValue());
        mplew.writeInt(mons.getObjectId());
        MonsterStatusEffect ms = mse.get(0);
        if (ms.getStati() == MonsterStatus.中毒)
        {
            PacketHelper.writeSingleMask(mplew, MonsterStatus.空白BUFF);
            mplew.write(mse.size());
            for (MonsterStatusEffect m : mse)
            {
                mplew.writeInt(m.getFromID());
                if (m.isMonsterSkill())
                {
                    mplew.writeShort(m.getMobSkill().getSkillId());
                    mplew.writeShort(m.getMobSkill().getSkillLevel());
                }
                else if (m.getSkill() > 0)
                {
                    mplew.writeInt(m.getSkill());
                }

                mplew.writeInt(m.getX());
                mplew.writeInt(1000);
                mplew.writeInt(0);
                mplew.writeInt(10000);
                mplew.writeInt((int) (m.getDotTime() / 1000L));
                mplew.writeInt(0);
            }
            mplew.writeShort(1000);
            mplew.write(1);
        }
        else
        {
            PacketHelper.writeSingleMask(mplew, ms.getStati());
            mplew.writeInt(ms.getX());
            if (ms.isMonsterSkill())
            {
                mplew.writeShort(ms.getMobSkill().getSkillId());
                mplew.writeShort(ms.getMobSkill().getSkillLevel());
            }
            else if (ms.getSkill() > 0)
            {
                mplew.writeInt(ms.getSkill());
            }
            mplew.writeShort(0);
            mplew.writeShort(0);
            mplew.write(1);
            mplew.write(1);
        }

        return mplew.getPacket();
    }


    public static byte[] applyMonsterStatus(int oid, Map<MonsterStatus, Integer> stati, List<Integer> reflection, MobSkill skil)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.APPLY_MONSTER_STATUS.getValue());
        mplew.writeInt(oid);
        PacketHelper.writeMask(mplew, stati.keySet());

        for (Map.Entry<MonsterStatus, Integer> mse : stati.entrySet())
        {
            mplew.writeInt(mse.getValue());
            mplew.writeShort(skil.getSkillId());
            mplew.writeShort(skil.getSkillLevel());
            mplew.writeShort(0);
        }
        for (Integer ref : reflection)
        {
            mplew.writeInt(ref);
        }
        mplew.writeLong(0L);
        mplew.writeShort(0);

        int size = stati.size();
        if (reflection.size() > 0)
        {
            size /= 2;
        }
        mplew.write(size);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] cancelMonsterStatus(int oid, MonsterStatus stat)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_MONSTER_STATUS.getValue());
        mplew.writeInt(oid);
        PacketHelper.writeSingleMask(mplew, stat);
        mplew.write(1);
        mplew.write(2);

        return mplew.getPacket();
    }


    public static byte[] cancelMonsterPoisonStatus(int oid, MonsterStatusEffect m)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_MONSTER_STATUS.getValue());
        mplew.writeInt(oid);
        PacketHelper.writeSingleMask(mplew, MonsterStatus.空白BUFF);
        mplew.writeInt(0);
        mplew.writeInt(1);
        mplew.writeInt(m.getFromID());
        if (m.isMonsterSkill())
        {
            mplew.writeShort(m.getMobSkill().getSkillId());
            mplew.writeShort(m.getMobSkill().getSkillLevel());
        }
        else if (m.getSkill() > 0)
        {
            mplew.writeInt(m.getSkill());
        }
        mplew.write(3);

        return mplew.getPacket();
    }


    public static byte[] talkMonster(int oid, int itemId, String msg)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.TALK_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(500);
        mplew.writeInt(itemId);
        mplew.write(itemId <= 0 ? 0 : 1);
        mplew.write((msg == null) || (msg.length() <= 0) ? 0 : 1);
        if ((msg != null) && (msg.length() > 0))
        {
            mplew.writeMapleAsciiString(msg);
        }
        mplew.writeInt(1);

        return mplew.getPacket();
    }


    public static byte[] removeTalkMonster(int oid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_TALK_MONSTER.getValue());
        mplew.writeInt(oid);

        return mplew.getPacket();
    }

    public static byte[] getNodeProperties(MapleMonster objectid, MapleMap map)
    {
        if (objectid.getNodePacket() != null)
        {
            return objectid.getNodePacket();
        }
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MONSTER_PROPERTIES.getValue());
        mplew.writeInt(objectid.getObjectId());
        mplew.writeInt(map.getNodes().size());
        mplew.writeInt(objectid.getPosition().x);
        mplew.writeInt(objectid.getPosition().y);
        for (MapleNodes.MapleNodeInfo mni : map.getNodes())
        {
            mplew.writeInt(mni.x);
            mplew.writeInt(mni.y);
            mplew.writeInt(mni.attr);
            if (mni.attr == 2)
            {
                mplew.writeInt(500);
            }
        }
        mplew.writeZeroBytes(6);
        objectid.setNodePacket(mplew.getPacket());

        return objectid.getNodePacket();
    }


    public static byte[] showMagnet(int mobid, byte success, int skillId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_MAGNET.getValue());
        mplew.writeInt(mobid);
        mplew.write(success);
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] catchMonster(int mobid, int itemid, byte success)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CATCH_MONSTER.getValue());
        mplew.writeInt(mobid);
        mplew.writeInt(itemid);
        mplew.write(success);

        return mplew.getPacket();
    }

    public static byte[] catchMob(int mobid, int itemid, byte success)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CATCH_MOB.getValue());
        mplew.write(success);
        mplew.writeInt(itemid);
        mplew.writeInt(mobid);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\MobPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */