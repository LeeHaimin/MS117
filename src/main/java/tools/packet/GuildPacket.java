package tools.packet;

import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.List;

import client.MapleCharacter;
import handling.SendPacketOpcode;
import handling.channel.MapleGuildRanking;
import handling.world.WorldGuildService;
import handling.world.guild.MapleBBSReply;
import handling.world.guild.MapleBBSThread;
import handling.world.guild.MapleGuild;
import handling.world.guild.MapleGuildAlliance;
import handling.world.guild.MapleGuildCharacter;
import handling.world.guild.MapleGuildSkill;
import server.ServerProperties;
import tools.MaplePacketCreator;
import tools.data.output.MaplePacketLittleEndianWriter;


public class GuildPacket
{
    private static final Logger log = Logger.getLogger(GuildPacket.class);


    public static byte[] guildInvite(int guildId, String charName, int levelFrom, int jobFrom)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(7);
        mplew.writeInt(guildId);
        mplew.writeMapleAsciiString(charName);
        mplew.writeInt(levelFrom);
        mplew.writeInt(jobFrom);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] showGuildInfo(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(46);
        mplew.write(0);

        if ((chr == null) || (chr.getMGC() == null))
        {
            mplew.write(0);
            return mplew.getPacket();
        }
        MapleGuild guild = WorldGuildService.getInstance().getGuild(chr.getGuildId());
        if (guild == null)
        {
            mplew.write(0);
            return mplew.getPacket();
        }
        mplew.write(1);

        addGuildInfo(mplew, guild);

        addGuildExpInfo(mplew, guild);

        return mplew.getPacket();
    }

    private static void addGuildInfo(MaplePacketLittleEndianWriter mplew, MapleGuild guild)
    {
        mplew.writeInt(guild.getId());
        mplew.writeMapleAsciiString(guild.getName());

        for (int i = 1; i <= 5; i++)
        {
            mplew.writeMapleAsciiString(guild.getRankTitle(i));
        }

        guild.addMemberData(mplew);

        mplew.writeInt(guild.getCapacity());

        mplew.writeShort(guild.getLogoBG());
        mplew.write(guild.getLogoBGColor());
        mplew.writeShort(guild.getLogo());
        mplew.write(guild.getLogoColor());

        mplew.writeMapleAsciiString(guild.getNotice());

        mplew.writeInt(guild.getGP());
        mplew.writeInt(guild.getGP());

        mplew.writeInt(guild.getAllianceId() > 0 ? guild.getAllianceId() : 0);

        mplew.write(guild.getLevel());
        mplew.writeShort(0);
        mplew.writeInt(0);

        mplew.writeShort(guild.getSkills().size());
        for (MapleGuildSkill i : guild.getSkills())
        {
            mplew.writeInt(i.skillID);
            mplew.writeShort(i.level);
            mplew.writeLong(PacketHelper.getTime(i.timestamp));
            mplew.writeMapleAsciiString(i.purchaser);
            mplew.writeMapleAsciiString(i.activator);
        }
        mplew.write(0);
    }

    private static void addGuildExpInfo(MaplePacketLittleEndianWriter mplew, MapleGuild guild)
    {
        int[] guildExp = guild.getGuildExp();
        mplew.writeInt(guildExp.length);
        for (int value : guildExp)
        {
            mplew.writeInt(value);
        }
    }

    public static byte[] showPlayerGuildInfo(MapleGuild guild)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(47);
        mplew.writeInt(guild.getId());
        addGuildInfo(mplew, guild);

        return mplew.getPacket();
    }

    public static byte[] createGuild(MapleGuild guild)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(53);
        addGuildInfo(mplew, guild);

        return mplew.getPacket();
    }


    public static byte[] guildSkillPurchased(int guildId, int skillId, int level, long expiration, String purchase, String activate)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(85);
        mplew.writeInt(guildId);
        mplew.writeInt(skillId);
        mplew.writeShort(level);
        mplew.writeLong(PacketHelper.getTime(expiration));
        mplew.writeMapleAsciiString(purchase);
        mplew.writeMapleAsciiString(activate);

        return mplew.getPacket();
    }


    public static byte[] genericGuildMessage(byte code)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(code);

        return mplew.getPacket();
    }


    public static byte[] newGuildMember(MapleGuildCharacter mgc, boolean isApply)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(isApply ? 66 : 60);
        mplew.writeInt(mgc.getGuildId());
        mplew.writeInt(mgc.getId());
        mplew.writeAsciiString(mgc.getName(), 13);
        mplew.writeInt(mgc.getJobId());
        mplew.writeInt(mgc.getLevel());
        mplew.writeInt(mgc.getGuildRank());
        mplew.writeInt(mgc.isOnline() ? 1 : 0);
        mplew.writeInt(mgc.getAllianceRank());
        mplew.writeInt(mgc.getGuildContribution());
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeLong(PacketHelper.getTime(-2L));

        return mplew.getPacket();
    }


    public static byte[] DenyGuildApply(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(71);
        mplew.writeInt(chrId);

        return mplew.getPacket();
    }


    public static byte[] memberLeft(MapleGuildCharacter mgc, boolean isExpelled)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(isExpelled ? 75 : 72);
        mplew.writeInt(mgc.getGuildId());
        mplew.writeInt(mgc.getId());
        mplew.writeMapleAsciiString(mgc.getName());

        return mplew.getPacket();
    }


    public static byte[] guildNotice(int guildId, String notice)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(75);
        mplew.writeInt(guildId);
        mplew.writeMapleAsciiString(notice);

        return mplew.getPacket();
    }


    public static byte[] guildDisband(int guildId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(78);
        mplew.writeInt(guildId);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] denyGuildInvitation(String charname)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(84);
        mplew.writeMapleAsciiString(charname);

        return mplew.getPacket();
    }


    public static byte[] guildCapacityChange(int guildId, int capacity)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(87);
        mplew.writeInt(guildId);
        mplew.write(capacity);

        return mplew.getPacket();
    }


    public static byte[] guildMemberLevelJobUpdate(MapleGuildCharacter mgc)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(89);
        mplew.writeInt(mgc.getGuildId());
        mplew.writeInt(mgc.getId());
        mplew.writeInt(mgc.getLevel());
        mplew.writeInt(mgc.getJobId());

        return mplew.getPacket();
    }


    public static byte[] guildMemberOnline(int guildId, int chrId, boolean isOnline)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(90);
        mplew.writeInt(guildId);
        mplew.writeInt(chrId);
        mplew.write(isOnline ? 1 : 0);

        return mplew.getPacket();
    }


    public static byte[] rankTitleChange(int guildId, String[] ranks)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(91);
        mplew.writeInt(guildId);
        for (String r : ranks)
        {
            mplew.writeMapleAsciiString(r);
        }

        return mplew.getPacket();
    }


    public static byte[] changeRank(MapleGuildCharacter mgc)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(93);
        mplew.writeInt(mgc.getGuildId());
        mplew.writeInt(mgc.getId());
        mplew.write(mgc.getGuildRank());

        return mplew.getPacket();
    }


    public static byte[] updatePlayerContribution(int guildId, int chrId, int contribution)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(95);
        mplew.writeInt(guildId);
        mplew.writeInt(chrId);
        mplew.writeInt(contribution);
        mplew.writeInt(contribution);
        mplew.writeInt(0);
        mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));

        return mplew.getPacket();
    }


    public static byte[] guildEmblemChange(int guildId, short bg, byte bgcolor, short logo, byte logocolor)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(96);
        mplew.writeInt(guildId);
        mplew.writeShort(bg);
        mplew.write(bgcolor);
        mplew.writeShort(logo);
        mplew.write(logocolor);

        return mplew.getPacket();
    }


    public static byte[] updateGuildInfo(int guildId, int totalContribution, int guildlevel)
    {
        return updateGuildInfo(guildId, totalContribution, guildlevel, 0);
    }

    public static byte[] updateGuildInfo(int guildId, int totalContribution, int guildlevel, int totalGP)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(102);
        mplew.writeInt(guildId);
        mplew.writeInt(totalContribution);
        mplew.writeInt(guildlevel);
        mplew.writeInt(totalGP);

        return mplew.getPacket();
    }


    public static byte[] showGuildRanks(int npcid, List<MapleGuildRanking.GuildRankingInfo> all, boolean show)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(103);
        mplew.writeInt(npcid);
        mplew.writeInt(show ? all.size() : 0);
        if (show)
        {
            for (MapleGuildRanking.GuildRankingInfo info : all)
            {
                mplew.writeMapleAsciiString(info.getName());
                mplew.writeInt(info.getGP());
                mplew.writeInt(info.getLogo());
                mplew.writeInt(info.getLogoColor());
                mplew.writeInt(info.getLogoBg());
                mplew.writeInt(info.getLogoBgColor());
            }
        }

        return mplew.getPacket();
    }


    public static byte[] guildLeaderChanged(int guildId, int oldLeader, int newLeader, int allianceId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(118);
        mplew.writeInt(guildId);
        mplew.writeInt(oldLeader);
        mplew.writeInt(newLeader);
        mplew.write(allianceId > 0 ? 1 : 0);
        if (allianceId > 0)
        {
            mplew.writeInt(allianceId);
        }

        return mplew.getPacket();
    }


    public static byte[] showGuildBeginnerSkill()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GUILD_OPERATION.getValue());
        mplew.write(122);
        mplew.writeShort(0);
        mplew.writeShort(0);

        return mplew.getPacket();
    }


    public static byte[] removeGuildFromAlliance(MapleGuildAlliance alliance, MapleGuild expelledGuild, boolean expelled)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(16);
        addAllianceInfo(mplew, alliance);
        addGuildInfo(mplew, expelledGuild);
        mplew.write(expelled ? 1 : 0);

        return mplew.getPacket();
    }

    private static void addAllianceInfo(MaplePacketLittleEndianWriter mplew, MapleGuildAlliance alliance)
    {
        mplew.writeInt(alliance.getId());
        mplew.writeMapleAsciiString(alliance.getName());
        for (int i = 1; i <= 5; i++)
        {
            mplew.writeMapleAsciiString(alliance.getRank(i));
        }
        mplew.write(alliance.getNoGuilds());
        for (int i = 0; i < alliance.getNoGuilds(); i++)
        {
            mplew.writeInt(alliance.getGuildId(i));
        }
        mplew.writeInt(alliance.getCapacity());
        mplew.writeMapleAsciiString(alliance.getNotice());
    }

    public static byte[] changeAlliance(MapleGuildAlliance alliance, boolean in)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(1);
        mplew.write(in ? 1 : 0);
        mplew.writeInt(in ? alliance.getId() : 0);
        int noGuilds = alliance.getNoGuilds();
        MapleGuild[] g = new MapleGuild[noGuilds];
        for (int i = 0; i < noGuilds; i++)
        {
            g[i] = WorldGuildService.getInstance().getGuild(alliance.getGuildId(i));
            if (g[i] == null)
            {
                return MaplePacketCreator.enableActions();
            }
        }
        mplew.write(noGuilds);
        for (int i = 0; i < noGuilds; i++)
        {
            mplew.writeInt(g[i].getId());

            Collection<MapleGuildCharacter> members = g[i].getMembers();
            mplew.writeInt(members.size());
            for (MapleGuildCharacter mgc : members)
            {
                mplew.writeInt(mgc.getId());
                mplew.write(in ? mgc.getAllianceRank() : 0);
            }
        }

        return mplew.getPacket();
    }

    public static byte[] changeAllianceLeader(int allianceid, int newLeader, int oldLeader)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(2);
        mplew.writeInt(allianceid);
        mplew.writeInt(oldLeader);
        mplew.writeInt(newLeader);

        return mplew.getPacket();
    }

    public static byte[] updateAllianceLeader(int allianceid, int newLeader, int oldLeader)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(25);
        mplew.writeInt(allianceid);
        mplew.writeInt(oldLeader);
        mplew.writeInt(newLeader);

        return mplew.getPacket();
    }

    public static byte[] sendAllianceInvite(String allianceName, MapleCharacter inviter)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(3);
        mplew.writeInt(inviter.getGuildId());
        mplew.writeMapleAsciiString(inviter.getName());
        mplew.writeMapleAsciiString(allianceName);

        return mplew.getPacket();
    }

    public static byte[] changeGuildInAlliance(MapleGuildAlliance alliance, MapleGuild guild, boolean add)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(4);
        mplew.writeInt(add ? alliance.getId() : 0);
        mplew.writeInt(guild.getId());
        Collection<MapleGuildCharacter> members = guild.getMembers();
        mplew.writeInt(members.size());
        for (MapleGuildCharacter mgc : members)
        {
            mplew.writeInt(mgc.getId());
            mplew.write(add ? mgc.getAllianceRank() : 0);
        }

        return mplew.getPacket();
    }

    public static byte[] changeAllianceRank(int allianceid, MapleGuildCharacter player)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(5);
        mplew.writeInt(allianceid);
        mplew.writeInt(player.getId());
        mplew.writeInt(player.getAllianceRank());

        return mplew.getPacket();
    }

    public static byte[] createGuildAlliance(MapleGuildAlliance alliance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(15);
        addAllianceInfo(mplew, alliance);
        int noGuilds = alliance.getNoGuilds();
        MapleGuild[] g = new MapleGuild[noGuilds];
        for (int i = 0; i < alliance.getNoGuilds(); i++)
        {
            g[i] = WorldGuildService.getInstance().getGuild(alliance.getGuildId(i));
            if (g[i] == null)
            {
                return MaplePacketCreator.enableActions();
            }
        }
        for (MapleGuild gg : g)
        {
            addGuildInfo(mplew, gg);
        }

        return mplew.getPacket();
    }

    public static byte[] getAllianceInfo(MapleGuildAlliance alliance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(12);
        mplew.write(alliance == null ? 0 : 1);
        if (alliance != null)
        {
            addAllianceInfo(mplew, alliance);
        }

        return mplew.getPacket();
    }

    public static byte[] getAllianceUpdate(MapleGuildAlliance alliance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(23);
        addAllianceInfo(mplew, alliance);

        return mplew.getPacket();
    }

    public static byte[] getGuildAlliance(MapleGuildAlliance alliance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(13);
        if (alliance == null)
        {
            mplew.writeInt(0);
            return mplew.getPacket();
        }
        int noGuilds = alliance.getNoGuilds();
        MapleGuild[] guildlist = new MapleGuild[noGuilds];
        for (int i = 0; i < alliance.getNoGuilds(); i++)
        {
            guildlist[i] = WorldGuildService.getInstance().getGuild(alliance.getGuildId(i));
            if (guildlist[i] == null)
            {
                return MaplePacketCreator.enableActions();
            }
        }
        mplew.writeInt(noGuilds);
        for (MapleGuild guild : guildlist)
        {
            addGuildInfo(mplew, guild);
        }

        return mplew.getPacket();
    }

    public static byte[] addGuildToAlliance(MapleGuildAlliance alliance, MapleGuild newGuild)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(18);
        addAllianceInfo(mplew, alliance);
        mplew.writeInt(newGuild.getId());
        addGuildInfo(mplew, newGuild);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] allianceMemberOnline(int alliance, int gid, int id, boolean online)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(14);
        mplew.writeInt(alliance);
        mplew.writeInt(gid);
        mplew.writeInt(id);
        mplew.write(online ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] updateAlliance(MapleGuildCharacter mgc, int allianceid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(24);
        mplew.writeInt(allianceid);
        mplew.writeInt(mgc.getGuildId());
        mplew.writeInt(mgc.getId());
        mplew.writeInt(mgc.getLevel());
        mplew.writeInt(mgc.getJobId());

        return mplew.getPacket();
    }

    public static byte[] updateAllianceRank(int allianceid, MapleGuildCharacter mgc)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(27);
        mplew.writeInt(allianceid);
        mplew.writeInt(mgc.getId());
        mplew.writeInt(mgc.getAllianceRank());

        return mplew.getPacket();
    }


    public static byte[] disbandAlliance(int alliance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALLIANCE_OPERATION.getValue());
        mplew.write(29);
        mplew.writeInt(alliance);

        return mplew.getPacket();
    }


    public static byte[] BBSThreadList(List<MapleBBSThread> bbs, int start)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.BBS_OPERATION.getValue());
        mplew.write(6);

        if (bbs == null)
        {
            mplew.write(0);
            mplew.writeInt(0);
            return mplew.getPacket();
        }


        MapleBBSThread notice = null;
        for (MapleBBSThread b : bbs)
        {
            if (b.isNotice())
            {
                notice = b;
                bbs.remove(b);
                break;
            }
        }
        mplew.write(notice == null ? 0 : 1);
        if (notice != null)
        {
            addThread(mplew, notice);
        }
        int threadCount = bbs.size();
        if (threadCount < start)
        {
            start = 0;
        }

        mplew.writeInt(threadCount);
        int pages = Math.min(10, threadCount - start);
        mplew.writeInt(pages);

        for (int i = 0; i < pages; i++)
        {
            addThread(mplew, bbs.get(start + i));
        }

        return mplew.getPacket();
    }

    private static void addThread(MaplePacketLittleEndianWriter mplew, MapleBBSThread thread)
    {
        mplew.writeInt(thread.localthreadID);
        mplew.writeInt(thread.ownerID);
        mplew.writeMapleAsciiString(thread.name);
        mplew.writeLong(PacketHelper.getTime(thread.timestamp));
        mplew.writeInt(thread.icon);
        mplew.writeInt(thread.getReplyCount());
    }

    public static byte[] showThread(MapleBBSThread thread)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BBS_OPERATION.getValue());
        mplew.write(7);

        mplew.writeInt(thread.localthreadID);
        mplew.writeInt(thread.ownerID);
        mplew.writeLong(PacketHelper.getTime(thread.timestamp));
        mplew.writeMapleAsciiString(thread.name);
        mplew.writeMapleAsciiString(thread.text);
        mplew.writeInt(thread.icon);
        mplew.writeInt(thread.getReplyCount());
        for (MapleBBSReply reply : thread.replies.values())
        {
            mplew.writeInt(reply.replyid);
            mplew.writeInt(reply.ownerID);
            mplew.writeLong(PacketHelper.getTime(reply.timestamp));
            mplew.writeMapleAsciiString(reply.content);
        }

        return mplew.getPacket();
    }

    public static byte[] loadGuildName(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LOAD_GUILD_NAME.getValue());
        mplew.writeInt(chr.getId());
        if (chr.getGuildId() <= 0)
        {
            mplew.writeShort(0);
        }
        else
        {
            MapleGuild guild = WorldGuildService.getInstance().getGuild(chr.getGuildId());
            if (guild != null)
            {
                mplew.writeMapleAsciiString(guild.getName());
            }
            else
            {
                mplew.writeShort(0);
            }
        }
        return mplew.getPacket();
    }

    public static byte[] loadGuildIcon(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LOAD_GUILD_ICON.getValue());
        mplew.writeInt(chr.getId());
        if (chr.getGuildId() <= 0)
        {
            mplew.writeZeroBytes(6);
        }
        else
        {
            MapleGuild guild = WorldGuildService.getInstance().getGuild(chr.getGuildId());
            if (guild != null)
            {
                mplew.writeShort(guild.getLogoBG());
                mplew.write(guild.getLogoBGColor());
                mplew.writeShort(guild.getLogo());
                mplew.write(guild.getLogoColor());
            }
            else
            {
                mplew.writeZeroBytes(6);
            }
        }
        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\GuildPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */