package tools.packet;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.util.Collections;
import java.util.List;

import client.MapleCharacter;
import client.inventory.Equip;
import client.inventory.MaplePotionPot;
import client.inventory.ModifyInventory;
import handling.SendPacketOpcode;
import server.ServerProperties;
import server.maps.MapleMapItem;
import tools.data.output.MaplePacketLittleEndianWriter;


public class InventoryPacket
{
    private static final Logger log = Logger.getLogger(InventoryPacket.class);

    public static byte[] updateInventorySlotLimit(byte invType, byte newSlots)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_INVENTORY_SLOT.getValue());
        mplew.write(invType);
        mplew.write(newSlots);

        return mplew.getPacket();
    }

    public static byte[] getInventoryFull()
    {
        return modifyInventory(true, Collections.EMPTY_LIST);
    }

    public static byte[] modifyInventory(boolean updateTick, List<ModifyInventory> mods)
    {
        return modifyInventory(updateTick, mods, null);
    }

    public static byte[] modifyInventory(boolean updateTick, List<ModifyInventory> mods, MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MODIFY_INVENTORY_ITEM.getValue());
        mplew.writeBool(updateTick);
        mplew.writeShort(mods.size());
        int addMovement = -1;
        for (ModifyInventory mod : mods)
        {
            mplew.write(mod.getMode());
            mplew.write(mod.getInventoryType());
            boolean oldpos = (mod.getMode() == 2) || (mod.getMode() == 8) || ((mod.getMode() == 5) && (!mod.switchSrcDst()));
            mplew.writeShort(oldpos ? mod.getOldPosition() : mod.getPosition());
            switch (mod.getMode())
            {
                case 0:
                    PacketHelper.addItemInfo(mplew, mod.getItem(), chr);
                    break;
                case 1:
                case 6:
                    mplew.writeShort(mod.getQuantity());
                    break;
                case 2:
                    mplew.writeShort(mod.getPosition());
                    if ((mod.getPosition() < 0) || (mod.getOldPosition() < 0))
                    {
                        addMovement = mod.getOldPosition() < 0 ? 1 : 2;
                    }
                    break;
                case 3:
                    if (mod.getPosition() < 0)
                    {
                        addMovement = 2;
                    }
                    break;
                case 5:
                    mplew.writeShort(!mod.switchSrcDst() ? mod.getPosition() : mod.getOldPosition());
                    if (mod.getIndicator() != -1)
                    {
                        mplew.writeShort(mod.getIndicator());
                    }
                    break;
                case 7:
                    break;

                case 8:
                    mplew.writeShort(mod.getPosition());
                    break;
                case 9:
                    PacketHelper.addItemInfo(mplew, mod.getItem());
            }

            mod.clear();
        }
        if (addMovement > -1)
        {
            mplew.write(addMovement);
        }

        return mplew.getPacket();
    }

    public static byte[] getInventoryStatus()
    {
        return modifyInventory(false, Collections.EMPTY_LIST);
    }

    public static byte[] getShowInventoryFull()
    {
        return getShowInventoryStatus(255);
    }

    public static byte[] getShowInventoryStatus(int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(0);
        mplew.write(mode);

        return mplew.getPacket();
    }

    public static byte[] showItemUnavailable()
    {
        return getShowInventoryStatus(254);
    }

    public static byte[] showScrollTip(boolean success)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_SCROLL_TIP.getValue());
        mplew.writeInt(success ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] getScrollEffect(int chrId, int scroll, int toScroll)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_SCROLL_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.writeShort(1);
        mplew.writeInt(scroll);
        mplew.writeInt(toScroll);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] getScrollEffect(int chrId, Equip.ScrollResult scrollSuccess, boolean legendarySpirit, boolean whiteScroll, int scroll, int toScroll)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_SCROLL_EFFECT.getValue());
        mplew.writeInt(chrId);
        switch (scrollSuccess)
        {
            case 失败:
                mplew.write(0);
                break;
            case 成功:
                mplew.write(1);
                break;
            case 消失:
                mplew.write(2);
                break;
            default:
                throw new IllegalArgumentException("effect in illegal range");
        }
        mplew.write(legendarySpirit ? 0 : 0);
        mplew.writeInt(scroll);
        mplew.writeInt(toScroll);
        mplew.write(whiteScroll ? 1 : 0);

        return mplew.getPacket();
    }


    public static byte[] getPotentialEffect(int chrId, int itemid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_MAGNIFYING_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(1);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }


    public static byte[] showMagnifyingEffect(int chrId, short pos, boolean isPotAdd)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_MAGNIFYING_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.writeShort(pos);
        mplew.write(isPotAdd ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] showPotentialReset(boolean fireworks, int chrId, boolean success, int itemid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(fireworks ? SendPacketOpcode.SHOW_FIREWORKS_EFFECT.getValue() : SendPacketOpcode.SHOW_POTENTIAL_RESET.getValue());
        mplew.writeInt(chrId);
        mplew.write(success ? 1 : 0);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }


    public static byte[] 潜能变化效果(int chrId, boolean success, int itemid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ADDITIONAL_RESET.getValue());
        mplew.writeInt(chrId);
        mplew.write(success ? 1 : 0);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }


    public static byte[] 潜能扩展效果(int chrId, boolean success, int itemid, boolean 是否破坏)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ADDITIONAL_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(success ? 1 : 0);
        mplew.writeInt(itemid);
        mplew.write(是否破坏 ? 1 : 0);

        return mplew.getPacket();
    }


    public static byte[] showNebuliteEffect(int chrId, boolean success, String msg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_NEBULITE_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(success ? 1 : 0);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] useNebuliteFusion(int chrId, int itemId, boolean success)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FUSION_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(success ? 1 : 0);
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }


    public static byte[] showSynthesizingMsg(int itemId, int giveItemId, boolean success)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SYNTHESIZING_MSG.getValue());
        mplew.write(success ? 1 : 0);
        mplew.writeInt(itemId);
        mplew.writeInt(giveItemId);

        return mplew.getPacket();
    }

    public static byte[] ItemMaker_Success()
    {
        return ItemMaker_Success_3rdParty(-1);
    }

    public static byte[] ItemMaker_Success_3rdParty(int from_playerid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        if (from_playerid == -1)
        {
            mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        }
        else
        {
            mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
            mplew.writeInt(from_playerid);
        }
        mplew.write(20);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] dropItemFromMapObject(MapleMapItem drop, Point dropfrom, Point dropto, byte mod)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DROP_ITEM_FROM_MAPOBJECT.getValue());
        mplew.write(mod);
        mplew.writeInt(drop.getObjectId());
        mplew.write(drop.getMeso() > 0 ? 1 : 0);
        mplew.writeInt(drop.getItemId());
        mplew.writeInt(drop.getOwner());
        mplew.write(drop.getDropType());
        mplew.writePos(dropto);
        mplew.writeInt(0);
        if (mod != 2)
        {
            mplew.writePos(dropfrom);
            mplew.writeShort(0);
        }
        mplew.write(0);
        if (drop.getMeso() == 0)
        {
            PacketHelper.addExpirationTime(mplew, drop.getItem().getExpiration());
        }
        mplew.writeInt(drop.isPlayerDrop() ? 0 : 1);
        mplew.writeZeroBytes(6);

        return mplew.getPacket();
    }

    public static byte[] explodeDrop(int oid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_ITEM_FROM_MAP.getValue());
        mplew.write(4);
        mplew.writeInt(oid);
        mplew.writeShort(655);

        return mplew.getPacket();
    }

    public static byte[] removeItemFromMap(int oid, int animation, int chrId)
    {
        return removeItemFromMap(oid, animation, chrId, 0);
    }

    public static byte[] removeItemFromMap(int oid, int animation, int chrId, int slot)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_ITEM_FROM_MAP.getValue());
        mplew.write(animation);
        mplew.writeInt(oid);
        if (animation >= 2)
        {
            mplew.writeInt(chrId);
            if (animation == 5)
            {
                mplew.writeInt(slot);
            }
        }

        return mplew.getPacket();
    }


    public static byte[] showPotionPotMsg(int reason)
    {
        return showPotionPotMsg(reason, 0);
    }

    public static byte[] showPotionPotMsg(int reason, int msg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.POTION_POT_MSG.getValue());
        mplew.write(reason);
        if (reason == 0)
        {


            mplew.write(msg);
        }

        return mplew.getPacket();
    }


    public static byte[] updataPotionPot(MaplePotionPot potionPot)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.POTION_POT_UPDATE.getValue());
        PacketHelper.addPotionPotInfo(mplew, potionPot);

        return mplew.getPacket();
    }


    public static byte[] updataCoreAura(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_CORE_AURA.getValue());
        mplew.write(1);
        mplew.writeZeroBytes(6);
        mplew.write(4);
        mplew.writeZeroBytes(21);
        mplew.writeInt(8951284);
        mplew.writeLong(1L);
        PacketHelper.addCoreAura(mplew, chr);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\InventoryPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */