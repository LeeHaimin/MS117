package tools.packet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleClient;
import client.MapleStat;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePotionPot;
import constants.GameConstants;
import constants.ItemConstants;
import handling.CashShopOpcode;
import handling.SendPacketOpcode;
import server.MTSStorage;
import server.cashshop.CashItemFactory;
import server.cashshop.CashShop;
import tools.HexTool;
import tools.Pair;
import tools.data.output.MaplePacketLittleEndianWriter;

public class MTSCSPacket
{
    public static byte[] warpchartoCS(MapleClient c)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_CHAR.getValue());
        PacketHelper.addCharacterInfo(mplew, c.getPlayer());

        return mplew.getPacket();
    }

    public static byte[] warpCS(MapleClient c)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPEN.getValue());


        mplew.write(HexTool.getByteArrayFromHexString(c.getChannelServer().getShopPack()));

        return mplew.getPacket();
    }

    public static byte[] playCashSong(int itemid, String name)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CASH_SONG.getValue());
        mplew.writeInt(itemid);
        mplew.writeMapleAsciiString(name);

        return mplew.getPacket();
    }


    public static byte[] addCharBox(MapleCharacter c, int itemId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_CHAR_BOX.getValue());
        mplew.writeInt(c.getId());
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }


    public static byte[] removeCharBox(MapleCharacter c)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_CHAR_BOX.getValue());
        mplew.writeInt(c.getId());
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] useCharm(byte charmsleft, byte daysleft)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(8);
        mplew.write(1);
        mplew.write(charmsleft);
        mplew.write(daysleft);

        return mplew.getPacket();
    }

    public static byte[] useWheel(byte charmsleft)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(23);
        mplew.writeLong(charmsleft);

        return mplew.getPacket();
    }

    public static byte[] useAlienSocket(boolean start)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ALIEN_SOCKET_CREATOR.getValue());
        mplew.write(start ? 0 : 2);

        return mplew.getPacket();
    }


    public static byte[] sendHammerData(boolean start, int hammered)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.VICIOUS_HAMMER.getValue());


        mplew.write(start ? 82 : 90);
        mplew.writeInt(0);
        if (start)
        {
            mplew.writeInt(hammered);
        }
        return mplew.getPacket();
    }

    public static byte[] changePetFlag(int uniqueId, boolean added, int flagAdded)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.PET_FLAG_CHANGE.getValue());

        mplew.writeLong(uniqueId);
        mplew.write(added ? 1 : 0);
        mplew.writeShort(flagAdded);

        return mplew.getPacket();
    }

    public static byte[] changePetName(MapleCharacter chr, String newname, int slot)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.PET_NAMECHANGE.getValue());

        mplew.writeInt(chr.getId());
        mplew.write(0);
        mplew.writeMapleAsciiString(newname);
        mplew.writeInt(slot);

        return mplew.getPacket();
    }

    public static byte[] showNotes(ResultSet notes, int count) throws SQLException
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.SHOW_NOTES.getValue());
        mplew.write(3);
        mplew.write(count);
        for (int i = 0; i < count; i++)
        {
            mplew.writeInt(notes.getInt("id"));
            mplew.writeMapleAsciiString(notes.getString("from"));
            mplew.writeMapleAsciiString(notes.getString("message"));
            mplew.writeLong(PacketHelper.getKoreanTimestamp(notes.getLong("timestamp")));
            mplew.write(notes.getInt("gift"));
            notes.next();
        }

        return mplew.getPacket();
    }


    public static byte[] useChalkboard(int charid, String msg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CHALKBOARD.getValue());

        mplew.writeInt(charid);
        if ((msg == null) || (msg.length() <= 0))
        {
            mplew.write(0);
        }
        else
        {
            mplew.write(1);
            mplew.writeMapleAsciiString(msg);
        }

        return mplew.getPacket();
    }


    public static byte[] getTrockRefresh(MapleCharacter chr, byte vip, boolean delete)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.TROCK_LOCATIONS.getValue());
        mplew.write(delete ? 2 : 3);
        mplew.write(vip);
        if (vip == 1)
        {
            int[] map = chr.getRegRocks();
            for (int i = 0; i < 5; i++)
            {
                mplew.writeInt(map[i]);
            }
        }
        else if (vip == 2)
        {
            int[] map = chr.getRocks();
            for (int i = 0; i < 10; i++)
            {
                mplew.writeInt(map[i]);
            }
        }
        else if (vip == 3)
        {
            int[] map = chr.getHyperRocks();
            for (int i = 0; i < 13; i++)
            {
                mplew.writeInt(map[i]);
            }
        }
        return mplew.getPacket();
    }


    public static byte[] getTrockMessage(byte op)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.TROCK_LOCATIONS.getValue());


        mplew.writeShort(op);

        return mplew.getPacket();
    }


    public static byte[] 测试封包(String test)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.write(HexTool.getByteArrayFromHexString(test));

        return mplew.getPacket();
    }


    public static byte[] enableCSUse(int type)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_USE.getValue());


        mplew.write(type);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] updataPotionPot(MaplePotionPot potionPot)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_POTION_POT_UPDATE.getValue());
        PacketHelper.addPotionPotInfo(mplew, potionPot);

        return mplew.getPacket();
    }


    public static byte[] 刷新点卷信息(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_UPDATE.getValue());
        mplew.writeInt(chr.getCSPoints(1));
        mplew.writeInt(chr.getCSPoints(2));

        return mplew.getPacket();
    }


    public static byte[] updataMeso(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_UPDATE_MESO.getValue());
        mplew.writeLong(MapleStat.金币.getValue());
        mplew.writeLong(chr.getMeso());

        return mplew.getPacket();
    }


    public static byte[] 商城道具栏信息(MapleClient c)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.加载道具栏.getValue());
        mplew.write(0);
        CashShop mci = c.getPlayer().getCashInventory();
        int size = 0;
        mplew.writeShort(mci.getItemsSize());
        for (Item itemz : mci.getInventory())
        {
            addCashItemInfo(mplew, itemz, c.getAccID(), 0);
            if (ItemConstants.isPet(itemz.getItemId()))
            {
                size++;
            }
        }
        mplew.writeInt(size);
        if (mci.getInventory().size() > 0)
        {
            for (Item itemz : mci.getInventory())
            {
                if (ItemConstants.isPet(itemz.getItemId()))
                {
                    PacketHelper.addItemInfo(mplew, itemz);
                }
            }
        }
        mplew.writeShort(c.getPlayer().getStorage().getSlots());
        mplew.writeShort(c.getAccCharSlots());
        mplew.writeShort(0);
        mplew.writeShort(3);

        return mplew.getPacket();
    }

    public static void addCashItemInfo(MaplePacketLittleEndianWriter mplew, Item item, int accId, int sn)
    {
        CashItemFactory cashinfo = CashItemFactory.getInstance();
        mplew.writeLong(item.getUniqueId() > 0 ? item.getUniqueId() : 0L);
        mplew.writeLong(accId);
        mplew.writeInt(item.getItemId());
        mplew.writeInt(sn > 0 ? sn : cashinfo.getSnFromId(cashinfo.getLinkItemId(item.getItemId())));
        mplew.writeShort(item.getQuantity());
        mplew.writeAsciiString(item.getGiftFrom(), 13);
        PacketHelper.addExpirationTime(mplew, item.getExpiration());
        mplew.writeLong(item.getExpiration() == -1L ? 30L : 0L);
        mplew.writeZeroBytes(18);
        PacketHelper.addExpirationTime(mplew, -2L);
        mplew.writeZeroBytes(16);
    }

    public static byte[] 商城礼物信息(MapleClient c, List<Pair<Item, String>> gifts)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.加载礼物.getValue());
        mplew.writeShort(gifts.size());
        for (Pair<Item, String> gift : gifts)
        {
            mplew.writeLong(gift.getLeft().getUniqueId());
            mplew.writeInt(gift.getLeft().getItemId());
            mplew.writeAsciiString(gift.getLeft().getGiftFrom(), 13);
            mplew.writeAsciiString(gift.getRight(), 73);
        }

        return mplew.getPacket();
    }

    public static byte[] 商城购物车(MapleCharacter chr, boolean update)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(update ? CashShopOpcode.更新购物车.getValue() : CashShopOpcode.加载购物车.getValue());
        int[] list = chr.getWishlist();
        for (int i = 0; i < 12; i++)
        {
            mplew.writeInt(list[i] != -1 ? list[i] : 0);
        }
        return mplew.getPacket();
    }

    public static byte[] 购买商城道具(Item item, int sn, int accid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.购买道具.getValue());
        addCashItemInfo(mplew, item, accid, sn);
        mplew.writeZeroBytes(8);

        return mplew.getPacket();
    }

    public static byte[] 商城送礼(int itemid, int quantity, String receiver)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.商城送礼.getValue());
        mplew.writeMapleAsciiString(receiver);
        mplew.writeInt(itemid);
        mplew.writeShort(quantity);

        return mplew.getPacket();
    }

    public static byte[] 扩充道具栏(int inv, int slots)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.扩充道具栏.getValue());
        mplew.write(inv);
        mplew.writeShort(slots);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] 扩充仓库(int slots)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.扩充仓库.getValue());
        mplew.writeShort(slots);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] 购买角色卡(int slots)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.购买角色卡.getValue());
        mplew.writeShort(slots);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] 扩充项链(int days)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.扩充项链.getValue());
        mplew.writeShort(0);
        mplew.writeShort(days);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] 商城到背包(Item item)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.商城到背包.getValue());
        mplew.write(item.getQuantity());
        mplew.writeShort(item.getPosition());
        PacketHelper.addItemInfo(mplew, item);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] 背包到商城(Item item, int accId, int sn)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.背包到商城.getValue());
        addCashItemInfo(mplew, item, accId, sn);

        return mplew.getPacket();
    }

    public static byte[] 商城删除道具(int uniqueid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.删除道具.getValue());
        mplew.writeLong(uniqueid);

        return mplew.getPacket();
    }

    public static byte[] cashItemExpired(int uniqueid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.道具到期.getValue());
        mplew.writeLong(uniqueid);

        return mplew.getPacket();
    }

    public static byte[] 商城换购道具(int uniqueId, int Money)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.换购道具.getValue());
        mplew.writeLong(uniqueId);
        mplew.writeLong(Money);

        return mplew.getPacket();
    }

    public static byte[] 商城购买礼包(Map<Integer, Item> packageItems, int accId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.购买礼包.getValue());
        mplew.write(packageItems.size());
        int size = 0;
        for (Map.Entry<Integer, Item> sn : packageItems.entrySet())
        {
            addCashItemInfo(mplew, sn.getValue(), accId, sn.getKey());
            if ((ItemConstants.isPet(sn.getValue().getItemId())) || (ItemConstants.getInventoryType(sn.getValue().getItemId()) == MapleInventoryType.EQUIP))
            {
                size++;
            }
        }
        mplew.writeInt(size);
        if (packageItems.size() > 0)
        {
            for (Item itemz : packageItems.values())
            {
                if ((ItemConstants.isPet(itemz.getItemId())) || (ItemConstants.getInventoryType(itemz.getItemId()) == MapleInventoryType.EQUIP))
                {
                    PacketHelper.addItemInfo(mplew, itemz);
                }
            }
        }
        mplew.writeShort(0);

        return mplew.getPacket();
    }

    public static byte[] 商城送礼包(int itemId, int quantity, String receiver)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.商城送礼包.getValue());
        mplew.writeMapleAsciiString(receiver);
        mplew.writeInt(itemId);
        mplew.writeInt(quantity);

        return mplew.getPacket();
    }

    public static byte[] 商城购买任务道具(int price, short quantity, byte position, int itemid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.购买任务道具.getValue());
        mplew.writeInt(price);
        mplew.writeShort(quantity);
        mplew.writeShort(position);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }

    public static byte[] 商城错误提示(int err)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.错误提示.getValue());
        mplew.write(err);

        return mplew.getPacket();
    }


    public static byte[] showCouponRedeemedItem(int itemid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.writeShort(CashShopOpcode.领奖卡提示.getValue());
        mplew.writeInt(0);
        mplew.writeInt(1);
        mplew.writeShort(1);
        mplew.writeShort(26);
        mplew.writeInt(itemid);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] showCouponRedeemedItem(Map<Integer, Item> items, int mesos, int maplePoints, MapleClient c)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.领奖卡提示.getValue());
        mplew.write(items.size());
        for (Map.Entry<Integer, Item> item : items.entrySet())
        {
            addCashItemInfo(mplew, item.getValue(), c.getAccID(), item.getKey());
        }
        mplew.writeInt(maplePoints);
        mplew.writeInt(0);
        mplew.writeInt(mesos);

        return mplew.getPacket();
    }


    public static byte[] redeemResponse()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());


        mplew.write(CashShopOpcode.注册商城.getValue());
        mplew.writeInt(0);
        mplew.writeInt(1);

        return mplew.getPacket();
    }


    public static byte[] 商城打开箱子(Item item, Long uniqueId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_OPERATION.getValue());
        mplew.write(CashShopOpcode.打开箱子.getValue());
        mplew.writeLong(uniqueId);
        mplew.writeInt(0);
        PacketHelper.addItemInfo(mplew, item);
        mplew.writeInt(item.getPosition());
        mplew.writeShort(0);

        return mplew.getPacket();
    }


    public static byte[] 商城提示(int 消费, int 达到, int mode)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_MSG.getValue());


        mplew.write(CashShopOpcode.商城提示.getValue());
        mplew.writeInt(消费);
        mplew.writeInt(达到);
        mplew.write(mode);

        return mplew.getPacket();
    }

    public static byte[] 商城未知封包1()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_UNK1.getValue());
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] 热点推荐(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_HOT.getValue());
        int[] hotSn = {20400253, 20500121, 20600140, 20000348};

        mplew.writeInt(hotSn.length);
        for (int value : hotSn)
        {
            mplew.writeInt(value);
        }

        return mplew.getPacket();
    }


    public static byte[] 每日特卖()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_DAILY.getValue());
        mplew.writeInt(getTime());
        mplew.writeLong(0L);

        return mplew.getPacket();
    }

    public static int getTime()
    {
        String time = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).replace("-", "");
        return Integer.valueOf(time);
    }

    public static byte[] 商城未知封包2()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CS_UNK2.getValue());
        mplew.writeInt(0);
        mplew.writeInt(10300014);
        mplew.writeInt(50);
        mplew.writeInt(6120);
        mplew.writeInt(0);
        mplew.writeInt(20120914);
        mplew.writeInt(20120923);

        return mplew.getPacket();
    }

    public static byte[] showXmasSurprise(int idFirst, Item item, int accid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.XMAS_SURPRISE.getValue());
        mplew.write(230);
        mplew.writeLong(idFirst);
        mplew.writeInt(0);
        addCashItemInfo(mplew, item, accid, 0);
        mplew.writeInt(item.getItemId());
        mplew.write(1);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] getBoosterFamiliar(int cid, int familiar, int id)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOOSTER_FAMILIAR.getValue());
        mplew.writeInt(cid);
        mplew.writeInt(familiar);
        mplew.writeLong(id);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] getBoosterPack(int f1, int f2, int f3)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOOSTER_PACK.getValue());
        mplew.write(215);
        mplew.writeInt(f1);
        mplew.writeInt(f2);
        mplew.writeInt(f3);

        return mplew.getPacket();
    }

    public static byte[] getBoosterPackClick()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOOSTER_PACK.getValue());
        mplew.write(213);

        return mplew.getPacket();
    }

    public static byte[] getBoosterPackReveal()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOOSTER_PACK.getValue());
        mplew.write(214);

        return mplew.getPacket();
    }


    public static byte[] sendMesobagFailed()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MESOBAG_FAILURE.getValue());
        return mplew.getPacket();
    }


    public static byte[] sendMesobagSuccess(int mesos)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MESOBAG_SUCCESS.getValue());
        mplew.writeInt(mesos);
        return mplew.getPacket();
    }

    public static byte[] startMTS(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MTS_OPEN.getValue());

        PacketHelper.addCharacterInfo(mplew, chr);
        mplew.writeMapleAsciiString(chr.getClient().getAccountName());
        mplew.writeInt(2500);
        mplew.writeInt(5);
        mplew.writeInt(0);
        mplew.writeInt(24);
        mplew.writeInt(168);
        mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));

        return mplew.getPacket();
    }

    public static byte[] sendMTS(List<MTSStorage.MTSItemInfo> items, int tab, int type, int page, int pages)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());


        mplew.write(21);
        mplew.writeInt(pages);
        mplew.writeInt(items.size());
        mplew.writeInt(tab);
        mplew.writeInt(type);
        mplew.writeInt(page);
        mplew.write(1);
        mplew.write(1);

        for (MTSStorage.MTSItemInfo item : items)
        {
            addMTSItemInfo(mplew, item);
        }
        mplew.write(0);


        return mplew.getPacket();
    }

    private static void addMTSItemInfo(MaplePacketLittleEndianWriter mplew, MTSStorage.MTSItemInfo item)
    {
        PacketHelper.addItemInfo(mplew, item.getItem());
        mplew.writeInt(item.getId());
        mplew.writeInt(item.getTaxes());
        mplew.writeInt(item.getPrice());
        mplew.writeZeroBytes(GameConstants.GMS ? 4 : 8);
        mplew.writeLong(PacketHelper.getTime(item.getEndingDate()));
        mplew.writeMapleAsciiString(item.getSeller());
        mplew.writeMapleAsciiString(item.getSeller());
        mplew.writeZeroBytes(28);
    }

    public static byte[] showMTSCash(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GET_MTS_TOKENS.getValue());
        mplew.writeInt(chr.getCSPoints(2));

        return mplew.getPacket();
    }

    public static byte[] getMTSWantedListingOver(int nx, int items)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(61);
        mplew.writeInt(nx);
        mplew.writeInt(items);

        return mplew.getPacket();
    }

    public static byte[] getMTSConfirmSell()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(29);

        return mplew.getPacket();
    }

    public static byte[] getMTSFailSell()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(30);
        mplew.write(66);

        return mplew.getPacket();
    }

    public static byte[] getMTSConfirmBuy()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(51);

        return mplew.getPacket();
    }

    public static byte[] getMTSFailBuy()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(52);
        mplew.write(66);

        return mplew.getPacket();
    }

    public static byte[] getMTSConfirmCancel()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(37);

        return mplew.getPacket();
    }

    public static byte[] getMTSFailCancel()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(38);
        mplew.write(66);

        return mplew.getPacket();
    }

    public static byte[] getMTSConfirmTransfer(int quantity, int pos)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(39);
        mplew.writeInt(quantity);
        mplew.writeInt(pos);

        return mplew.getPacket();
    }

    public static byte[] getNotYetSoldInv(List<MTSStorage.MTSItemInfo> items)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(35);

        mplew.writeInt(items.size());

        for (MTSStorage.MTSItemInfo item : items)
        {
            addMTSItemInfo(mplew, item);
        }

        return mplew.getPacket();
    }

    public static byte[] getTransferInventory(List<Item> items, boolean changed)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        mplew.write(33);

        mplew.writeInt(items.size());
        int i = 0;
        for (Item item : items)
        {
            PacketHelper.addItemInfo(mplew, item);
            mplew.writeInt(Integer.MAX_VALUE - i);
            mplew.writeZeroBytes(GameConstants.GMS ? 52 : 56);
            i++;
        }
        mplew.writeInt(-47 + i - 1);
        mplew.write(changed ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] addToCartMessage(boolean fail, boolean remove)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MTS_OPERATION.getValue());
        if (remove)
        {
            if (fail)
            {
                mplew.write(44);
                mplew.writeInt(-1);
            }
            else
            {
                mplew.write(43);
            }
        }
        else if (fail)
        {
            mplew.write(42);
            mplew.writeInt(-1);
        }
        else
        {
            mplew.write(41);
        }


        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\MTSCSPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */