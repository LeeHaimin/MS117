package tools.packet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleQuestStatus;
import client.Skill;
import client.SkillEntry;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePet;
import client.inventory.MapleRing;
import server.shop.MapleShopItem;
import server.shops.IMaplePlayerShop;
import tools.data.output.MaplePacketLittleEndianWriter;

public class PacketHelper
{
    public static final long MAX_TIME = 150842304000000000L;
    public static final long ZERO_TIME = 94354848000000000L;
    public static final long PERMANENT = 150841440000000000L;

    public static long getKoreanTimestamp(long realTimestamp)
    {
        return realTimestamp * 10000L + 116444592000000000L;
    }

    public static long getTime(long realTimestamp)
    {
        if (realTimestamp == -1L) return MAX_TIME;
        if (realTimestamp == -2L) return ZERO_TIME;
        if (realTimestamp == -3L)
        {
            return PERMANENT;
        }
        return tools.DateUtil.getFileTimestamp(realTimestamp);
    }

    public static void addQuestInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        List<MapleQuestStatus> started = chr.getStartedQuests();
        mplew.write(1);
        mplew.writeShort(started.size());
        for (MapleQuestStatus q : started)
        {
            mplew.writeShort(q.getQuest().getId());
            if (q.hasMobKills())
            {
                StringBuilder sb = new StringBuilder();
                for (Integer integer : q.getMobKills().values())
                {
                    int kills = integer;
                    sb.append(tools.StringUtil.getLeftPaddedStr(String.valueOf(kills), '0', 3));
                }
                mplew.writeMapleAsciiString(sb.toString());
            }
            else
            {
                mplew.writeMapleAsciiString(q.getCustomData() == null ? "" : q.getCustomData());
            }
        }
        mplew.write(1);
        List<MapleQuestStatus> completed = chr.getCompletedQuests();
        mplew.writeShort((completed).size());
        for (MapleQuestStatus q : completed)
        {
            mplew.writeShort(q.getQuest().getId());
            mplew.writeInt(tools.DateUtil.getTime(q.getCompletionTime()));
        }
    }

    public static void addSkillInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        Map<Skill, SkillEntry> skills = chr.getSkills(true);
        mplew.write(1);
        mplew.writeShort(skills.size());
        for (Map.Entry<Skill, SkillEntry> skill : skills.entrySet())
        {


            mplew.writeInt(skill.getKey().getId());
            if (skill.getKey().isLinkSkills())
            {
                mplew.writeInt(skill.getValue().teachId);
            }
            else if (skill.getKey().isTeachSkills())
            {
                mplew.writeInt(skill.getValue().teachId > 0 ? skill.getValue().teachId : chr.getId());
            }
            else
            {
                mplew.writeInt(skill.getValue().skillevel);
            }
            addExpirationTime(mplew, skill.getValue().expiration);
            if (skill.getKey().isFourthJob())
            {
                mplew.writeInt(skill.getValue().masterlevel);
            }
            if (chr.isShowPacket())
            {
                String job = "addSkillInfo\\" + server.MapleCarnivalChallenge.getJobNameById(chr.getJob()) + ".txt";
                tools.FileoutputUtil.log(job,
                        "玩家技能: " + skill.getKey().getId() + " 名字: " + client.SkillFactory.getSkillName(skill.getKey().getId()) + " 技能等级: " + skill.getValue().skillevel + "/" + skill.getValue().masterlevel + " 是否写最大等级: " + skill.getKey().isFourthJob(), true);
            }
        }
    }

    public static void addCoolDownInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        List<client.MapleCoolDownValueHolder> cooldowns = chr.getCooldowns();
        mplew.writeShort(cooldowns.size());
        for (client.MapleCoolDownValueHolder cooling : cooldowns)
        {
            mplew.writeInt(cooling.skillId);
            int timeLeft = (int) (cooling.length + cooling.startTime - System.currentTimeMillis());
            mplew.writeInt(timeLeft / 1000);
        }
    }

    public static void addRocksInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        int[] mapz = chr.getRegRocks();
        for (int i = 0; i < 5; i++)
        {
            mplew.writeInt(mapz[i]);
        }
        int[] map = chr.getRocks();
        for (int i = 0; i < 10; i++)
        {
            mplew.writeInt(map[i]);
        }
        int[] maps = chr.getHyperRocks();
        for (int i = 0; i < 13; i++)
        {
            mplew.writeInt(maps[i]);
        }
    }

    public static void addRingInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        mplew.writeShort(0);


        tools.Triple<List<MapleRing>, List<MapleRing>, List<MapleRing>> aRing = chr.getRings(true);

        List<MapleRing> cRing = aRing.getLeft();
        mplew.writeShort(cRing.size());
        for (MapleRing ring : cRing)
        {
            mplew.writeInt(ring.getPartnerChrId());
            mplew.writeAsciiString(ring.getPartnerName(), 13);
            mplew.writeLong(ring.getRingId());
            mplew.writeLong(ring.getPartnerRingId());
        }
        List<MapleRing> fRing = aRing.getMid();
        mplew.writeShort(fRing.size());
        for (MapleRing ring : fRing)
        {
            mplew.writeInt(ring.getPartnerChrId());
            mplew.writeAsciiString(ring.getPartnerName(), 13);
            mplew.writeLong(ring.getRingId());
            mplew.writeLong(ring.getPartnerRingId());
            mplew.writeInt(ring.getItemId());
        }

        List<MapleRing> mRing = aRing.getRight();
        mplew.writeShort(mRing.size());
        int marriageId = 30000;
        for (MapleRing ring1 : mRing)
        {
            mplew.writeInt(marriageId);
            mplew.writeInt(chr.getId());
            mplew.writeInt(ring1.getPartnerChrId());
            mplew.writeShort(3);
            mplew.writeInt(ring1.getItemId());
            mplew.writeInt(ring1.getItemId());
            mplew.writeAsciiString(chr.getName(), 13);
            mplew.writeAsciiString(ring1.getPartnerName(), 13);
        }
    }


    public static void addPotionPotInfo(MaplePacketLittleEndianWriter mplew, client.inventory.MaplePotionPot potionPot)
    {
        mplew.writeInt(potionPot.getItmeId());
        mplew.writeInt(potionPot.getChrId());
        mplew.writeInt(potionPot.getMaxValue());
        mplew.writeInt(potionPot.getHp());
        mplew.writeInt(potionPot.getMp());
        mplew.writeLong(getTime(potionPot.getStartDate()));
        mplew.writeLong(getTime(potionPot.getEndDate()));
    }

    public static void addInventoryInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        mplew.writeLong(chr.getMeso());
        mplew.writeInt(chr.getId());
        mplew.writeInt(chr.getBeans());
        mplew.writeZeroBytes(12);
        mplew.writeInt(chr.getId());
        mplew.writeZeroBytes(31);
        mplew.writeInt(chr.getPotionPot() != null ? 1 : 0);
        if (chr.getPotionPot() != null)
        {
            addPotionPotInfo(mplew, chr.getPotionPot());
        }
        mplew.write(chr.getInventory(MapleInventoryType.EQUIP).getSlotLimit());
        mplew.write(chr.getInventory(MapleInventoryType.USE).getSlotLimit());
        mplew.write(chr.getInventory(MapleInventoryType.SETUP).getSlotLimit());
        mplew.write(chr.getInventory(MapleInventoryType.ETC).getSlotLimit());
        mplew.write(chr.getInventory(MapleInventoryType.CASH).getSlotLimit());

        MapleQuestStatus stat = chr.getQuestNoAdd(server.quest.MapleQuest.getInstance(122700));
        if ((stat != null) && (stat.getCustomData() != null) && (Long.parseLong(stat.getCustomData()) > System.currentTimeMillis()))
        {
            mplew.writeLong(getTime(Long.parseLong(stat.getCustomData())));
        }
        else
        {
            mplew.writeLong(getTime(-2L));
        }
        mplew.write(0);

        MapleInventory iv = chr.getInventory(MapleInventoryType.EQUIPPED);
        List<Item> equippedList = iv.newList();
        java.util.Collections.sort(equippedList);
        List<Item> equipped = new ArrayList<>();
        List<Item> equippedCash = new ArrayList<>();
        List<Item> equippedDragon = new ArrayList<>();
        List<Item> equippedMechanic = new ArrayList<>();
        List<Item> equippedAndroid = new ArrayList<>();
        List<Item> equippedTotem = new ArrayList<>();
        List<Item> equippedLolitaCash = new ArrayList<>();
        for (Item item : equippedList)
        {
            if ((item.getPosition() < 0) && (item.getPosition() > -100))
            {
                equipped.add(item);
            }
            else if ((item.getPosition() <= -100) && (item.getPosition() > 64536))
            {
                equippedCash.add(item);
            }
            else if ((item.getPosition() <= 64536) && (item.getPosition() > 64436))
            {
                equippedDragon.add(item);
            }
            else if ((item.getPosition() <= 64436) && (item.getPosition() > 64336))
            {
                equippedMechanic.add(item);
            }
            else if ((item.getPosition() <= 64336) && (item.getPosition() > 64236))
            {
                equippedAndroid.add(item);
            }
            else if ((item.getPosition() <= 60536) && (item.getPosition() > 60533))
            {
                equippedTotem.add(item);
            }
            else if ((item.getPosition() <= 64236) && (item.getPosition() > 64230))
            {
                equippedLolitaCash.add(item);
            }
        }

        for (Item item : equipped)
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        for (Item item : equippedCash)
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        iv = chr.getInventory(MapleInventoryType.EQUIP);
        for (Item item : iv.list())
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        for (Item item : equippedDragon)
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        for (Item item : equippedMechanic)
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        for (Item item : equippedAndroid)
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        for (Item item : equippedTotem)
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        for (Item item : equippedLolitaCash)
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.writeShort(0);
        mplew.writeShort(0);
        iv = chr.getInventory(MapleInventoryType.USE);
        for (Item item : iv.list())
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.write(0);
        iv = chr.getInventory(MapleInventoryType.SETUP);
        for (Item item : iv.list())
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.write(0);
        iv = chr.getInventory(MapleInventoryType.ETC);
        for (Item item : iv.list())
        {
            if (item.getPosition() < 100)
            {
                addItemPosition(mplew, item, false, false);
                addItemInfo(mplew, item, chr);
            }
        }
        mplew.write(0);
        iv = chr.getInventory(MapleInventoryType.CASH);
        for (Item item : iv.list())
        {
            addItemPosition(mplew, item, false, false);
            addItemInfo(mplew, item, chr);
        }
        mplew.writeInt(0);
        mplew.write(0);
        mplew.writeInt(chr.getExtendedSlots().size());
        for (int i = 0; i < chr.getExtendedSlots().size(); i++)
        {
            mplew.writeInt(i);
            mplew.writeInt(chr.getExtendedSlot(i));
            for (Item item : chr.getInventory(MapleInventoryType.ETC).list())
            {
                if ((item.getPosition() > i * 100 + 100) && (item.getPosition() < i * 100 + 200))
                {
                    addItemPosition(mplew, item, false, true);
                    addItemInfo(mplew, item, chr);
                }
            }
            mplew.writeInt(-1);
        }
        mplew.writeZeroBytes(9);
    }

    public static void addCharStats(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        mplew.writeInt(chr.getId());
        mplew.writeAsciiString(chr.getName(), 13);
        mplew.write(chr.getGender());
        mplew.write(chr.getSkinColor());
        mplew.writeInt(chr.getFace());
        mplew.writeInt(chr.getHair());

        mplew.write(chr.getLevel());
        mplew.writeShort(chr.getJob());
        chr.getStat().connectData(mplew);
        mplew.writeShort(chr.getRemainingAp());
        int size;
        int i;
        if (constants.GameConstants.isSeparatedSpJob(chr.getJob()))
        {
            size = chr.getRemainingSpSize();
            mplew.write(size);
            for (i = 0; i < chr.getRemainingSps().length; i++)
            {
                if (chr.getRemainingSp(i) > 0)
                {
                    mplew.write(i + 1);
                    mplew.writeInt(chr.getRemainingSp(i));
                }
            }
        }
        else
        {
            mplew.writeShort(chr.getRemainingSp());
        }
        mplew.writeLong(chr.getExp());
        mplew.writeInt(chr.getFame());
        mplew.write(new byte[12]);
        mplew.writeLong(tools.DateUtil.getFileTimestamp(System.currentTimeMillis()));
        mplew.writeInt(chr.getMapId());
        mplew.write(chr.getInitialSpawnpoint());
        mplew.writeShort(chr.getSubcategory());
        if (chr.hasDecorate())
        {
            mplew.writeInt(chr.getDecorate());
        }
        mplew.write(chr.getFatigue());
        mplew.writeInt(tools.DateUtil.getTime());


        for (client.MapleTraitType t : client.MapleTraitType.values())
        {
            mplew.writeInt(chr.getTrait(t).getTotalExp());
        }
        for (client.MapleTraitType t : client.MapleTraitType.values())
        {
            mplew.writeShort(0);
        }
        mplew.write(0);
        mplew.writeLong(getTime(-2L));


        mplew.writeInt(chr.getStat().pvpExp);
        mplew.write(chr.getStat().pvpRank);
        mplew.writeInt(chr.getBattlePoints());
        mplew.write(6);
        mplew.write(7);
        mplew.write(new byte[5]);
        mplew.writeReversedLong(getTime(-2L));
        mplew.write(new byte[5]);

        chr.getCharacterCard().connectData(mplew);
        mplew.writeReversedInt(getTime(System.currentTimeMillis()));
        mplew.writeReversedLong(getTime(System.currentTimeMillis()));
    }

    public static void addCharLook(MaplePacketLittleEndianWriter mplew, MapleCharacter chr, boolean mega, boolean second)
    {
        mplew.write(second ? chr.getSecondGender() : chr.getGender());
        mplew.write(chr.getSkinColor());
        mplew.writeInt(second ? chr.getSecondFace() : chr.getFace());
        mplew.writeInt(chr.getJob());
        mplew.write(mega ? 0 : 1);
        mplew.writeInt(second ? chr.getSecondHair() : chr.getHair());

        Map<Byte, Integer> myEquip = new LinkedHashMap<>();
        Map<Byte, Integer> maskedEquip = new LinkedHashMap<>();
        Map<Byte, Integer> totemEquip = new LinkedHashMap<>();
        MapleInventory equip = chr.getInventory(MapleInventoryType.EQUIPPED);

        for (Item item : equip.newList())
        {
            if ((item.getPosition() <= 60536) && (item.getPosition() > 60533))
            {
                byte pos = (byte) (item.getPosition() * -1 - 5000);
                if (totemEquip.get(pos) == null)
                {
                    totemEquip.put(pos, item.getItemId());
                }
            }
            if (item.getPosition() >= -128)
            {


                byte pos = (byte) (item.getPosition() * -1);
                if ((pos < 100) && (myEquip.get(pos) == null))
                {
                    Equip skin = (Equip) item;
                    myEquip.put(pos, skin.getItemSkin() % 10000 > 0 ? skin.getItemSkin() : item.getItemId());
                }
                else if (((pos > 100) || (pos == Byte.MIN_VALUE)) && (pos != 111))
                {
                    pos = (byte) (pos == Byte.MIN_VALUE ? 28 : pos - 100);
                    if (myEquip.get(pos) != null)
                    {
                        maskedEquip.put(pos, myEquip.get(pos));
                    }
                    myEquip.put(pos, item.getItemId());
                }
                else if (myEquip.get(pos) != null)
                {
                    maskedEquip.put(pos, item.getItemId());
                }
            }
        }


        boolean zero = constants.GameConstants.is神之子(chr.getJob());
        int itemId;
        if ((zero) && (second) && (myEquip.containsKey((byte) 10)))
        {
            itemId = myEquip.remove((byte) 10);
            myEquip.put((byte) 11, itemId);
        }


        for (Map.Entry<Byte, Integer> entry : myEquip.entrySet())
        {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }

        mplew.write(255);

        for (Map.Entry<Byte, Integer> entry : maskedEquip.entrySet())
        {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }

        mplew.write(255);

        for (Map.Entry<Byte, Integer> entry : totemEquip.entrySet())
        {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }

        mplew.write(255);

        Item cWeapon = equip.getItem((short) -111);
        mplew.writeInt(cWeapon != null ? cWeapon.getItemId() : 0);

        Item weapon = equip.getItem((short) (second ? -10 : -11));
        mplew.writeInt(weapon != null ? weapon.getItemId() : 0);

        Item subWeapon = equip.getItem((short) -10);
        mplew.writeInt((!zero) && (subWeapon != null) ? subWeapon.getItemId() : 0);

        mplew.writeBool(chr.isElfEar());

        for (int i = 0; i < 3; i++)
        {
            mplew.writeInt((!second) && (chr.getSpawnPet(i) != null) ? chr.getSpawnPet(i).getPetItemId() : 0);
        }
        if (chr.hasDecorate())
        {
            mplew.writeInt(chr.getDecorate());
        }
        if (zero)
        {
            mplew.write(second ? 1 : 0);
        }
        if (constants.GameConstants.is林之灵(chr.getJob()))
        {
            chr.checkTailAndEar();
            mplew.write(1);
            mplew.writeInt(5010116);
            mplew.write(1);
            mplew.writeInt(5010119);
        }
    }

    public static void addExpirationTime(MaplePacketLittleEndianWriter mplew, long time)
    {
        mplew.writeLong(getTime(time));
    }

    public static void addItemPosition(MaplePacketLittleEndianWriter mplew, Item item, boolean trade, boolean bagSlot)
    {
        if (item == null)
        {
            mplew.write(0);
            return;
        }
        short pos = item.getPosition();
        if (pos <= -1)
        {
            pos = (short) (pos * -1);
            if ((pos > 100) && (pos < 1000))
            {
                pos = (short) (pos - 100);
            }
        }
        if (bagSlot)
        {
            mplew.writeInt(pos % 100 - 1);
        }
        else if ((!trade) && (item.getType() == 1))
        {
            mplew.writeShort(pos);
        }
        else
        {
            mplew.write(pos);
        }
    }

    public static void addItemInfo(MaplePacketLittleEndianWriter mplew, Item item)
    {
        addItemInfo(mplew, item, null);
    }

    public static void addItemInfo(MaplePacketLittleEndianWriter mplew, Item item, MapleCharacter chr)
    {
        mplew.write(item.getPet() != null ? 3 : item.getType());
        mplew.writeInt(item.getItemId());

        boolean hasUniqueId = (item.getUniqueId() > 0) && (!constants.ItemConstants.is结婚戒指(item.getItemId())) && (item.getItemId() / 10000 != 166);
        mplew.write(hasUniqueId ? 1 : 0);
        if (hasUniqueId)
        {
            mplew.writeLong(item.getUniqueId());
        }
        if (item.getPet() != null)
        {
            addPetItemInfo(mplew, item, item.getPet(), true);
        }
        else
        {
            addExpirationTime(mplew, item.getExpiration());
            mplew.writeInt(chr == null ? -1 : chr.getExtendedSlots().indexOf(item.getItemId()));
            if (item.getType() == 1)
            {
                Equip equip = (Equip) item;
                mplew.writeInt(equip.getEquipFlag());
                if (equip.getUpgradeSlots() > 0)
                {
                    mplew.write(equip.getUpgradeSlots());
                }
                if (equip.getLevel() > 0)
                {
                    mplew.write(equip.getLevel());
                }
                if (equip.getStr() > 0)
                {
                    mplew.writeShort(equip.getStr());
                }
                if (equip.getDex() > 0)
                {
                    mplew.writeShort(equip.getDex());
                }
                if (equip.getInt() > 0)
                {
                    mplew.writeShort(equip.getInt());
                }
                if (equip.getLuk() > 0)
                {
                    mplew.writeShort(equip.getLuk());
                }
                if (equip.getHp() > 0)
                {
                    mplew.writeShort(equip.getHp());
                }
                if (equip.getMp() > 0)
                {
                    mplew.writeShort(equip.getMp());
                }
                if (equip.getWatk() > 0)
                {
                    mplew.writeShort(equip.getWatk());
                }
                if (equip.getMatk() > 0)
                {
                    mplew.writeShort(equip.getMatk());
                }
                if (equip.getWdef() > 0)
                {
                    mplew.writeShort(equip.getWdef());
                }
                if (equip.getMdef() > 0)
                {
                    mplew.writeShort(equip.getMdef());
                }
                if (equip.getAcc() > 0)
                {
                    mplew.writeShort(equip.getAcc());
                }
                if (equip.getAvoid() > 0)
                {
                    mplew.writeShort(equip.getAvoid());
                }
                if (equip.getHands() > 0)
                {
                    mplew.writeShort(equip.getHands());
                }
                if (equip.getSpeed() > 0)
                {
                    mplew.writeShort(equip.getSpeed());
                }
                if (equip.getJump() > 0)
                {
                    mplew.writeShort(equip.getJump());
                }
                if (equip.getFlag() > 0)
                {
                    mplew.writeInt(equip.getFlag());
                }
                if (equip.getIncSkill() > 0)
                {
                    mplew.write(equip.getIncSkill() > 0 ? 1 : 0);
                }
                if (equip.getEquipLevel() > 0)
                {
                    mplew.write(Math.max(equip.getBaseLevel(), equip.getEquipLevel()));
                }
                if (equip.getExpPercentage() > 0)
                {
                    mplew.writeLong(equip.getExpPercentage() * 100000);
                }
                if (equip.getDurability() > 0)
                {
                    mplew.writeInt(equip.getDurability());
                }
                if (equip.getViciousHammer() > 0)
                {
                    mplew.writeInt(equip.getViciousHammer());
                }
                if (equip.getPVPDamage() > 0)
                {
                    mplew.writeShort(equip.getPVPDamage());
                }
                if (equip.getEnhanctBuff() > 0)
                {
                    mplew.writeShort(equip.getEnhanctBuff());
                }
                if (equip.getReqLevel() > 0)
                {
                    mplew.write(equip.getReqLevel());
                }
                if (equip.getYggdrasilWisdom() > 0)
                {
                    mplew.write(equip.getYggdrasilWisdom());
                }
                if (equip.getFinalStrike())
                {
                    mplew.writeBool(equip.getFinalStrike());
                }
                if (equip.getBossDamage() > 0)
                {
                    mplew.write(equip.getBossDamage());
                }
                if (equip.getIgnorePDR() > 0)
                {
                    mplew.write(equip.getIgnorePDR());
                }


                mplew.writeInt(equip.getEquipSpecialFlag());
                if (equip.getTotalDamage() > 0)
                {
                    mplew.write(equip.getTotalDamage());
                }
                if (equip.getAllStat() > 0)
                {
                    mplew.write(equip.getAllStat());
                }
                mplew.write(equip.getKarmaCount());

                mplew.writeMapleAsciiString(equip.getOwner());
                mplew.write(equip.getState(true));
                mplew.write(equip.getEnhance());
                mplew.writeShort(equip.getPotential1() <= 0 ? 0 : equip.getPotential1());
                mplew.writeShort(equip.getPotential2() <= 0 ? 0 : equip.getPotential2());
                mplew.writeShort(equip.getPotential3() <= 0 ? 0 : equip.getPotential3());
                mplew.writeShort(equip.getPotential4() < 0 ? 1 : equip.getPotential4());
                mplew.writeShort(equip.getPotential5() < 0 ? 1 : equip.getPotential5());
                mplew.writeShort(equip.getPotential6() < 0 ? 1 : equip.getPotential6());
                mplew.writeShort(equip.getItemSkin() % 10000);


                mplew.writeShort(equip.getSocketState());
                mplew.writeShort(equip.getSocket1() % 10000);
                mplew.writeShort(equip.getSocket2() % 10000);
                mplew.writeShort(equip.getSocket3() % 10000);

                if (!hasUniqueId)
                {
                    mplew.writeLong(equip.getEquipOnlyId());
                }
                mplew.writeLong(getTime(-2L));
                mplew.writeInt(-1);
                mplew.writeLong(0L);
                mplew.writeLong(getTime(-2L));
                mplew.writeLong(0L);
                mplew.writeLong(0L);
                mplew.writeShort(0);
                mplew.writeInt(0);
                mplew.writeInt(equip.getLimitBreak());
            }
            else
            {
                mplew.writeShort(item.getQuantity());
                mplew.writeMapleAsciiString(item.getOwner());
                mplew.writeShort(item.getFlag());
                mplew.writeShort(0);
                if ((constants.ItemConstants.is飞镖道具(item.getItemId())) || (constants.ItemConstants.is子弹道具(item.getItemId())) || (item.getItemId() / 10000 == 287))
                {
                    mplew.writeLong(item.getInventoryId() <= 0L ? 0L : item.getInventoryId());
                }
            }
        }
    }


    public static void serializeMovementList(MaplePacketLittleEndianWriter lew, List<server.movement.LifeMovementFragment> moves)
    {
        lew.write(moves.size());
        for (server.movement.LifeMovementFragment move : moves)
        {
            move.serialize(lew);
        }
    }

    public static void addAnnounceBox(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        if ((chr.getPlayerShop() != null) && (chr.getPlayerShop().isOwner(chr)) && (chr.getPlayerShop().getShopType() != 1) && (chr.getPlayerShop().isAvailable()))
        {
            addInteraction(mplew, chr.getPlayerShop());
        }
        else
        {
            mplew.write(0);
        }
    }

    public static void addInteraction(MaplePacketLittleEndianWriter mplew, IMaplePlayerShop shop)
    {
        mplew.write(shop.getGameType());
        mplew.writeInt(((server.shops.AbstractPlayerStore) shop).getObjectId());
        mplew.writeMapleAsciiString(shop.getDescription());
        if (shop.getShopType() != 1)
        {
            mplew.write(shop.getPassword().length() > 0 ? 1 : 0);
        }
        mplew.write(shop.getItemId() - 5030000);
        mplew.write(shop.getSize());
        mplew.write(shop.getMaxSize());
        if (shop.getShopType() != 1)
        {
            mplew.write(shop.isOpen() ? 0 : 1);
        }
    }

    public static void addCharacterInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        mplew.writeLong(-1L);
        mplew.write(new byte[7]);
        mplew.write(new byte[12]);
        addCharStats(mplew, chr);
        mplew.write(chr.getBuddylist().getCapacity());

        if (chr.getBlessOfFairyOrigin() != null)
        {
            mplew.write(1);
            mplew.writeMapleAsciiString(chr.getBlessOfFairyOrigin());
        }
        else
        {
            mplew.write(0);
        }

        if (chr.getBlessOfEmpressOrigin() != null)
        {
            mplew.write(1);
            mplew.writeMapleAsciiString(chr.getBlessOfEmpressOrigin());
        }
        else
        {
            mplew.write(0);
        }

        MapleQuestStatus ultExplorer = chr.getQuestNoAdd(server.quest.MapleQuest.getInstance(111111));
        if ((ultExplorer != null) && (ultExplorer.getCustomData() != null))
        {
            mplew.write(1);
            mplew.writeMapleAsciiString(ultExplorer.getCustomData());
        }
        else
        {
            mplew.write(0);
        }
        addInventoryInfo(mplew, chr);
        addSkillInfo(mplew, chr);
        mplew.writeShort(0);
        addCoolDownInfo(mplew, chr);
        addQuestInfo(mplew, chr);
        addRingInfo(mplew, chr);
        addRocksInfo(mplew, chr);

        addQuestDataInfo(mplew, chr);
        mplew.writeZeroBytes(6);
        addJaguarInfo(mplew, chr);
        addZeroInfo(mplew, chr);
        mplew.writeZeroBytes(4);
        addPhantomSkills(mplew, chr);
        mplew.writeShort(chr.getInnerSkillSize());
        for (int i = 0; i < 3; i++)
        {
            client.InnerSkillEntry innerSkill = chr.getInnerSkills()[i];
            if (innerSkill != null)
            {
                mplew.write(innerSkill.getPosition());
                mplew.writeInt(innerSkill.getSkillId());
                mplew.write(innerSkill.getSkillLevel());
                mplew.write(innerSkill.getRank());
            }
        }
        mplew.writeShort(0);
        mplew.writeInt(chr.getHonorLevel());
        mplew.writeInt(chr.getHonorExp());
        mplew.write(1);
        mplew.writeZeroBytes(24);
        mplew.writeLong(getTime(-2L));
        mplew.writeZeroBytes(5);
        mplew.writeInt(chr.getLove());
        mplew.writeLong(getTime(-2L));
        mplew.writeInt(0);
        mplew.write(new byte[]{0, 1, 0});
        mplew.writeLong(1L);
        mplew.writeInt(100);
        mplew.writeLong(getTime(System.currentTimeMillis()));
        mplew.writeZeroBytes(4);
        mplew.writeZeroBytes(9);
        mplew.write(tools.HexTool.getByteArrayFromHexString("EC 21 1C 12"));
        mplew.writeZeroBytes(8);
        addCoreAura(mplew, chr);
        mplew.writeInt(0);
        mplew.writeInt(chr.getClient().getAccID());
        mplew.writeInt(chr.getId());
        int size = 5;
        mplew.writeLong(size);
        for (int i = 0; i < size; i++)
        {
            mplew.writeLong(i == 4 ? 9410198L : 9410165 + i);
        }

        mplew.writeInt(chr.getId());
        mplew.writeLong(0L);
        mplew.writeLong(getTime(-2L));
        mplew.writeInt(30);
    }


    public static void addQuestDataInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        Map<Integer, String> questInfos = chr.getInfoQuest_Map();
        mplew.writeShort(questInfos.size());
        for (Map.Entry<Integer, String> quest : questInfos.entrySet())
        {
            mplew.writeShort(quest.getKey());
            mplew.writeMapleAsciiString(quest.getValue() == null ? "" : quest.getValue());
        }
    }


    public static void addZeroInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        if (constants.GameConstants.is神之子(chr.getJob()))
        {
            chr.getStat().zeroData(mplew, chr);
        }
    }


    public static void addCoreAura(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        client.MapleCoreAura aura = chr.getCoreAura();
        if (aura != null)
        {
            mplew.writeInt(chr.getId());
            mplew.writeInt(aura.getId());
            mplew.writeInt(aura.getLevel());
            mplew.writeInt(aura.getCoreAuraLevel());
            mplew.writeInt(aura.getTotal());
            mplew.writeInt(aura.getWatk());
            mplew.writeInt(aura.getDex());
            mplew.writeInt(aura.getLuk());
            mplew.writeInt(aura.getMagic());
            mplew.writeInt(aura.getInt());
            mplew.writeInt(aura.getStr());
            mplew.writeInt(5);
            mplew.writeInt(32);
            mplew.writeInt(18);
            mplew.writeInt(68);
            mplew.writeLong(getTime(aura.getExpiration()));
        }
        else
        {
            mplew.writeZeroBytes(60);
            mplew.writeLong(getTime(System.currentTimeMillis()));
        }
        mplew.writeInt(0);
        mplew.write(1);
    }

    public static void addPhantomSkills(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        if (constants.GameConstants.is幻影(chr.getJob()))
        {
            for (int i = 0; i < 13; i++)
            {
                mplew.writeInt(chr.幻影复制技能(i));
            }

            int[] skills = {24001001, 24101001, 24111001, 24121001};
            for (int i : skills)
            {
                mplew.writeInt(chr.获取幻影装备技能(client.SkillFactory.getSkill(i)));
            }
        }
        else
        {
            mplew.writeZeroBytes(68);
        }
    }

    public static void addMonsterBookInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        mplew.writeInt(0);
        if (chr.getMonsterBook().getSetScore() > 0)
        {
            chr.getMonsterBook().writeFinished(mplew);
        }
        else
        {
            chr.getMonsterBook().writeUnfinished(mplew);
        }
        mplew.writeInt(chr.getMonsterBook().getSet());
        mplew.writeZeroBytes(9);
    }


    public static void addPetItemInfo(MaplePacketLittleEndianWriter mplew, Item item, MaplePet pet, boolean active)
    {
        if (item == null)
        {
            mplew.writeLong(getKoreanTimestamp((long) (System.currentTimeMillis() * 1.5D)));
        }
        else
        {
            addExpirationTime(mplew, item.getExpiration() <= System.currentTimeMillis() ? -1L : item.getExpiration());
        }
        mplew.writeInt(-1);
        mplew.writeAsciiString(pet.getName(), 13);
        mplew.write(pet.getLevel());
        mplew.writeShort(pet.getCloseness());
        mplew.write(pet.getFullness());
        if (item == null)
        {
            mplew.writeLong(getKoreanTimestamp((long) (System.currentTimeMillis() * 1.5D)));
        }
        else
        {
            addExpirationTime(mplew, item.getExpiration() <= System.currentTimeMillis() ? -1L : item.getExpiration());
        }
        mplew.writeShort(0);
        mplew.writeShort(pet.getFlags());
        mplew.writeInt((pet.getPetItemId() == 5000054) && (pet.getSecondsLeft() > 0) ? pet.getSecondsLeft() : 0);
        mplew.writeShort(pet.isCanPickup() ? 0 : 2);
        mplew.write(active ? 0 : pet.getSummoned() ? pet.getSummonedValue() : 0);
        mplew.writeInt(active ? pet.getBuffSkill() : 0);
        mplew.writeInt(-1);
        mplew.writeShort(100);
        mplew.write(new byte[12]);
    }


    public static void addShopInfo(MaplePacketLittleEndianWriter mplew, server.shop.MapleShop shop, client.MapleClient c)
    {
        server.MapleItemInformationProvider ii = server.MapleItemInformationProvider.getInstance();

        mplew.write(0);
        mplew.writeInt(tools.DateUtil.getTime());
        mplew.write(shop.getRanks().size() > 0 ? 1 : 0);
        Iterator localIterator;
        tools.Pair<Integer, String> s;
        if (shop.getRanks().size() > 0)
        {
            mplew.write(shop.getRanks().size());
            for (localIterator = shop.getRanks().iterator(); localIterator.hasNext(); )
            {
                s = (tools.Pair) localIterator.next();
                mplew.writeInt(s.left);
                mplew.writeMapleAsciiString(s.right);
            }
        }
        List<MapleShopItem> shopItems = shop.getItems(c);
        mplew.writeShort((shopItems).size());
        for (MapleShopItem item : shopItems)
        {
            mplew.writeLong(0L);
            mplew.writeInt(item.getItemId());
            mplew.writeInt(item.getPrice());
            mplew.write(0);
            mplew.writeInt(item.getReqItem());
            mplew.writeInt(item.getReqItemQ());
            mplew.writeZeroBytes(16);
            mplew.writeLong(1440 * item.getPeriod());
            mplew.writeZeroBytes(8);
            mplew.writeLong(getTime(-2L));
            mplew.writeLong(getTime(-1L));
            mplew.writeInt(item.getRank());
            mplew.writeMapleAsciiString("1900010100");
            mplew.writeMapleAsciiString("2079010100");
            mplew.write(item.getState() > 0 ? 1 : 0);
            mplew.write(0);
            mplew.writeInt(0);
            if ((!constants.ItemConstants.is飞镖道具(item.getItemId())) && (!constants.ItemConstants.is子弹道具(item.getItemId())))
            {
                mplew.writeShort(1);
                mplew.writeShort(item.getBuyable());
            }
            else
            {
                mplew.writeZeroBytes(6);
                mplew.writeShort(tools.BitTools.doubleToShortBits(ii.getPrice(item.getItemId())));
                mplew.writeShort(ii.getSlotMax(item.getItemId()));
            }

            Item rebuy = item.getRebuy();
            mplew.write(rebuy == null ? 0 : 1);
            if (rebuy != null)
            {
                addItemInfo(mplew, rebuy);
            }

            if (shop.getRanks().size() > 0)
            {
                mplew.write(item.getRank() >= 0 ? 1 : 0);
                if (item.getRank() >= 0)
                {
                    mplew.write(item.getRank());
                }
            }

            mplew.write(new byte[16]);
            int size = 5;
            for (int i = 0; i < size; i++)
            {
                mplew.writeLong(9410165 + i);
            }
        }
    }

    public static void addJaguarInfo(MaplePacketLittleEndianWriter mplew, MapleCharacter chr)
    {
        if ((chr.getJob() >= 3300) && (chr.getJob() <= 3312))
        {
            mplew.write(chr.getIntNoRecord(111112));
            for (int i = 0; i < 5; i++)
            {
                mplew.writeInt(0);
            }
        }
    }

    public static <E extends handling.Buffstat> void writeSingleMask(MaplePacketLittleEndianWriter mplew, E statup)
    {
        for (int i = 12; i >= 1; i--)
        {
            mplew.writeInt(i == statup.getPosition() ? statup.getValue() : 0);
        }
    }

    public static <E extends handling.Buffstat> void writeMask(MaplePacketLittleEndianWriter mplew, java.util.Collection<E> statups)
    {
        int[] mask = new int[12];
        for (E statup : statups)
        {
            mask[(statup.getPosition() - 1)] |= statup.getValue();
        }
        for (int i = mask.length; i >= 1; i--)
        {
            mplew.writeInt(mask[(i - 1)]);
        }
    }

    public static <E extends handling.Buffstat> void writeBuffMask(MaplePacketLittleEndianWriter mplew, java.util.Collection<tools.Pair<E, Integer>> statups)
    {
        int[] mask = new int[12];
        for (tools.Pair<E, Integer> statup : statups)
        {
            mask[(statup.left.getPosition() - 1)] |= statup.left.getValue();
        }
        for (int i = mask.length; i >= 1; i--)
        {
            mplew.writeInt(mask[(i - 1)]);
        }
    }

    public static <E extends handling.Buffstat> void writeBuffMask(MaplePacketLittleEndianWriter mplew, Map<E, Integer> statups)
    {
        int[] mask = new int[12];
        for (E statup : statups.keySet())
        {
            mask[(statup.getPosition() - 1)] |= statup.getValue();
        }
        for (int i = mask.length; i >= 1; i--)
        {
            mplew.writeInt(mask[(i - 1)]);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\PacketHelper.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */