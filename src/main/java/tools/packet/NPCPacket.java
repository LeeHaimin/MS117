package tools.packet;

import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import client.MapleClient;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import handling.SendPacketOpcode;
import server.ServerProperties;
import server.life.MapleNPC;
import server.life.PlayerNPC;
import server.shop.MapleShop;
import server.shop.MapleShopResponse;
import tools.HexTool;
import tools.Pair;
import tools.data.output.MaplePacketLittleEndianWriter;


public class NPCPacket
{
    private static final Logger log = Logger.getLogger(NPCPacket.class);

    public static byte[] sendNpcHide(int[] npcIds)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NPC_HIDE.getValue());
        mplew.write(npcIds.length);
        for (int npcId : npcIds)
        {
            mplew.writeInt(npcId);
        }

        return mplew.getPacket();
    }

    public static byte[] spawnNPC(MapleNPC life, boolean show)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_NPC.getValue());
        mplew.writeInt(life.getObjectId());
        mplew.writeInt(life.getId());
        mplew.writeShort(life.getPosition().x);
        mplew.writeShort(life.getCy());
        mplew.write(life.getF() == 1 ? 0 : 1);
        mplew.writeShort(life.getFh());
        mplew.writeShort(life.getRx0());
        mplew.writeShort(life.getRx1());
        mplew.write(show ? 1 : 0);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] removeNPC(int objectid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_NPC.getValue());
        mplew.writeInt(objectid);

        return mplew.getPacket();
    }

    public static byte[] removeNPCController(int objectid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_NPC_REQUEST_CONTROLLER.getValue());
        mplew.write(0);
        mplew.writeInt(objectid);

        return mplew.getPacket();
    }

    public static byte[] spawnNPCRequestController(MapleNPC life, boolean MiniMap)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_NPC_REQUEST_CONTROLLER.getValue());
        mplew.write(1);
        mplew.writeInt(life.getObjectId());
        mplew.writeInt(life.getId());
        mplew.writeShort(life.getPosition().x);
        mplew.writeShort(life.getCy());
        mplew.write(life.getF() == 1 ? 0 : 1);
        mplew.writeShort(life.getFh());
        mplew.writeShort(life.getRx0());
        mplew.writeShort(life.getRx1());
        mplew.write(MiniMap ? 1 : 0);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] spawnPlayerNPC(PlayerNPC npc)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.PLAYER_NPC.getValue());
        mplew.write(npc.getF() == 1 ? 0 : 1);
        mplew.writeInt(npc.getId());
        mplew.writeMapleAsciiString(npc.getName());
        mplew.write(npc.getGender());
        mplew.write(npc.getSkin());
        mplew.writeInt(npc.getFace());
        mplew.writeInt(0);
        mplew.write(0);
        mplew.writeInt(npc.getHair());
        Map<Byte, Integer> equip = npc.getEquips();
        Map<Byte, Integer> myEquip = new LinkedHashMap<>();
        Map<Byte, Integer> maskedEquip = new LinkedHashMap<>();
        for (Map.Entry<Byte, Integer> position : equip.entrySet())
        {
            byte pos = (byte) (position.getKey() * -1);
            if ((pos < 100) && (myEquip.get(pos) == null))
            {
                myEquip.put(pos, position.getValue());
            }
            else if ((pos > 100) && (pos != 111))
            {
                pos = (byte) (pos - 100);
                if (myEquip.get(pos) != null)
                {
                    maskedEquip.put(pos, myEquip.get(pos));
                }
                myEquip.put(pos, position.getValue());
            }
            else if (myEquip.get(pos) != null)
            {
                maskedEquip.put(pos, position.getValue());
            }
        }
        for (Map.Entry<Byte, Integer> entry : myEquip.entrySet())
        {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }
        mplew.write(255);
        for (Map.Entry<Byte, Integer> entry : maskedEquip.entrySet())
        {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }
        mplew.write(255);
        Integer cWeapon = equip.get((byte) -111);
        if (cWeapon != null)
        {
            mplew.writeInt(cWeapon);
        }
        else
        {
            mplew.writeInt(0);
        }
        for (int i = 0; i < 3; i++)
        {
            mplew.writeInt(npc.getPet(i));
        }

        return mplew.getPacket();
    }


    public static byte[] setNPCScriptable(List<Pair<Integer, String>> npcs)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.NPC_SCRIPTABLE.getValue());
        mplew.write(npcs.size());
        for (Pair<Integer, String> s : npcs)
        {
            mplew.writeInt(s.left);
            mplew.writeMapleAsciiString(s.right);
            mplew.writeInt(0);
            mplew.writeInt(Integer.MAX_VALUE);
        }
        return mplew.getPacket();
    }

    public static byte[] getNPCTalk(int npc, byte msgType, String talk, String endBytes, byte type)
    {
        return getNPCTalk(npc, msgType, talk, endBytes, type, npc, false);
    }

    public static byte[] getNPCTalk(int npc, byte msgType, String talk, String endBytes, byte type, int diffNpc, boolean player)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NPC_TALK.getValue());
        mplew.write(player ? 3 : 4);
        mplew.writeInt(npc);
        mplew.write(0);
        mplew.write(msgType);
        mplew.write(type);
        if ((type & 0x4) != 0)
        {
            mplew.writeInt(diffNpc);
        }
        mplew.writeMapleAsciiString(talk);
        mplew.write(HexTool.getByteArrayFromHexString(endBytes));

        return mplew.getPacket();
    }

    public static byte[] getNPCTalk(int npc, byte msgType, String talk, String endBytes, byte type, int diffNpc)
    {
        return getNPCTalk(npc, msgType, talk, endBytes, type, diffNpc, false);
    }

    public static byte[] getPlayerTalk(int npc, byte msgType, String talk, String endBytes, byte type)
    {
        return getNPCTalk(npc, msgType, talk, endBytes, type, npc, true);
    }

    public static byte[] getMapSelection(int npcid, byte msgType, String sel)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NPC_TALK.getValue());
        mplew.write(4);
        mplew.writeInt(npcid);
        mplew.write(0);
        mplew.writeShort(msgType);
        mplew.writeInt(npcid == 2083006 ? 1 : npcid == 9010000 ? 3 : npcid == 3000012 ? 5 : 0);
        mplew.writeInt(npcid == 9010022 ? 1 : 0);
        mplew.writeMapleAsciiString(sel);

        return mplew.getPacket();
    }

    public static byte[] getNPCTalkStyle(int npc, String talk, int[] styles, int card, boolean android)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NPC_TALK.getValue());
        mplew.write(4);
        mplew.writeInt(npc);
        mplew.write(0);
        mplew.writeShort(android ? 10 : 9);
        if (!android)
        {
            mplew.writeShort(0);
        }
        mplew.writeMapleAsciiString(talk);
        mplew.write(styles.length);
        for (int style : styles)
        {
            mplew.writeInt(style);
        }
        mplew.writeInt(card);

        return mplew.getPacket();
    }

    public static byte[] getNPCTalkNum(int npc, byte msgType, String talk, int def, int min, int max)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NPC_TALK.getValue());
        mplew.write(4);
        mplew.writeInt(npc);
        mplew.write(0);
        mplew.writeShort(msgType);
        mplew.writeMapleAsciiString(talk);
        mplew.writeInt(def);
        mplew.writeInt(min);
        mplew.writeInt(max);

        return mplew.getPacket();
    }

    public static byte[] getNPCTalkText(int npc, byte msgType, String talk)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NPC_TALK.getValue());
        mplew.write(4);
        mplew.writeInt(npc);
        mplew.write(0);
        mplew.writeShort(msgType);
        mplew.writeMapleAsciiString(talk);
        mplew.writeInt(0);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] getEvanTutorial(String data)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NPC_TALK.getValue());
        mplew.write(8);
        mplew.writeInt(0);
        mplew.write(1);
        mplew.write(1);
        mplew.write(1);
        mplew.writeMapleAsciiString(data);

        return mplew.getPacket();
    }


    public static byte[] getNPCShop(int shopId, MapleShop shop, MapleClient c)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_NPC_SHOP.getValue());
        mplew.write(0);
        mplew.writeInt(shop.getShopItemId());
        mplew.writeInt(shopId);
        PacketHelper.addShopInfo(mplew, shop, c);

        return mplew.getPacket();
    }


    public static byte[] confirmShopTransaction(MapleShopResponse code, MapleShop shop, MapleClient c, int indexBought)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CONFIRM_SHOP_TRANSACTION.getValue());
        mplew.write(code.getValue());
        switch (code)
        {
            case 购买道具完成:
            case 背包空间不够:
                mplew.write(indexBought >= 0 ? 1 : 0);
                if (indexBought >= 0)
                {
                    mplew.writeInt(indexBought);
                }
                else
                {
                    mplew.write(0);
                }
                break;
            case 卖出道具完成:
                mplew.writeInt(shop.getShopItemId());
                mplew.writeInt(shop.getNpcId());
                PacketHelper.addShopInfo(mplew, shop, c);
                break;
            case 充值飞镖完成:
            case 充值金币不够:
                break;
            case 购买回购出错:
                mplew.write(0);
                mplew.write(0);
                break;
            default:
                System.err.println("未知商店买卖操作: " + code);
        }


        return mplew.getPacket();
    }


    public static byte[] takeOutStorage(byte slots, MapleInventoryType type, Collection<Item> items)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_STORAGE.getValue());
        mplew.write(9);
        mplew.write(slots);
        mplew.writeLong(type.getBitfieldEncoding());
        mplew.write(items.size());
        for (Item item : items)
        {
            PacketHelper.addItemInfo(mplew, item);
        }

        return mplew.getPacket();
    }


    public static byte[] getStorageError(byte op)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_STORAGE.getValue());
        mplew.write(op);

        return mplew.getPacket();
    }


    public static byte[] storeStorage(byte slots, MapleInventoryType type, Collection<Item> items)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_STORAGE.getValue());
        mplew.write(13);
        mplew.write(slots);
        mplew.writeLong(type.getBitfieldEncoding());
        mplew.write(items.size());
        for (Item item : items)
        {
            PacketHelper.addItemInfo(mplew, item);
        }

        return mplew.getPacket();
    }


    public static byte[] arrangeStorage(byte slots, Collection<Item> items, boolean changed)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_STORAGE.getValue());
        mplew.write(15);
        mplew.write(slots);
        mplew.writeLong(124L);


        mplew.write(items.size());
        for (Item item : items)
        {
            PacketHelper.addItemInfo(mplew, item);
        }
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] mesoStorage(byte slots, int meso)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_STORAGE.getValue());
        mplew.write(19);
        mplew.write(slots);
        mplew.writeLong(2L);
        mplew.writeLong(meso);

        return mplew.getPacket();
    }


    public static byte[] getStorage(int npcId, byte slots, Collection<Item> items, int meso)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_STORAGE.getValue());
        mplew.write(22);
        mplew.writeInt(npcId);
        mplew.write(slots);
        mplew.writeLong(126L);
        mplew.writeLong(meso);


        mplew.write(items.size());
        for (Item item : items)
        {
            PacketHelper.addItemInfo(mplew, item);
        }
        mplew.writeInt(0);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\NPCPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */