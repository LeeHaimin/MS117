package tools.packet;

import org.apache.log4j.Logger;

import java.util.Collection;

import client.BuddylistEntry;
import handling.SendPacketOpcode;
import server.ServerProperties;
import tools.data.output.MaplePacketLittleEndianWriter;


public class BuddyListPacket
{
    private static final Logger log = Logger.getLogger(BuddyListPacket.class);


    public static byte[] buddylistMessage(int message)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BUDDYLIST.getValue());
        mplew.write(message);

        return mplew.getPacket();
    }

    public static byte[] updateBuddylist(Collection<BuddylistEntry> buddylist)
    {
        return updateBuddylist(buddylist, 7);
    }


    public static byte[] updateBuddylist(Collection<BuddylistEntry> buddylist, int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BUDDYLIST.getValue());
        mplew.write(mode);
        mplew.write(buddylist.size());
        for (BuddylistEntry buddy : buddylist)
        {
            mplew.writeInt(buddy.getCharacterId());
            mplew.writeAsciiString(buddy.getName(), 13);
            mplew.write(buddy.isVisible() ? 0 : 1);
            mplew.writeInt(buddy.getChannel() == -1 ? -1 : buddy.getChannel() - 1);
            mplew.writeAsciiString(buddy.getGroup(), 18);
        }
        for (int x = 0; x < buddylist.size(); x++)
        {
            mplew.writeInt(0);
        }
        return mplew.getPacket();
    }


    public static byte[] requestBuddylistAdd(int chrIdFrom, String nameFrom, int levelFrom, int jobFrom)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BUDDYLIST.getValue());
        mplew.write(9);
        mplew.writeInt(chrIdFrom);
        mplew.writeMapleAsciiString(nameFrom);
        mplew.writeInt(levelFrom);
        mplew.writeInt(jobFrom);
        mplew.writeInt(0);
        mplew.writeInt(chrIdFrom);
        mplew.writeAsciiString(nameFrom, 13);
        mplew.write(1);
        mplew.writeInt(0);
        mplew.writeAsciiString("未指定群组", 18);
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] updateBuddyChannel(int chrId, int channel)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BUDDYLIST.getValue());
        mplew.write(20);
        mplew.writeInt(chrId);
        mplew.write(0);
        mplew.writeInt(channel);

        return mplew.getPacket();
    }


    public static byte[] updateBuddyCapacity(int capacity)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BUDDYLIST.getValue());
        mplew.write(22);
        mplew.write(capacity);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\BuddyListPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */