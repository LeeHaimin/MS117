package tools.packet;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import client.MapleCharacter;
import handling.SendPacketOpcode;
import handling.world.PartyOperation;
import handling.world.WrodlPartyService;
import handling.world.party.MapleExpedition;
import handling.world.party.MapleParty;
import handling.world.party.MaplePartyCharacter;
import handling.world.party.PartySearch;
import handling.world.party.PartySearchType;
import handling.world.sidekick.MapleSidekick;
import handling.world.sidekick.MapleSidekickCharacter;
import server.ServerProperties;
import tools.data.output.MaplePacketLittleEndianWriter;


public class PartyPacket
{
    private static final Logger log = Logger.getLogger(PartyPacket.class);


    public static byte[] partyCreated(int partyid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(14);
        mplew.writeInt(partyid);
        mplew.writeInt(999999999);
        mplew.writeInt(999999999);
        mplew.writeLong(0L);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] partyInvite(MapleCharacter from)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(4);
        mplew.writeInt(from.getParty() == null ? 0 : from.getParty().getId());
        mplew.writeMapleAsciiString(from.getName());
        mplew.writeInt(from.getLevel());
        mplew.writeInt(from.getJob());
        mplew.writeInt(0);
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] partyRequestInvite(MapleCharacter from)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(8);
        mplew.writeInt(from.getId());
        mplew.writeMapleAsciiString(from.getName());
        mplew.writeInt(from.getLevel());
        mplew.writeInt(from.getJob());
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] partyStatusMessage(int message)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(message);

        return mplew.getPacket();
    }


    public static byte[] partyStatusMessage(int message, String charName)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());


        mplew.write(message);
        mplew.writeMapleAsciiString(charName);

        return mplew.getPacket();
    }


    public static byte[] partyStatusMessage(String message)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPOUSE_MESSAGE.getValue());
        mplew.writeShort(11);
        mplew.writeMapleAsciiString(message);

        return mplew.getPacket();
    }

    public static byte[] updateParty(int forChannel, MapleParty party, PartyOperation op, MaplePartyCharacter target)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        switch (op)
        {
            case 解散队伍:
            case 驱逐成员:
            case 离开队伍:
                mplew.write(19);
                mplew.writeInt(party.getId());
                mplew.writeInt(target.getId());
                mplew.write(op == PartyOperation.解散队伍 ? 0 : 1);
                if (op == PartyOperation.解散队伍)
                {
                    mplew.writeInt(target.getId());
                }
                else
                {
                    mplew.write(op == PartyOperation.驱逐成员 ? 1 : 0);
                    mplew.writeMapleAsciiString(target.getName());
                    addPartyStatus(forChannel, party, mplew, op == PartyOperation.离开队伍);
                }
                break;
            case 加入队伍:
                mplew.write(22);
                mplew.writeInt(party.getId());
                mplew.writeMapleAsciiString(target.getName());
                addPartyStatus(forChannel, party, mplew, false);
                break;
            case 更新队伍:
            case LOG_ONOFF:
                mplew.write(op == PartyOperation.LOG_ONOFF ? 55 : 13);
                mplew.writeInt(party.getId());
                addPartyStatus(forChannel, party, mplew, op == PartyOperation.LOG_ONOFF);
                break;
            case 改变队长:
            case CHANGE_LEADER_DC:
                mplew.write(46);
                mplew.writeInt(target.getId());
                mplew.write(op == PartyOperation.CHANGE_LEADER_DC ? 1 : 0);
        }

        return mplew.getPacket();
    }

    private static void addPartyStatus(int forchannel, MapleParty party, MaplePacketLittleEndianWriter lew, boolean leaving)
    {
        addPartyStatus(forchannel, party, lew, leaving, false);
    }

    private static void addPartyStatus(int forchannel, MapleParty party, MaplePacketLittleEndianWriter lew, boolean leaving, boolean exped)
    {
        List<MaplePartyCharacter> partymembers;
        if (party == null)
        {
            partymembers = new ArrayList<>();
        }
        else
        {
            partymembers = new ArrayList(party.getMembers());
        }
        while (partymembers.size() < 6)
        {
            partymembers.add(new MaplePartyCharacter());
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            lew.writeInt(partychar.getId());
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            lew.writeAsciiString(partychar.getName(), 13);
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            lew.writeInt(partychar.getJobId());
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            lew.writeInt(0);
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            lew.writeInt(partychar.getLevel());
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            if (partychar.isOnline())
            {
                lew.writeInt(partychar.getChannel() - 1);
            }
            else
            {
                lew.writeInt(-2);
            }
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            lew.writeInt(0);
        }
        lew.writeInt(party == null ? 0 : party.getLeader().getId());
        if (exped)
        {
            return;
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            if (partychar.getChannel() == forchannel)
            {
                lew.writeInt(partychar.getMapid());
            }
            else
            {
                lew.writeInt(0);
            }
        }
        for (MaplePartyCharacter partychar : partymembers)
        {
            if ((partychar.getChannel() == forchannel) && (!leaving))
            {
                lew.writeInt(partychar.getDoorTown());
                lew.writeInt(partychar.getDoorTarget());
                lew.writeInt(partychar.getDoorSkill());
                lew.writeInt(partychar.getDoorPosition().x);
                lew.writeInt(partychar.getDoorPosition().y);
            }
            else
            {
                lew.writeInt(leaving ? 999999999 : 0);
                lew.writeInt(leaving ? 999999999 : 0);
                lew.writeInt(0);
                lew.writeInt(leaving ? -1 : 0);
                lew.writeInt(leaving ? -1 : 0);
            }
        }
        lew.write(1);
    }

    public static byte[] partyPortal(int townId, int targetId, int skillId, Point position, boolean animation)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(72);
        mplew.write(animation ? 0 : 1);
        mplew.writeInt(townId);
        mplew.writeInt(targetId);
        mplew.writeInt(skillId);
        mplew.writePos(position);

        return mplew.getPacket();
    }


    public static byte[] updatePartyMemberHP(int chrId, int curhp, int maxhp)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_PARTYMEMBER_HP.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(curhp);
        mplew.writeInt(maxhp);

        return mplew.getPacket();
    }


    public static byte[] getPartyListing(PartySearchType pst)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(108);
        mplew.writeInt(pst.id);
        List<PartySearch> parties = WrodlPartyService.getInstance().searchParty(pst);
        mplew.writeInt(parties.size());
        for (PartySearch party : parties)
        {
            if (pst.exped)
            {
                MapleExpedition me = WrodlPartyService.getInstance().getExped(party.getId());
                mplew.writeInt(party.getId());
                mplew.writeAsciiString(party.getName(), 37);

                mplew.writeInt(pst.id);
                mplew.writeInt(0);
                for (int i = 0; i < 5; i++)
                {
                    if (i < me.getParties().size())
                    {
                        MapleParty part = WrodlPartyService.getInstance().getParty(me.getParties().get(i));
                        if (part != null)
                        {
                            addPartyStatus(-1, part, mplew, false, true);
                        }
                        else
                        {
                            mplew.writeZeroBytes(226);
                        }
                    }
                    else
                    {
                        mplew.writeZeroBytes(226);
                    }
                }
            }
            else
            {
                mplew.writeInt(party.getId());
                mplew.writeAsciiString(party.getName(), 37);
                addPartyStatus(-1, WrodlPartyService.getInstance().getParty(party.getId()), mplew, false, true);
            }
        }

        return mplew.getPacket();
    }


    public static byte[] partyListingAdded(PartySearch ps)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(106);
        mplew.writeInt(ps.getType().id);
        if (ps.getType().exped)
        {
            MapleExpedition me = WrodlPartyService.getInstance().getExped(ps.getId());

            mplew.writeInt(ps.getId());
            mplew.writeAsciiString(ps.getName(), 37);
            mplew.writeInt(ps.getType().id);
            mplew.writeInt(0);
            for (int i = 0; i < 5; i++)
            {
                if (i < me.getParties().size())
                {
                    MapleParty party = WrodlPartyService.getInstance().getParty(me.getParties().get(i));
                    if (party != null)
                    {
                        addPartyStatus(-1, party, mplew, false, true);
                    }
                    else
                    {
                        mplew.writeZeroBytes(226);
                    }
                }
                else
                {
                    mplew.writeZeroBytes(226);
                }
            }
        }
        else
        {
            mplew.writeInt(ps.getId());
            mplew.writeAsciiString(ps.getName(), 37);
            addPartyStatus(-1, WrodlPartyService.getInstance().getParty(ps.getId()), mplew, false, true);
        }

        return mplew.getPacket();
    }


    public static byte[] removePartySearch(PartySearch ps)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.PARTY_OPERATION.getValue());
        mplew.write(107);
        mplew.writeInt(ps.getType().id);
        mplew.writeInt(ps.getId());
        mplew.writeInt(2);

        return mplew.getPacket();
    }


    public static byte[] expeditionStatus(MapleExpedition me, boolean created)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());
        mplew.write(created ? 86 : 88);
        mplew.writeInt(me.getType().exped);
        mplew.writeInt(0);
        for (int i = 0; i < 5; i++)
        {
            if (i < me.getParties().size())
            {
                MapleParty party = WrodlPartyService.getInstance().getParty(me.getParties().get(i));
                if (party != null)
                {
                    addPartyStatus(-1, party, mplew, false, true);
                }
                else
                {
                    mplew.writeZeroBytes(226);
                }
            }
            else
            {
                mplew.writeZeroBytes(226);
            }
        }

        return mplew.getPacket();
    }


    public static byte[] expeditionInviteMessage(int code, String name)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());
        mplew.write(100);


        mplew.writeInt(code);
        mplew.writeMapleAsciiString(name);

        return mplew.getPacket();
    }

    public static byte[] expeditionJoined(String name)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());
        mplew.write(87);
        mplew.writeMapleAsciiString(name);

        return mplew.getPacket();
    }

    public static byte[] expeditionLeft(boolean left, String name)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());


        mplew.write(left ? 91 : 93);
        mplew.writeMapleAsciiString(name);

        return mplew.getPacket();
    }

    public static byte[] expeditionMessage(boolean disbanded)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());


        mplew.write(disbanded ? 95 : 94);

        return mplew.getPacket();
    }


    public static byte[] expeditionLeaderChanged(int newLeader)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());
        mplew.write(96);
        mplew.writeInt(newLeader);

        return mplew.getPacket();
    }


    public static byte[] expeditionUpdate(int partyIndex, MapleParty party)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());
        mplew.write(97);
        mplew.writeInt(0);
        mplew.writeInt(partyIndex);
        if (party == null)
        {
            mplew.writeZeroBytes(226);
        }
        else
        {
            addPartyStatus(-1, party, mplew, false, true);
        }

        return mplew.getPacket();
    }


    public static byte[] expeditionInvite(MapleCharacter from, int exped)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.EXPEDITION_OPERATION.getValue());
        mplew.write(99);
        mplew.writeInt(from.getLevel());
        mplew.writeInt(from.getJob());
        mplew.writeInt(0);
        mplew.writeMapleAsciiString(from.getName());
        mplew.writeInt(exped);

        return mplew.getPacket();
    }


    public static byte[] showMemberSearch(List<MapleCharacter> players)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MEMBER_SEARCH.getValue());
        mplew.write(players.size());
        for (MapleCharacter chr : players)
        {
            mplew.writeInt(chr.getId());
            mplew.writeMapleAsciiString(chr.getName());
            mplew.writeInt(chr.getJob());
            mplew.write(chr.getLevel());
        }

        return mplew.getPacket();
    }


    public static byte[] showPartySearch(List<MapleParty> partylist)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.PARTY_SEARCH.getValue());
        mplew.write(partylist.size());
        for (MapleParty party : partylist)
        {
            mplew.writeInt(party.getId());
            mplew.writeMapleAsciiString(party.getLeader().getName());
            mplew.write(party.getLeader().getLevel());
            mplew.write(party.getLeader().isOnline() ? 1 : 0);
            mplew.write(party.getMembers().size());
            for (MaplePartyCharacter partyChr : party.getMembers())
            {
                mplew.writeInt(partyChr.getId());
                mplew.writeMapleAsciiString(partyChr.getName());
                mplew.writeInt(partyChr.getJobId());
                mplew.write(partyChr.getLevel());
                mplew.write(partyChr.isOnline() ? 1 : 0);
            }
        }

        return mplew.getPacket();
    }

    public static byte[] sidekickInvite(MapleCharacter from)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SIDEKICK_OPERATION.getValue());
        mplew.write(65);
        mplew.writeInt(from.getId());
        mplew.writeMapleAsciiString(from.getName());
        mplew.writeInt(from.getLevel());
        mplew.writeInt(from.getJob());
        mplew.writeInt(0);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] disbandSidekick(MapleSidekick s)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SIDEKICK_OPERATION.getValue());
        mplew.write(75);
        mplew.writeInt(s.getId());
        mplew.writeInt(s.getCharacter(0).getId());
        mplew.write(0);
        mplew.writeInt(s.getCharacter(1).getId());

        return mplew.getPacket();
    }

    public static byte[] updateSidekick(MapleCharacter first, MapleSidekick s, boolean f)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SIDEKICK_OPERATION.getValue());
        mplew.write(f ? 78 : 70);
        MapleSidekickCharacter second = s.getCharacter(s.getCharacter(0).getId() == first.getId() ? 1 : 0);
        boolean online = first.getMap().getCharacterById(second.getId()) != null;
        mplew.writeInt(s.getId());
        if (f)
        {
            mplew.writeMapleAsciiString(second.getName());
        }
        List<String> msg = s.getSidekickMsg(online);
        mplew.writeInt(msg.size());
        for (String m : msg)
        {
            mplew.writeMapleAsciiString(m);
        }
        mplew.writeInt(first.getId());
        mplew.writeInt(second.getId());
        mplew.writeAsciiString(first.getName(), 13);
        mplew.writeAsciiString(second.getName(), 13);
        mplew.writeInt(first.getJob());
        mplew.writeInt(second.getJobId());
        mplew.writeInt(first.getLevel());
        mplew.writeInt(second.getLevel());
        mplew.writeInt(first.getClient().getChannel() - 1);
        mplew.writeInt(online ? first.getClient().getChannel() - 1 : 0);
        mplew.writeLong(0L);
        mplew.writeInt(first.getId());
        if (f)
        {
            mplew.writeInt(first.getId());
        }
        mplew.writeInt(second.getId());
        if (!f)
        {
            mplew.writeInt(first.getId());
        }
        mplew.writeInt(first.getMapId());
        mplew.writeInt(online ? first.getMapId() : 999999999);
        mplew.writeInt(1);
        mplew.write(Math.abs(first.getLevel() - second.getLevel()));
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(Integer.MAX_VALUE);
        mplew.writeInt(1);


        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\PartyPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */