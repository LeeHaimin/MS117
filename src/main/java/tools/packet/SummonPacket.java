package tools.packet;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.util.List;

import client.MapleCharacter;
import handling.SendPacketOpcode;
import server.ServerProperties;
import server.maps.MapleSummon;
import server.movement.LifeMovementFragment;
import tools.AttackPair;
import tools.Pair;
import tools.data.output.MaplePacketLittleEndianWriter;


public class SummonPacket
{
    private static final Logger log = Logger.getLogger(SummonPacket.class);

    public static byte[] spawnSummon(MapleSummon summon, boolean animated)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_SUMMON.getValue());
        mplew.writeInt(summon.getOwnerId());
        mplew.writeInt(summon.getObjectId());
        mplew.writeInt(summon.getSkillId());
        mplew.write(summon.getOwnerLevel());
        mplew.write(summon.getSkillLevel());
        mplew.writePos(summon.getPosition());
        mplew.write(summon.is战法重生() ? 5 : 4);
        mplew.writeShort(0);
        mplew.write(summon.getMovementType().getValue());
        mplew.write(summon.getAttackType());
        mplew.write(animated ? 1 : 0);
        mplew.write(1);
        if (summon.is机械磁场())
        {
            mplew.write(0);
        }
        MapleCharacter chr = summon.getOwner();
        mplew.write((summon.is傀儡召唤()) && (chr != null) ? 1 : 0);
        if ((summon.is傀儡召唤()) && (chr != null))
        {
            PacketHelper.addCharLook(mplew, chr, true, false);
        }

        return mplew.getPacket();
    }

    public static byte[] removeSummon(int ownerId, int objId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_SUMMON.getValue());
        mplew.writeInt(ownerId);
        mplew.writeInt(objId);
        mplew.write(12);

        return mplew.getPacket();
    }

    public static byte[] removeSummon(MapleSummon summon, boolean animated)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_SUMMON.getValue());
        mplew.writeInt(summon.getOwnerId());
        mplew.writeInt(summon.getObjectId());
        mplew.write(animated ? 12 : summon.getRemoveStatus());

        return mplew.getPacket();
    }

    public static byte[] moveSummon(int chrId, int oid, Point startPos, List<LifeMovementFragment> moves)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_SUMMON.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(oid);
        mplew.writeInt(0);
        mplew.writePos(startPos);
        mplew.writeInt(0);
        PacketHelper.serializeMovementList(mplew, moves);

        return mplew.getPacket();
    }

    public static byte[] summonAttack(int chrId, int summonSkillId, byte animation, byte numAttackedAndDamage, List<AttackPair> allDamage, int level, boolean darkFlare)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SUMMON_ATTACK.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(summonSkillId);
        mplew.write(level);
        mplew.write(animation);
        mplew.write(numAttackedAndDamage);

        for (AttackPair attackEntry : allDamage)
        {
            if (attackEntry.attack != null)
            {
                mplew.writeInt(attackEntry.objectid);
                mplew.write(7);
                for (Pair<Integer, Boolean> eachd : attackEntry.attack)
                {
                    if (eachd.right)
                    {
                        mplew.writeInt(eachd.left + Integer.MIN_VALUE);
                    }
                    else
                    {
                        mplew.writeInt(eachd.left);
                    }
                }
            }
        }
        mplew.write(darkFlare ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] pvpSummonAttack(int cid, int playerLevel, int oid, int animation, Point pos, List<AttackPair> attack)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_SUMMON.getValue());
        mplew.writeInt(cid);
        mplew.writeInt(oid);
        mplew.write(playerLevel);
        mplew.write(animation);
        mplew.writePos(pos);
        mplew.writeInt(0);
        mplew.write(attack.size());
        for (AttackPair p : attack)
        {
            mplew.writeInt(p.objectid);
            mplew.writePos(p.point);
            mplew.writeShort(p.attack.size());
            for (Pair<Integer, Boolean> atk : p.attack)
            {
                mplew.writeInt(atk.left);
            }
        }

        return mplew.getPacket();
    }

    public static byte[] summonSkill(int chrId, int summonSkillId, int newStance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SUMMON_SKILL.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(summonSkillId);
        mplew.write(newStance);

        return mplew.getPacket();
    }

    public static byte[] damageSummon(int chrId, int summonSkillId, int damage, int unkByte, int monsterIdFrom)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_SUMMON.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(summonSkillId);
        mplew.write(unkByte);
        mplew.writeInt(damage);
        mplew.writeInt(monsterIdFrom);
        mplew.write(0);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\SummonPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */