package tools.packet;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleDisease;
import client.SpecialBuffInfo;
import constants.GameConstants;
import handling.SendPacketOpcode;
import server.MapleStatEffect;
import server.ServerProperties;
import tools.HexTool;
import tools.Pair;
import tools.data.output.MaplePacketLittleEndianWriter;


public class BuffPacket
{
    private static final Logger log = Logger.getLogger(BuffPacket.class);


    public static byte[] giveDice(int buffid, int skillid, int duration, List<Pair<MapleBuffStat, Integer>> statups)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeBuffMask(mplew, statups);

        int dice = buffid >= 100 ? buffid / 100 : buffid;
        mplew.writeShort(dice);

        mplew.writeInt(skillid);
        mplew.writeInt(duration);
        mplew.writeZeroBytes(5);

        mplew.writeInt(GameConstants.getDiceStat(dice, 3));
        mplew.writeInt(GameConstants.getDiceStat(dice, 3));
        mplew.writeInt(GameConstants.getDiceStat(dice, 4));
        mplew.writeZeroBytes(20);
        mplew.writeInt(GameConstants.getDiceStat(dice, 2));
        mplew.writeZeroBytes(12);
        mplew.writeInt(GameConstants.getDiceStat(dice, 5));
        mplew.writeZeroBytes(16);
        mplew.writeInt(GameConstants.getDiceStat(dice, 6));
        mplew.writeZeroBytes(16);
        mplew.writeZeroBytes(4);

        mplew.writeInt(1000);
        mplew.write(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] showMonsterRiding(int chrId, List<Pair<MapleBuffStat, Integer>> statups, int itemId, int skillId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeBuffMask(mplew, statups);
        mplew.writeZeroBytes(16);
        mplew.writeZeroBytes(7);
        mplew.writeInt(itemId);
        mplew.writeInt(skillId);
        mplew.writeZeroBytes(7);

        return mplew.getPacket();
    }


    public static byte[] givePirateBuff(List<Pair<MapleBuffStat, Integer>> statups, int duration, int skillid)
    {
        boolean infusion = (skillid == 5121009) || (skillid == 15121005) || (skillid % 10000 == 8006);
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeBuffMask(mplew, statups);
        mplew.writeZeroBytes(9);
        for (Pair<MapleBuffStat, Integer> stat : statups)
        {
            mplew.writeInt(stat.getRight());
            mplew.writeLong(skillid);
            mplew.writeZeroBytes(infusion ? 6 : 1);
            mplew.writeShort(duration);
        }
        mplew.writeInt(infusion ? 600 : 0);
        mplew.write(1);
        if (!infusion)
        {
            mplew.write(4);
        }
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] giveForeignDash(List<Pair<MapleBuffStat, Integer>> statups, int duration, int chrId, int skillid)
    {
        boolean infusion = (skillid == 5121009) || (skillid == 15121005) || (skillid % 10000 == 8006);
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeBuffMask(mplew, statups);
        if (!infusion)
        {
            mplew.writeZeroBytes(16);
        }
        mplew.writeZeroBytes(7);
        for (Pair<MapleBuffStat, Integer> stat : statups)
        {
            mplew.writeInt(stat.getRight());
            mplew.writeLong(skillid);
            mplew.writeZeroBytes(infusion ? 6 : 1);
            mplew.writeShort(duration);
        }
        mplew.writeShort(0);

        return mplew.getPacket();
    }


    public static byte[] give导航辅助(int skillid, int mobid, int x)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.导航辅助);
        mplew.writeZeroBytes(9);
        mplew.writeInt(x);
        mplew.writeInt(skillid);
        mplew.writeZeroBytes(5);
        mplew.writeInt(mobid);
        mplew.writeInt(0);
        mplew.writeInt(720);
        mplew.writeZeroBytes(5);

        return mplew.getPacket();
    }


    public static byte[] give神秘瞄准术(int x, int skillId, int duration)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.神秘瞄准术);
        mplew.writeShort(x);
        mplew.writeInt(skillId);
        mplew.writeInt(duration);
        mplew.writeZeroBytes(18);

        return mplew.getPacket();
    }


    public static byte[] giveEnergyCharge(int bar, int buffId, boolean fullbar, boolean consume)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.能量获得);
        mplew.writeZeroBytes(5);
        mplew.writeInt((fullbar) || ((consume) && (bar > 0)) ? buffId : 0);
        mplew.writeInt(Math.min(bar, 10000));
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeZeroBytes(6);
        mplew.write(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] showEnergyCharge(int chrId, int bar, int buffId, boolean fullbar, boolean consume)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.能量获得);
        mplew.writeZeroBytes(19);
        mplew.writeInt((fullbar) || ((consume) && (bar > 0)) ? buffId : 0);
        mplew.writeInt(Math.min(bar, 10000));
        mplew.writeZeroBytes(11);

        return mplew.getPacket();
    }


    public static byte[] updateLuminousGauge(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LUMINOUS_COMBO.getValue());
        mplew.writeInt(chr.getDarkTotal());
        mplew.writeInt(chr.getLightTotal());
        mplew.writeInt(chr.getDarkType());
        mplew.writeInt(chr.getLightType());
        mplew.write(new byte[]{79, 23, -106, -113});

        return mplew.getPacket();
    }


    public static byte[] giveLuminousState(int buffid, int bufflength, MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.光暗转换);
        mplew.writeShort(1);
        mplew.writeInt(buffid);
        mplew.writeInt(bufflength);
        mplew.writeZeroBytes(5);
        mplew.writeInt(buffid);
        mplew.write(new byte[]{79, -39, -127, -101});
        mplew.writeZeroBytes(8);
        mplew.writeInt(chr.getDarkTotal());
        mplew.writeInt(chr.getLightTotal());
        mplew.writeInt(chr.getDarkType());
        mplew.writeInt(chr.getLightType());
        mplew.write(new byte[]{79, 23, -106, -113});
        mplew.writeZeroBytes(8);
        mplew.write(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] giveDarkCrescendo(int buffid, int bufflength, int count)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.黑暗高潮);
        mplew.writeShort(count);
        mplew.writeInt(buffid);
        mplew.writeInt(bufflength);
        mplew.writeZeroBytes(9);
        mplew.write(count);
        mplew.writeInt(0);
        mplew.write(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] startPower(boolean start)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.尖兵能量);
        mplew.writeInt(start ? 1 : 0);
        mplew.writeInt(7200);
        mplew.writeInt(6);
        mplew.writeInt(7200);
        mplew.writeZeroBytes(18);

        return mplew.getPacket();
    }


    public static byte[] updatePowerCount(int skillId, int count)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.尖兵电力);
        mplew.writeShort(count);
        mplew.writeInt(skillId);
        mplew.writeInt(2100000000);
        mplew.writeZeroBytes(18);

        return mplew.getPacket();
    }


    public static byte[] give卡牌审判(int buffid, int bufflength, List<Pair<MapleBuffStat, Integer>> statups, int theStat)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeBuffMask(mplew, statups);
        for (Pair<MapleBuffStat, Integer> stat : statups)
        {
            mplew.writeShort(stat.getRight());
            mplew.writeInt(buffid);
            mplew.writeInt(bufflength);
        }
        mplew.writeZeroBytes(5);
        mplew.writeInt(theStat);
        mplew.writeZeroBytes(8);
        mplew.write(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] give狂龙变形值(int bar)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.变形值);

        mplew.writeInt(Math.min(bar, 700));
        mplew.writeShort(0);
        mplew.write(HexTool.getByteArrayFromHexString("78 90 2A EC"));
        mplew.writeZeroBytes(9);

        mplew.writeInt(bar >= 100 ? 1 : bar >= 300 ? 2 : bar >= 700 ? 3 : 0);
        mplew.writeZeroBytes(13);

        return mplew.getPacket();
    }

    public static byte[] show狂龙变形值(int chrId, int bar)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.变形值);
        mplew.writeInt(Math.min(bar, 1000));
        mplew.writeZeroBytes(23);

        return mplew.getPacket();
    }


    public static byte[] give剑刃之壁(int buffid, int bufflength, List<Pair<MapleBuffStat, Integer>> statups, int ItemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeBuffMask(mplew, statups);
        for (Pair<MapleBuffStat, Integer> stat : statups)
        {
            mplew.writeShort(stat.getRight());
            mplew.writeInt(buffid);
            mplew.writeInt(bufflength);
        }
        mplew.writeZeroBytes(5);
        mplew.writeInt(buffid == 61101002 ? 1 : 2);
        mplew.writeInt(buffid == 61101002 ? 3 : 5);
        mplew.writeInt(ItemId);
        mplew.writeInt(buffid == 61101002 ? 3 : 5);
        mplew.writeZeroBytes(buffid == 61101002 ? 16 : 24);
        mplew.writeInt(0);
        mplew.write(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] show剑刃之壁(int chrId, int buffid, List<Pair<MapleBuffStat, Integer>> statups, int ItemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeBuffMask(mplew, statups);
        for (Pair<MapleBuffStat, Integer> stat : statups)
        {
            mplew.writeShort(stat.getRight());
            mplew.writeInt(buffid);
        }
        mplew.writeZeroBytes(3);
        mplew.writeInt(buffid == 61101002 ? 1 : 2);
        mplew.writeInt(buffid == 61101002 ? 3 : 5);
        mplew.writeInt(ItemId);
        mplew.writeInt(buffid == 61101002 ? 3 : 5);
        mplew.writeZeroBytes(buffid == 61101002 ? 22 : 26);

        return mplew.getPacket();
    }


    public static byte[] give血之契约(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.生命潮汐);
        mplew.write(3);
        mplew.writeZeroBytes(5);
        mplew.writeLong(2100000000L);
        mplew.write(0);
        mplew.writeLong(chr.getStat().getMaxHp());
        mplew.writeZeroBytes(9);

        return mplew.getPacket();
    }


    public static byte[] give恶魔复仇者超越(int bar, int skillId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, MapleBuffStat.恶魔超越);
        mplew.writeShort(bar);
        mplew.writeInt(skillId);
        mplew.writeLong(2100000000L);
        mplew.writeZeroBytes(14);

        return mplew.getPacket();
    }

    public static byte[] giveBuff(int buffid, int bufflength, List<Pair<MapleBuffStat, Integer>> statups, MapleStatEffect effect, MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeBuffMask(mplew, statups);
        boolean stacked = false;
        boolean special = false;
        boolean isMountBuff = false;
        boolean isUnknown = (buffid == 36121003) || (buffid == 32110000);
        boolean isZeroUnknown = (buffid == 100001263) || (buffid == 100001264) || (buffid == 2311012);
        boolean isWriteIntSkill = (buffid == 23101003) || (buffid == 65101002) || (buffid == 5220019);
        int count = 0;
        List<MapleBuffStat> buffStat = new ArrayList<>();
        for (Pair<MapleBuffStat, Integer> stat : statups)
        {
            if (stat.getLeft() == MapleBuffStat.骑兽技能)
            {
                isMountBuff = true;
            }
            else if ((stat.getLeft() == MapleBuffStat.黑暗灵气) || (stat.getLeft() == MapleBuffStat.蓝色灵气) || (stat.getLeft() == MapleBuffStat.黄色灵气) || (stat.getLeft() == MapleBuffStat.月光转换))
            {
                isUnknown = true;
            }
            else if ((stat.getLeft() == MapleBuffStat.暴击概率) && (buffid == 11101022))
            {
                mplew.writeShort(stat.getRight());
                mplew.writeInt(buffid);
                mplew.writeInt(bufflength);
            }
            else if ((stat.getLeft() == MapleBuffStat.百分比无视防御) && (buffid == 15001022))
            {
                count = stat.getRight() / 5;
            }
            else if ((stat.getLeft() == MapleBuffStat.黑暗高潮) && (buffid == 27121005))
            {
                count = stat.getRight();
            }
            buffStat.add(stat.getLeft());
        }
        for (Pair<MapleBuffStat, Integer> stat : statups)
        {
            if ((stat.getLeft() == MapleBuffStat.击杀点数) && (buffid == 4221013))
            {
                count = stat.getRight();
                break;
            }
            if (stat.getLeft().canStack())
            {
                if (!stacked)
                {
                    if (special)
                    {
                        mplew.writeInt(0);
                    }
                    if (isZeroUnknown)
                    {
                        mplew.writeZeroBytes(5);
                        mplew.write(1);
                        mplew.writeZeroBytes(4);
                    }
                    else
                    {
                        mplew.writeZeroBytes(9);
                    }
                    if (isUnknown)
                    {
                        mplew.write(1);
                    }
                    stacked = true;
                }
                if (stat.getLeft() == MapleBuffStat.骑兽技能)
                {
                    int mountId = stat.getRight();
                    mplew.writeInt(mountId);
                    mplew.writeInt(buffid);
                    mplew.write(mountId == 1932016 ? 1 : 0);
                    mplew.writeInt(mountId == 1932016 ? 1 : 0);
                }
                else
                {
                    List<SpecialBuffInfo> buffs = chr.getSpecialBuffInfo(stat.getLeft(), buffid, stat.getRight(), bufflength);
                    if ((stat.getLeft() != MapleBuffStat.暴击概率) || (buffid != 11101022) || (buffs.size() != 1))
                    {

                        mplew.writeInt(buffs.size());
                        for (SpecialBuffInfo info : buffs)
                        {
                            mplew.writeInt(info.buffid);
                            mplew.writeLong(info.value);
                            mplew.writeInt(0);
                            mplew.writeInt(isMountBuff ? 0 : info.bufflength);
                            if (ServerProperties.ShowPacket())
                            {
                                log.info("技能ID: " + info.buffid + " LongStat: " + info.value + " 持续时间: " + info.bufflength + " 转换: " + info.bufflength / 1000 + "秒");
                            }
                        }
                    }
                }
            }
            else
            {
                if ((isMountBuff) || (isWriteIntSkill) || (stat.getLeft() == MapleBuffStat.影分身))
                {
                    mplew.writeInt(stat.getRight());
                }
                else if (buffid == 27110007)
                {
                    mplew.writeShort(2);
                }
                else if (buffid == 65121004)
                {
                    mplew.writeShort(stat.getRight() / 2);
                    mplew.writeShort(stat.getRight());
                }
                else if (buffid == 3221054)
                {
                    mplew.write(stat.getRight().byteValue());
                    mplew.write(stat.getRight().byteValue());
                }
                else
                {
                    mplew.writeShort(stat.getRight());
                }
                mplew.writeInt(buffid);
                mplew.writeInt(bufflength);
                if (stat.getLeft().isSpecial())
                {
                    special = true;
                }
                if (ServerProperties.ShowPacket())
                {
                    log.info("技能ID: " + buffid + " ShortStat: " + stat.getRight() + " 持续时间: " + bufflength + " 转换: " + bufflength / 1000 + "秒");
                }
            }
        }
        if (!stacked)
        {
            if (special)
            {
                mplew.writeInt(0);
            }
            mplew.writeZeroBytes(9);
            if (isUnknown)
            {
                mplew.write(1);
            }
        }

        if (buffStat.contains(MapleBuffStat.幻灵霸体))
        {
            mplew.writeInt(0);
        }
        switch (buffid)
        {
            case 1301013:
            case 3101009:
                mplew.writeInt(buffid);
                mplew.writeInt(0);
                break;
            case 4221013:
                mplew.write(Math.min(count, 5));
                break;
            case 15001022:
                if (buffStat.contains(MapleBuffStat.百分比无视防御))
                {
                    mplew.writeInt(Math.min(count, 5));
                }
                break;
            case 24121004:
            case 27121006:
            case 36121004:
                mplew.writeInt(0);
                break;
            case 2311012:
            case 27111004:
                mplew.write(0);
                break;
            case 27110007:
                mplew.writeInt(effect.getProb());
                break;
            case 27121005:
                mplew.write(count);
                break;
            case 36111003:
                mplew.write(10);
                break;
            case 30021237:
                mplew.writeZeroBytes(5);
        }

        mplew.writeInt(0);
        mplew.write(1);


        if ((isMountBuff) || (buffStat.contains(MapleBuffStat.变身术)) || (buffStat.contains(MapleBuffStat.隐身术)) || (buffStat.contains(MapleBuffStat.移动速度)) || (buffStat.contains(MapleBuffStat.跳跃力)) || (buffStat.contains(MapleBuffStat.indieJump)) || (buffStat.contains(MapleBuffStat.indieSpeed)) || (buffStat.contains(MapleBuffStat.冒险岛勇士)) || (buffStat.contains(MapleBuffStat.金属机甲)) || (buffStat.contains(MapleBuffStat.黄色灵气)) || (buffStat.contains(MapleBuffStat.变形值)) || (buffStat.contains(MapleBuffStat.能量获得)) || (buffStat.contains(MapleBuffStat.疾驰速度)) || (buffStat.contains(MapleBuffStat.疾驰跳跃)) || (buffStat.contains(MapleBuffStat.尖兵飞行)))
        {
            mplew.write(buffStat.contains(MapleBuffStat.隐身术) ? 6 : 4);
        }
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] giveDebuff(MapleDisease statups, int x, int skillid, int level, int duration)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, statups);
        mplew.writeShort(x);
        mplew.writeShort(skillid);
        mplew.writeShort(level);
        mplew.writeInt(duration);
        mplew.writeZeroBytes(4);
        if (skillid == 126)
        {
            mplew.write(0);
        }
        mplew.writeZeroBytes(5);
        mplew.writeShort(0);
        mplew.writeZeroBytes(3);
        mplew.write(4);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] giveForeignDebuff(int chrId, MapleDisease statups, int skillid, int level, int x)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeSingleMask(mplew, statups);
        if (skillid == 125)
        {
            mplew.writeShort(0);
            mplew.write(0);
        }
        mplew.writeShort(x);
        mplew.writeShort(skillid);
        mplew.writeShort(level);
        mplew.writeZeroBytes(3);
        mplew.writeZeroBytes(16);
        mplew.writeZeroBytes(4);
        mplew.writeShort(900);

        return mplew.getPacket();
    }


    public static byte[] cancelForeignDebuff(int chrId, MapleDisease mask)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeSingleMask(mplew, mask);
        mplew.write(3);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] cancelForeignBuff(int chrId, List<MapleBuffStat> statups)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeMask(mplew, statups);
        mplew.write(3);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] cancelForeignBuff(int chrId, MapleBuffStat buffstat)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeSingleMask(mplew, buffstat);
        mplew.write(3);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] giveForeignBuff(int chrId, List<Pair<MapleBuffStat, Integer>> statups, MapleStatEffect effect)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_FOREIGN_BUFF.getValue());
        mplew.writeInt(chrId);
        PacketHelper.writeBuffMask(mplew, statups);
        for (Pair<MapleBuffStat, Integer> statup : statups)
        {
            if ((statup.getLeft() == MapleBuffStat.影分身) || (statup.getLeft() == MapleBuffStat.金属机甲) || (statup.getLeft() == MapleBuffStat.黑暗灵气) || (statup.getLeft() == MapleBuffStat.黄色灵气) || (statup.getLeft() == MapleBuffStat.蓝色灵气) || (statup.getLeft() == MapleBuffStat.GIANT_POTION) || (statup.getLeft() == MapleBuffStat.精神连接) || (statup.getLeft() == MapleBuffStat.天使状态) || (statup.getLeft() == MapleBuffStat.属性攻击) || (statup.getLeft() == MapleBuffStat.爆击提升) || (statup.getLeft() == MapleBuffStat.变身术) || (statup.getLeft() == MapleBuffStat.模式转换) || (statup.getLeft() == MapleBuffStat.伤害吸收) || (statup.getLeft() == MapleBuffStat.尖兵飞行) || (statup.getLeft() == MapleBuffStat.灵魂融入) || (statup.getLeft() == MapleBuffStat.元素灵魂) || (statup.getLeft() == MapleBuffStat.月光转换) || (statup.getLeft() == MapleBuffStat.圣洁之力) || (statup.getLeft() == MapleBuffStat.神圣迅捷))
            {
                mplew.writeShort(statup.getRight().shortValue());
                mplew.writeInt(effect.isSkill() ? effect.getSourceId() : -effect.getSourceId());
            }
            else if (statup.getLeft() == MapleBuffStat.FAMILIAR_SHADOW)
            {
                mplew.writeInt(statup.getRight());
                mplew.writeInt(effect.getCharColor());
            }
            else
            {
                mplew.writeShort(statup.getRight().shortValue());
            }
        }
        mplew.writeZeroBytes(23);
        mplew.writeShort(0);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] cancelBuff(List<MapleBuffStat> statups, MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_BUFF.getValue());
        PacketHelper.writeMask(mplew, statups);
        for (MapleBuffStat mask : statups)
        {
            if (mask.canStack())
            {
                List<SpecialBuffInfo> buffs = chr.getSpecialBuffInfo(mask);
                mplew.writeInt(buffs.size());
                for (SpecialBuffInfo info : buffs)
                {
                    mplew.writeInt(info.buffid);
                    mplew.writeLong(info.value);
                    mplew.writeInt(0);
                    mplew.writeInt(info.bufflength);
                }
            }
        }
        if ((statups.contains(MapleBuffStat.变身术)) || (statups.contains(MapleBuffStat.移动速度)) || (statups.contains(MapleBuffStat.跳跃力)) || (statups.contains(MapleBuffStat.indieJump)) || (statups.contains(MapleBuffStat.indieSpeed)) || (statups.contains(MapleBuffStat.冒险岛勇士)) || (statups.contains(MapleBuffStat.金属机甲)) || (statups.contains(MapleBuffStat.黄色灵气)) || (statups.contains(MapleBuffStat.变形值)) || (statups.contains(MapleBuffStat.能量获得)) || (statups.contains(MapleBuffStat.疾驰速度)) || (statups.contains(MapleBuffStat.疾驰跳跃)) || (statups.contains(MapleBuffStat.尖兵飞行)))
        {
            mplew.write(3);
        }
        else if (statups.contains(MapleBuffStat.骑兽技能))
        {
            mplew.write(3);
            mplew.write(1);
        }
        else if (statups.contains(MapleBuffStat.月光转换))
        {
            mplew.write(0);
        }

        return mplew.getPacket();
    }


    public static byte[] cancelBuff(MapleBuffStat buffstat)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, buffstat);
        if (buffstat.canStack())
        {
            mplew.writeInt(0);
        }

        return mplew.getPacket();
    }


    public static byte[] cancelDebuff(MapleDisease mask)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_BUFF.getValue());
        PacketHelper.writeSingleMask(mplew, mask);
        mplew.write(3);
        mplew.write(0);
        mplew.write(1);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\BuffPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */