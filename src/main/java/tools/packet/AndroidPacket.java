package tools.packet;

import java.awt.Point;
import java.util.List;

import client.MapleCharacter;
import client.inventory.Item;
import client.inventory.MapleAndroid;
import client.inventory.MapleInventoryType;
import handling.SendPacketOpcode;
import server.movement.LifeMovementFragment;
import tools.HexTool;
import tools.data.output.MaplePacketLittleEndianWriter;


public class AndroidPacket
{
    public static byte[] spawnAndroid(MapleCharacter chr, MapleAndroid android)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ANDROID_SPAWN.getValue());
        mplew.writeInt(chr.getId());
        mplew.write(android.getType());
        mplew.writePos(android.getPos());
        mplew.write(android.getStance());
        mplew.writeShort(android.getFh());
        mplew.writeShort(android.getSkin() >= 2000 ? android.getSkin() - 2000 : android.getSkin());
        mplew.writeShort(android.getHair() - 30000);
        mplew.writeShort(android.getFace() - 20000);
        mplew.writeMapleAsciiString(android.getName());
        for (short i = (short) 64336; i > 64329; i = (short) (i - 1))
        {
            Item item = chr.getInventory(MapleInventoryType.EQUIPPED).getItem(i);
            mplew.writeInt(item != null ? item.getItemId() : 0);
        }

        return mplew.getPacket();
    }


    public static byte[] moveAndroid(int chrId, Point pos, List<LifeMovementFragment> res)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ANDROID_MOVE.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(0);
        mplew.writePos(pos);
        mplew.write(HexTool.getByteArrayFromHexString("C1 30 41 02"));
        PacketHelper.serializeMovementList(mplew, res);

        return mplew.getPacket();
    }


    public static byte[] showAndroidEmotion(int chrId, int animation)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ANDROID_EMOTION.getValue());
        mplew.writeInt(chrId);
        mplew.write(0);
        mplew.write(animation);

        return mplew.getPacket();
    }


    public static byte[] updateAndroidLook(int chrId, int size, int itemId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ANDROID_UPDATE.getValue());
        mplew.writeInt(chrId);
        switch (size)
        {
            case -1200:
                mplew.write(1);
                break;
            case -1201:
                mplew.write(2);
                break;
            case -1202:
                mplew.write(4);
                break;
            case -1203:
                mplew.write(8);
                break;
            case -1204:
                mplew.write(16);
                break;
            case -1205:
                mplew.write(32);
                break;
            case -1206:
                mplew.write(64);
        }

        mplew.writeInt(itemId);
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] deactivateAndroid(int chrId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ANDROID_DEACTIVATED.getValue());
        mplew.writeInt(chrId);

        return mplew.getPacket();
    }


    public static byte[] removeAndroidHeart()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());


        mplew.write(20);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\packet\AndroidPacket.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */