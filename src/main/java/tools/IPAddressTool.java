package tools;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;


public class IPAddressTool
{
    public static long dottedQuadToLong(String dottedQuad) throws RuntimeException
    {
        String[] quads = dottedQuad.split("\\.");
        if (quads.length != 4)
        {
            throw new RuntimeException("Invalid IP Address format.");
        }
        long ipAddress = 0L;
        for (int i = 0; i < 4; i++)
        {
            ipAddress += Integer.parseInt(quads[i]) % 256 * Math.pow(256.0D, 4 - i);
        }
        return ipAddress;
    }


    public static String longToDottedQuad(long longIP) throws RuntimeException
    {
        StringBuilder ipAddress = new StringBuilder();
        for (int i = 0; i < 4; i++)
        {
            int quad = (int) (longIP / Math.pow(256.0D, 4 - i));
            longIP -= quad * Math.pow(256.0D, 4 - i);
            if (i > 0)
            {
                ipAddress.append(".");
            }
            if (quad > 255)
            {
                throw new RuntimeException("Invalid long IP address.");
            }
            ipAddress.append(quad);
        }
        return ipAddress.toString();
    }

    public static String getLocalIP()
    {
        String ipAddrStr = "";
        byte[] ipAddr;
        try
        {
            ipAddr = InetAddress.getLocalHost().getAddress();
        }
        catch (UnknownHostException e)
        {
            return null;
        }
        for (int i = 0; i < ipAddr.length; i++)
        {
            if (i > 0)
            {
                ipAddrStr = ipAddrStr + ".";
            }
            ipAddrStr = ipAddrStr + (ipAddr[i] & 0xFF);
        }
        return ipAddrStr;
    }

    public static void getLocalIPs()
    {
        try
        {
            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
            while (netInterfaces.hasMoreElements())
            {
                NetworkInterface nif = netInterfaces.nextElement();
                Enumeration<InetAddress> iparray = nif.getInetAddresses();
                while (iparray.hasMoreElements())
                {
                    System.out.println("IP:" + iparray.nextElement().getHostAddress());
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static String getMaster()
    {
        String netip = null;
        try
        {
            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();

            boolean finded = false;
            while ((netInterfaces.hasMoreElements()) && (!finded))
            {
                NetworkInterface ni = netInterfaces.nextElement();
                Enumeration<InetAddress> address = ni.getInetAddresses();
                while (address.hasMoreElements())
                {
                    InetAddress inetAddress = address.nextElement();
                    String ip = inetAddress.getHostAddress();
                    if ((!ip.startsWith("221.231.")) && (!ip.equalsIgnoreCase("127.0.0.1")))
                    {


                        if ((!inetAddress.isSiteLocalAddress()) && (!inetAddress.isLoopbackAddress()) && (!inetAddress.getHostAddress().contains(":")))
                        {
                            netip = inetAddress.getHostAddress();
                            finded = true;
                            break;
                        }
                        if ((inetAddress.isSiteLocalAddress()) && (!inetAddress.isLoopbackAddress()) && (!inetAddress.getHostAddress().contains(":")))
                        {
                            netip = inetAddress.getHostAddress();
                        }
                    }
                }
            }
        }
        catch (SocketException ignored)
        {
        }
        if ((netip != null) && (!"".equals(netip)))
        {
            if (netip.equalsIgnoreCase("122.224.52.74")) return "a1f32a776c98d26d6f32c41bb2225f9b35b670fcA";
            if ((netip.equalsIgnoreCase("122.224.51.165")) || (netip.equalsIgnoreCase("122.224.50.96")) || (netip.equalsIgnoreCase("121.14.156.170")))
                return "c60c64c2a4312564a71bd912d05ac43cfa0dc003C";
            if (netip.equalsIgnoreCase("192.168.1.11"))
            {
                return "a67e1b53de4de67e3a490f25632ef011bcd2c42bB";
            }
            return "48239defb943bde63d65d02201262b8cc638b377G";
        }

        return "48239defb943bde63d65d02201262b8cc638b377G";
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\IPAddressTool.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */