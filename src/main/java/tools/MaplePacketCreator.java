package tools;

import org.apache.log4j.Logger;

import java.awt.Point;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleExpStat;
import client.MapleQuestStatus;
import client.MapleStat;
import client.MapleTraitType;
import client.MonsterFamiliar;
import client.SkillEntry;
import client.inventory.ImpFlag;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.inventory.MapleRing;
import constants.GameConstants;
import handling.SendPacketOpcode;
import handling.channel.handler.AttackInfo;
import handling.world.WorldGuildService;
import handling.world.guild.MapleGuild;
import server.Randomizer;
import server.ServerProperties;
import server.maps.MapleMap;
import server.maps.MapleNodes;
import server.maps.MapleReactor;
import server.shops.HiredMerchant;
import tools.data.output.MaplePacketLittleEndianWriter;
import tools.packet.PacketHelper;

public class MaplePacketCreator
{
    private static final Logger log = Logger.getLogger(MaplePacketCreator.class);

    public static byte[] getWzCheck(String WzCheckPack)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.WZ_CHECK.getValue());
        mplew.write(HexTool.getByteArrayFromHexString(WzCheckPack));
        return mplew.getPacket();
    }


    public static byte[] getServerIP(client.MapleClient c, int port, int charId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVER_IP.getValue());
        mplew.writeShort(0);
        mplew.write(constants.ServerConstants.NEXON_IP);
        mplew.writeShort(port);
        mplew.writeInt(charId);
        mplew.write(1);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] getChannelChange(client.MapleClient c, int port)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHANGE_CHANNEL.getValue());
        mplew.write(1);
        mplew.write(constants.ServerConstants.NEXON_IP);
        mplew.writeShort(port);
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] cancelTitleEffect()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_TITLE_EFFECT.getValue());
        for (int i = 0; i < 5; i++)
        {
            mplew.writeShort(0);
            mplew.write(-1);
        }

        return mplew.getPacket();
    }


    public static byte[] getCharInfo(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.WARP_TO_MAP.getValue());
        mplew.writeShort(1);
        mplew.writeLong(1L);
        mplew.writeLong(chr.getClient().getChannel() - 1);
        mplew.writeShort(0);
        mplew.writeInt(1);
        mplew.write(new byte[]{0, 1, 0, 0});
        for (int i = 0; i < 3; i++)
        {
            mplew.writeInt(Randomizer.nextInt());
        }
        PacketHelper.addCharacterInfo(mplew, chr);
        mplew.writeZeroBytes(16);
        mplew.write(0);
        mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        mplew.writeInt(100);
        mplew.writeZeroBytes(3);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] updatePlayerStats(Map<client.MapleStat, Long> stats, MapleCharacter chr)
    {
        return updatePlayerStats(stats, false, chr);
    }

    public static byte[] updatePlayerStats(Map<client.MapleStat, Long> mystats, boolean itemReaction, MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_STATS.getValue());
        mplew.write(itemReaction ? 1 : 0);
        long updateMask = 0L;
        for (client.MapleStat statupdate : mystats.keySet())
        {
            updateMask |= statupdate.getValue();
        }
        mplew.writeLong(updateMask);
        for (Map.Entry<client.MapleStat, Long> statupdate : mystats.entrySet())
        {
            switch (statupdate.getKey())
            {
                case 皮肤:
                case 力量:
                case 敏捷:
                case 智力:
                case 运气:
                case AVAILABLEAP:
                    mplew.writeShort(statupdate.getValue().shortValue());
                    break;
                case 脸型:
                case 发型:
                case 职业:
                case HP:
                case MAXHP:
                case MP:
                case MAXMP:
                case 人气:
                case 领袖:
                case 洞察:
                case 意志:
                case 手技:
                case 感性:
                case 魅力:
                    mplew.writeInt(statupdate.getValue().intValue());
                    break;
                case 等级:
                case 疲劳:
                case ICE_GAGE:
                    mplew.write(statupdate.getValue().byteValue());
                    break;
                case AVAILABLESP:
                    if (GameConstants.isSeparatedSpJob(chr.getJob()))
                    {
                        mplew.write(chr.getRemainingSpSize());
                        for (int i = 0; i < chr.getRemainingSps().length; i++)
                        {
                            if (chr.getRemainingSp(i) > 0)
                            {
                                mplew.write(i + 1);
                                mplew.writeInt(chr.getRemainingSp(i));
                            }
                        }
                    }
                    else
                    {
                        mplew.writeShort(chr.getRemainingSp());
                    }
                    break;
                case 经验:
                case 金币:
                    mplew.writeLong(statupdate.getValue());
                    break;
                default:
                    mplew.writeInt(statupdate.getValue().intValue());
            }

        }
        if ((updateMask == 0L) && (!itemReaction))
        {
            mplew.write(1);
        }
        mplew.write(0);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] getWarpToMap(MapleMap to, int spawnPoint, MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.WARP_TO_MAP.getValue());
        mplew.writeShort(1);
        mplew.writeLong(1L);
        mplew.writeLong(chr.getClient().getChannel() - 1);
        mplew.writeShort(0);
        mplew.writeLong(2L);
        mplew.write(0);
        mplew.writeInt(to.getId());
        mplew.write(spawnPoint);
        mplew.writeInt(chr.getStat().getHp());
        mplew.write(0);
        mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        mplew.writeInt(100);
        mplew.writeZeroBytes(3);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] instantMapWarp(byte portal)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CURRENT_MAP_WARP.getValue());
        mplew.write(0);
        mplew.write(portal);

        return mplew.getPacket();
    }

    public static byte[] spawnPortal(int townId, int targetId, int skillId, Point pos)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_PORTAL.getValue());
        mplew.writeInt(townId);
        mplew.writeInt(targetId);
        if ((townId != 999999999) && (targetId != 999999999))
        {
            mplew.writeInt(skillId);
            mplew.writePos(pos);
        }

        return mplew.getPacket();
    }

    public static byte[] spawnDoor(int ownerId, int skillId, Point pos, boolean animation)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_DOOR.getValue());
        mplew.write(animation ? 0 : 1);
        mplew.writeInt(ownerId);
        mplew.writeInt(skillId);
        mplew.writePos(pos);

        return mplew.getPacket();
    }

    public static byte[] removeDoor(int ownerId, boolean animation)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.REMOVE_DOOR.getValue());
        mplew.write(animation ? 0 : 1);
        mplew.writeInt(ownerId);

        return mplew.getPacket();
    }

    public static byte[] resetScreen()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.RESET_SCREEN.getValue());

        return mplew.getPacket();
    }

    public static byte[] mapBlocked(int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MAP_BLOCKED.getValue());
        mplew.write(type);

        return mplew.getPacket();
    }

    public static byte[] serverBlocked(int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVER_BLOCKED.getValue());
        mplew.write(type);

        return mplew.getPacket();
    }

    public static byte[] partyBlocked(int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PARTY_BLOCKED.getValue());
        mplew.write(type);

        return mplew.getPacket();
    }

    public static byte[] serverMessage(String message)
    {
        return serverMessage(4, 0, message, false);
    }

    private static byte[] serverMessage(int type, int channel, String message, boolean megaEar)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.SERVERMESSAGE.getValue());
        mplew.write(type);
        if (type == 4)
        {
            mplew.write(1);
        }
        mplew.writeMapleAsciiString(message);
        switch (type)
        {
            case 3:
            case 9:
            case 24:
            case 25:
            case 26:
            case 27:
                mplew.write(channel - 1);
                mplew.write(megaEar ? 1 : 0);
                break;
            case 16:
                mplew.writeInt(channel - 1);
                break;
            case 6:
                mplew.writeInt((channel >= 1000000) && (channel < 6000000) ? channel : 0);
        }

        return mplew.getPacket();
    }

    public static byte[] serverNotice(int type, int channel, String message)
    {
        return serverMessage(type, channel, message, false);
    }

    public static byte[] serverNotice(int type, int channel, String message, boolean smegaEar)
    {
        return serverMessage(type, channel, message, smegaEar);
    }

    public static byte[] getGachaponMega(String name, String message, Item item, int rareness, int channel)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVERMESSAGE.getValue());
        mplew.write(34);
        mplew.writeMapleAsciiString(name + message);
        mplew.writeInt(item.getItemId());
        mplew.writeInt(channel > 0 ? channel - 1 : -1);
        mplew.writeInt(rareness == 3 ? 3 : rareness == 1 ? 0 : 2);
        mplew.write(1);
        PacketHelper.addItemInfo(mplew, item);

        return mplew.getPacket();
    }

    public static byte[] getAniMsg(int questID, int time)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVERMESSAGE.getValue());
        mplew.write(13);
        mplew.writeShort(questID);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] tripleSmega(List<String> message, boolean ear, int channel)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVERMESSAGE.getValue());
        mplew.write(10);
        if (message.get(0) != null)
        {
            mplew.writeMapleAsciiString(message.get(0));
        }
        mplew.write(message.size());
        for (int i = 1; i < message.size(); i++)
        {
            if (message.get(i) != null)
            {
                mplew.writeMapleAsciiString(message.get(i));
            }
        }
        mplew.write(channel - 1);
        mplew.write(ear ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] getAvatarMega(MapleCharacter chr, int channel, int itemId, List<String> message, boolean ear)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.AVATAR_MEGA.getValue());
        mplew.writeInt(itemId);
        mplew.writeMapleAsciiString(chr.getName());
        for (int i = 0; i < 4; i++)
        {
            mplew.writeMapleAsciiString(message.get(i));
        }
        mplew.writeInt(channel - 1);
        mplew.write(ear ? 1 : 0);
        PacketHelper.addCharLook(mplew, chr, true, chr.isZeroSecondLook());

        return mplew.getPacket();
    }

    public static byte[] itemMegaphone(String msg, boolean whisper, int channel, Item item)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVERMESSAGE.getValue());
        mplew.write(8);
        mplew.writeMapleAsciiString(msg);
        mplew.write(channel - 1);
        mplew.write(whisper ? 1 : 0);
        PacketHelper.addItemPosition(mplew, item, true, false);
        if (item != null)
        {
            PacketHelper.addItemInfo(mplew, item);
        }

        return mplew.getPacket();
    }

    public static byte[] echoMegaphone(String name, String message)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ECHO_MESSAGE.getValue());
        mplew.write(0);
        mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        mplew.writeMapleAsciiString(name);
        mplew.writeMapleAsciiString(message);

        return mplew.getPacket();
    }

    public static byte[] getChatText(int cidfrom, String text, boolean whiteBG, int show)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHATTEXT.getValue());
        mplew.writeInt(cidfrom);
        mplew.write(whiteBG ? 1 : 0);
        mplew.writeMapleAsciiString(text);
        mplew.writeShort(show);
        mplew.write(255);

        return mplew.getPacket();
    }

    public static byte[] GameMaster_Func(int value)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GM_EFFECT.getValue());
        mplew.write(value);
        mplew.writeZeroBytes(17);

        return mplew.getPacket();
    }

    public static byte[] ShowAranCombo(int combo)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ARAN_COMBO.getValue());
        mplew.writeInt(combo);

        return mplew.getPacket();
    }

    public static byte[] rechargeCombo(int value)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ARAN_COMBO_RECHARGE.getValue());
        mplew.writeInt(value);

        return mplew.getPacket();
    }

    public static byte[] getPacketFromHexString(String hex)
    {
        return HexTool.getByteArrayFromHexString(hex);
    }

    public static byte[] showGainExpFromMonster(int gain, boolean white, Map<client.MapleExpStat, Integer> expStats, int 召回戒指经验)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(3);
        mplew.write(white ? 1 : 0);
        mplew.writeInt(gain);
        mplew.write(0);
        long expMask = 0L;
        for (MapleExpStat statupdate : expStats.keySet())
        {
            expMask |= statupdate.getValue();
        }
        client.MapleExpStat statupdate;
        mplew.writeLong(expMask);

        for (Map.Entry<client.MapleExpStat, Integer> sUpdate : expStats.entrySet())
        {
            Long value = sUpdate.getKey().getValue();
            if (value >= 1L)
            {
                if (value == client.MapleExpStat.活动组队经验.getValue())
                {
                    mplew.write(sUpdate.getValue().byteValue());
                }
                else
                {
                    mplew.writeInt(sUpdate.getValue());
                }
            }
        }
        mplew.writeInt(召回戒指经验);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] GainEXP_Monster(int gain, boolean white, int 组队经验, int 精灵祝福经验, int 道具佩戴经验, int 召回戒指经验, int Sidekick_Bonus_EXP, int 网吧特别经验, int 结婚奖励经验)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(3);
        mplew.write(white ? 1 : 0);
        mplew.writeInt(gain);
        mplew.write(0);

        mplew.writeInt(0);
        mplew.writeShort(0);
        mplew.writeInt(结婚奖励经验);
        mplew.writeInt(召回戒指经验);
        mplew.write(0);
        mplew.writeInt(组队经验);
        mplew.writeInt(道具佩戴经验);
        mplew.writeInt(网吧特别经验);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(精灵祝福经验);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] GainEXP_Others(int gain, boolean inChat, boolean white)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(3);
        mplew.write(white ? 1 : 0);
        mplew.writeInt(gain);
        mplew.write(inChat ? 1 : 0);
        mplew.writeZeroBytes(28);
        if (inChat)
        {
            mplew.write(0);
        }

        return mplew.getPacket();
    }

    public static byte[] getShowFameGain(int gain)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(5);
        mplew.writeInt(gain);

        return mplew.getPacket();
    }

    public static byte[] showMesoGain(int gain, boolean inChat)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        if (!inChat)
        {
            mplew.write(0);
            mplew.write(1);
            mplew.write(0);
            mplew.writeInt(gain);
            mplew.writeInt(0);
        }
        else
        {
            mplew.write(6);
            mplew.writeInt(gain);
            mplew.writeInt(-1);
        }

        return mplew.getPacket();
    }

    public static byte[] getShowItemGain(int itemId, short quantity)
    {
        return getShowItemGain(itemId, quantity, false);
    }

    public static byte[] getShowItemGain(int itemId, short quantity, boolean inChat)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        if (inChat)
        {
            mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
            mplew.write(5);
            mplew.write(1);
            mplew.writeInt(itemId);
            mplew.writeInt(quantity);
        }
        else
        {
            mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
            mplew.writeShort(0);
            mplew.writeInt(itemId);
            mplew.writeInt(quantity);
        }
        return mplew.getPacket();
    }

    public static byte[] getShowItemGain(Map<Integer, Integer> showItems)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(5);
        mplew.write(showItems.size());
        for (Map.Entry<Integer, Integer> items : showItems.entrySet())
        {
            mplew.writeInt(items.getKey());
            mplew.writeInt(items.getValue());
        }
        return mplew.getPacket();
    }

    public static byte[] getShowItemGain(List<Pair<Integer, Integer>> showItems)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(5);
        mplew.write(showItems.size());
        for (Pair<Integer, Integer> items : showItems)
        {
            mplew.writeInt(items.left);
            mplew.writeInt(items.right);
        }
        return mplew.getPacket();
    }

    public static byte[] showItemExpired(int itemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(10);
        mplew.write(1);
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }

    public static byte[] showSkillExpired(Map<client.Skill, SkillEntry> update)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(15);
        mplew.write(update.size());
        for (Map.Entry<client.Skill, SkillEntry> skills : update.entrySet())
        {
            mplew.writeInt(skills.getKey().getId());
        }

        return mplew.getPacket();
    }

    public static byte[] showCashItemExpired(int itemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(2);
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }

    public static byte[] showRewardItemAnimation(int itemId, String effect)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(18);
        mplew.writeInt(itemId);
        mplew.write((effect != null) && (effect.length() > 0) ? 1 : 0);
        if ((effect != null) && (effect.length() > 0))
        {
            mplew.writeMapleAsciiString(effect);
        }

        return mplew.getPacket();
    }

    public static byte[] showRewardItemAnimation(int itemId, String effect, int from_playerid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(from_playerid);
        mplew.write(18);
        mplew.writeInt(itemId);
        mplew.write((effect != null) && (effect.length() > 0) ? 1 : 0);
        if ((effect != null) && (effect.length() > 0))
        {
            mplew.writeMapleAsciiString(effect);
        }

        return mplew.getPacket();
    }

    public static byte[] spawnPlayerMapobject(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_PLAYER.getValue());
        mplew.writeInt(chr.getId());
        mplew.write(chr.getLevel());
        mplew.writeMapleAsciiString(chr.getName());
        MapleQuestStatus ultExplorer = chr.getQuestNoAdd(server.quest.MapleQuest.getInstance(111111));
        if ((ultExplorer != null) && (ultExplorer.getCustomData() != null))
        {
            mplew.writeMapleAsciiString(ultExplorer.getCustomData());
        }
        else
        {
            mplew.writeMapleAsciiString("");
        }
        if (chr.getGuildId() <= 0)
        {
            mplew.writeInt(0);
            mplew.writeInt(0);
        }
        else
        {
            MapleGuild guild = WorldGuildService.getInstance().getGuild(chr.getGuildId());
            if (guild != null)
            {
                mplew.writeMapleAsciiString(guild.getName());
                mplew.writeShort(guild.getLogoBG());
                mplew.write(guild.getLogoBGColor());
                mplew.writeShort(guild.getLogo());
                mplew.write(guild.getLogoColor());
            }
            else
            {
                mplew.writeInt(0);
                mplew.writeInt(0);
            }
        }
        mplew.writeZeroBytes(6);
        List<Pair<Integer, Integer>> oldBuffValue = new ArrayList<>();
        List<Pair<Integer, Integer>> newBuffValue = new ArrayList<>();
        int[] mask = new int[12];
        mask[0] |= 0xFF000000;
        mask[1] |= 0x3000;
        mask[5] |= 0x28000;

        if ((chr.getBuffedValue(MapleBuffStat.隐身术) != null) || (chr.isHidden()))
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 隐身术");
            }
            mask[MapleBuffStat.隐身术.getPosition(true)] |= MapleBuffStat.隐身术.getValue();
        }
        if (chr.getBuffedValue(MapleBuffStat.无形箭弩) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 无形箭弩");
            }
            mask[MapleBuffStat.无形箭弩.getPosition(true)] |= MapleBuffStat.无形箭弩.getValue();
        }
        if (chr.getBuffedValue(MapleBuffStat.斗气集中) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 斗气集中");
            }
            mask[MapleBuffStat.斗气集中.getPosition(true)] |= MapleBuffStat.斗气集中.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.斗气集中), 1));
        }
        if (chr.getBuffedValue(MapleBuffStat.属性攻击) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 属性攻击");
            }
            mask[MapleBuffStat.属性攻击.getPosition(true)] |= MapleBuffStat.属性攻击.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.属性攻击), 2));
            oldBuffValue.add(new Pair(chr.getBuffSource(MapleBuffStat.属性攻击), 3));
        }
        if ((chr.getBuffedValue(MapleBuffStat.影分身) != null) && (chr.getBuffSource(MapleBuffStat.影分身) != 15121004))
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 影分身");
            }
            mask[MapleBuffStat.影分身.getPosition(true)] |= MapleBuffStat.影分身.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.影分身), 2));
            oldBuffValue.add(new Pair(chr.getBuffSource(MapleBuffStat.影分身), 3));
        }

        if (chr.getBuffedValue(MapleBuffStat.变身术) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 变身术");
            }
            mask[MapleBuffStat.变身术.getPosition(true)] |= MapleBuffStat.变身术.getValue();
            oldBuffValue.add(new Pair(chr.getStatForBuff(MapleBuffStat.变身术).getMorph(chr), 2));
            oldBuffValue.add(new Pair(chr.getBuffSource(MapleBuffStat.变身术), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.金刚霸体) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 金刚霸体");
            }
            mask[MapleBuffStat.金刚霸体.getPosition(true)] |= MapleBuffStat.金刚霸体.getValue();
        }
        if (chr.getBuffedValue(MapleBuffStat.狂暴战魂) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 狂暴战魂");
            }
            mask[MapleBuffStat.狂暴战魂.getPosition(true)] |= MapleBuffStat.狂暴战魂.getValue();
        }

        if ((chr.getBuffedValue(MapleBuffStat.风影漫步) != null) && (ServerProperties.ShowPacket()))
        {
            System.err.println("出现: 风影漫步");
        }


        if (chr.getBuffedValue(MapleBuffStat.天使状态) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: PYRAMID_PQ");
            }
            mask[MapleBuffStat.天使状态.getPosition(true)] |= MapleBuffStat.天使状态.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.天使状态).intValue(), 2));
            oldBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.天使状态), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.飞行骑乘) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 飞行骑乘");
            }
            mask[MapleBuffStat.飞行骑乘.getPosition(true)] |= MapleBuffStat.飞行骑乘.getValue();
        }
        if ((chr.getBuffedValue(MapleBuffStat.死亡猫头鹰) == null) ||


                (chr.getBuffedValue(MapleBuffStat.终极斩) != null))
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 终极斩");
            }
            mask[MapleBuffStat.终极斩.getPosition(true)] |= MapleBuffStat.终极斩.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.终极斩).intValue(), 2));
            oldBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.终极斩), 3));
        }

        if (chr.getBuffedValue(MapleBuffStat.幻灵飓风) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 幻灵飓风");
            }
            mask[MapleBuffStat.幻灵飓风.getPosition(true)] |= MapleBuffStat.幻灵飓风.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.幻灵飓风).intValue(), 1));
        }
        if (chr.getBuffedValue(MapleBuffStat.潜入) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 潜入");
            }
            mask[MapleBuffStat.潜入.getPosition(true)] |= MapleBuffStat.潜入.getValue();
        }
        if (chr.getBuffedValue(MapleBuffStat.金属机甲) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 金属机甲");
            }
            mask[MapleBuffStat.金属机甲.getPosition(true)] |= MapleBuffStat.金属机甲.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.金属机甲).intValue(), 2));
            oldBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.金属机甲), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.黑暗灵气) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 黑暗灵气");
            }
            mask[MapleBuffStat.黑暗灵气.getPosition(true)] |= MapleBuffStat.黑暗灵气.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.黑暗灵气).intValue(), 2));
            oldBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.黑暗灵气), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.蓝色灵气) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 蓝色灵气");
            }
            mask[MapleBuffStat.蓝色灵气.getPosition(true)] |= MapleBuffStat.蓝色灵气.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.蓝色灵气).intValue(), 2));
            oldBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.蓝色灵气), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.黄色灵气) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 黄色灵气");
            }
            mask[MapleBuffStat.黄色灵气.getPosition(true)] |= MapleBuffStat.黄色灵气.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.黄色灵气).intValue(), 2));
            oldBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.黄色灵气), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.祝福护甲) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 祝福护甲");
            }
            mask[MapleBuffStat.祝福护甲.getPosition(true)] |= MapleBuffStat.祝福护甲.getValue();
        }
        if (chr.getBuffedValue(MapleBuffStat.GIANT_POTION) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: GIANT_POTION");
            }
            mask[MapleBuffStat.GIANT_POTION.getPosition(true)] |= MapleBuffStat.GIANT_POTION.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.GIANT_POTION).intValue(), 2));
            oldBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.GIANT_POTION), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.暴走形态) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 暴走形态");
            }
            mask[MapleBuffStat.暴走形态.getPosition(true)] |= MapleBuffStat.暴走形态.getValue();
        }

        if (chr.getBuffedValue(MapleBuffStat.神圣魔法盾) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 神圣魔法盾");
            }
            mask[MapleBuffStat.神圣魔法盾.getPosition(true)] |= MapleBuffStat.神圣魔法盾.getValue();
        }
        if ((chr.getBuffedValue(MapleBuffStat.黑暗变形) != null) && (ServerProperties.ShowPacket()))
        {
            System.err.println("出现: 黑暗变形");
        }


        if ((chr.getBuffedValue(MapleBuffStat.爆击提升) != null) && (ServerProperties.ShowPacket()))
        {
            System.err.println("出现: 精神注入");
        }


        if ((chr.getBuffedValue(MapleBuffStat.精神连接) != null) && (ServerProperties.ShowPacket()))
        {
            System.err.println("出现: 精神连接");
        }


        if (chr.getBuffedValue(MapleBuffStat.FAMILIAR_SHADOW) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: FAMILIAR_SHADOW");
            }
            mask[MapleBuffStat.FAMILIAR_SHADOW.getPosition(true)] |= MapleBuffStat.FAMILIAR_SHADOW.getValue();
            oldBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.FAMILIAR_SHADOW).intValue(), 3));
            oldBuffValue.add(new Pair(chr.getStatForBuff(MapleBuffStat.FAMILIAR_SHADOW).getCharColor(), 3));
        }

        if (chr.getBuffedValue(MapleBuffStat.伤害吸收) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 伤害吸收");
            }
            mask[MapleBuffStat.伤害吸收.getPosition(true)] |= MapleBuffStat.伤害吸收.getValue();
            newBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.伤害吸收).intValue(), 2));
            newBuffValue.add(new Pair(chr.getBuffSource(MapleBuffStat.伤害吸收), 3));
        }
        if (chr.getBuffedValue(MapleBuffStat.黑暗高潮) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 黑暗高潮");
            }
            mask[MapleBuffStat.黑暗高潮.getPosition(true)] |= MapleBuffStat.黑暗高潮.getValue();
            newBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.黑暗高潮).intValue(), 2));
        }
        Item weapon = chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
        if ((chr.getBuffedValue(MapleBuffStat.剑刃之壁) != null) && (weapon != null))
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 剑刃之壁");
            }
            mask[MapleBuffStat.剑刃之壁.getPosition(true)] |= MapleBuffStat.剑刃之壁.getValue();
        }

        int powerCount = chr.getSpecialStat().getPowerCount();
        if (powerCount > 0)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 尖兵能量");
            }
            mask[MapleBuffStat.尖兵电力.getPosition(true)] |= MapleBuffStat.尖兵电力.getValue();
            newBuffValue.add(new Pair(powerCount, 1));
        }
        if (chr.getBuffedValue(MapleBuffStat.尖兵飞行) != null)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: 尖兵飞行");
            }
            mask[MapleBuffStat.尖兵飞行.getPosition(true)] |= MapleBuffStat.尖兵飞行.getValue();
            newBuffValue.add(new Pair(chr.getBuffedValue(MapleBuffStat.尖兵飞行).intValue(), 2));
            newBuffValue.add(new Pair(chr.getTrueBuffSource(MapleBuffStat.尖兵飞行), 3));
        }

        for (int i = 0; i < mask.length; i++)
        {
            if (ServerProperties.ShowPacket())
            {
                System.err.println("出现: mask [" + i + "] " + mask[i]);
            }
            mplew.writeInt(mask[i]);
        }

        for (Pair<Integer, Integer> i : oldBuffValue)
        {
            if (i.right == 1)
            {
                if (ServerProperties.ShowPacket())
                {
                    System.err.println("oldBuffValue 出现: i.right == 1 " + i.left.byteValue());
                }
                mplew.write(i.left.byteValue());
            }
            else if (i.right == 2)
            {
                if (ServerProperties.ShowPacket())
                {
                    System.err.println("oldBuffValue 出现: i.right == 2 " + i.left.shortValue());
                }
                mplew.writeShort(i.left.shortValue());
            }
            else if (i.right == 3)
            {
                if (ServerProperties.ShowPacket())
                {
                    System.err.println("oldBuffValue 出现: i.right == 3 " + i.left);
                }
                mplew.writeInt(i.left);
            }
        }
        mplew.writeInt(-1);
        mplew.write(Math.min(chr.getBuffedIntValue(MapleBuffStat.击杀点数), 5));

        for (Pair<Integer, Integer> i : newBuffValue)
        {
            if (i.right == 1)
            {
                if (ServerProperties.ShowPacket())
                {
                    System.err.println("newBuffValue 出现: i.right == 1 " + i.left.byteValue());
                }
                mplew.write(i.left.byteValue());
            }
            else if (i.right == 2)
            {
                if (ServerProperties.ShowPacket())
                {
                    System.err.println("newBuffValue 出现: i.right == 2 " + i.left.shortValue());
                }
                mplew.writeShort(i.left.shortValue());
            }
            else if (i.right == 3)
            {
                if (ServerProperties.ShowPacket())
                {
                    System.err.println("newBuffValue 出现: i.right == 3 " + i.left);
                }
                mplew.writeInt(i.left);
            }
        }
        if ((chr.getBuffedValue(MapleBuffStat.剑刃之壁) != null) && (weapon != null))
        {
            client.inventory.Equip skin = (client.inventory.Equip) weapon;
            int itemId = skin.getItemSkin() % 10000 > 0 ? skin.getItemSkin() : weapon.getItemId();
            int buffid = chr.getTrueBuffSource(MapleBuffStat.剑刃之壁);
            mplew.writeShort(chr.getBuffedValue(MapleBuffStat.剑刃之壁));
            mplew.writeInt(buffid);
            mplew.writeZeroBytes(17);
            mplew.writeInt(buffid == 61101002 ? 1 : 2);
            mplew.writeInt(buffid == 61101002 ? 3 : 5);
            mplew.writeInt(itemId);
            mplew.writeInt(buffid == 61101002 ? 3 : 5);
            mplew.writeZeroBytes(buffid == 61101002 ? 5 : 13);
        }
        else
        {
            mplew.writeZeroBytes(26);
        }
        int CHAR_MAGIC_SPAWN = Randomizer.nextInt();
        mplew.writeZeroBytes(7);


        int energybuff = chr.getBuffSource(MapleBuffStat.能量获得);
        if (ServerProperties.ShowPacket())
        {
            System.err.println("energybuff: " + energybuff);
        }
        if (energybuff > 0)
        {
            mplew.writeInt(chr.getSpecialStat().isEnergyfull() ? energybuff : 0);
            mplew.writeLong(Math.min(chr.getEnergyCount(), 10000));
        }
        else
        {
            mplew.writeZeroBytes(12);
        }
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);

        mplew.writeZeroBytes(8);
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);

        mplew.writeZeroBytes(10);
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);

        mplew.writeShort(0);
        int buffSrc = chr.getBuffSource(MapleBuffStat.骑兽技能);
        if (ServerProperties.ShowPacket())
        {
            System.err.println("buffSrc: " + buffSrc);
        }
        if (buffSrc > 0)
        {
            addMountId(mplew, chr, buffSrc);
            mplew.writeInt(buffSrc);
        }
        else
        {
            mplew.writeLong(0L);
        }
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);

        mplew.writeZeroBytes(8);
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);

        mplew.write(1);
        mplew.writeInt(Randomizer.nextInt());
        mplew.writeZeroBytes(10);
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);

        mplew.writeZeroBytes(16);
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);

        mplew.writeZeroBytes(10);
        mplew.write(1);
        mplew.writeInt(CHAR_MAGIC_SPAWN);
        if (chr.getBuffedValue(MapleBuffStat.尖兵飞行) != null)
        {
            mplew.writeZeroBytes(4);
        }

        mplew.writeInt(chr.getJob());
        PacketHelper.addCharLook(mplew, chr, true, chr.isZeroSecondLook());
        if (GameConstants.is神之子(chr.getJob()))
        {
            PacketHelper.addCharLook(mplew, chr, true, !chr.isZeroSecondLook());
        }

        mplew.writeInt(0);
        mplew.writeInt(0);
        if ((chr.getBuffedValue(MapleBuffStat.飞行骑乘) != null) && (buffSrc > 0))
        {
            addMountId(mplew, chr, buffSrc);
            mplew.writeInt(chr.getId());
        }
        else
        {
            mplew.writeLong(0L);
        }
        mplew.writeInt(0);
        mplew.writeInt(Math.min(250, chr.getInventory(MapleInventoryType.CASH).countById(5110000)));
        mplew.writeInt(chr.getItemEffect());
        mplew.writeInt(0);
        mplew.writeInt(chr.getTitleEffect());
        mplew.writeZeroBytes(24);
        mplew.writeInt(-1);
        mplew.write(0);
        mplew.writeInt(constants.ItemConstants.getInventoryType(chr.getChair()) == MapleInventoryType.SETUP ? chr.getChair() : 0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writePos(chr.getTruePosition());
        mplew.write(chr.getStance());
        mplew.writeShort(0);

        client.inventory.MaplePet[] pet = chr.getSpawnPets();
        for (int i = 0; i < 3; i++)
        {
            if ((pet[i] != null) && (pet[i].getSummoned()))
            {
                tools.packet.PetPacket.addPetInfo(mplew, chr, pet[i], true);
            }
        }
        mplew.write(0);

        mplew.writeInt(chr.getMount() != null ? chr.getMount().getLevel() : 1);
        mplew.writeInt(chr.getMount() != null ? chr.getMount().getExp() : 0);
        mplew.writeInt(chr.getMount() != null ? chr.getMount().getFatigue() : 0);

        PacketHelper.addAnnounceBox(mplew, chr);

        mplew.write((chr.getChalkboard() != null) && (chr.getChalkboard().length() > 0) ? 1 : 0);
        if ((chr.getChalkboard() != null) && (chr.getChalkboard().length() > 0))
        {
            mplew.writeMapleAsciiString(chr.getChalkboard());
        }

        Triple<List<MapleRing>, List<MapleRing>, List<MapleRing>> rings = chr.getRings(false);
        addRingInfo(mplew, rings.getLeft());
        addRingInfo(mplew, rings.getMid());
        addMRingInfo(mplew, rings.getRight(), chr);
        mplew.write(0);
        mplew.write(chr.getStat().Berserk ? 1 : 0);
        mplew.writeZeroBytes(9);
        if ((GameConstants.is狂龙战士(chr.getJob())) || (GameConstants.is爆莉萌天使(chr.getJob())))
        {
            mplew.writeZeroBytes(9);
        }

        for (int i = 0; i < 5; i++)
        {
            mplew.write(-1);
        }
        mplew.writeZeroBytes(4);
        mplew.write(1);
        mplew.writeZeroBytes(12);

        return mplew.getPacket();
    }

    public static void addMountId(MaplePacketLittleEndianWriter mplew, MapleCharacter chr, int buffSrc)
    {
        Item c_mount = chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -123);
        Item mount = chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -18);
        int mountId = GameConstants.getMountItem(buffSrc, chr);
        if ((mountId == 0) && (c_mount != null) && (chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -124) != null))
        {
            mplew.writeInt(c_mount.getItemId());
        }
        else if ((mountId == 0) && (mount != null) && (chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -19) != null))
        {
            mplew.writeInt(mount.getItemId());
        }
        else
        {
            mplew.writeInt(mountId);
        }
    }

    public static void addRingInfo(MaplePacketLittleEndianWriter mplew, List<MapleRing> rings)
    {
        mplew.write(rings.size());
        for (MapleRing ring : rings)
        {
            mplew.writeInt(1);
            mplew.writeLong(ring.getRingId());
            mplew.writeLong(ring.getPartnerRingId());
            mplew.writeInt(ring.getItemId());
        }
    }

    public static void addMRingInfo(MaplePacketLittleEndianWriter mplew, List<MapleRing> rings, MapleCharacter chr)
    {
        mplew.write(rings.size());
        for (MapleRing ring : rings)
        {
            mplew.writeInt(chr.getId());
            mplew.writeInt(ring.getPartnerChrId());
            mplew.writeInt(ring.getItemId());
        }
    }

    public static byte[] removePlayerFromMap(int chrId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_PLAYER_FROM_MAP.getValue());
        mplew.writeInt(chrId);

        return mplew.getPacket();
    }

    public static byte[] facialExpression(MapleCharacter from, int expression)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FACIAL_EXPRESSION.getValue());
        mplew.writeInt(from.getId());
        mplew.writeInt(expression);
        mplew.writeInt(-1);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] movePlayer(int chrId, List<server.movement.LifeMovementFragment> moves, Point startPos)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_PLAYER.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(0);
        mplew.writePos(startPos);
        mplew.writeInt(0);
        PacketHelper.serializeMovementList(mplew, moves);

        return mplew.getPacket();
    }

    public static byte[] closeRangeAttack(MapleCharacter chr, int skilllevel, int itemId, AttackInfo attackInfo, boolean energy, boolean hasMoonBuff)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(energy ? SendPacketOpcode.ENERGY_ATTACK.getValue() : SendPacketOpcode.CLOSE_RANGE_ATTACK.getValue());
        addAttackBody(mplew, chr, skilllevel, itemId, attackInfo, hasMoonBuff);
        if (attackInfo.skillId == 2221052)
        {
            mplew.writeInt(attackInfo.charge > 0 ? attackInfo.charge : 0);
        }

        return mplew.getPacket();
    }

    public static void addAttackBody(MaplePacketLittleEndianWriter mplew, MapleCharacter chr, int skilllevel, int itemId, AttackInfo attackInfo, boolean hasMoonBuff)
    {
        int skillId = attackInfo.skillId;
        mplew.writeInt(chr.getId());
        mplew.write(0);
        mplew.write(attackInfo.numAttackedAndDamage);
        mplew.write(chr.getLevel());
        if (skillId > 0)
        {
            mplew.write(skilllevel);
            mplew.writeInt(skillId);
        }
        else
        {
            mplew.write(0);
        }
        switch (skillId)
        {
        }


        switch (skillId)
        {
            case 4121013:
            case 5121016:
            case 5121017:
            case 5321000:
            case 5321012:
            case 5721007:
            case 13121002:
                mplew.write(0);
        }

        if (hasMoonBuff)
        {
            mplew.write(0);
            mplew.write(2);
            mplew.writeInt(11101022);
            mplew.writeShort(20);
        }
        if ((GameConstants.is神之子(chr.getJob())) && (skillId >= 100000000))
        {
            mplew.write(0);
        }
        mplew.write(0);
        mplew.write(attackInfo.unk);
        mplew.write(attackInfo.display);
        mplew.write(attackInfo.direction);
        mplew.write(attackInfo.speed);
        mplew.write(chr.getStat().passive_mastery());
        mplew.writeInt(itemId);
        for (AttackPair oned : attackInfo.allDamage)
        {
            if (oned.attack != null)
            {
                mplew.writeInt(oned.objectid);
                mplew.write(7);
                mplew.writeShort(0);
                if (skillId == 4211006)
                {
                    mplew.write(oned.attack.size());
                }
                for (Pair<Integer, Boolean> eachd : oned.attack)
                {
                    if (eachd.right)
                    {
                        mplew.writeInt(eachd.left + Integer.MIN_VALUE);
                    }
                    else mplew.writeInt(eachd.left);
                }
            }
        }
    }

    public static byte[] rangedAttack(MapleCharacter chr, int skilllevel, int itemId, AttackInfo attackInfo)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.RANGED_ATTACK.getValue());
        addAttackBody(mplew, chr, skilllevel, itemId, attackInfo, false);
        if ((GameConstants.is神之子(chr.getJob())) && (attackInfo.skillId >= 100000000))
        {
            mplew.writeInt(attackInfo.position.x);
            mplew.writeInt(attackInfo.position.y);
        }
        else if (attackInfo.skillposition != null)
        {
            mplew.writePos(attackInfo.skillposition);
        }

        return mplew.getPacket();
    }

    public static byte[] magicAttack(MapleCharacter chr, int skilllevel, int itemId, AttackInfo attackInfo)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MAGIC_ATTACK.getValue());
        addAttackBody(mplew, chr, skilllevel, itemId, attackInfo, false);
        if (attackInfo.charge > 0)
        {
            mplew.writeInt(attackInfo.charge);
        }

        return mplew.getPacket();
    }

    public static byte[] 金钱炸弹效果(int chrId, int skillId, int forceCount, List<Integer> moboids, List<Point> posFroms)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAIN_FORCE.getValue());
        mplew.write(0);
        mplew.writeInt(chrId);
        mplew.writeInt(12);
        mplew.write(1);
        mplew.writeInt(moboids.size());
        for (Integer integer : moboids)
        {
            int moboid = integer;
            mplew.writeInt(moboid);
        }
        mplew.writeInt(skillId);

        for (int i = 0; i < posFroms.size(); i++)
        {
            mplew.write(1);
            mplew.writeInt(forceCount + i);
            mplew.writeInt(1);
            mplew.writeInt(Randomizer.rand(40, 44));
            mplew.writeInt(Randomizer.rand(3, 4));
            mplew.writeInt(Randomizer.rand(50 + i * 10, 100 + i * 10));
            mplew.writeInt(700);
            Point posFrom = posFroms.get(i);
            mplew.writeInt(posFrom != null ? posFrom.x : 0);
            mplew.writeInt(posFrom != null ? posFrom.y : 0);
            mplew.writeZeroBytes(8);
            mplew.writeInt(0);
        }
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] showSpecialAttack(int chrId, int tickCount, int pot_x, int pot_y, int display, int skillId, int skilllevel, boolean isLeft, int speed)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_SPECIAL_ATTACK.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(tickCount);
        mplew.writeInt(pot_x);
        mplew.writeInt(pot_y);
        mplew.writeInt(display);
        mplew.writeInt(skillId);
        mplew.writeInt(skilllevel);
        mplew.write(isLeft ? 1 : 0);
        mplew.writeInt(speed);

        return mplew.getPacket();
    }


    public static byte[] updateCharLook(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_CHAR_LOOK.getValue());
        mplew.writeInt(chr.getId());
        mplew.write(1);
        PacketHelper.addCharLook(mplew, chr, false, chr.isZeroSecondLook());
        Triple<List<MapleRing>, List<MapleRing>, List<MapleRing>> rings = chr.getRings(false);
        addRingInfo(mplew, rings.getLeft());
        addRingInfo(mplew, rings.getMid());
        addMRingInfo(mplew, rings.getRight(), chr);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] updateZeroLook(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_ZERO_LOOK.getValue());
        mplew.writeInt(chr.getId());
        PacketHelper.addCharLook(mplew, chr, false, chr.isZeroSecondLook());

        return mplew.getPacket();
    }

    public static byte[] removeZeroFromMap(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_ZERO_FROM_MAP.getValue());
        mplew.writeInt(chrId);

        return mplew.getPacket();
    }

    public static byte[] damagePlayer(int chrId, int type, int damage, int monsteridfrom, byte direction, int skillid, int pDMG, boolean pPhysical, int pID, byte pType, Point pPos, byte offset,
                                      int offset_d, int fake)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_PLAYER.getValue());
        mplew.writeInt(chrId);
        mplew.write(type);
        mplew.writeInt(damage);
        mplew.write(0);
        if (type >= -1)
        {
            mplew.writeInt(monsteridfrom);
            mplew.write(direction);
            mplew.writeInt(skillid);
            mplew.writeInt(pDMG);
            mplew.write(0);
            if (pDMG > 0)
            {
                mplew.write(pPhysical ? 1 : 0);
                mplew.writeInt(pID);
                mplew.write(pType);
                mplew.writePos(pPos);
            }
            mplew.write(offset);
            if (offset == 1)
            {
                mplew.writeInt(offset_d);
            }
        }
        mplew.writeInt(damage);
        if ((damage <= 0) || (fake > 0))
        {
            mplew.writeInt(fake);
        }
        return mplew.getPacket();
    }

    public static byte[] damagePlayer(int chrId, int type, int monsteridfrom, int damage)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_PLAYER.getValue());
        mplew.writeInt(chrId);
        mplew.write(type);
        mplew.writeInt(damage);
        mplew.write(0);
        mplew.writeInt(monsteridfrom);
        mplew.write(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.write(0);
        mplew.write(0);
        mplew.writeInt(damage);

        return mplew.getPacket();
    }

    public static byte[] updateQuest(MapleQuestStatus quest)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(1);
        mplew.writeShort(quest.getQuest().getId());
        mplew.write(quest.getStatus());
        switch (quest.getStatus())
        {
            case 0:
                mplew.writeZeroBytes(10);
                break;
            case 1:
                mplew.writeMapleAsciiString(quest.getCustomData() != null ? quest.getCustomData() : "");
                break;
            case 2:
                mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        }


        return mplew.getPacket();
    }

    public static byte[] updateInfoQuest(int quest, String data)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(12);
        mplew.writeShort(quest);
        mplew.writeMapleAsciiString(data);

        return mplew.getPacket();
    }

    public static byte[] updateQuestInfo(int quest, int npc, boolean updata)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_QUEST_INFO.getValue());
        mplew.write(11);
        mplew.writeShort(quest);
        mplew.writeInt(npc);
        mplew.writeShort(0);
        mplew.write(updata ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] updateQuestFinish(int quest, int npc, int nextquest)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_QUEST_INFO.getValue());
        mplew.write(11);
        mplew.writeShort(quest);
        mplew.writeInt(npc);
        mplew.writeShort(nextquest);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] updateMedalQuestInfo(byte op, int itemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REISSUE_MEDAL.getValue());


        mplew.write(op);
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }

    public static byte[] charInfo(MapleCharacter chr, boolean isSelf)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHAR_INFO.getValue());
        mplew.writeInt(chr.getId());
        mplew.write(chr.getLevel());
        mplew.writeInt(chr.getJob());
        mplew.write(chr.getStat().pvpRank);
        mplew.writeInt(chr.getFame());
        MapleRing mRing = chr.getMarriageRing();
        mplew.write(mRing != null ? 1 : 0);
        if (mRing != null)
        {
            mplew.writeInt(mRing.getRingId());
            mplew.writeInt(chr.getId());
            mplew.writeInt(mRing.getPartnerChrId());
            mplew.writeShort(3);
            mplew.writeInt(mRing.getItemId());
            mplew.writeInt(mRing.getItemId());
            mplew.writeAsciiString(chr.getName(), 13);
            mplew.writeAsciiString(mRing.getPartnerName(), 13);
        }

        List<Integer> prof = chr.getProfessions();
        mplew.write(prof.size());
        for (Integer integer : prof)
        {
            int i = integer;
            mplew.writeShort(i);
        }

        if (chr.getGuildId() <= 0)
        {
            mplew.writeMapleAsciiString("-");
            mplew.writeMapleAsciiString("");
        }
        else
        {
            handling.world.guild.MapleGuild gs = handling.world.WorldGuildService.getInstance().getGuild(chr.getGuildId());
            if (gs != null)
            {
                mplew.writeMapleAsciiString(gs.getName());
                if (gs.getAllianceId() > 0)
                {
                    handling.world.guild.MapleGuildAlliance allianceName = handling.world.WorldAllianceService.getInstance().getAlliance(gs.getAllianceId());
                    if (allianceName != null)
                    {
                        mplew.writeMapleAsciiString(allianceName.getName());
                    }
                    else
                    {
                        mplew.writeMapleAsciiString("");
                    }
                }
                else
                {
                    mplew.writeMapleAsciiString("");
                }
            }
            else
            {
                mplew.writeMapleAsciiString("-");
                mplew.writeMapleAsciiString("");
            }
        }
        mplew.write(-1);
        mplew.write(0);

        client.inventory.MaplePet[] pets = chr.getSpawnPets();
        mplew.write(chr.getSpawnPet(0) != null ? 1 : 0);
        for (int i = 0; i < 3; i++)
        {
            if ((pets[i] != null) && (pets[i].getSummoned()))
            {
                mplew.write(1);
                mplew.writeInt(i);
                mplew.writeInt(pets[i].getPetItemId());
                mplew.writeMapleAsciiString(pets[i].getName());
                mplew.write(pets[i].getLevel());
                mplew.writeShort(pets[i].getCloseness());
                mplew.write(pets[i].getFullness());
                mplew.writeShort(pets[i].getFlags());
                Item inv = chr.getInventory(MapleInventoryType.EQUIPPED).getItem((byte) (i == 1 ? -122 : i == 0 ? -114 : -124));
                mplew.writeInt(inv == null ? 0 : inv.getItemId());
                mplew.writeInt(-1);
            }
        }
        mplew.write(0);


        int wishlistSize = chr.getWishlistSize();
        mplew.write(wishlistSize);
        if (wishlistSize > 0)
        {
            int[] wishlist = chr.getWishlist();
            for (int x = 0; x < wishlistSize; x++)
            {
                mplew.writeInt(wishlist[x]);
            }
        }

        Item medal = chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -26);
        mplew.writeInt(medal == null ? 0 : medal.getItemId());

        List<Pair<Integer, Long>> medalQuests = chr.getCompletedMedals();
        mplew.writeShort(medalQuests.size());
        for (Object localObject1 = medalQuests.iterator(); ((Iterator) localObject1).hasNext(); )
        {
            Pair x = (Pair) ((Iterator) localObject1).next();
            mplew.writeShort((Integer) x.left);
            mplew.writeLong((Long) x.right);
        }

        MapleTraitType[] localObject1 = client.MapleTraitType.values();
        for (MapleTraitType t : localObject1)
        {
            mplew.write(chr.getTrait(t).getLevel());
        }
        mplew.writeInt(0);
        mplew.writeInt(0);

        Object chairs = new ArrayList<>();
        for (Item i : chr.getInventory(MapleInventoryType.SETUP).newList())
        {
            if ((i.getItemId() / 10000 == 301) && (!((List) chairs).contains(i.getItemId())))
            {
                ((List) chairs).add(i.getItemId());
            }
        }
        mplew.writeInt(((List) chairs).size());
        for (Object o : (List) chairs)
        {
            int i = (Integer) o;
            mplew.writeInt(i);
        }

        List<Integer> medals = new ArrayList<>();
        for (Item i : chr.getInventory(MapleInventoryType.EQUIP).list())
        {
            if ((i.getItemId() >= 1142000) && (i.getItemId() < 1152000))
            {
                medals.add(i.getItemId());
            }
        }
        mplew.writeInt(medals.size());
        for (int i : medals)
        {
            mplew.writeInt(i);
        }

        return mplew.getPacket();
    }

    public static byte[] updateMount(MapleCharacter chr, boolean levelup)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_MOUNT.getValue());
        mplew.writeInt(chr.getId());
        mplew.writeInt(chr.getMount().getLevel());
        mplew.writeInt(chr.getMount().getExp());
        mplew.writeInt(chr.getMount().getFatigue());
        mplew.write(levelup ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] mountInfo(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_MOUNT.getValue());
        mplew.writeInt(chr.getId());
        mplew.write(1);
        mplew.writeInt(chr.getMount().getLevel());
        mplew.writeInt(chr.getMount().getExp());
        mplew.writeInt(chr.getMount().getFatigue());

        return mplew.getPacket();
    }

    public static byte[] showForeignEffect(int chrId, int effect)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(chrId);


        mplew.write(effect);

        return mplew.getPacket();
    }

    public static byte[] showBuffeffect(int chrId, int skillid, int effectid, int playerLevel, int skillLevel)
    {
        return showBuffeffect(chrId, skillid, effectid, playerLevel, skillLevel, (byte) 3);
    }

    public static byte[] showBuffeffect(int chrId, int skillid, int effectid, int playerLevel, int skillLevel, byte direction)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(effectid);
        mplew.writeInt(skillid);
        mplew.write(playerLevel - 1);
        mplew.write(skillLevel);
        if (direction != 3)
        {
            mplew.write(direction);
        }

        return mplew.getPacket();
    }

    public static byte[] showOwnBuffEffect(int skillid, int effectid, int playerLevel, int skillLevel)
    {
        return showOwnBuffEffect(skillid, effectid, playerLevel, skillLevel, (byte) 3);
    }

    public static byte[] showOwnBuffEffect(int skillid, int effectid, int playerLevel, int skillLevel, byte direction)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(effectid);
        mplew.writeInt(skillid);
        mplew.write(playerLevel - 1);
        mplew.write(skillLevel);
        if (direction != 3)
        {
            mplew.write(direction);
        }

        return mplew.getPacket();
    }

    public static byte[] showOwnDiceEffect(int skillid, int effectid, int effectid2, int level)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(3);
        mplew.writeInt(effectid);
        mplew.writeInt(effectid2);
        mplew.writeInt(skillid);
        mplew.write(level);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] showDiceEffect(int chrId, int skillid, int effectid, int effectid2, int level)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(3);
        mplew.writeInt(effectid);
        mplew.writeInt(effectid2);
        mplew.writeInt(skillid);
        mplew.write(level);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] showItemLevelupEffect()
    {
        return showSpecialEffect(19);
    }

    public static byte[] showSpecialEffect(int effect)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(effect);

        return mplew.getPacket();
    }

    public static byte[] showForeignItemLevelupEffect(int chrId)
    {
        return showSpecialEffect(chrId, 19);
    }

    public static byte[] showSpecialEffect(int chrId, int effect)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(effect);

        return mplew.getPacket();
    }

    public static byte[] updateSkill(int skillid, int level, int masterlevel, long expiration)
    {
        boolean isProfession = (skillid == 92000000) || (skillid == 92010000) || (skillid == 92020000) || (skillid == 92030000) || (skillid == 92040000);
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_SKILLS.getValue());
        mplew.write(isProfession ? 0 : 1);
        mplew.write(0);
        mplew.write(0);
        mplew.writeShort(1);
        mplew.writeInt(skillid);
        mplew.writeInt(level);
        mplew.writeInt(masterlevel);
        PacketHelper.addExpirationTime(mplew, expiration);
        mplew.write(isProfession ? 4 : 3);

        return mplew.getPacket();
    }

    public static byte[] updateSkills(Map<client.Skill, SkillEntry> update)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_SKILLS.getValue());
        mplew.write(1);
        mplew.write(0);
        mplew.write(0);
        mplew.writeShort(update.size());
        for (Map.Entry<client.Skill, SkillEntry> skills : update.entrySet())
        {
            mplew.writeInt(skills.getKey().getId());
            mplew.writeInt(skills.getValue().skillevel);
            mplew.writeInt(skills.getValue().masterlevel);
            PacketHelper.addExpirationTime(mplew, skills.getValue().expiration);
        }
        mplew.write(4);

        return mplew.getPacket();
    }

    public static byte[] updatePetSkill(int skillid, int level, int masterlevel, long expiration)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_SKILLS.getValue());
        mplew.write(0);
        mplew.write(1);
        mplew.write(0);
        mplew.writeShort(1);
        mplew.writeInt(skillid);
        mplew.writeInt(level == 0 ? -1 : level);
        mplew.writeInt(masterlevel);
        PacketHelper.addExpirationTime(mplew, expiration);
        mplew.write(4);

        return mplew.getPacket();
    }

    public static byte[] updateQuestMobKills(MapleQuestStatus status)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(1);
        mplew.writeShort(status.getQuest().getId());
        mplew.write(1);
        StringBuilder sb = new StringBuilder();
        for (Integer integer : status.getMobKills().values())
        {
            int kills = integer;
            sb.append(StringUtil.getLeftPaddedStr(String.valueOf(kills), '0', 3));
        }
        mplew.writeMapleAsciiString(sb.toString());

        return mplew.getPacket();
    }

    public static byte[] getShowQuestCompletion(int id)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_QUEST_COMPLETION.getValue());
        mplew.writeShort(id);

        return mplew.getPacket();
    }

    public static byte[] getKeymap(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.KEYMAP.getValue());
        client.MapleKeyLayout keymap = chr.getKeyLayout();
        keymap.writeData(mplew, GameConstants.is林之灵(chr.getJob()) ? 5 : 1);

        return mplew.getPacket();
    }

    public static byte[] petAutoHP(int itemId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PET_AUTO_HP.getValue());
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }

    public static byte[] petAutoMP(int itemId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PET_AUTO_MP.getValue());
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }

    public static byte[] petAutoBuff(int skillId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PET_AUTO_BUFF.getValue());
        mplew.writeInt(skillId);

        return mplew.getPacket();
    }

    public static byte[] openFishingStorage(int npcId, byte slots)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.FISHING_STORE.getValue());
        mplew.write(33);
        mplew.writeLong(-1L);
        mplew.write(slots);
        mplew.writeLong(0L);
        mplew.writeInt(npcId);

        return mplew.getPacket();
    }

    public static byte[] fairyPendantMessage(int position, int percent)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.FAIRY_PEND_MSG.getValue());
        mplew.writeInt(position);
        mplew.writeInt(0);
        mplew.writeInt(percent);

        return mplew.getPacket();
    }

    public static byte[] giveFameResponse(int mode, String charname, int newfame)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FAME_RESPONSE.getValue());
        mplew.write(0);
        mplew.writeMapleAsciiString(charname);
        mplew.write(mode);
        mplew.writeInt(newfame);

        return mplew.getPacket();
    }

    public static byte[] giveFameErrorResponse(int status)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.FAME_RESPONSE.getValue());
        mplew.write(status);

        return mplew.getPacket();
    }

    public static byte[] receiveFame(int mode, String charnameFrom)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FAME_RESPONSE.getValue());
        mplew.write(5);
        mplew.writeMapleAsciiString(charnameFrom);
        mplew.write(mode);

        return mplew.getPacket();
    }

    public static byte[] multiChat(String name, String chattext, int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MULTICHAT.getValue());
        mplew.write(mode);
        mplew.writeMapleAsciiString(name);
        mplew.writeMapleAsciiString(chattext);

        return mplew.getPacket();
    }

    public static byte[] getClock(int time)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CLOCK.getValue());
        mplew.write(2);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] getClockTime(int hour, int min, int sec)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.CLOCK.getValue());
        mplew.write(1);
        mplew.write(hour);
        mplew.write(min);
        mplew.write(sec);

        return mplew.getPacket();
    }

    public static byte[] stopClock()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.STOP_CLOCK.getValue());

        return mplew.getPacket();
    }

    public static byte[] spawnMist(server.maps.MapleMist mist)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.SPAWN_MIST.getValue());
        mplew.writeInt(mist.getObjectId());
        mplew.writeInt(mist.getMistType());
        mplew.writeInt(mist.getOwnerId());
        if (mist.getMobSkill() == null)
        {
            mplew.writeInt(mist.getSourceSkill().getId());
        }
        else
        {
            mplew.writeInt(mist.getMobSkill().getSkillId());
        }
        mplew.write(mist.getSkillLevel());
        mplew.writeShort(mist.getSkillDelay());


        mplew.writeRect(mist.getBox());
        mplew.writeZeroBytes(12);

        return mplew.getPacket();
    }

    public static byte[] removeMist(int oid, boolean eruption)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_MIST.getValue());
        mplew.writeInt(oid);
        mplew.write(eruption ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] spawnLove(int oid, int itemid, String name, String msg, Point pos, int ft)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_LOVE.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(itemid);
        mplew.writeMapleAsciiString(msg);
        mplew.writeMapleAsciiString(name);
        mplew.writeShort(pos.x);
        mplew.writeShort(pos.y + ft);

        return mplew.getPacket();
    }

    public static byte[] removeLove(int oid, int itemid)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_LOVE.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }

    public static byte[] itemEffect(int chrId, int itemid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }

    public static byte[] showTitleEffect(int chrId, int itemid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_TITLE_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }

    public static byte[] showUnkEffect(int chrId, int itemid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_UNK_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(itemid);

        return mplew.getPacket();
    }

    public static byte[] showChair(int chrId, int itemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_CHAIR.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(itemId);
        mplew.writeInt(0);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] cancelChair(int id)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_CHAIR.getValue());
        if (id == -1)
        {
            mplew.write(0);
        }
        else
        {
            mplew.write(1);
            mplew.writeShort(id);
        }

        return mplew.getPacket();
    }

    public static byte[] spawnReactor(MapleReactor reactor)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }


        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REACTOR_SPAWN.getValue());
        mplew.writeInt(reactor.getObjectId());
        mplew.writeInt(reactor.getReactorId());
        mplew.write(reactor.getState());
        mplew.writePos(reactor.getTruePosition());
        mplew.write(reactor.getFacingDirection());
        mplew.writeMapleAsciiString(reactor.getName());

        return mplew.getPacket();
    }

    public static byte[] triggerReactor(MapleReactor reactor, int stance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REACTOR_HIT.getValue());
        mplew.writeInt(reactor.getObjectId());
        mplew.write(reactor.getState());
        mplew.writePos(reactor.getTruePosition());
        mplew.writeInt(stance);

        return mplew.getPacket();
    }

    public static byte[] destroyReactor(MapleReactor reactor)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REACTOR_DESTROY.getValue());
        mplew.writeInt(reactor.getObjectId());
        mplew.write(reactor.getState());
        mplew.writePos(reactor.getPosition());

        return mplew.getPacket();
    }

    public static byte[] musicChange(String song)
    {
        return environmentChange(song, 7);
    }

    public static byte[] environmentChange(String env, int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOSS_ENV.getValue());
        mplew.write(mode);
        mplew.writeMapleAsciiString(env);

        return mplew.getPacket();
    }

    public static byte[] showEffect(String effect)
    {
        return environmentChange(effect, 12);
    }

    public static byte[] playSound(String sound)
    {
        return environmentChange(sound, 5);
    }

    public static byte[] environmentMove(String env, int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_ENV.getValue());
        mplew.writeMapleAsciiString(env);
        mplew.writeInt(mode);

        return mplew.getPacket();
    }

    public static byte[] startMapEffect(String msg, int itemid, boolean active)
    {
        return startMapEffect(msg, 0, -1, active);
    }

    public static byte[] startMapEffect(String msg, int itemid, int effectType, boolean active)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.MAP_EFFECT.getValue());
        mplew.write(active ? 0 : 1);
        mplew.writeInt(itemid);
        if (effectType > 0)
        {
            mplew.writeInt(effectType);
        }
        if (active)
        {
            mplew.writeMapleAsciiString(msg);
        }

        return mplew.getPacket();
    }

    public static byte[] removeMapEffect()
    {
        return startMapEffect(null, 0, -1, false);
    }

    public static byte[] showPredictCard(String name, String otherName, int love, int cardId, int commentId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_PREDICT_CARD.getValue());
        mplew.writeMapleAsciiString(name);
        mplew.writeMapleAsciiString(otherName);
        mplew.writeInt(love);
        mplew.writeInt(cardId);
        mplew.writeInt(commentId);

        return mplew.getPacket();
    }

    public static byte[] skillEffect(int fromId, int skillId, byte level, byte display, byte direction, byte speed, Point position)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SKILL_EFFECT.getValue());
        mplew.writeInt(fromId);
        mplew.writeInt(skillId);
        mplew.write(level);
        mplew.write(display);
        mplew.write(direction);
        mplew.write(speed);
        if (position != null)
        {
            mplew.writePos(position);
        }

        return mplew.getPacket();
    }

    public static byte[] skillCancel(MapleCharacter from, int skillId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_SKILL_EFFECT.getValue());
        mplew.writeInt(from.getId());
        mplew.writeInt(skillId);

        return mplew.getPacket();
    }

    public static byte[] sendHint(String hint, int width, int height)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        if (width < 1)
        {
            width = hint.length() * 10;
            if (width < 40)
            {
                width = 40;
            }
        }
        if (height < 5)
        {
            height = 5;
        }
        mplew.writeShort(SendPacketOpcode.PLAYER_HINT.getValue());
        mplew.writeMapleAsciiString(hint);
        mplew.writeShort(width);
        mplew.writeShort(height);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] showEquipEffect()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_EQUIP_EFFECT.getValue());

        return mplew.getPacket();
    }

    public static byte[] showEquipEffect(int team)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_EQUIP_EFFECT.getValue());
        mplew.writeShort(team);

        return mplew.getPacket();
    }

    public static byte[] skillCooldown(int skillId, int time)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.COOLDOWN.getValue());
        mplew.writeInt(skillId);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] useSkillBook(MapleCharacter chr, int skillid, int maxlevel, boolean canuse, boolean success)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.USE_SKILL_BOOK.getValue());
        mplew.write(0);
        mplew.writeInt(chr.getId());
        mplew.write(1);
        mplew.writeInt(skillid);
        mplew.writeInt(maxlevel);
        mplew.write(canuse ? 1 : 0);
        mplew.write(success ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] getMacros(client.SkillMacro[] macros)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SKILL_MACRO.getValue());
        int count = 0;
        for (int i = 0; i < 5; i++)
        {
            if (macros[i] != null)
            {
                count++;
            }
        }
        mplew.write(count);
        for (int i = 0; i < 5; i++)
        {
            client.SkillMacro macro = macros[i];
            if (macro != null)
            {
                mplew.writeMapleAsciiString(macro.getName());
                mplew.write(macro.getShout());
                mplew.writeInt(macro.getSkill1());
                mplew.writeInt(macro.getSkill2());
                mplew.writeInt(macro.getSkill3());
            }
        }

        return mplew.getPacket();
    }

    public static byte[] updateAriantPQRanking(String name, int score, boolean empty)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ARIANT_PQ_START.getValue());
        mplew.write(empty ? 0 : 1);
        if (!empty)
        {
            mplew.writeMapleAsciiString(name);
            mplew.writeInt(score);
        }

        return mplew.getPacket();
    }

    public static byte[] showAriantScoreBoard()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ARIANT_SCOREBOARD.getValue());

        return mplew.getPacket();
    }

    public static byte[] boatPacket(int effect)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.BOAT_EFFECT.getValue());
        mplew.writeShort(effect);


        return mplew.getPacket();
    }

    public static byte[] boatEffect(int effect)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.BOAT_EFF.getValue());
        mplew.writeShort(effect);


        return mplew.getPacket();
    }

    public static byte[] removeItemFromDuey(boolean remove, int Package)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DUEY.getValue());
        mplew.write(24);
        mplew.writeInt(Package);
        mplew.write(remove ? 3 : 4);

        return mplew.getPacket();
    }

    public static byte[] sendDuey(byte operation, List<server.MapleDueyActions> packages)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DUEY.getValue());
        mplew.write(operation);
        switch (operation)
        {
            case 9:
                mplew.write(1);

                break;

            case 10:
                mplew.write(0);
                mplew.write(packages.size());
                for (server.MapleDueyActions dp : packages)
                {
                    mplew.writeInt(dp.getPackageId());
                    mplew.writeAsciiString(dp.getSender(), 13);
                    mplew.writeInt(dp.getMesos());
                    mplew.writeLong(PacketHelper.getTime(dp.getSentTime()));
                    mplew.writeZeroBytes(202);
                    if (dp.getItem() != null)
                    {
                        mplew.write(1);
                        PacketHelper.addItemInfo(mplew, dp.getItem());
                    }
                    else
                    {
                        mplew.write(0);
                    }
                }
                mplew.write(0);
        }


        return mplew.getPacket();
    }

    public static byte[] enableTV()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ENABLE_TV.getValue());
        mplew.writeInt(0);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] removeTV()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_TV.getValue());

        return mplew.getPacket();
    }

    public static byte[] sendTV(MapleCharacter chr, List<String> messages, int type, MapleCharacter partner, int delay)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.START_TV.getValue());
        mplew.write(partner != null ? 2 : 1);
        mplew.write(type);
        PacketHelper.addCharLook(mplew, chr, false, chr.isZeroSecondLook());
        mplew.writeMapleAsciiString(chr.getName());

        if (partner != null)
        {
            mplew.writeMapleAsciiString(partner.getName());
        }
        else
        {
            mplew.writeShort(0);
        }
        for (int i = 0; i < messages.size(); i++)
        {
            if ((i == 4) && (messages.get(4).length() > 15))
            {
                mplew.writeMapleAsciiString(messages.get(4).substring(0, 15));
            }
            else
            {
                mplew.writeMapleAsciiString(messages.get(i));
            }
        }
        mplew.writeInt(delay);
        if (partner != null)
        {
            PacketHelper.addCharLook(mplew, partner, false, partner.isZeroSecondLook());
        }

        return mplew.getPacket();
    }

    public static byte[] Mulung_DojoUp2()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(11);

        return mplew.getPacket();
    }

    public static byte[] Mulung_Pts(int recv, int total)
    {
        return showQuestMsg("获得了 " + recv + " 点修炼点数。总修炼点数为 " + total + " 点。");
    }

    public static byte[] showQuestMsg(String msg)
    {
        return serverNotice(5, msg);
    }

    public static byte[] serverNotice(int type, String message)
    {
        return serverMessage(type, 0, message, false);
    }

    public static byte[] showOXQuiz(int questionSet, int questionId, boolean askQuestion)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OX_QUIZ.getValue());
        mplew.write(askQuestion ? 1 : 0);
        mplew.write(questionSet);
        mplew.writeShort(questionId);

        return mplew.getPacket();
    }

    public static byte[] leftKnockBack()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LEFT_KNOCK_BACK.getValue());

        return mplew.getPacket();
    }

    public static byte[] enterSnowBall()
    {
        return rollSnowball(0, null, null);
    }

    public static byte[] rollSnowball(int type, server.events.MapleSnowball.MapleSnowballs ball1, server.events.MapleSnowball.MapleSnowballs ball2)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ROLL_SNOWBALL.getValue());
        mplew.write(type);
        mplew.writeInt(ball1 == null ? 0 : ball1.getSnowmanHP() / 75);
        mplew.writeInt(ball2 == null ? 0 : ball2.getSnowmanHP() / 75);
        mplew.writeShort(ball1 == null ? 0 : ball1.getPosition());
        mplew.write(0);
        mplew.writeShort(ball2 == null ? 0 : ball2.getPosition());
        mplew.writeZeroBytes(11);

        return mplew.getPacket();
    }

    public static byte[] hitSnowBall(int team, int damage, int distance, int delay)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.HIT_SNOWBALL.getValue());
        mplew.write(team);
        mplew.writeShort(damage);
        mplew.write(distance);
        mplew.write(delay);

        return mplew.getPacket();
    }

    public static byte[] snowballMessage(int team, int message)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SNOWBALL_MESSAGE.getValue());
        mplew.write(team);
        mplew.writeInt(message);

        return mplew.getPacket();
    }

    public static byte[] finishedSort(int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }


        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FINISH_SORT.getValue());
        mplew.write(1);
        mplew.write(type);

        return mplew.getPacket();
    }

    public static byte[] coconutScore(int[] coconutscore)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.COCONUT_SCORE.getValue());
        mplew.writeShort(coconutscore[0]);
        mplew.writeShort(coconutscore[1]);

        return mplew.getPacket();
    }

    public static byte[] hitCoconut(boolean spawn, int id, int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.HIT_COCONUT.getValue());
        if (spawn)
        {
            mplew.write(0);
            mplew.writeInt(128);
        }
        else
        {
            mplew.writeInt(id);
            mplew.write(type);
        }

        return mplew.getPacket();
    }

    public static byte[] finishedGather(int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }


        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FINISH_GATHER.getValue());
        mplew.write(1);
        mplew.write(type);

        return mplew.getPacket();
    }

    public static byte[] yellowChat(String msg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.SPOUSE_MESSAGE.getValue());
        mplew.writeShort(7);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] getPeanutResult(int itemId, short quantity, int itemId2, short quantity2, int ourItem)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PIGMI_REWARD.getValue());
        mplew.writeInt(itemId);
        mplew.writeShort(quantity);
        mplew.writeInt(ourItem);
        mplew.writeInt(itemId2);
        mplew.writeInt(quantity2);

        return mplew.getPacket();
    }

    public static byte[] sendLevelup(boolean family, int level, String name)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LEVEL_UPDATE.getValue());
        mplew.write(family ? 1 : 2);
        mplew.writeInt(level);
        mplew.writeMapleAsciiString(name);

        return mplew.getPacket();
    }

    public static byte[] sendMarriage(boolean family, String name)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MARRIAGE_UPDATE.getValue());
        mplew.write(family ? 1 : 0);
        mplew.writeMapleAsciiString(name);

        return mplew.getPacket();
    }

    public static byte[] sendJobup(boolean family, int jobid, String name)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.JOB_UPDATE.getValue());
        mplew.write(family ? 1 : 0);
        mplew.writeInt(jobid);
        mplew.writeMapleAsciiString(((GameConstants.GMS) && (!family) ? "> " : "") + name);

        return mplew.getPacket();
    }

    public static byte[] showHorntailShrine(boolean spawned, int time)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.HORNTAIL_SHRINE.getValue());
        mplew.write(spawned ? 1 : 0);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] showChaosZakumShrine(boolean spawned, int time)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHAOS_ZAKUM_SHRINE.getValue());
        mplew.write(spawned ? 1 : 0);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] showChaosHorntailShrine(boolean spawned, int time)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHAOS_HORNTAIL_SHRINE.getValue());
        mplew.write(spawned ? 1 : 0);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] spawnDragon(server.maps.MapleDragon dragon)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DRAGON_SPAWN.getValue());
        mplew.writeInt(dragon.getOwner());
        mplew.writeInt(dragon.getPosition().x);
        mplew.writeInt(dragon.getPosition().y);
        mplew.write(dragon.getStance());
        mplew.writeShort(0);
        mplew.writeShort(dragon.getJobId());

        return mplew.getPacket();
    }

    public static byte[] removeDragon(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DRAGON_REMOVE.getValue());
        mplew.writeInt(chrId);

        return mplew.getPacket();
    }

    public static byte[] moveDragon(server.maps.MapleDragon dragon, Point startPos, List<server.movement.LifeMovementFragment> moves)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DRAGON_MOVE.getValue());
        mplew.writeInt(dragon.getOwner());
        mplew.writeInt(0);
        mplew.writePos(startPos);
        mplew.writeInt(0);
        PacketHelper.serializeMovementList(mplew, moves);

        return mplew.getPacket();
    }

    public static byte[] showDragonFly(int chrId, int type, int mountId)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_DRAGON_FLY.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(type);
        if (type == 0)
        {
            mplew.writeInt(mountId);
        }

        return mplew.getPacket();
    }

    public static byte[] temporaryStats_Aran()
    {
        Map<MapleStat.Temp, Integer> stats = new EnumMap(MapleStat.Temp.class);
        stats.put(MapleStat.Temp.力量, 999);
        stats.put(MapleStat.Temp.敏捷, 999);
        stats.put(MapleStat.Temp.智力, 999);
        stats.put(MapleStat.Temp.运气, 999);
        stats.put(MapleStat.Temp.物攻, 255);
        stats.put(MapleStat.Temp.命中, 999);
        stats.put(MapleStat.Temp.回避, 999);
        stats.put(MapleStat.Temp.速度, 140);
        stats.put(MapleStat.Temp.跳跃, 120);
        return temporaryStats(stats);
    }

    public static byte[] temporaryStats(Map<MapleStat.Temp, Integer> mystats)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.TEMP_STATS.getValue());


        int updateMask = 0;
        for (MapleStat.Temp statupdate : mystats.keySet())
        {
            updateMask |= statupdate.getValue();
        }
        MapleStat.Temp statupdate;
        mplew.writeInt(updateMask);

        for (Map.Entry<MapleStat.Temp, Integer> staUpdate : mystats.entrySet())
        {
            Integer value = staUpdate.getKey().getValue();
            if (value >= 1)
            {
                if (value <= 512)
                {
                    mplew.writeShort(staUpdate.getValue().shortValue());
                }
                else
                {
                    mplew.write(staUpdate.getValue().byteValue());
                }
            }
        }

        return mplew.getPacket();
    }

    public static byte[] temporaryStats_Balrog(MapleCharacter chr)
    {
        Map<MapleStat.Temp, Integer> stats = new EnumMap(MapleStat.Temp.class);
        int offset = 1 + (chr.getLevel() - 90) / 20;


        stats.put(MapleStat.Temp.力量, chr.getStat().getTotalStr() / offset);
        stats.put(MapleStat.Temp.敏捷, chr.getStat().getTotalDex() / offset);
        stats.put(MapleStat.Temp.智力, chr.getStat().getTotalInt() / offset);
        stats.put(MapleStat.Temp.运气, chr.getStat().getTotalLuk() / offset);
        stats.put(MapleStat.Temp.物攻, chr.getStat().getTotalWatk() / offset);
        stats.put(MapleStat.Temp.物防, chr.getStat().getTotalMagic() / offset);
        return temporaryStats(stats);
    }

    public static byte[] temporaryStats_Reset()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.TEMP_STATS_RESET.getValue());

        return mplew.getPacket();
    }

    public static byte[] showHpHealed(int chrId, int amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(31);
        mplew.writeInt(amount);

        return mplew.getPacket();
    }

    public static byte[] showOwnHpHealed(int amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(31);
        mplew.writeInt(amount);

        return mplew.getPacket();
    }

    public static byte[] showBlessOfDarkness(int skillId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(7);
        mplew.writeInt(skillId);

        return mplew.getPacket();
    }

    public static byte[] showHolyFountain(int skillId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(2);
        mplew.writeInt(skillId);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] sendLinkSkillWindow(int skillId)
    {
        return sendUIWindow(3, skillId);
    }

    public static byte[] sendUIWindow(int op, int npc)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REPAIR_WINDOW.getValue());


        mplew.writeInt(op);
        mplew.writeInt(npc);
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] sendPartyWindow(int npc)
    {
        return sendUIWindow(21, npc);
    }


    public static byte[] sendRepairWindow(int npc)
    {
        return sendUIWindow(33, npc);
    }


    public static byte[] sendProfessionWindow(int npc)
    {
        return sendUIWindow(42, npc);
    }

    public static byte[] sendPVPWindow(int npc)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.EVENT_WINDOW.getValue());
        mplew.writeInt(50);
        if (npc > 0)
        {
            mplew.writeInt(npc);
        }

        return mplew.getPacket();
    }

    public static byte[] sendEventWindow(int npc)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.EVENT_WINDOW.getValue());
        mplew.writeInt(55);
        if (npc > 0)
        {
            mplew.writeInt(npc);
        }

        return mplew.getPacket();
    }

    public static byte[] sendPVPMaps()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_INFO.getValue());
        mplew.write(1);
        mplew.writeInt(0);
        for (int i = 0; i < 3; i++)
        {
            mplew.writeInt(1);
        }
        mplew.writeLong(0L);
        for (int i = 0; i < 3; i++)
        {
            mplew.writeInt(1);
        }
        mplew.writeLong(0L);
        for (int i = 0; i < 4; i++)
        {
            mplew.writeInt(1);
        }
        for (int i = 0; i < 10; i++)
        {
            mplew.writeInt(1);
        }
        mplew.writeInt(14);
        mplew.writeShort(100);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] sendPyramidUpdate(int amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PYRAMID_UPDATE.getValue());
        mplew.writeInt(amount);

        return mplew.getPacket();
    }

    public static byte[] sendPyramidResult(byte rank, int amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PYRAMID_RESULT.getValue());
        mplew.write(rank);
        mplew.writeInt(amount);

        return mplew.getPacket();
    }

    public static byte[] sendGhostPoint(String type, String amount)
    {
        return sendString(2, type, amount);
    }

    public static byte[] sendString(int type, String object, String amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        switch (type)
        {
            case 1:
                mplew.writeShort(SendPacketOpcode.ENERGY.getValue());
                break;
            case 2:
                mplew.writeShort(SendPacketOpcode.GHOST_POINT.getValue());
                break;
            case 3:
                mplew.writeShort(SendPacketOpcode.GHOST_STATUS.getValue());
        }

        mplew.writeMapleAsciiString(object);
        mplew.writeMapleAsciiString(amount);

        return mplew.getPacket();
    }

    public static byte[] sendGhostStatus(String type, String amount)
    {
        return sendString(3, type, amount);
    }

    public static byte[] MulungEnergy(int energy)
    {
        return sendPyramidEnergy("energy", String.valueOf(energy));
    }

    public static byte[] sendPyramidEnergy(String type, String amount)
    {
        return sendString(1, type, amount);
    }

    public static byte[] getPollQuestion()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAME_POLL_QUESTION.getValue());
        mplew.writeInt(1);
        mplew.writeInt(14);
        mplew.writeMapleAsciiString(constants.ServerConstants.Poll_Question);
        mplew.writeInt(constants.ServerConstants.Poll_Answers.length);
        for (byte i = 0; i < constants.ServerConstants.Poll_Answers.length; i = (byte) (i + 1))
        {
            mplew.writeMapleAsciiString(constants.ServerConstants.Poll_Answers[i]);
        }

        return mplew.getPacket();
    }

    public static byte[] getPollReply(String message)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAME_POLL_REPLY.getValue());
        mplew.writeMapleAsciiString(message);

        return mplew.getPacket();
    }

    public static byte[] showEventInstructions()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GMEVENT_INSTRUCTIONS.getValue());
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] getOwlOpen()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OWL_OF_MINERVA.getValue());
        mplew.write(10);
        List<Integer> owlItems = server.RankingWorker.getItemSearch();
        mplew.write(owlItems.size());
        for (Integer owlItem : owlItems)
        {
            int i = owlItem;
            mplew.writeInt(i);
        }

        return mplew.getPacket();
    }

    public static byte[] getOwlSearched(int itemSearch, List<HiredMerchant> hms)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OWL_OF_MINERVA.getValue());
        mplew.write(9);
        mplew.writeInt(0);
        mplew.writeShort(0);
        mplew.writeInt(itemSearch);
        int size = 0;
        for (HiredMerchant hm : hms)
        {
            size += hm.searchItem(itemSearch).size();
        }
        mplew.writeInt(size);
        for (HiredMerchant hm : hms)
        {
            List<server.shops.MaplePlayerShopItem> items = hm.searchItem(itemSearch);
            for (server.shops.MaplePlayerShopItem item : items)
            {
                mplew.writeMapleAsciiString(hm.getOwnerName());
                mplew.writeInt(hm.getMap().getId());
                mplew.writeMapleAsciiString(hm.getDescription());
                mplew.writeInt(item.item.getQuantity());
                mplew.writeInt(item.bundles);
                mplew.writeLong(item.price);
                switch (1)
                {
                    case 0:
                        mplew.writeInt(hm.getOwnerId());
                        break;
                    case 1:
                        mplew.writeInt(hm.getStoreId());
                        break;
                    default:
                        mplew.writeInt(hm.getObjectId());
                }

                mplew.write(hm.getChannel() - 1);
                mplew.write(constants.ItemConstants.getInventoryType(itemSearch).getType());
                if (constants.ItemConstants.getInventoryType(itemSearch) == MapleInventoryType.EQUIP) PacketHelper.addItemInfo(mplew, item.item);
            }
        }
        HiredMerchant hm;
        return mplew.getPacket();
    }

    public static byte[] getRPSMode(byte mode, int mesos, int selection, int answer)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.RPS_GAME.getValue());
        mplew.write(mode);
        switch (mode)
        {
            case 6:
                if (mesos != -1)
                {
                    mplew.writeInt(mesos);
                }

                break;
            case 8:
                mplew.writeInt(9000019);
                break;

            case 11:
                mplew.write(selection);
                mplew.write(answer);
        }


        return mplew.getPacket();
    }

    public static byte[] followRequest(int chrid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FOLLOW_REQUEST.getValue());
        mplew.writeInt(chrid);

        return mplew.getPacket();
    }

    public static byte[] followEffect(int initiator, int replier, Point toMap)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FOLLOW_EFFECT.getValue());
        mplew.writeInt(initiator);
        mplew.writeInt(replier);
        if (replier == 0)
        {
            mplew.write(toMap == null ? 0 : 1);
            if (toMap != null)
            {
                mplew.writeInt(toMap.x);
                mplew.writeInt(toMap.y);
            }
        }
        return mplew.getPacket();
    }

    public static byte[] getFollowMsg(int opcode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FOLLOW_MSG.getValue());


        mplew.writeLong(opcode);

        return mplew.getPacket();
    }

    public static byte[] moveFollow(Point otherStart, Point myStart, Point otherEnd, List<server.movement.LifeMovementFragment> moves)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FOLLOW_MOVE.getValue());
        mplew.writeInt(0);
        mplew.writePos(otherStart);
        mplew.writePos(myStart);
        PacketHelper.serializeMovementList(mplew, moves);
        mplew.write(17);
        for (int i = 0; i < 8; i++)
        {
            mplew.write(0);
        }
        mplew.write(0);
        mplew.writePos(otherEnd);
        mplew.writePos(otherStart);

        return mplew.getPacket();
    }

    public static byte[] getFollowMessage(String msg)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPOUSE_MESSAGE.getValue());
        mplew.writeShort(11);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] getMovingPlatforms(MapleMap map)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_PLATFORM.getValue());
        mplew.writeInt(map.getPlatforms().size());
        for (MapleNodes.MaplePlatform mp : map.getPlatforms())
        {
            mplew.writeMapleAsciiString(mp.name);
            mplew.writeInt(mp.start);
            mplew.writeInt(mp.SN.size());
            for (int x = 0; x < mp.SN.size(); x++)
            {
                mplew.writeInt(mp.SN.get(x));
            }
            mplew.writeInt(mp.speed);
            mplew.writeInt(mp.x1);
            mplew.writeInt(mp.x2);
            mplew.writeInt(mp.y1);
            mplew.writeInt(mp.y2);
            mplew.writeInt(mp.x1);
            mplew.writeInt(mp.y1);
            mplew.writeShort(mp.r);
        }
        return mplew.getPacket();
    }

    public static byte[] getUpdateEnvironment(MapleMap map)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_ENV.getValue());
        mplew.writeInt(map.getEnvironment().size());
        for (Map.Entry<String, Integer> mp : map.getEnvironment().entrySet())
        {
            mplew.writeMapleAsciiString(mp.getKey());
            mplew.writeInt(mp.getValue());
        }
        return mplew.getPacket();
    }

    public static byte[] trembleEffect(int type, int delay)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOSS_ENV.getValue());
        mplew.write(1);
        mplew.write(type);
        mplew.writeInt(delay);

        return mplew.getPacket();
    }

    public static byte[] sendEngagementRequest(String name, int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ENGAGE_REQUEST.getValue());
        mplew.write(0);
        mplew.writeMapleAsciiString(name);
        mplew.writeInt(chrId);

        return mplew.getPacket();
    }

    public static byte[] sendEngagement(byte msg, int item, MapleCharacter male, MapleCharacter female)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ENGAGE_RESULT.getValue());
        mplew.write(msg);
        switch (msg)
        {
            case 13:
            case 14:
            case 17:
                mplew.writeInt(0);
                mplew.writeInt(male.getId());
                mplew.writeInt(female.getId());
                mplew.writeShort(msg == 14 ? 3 : 1);
                mplew.writeInt(item);
                mplew.writeInt(item);
                mplew.writeAsciiString(male.getName(), 13);
                mplew.writeAsciiString(female.getName(), 13);
        }


        return mplew.getPacket();
    }

    public static byte[] updateJaguar(MapleCharacter from)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_JAGUAR.getValue());
        PacketHelper.addJaguarInfo(mplew, from);

        return mplew.getPacket();
    }

    public static byte[] teslaTriangle(int chrId, int sum1, int sum2, int sum3)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.TESLA_TRIANGLE.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(sum1);
        mplew.writeInt(sum2);
        mplew.writeInt(sum3);

        return mplew.getPacket();
    }

    public static byte[] mechPortal(Point pos)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MECH_PORTAL.getValue());
        mplew.writePos(pos);

        return mplew.getPacket();
    }

    public static byte[] spawnMechDoor(server.maps.MechDoor md, boolean animated)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MECH_DOOR_SPAWN.getValue());
        mplew.write(animated ? 0 : 1);
        mplew.writeInt(md.getOwnerId());
        mplew.writePos(md.getTruePosition());
        mplew.write(md.getId());
        mplew.writeInt(md.getPartyId());

        return mplew.getPacket();
    }

    public static byte[] removeMechDoor(server.maps.MechDoor md, boolean animated)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MECH_DOOR_REMOVE.getValue());
        mplew.write(animated ? 0 : 1);
        mplew.writeInt(md.getOwnerId());
        mplew.write(md.getId());

        return mplew.getPacket();
    }

    public static byte[] useSPReset(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SP_RESET.getValue());
        mplew.write(1);
        mplew.writeInt(chrId);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] useAPReset(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.AP_RESET.getValue());
        mplew.write(1);
        mplew.writeInt(chrId);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] playerDamaged(int chrId, int dmg)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PLAYER_DAMAGED.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(dmg);

        return mplew.getPacket();
    }

    public static byte[] pamsSongEffect(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PAMS_SONG.getValue());
        mplew.writeInt(chrId);

        return mplew.getPacket();
    }

    public static byte[] pamsSongUI()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PAMS_SONG.getValue());
        mplew.writeShort(0);

        return mplew.getPacket();
    }

    public static byte[] englishQuizMsg(String msg)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ENGLISH_QUIZ.getValue());
        mplew.writeInt(20);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] report(int err)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REPORT_RESULT.getValue());
        mplew.write(err);
        if (err == 2)
        {
            mplew.write(0);
            mplew.writeInt(1);
        }
        return mplew.getPacket();
    }

    public static byte[] sendLieDetector(byte[] image, int attempt)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0] + " 测谎仪图片大小: " + image.length + " 换图次数: " + (attempt - 1));
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LIE_DETECTOR.getValue());
        mplew.write(9);
        mplew.write(1);
        mplew.write(1);
        mplew.write(attempt - 1);
        if (image == null)
        {
            mplew.writeInt(0);
            return mplew.getPacket();
        }
        mplew.writeInt(image.length);
        mplew.write(image);

        return mplew.getPacket();
    }

    public static byte[] LieDetectorResponse(byte msg)
    {
        return LieDetectorResponse(msg, (byte) 0);
    }

    public static byte[] LieDetectorResponse(byte msg, byte msg2)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LIE_DETECTOR.getValue());


        mplew.write(msg);


        mplew.write(msg2);

        return mplew.getPacket();
    }

    public static byte[] enableReport()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(3);

        mplew.writeShort(SendPacketOpcode.ENABLE_REPORT.getValue());
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] reportResponse(byte mode, int remainingReports)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REPORT_RESPONSE.getValue());
        mplew.writeShort(mode);
        if (mode == 2)
        {
            mplew.write(1);
            mplew.writeInt(remainingReports);
        }

        return mplew.getPacket();
    }

    public static byte[] ultimateExplorer()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ULTIMATE_EXPLORER.getValue());

        return mplew.getPacket();
    }

    public static byte[] GMPoliceMessage()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GM_POLICE.getValue());
        mplew.writeInt(0);

        return mplew.getPacket();
    }

    public static byte[] pamSongUI()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PAM_SONG.getValue());

        return mplew.getPacket();
    }

    public static byte[] dragonBlink(int portalId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DRAGON_BLINK.getValue());
        mplew.write(portalId);

        return mplew.getPacket();
    }

    public static byte[] showTraitGain(client.MapleTraitType trait, int amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(17);
        mplew.writeLong(trait.getStat().getValue());
        mplew.writeInt(amount);

        return mplew.getPacket();
    }

    public static byte[] showTraitMaxed(client.MapleTraitType trait)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(18);
        mplew.writeLong(trait.getStat().getValue());

        return mplew.getPacket();
    }

    public static byte[] harvestMessage(int oid, int msg)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.HARVEST_MESSAGE.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(msg);

        return mplew.getPacket();
    }

    public static byte[] showHarvesting(int chrId, int tool)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_HARVEST.getValue());
        mplew.writeInt(chrId);
        mplew.write(tool > 0 ? 1 : 0);
        if (tool > 0)
        {
            mplew.writeInt(tool);
            mplew.writeInt(0);
        }

        return mplew.getPacket();
    }

    public static byte[] harvestResult(int chrId, boolean success)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.HARVESTED.getValue());
        mplew.writeInt(chrId);
        mplew.write(success ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] makeExtractor(int chrId, String cname, Point pos, int timeLeft, int itemId, int fee)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_EXTRACTOR.getValue());
        mplew.writeInt(chrId);
        mplew.writeMapleAsciiString(cname);
        mplew.writeInt(pos.x);
        mplew.writeInt(pos.y);
        mplew.writeShort(timeLeft);
        mplew.writeInt(itemId);
        mplew.writeInt(fee);

        return mplew.getPacket();
    }

    public static byte[] removeExtractor(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_EXTRACTOR.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(1);

        return mplew.getPacket();
    }

    public static byte[] spouseMessage(String msg, boolean white)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPOUSE_MESSAGE.getValue());
        mplew.writeShort(white ? 10 : 6);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] spouseMessage(int op, String msg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPOUSE_MESSAGE.getValue());


        mplew.writeShort(op);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] openBag(int index, int itemId, boolean firstTime)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_BAG.getValue());
        mplew.writeInt(index);
        mplew.writeInt(itemId);
        mplew.writeShort(firstTime ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] showOwnCraftingEffect(String effect, int time, int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(36);
        mplew.writeMapleAsciiString(effect);
        mplew.write(1);
        mplew.writeInt(time);
        mplew.writeInt(mode);

        return mplew.getPacket();
    }

    public static byte[] showCraftingEffect(int chrId, String effect, int time, int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(36);
        mplew.writeMapleAsciiString(effect);
        mplew.write(1);
        mplew.writeInt(time);
        mplew.writeInt(mode);

        return mplew.getPacket();
    }

    public static byte[] craftMake(int chrId, int something, int time)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CRAFT_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(something);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] craftFinished(int chrId, int craftID, int ranking, int itemId, int quantity, int exp)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CRAFT_COMPLETE.getValue());
        mplew.writeInt(chrId);
        mplew.writeInt(craftID);
        mplew.writeInt(ranking);


        if ((ranking == 21) || (ranking == 22) || (ranking == 23))
        {
            mplew.writeInt(itemId);
            mplew.writeInt(quantity);
        }
        mplew.writeInt(exp);

        return mplew.getPacket();
    }

    public static byte[] craftMessage(String msg)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CRAFT_MESSAGE.getValue());
        mplew.writeMapleAsciiString(msg);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] shopDiscount(int percent)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOP_DISCOUNT.getValue());
        mplew.write(percent);

        return mplew.getPacket();
    }

    public static byte[] changeCardSet(int set)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CARD_SET.getValue());
        mplew.writeInt(set);

        return mplew.getPacket();
    }

    public static byte[] getCard(int itemid, int level)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GET_CARD.getValue());
        mplew.write(itemid > 0 ? 1 : 0);
        if (itemid > 0)
        {
            mplew.writeInt(itemid);
            mplew.writeInt(level);
        }

        return mplew.getPacket();
    }

    public static byte[] upgradeBook(Item book, MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOOK_STATS.getValue());
        mplew.writeInt(book.getPosition());
        PacketHelper.addItemInfo(mplew, book, chr);

        return mplew.getPacket();
    }

    public static byte[] pendantSlot(boolean p)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PENDANT_SLOT.getValue());
        mplew.write(p ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] getMonsterBookInfo(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOOK_INFO.getValue());
        mplew.writeInt(chr.getId());
        mplew.writeInt(chr.getLevel());
        chr.getMonsterBook().writeCharInfoPacket(mplew);

        return mplew.getPacket();
    }

    public static byte[] getBuffBar(long millis)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BUFF_BAR.getValue());
        mplew.writeLong(millis);

        return mplew.getPacket();
    }

    public static byte[] showMidMsg(String s, int l)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MID_MSG.getValue());
        mplew.write(l);
        mplew.writeMapleAsciiString(s);
        mplew.write(s.length() > 0 ? 0 : 1);

        return mplew.getPacket();
    }

    public static byte[] showBackgroundEffect(String eff, int value)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.VISITOR.getValue());
        mplew.writeMapleAsciiString(eff);
        mplew.write(value);

        return mplew.getPacket();
    }

    public static byte[] updateGender(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_GENDER.getValue());
        mplew.write(chr.getGender());

        return mplew.getPacket();
    }

    public static byte[] registerFamiliar(MonsterFamiliar mf)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REGISTER_FAMILIAR.getValue());
        mplew.writeLong(mf.getId());
        mf.writeRegisterPacket(mplew, false);
        mplew.writeShort(mf.getVitality() >= 3 ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] touchFamiliar(int chrId, byte unk, int objectid, int type, int delay, int damage)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.TOUCH_FAMILIAR.getValue());
        mplew.writeInt(chrId);
        mplew.write(0);
        mplew.write(unk);
        mplew.writeInt(objectid);
        mplew.writeInt(type);
        mplew.writeInt(delay);
        mplew.writeInt(damage);

        return mplew.getPacket();
    }

    public static byte[] familiarAttack(int chrId, byte unk, List<Triple<Integer, Integer, List<Integer>>> attackPair)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ATTACK_FAMILIAR.getValue());
        mplew.writeInt(chrId);
        mplew.write(0);
        mplew.write(unk);
        mplew.write(attackPair.size());
        for (Triple<Integer, Integer, List<Integer>> s : attackPair)
        {
            mplew.writeInt(s.left);
            mplew.write(s.mid);
            mplew.write(s.right.size());
            for (Object o : s.right)
            {
                int damage = (Integer) o;
                mplew.writeInt(damage);
            }
        }
        Iterator localIterator2;
        return mplew.getPacket();
    }

    public static byte[] updateFamiliar(MonsterFamiliar mf)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_FAMILIAR.getValue());
        mplew.writeInt(mf.getCharacterId());
        mplew.writeInt(mf.getFamiliar());
        mplew.writeInt(mf.getFatigue());
        mplew.writeLong(PacketHelper.getTime(mf.getVitality() >= 3 ? System.currentTimeMillis() : -2L));

        return mplew.getPacket();
    }

    public static byte[] removeFamiliar(int chrId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_FAMILIAR.getValue());
        mplew.writeInt(chrId);
        mplew.writeShort(0);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] spawnFamiliar(MonsterFamiliar mf, boolean spawn)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_FAMILIAR.getValue());
        mplew.writeInt(mf.getCharacterId());
        mplew.writeShort(spawn ? 1 : 0);
        mplew.write(0);
        if (spawn)
        {
            mplew.writeInt(mf.getFamiliar());
            mplew.writeInt(mf.getFatigue());
            mplew.writeInt(mf.getVitality() * 300);
            mplew.writeMapleAsciiString(mf.getName());
            mplew.writePos(mf.getTruePosition());
            mplew.write(mf.getStance());
            mplew.writeShort(mf.getFh());
        }
        return mplew.getPacket();
    }

    public static byte[] moveFamiliar(int chrId, Point startPos, List<server.movement.LifeMovementFragment> moves)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_FAMILIAR.getValue());
        mplew.writeInt(chrId);
        mplew.write(0);
        mplew.writePos(startPos);
        mplew.writeInt(0);
        PacketHelper.serializeMovementList(mplew, moves);

        return mplew.getPacket();
    }

    public static byte[] achievementRatio(int amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ACHIEVEMENT_RATIO.getValue());
        mplew.writeInt(amount);

        return mplew.getPacket();
    }

    public static byte[] createUltimate(int amount)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.CREATE_ULTIMATE.getValue());
        mplew.writeInt(amount);

        return mplew.getPacket();
    }

    public static byte[] updateSpecialStat(String stat, int array, int mode, boolean unk, int chance)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.PROFESSION_INFO.getValue());
        mplew.writeMapleAsciiString(stat);
        mplew.writeInt(array);
        mplew.writeInt(mode);
        mplew.write(unk ? 1 : 0);
        mplew.writeInt(chance);

        return mplew.getPacket();
    }

    public static byte[] getQuickSlot(client.MapleQuickSlot quickslot)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.QUICK_SLOT.getValue());
        quickslot.writeData(mplew);

        return mplew.getPacket();
    }

    public static byte[] getFamiliarInfo(MapleCharacter chr)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FAMILIAR_INFO.getValue());
        mplew.writeInt(chr.getFamiliars().size());
        for (MonsterFamiliar mf : chr.getFamiliars().values())
        {
            mf.writeRegisterPacket(mplew, true);
        }
        List<Pair<Integer, Long>> size = new ArrayList<>();
        for (Item i : chr.getInventory(MapleInventoryType.USE).list())
        {
            if (i.getItemId() / 10000 == 287)
            {
                server.StructFamiliar f = server.MapleItemInformationProvider.getInstance().getFamiliarByItem(i.getItemId());
                if (f != null)
                {
                    size.add(new Pair(f.familiar, i.getInventoryId()));
                }
            }
        }
        mplew.writeInt(size.size());
        for (Pair<Integer, Long> s : size)
        {
            mplew.writeInt(chr.getId());
            mplew.writeInt(s.left);
            mplew.writeLong(s.right);
            mplew.write(0);
        }
        size.clear();
        return mplew.getPacket();
    }

    public static byte[] updateImp(client.inventory.MapleImp imp, int mask, int index, boolean login)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ITEM_POT.getValue());
        mplew.write(login ? 0 : 1);
        mplew.writeInt(index + 1);
        mplew.writeInt(mask);
        if ((mask & ImpFlag.SUMMONED.getValue()) != 0)
        {
            Pair<Integer, Integer> i = server.MapleItemInformationProvider.getInstance().getPot(imp.getItemId());
            if (i == null)
            {
                return enableActions();
            }
            mplew.writeInt(i.left);
            mplew.write(imp.getLevel());
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.STATE.getValue()) != 0))
        {
            mplew.write(imp.getState());
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.FULLNESS.getValue()) != 0))
        {
            mplew.writeInt(imp.getFullness());
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.CLOSENESS.getValue()) != 0))
        {
            mplew.writeInt(imp.getCloseness());
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.CLOSENESS_LEFT.getValue()) != 0))
        {
            mplew.writeInt(1);
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.MINUTES_LEFT.getValue()) != 0))
        {
            mplew.writeInt(0);
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.LEVEL.getValue()) != 0))
        {
            mplew.write(1);
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.FULLNESS_2.getValue()) != 0))
        {
            mplew.writeInt(imp.getFullness());
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.UPDATE_TIME.getValue()) != 0))
        {
            mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.CREATE_TIME.getValue()) != 0))
        {
            mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.AWAKE_TIME.getValue()) != 0))
        {
            mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.SLEEP_TIME.getValue()) != 0))
        {
            mplew.writeLong(PacketHelper.getTime(System.currentTimeMillis()));
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.MAX_CLOSENESS.getValue()) != 0))
        {
            mplew.writeInt(100);
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.MAX_DELAY.getValue()) != 0))
        {
            mplew.writeInt(1000);
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.MAX_FULLNESS.getValue()) != 0))
        {
            mplew.writeInt(1000);
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.MAX_ALIVE.getValue()) != 0))
        {
            mplew.writeInt(1);
        }
        if (((mask & ImpFlag.SUMMONED.getValue()) != 0) || ((mask & ImpFlag.MAX_MINUTES.getValue()) != 0))
        {
            mplew.writeInt(10);
        }
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] enableActions()
    {
        return updatePlayerStats(new EnumMap(client.MapleStat.class), true, null);
    }

    public static byte[] spawnFlags(List<Pair<String, Integer>> flags)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LOGIN_WELCOME.getValue());
        mplew.write(flags == null ? 0 : flags.size());
        if (flags != null)
        {
            for (Pair<String, Integer> f : flags)
            {
                mplew.writeMapleAsciiString(f.left);
                mplew.write(f.right);
            }
        }

        return mplew.getPacket();
    }

    public static byte[] getPVPScoreboard(List<Pair<Integer, MapleCharacter>> flags, int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_SCOREBOARD.getValue());
        mplew.writeShort(flags.size());
        for (Pair<Integer, MapleCharacter> f : flags)
        {
            mplew.writeInt(f.right.getId());
            mplew.writeMapleAsciiString(f.right.getName());
            mplew.writeInt(f.left);
            mplew.write(type == 0 ? 0 : f.right.getTeam() + 1);
        }

        return mplew.getPacket();
    }

    public static byte[] getPVPResult(List<Pair<Integer, MapleCharacter>> flags, int exp, int winningTeam, int playerTeam)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_RESULT.getValue());
        mplew.writeInt(flags.size());
        for (Pair<Integer, MapleCharacter> f : flags)
        {
            mplew.writeInt(f.right.getId());
            mplew.writeMapleAsciiString(f.right.getName());
            mplew.writeInt(f.left);
            mplew.writeShort(f.right.getTeam() + 1);
            if (GameConstants.GMS)
            {
                mplew.writeInt(0);
            }
        }
        mplew.writeZeroBytes(24);
        mplew.writeInt(exp);
        mplew.write(0);
        if (GameConstants.GMS)
        {
            mplew.writeShort(100);
            mplew.writeInt(0);
        }
        mplew.write(winningTeam);
        mplew.write(playerTeam);

        return mplew.getPacket();
    }

    public static byte[] showStatusMessage(String info, String data)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(22);
        mplew.writeMapleAsciiString(info);
        mplew.writeMapleAsciiString(data);

        return mplew.getPacket();
    }

    public static byte[] showOwnChampionEffect()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(32);
        mplew.writeInt(30000);

        return mplew.getPacket();
    }

    public static byte[] showChampionEffect(int from_playerid)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(from_playerid);
        mplew.write(32);
        mplew.writeInt(30000);

        return mplew.getPacket();
    }

    public static byte[] enablePVP(boolean enabled)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_ENABLED.getValue());
        mplew.write(enabled ? 1 : 2);

        return mplew.getPacket();
    }

    public static byte[] getPVPMode(int mode)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_MODE.getValue());
        mplew.write(mode);

        return mplew.getPacket();
    }

    public static byte[] getPVPType(int type, List<Pair<Integer, String>> players1, int team, boolean enabled, int lvl)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.PVP_TYPE.getValue());
        mplew.write(type);
        mplew.write(lvl);
        mplew.write(enabled ? 1 : 0);
        if (type > 0)
        {
            mplew.write(team);
            mplew.writeInt(players1.size());
            for (Pair<Integer, String> pl : players1)
            {
                mplew.writeInt(pl.left);
                mplew.writeMapleAsciiString(pl.right);
                mplew.writeShort(2660);
            }
        }

        return mplew.getPacket();
    }

    public static byte[] getPVPTeam(List<Pair<Integer, String>> players)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();


        mplew.writeShort(SendPacketOpcode.PVP_TEAM.getValue());
        mplew.writeInt(players.size());
        for (Pair<Integer, String> pl : players)
        {
            mplew.writeInt(pl.left);
            mplew.writeMapleAsciiString(pl.right);
            mplew.writeShort(2660);
        }

        return mplew.getPacket();
    }

    public static byte[] getPVPScore(int score, boolean kill)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_SCORE.getValue());
        mplew.writeInt(score);
        mplew.write(kill ? 1 : 0);

        return mplew.getPacket();
    }

    public static byte[] getPVPIceGage(int score)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_ICEGAGE.getValue());
        mplew.writeInt(score);

        return mplew.getPacket();
    }

    public static byte[] getPVPKilled(String lastWords)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_KILLED.getValue());
        mplew.writeMapleAsciiString(lastWords);

        return mplew.getPacket();
    }

    public static byte[] getPVPPoints(int p1, int p2)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_POINTS.getValue());

        mplew.writeInt(p1);
        mplew.writeInt(p2);

        return mplew.getPacket();
    }

    public static byte[] getPVPHPBar(int cid, int hp, int maxHp)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_HP.getValue());

        mplew.writeInt(cid);
        mplew.writeInt(hp);
        mplew.writeInt(maxHp);

        return mplew.getPacket();
    }

    public static byte[] getPVPIceHPBar(int hp, int maxHp)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_ICEKNIGHT.getValue());

        mplew.writeInt(hp);
        mplew.writeInt(maxHp);

        return mplew.getPacket();
    }

    public static byte[] getPVPMist(int cid, int mistSkill, int mistLevel, int damage)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_MIST.getValue());

        mplew.writeInt(cid);
        mplew.writeInt(mistSkill);
        mplew.write(mistLevel);
        mplew.writeInt(damage);
        mplew.write(8);
        mplew.writeInt(1000);

        return mplew.getPacket();
    }

    public static byte[] getCaptureFlags(MapleMap map)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CAPTURE_FLAGS.getValue());
        mplew.writeRect(map.getArea(0));
        mplew.writeInt(map.getGuardians().get(0).left.x);
        mplew.writeInt(map.getGuardians().get(0).left.y);
        mplew.writeRect(map.getArea(1));
        mplew.writeInt(map.getGuardians().get(1).left.x);
        mplew.writeInt(map.getGuardians().get(1).left.y);
        return mplew.getPacket();
    }

    public static byte[] getCapturePosition(MapleMap map)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        Point p1 = map.getPointOfItem(2910000);
        Point p2 = map.getPointOfItem(2910001);
        mplew.writeShort(SendPacketOpcode.CAPTURE_POSITION.getValue());
        mplew.write(p1 == null ? 0 : 1);
        if (p1 != null)
        {
            mplew.writeInt(p1.x);
            mplew.writeInt(p1.y);
        }
        mplew.write(p2 == null ? 0 : 1);
        if (p2 != null)
        {
            mplew.writeInt(p2.x);
            mplew.writeInt(p2.y);
        }

        return mplew.getPacket();
    }

    public static byte[] resetCapture()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CAPTURE_RESET.getValue());

        return mplew.getPacket();
    }

    public static byte[] pvpAttack(int cid, int playerLevel, int skill, int skillLevel, int speed, int mastery, int projectile, int attackCount, int chargeTime, int stance, int direction, int range
            , int linkSkill, int linkSkillLevel, boolean movementSkill, boolean pushTarget, boolean pullTarget, List<AttackPair> attack)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_ATTACK.getValue());
        mplew.writeInt(cid);
        mplew.write(playerLevel);
        mplew.writeInt(skill);
        mplew.write(skillLevel);
        mplew.writeInt(linkSkill != skill ? linkSkill : 0);
        mplew.write(linkSkillLevel != skillLevel ? linkSkillLevel : 0);
        mplew.write(direction);
        mplew.write(movementSkill ? 1 : 0);
        mplew.write(pushTarget ? 1 : 0);
        mplew.write(pullTarget ? 1 : 0);
        mplew.write(0);
        mplew.writeShort(stance);
        mplew.write(speed);
        mplew.write(mastery);
        mplew.writeInt(projectile);
        mplew.writeInt(chargeTime);
        mplew.writeInt(range);
        mplew.writeShort(attack.size());
        if (GameConstants.GMS)
        {
            mplew.writeInt(0);
        }
        mplew.write(attackCount);
        mplew.write(0);
        for (AttackPair p : attack)
        {
            mplew.writeInt(p.objectid);
            if (GameConstants.GMS)
            {
                mplew.writeInt(0);
            }
            mplew.writePos(p.point);
            mplew.writeZeroBytes(5);
            for (Pair<Integer, Boolean> atk : p.attack)
            {
                mplew.writeInt(atk.left);
                if (GameConstants.GMS)
                {
                    mplew.writeInt(0);
                }
                mplew.write(atk.right ? 1 : 0);
                mplew.writeShort(0);
            }
        }

        return mplew.getPacket();
    }

    public static byte[] pvpCool(int cid, List<Integer> attack)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_COOL.getValue());
        mplew.writeInt(cid);
        mplew.write(attack.size());
        for (Integer integer : attack)
        {
            int b = integer;
            mplew.writeInt(b);
        }
        return mplew.getPacket();
    }

    public static byte[] getPVPClock(int type, int time)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CLOCK.getValue());
        mplew.write(3);
        mplew.write(type);
        mplew.writeInt(time);

        return mplew.getPacket();
    }

    public static byte[] getPVPTransform(int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PVP_TRANSFORM.getValue());
        mplew.write(type);

        return mplew.getPacket();
    }

    public static byte[] changeTeam(int cid, int type)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LOAD_TEAM.getValue());
        mplew.writeInt(cid);
        mplew.write(type);

        return mplew.getPacket();
    }


    public static byte[] showQuickMove(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.QUICK_MOVE.getValue());
        List<server.maps.MapleQuickMove> quickMove = server.maps.MapleQuickMove.getQuickMove(chr.getMapId());
        mplew.write(quickMove.size());
        int i = 0;
        for (server.maps.MapleQuickMove map : quickMove)
        {
            mplew.writeShort(i);
            mplew.writeMapleAsciiString(map.name);
            mplew.writeInt(map.npcid);
            mplew.writeInt(map.type);
            mplew.writeInt(map.level);
            mplew.writeMapleAsciiString(map.desc);
            mplew.writeLong(PacketHelper.getTime(-2L));
            mplew.writeLong(PacketHelper.getTime(-1L));
            i++;
        }

        return mplew.getPacket();
    }


    public static byte[] showForce(MapleCharacter chr, int oid, int forceCount, int forceColor)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAIN_FORCE.getValue());
        mplew.write(1);
        mplew.writeInt(chr.getId());
        mplew.writeInt(oid);
        mplew.writeInt(0);
        mplew.write(1);
        mplew.writeInt(forceCount);


        mplew.writeInt(forceColor);
        mplew.writeInt(37);
        mplew.writeInt(6);
        mplew.writeInt(46);
        mplew.writeZeroBytes(13);

        return mplew.getPacket();
    }

    public static byte[] updateCardStack(int total)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_CARTE.getValue());
        mplew.write(total);

        return mplew.getPacket();
    }

    public static byte[] gainCardStack(MapleCharacter chr, int oid, int skillId, int forceCount, int color)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAIN_FORCE.getValue());
        mplew.write(0);
        mplew.writeInt(chr.getId());
        mplew.writeInt(1);
        mplew.write(1);
        mplew.writeInt(oid);
        mplew.writeInt(skillId);
        mplew.write(1);
        mplew.writeInt(forceCount);
        mplew.writeInt(color);
        mplew.writeInt(28);
        mplew.writeInt(7);
        mplew.writeInt(9);
        mplew.writeZeroBytes(13);

        return mplew.getPacket();
    }

    public static byte[] gainCardStack(MapleCharacter chr, int oid, int skillId, int forceCount, int color, int times)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAIN_FORCE.getValue());
        mplew.write(0);
        mplew.writeInt(chr.getId());


        mplew.writeInt(color);
        mplew.write(1);
        mplew.writeInt(1);
        mplew.writeInt(oid);
        mplew.writeInt(skillId);
        for (int i = 0; i < times; i++)
        {
            mplew.write(1);
            mplew.writeInt(forceCount + i);
            mplew.writeInt(skillId == 36001005 ? 0 : 2);
            mplew.writeInt(Randomizer.rand(15, 20));
            mplew.writeInt(Randomizer.rand(20, 30));
            mplew.writeInt(skillId == 36001005 ? Randomizer.rand(120, 150) : 0);
            mplew.writeInt(Randomizer.rand(300, 900));
            mplew.writeZeroBytes(8);
        }
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] gainAssassinStack(int chrId, int oid, int forceCount, boolean isAssassin, List<Integer> moboids, int visProjectile, Point posFrom)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAIN_FORCE.getValue());
        mplew.write(1);
        mplew.writeInt(chrId);
        mplew.writeInt(oid);
        mplew.writeInt(11);
        mplew.write(1);
        mplew.writeInt(moboids.size());
        for (Integer integer : moboids)
        {
            int moboid = integer;
            mplew.writeInt(moboid);
        }
        mplew.writeInt(isAssassin ? 4100012 : 4120019);
        for (int i = 0; i < moboids.size(); i++)
        {
            mplew.write(1);
            mplew.writeInt(forceCount + i);
            mplew.writeInt(isAssassin ? 1 : 2);
            mplew.writeInt(Randomizer.rand(32, 48));
            mplew.writeInt(Randomizer.rand(3, 4));
            mplew.writeInt(Randomizer.rand(100, 200));
            mplew.writeInt(200);
            mplew.writeZeroBytes(8);
        }
        mplew.write(0);
        mplew.writeInt(posFrom.x - 120);
        mplew.writeInt(posFrom.y - 100);
        mplew.writeInt(posFrom.x + 120);
        mplew.writeInt(posFrom.y + 100);
        mplew.writeInt(visProjectile);

        return mplew.getPacket();
    }


    public static byte[] showTempestBladesAttack(int chrId, int skillId, int forceCount, List<Integer> moboids, int attackCount)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAIN_FORCE.getValue());
        mplew.write(0);
        mplew.writeInt(chrId);
        mplew.writeInt(2);
        mplew.write(1);
        mplew.writeInt(moboids.size());
        for (Integer integer : moboids)
        {
            int moboid = integer;
            mplew.writeInt(moboid);
        }
        mplew.writeInt(skillId);

        for (int i = 0; i < attackCount; i++)
        {
            mplew.write(1);
            mplew.writeInt(forceCount + i + 1);
            mplew.writeInt(2);
            mplew.writeInt(Randomizer.rand(15, 20));
            mplew.writeInt(Randomizer.rand(20, 30));
            mplew.writeInt(0);
            mplew.writeInt(Randomizer.rand(1000, 1500));
            mplew.writeZeroBytes(8);
        }
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] showAegisSystemAttack(int chrId, int skillId, int forceCount, int moboid, int attackCount)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GAIN_FORCE.getValue());
        mplew.write(0);
        mplew.writeInt(chrId);
        mplew.writeInt(5);
        mplew.write(1);
        mplew.writeInt(moboid);
        mplew.writeInt(skillId);
        for (int i = 0; i < attackCount; i++)
        {
            mplew.write(1);
            mplew.writeInt(forceCount + i + 1);
            mplew.writeInt(0);
            mplew.writeInt(35);
            mplew.writeInt(5);
            mplew.writeInt(Randomizer.rand(80, 90));
            mplew.writeInt(Randomizer.rand(100, 500));
            mplew.writeZeroBytes(8);
        }
        mplew.write(0);

        return mplew.getPacket();
    }

    public static byte[] showQuestMessage(String msg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(11);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] sendloginSuccess()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LOGIN_SUCC.getValue());

        return mplew.getPacket();
    }

    public static byte[] showCharCash(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHAR_CASH.getValue());
        mplew.writeInt(chr.getId());
        mplew.writeInt(chr.getCSPoints(2));

        return mplew.getPacket();
    }

    public static byte[] showPlayerCash(MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_PLAYER_CASH.getValue());
        mplew.writeInt(chr.getCSPoints(1));
        mplew.writeInt(chr.getCSPoints(2));

        return mplew.getPacket();
    }

    public static byte[] playerCashUpdate(int mode, int toCharge, MapleCharacter chr)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.PLAYER_CASH_UPDATE.getValue());
        mplew.writeInt(mode);
        mplew.writeInt(toCharge == 1 ? chr.getCSPoints(1) : 0);
        mplew.writeInt(chr.getCSPoints(2));
        mplew.write(toCharge);
        mplew.writeShort(0);

        return mplew.getPacket();
    }

    public static byte[] sendTestPacket(String test)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.write(HexTool.getByteArrayFromHexString(test));
        return mplew.getPacket();
    }

    public static byte[] GainEXP_Monster(String testmsg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SHOW_STATUS_INFO.getValue());
        mplew.write(3);
        mplew.write(1);
        mplew.writeInt(1000);
        mplew.write(0);

        mplew.writeInt(0);
        mplew.writeShort(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.write(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);


        return mplew.getPacket();
    }


    public static byte[] giveCharacterSkill(int skillId, int toChrId, String toChrName)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.GIVE_CHARACTER_SKILL.getValue());
        mplew.writeInt(0);
        mplew.writeInt(skillId);
        mplew.writeInt(toChrId);
        mplew.writeMapleAsciiString(toChrName);

        return mplew.getPacket();
    }


    public static byte[] showDoJangRank()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MULUNG_DOJO_RANKING.getValue());
        mplew.writeInt(1);
        mplew.writeShort(1);
        mplew.writeMapleAsciiString("小心");
        mplew.writeLong(60L);

        return mplew.getPacket();
    }


    public static byte[] confirmCrossHunter(byte code)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CONFIRM_CROSS_HUNTER.getValue());


        mplew.write(code);

        return mplew.getPacket();
    }


    public static byte[] openWeb(String web)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.OPEN_WEB.getValue());
        mplew.writeMapleAsciiString(web);

        return mplew.getPacket();
    }


    public static byte[] updateInnerSkill(int skillId, int skillevel, byte position, byte rank)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_INNER_SKILL.getValue());
        mplew.write(1);
        mplew.write(1);
        mplew.writeShort(position);
        mplew.writeInt(skillId);
        mplew.writeShort(skillevel);
        mplew.writeShort(rank);
        mplew.write(1);

        return mplew.getPacket();
    }


    public static byte[] updateInnerStats(MapleCharacter chr, boolean levelup)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_INNER_STATS.getValue());
        mplew.writeInt(chr.getHonorLevel());
        mplew.writeInt(chr.getHonorExp());
        mplew.write(levelup ? 1 : 0);

        return mplew.getPacket();
    }


    public static byte[] sendPolice(String text)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MAPLE_ADMIN.getValue());
        mplew.writeMapleAsciiString(text);

        return mplew.getPacket();
    }


    public static byte[] 显示免费时空(int mf, int cs)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAY_OF_SHIKONG.getValue());
        mplew.writeInt(30 - mf);
        mplew.writeInt(cs);

        return mplew.getPacket();
    }


    public static byte[] 时空移动错误()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAY_OF_SHIKONG1.getValue());
        mplew.write(0);

        return mplew.getPacket();
    }


    public static byte[] showOwnJobChangedElf(String effect, int time, int itemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_ITEM_GAIN_INCHAT.getValue());
        mplew.write(36);
        mplew.writeMapleAsciiString(effect);
        mplew.write(1);
        mplew.writeInt(0);
        mplew.writeInt(time);
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }


    public static byte[] showJobChangedElf(int chrId, String effect, int time, int itemId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_FOREIGN_EFFECT.getValue());
        mplew.writeInt(chrId);
        mplew.write(36);
        mplew.writeMapleAsciiString(effect);
        mplew.write(1);
        mplew.writeInt(0);
        mplew.writeInt(time);
        mplew.writeInt(itemId);

        return mplew.getPacket();
    }

    public static byte[] testPacket(String testmsg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.write(HexTool.getByteArrayFromHexString(testmsg));

        return mplew.getPacket();
    }

    public static byte[] testPacket(byte[] testmsg)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.write(testmsg);

        return mplew.getPacket();
    }

    public static byte[] testPacket(String op, String text)
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.write(HexTool.getByteArrayFromHexString(op));
        mplew.writeMapleAsciiString(text);

        return mplew.getPacket();
    }


    public static byte[] 封印之瞳(MapleCharacter chr, List<Integer> memorySkills)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SKILL_MEMORY.getValue());
        mplew.write(1);
        mplew.writeInt(chr.getId());
        mplew.writeInt(4);
        mplew.writeInt(chr.getJob());
        mplew.writeInt(memorySkills.size());
        for (Integer memorySkill : memorySkills)
        {
            int i = memorySkill;
            mplew.writeInt(i);
        }

        return mplew.getPacket();
    }


    public static byte[] 幻影删除技能(int position)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_SKILL_TICK.getValue());
        mplew.write(1);
        mplew.write(3);
        if (position < 4)
        {
            mplew.writeInt(1);
            mplew.writeInt(position);
        }
        else if ((position >= 4) && (position < 8))
        {
            mplew.writeInt(2);
            mplew.writeInt(position - 4);
        }
        else if ((position >= 8) && (position < 11))
        {
            mplew.writeInt(3);
            mplew.writeInt(position - 8);
        }
        else if ((position >= 11) && (position < 13))
        {
            mplew.writeInt(4);
            mplew.writeInt(position - 11);
        }

        return mplew.getPacket();
    }

    public static byte[] 修改幻影装备技能(int skillId, int teachId)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.EQUIPPED_SKILL.getValue());
        mplew.write(1);
        mplew.write(1);
        mplew.writeInt(skillId);
        mplew.writeInt(teachId);

        return mplew.getPacket();
    }

    public static byte[] 幻影复制错误()
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_SKILL_TICK.getValue());
        mplew.write(1);
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] 幻影复制技能(int position, int skillId, int level)
    {
        if (ServerProperties.ShowPacket())
        {
            log.info("调用: " + new Throwable().getStackTrace()[0]);
        }
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UPDATE_SKILL_TICK.getValue());
        mplew.write(1);
        mplew.write(0);
        if (position < 4)
        {
            mplew.writeInt(1);
            mplew.writeInt(position);
        }
        else if ((position >= 4) && (position < 8))
        {
            mplew.writeInt(2);
            mplew.writeInt(position - 4);
        }
        else if ((position >= 8) && (position < 11))
        {
            mplew.writeInt(3);
            mplew.writeInt(position - 8);
        }
        else if ((position >= 11) && (position < 13))
        {
            mplew.writeInt(4);
            mplew.writeInt(position - 11);
        }
        mplew.writeInt(skillId);
        mplew.writeInt(level);
        mplew.writeInt(0);

        return mplew.getPacket();
    }


    public static byte[] sendUnkPacket1FC()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.UNKNOWN_1FC.getValue());
        mplew.write(1);

        return mplew.getPacket();
    }

    public static byte[] SystemProcess()
    {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SYSTEM_PROCESS_LIST.getValue());
        mplew.write(1);

        return mplew.getPacket();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\MaplePacketCreator.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */