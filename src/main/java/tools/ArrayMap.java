package tools;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class ArrayMap<K, V> extends AbstractMap<K, V> implements Serializable
{
    public static final long serialVersionUID = 9179541993413738569L;
    private final ArrayList<Entry<K, V>> list;
    private transient Set<? extends Map.Entry<K, V>> entries = null;

    public ArrayMap()
    {
        this.list = new ArrayList<>();
    }


    public ArrayMap(Map<K, V> map)
    {
        this.list = new ArrayList<>();
        putAll(map);
    }


    public ArrayMap(int initialCapacity)
    {
        this.list = new ArrayList(initialCapacity);
    }

    public V put(K key, V value)
    {
        int size = this.list.size();
        Entry<K, V> entry = null;
        int i;
        if (key == null)
        {
            for (i = 0; i < size; i++)
            {
                entry = this.list.get(i);
                if (entry.getKey() == null)
                {
                    break;
                }
            }
        }
        for (i = 0; i < size; i++)
        {
            entry = this.list.get(i);
            if (key.equals(entry.getKey()))
            {
                break;
            }
        }

        V oldValue = null;
        if (i < size)
        {
            oldValue = entry.getValue();
            entry.setValue(value);
        }
        else
        {
            this.list.add(new Entry(key, value));
        }
        return oldValue;
    }

    public Set<Map.Entry<K, V>> entrySet()
    {
        if (this.entries == null)
        {
            this.entries = new AbstractSet()
            {
                public Iterator<ArrayMap.Entry<K, V>> iterator()
                {
                    return ArrayMap.this.list.iterator();
                }

                public int size()
                {
                    return ArrayMap.this.list.size();
                }

                public void clear()
                {
                    throw new UnsupportedOperationException();
                }
            };
        }
        return (Set<Map.Entry<K, V>>) this.entries;
    }

    public static class Entry<K, V> implements Map.Entry<K, V>, Serializable
    {
        public static final long serialVersionUID = 9179541993413738569L;
        protected final K key;
        protected V value;

        public Entry(K key, V value)
        {
            this.key = key;
            this.value = value;
        }


        public K getKey()
        {
            return this.key;
        }


        public V getValue()
        {
            return this.value;
        }


        public V setValue(V newValue)
        {
            V oldValue = this.value;
            this.value = newValue;
            return oldValue;
        }

        public int hashCode()
        {
            int keyHash = this.key == null ? 0 : this.key.hashCode();
            int valueHash = this.value == null ? 0 : this.value.hashCode();
            return keyHash ^ valueHash;
        }

        public boolean equals(Object o)
        {
            if (!(o instanceof Map.Entry))
            {
                return false;
            }
            Map.Entry e = (Map.Entry) o;
            return (this.key == null ? e.getKey() == null : this.key.equals(e.getKey())) && (this.value == null ? e.getValue() == null : this.value.equals(e.getValue()));
        }

        public String toString()
        {
            return this.key + "=" + this.value;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\ArrayMap.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */