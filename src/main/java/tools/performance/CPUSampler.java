package tools.performance;

import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class CPUSampler
{
    private static final CPUSampler instance = new CPUSampler();
    private final List<String> included = new LinkedList<>();
    private final Map<StackTrace, Integer> recorded = new HashMap<>();
    private long interval = 5L;
    private SamplerThread sampler = null;
    private int totalSamples = 0;

    public static CPUSampler getInstance()
    {
        return instance;
    }

    public void setInterval(long millis)
    {
        this.interval = millis;
    }

    public void addIncluded(String include)
    {
        for (String alreadyIncluded : this.included)
        {
            if (include.startsWith(alreadyIncluded))
            {
                return;
            }
        }
        this.included.add(include);
    }

    public void reset()
    {
        this.recorded.clear();
        this.totalSamples = 0;
    }

    public void start()
    {
        if (this.sampler == null)
        {
            this.sampler = new SamplerThread();
            this.sampler.start();
        }
    }

    public void stop()
    {
        if (this.sampler != null)
        {
            this.sampler.stop();
            this.sampler = null;
        }
    }

    public void save(Writer writer, int minInvocations, int topMethods) throws java.io.IOException
    {
        SampledStacktraces topConsumers = getTopConsumers();
        StringBuilder builder = new StringBuilder();
        builder.append("Top Methods:\r\n");
        for (int i = 0; (i < topMethods) && (i < topConsumers.getTopConsumers().size()); i++)
        {
            builder.append(topConsumers.getTopConsumers().get(i).toString(topConsumers.getTotalInvocations(), 1));
        }
        builder.append("\r\nStack Traces:\r\n");
        writer.write(builder.toString());
        writer.write(topConsumers.toString(minInvocations));
        writer.flush();
    }

    public SampledStacktraces getTopConsumers()
    {
        List<StacktraceWithCount> ret = new ArrayList<>();
        java.util.Set<Map.Entry<StackTrace, Integer>> entrySet = this.recorded.entrySet();
        for (Map.Entry<StackTrace, Integer> entry : entrySet)
        {
            ret.add(new StacktraceWithCount(entry.getValue(), entry.getKey()));
        }
        java.util.Collections.sort(ret);
        return new SampledStacktraces(ret, this.totalSamples);
    }

    private void consumeStackTraces(Map<Thread, StackTraceElement[]> traces)
    {
        for (Map.Entry<Thread, StackTraceElement[]> trace : traces.entrySet())
        {
            int relevant = findRelevantElement(trace.getValue());
            if (relevant != -1)
            {
                StackTrace st = new StackTrace(trace.getValue(), relevant, trace.getKey().getState());
                Integer i = this.recorded.get(st);
                this.totalSamples += 1;
                if (i == null)
                {
                    this.recorded.put(st, 1);
                }
                else
                {
                    this.recorded.put(st, i.intValue() + 1);
                }
            }
        }
    }

    private int findRelevantElement(StackTraceElement[] trace)
    {
        if (trace.length == 0) return -1;
        if (this.included.isEmpty())
        {
            return 0;
        }
        int firstIncluded = -1;
        for (String myIncluded : this.included)
        {
            for (int i = 0; i < trace.length; i++)
            {
                StackTraceElement ste = trace[i];
                if ((ste.getClassName().startsWith(myIncluded)) && ((i < firstIncluded) || (firstIncluded == -1)))
                {
                    firstIncluded = i;
                    break;
                }
            }
        }

        if ((firstIncluded >= 0) && (trace[firstIncluded].getClassName().equals("tools.performance.CPUSampler$SamplerThread")))
        {
            return -1;
        }
        return firstIncluded;
    }

    private static class StackTrace
    {
        private final StackTraceElement[] trace;
        private final Thread.State state;

        public StackTrace(StackTraceElement[] trace, int startAt, Thread.State state)
        {
            this.state = state;
            if (startAt == 0)
            {
                this.trace = trace;
            }
            else
            {
                this.trace = new StackTraceElement[trace.length - startAt];
                System.arraycopy(trace, startAt, this.trace, 0, this.trace.length);
            }
        }

        public int hashCode()
        {
            int ret = 13 * this.trace.length + this.state.hashCode();
            for (StackTraceElement ste : this.trace)
            {
                ret ^= ste.hashCode();
            }
            return ret;
        }

        public boolean equals(Object obj)
        {
            if (!(obj instanceof StackTrace))
            {
                return false;
            }
            StackTrace other = (StackTrace) obj;
            if (other.trace.length != this.trace.length)
            {
                return false;
            }
            if (other.state != this.state)
            {
                return false;
            }
            for (int i = 0; i < this.trace.length; i++)
            {
                if (!this.trace[i].equals(other.trace[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public String toString()
        {
            return toString(-1);
        }

        public String toString(int traceLength)
        {
            StringBuilder ret = new StringBuilder("State: ");
            ret.append(this.state.name());
            if (traceLength > 1)
            {
                ret.append("\r\n");
            }
            else
            {
                ret.append(" ");
            }
            int i = 0;
            for (StackTraceElement ste : this.trace)
            {
                i++;
                if (i > traceLength)
                {
                    break;
                }
                ret.append(ste.getClassName());
                ret.append("#");
                ret.append(ste.getMethodName());
                ret.append(" (Line: ");
                ret.append(ste.getLineNumber());
                ret.append(")\r\n");
            }
            return ret.toString();
        }

        public StackTraceElement[] getTrace()
        {
            return this.trace;
        }
    }

    public static class StacktraceWithCount implements Comparable<StacktraceWithCount>
    {
        private final int count;
        private final CPUSampler.StackTrace trace;

        public StacktraceWithCount(int count, CPUSampler.StackTrace trace)
        {
            this.count = count;
            this.trace = trace;
        }

        public int getCount()
        {
            return this.count;
        }

        public StackTraceElement[] getTrace()
        {
            return this.trace.getTrace();
        }

        public int compareTo(StacktraceWithCount o)
        {
            return -Integer.valueOf(this.count).compareTo(o.count);
        }

        public boolean equals(Object oth)
        {
            if (!(oth instanceof StacktraceWithCount))
            {
                return false;
            }
            StacktraceWithCount o = (StacktraceWithCount) oth;
            return this.count == o.count;
        }

        public String toString()
        {
            return this.count + " Sampled Invocations\r\n" + this.trace.toString();
        }

        public String toString(int totalInvoations, int traceLength)
        {
            return this.count + "/" + totalInvoations + " Sampled Invocations (" + getPercentage(totalInvoations) + "%) " + this.trace.toString(traceLength);
        }

        private double getPercentage(int total)
        {
            return Math.round(this.count / total * 10000.0D) / 100.0D;
        }
    }

    public static class SampledStacktraces
    {
        final List<CPUSampler.StacktraceWithCount> topConsumers;
        final int totalInvocations;

        public SampledStacktraces(List<CPUSampler.StacktraceWithCount> topConsumers, int totalInvocations)
        {
            this.topConsumers = topConsumers;
            this.totalInvocations = totalInvocations;
        }

        public List<CPUSampler.StacktraceWithCount> getTopConsumers()
        {
            return this.topConsumers;
        }

        public int getTotalInvocations()
        {
            return this.totalInvocations;
        }

        public String toString()
        {
            return toString(0);
        }

        public String toString(int minInvocation)
        {
            StringBuilder ret = new StringBuilder();
            for (CPUSampler.StacktraceWithCount swc : this.topConsumers)
            {
                if (swc.getCount() >= minInvocation)
                {
                    ret.append(swc.toString(this.totalInvocations, Integer.MAX_VALUE));
                    ret.append("\r\n");
                }
            }
            return ret.toString();
        }
    }

    private class SamplerThread implements Runnable
    {
        private boolean running = false;
        private boolean shouldRun = false;
        private Thread rthread;

        private SamplerThread()
        {
        }

        public void start()
        {
            if (!this.running)
            {
                this.shouldRun = true;
                this.rthread = new Thread(this, "CPU Sampling Thread");
                this.rthread.start();
                this.running = true;
            }
        }

        public void stop()
        {
            this.shouldRun = false;
            this.rthread.interrupt();
            try
            {
                this.rthread.join();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        public void run()
        {
            while (this.shouldRun)
            {
                CPUSampler.this.consumeStackTraces(Thread.getAllStackTraces());
                try
                {
                    Thread.sleep(CPUSampler.this.interval);
                }
                catch (InterruptedException ignored)
                {
                }
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\performance\CPUSampler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */