package tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FileoutputUtil
{
    public static final String Acc_Stuck = "log\\AccountStuck.log";
    public static final String EXCEPTION_CAUGHT = "log\\exceptionCaught.txt";
    public static final String Login_Error = "log\\Login_Error.log";
    public static final String PacketLog = "log\\PacketLog.log";
    public static final String SkillsLog = "log\\SkillsLog.log";
    public static final String SkillBuff = "log\\SkillBuffLog.log";
    public static final String AttackLog = "log\\AttackLog.log";
    public static final String ClientError = "log\\ClientError.log";
    public static final String PlayerSkills = "log\\PlayerSkills.log";
    public static final String Zakum_Log = "log\\Log_Zakum.log";
    public static final String Horntail_Log = "log\\Log_Horntail.log";
    public static final String Pinkbean_Log = "log\\Pinkbean.log";
    public static final String PacketEx_Log = "log\\Packet_Except.log";
    public static final String Donator_Log = "log\\Donator.log";
    public static final String Hacker_Log = "log\\Hacker.log";
    public static final String Movement_Log = "log\\Movement.log";
    public static final String SpecialMove_log = "log\\SpecialMove.log";
    public static final String 掉血错误 = "log\\掉血错误.log";
    public static final String 攻击出错 = "log\\攻击出错.log";
    public static final String 封包出错 = "log\\封包出错.log";
    public static final String 数据异常 = "log\\数据异常.log";
    public static final String 复制装备 = "log\\复制装备.log";
    public static final String 宠物说话 = "log\\宠物说话.log";
    public static final String 在线统计 = "在线统计.txt";
    public static final String CommandEx_Log = "log\\Command_Except.log";
    public static final String ScriptEx_Log = "log\\Script\\Script_Except.log";
    public static final String Event_ScriptEx_Log = "log\\Script\\Event_Script_Except.log";
    public static final String Item_ScriptEx_Log = "log\\Script\\Item_Script_Except.log";
    public static final String Map_ScriptEx_Log = "log\\Script\\Map_Script_Except.log";
    public static final String Portal_ScriptEx_Log = "log\\Script\\Portal_Script_Except.log";
    public static final String Reactor_ScriptEx_Log = "log\\Script\\Reactor_Script_Except.log";
    public static final String Quest_ScriptEx_Log = "log\\Script\\Quest_Script_Except.log";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat sdfGMT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat sdf_ = new SimpleDateFormat("yyyy-MM-dd");
    private static final String FILE_PATH = "error/" + sdf_.format(Calendar.getInstance().getTime()) + "/";

    static
    {
        sdfGMT.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
    }

    public static void printError(String name, String msg)
    {
        FileOutputStream out = null;
        String file = FILE_PATH + name;
        try
        {
            File outputFile = new File(file);
            if (outputFile.getParentFile() != null)
            {
                outputFile.getParentFile().mkdirs();
            }
            out = new FileOutputStream(file, true);
            out.write(msg.getBytes());
            String time = "\r\n------------------------ " + CurrentReadable_Time() + " ------------------------\r\n";
            out.write(time.getBytes());
            return;
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignored)
            {
            }
        }
    }

    public static String CurrentReadable_Time()
    {
        return sdf.format(Calendar.getInstance().getTime());
    }

    public static void packetLog(String file, String msg)
    {
        FileOutputStream out = null;
        try
        {
            File outputFile = new File(file);
            if (outputFile.getParentFile() != null)
            {
                outputFile.getParentFile().mkdirs();
            }
            out = new FileOutputStream(file, true);
            out.write(msg.getBytes());
            out.write("\r\n\r\n".getBytes());
            return;
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignored)
            {
            }
        }
    }

    public static void log(String file, String msg)
    {
        log(file, msg, false);
    }

    public static void log(String file, String msg, boolean A)
    {
        FileOutputStream out = null;
        try
        {
            File outputFile = new File(file);
            if (outputFile.getParentFile() != null)
            {
                outputFile.getParentFile().mkdirs();
            }
            out = new FileOutputStream(file, true);
            out.write(msg.getBytes());
            String rn = "\r\n------------------------ " + CurrentReadable_Time() + " ------------------------\r\n";
            out.write(rn.getBytes());
            return;
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignored)
            {
            }
        }
    }

    public static void hiredMerchLog(String file, String msg)
    {
        String newfile = "log\\HiredMerch\\" + file + ".txt";
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(newfile, true);
            out.write(("[" + CurrentReadable_Time() + "] ").getBytes());
            out.write(msg.getBytes());
            out.write("\r\n".getBytes());
            return;
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignored)
            {
            }
        }
    }

    public static void outputFileError(String file, Throwable t)
    {
        outputFileError(file, t, null);
    }

    public static void outputFileError(String file, Throwable t, String info)
    {
        FileOutputStream out = null;
        try
        {
            File outputFile = new File(file);
            if (outputFile.getParentFile() != null)
            {
                outputFile.getParentFile().mkdirs();
            }
            out = new FileOutputStream(file, true);
            out.write(("\r\n------------------------ " + CurrentReadable_Time() + " ------------------------\r\n").getBytes());
            if (info != null)
            {
                out.write((info + "\r\n").getBytes());
            }
            out.write(getString(t).getBytes());
            return;
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignored)
            {
            }
        }
    }

    public static String getString(Throwable e)
    {
        java.io.StringWriter sw = null;
        java.io.PrintWriter pw = null;
        try
        {
            sw = new java.io.StringWriter();
            pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            return sw.toString();
        }
        finally
        {
            try
            {
                if (pw != null)
                {
                    pw.close();
                }
                if (sw != null)
                {
                    sw.close();
                }
            }
            catch (IOException ignored)
            {
            }
        }
    }

    public static String CurrentReadable_Date()
    {
        return sdf_.format(Calendar.getInstance().getTime());
    }

    public static String CurrentReadable_TimeGMT()
    {
        return sdfGMT.format(new Date());
    }

    public static void printError(String file, Throwable t, String info)
    {
        FileOutputStream out = null;
        try
        {
            File outputFile = new File(file);
            if (outputFile.getParentFile() != null)
            {
                outputFile.getParentFile().mkdirs();
            }
            out = new FileOutputStream(file, true);
            out.write((info + "\r\n").getBytes());
            out.write(getString(t).getBytes());
            out.write("\r\n---------------------------------\r\n".getBytes());
            return;
        }
        catch (IOException ignored)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException ignored)
            {
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\FileoutputUtil.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */