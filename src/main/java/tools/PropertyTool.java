package tools;

import java.util.Properties;


public class PropertyTool
{
    private Properties props = new Properties();

    public PropertyTool(Properties props)
    {
        this.props = props;
    }

    public byte getSettingByte(String key)
    {
        return getSettingByte(key, (byte) -1);
    }

    public byte getSettingByte(String key, byte def)
    {
        String property = this.props.getProperty(key);
        if (property != null)
        {
            return Byte.parseByte(property);
        }
        return def;
    }

    public short getSettingShort(String key)
    {
        return getSettingShort(key, (short) -1);
    }

    public short getSettingShort(String key, short def)
    {
        String property = this.props.getProperty(key);
        if (property != null)
        {
            return Short.parseShort(property);
        }
        return def;
    }

    public int getSettingInt(String key)
    {
        return getSettingInt(key, -1);
    }

    public int getSettingInt(String key, int def)
    {
        String property = this.props.getProperty(key);
        if (property != null)
        {
            return Integer.parseInt(property);
        }
        return def;
    }

    public long getSettingLong(String key)
    {
        return getSettingLong(key, -1L);
    }

    public long getSettingLong(String key, long def)
    {
        String property = this.props.getProperty(key);
        if (property != null)
        {
            return Long.parseLong(property);
        }
        return def;
    }

    public String getSettingStr(String key)
    {
        return getSettingStr(key, null);
    }

    public String getSettingStr(String key, String def)
    {
        String property = this.props.getProperty(key);
        if (property != null)
        {
            return property;
        }
        return def;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\PropertyTool.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */