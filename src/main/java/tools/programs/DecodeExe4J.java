package tools.programs;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class DecodeExe4J
{
    public static void main(String[] args) throws IOException
    {
        FileInputStream fin = new FileInputStream(args[0]);
        FileOutputStream fout = new FileOutputStream(args[1]);
        BufferedInputStream bin = new BufferedInputStream(fin);
        BufferedOutputStream bout = new BufferedOutputStream(fout);
        int in = 0;
        for (; ; )
        {
            in = bin.read();
            if (in == -1)
            {
                break;
            }
            in ^= 0x88;
            bout.write(in);
        }
        bin.close();
        fin.close();
        bout.close();
        fout.close();
    }
}