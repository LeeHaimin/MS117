package tools.programs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import tools.FileoutputUtil;


public class QuestTest
{
    public static void main(String[] args) throws IOException
    {
        MapleDataProvider quest = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/Quest.wz"));
        MapleData checkz = quest.getData("Check.img");
        MapleData actz = quest.getData("Act.img");
        MapleData infoz = quest.getData("QuestInfo.img");
        MapleData pinfoz = quest.getData("PQuest.img");
        List<String> RequirementType = new ArrayList<>();
        List<String> ActionType = new ArrayList<>();

        for (MapleData qz : checkz.getChildren())
        {
            int id = Integer.parseInt(qz.getName());
            MapleData req;
            for (int i = 0; i < 2; i++)
            {
                MapleData reqData = qz.getChildByPath(String.valueOf(i));
                Iterator localIterator2;
                if (reqData != null)
                {
                    for (localIterator2 = reqData.getChildren().iterator(); localIterator2.hasNext(); )
                    {
                        req = (MapleData) localIterator2.next();
                        if (!RequirementType.contains(req.getName().toLowerCase()))
                        {
                            RequirementType.add(req.getName().toLowerCase());
                        }
                    }
                }
                MapleData actData = actz.getChildByPath(id + "/" + i);
                if (actData != null)
                {
                    for (MapleData act : actData.getChildren())
                    {
                        if (!ActionType.contains(act.getName().toLowerCase()))
                        {
                            ActionType.add(act.getName().toLowerCase());
                        }
                    }
                }
            }
        }
        Collections.sort(RequirementType);
        for (String i : RequirementType)
        {
            FileoutputUtil.log("RequirementType.txt", i, true);
        }
        Collections.sort(ActionType);
        for (String i : ActionType)
        {
            FileoutputUtil.log("ActionType.txt", i, true);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\programs\QuestTest.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */