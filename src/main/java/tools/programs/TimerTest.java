package tools.programs;

import java.io.IOException;
import java.util.Calendar;

import server.Timer;


public class TimerTest
{
    public static void main(String[] args) throws IOException
    {
        System.out.println("启动定时器...");
        Timer.EventTimer.getInstance().start();
        long oneDay = 86400000L;

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(1);
        int month = calendar.get(2);
        int day = calendar.get(5);

        calendar.set(year, month, day, 17, 15, 0);
        long initDelay = calendar.getTimeInMillis() - System.currentTimeMillis();
        System.out.println("下次执行的时间间隔: " + initDelay);
        initDelay = initDelay > 0L ? initDelay : oneDay + initDelay;
        System.out.println("最终下次执行的时间间隔: " + initDelay);


        Timer.EventTimer.getInstance().register(new EchoServer(), oneDay, initDelay);
    }

    public static class EchoServer implements Runnable
    {
        public void run()
        {
            System.out.println("This is a echo server. The current time is " + System.currentTimeMillis() + ".");
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\programs\TimerTest.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */