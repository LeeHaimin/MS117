package tools;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleJob;
import constants.GameConstants;
import database.DatabaseConnection;
import server.Randomizer;

public class test
{
    public static void main(String[] args) throws IOException
    {
        String in = "落叶";
        String name = "落叶无痕";
        System.err.println("是否包含: " + name.toLowerCase().indexOf(in.toLowerCase()));
        System.err.println("测试: 11200");
        System.err.println("10000: " + GameConstants.getJobNumber(10000));
        System.err.println("10100: " + GameConstants.getJobNumber(10100));
        System.err.println("10110: " + GameConstants.getJobNumber(10110));
        System.err.println("10111: " + GameConstants.getJobNumber(10111));
        System.err.println("10112: " + GameConstants.getJobNumber(10112));
        for (MapleJob list : MapleJob.values())
        {
            int k = list.getId();
        }

        System.err.println("测试: 0");
        System.err.println("-----------------------------------------");
        System.err.println("长度: " + "00-00-00-00-00-00".length());
        String dateString = "201309291830";
        System.err.println("时间: " + DateUtil.getStringToTime(dateString));
        System.err.println("当前: " + System.currentTimeMillis());
        int[] bytes = new int[6];
        bytes[0] = 16;
        bytes[1] = 191;
        bytes[2] = 72;
        bytes[3] = 123;
        bytes[4] = 16;
        bytes[5] = 139;
        StringBuilder mac = new StringBuilder();
        for (int aByte : bytes)
        {
            mac.append(StringUtil.getLeftPaddedStr(Integer.toHexString(aByte).toUpperCase(), '0', 2));
            mac.append("-");
        }
        String sp = mac.toString();
        System.err.println("MAC: " + sp.substring(0, sp.length() - 1));

        String text = "以下道具不能重复装备。\\r\\n\\r\\n-------&amp;lt;不能重复装备的道具列表&gt;------\\r\\n新月戒指\\r\\n半月戒指";
        text = text.replace("&amp;lt;", "<");
        text = text.replace("&gt;", ">");
        System.err.println("地图: " + text);
    }

    public static int getZeroWeapon(int level)
    {
        int weapon = 1562000;
        if (level < 110)
        {
            return weapon;
        }
        if (level < 170)
        {
            weapon += level % 100 / 10;
        }
        else
        {
            weapon += 7;
        }
        return weapon;
    }

    public static void initTime()
    {
        System.err.println("地图: 1662");
        String a = "123466488advaeesd";
        System.err.println("测试 " + DateUtil.getNowTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒");
        String ly_time = sdf.format(new Date());
        System.out.println("现在时间是 - 方法1: " + ly_time);
        String ly_time2 = new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒").format(Calendar.getInstance().getTime());
        System.out.println("现在时间是 - 方法2: " + ly_time2);
    }

    public static void init1()
    {
        List<Integer> Ids = new ArrayList<>();
        PreparedStatement ps;
        ResultSet rs;
        Connection con = DatabaseConnection.getConnection();
        int reactorid;
        try
        {
            ps = con.prepareStatement("SELECT * FROM reactordrops WHERE reactorid > 0");
            rs = ps.executeQuery();
            while (rs.next())
            {
                reactorid = rs.getInt("reactorid");
                if (!Ids.contains(reactorid))
                {

                    Ids.add(reactorid);
                }
            }
            rs.close();
            ps.close();
            java.util.Collections.sort(Ids);
            outputWithLogging("INSERT INTO reactordrops (`reactorid`, `itemid`, `chance`, `questid`) VALUES");
            for (Integer rid : Ids)
            {
                ps = con.prepareStatement("SELECT * FROM reactordrops WHERE reactorid = ?");
                ps.setInt(1, rid);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    outputWithLogging("(" + rid + ", " + rs.getInt("itemid") + ", " + rs.getInt("chance") + ", " + rs.getInt("questid") + "),");
                }
                rs.close();
                ps.close();
            }
        }
        catch (SQLException ignored)
        {
        }
    }

    public static void outputWithLogging(String buff)
    {
        String file = "reactordrops.sql";
        FileoutputUtil.log(file, buff, true);
    }

    public static void init()
    {
        System.err.println("测试 0.5");
        List<Integer> availableSN = new LinkedList<>();
        availableSN.add(20000485);
        availableSN.add(20000486);
        availableSN.add(20000487);
        availableSN.add(20000488);
        availableSN.add(20000489);
        availableSN.add(20000490);
        availableSN.add(20000491);

        String time1 = new SimpleDateFormat("yy-MM-dd-HH-mm").format(new Date()).replace("-", "");

        System.err.println("时间: " + Integer.valueOf(time1));
        System.err.println("时间: " + DateUtil.getTime(1352714698000L));
        System.err.println("白天使 - 1 1004");
        int i = 0;
        short prop;
        for (int x = 0; x < 30; x++)
        {
            prop = (short) Randomizer.rand(1, 5);
            System.err.println("点数: " + prop + " - " + x);
            System.err.println("随机 " + availableSN.get(Randomizer.nextInt(availableSN.size())));
            if (prop >= 4)
            {
                i++;
            }
        }
        System.err.println("成功次数: " + i);

        Map<Integer, Integer> totemEquip = new HashMap<>();
        totemEquip.put(4, 30000);
        totemEquip.put(2, 20000);
        totemEquip.put(0, 10000);
        for (Map.Entry<Integer, Integer> entry : totemEquip.entrySet())
        {
            System.err.println("数组 - > " + entry.getKey() + " " + entry.getValue());
        }

        long time = System.currentTimeMillis() + 28800000L;
        Timestamp currentVipTime = new Timestamp(time);
        System.err.println(currentVipTime);
        System.err.println(time);

        Calendar cal = Calendar.getInstance();
        cal.set(10, 2);
        cal.set(12, 22);
        cal.set(13, 22);
        System.err.println(cal.getTime());
        long nextTime = cal.getTimeInMillis();
        nextTime += 8520000L;
        currentVipTime = new Timestamp(nextTime);
        System.err.println(currentVipTime);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\test.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */