package tools.data.input;

import java.io.IOException;
import java.io.InputStream;


public class InputStreamByteStream implements ByteInputStream
{
    private final InputStream is;
    private long read = 0L;


    public InputStreamByteStream(InputStream is)
    {
        this.is = is;
    }


    public int readByte()
    {
        try
        {
            int temp = this.is.read();
            if (temp == -1)
            {
                throw new RuntimeException("EOF");
            }
            this.read += 1L;
            return temp;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }


    public long getBytesRead()
    {
        return this.read;
    }


    public long available()
    {
        try
        {
            return this.is.available();
        }
        catch (IOException e)
        {
            System.err.println("ERROR" + e);
        }
        return 0L;
    }


    public String toString(boolean b)
    {
        return toString();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\input\InputStreamByteStream.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */