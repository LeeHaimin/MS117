package tools.data.input;

import java.awt.Point;

public interface LittleEndianAccessor
{
    byte readByte();

    int readByteAsInt();

    char readChar();

    short readShort();

    int readUShort();

    int readInt();

    long readLong();

    void skip(int paramInt);

    byte[] read(int paramInt);

    float readFloat();

    double readDouble();

    String readAsciiString(int paramInt);

    String readMapleAsciiString();

    Point readPos();

    long getBytesRead();

    long available();

    String toString(boolean paramBoolean);
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\input\LittleEndianAccessor.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */