package tools.data.input;

import java.io.IOException;


public class GenericSeekableLittleEndianAccessor extends GenericLittleEndianAccessor implements SeekableLittleEndianAccessor
{
    private final SeekableInputStreamBytestream bs;

    public GenericSeekableLittleEndianAccessor(SeekableInputStreamBytestream bs)
    {
        super(bs);
        this.bs = bs;
    }

    public void skip(int num)
    {
        seek(getPosition() + num);
    }

    public void seek(long offset)
    {
        try
        {
            this.bs.seek(offset);
        }
        catch (IOException e)
        {
            System.err.println("Seek failed" + e);
        }
    }

    public long getPosition()
    {
        try
        {
            return this.bs.getPosition();
        }
        catch (IOException e)
        {
            System.err.println("getPosition failed" + e);
        }
        return -1L;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\input\GenericSeekableLittleEndianAccessor.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */