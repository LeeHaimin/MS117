package tools.data.input;

import java.io.IOException;

public interface SeekableInputStreamBytestream extends ByteInputStream
{
    void seek(long paramLong) throws IOException;

    long getPosition() throws IOException;

    String toString(boolean paramBoolean);
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\input\SeekableInputStreamBytestream.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */