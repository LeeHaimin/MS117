package tools.data.input;

import java.io.IOException;

import tools.HexTool;


public class ByteArrayByteStream implements SeekableInputStreamBytestream
{
    private final byte[] arr;
    private int pos = 0;
    private long bytesRead = 0L;


    public ByteArrayByteStream(byte[] arr)
    {
        this.arr = arr;
    }

    public void seek(long offset) throws IOException
    {
        this.pos = ((int) offset);
    }

    public long getPosition()
    {
        return this.pos;
    }

    public String toString(boolean b)
    {
        String nows = "";
        if (this.arr.length - this.pos > 0)
        {
            byte[] now = new byte[this.arr.length - this.pos];
            System.arraycopy(this.arr, this.pos, now, 0, this.arr.length - this.pos);
            nows = HexTool.toString(now);
        }
        if (b)
        {
            return "\r\n所有: " + HexTool.toString(this.arr) + "\r\n现在: " + nows;
        }
        return "\r\n封包: " + nows;
    }

    public int readByte()
    {
        this.bytesRead += 1L;
        return this.arr[(this.pos++)] & 0xFF;
    }

    public long getBytesRead()
    {
        return this.bytesRead;
    }

    public long available()
    {
        return this.arr.length - this.pos;
    }

    public String toString()
    {
        return toString(false);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\input\ByteArrayByteStream.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */