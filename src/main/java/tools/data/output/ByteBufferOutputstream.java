package tools.data.output;

import org.apache.mina.core.buffer.IoBuffer;


public class ByteBufferOutputstream implements ByteOutputStream
{
    private final IoBuffer bb;

    public ByteBufferOutputstream(IoBuffer bb)
    {
        this.bb = bb;
    }


    public void writeByte(byte b)
    {
        this.bb.put(b);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\output\ByteBufferOutputstream.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */