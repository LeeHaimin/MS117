package tools.data.output;

import org.apache.mina.core.buffer.IoBuffer;


public class ByteBufferLittleEndianWriter extends GenericLittleEndianWriter
{
    private final IoBuffer bb;

    public ByteBufferLittleEndianWriter()
    {
        this(50, true);
    }


    public ByteBufferLittleEndianWriter(int initialSize, boolean autoExpand)
    {
        this.bb = IoBuffer.allocate(initialSize);
        this.bb.setAutoExpand(autoExpand);
        setByteOutputStream(new ByteBufferOutputstream(this.bb));
    }


    public ByteBufferLittleEndianWriter(int size)
    {
        this(size, false);
    }

    public IoBuffer getFlippedBB()
    {
        return this.bb.flip();
    }


    public IoBuffer getByteBuffer()
    {
        return this.bb;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\output\ByteBufferLittleEndianWriter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */