package tools.data.output;

import java.io.ByteArrayOutputStream;


public class BAOSByteOutputStream implements ByteOutputStream
{
    private final ByteArrayOutputStream baos;

    public BAOSByteOutputStream(ByteArrayOutputStream baos)
    {
        this.baos = baos;
    }


    public void writeByte(byte b)
    {
        this.baos.write(b);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\output\BAOSByteOutputStream.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */