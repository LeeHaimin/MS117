package tools.data.output;

import java.awt.Point;
import java.awt.Rectangle;

public interface LittleEndianWriter
{
    void writeZeroBytes(int paramInt);

    void write(byte[] paramArrayOfByte);

    void write(byte paramByte);

    void write(int paramInt);

    void writeInt(int paramInt);

    void writeReversedInt(long paramLong);

    void writeShort(short paramShort);

    void writeShort(int paramInt);

    void writeLong(long paramLong);

    void writeReversedLong(long paramLong);

    void writeAsciiString(String paramString);

    void writeAsciiString(String paramString, int paramInt);

    void writeMapleNameString(String paramString);

    void writePos(Point paramPoint);

    void writeRect(Rectangle paramRectangle);

    void writeMapleAsciiString(String paramString);

    void writeBool(boolean paramBoolean);
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\data\output\LittleEndianWriter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */