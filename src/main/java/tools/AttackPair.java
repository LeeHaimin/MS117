package tools;

import java.util.List;

public class AttackPair
{
    public final int objectid;
    public final List<Pair<Integer, Boolean>> attack;
    public java.awt.Point point;

    public AttackPair(int objectid, List<Pair<Integer, Boolean>> attack)
    {
        this.objectid = objectid;
        this.attack = attack;
    }

    public AttackPair(int objectid, java.awt.Point point, List<Pair<Integer, Boolean>> attack)
    {
        this.objectid = objectid;
        this.point = point;
        this.attack = attack;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\AttackPair.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */