package tools;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import tools.api.Console;


public class MapleLog
{
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
    private final Lock logLock = this.lock.readLock();
    private final int Red = 12;
    private final int Pink = 13;
    private final int Yellow = 14;
    private final int White = 15;
    private int Green = 10;
    private int Blue = 11;

    public static MapleLog getInstance()
    {
        return SingletonHolder.instance;
    }


    public void logInfo(String msg)
    {
        logWrite(this.Yellow, msg);
    }

    public void logWrite(int color, String msg)
    {
        this.logLock.lock();

        try
        {
            Console.setColor(color);
            System.out.println(msg);
            Console.setColor(this.White);
        }
        finally
        {
            this.logLock.unlock();
        }

    }

    public void logWarn(String msg)
    {
        logWrite(this.Red, msg);
    }

    public void logError(String msg)
    {
        logWrite(this.Pink, msg);
    }

    private static class SingletonHolder
    {
        protected static final MapleLog instance = new MapleLog();
    }
}