package tools;

import java.io.Serializable;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class ConcurrentEnumMap<K extends Enum<K>, V> extends EnumMap<K, V> implements Serializable
{
    private static final long serialVersionUID = 11920818021L;
    private final ReentrantReadWriteLock reentlock = new ReentrantReadWriteLock();
    private final Lock rL = this.reentlock.readLock();
    private final Lock wL = this.reentlock.writeLock();

    public ConcurrentEnumMap(Class<K> keyType)
    {
        super(keyType);
    }

    public int size()
    {
        this.rL.lock();
        try
        {
            return super.size();
        }
        finally
        {
            this.rL.unlock();
        }
    }

    public boolean containsValue(Object value)
    {
        this.rL.lock();
        try
        {
            return super.containsValue(value);
        }
        finally
        {
            this.rL.unlock();
        }
    }

    public boolean containsKey(Object key)
    {
        this.rL.lock();
        try
        {
            return super.containsKey(key);
        }
        finally
        {
            this.rL.unlock();
        }
    }

    public V get(Object key)
    {
        this.rL.lock();
        try
        {
            return super.get(key);
        }
        finally
        {
            this.rL.unlock();
        }
    }

    public V put(K key, V value)
    {
        this.wL.lock();
        try
        {
            return super.put(key, value);
        }
        finally
        {
            this.wL.unlock();
        }
    }

    public V remove(Object key)
    {
        this.wL.lock();
        try
        {
            return super.remove(key);
        }
        finally
        {
            this.wL.unlock();
        }
    }

    public void putAll(java.util.Map<? extends K, ? extends V> m)
    {
        wL.lock();
        try
        {
            super.putAll(m);
        }
        finally
        {
            wL.unlock();
        }
    }

    public void clear()
    {
        wL.lock();
        try
        {
            super.clear();
        }
        finally
        {
            wL.unlock();
        }
    }

    public Set<K> keySet()
    {
        this.rL.lock();
        try
        {
            return super.keySet();
        }
        finally
        {
            this.rL.unlock();
        }
    }

    public Collection<V> values()
    {
        this.rL.lock();
        try
        {
            return super.values();
        }
        finally
        {
            this.rL.unlock();
        }
    }

    public Set<Map.Entry<K, V>> entrySet()
    {
        this.rL.lock();
        try
        {
            return super.entrySet();
        }
        finally
        {
            this.rL.unlock();
        }
    }

    public boolean equals(Object o)
    {
        return super.equals(o);
    }

    public EnumMap<K, V> clone()
    {
        return super.clone();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\ConcurrentEnumMap.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */