package tools.api;

import com.sun.jna.Structure;

public class PCONSOLE_SCREEN_BUFFER_INFO extends Structure
{
    public COORD dwSize;
    public COORD dwCursorPosition;
    public short wAttributes;
    public SMALL_RECT srWindow;
    public COORD dwMaximumWindowSize;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\api\PCONSOLE_SCREEN_BUFFER_INFO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */