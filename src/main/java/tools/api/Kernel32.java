package tools.api;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;


public interface Kernel32 extends StdCallLibrary
{
    Kernel32 INSTANCE = Native.loadLibrary("Kernel32", Kernel32.class);

    int GetStdHandle(int paramInt);

    boolean SetConsoleTextAttribute(int paramInt1, int paramInt2);

    boolean GetConsoleScreenBufferInfo(int paramInt, PCONSOLE_SCREEN_BUFFER_INFO paramPCONSOLE_SCREEN_BUFFER_INFO);

    boolean FillConsoleOutputCharacterA(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt);

    boolean FillConsoleOutputAttribute(int paramInt1, short paramShort, int paramInt2, int paramInt3, int[] paramArrayOfInt);

    boolean SetConsoleCursorPosition(int paramInt1, int paramInt2);

    boolean SetConsoleTitleA(String paramString);

    void CloseHandle(int paramInt);
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\tools\api\Kernel32.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */