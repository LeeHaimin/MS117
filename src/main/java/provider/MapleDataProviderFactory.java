package provider;

import java.io.File;

public class MapleDataProviderFactory
{
    public static MapleDataProvider getDataProvider(File in)
    {
//        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");
        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");
        return getWZ(in);
    }

    private static MapleDataProvider getWZ(Object in)
    {
        if ((in instanceof File))
        {
            return new MapleDataProvider((File) in);
        }
        throw new IllegalArgumentException("Can't create data provider for input " + in);
    }

    public static File fileInWZPath(String filename)
    {
        System.setProperty("wzpath", "D:\\MapleStory\\Project\\MS117\\WZ");//公司
        String wzPath = System.getProperty("wzpath");
        return new File(wzPath, filename);
    }
}