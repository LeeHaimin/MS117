package provider;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MapleData implements MapleDataEntity, Iterable<MapleData>
{
    private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    private Node node;
    private File imageDataDir;

    private MapleData(Node node)
    {
        this.node = node;
    }

    public MapleData(FileInputStream fis, File imageDataDir)
    {
        try
        {
            this.node = documentBuilderFactory.newDocumentBuilder().parse(fis).getFirstChild();
        }
        catch (ParserConfigurationException e)
        {
            throw new RuntimeException(e);
        }
        catch (SAXException e)
        {
            throw new RuntimeException(e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        this.imageDataDir = imageDataDir;
    }

    public MapleData getChildByPath(String path)
    {
        String[] segments = path.split("/");
        if (segments[0].equals(".."))
        {
            return ((MapleData) getParent()).getChildByPath(path.substring(path.indexOf("/") + 1));
        }

        Node myNode = this.node;
        for (String segment : segments)
        {
            NodeList childNodes = myNode.getChildNodes();
            boolean foundChild = false;
            for (int i = 0; i < childNodes.getLength(); i++)
            {
                Node childNode = childNodes.item(i);
                if ((childNode != null) && (childNode.getAttributes() != null) && (childNode.getAttributes().getNamedItem("name") != null) && (childNode.getNodeType() == 1) && (childNode.getAttributes().getNamedItem("name").getNodeValue().equals(segment)))
                {
                    myNode = childNode;
                    foundChild = true;
                    break;
                }
            }
            if (!foundChild)
            {
                return null;
            }
        }
        MapleData ret = new MapleData(myNode);
        ret.imageDataDir = new File(this.imageDataDir, getName() + "/" + path).getParentFile();
        return ret;
    }

    public Object getData()
    {
        NamedNodeMap attributes = this.node.getAttributes();
        MapleDataType type = getType();
        switch (type)
        {
            case DOUBLE:
                return Double.parseDouble(attributes.getNamedItem("value").getNodeValue());

            case FLOAT:
                return Float.parseFloat(attributes.getNamedItem("value").getNodeValue());

            case INT:
                return Integer.parseInt(attributes.getNamedItem("value").getNodeValue());

            case SHORT:
                return Short.parseShort(attributes.getNamedItem("value").getNodeValue());

            case STRING:
            case UOL:
                return attributes.getNamedItem("value").getNodeValue();

            case VECTOR:
                return new Point(Integer.parseInt(attributes.getNamedItem("x").getNodeValue()), Integer.parseInt(attributes.getNamedItem("y").getNodeValue()));

            case CANVAS:
                return new MapleCanvas(Integer.parseInt(attributes.getNamedItem("width").getNodeValue()), Integer.parseInt(attributes.getNamedItem("height").getNodeValue()),
                        new File(this.imageDataDir, getName() + ".png"));
        }

        return null;
    }

    public MapleDataType getType()
    {
        String nodeName = this.node.getNodeName();
        if (nodeName.equals("imgdir")) return MapleDataType.PROPERTY;
        if (nodeName.equals("canvas")) return MapleDataType.CANVAS;
        if (nodeName.equals("convex")) return MapleDataType.CONVEX;
        if (nodeName.equals("sound")) return MapleDataType.SOUND;
        if (nodeName.equals("uol")) return MapleDataType.UOL;
        if (nodeName.equals("double")) return MapleDataType.DOUBLE;
        if (nodeName.equals("float")) return MapleDataType.FLOAT;
        if (nodeName.equals("int")) return MapleDataType.INT;
        if (nodeName.equals("short")) return MapleDataType.SHORT;
        if (nodeName.equals("string")) return MapleDataType.STRING;
        if (nodeName.equals("vector")) return MapleDataType.VECTOR;
        if (nodeName.equals("null"))
        {
            return MapleDataType.IMG_0x00;
        }
        return null;
    }

    public String getName()
    {
        return this.node.getAttributes().getNamedItem("name").getNodeValue();
    }

    public MapleDataEntity getParent()
    {
        Node parentNode = this.node.getParentNode();
        if (parentNode.getNodeType() == 9)
        {
            return null;
        }
        MapleData parentData = new MapleData(parentNode);
        parentData.imageDataDir = this.imageDataDir.getParentFile();
        return parentData;
    }

    public Iterator<MapleData> iterator()
    {
        return getChildren().iterator();
    }

    public List<MapleData> getChildren()
    {
        List<MapleData> ret = new ArrayList<>();
        NodeList childNodes = this.node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++)
        {
            Node childNode = childNodes.item(i);
            if ((childNode != null) && (childNode.getNodeType() == 1))
            {
                MapleData child = new MapleData(childNode);
                child.imageDataDir = new File(this.imageDataDir, getName());
                ret.add(child);
            }
        }
        return ret;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\provider\MapleData.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */