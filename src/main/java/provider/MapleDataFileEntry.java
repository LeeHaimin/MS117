package provider;

public class MapleDataFileEntry extends MapleDataEntry
{
    private int offset;

    public MapleDataFileEntry(String name, int size, int checksum, MapleDataEntity parent)
    {
        super(name, size, checksum, parent);
    }

    public int getOffset()
    {
        return this.offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\provider\MapleDataFileEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */