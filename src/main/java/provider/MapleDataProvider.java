package provider;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MapleDataProvider
{
    private final File root;
    private final MapleDataDirectoryEntry rootForNavigation;

    public MapleDataProvider(File fileIn)
    {
        this.root = fileIn;
        this.rootForNavigation = new MapleDataDirectoryEntry(fileIn.getName(), 0, 0, null);
        fillMapleDataEntitys(this.root, this.rootForNavigation);
    }

    private void fillMapleDataEntitys(File lroot, MapleDataDirectoryEntry wzdir)
    {
        for (File file : lroot.listFiles())
        {
            String fileName = file.getName();
            if ((file.isDirectory()) && (!fileName.endsWith(".img")))
            {
                MapleDataDirectoryEntry newDir = new MapleDataDirectoryEntry(fileName, 0, 0, wzdir);
                wzdir.addDirectory(newDir);
                fillMapleDataEntitys(file, newDir);
            }
            else if (fileName.endsWith(".xml"))
            {
                wzdir.addFile(new MapleDataFileEntry(fileName.substring(0, fileName.length() - 4), 0, 0, wzdir));
            }
        }
    }

    public MapleData getData(String path)
    {
        File dataFile = new File(this.root, path + ".xml");
        File imageDataDir = new File(this.root, path);
        FileInputStream fis;
        try
        {
            fis = new FileInputStream(dataFile);
        }
        catch (java.io.FileNotFoundException e)
        {
            throw new RuntimeException("Datafile " + path + " does not exist in " + this.root.getAbsolutePath());
        }
        try
        {
            return new MapleData(fis, imageDataDir.getParentFile());
        }
        finally
        {
            try
            {
                fis.close();
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public MapleDataDirectoryEntry getRoot()
    {
        return this.rootForNavigation;
    }
}
