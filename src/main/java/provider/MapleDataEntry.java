package provider;


public class MapleDataEntry implements MapleDataEntity
{
    private final String name;

    private final int size;

    private final int checksum;
    private final MapleDataEntity parent;
    private int offset;

    public MapleDataEntry(String name, int size, int checksum, MapleDataEntity parent)
    {
        this.name = name;
        this.size = size;
        this.checksum = checksum;
        this.parent = parent;
    }

    public String getName()
    {
        return this.name;
    }

    public MapleDataEntity getParent()
    {
        return this.parent;
    }

    public int getSize()
    {
        return this.size;
    }

    public int getChecksum()
    {
        return this.checksum;
    }

    public int getOffset()
    {
        return this.offset;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\provider\MapleDataEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */