package provider;

public enum MapleDataType
{
    NONE, IMG_0x00, SHORT, INT, FLOAT, DOUBLE, STRING, EXTENDED, PROPERTY, CANVAS, VECTOR, CONVEX, SOUND, UOL, UNKNOWN_TYPE, UNKNOWN_EXTENDED_TYPE;

    MapleDataType()
    {
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\provider\MapleDataType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */