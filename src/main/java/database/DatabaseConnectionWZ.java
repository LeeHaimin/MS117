//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package database;

import com.mysql.jdbc.Statement;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

public class DatabaseConnectionWZ
{
    public static final int CLOSE_CURRENT_RESULT = 1;
    public static final int KEEP_CURRENT_RESULT = 2;
    public static final int CLOSE_ALL_RESULTS = 3;
    public static final int SUCCESS_NO_INFO = -2;
    public static final int EXECUTE_FAILED = -3;
    public static final int RETURN_GENERATED_KEYS = 1;
    public static final int NO_GENERATED_KEYS = 2;
    private static final HashMap<Integer, DatabaseConnectionWZ.ConWrapper> connections = new HashMap<>();
    private static final Properties dbProps = new Properties();
    private static String dbDriver;
    private static String dbUrl;
    private static String dbUser;
    private static String dbPass;
    private static boolean propsInited = false;
    private static long connectionTimeOut = 300000L;

    private DatabaseConnectionWZ()
    {
    }

    public static Connection getConnection()
    {
        Thread cThread = Thread.currentThread();
        int threadID = (int) cThread.getId();
        DatabaseConnectionWZ.ConWrapper ret = connections.get(threadID);
        if (ret == null)
        {
            Connection retCon = connectToDB();
            ret = new DatabaseConnectionWZ.ConWrapper(retCon);
            ret.id = threadID;
            connections.put(threadID, ret);
        }

        return ret.getConnection();
    }

    private static Connection connectToDB()
    {
        if (!propsInited)
        {
            try
            {
                FileReader fR = new FileReader("config/db.properties");
                dbProps.load(fR);
                fR.close();
            }
            catch (IOException var6)
            {
                System.out.println("[WZDB信息] Unable to start server: Error reading from db.properties.");
            }

            dbDriver = dbProps.getProperty("driver");
            dbUrl = dbProps.getProperty("wzdburl");
            dbUser = dbProps.getProperty("user");
            dbPass = dbProps.getProperty("password");

            try
            {
                connectionTimeOut = Long.parseLong(dbProps.getProperty("timeout"));
            }
            catch (Exception var5)
            {
                System.out.println("[WZDB信息] Cannot read Timeout Information, using default: " + connectionTimeOut + " ");
            }
        }

        try
        {
            Class.forName(dbDriver);
        }
        catch (ClassNotFoundException var4)
        {
            System.out.println("[WZDB信息] Could not locate the JDBC mysql driver.");
        }

        try
        {
            Connection con = DriverManager.getConnection(dbUrl, dbUser, dbPass);
            if (!propsInited)
            {
                long timeout = getWaitTimeout(con);
                if (timeout != -1L)
                {
                    connectionTimeOut = timeout;
                }

                propsInited = true;
            }

            return con;
        }
        catch (SQLException var3)
        {
            throw new DatabaseException(var3);
        }
    }

    private static long getWaitTimeout(Connection con)
    {
        Statement stmt = null;
        ResultSet rs = null;

        long var4;
        try
        {
            stmt = (Statement) con.createStatement();
            rs = stmt.executeQuery("SHOW VARIABLES LIKE 'wait_timeout'");
            long var3;
            if (rs.next())
            {
                var3 = Math.max(1000, rs.getInt(2) * 1000 - 1000);
                return var3;
            }

            var3 = -1L;
            return var3;
        }
        catch (SQLException var109)
        {
            var4 = -1L;
        }
        finally
        {
            if (stmt != null)
            {
                try
                {
                    stmt.close();
                }
                catch (SQLException ignored)
                {
                }
                finally
                {
                    if (rs != null)
                    {
                        try
                        {
                            rs.close();
                        }
                        catch (SQLException ignored)
                        {
                        }
                    }

                }
            }

        }

        return var4;
    }

    public static void closeAll() throws SQLException
    {

        for (ConWrapper con : connections.values())
        {
            con.connection.close();
        }

        connections.clear();
    }

    public static class ConWrapper
    {
        private long lastAccessTime = 0L;
        private Connection connection;
        private int id;

        public ConWrapper(Connection con)
        {
            this.connection = con;
        }

        public Connection getConnection()
        {
            if (this.expiredConnection())
            {
                System.out.println("[WZDB信息] 连接 " + this.id + " 已经超时.重新连接...");

                try
                {
                    this.connection.close();
                }
                catch (Throwable ignored)
                {
                }

                this.connection = DatabaseConnectionWZ.connectToDB();
            }

            this.lastAccessTime = System.currentTimeMillis();
            return this.connection;
        }

        public boolean expiredConnection()
        {
            if (this.lastAccessTime == 0L)
            {
                return false;
            }
            else
            {
                try
                {
                    return System.currentTimeMillis() - this.lastAccessTime >= DatabaseConnectionWZ.connectionTimeOut || this.connection.isClosed();
                }
                catch (Throwable var2)
                {
                    return true;
                }
            }
        }
    }
}
