package server.life;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import client.inventory.MapleInventoryType;
import constants.ItemConstants;
import database.DatabaseConnection;
import provider.MapleData;
import provider.MapleDataProvider;
import server.MapleItemInformationProvider;
import server.StructFamiliar;

public class MapleMonsterInformationProvider
{
    private static final MapleMonsterInformationProvider instance = new MapleMonsterInformationProvider();
    private static final MapleDataProvider stringDataWZ = provider.MapleDataProviderFactory.getDataProvider(new java.io.File(System.getProperty("wzpath") + "/String.wz"));
    private static final MapleData mobStringData = stringDataWZ.getData("MonsterBook.img");
    private final Map<Integer, ArrayList<MonsterDropEntry>> drops = new HashMap<>();
    private final List<MonsterGlobalDropEntry> globaldrops = new ArrayList<>();

    public static MapleMonsterInformationProvider getInstance()
    {
        return instance;
    }

    public List<MonsterGlobalDropEntry> getGlobalDrop()
    {
        return this.globaldrops;
    }

    public ArrayList<MonsterDropEntry> retrieveDrop(int monsterId)
    {
        return this.drops.get(monsterId);
    }

    public void clearDrops()
    {
        this.drops.clear();
        this.globaldrops.clear();
        load();
        addExtra();
    }

    public void load()
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            Connection con = DatabaseConnection.getConnection();
            ps = con.prepareStatement("SELECT * FROM drop_data_global WHERE chance > 0");
            rs = ps.executeQuery();

            while (rs.next())
            {
                this.globaldrops.add(new MonsterGlobalDropEntry(rs

                        .getInt("itemid"), rs.getInt("chance"), rs.getInt("continent"), rs.getByte("dropType"), rs.getInt("minimum_quantity"), rs.getInt("maximum_quantity"), rs.getInt("questid")));
            }
            rs.close();
            ps.close();

            ps = con.prepareStatement("SELECT dropperid FROM drop_data");
            List<Integer> mobIds = new ArrayList<>();
            rs = ps.executeQuery();
            while (rs.next())
            {
                int mobId = rs.getInt("dropperid");
                if (!mobIds.contains(mobId))
                {
                    loadDrop(mobId);
                    mobIds.add(mobId);
                }
            }
            return;
        }
        catch (SQLException e)
        {
            System.err.println("Error retrieving drop" + e);
        }
        finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (rs != null)
                {
                    rs.close();
                }
            }
            catch (SQLException ignored)
            {
            }
        }
    }

    public void addExtra()
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        for (Map.Entry<Integer, ArrayList<MonsterDropEntry>> e : this.drops.entrySet())
        {
            for (int i = 0; i < e.getValue().size(); i++)
            {
                if ((((MonsterDropEntry) ((ArrayList) e.getValue()).get(i)).itemId != 0) && (!ii.itemExists(((MonsterDropEntry) ((ArrayList) e.getValue()).get(i)).itemId)))
                {
                    ((ArrayList) e.getValue()).remove(i);
                }
            }
            MapleMonsterStats mons = MapleLifeFactory.getMonsterStats(e.getKey());
            Integer item = ii.getItemIdByMob(e.getKey());
            if ((item != null) && (item > 0))
            {
                if (item / 10000 != 238)
                {

                    e.getValue().add(new MonsterDropEntry(item, mons.isBoss() ? 1000000 : 10000, 1, 1, 0));
                }
            }
            else
            {
                StructFamiliar f = ii.getFamiliarByMob(e.getKey());
                if (f != null) if (f.itemid / 10000 != 238)
                {

                    e.getValue().add(new MonsterDropEntry(f.itemid, mons.isBoss() ? 10000 : 100, 1, 1, 0));
                }
            }
        }
        for (Map.Entry<Integer, Integer> i : ii.getMonsterBook().entrySet())
            if (!this.drops.containsKey(i.getKey()))
            {
                MapleMonsterStats mons = MapleLifeFactory.getMonsterStats(i.getKey());
                ArrayList<MonsterDropEntry> e = new ArrayList<>();
                if (i.getValue() / 10000 != 238)
                {

                    e.add(new MonsterDropEntry(i.getValue(), mons.isBoss() ? 1000000 : 10000, 1, 1, 0));
                    StructFamiliar f = ii.getFamiliarByMob(i.getKey());
                    if (f != null)
                    {
                        if (f.itemid / 10000 != 238)
                        {

                            e.add(new MonsterDropEntry(f.itemid, mons.isBoss() ? 10000 : 100, 1, 1, 0));
                        }
                    }
                    else
                    {
                        addMeso(mons, e);
                        this.drops.put(i.getKey(), e);
                    }
                }
            }
        for (StructFamiliar f : ii.getFamiliars().values())
        {
            if (!this.drops.containsKey(f.mob))
            {
                MapleMonsterStats mons = MapleLifeFactory.getMonsterStats(f.mob);
                ArrayList<MonsterDropEntry> e = new ArrayList<>();
                if (f.itemid / 10000 != 238)
                {

                    e.add(new MonsterDropEntry(f.itemid, mons.isBoss() ? 10000 : 100, 1, 1, 0));
                    addMeso(mons, e);
                    this.drops.put(f.mob, e);
                }
            }
        }

        MapleMonsterStats mons;
        for (Map.Entry<Integer, ArrayList<MonsterDropEntry>> integerArrayListEntry : this.drops.entrySet())
        {
            Map.Entry e = integerArrayListEntry;
            if (((Integer) e.getKey() != 9400408) && (mobStringData.getChildByPath(String.valueOf(e.getKey())) != null))
            {
                for (MapleData d : mobStringData.getChildByPath(e.getKey() + "/reward"))
                {
                    int toAdd = provider.MapleDataTool.getInt(d, 0);
                    if ((toAdd > 0) && (!contains((ArrayList) e.getValue(), toAdd)) && (ii.itemExists(toAdd)))
                        if ((toAdd / 10000 != 238) && (toAdd / 10000 != 243) && (toAdd / 10000 != 399) && (toAdd != 4001126) && (toAdd != 4001128) && (toAdd != 4001246) && (toAdd != 4001473) && (toAdd != 4001447) && (toAdd != 2022450) && (toAdd != 2022451) && (toAdd != 2022452) && (toAdd != 4032302) && (toAdd != 4032303) && (toAdd != 4032304))
                        {
                            ((ArrayList) e.getValue()).add(new MonsterDropEntry(toAdd, chanceLogic(toAdd), 1, 1, 0));
                        }
                }
            }
        }
        Map.Entry<Integer, ArrayList<MonsterDropEntry>> e;
    }

    private void loadDrop(int monsterId)
    {
        ArrayList<MonsterDropEntry> ret = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            MapleMonsterStats mons = MapleLifeFactory.getMonsterStats(monsterId);
            if (mons == null)
            {
                return;
            }
            ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM drop_data WHERE dropperid = ?");
            ps.setInt(1, monsterId);
            rs = ps.executeQuery();


            boolean doneMesos = false;
            while (rs.next())
            {
                int itemid = rs.getInt("itemid");
                int chance = rs.getInt("chance");
                if (itemid / 10000 != 238)
                {

                    ret.add(new MonsterDropEntry(itemid, chance, rs


                            .getInt("minimum_quantity"), rs.getInt("maximum_quantity"), rs.getInt("questid")));
                    if (itemid == 0) doneMesos = true;
                }
            }
            if (!doneMesos)
            {
                addMeso(mons, ret);
            }


            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (rs != null)
                {
                    rs.close();
                }
            }
            catch (SQLException ignore)
            {
                return;
            }
            this.drops.put(monsterId, ret);
        }
        catch (SQLException e)
        {
            System.err.println("Error retrieving drop" + e);
        }
        finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (rs != null)
                {
                    rs.close();
                }
            }
            catch (SQLException ignore)
            {
            }
        }
    }

    public void addMeso(MapleMonsterStats mons, ArrayList<MonsterDropEntry> ret)
    {
        double divided = mons.getLevel() < 100 ? 10.0D : mons.getLevel() < 10 ? mons.getLevel() : mons.getLevel() / 10.0D;
        int maxMeso = mons.getLevel() * (int) Math.ceil(mons.getLevel() / divided);
        if ((mons.isBoss()) && (!mons.isPartyBonus()))
        {
            maxMeso *= 3;
        }
        for (int i = 0; i < mons.dropsMesoCount(); i++)
        {
            if ((mons.getId() >= 9600086) && (mons.getId() <= 9600098))
            {
                int meso = (int) Math.floor(Math.random() * 500.0D + 1000.0D);
                ret.add(new MonsterDropEntry(0, 20000, (int) Math.floor(0.46D * meso), meso, 0));
            }
            else
            {
                ret.add(new MonsterDropEntry(0, mons.isPartyBonus() ? 40000 : (mons.isBoss()) && (!mons.isPartyBonus()) ? 800000 : 40000, (int) Math.floor(0.66D * maxMeso), maxMeso, 0));
            }
        }
    }

    public boolean contains(ArrayList<MonsterDropEntry> e, int toAdd)
    {
        for (MonsterDropEntry f : e)
        {
            if (f.itemId == toAdd)
            {
                return true;
            }
        }
        return false;
    }

    public int chanceLogic(int itemId)
    {
        switch (itemId)
        {
            case 2049301:
            case 2049401:
            case 4280000:
            case 4280001:
                return 5000;
            case 1002419:
            case 2049300:
            case 2049400:
                return 2000;
            case 1002938:
                return 50;
        }
        if (ItemConstants.getInventoryType(itemId) == MapleInventoryType.EQUIP) return 8000;
        if ((ItemConstants.getInventoryType(itemId) == MapleInventoryType.SETUP) || (ItemConstants.getInventoryType(itemId) == MapleInventoryType.CASH))
        {
            return 500;
        }
        switch (itemId / 10000)
        {
            case 204:
                return 1800;
            case 207:
            case 233:
                return 3000;
            case 229:
                return 400;
            case 401:
            case 402:
                return 5000;
            case 403:
                return 4000;
        }
        return 8000;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\life\MapleMonsterInformationProvider.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */