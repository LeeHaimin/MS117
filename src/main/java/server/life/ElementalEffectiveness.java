package server.life;

public enum ElementalEffectiveness
{
    正常(1.0D), 免疫(0.0D), 增强(0.5D), 虚弱(1.5D);

    private final double value;

    ElementalEffectiveness(double val)
    {
        this.value = val;
    }

    public static ElementalEffectiveness getByNumber(int num)
    {
        switch (num)
        {
            case 1:
                return 免疫;
            case 2:
                return 增强;
            case 3:
                return 虚弱;
        }
        throw new IllegalArgumentException("Unkown effectiveness: " + num);
    }

    public double getValue()
    {
        return this.value;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\life\ElementalEffectiveness.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */