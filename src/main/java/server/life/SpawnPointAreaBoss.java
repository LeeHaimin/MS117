package server.life;

import java.awt.Point;
import java.util.concurrent.atomic.AtomicBoolean;

import server.maps.MapleMap;

public class SpawnPointAreaBoss extends Spawns
{
    private final MapleMonsterStats monster;
    private final Point pos1;
    private final Point pos2;
    private final Point pos3;
    private final int mobTime;
    private final int fh;
    private final int f;
    private final int id;
    private final AtomicBoolean spawned = new AtomicBoolean(false);
    private final String msg;
    private long nextPossibleSpawn;
    private boolean sendWorldMsg = false;

    public SpawnPointAreaBoss(MapleMonster monster, Point pos1, Point pos2, Point pos3, int mobTime, String msg, boolean shouldSpawn, boolean sendWorldMsg)
    {
        this.monster = monster.getStats();
        this.id = monster.getId();
        this.fh = monster.getFh();
        this.f = monster.getF();
        this.pos1 = pos1;
        this.pos2 = pos2;
        this.pos3 = pos3;
        this.mobTime = (mobTime < 0 ? -1 : mobTime * 1000);
        this.msg = msg;
        this.sendWorldMsg = ((msg != null) && (sendWorldMsg));
        this.nextPossibleSpawn = (System.currentTimeMillis() + (shouldSpawn ? 0 : this.mobTime));
    }

    public MapleMonsterStats getMonster()
    {
        return this.monster;
    }

    public byte getCarnivalTeam()
    {
        return -1;
    }

    public boolean shouldSpawn(long time)
    {
        if ((this.mobTime < 0) || (this.spawned.get()))
        {
            return false;
        }
        return this.nextPossibleSpawn <= time;
    }

    public int getCarnivalId()
    {
        return -1;
    }

    public MapleMonster spawnMonster(MapleMap map)
    {
        Point pos = getPosition();
        MapleMonster mob = new MapleMonster(this.id, this.monster);
        mob.setPosition(pos);
        mob.setCy(pos.y);
        mob.setRx0(pos.x - 50);
        mob.setRx1(pos.x + 50);
        mob.setFh(this.fh);
        mob.setF(this.f);
        this.spawned.set(true);
        mob.addListener(new MonsterListener()
        {
            public void monsterKilled()
            {
                SpawnPointAreaBoss.this.nextPossibleSpawn = System.currentTimeMillis();

                if (SpawnPointAreaBoss.this.mobTime > 0)
                {
                    SpawnPointAreaBoss.this.nextPossibleSpawn = (SpawnPointAreaBoss.this.nextPossibleSpawn + SpawnPointAreaBoss.this.mobTime);
                }
                SpawnPointAreaBoss.this.spawned.set(false);
            }
        });
        map.spawnMonster(mob, -2);


        if (this.msg != null)
        {
            if (this.sendWorldMsg)
            {
                handling.world.WorldBroadcastService.getInstance().broadcastMessage(tools.MaplePacketCreator.spouseMessage(20, "[系统提示] " + this.msg));
            }
            else
            {
                map.broadcastMessage(tools.MaplePacketCreator.serverNotice(6, this.msg));
            }
        }
        return mob;
    }

    public int getMobTime()
    {
        return this.mobTime;
    }

    public Point getPosition()
    {
        int rand = server.Randomizer.nextInt(3);
        return rand == 1 ? this.pos2 : rand == 0 ? this.pos1 : this.pos3;
    }

    public int getF()
    {
        return this.f;
    }

    public int getFh()
    {
        return this.fh;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\life\SpawnPointAreaBoss.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */