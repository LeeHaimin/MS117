package server.life;

public class MonsterDropEntry
{
    public final int itemId;
    public final int chance;
    public final int Minimum;
    public final int Maximum;
    public final int questid;

    public MonsterDropEntry(int itemId, int chance, int Minimum, int Maximum, int questid)
    {
        this.itemId = itemId;
        this.chance = chance;
        this.questid = questid;
        this.Minimum = Minimum;
        this.Maximum = Maximum;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\life\MonsterDropEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */