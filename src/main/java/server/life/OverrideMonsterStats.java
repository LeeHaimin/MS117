package server.life;

public class OverrideMonsterStats
{
    public long hp;
    public int exp;
    public int mp;

    public OverrideMonsterStats()
    {
        this.hp = 1L;
        this.exp = 0;
        this.mp = 0;
    }

    public OverrideMonsterStats(long hp, int mp, int exp)
    {
        this(hp, mp, exp, true);
    }

    public OverrideMonsterStats(long hp, int mp, int exp, boolean change)
    {
        this.hp = hp;
        this.mp = mp;
        this.exp = exp;
    }

    public int getExp()
    {
        return this.exp;
    }

    public void setOExp(int exp)
    {
        this.exp = exp;
    }

    public long getHp()
    {
        return this.hp;
    }

    public void setOHp(long hp)
    {
        this.hp = hp;
    }

    public int getMp()
    {
        return this.mp;
    }

    public void setOMp(int mp)
    {
        this.mp = mp;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\life\OverrideMonsterStats.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */