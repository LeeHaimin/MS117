package server.life;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleCharacter;
import client.MapleClient;
import client.Skill;
import client.status.MonsterStatus;
import client.status.MonsterStatusEffect;
import constants.GameConstants;
import handling.channel.ChannelServer;
import handling.world.party.MapleParty;
import handling.world.party.MaplePartyCharacter;
import scripting.event.EventInstanceManager;
import server.MapleItemInformationProvider;
import server.MapleStatEffect;
import server.maps.MapleMap;
import server.maps.MapleMapObject;
import server.maps.MapleMapObjectType;
import tools.ConcurrentEnumMap;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.packet.MobPacket;

public class MapleMonster extends AbstractLoadedMapleLife
{
    private final Collection<AttackerEntry> attackers = new LinkedList<>();
    private final ConcurrentEnumMap<MonsterStatus, MonsterStatusEffect> mobEffects = new ConcurrentEnumMap(MonsterStatus.class);
    private final LinkedList<MonsterStatusEffect> poisons = new LinkedList<>();
    private final ReentrantReadWriteLock poisonsLock = new ReentrantReadWriteLock();
    private MapleMonsterStats stats;
    private ChangeableStats ostats = null;
    private long hp;
    private long nextKill = 0L;
    private long lastDropTime = 0L;
    private int mp;
    private byte carnivalTeam = -1;
    private MapleMap map;
    private WeakReference<MapleMonster> sponge = new WeakReference(null);
    private int linkoid = 0;
    private int lastNode = -1;
    private int highestDamageChar = 0;
    private int linkCID = 0;
    private WeakReference<MapleCharacter> controller = new WeakReference(null);
    private boolean fake = false;
    private boolean dropsDisabled = false;
    private boolean controllerHasAggro = false;
    private EventInstanceManager eventInstance;
    private MonsterListener listener = null;
    private byte[] reflectpack = null;
    private byte[] nodepack = null;
    private Map<Integer, Long> usedSkills;
    private int stolen = -1;
    private boolean shouldDropItem = false;
    private boolean killed = false;

    public MapleMonster(int id, MapleMonsterStats stats)
    {
        super(id);
        initWithStats(stats);
    }

    private void initWithStats(MapleMonsterStats stats)
    {
        setStance(5);
        this.stats = stats;
        this.hp = stats.getHp();
        this.mp = stats.getMp();

        if (stats.getNoSkills() > 0)
        {
            this.usedSkills = new HashMap<>();
        }
    }

    public MapleMonster(MapleMonster monster)
    {
        super(monster);
        initWithStats(monster.stats);
    }

    public int getMobLevel()
    {
        if (this.ostats != null)
        {
            return this.ostats.level;
        }
        return this.stats.getLevel();
    }

    public ChangeableStats getChangedStats()
    {
        return this.ostats;
    }

    public void setOverrideStats(OverrideMonsterStats ostats)
    {
        this.ostats = new ChangeableStats(this.stats, ostats);
        this.hp = ostats.getHp();
        this.mp = ostats.getMp();
    }

    public void changeLevel(int newLevel)
    {
        changeLevel(newLevel, true);
    }

    public void changeLevel(int newLevel, boolean pqMob)
    {
        if (!this.stats.isChangeable())
        {
            return;
        }
        this.ostats = new ChangeableStats(this.stats, newLevel, pqMob);
        this.hp = this.ostats.getHp();
        this.mp = this.ostats.getMp();
    }

    public void changeLevelmod(int newLevel, int multipler)
    {
        if (!this.stats.isChangeable())
        {
            return;
        }
        this.ostats = new ChangeableStats(this.stats, newLevel, multipler);
        this.hp = this.ostats.getHp();
        this.mp = this.ostats.getMp();
    }

    public void heal(int hp, int mp, boolean broadcast)
    {
        long TotalHP = getHp() + hp;
        int TotalMP = getMp() + mp;
        if (TotalHP >= getMobMaxHp())
        {
            setHp(getMobMaxHp());
        }
        else
        {
            setHp(TotalHP);
        }
        if (TotalMP >= getMp())
        {
            setMp(getMp());
        }
        else
        {
            setMp(TotalMP);
        }
        if (broadcast)
        {
            this.map.broadcastMessage(MobPacket.healMonster(getObjectId(), hp));
        }
        else if (this.sponge.get() != null)
        {
            this.sponge.get().hp += hp;
        }
    }

    public long getHp()
    {
        return this.hp;
    }

    public void setHp(long hp)
    {
        this.hp = hp;
    }

    public int getMp()
    {
        return this.mp;
    }

    public long getMobMaxHp()
    {
        if (this.ostats != null)
        {
            return this.ostats.hp;
        }
        return this.stats.getHp();
    }

    public void setMp(int mp)
    {
        if (mp < 0)
        {
            mp = 0;
        }
        this.mp = mp;
    }

    public void killed()
    {
        if (this.listener != null)
        {
            this.listener.monsterKilled();
        }
        this.listener = null;
    }

    private void giveExpToCharacter(MapleCharacter attacker, int exp, boolean highestDamage, int numExpSharers, int lastskillID)
    {
        if (highestDamage)
        {
            if (this.eventInstance != null)
            {
                this.eventInstance.monsterKilled(attacker, this);
            }
            else
            {
                EventInstanceManager em = attacker.getEventInstance();
                if (em != null)
                {
                    em.monsterKilled(attacker, this);
                }
            }
            this.highestDamageChar = attacker.getId();
        }
        if ((exp > 0) && (attacker.isAlive()))
        {
            MonsterStatusEffect ms = this.mobEffects.get(MonsterStatus.挑衅);
            if (ms != null)
            {
                exp += (int) (exp * (ms.getX() / 100.0D));
            }
            if (attacker.hasDisease(client.MapleDisease.诅咒))
            {
                exp /= 2;
            }
            Integer holySymbol = attacker.getBuffedValue(client.MapleBuffStat.神圣祈祷);
            if (holySymbol != null)
            {
                exp = (int) (exp * (1.0D + holySymbol.doubleValue() / 100.0D));
            }

            exp = (int) Math.min(2.147483647E9D, exp * attacker.getEXPMod() * attacker.getStat().expBuff / 100.0D * ChannelServer.getInstance(this.map.getChannel()).getExpRate());

            attacker.getTrait(client.MapleTraitType.charisma).addExp(this.stats.getCharismaEXP(), attacker);

            attacker.gainExpMonster(exp, true, highestDamage, numExpSharers, this.stats.isPartyBonus(), this.stats.getPartyBonusRate());
        }
        attacker.mobKilled(getId(), lastskillID);
    }

    public int killBy(MapleCharacter killer, int lastSkill)
    {
        if (this.killed)
        {
            return 1;
        }
        this.killed = true;
        int totalBaseExp = getMobExp();
        AttackerEntry highest = null;
        long highdamage = 0L;
        List<AttackerEntry> list = getAttackers();
        for (AttackerEntry attackEntry : list)
        {
            if ((attackEntry != null) && (attackEntry.getDamage() > highdamage))
            {
                highest = attackEntry;
                highdamage = attackEntry.getDamage();
            }
        }
        for (AttackerEntry attackEntry : list)
        {
            if (attackEntry != null)
            {
                int baseExp = (int) Math.ceil(totalBaseExp * (attackEntry.getDamage() / getMobMaxHp()));
                attackEntry.killedMob(getMap(), baseExp, attackEntry == highest, lastSkill);
            }
        }
        MapleCharacter controll = this.controller.get();
        if (controll != null)
        {
            controll.getClient().getSession().write(MobPacket.stopControllingMonster(getObjectId()));
            controll.stopControllingMonster(this);
        }
        int achievement = 0;
        switch (getId())
        {
            case 9400121:
                achievement = 12;
                break;
            case 8500002:
                achievement = 13;
                break;
            case 8510000:
            case 8520000:
                achievement = 14;
                break;
        }

        MapleCharacter mpc;
        if (achievement != 0)
        {
            if ((killer != null) && (killer.getParty() != null))
            {
                for (MaplePartyCharacter pChar : killer.getParty().getMembers())
                {
                    mpc = killer.getMap().getCharacterById(pChar.getId());
                    if (mpc != null)
                    {
                        mpc.finishAchievement(achievement);
                    }
                }
            }
            else if (killer != null)
            {
                killer.finishAchievement(achievement);
            }
        }
        if ((killer != null) && (this.stats.isBoss()))
        {
            killer.finishAchievement(18);
        }


        spawnRevives(getMap());
        if (this.eventInstance != null)
        {
            this.eventInstance.unregisterMonster(this);
            this.eventInstance = null;
        }
        if ((killer != null) && (killer.getPyramidSubway() != null))
        {
            killer.getPyramidSubway().onKill(killer);
        }
        this.hp = 0L;
        MapleMonster oldSponge = getSponge();
        this.sponge = new WeakReference(null);
        if ((oldSponge != null) && (oldSponge.isAlive()))
        {
            boolean set = true;
            for (MapleMapObject mon : this.map.getAllMonstersThreadsafe())
            {
                MapleMonster mons = (MapleMonster) mon;
                if ((mons.isAlive()) && (mons.getObjectId() != oldSponge.getObjectId()) && (mons.getStats().getLevel() > 1) && (mons.getObjectId() != getObjectId()) && ((mons.getSponge() == oldSponge) || (mons.getLinkOid() == oldSponge.getObjectId())))
                {
                    set = false;
                    break;
                }
            }
            if (set)
            {
                this.map.killMonster(oldSponge, killer, true, false, (byte) 1);
            }
        }

        this.reflectpack = null;
        this.nodepack = null;
        if (this.mobEffects.size() > 0)
        {
            List<MonsterStatus> statuses = new LinkedList(this.mobEffects.keySet());
            for (MonsterStatus ms : statuses)
            {
                cancelStatus(ms);
            }
            statuses.clear();
        }
        if (this.poisons.size() > 0)
        {
            List<MonsterStatusEffect> ps = new LinkedList<>();
            this.poisonsLock.readLock().lock();
            try
            {
                ps.addAll(this.poisons);
            }
            finally
            {
                this.poisonsLock.readLock().unlock();
            }
            for (MonsterStatusEffect p : ps)
            {
                cancelSingleStatus(p);
            }
            ps.clear();
        }

        cancelDropItem();
        int v1 = this.highestDamageChar;
        this.highestDamageChar = 0;
        return v1;
    }

    public int getMobExp()
    {
        if (this.ostats != null)
        {
            return this.ostats.exp;
        }
        return this.stats.getExp();
    }

    public ArrayList<AttackerEntry> getAttackers()
    {
        if ((this.attackers == null) || (this.attackers.size() <= 0))
        {
            return new ArrayList();
        }
        ArrayList<AttackerEntry> ret = new ArrayList<>();
        for (AttackerEntry e : this.attackers)
        {
            if (e != null)
            {
                ret.add(e);
            }
        }
        return ret;
    }

    public MapleMap getMap()
    {
        return this.map;
    }

    public void setMap(MapleMap map)
    {
        this.map = map;
        startDropItemSchedule();
    }

    public void startDropItemSchedule()
    {
        cancelDropItem();
        if ((this.stats.getDropItemPeriod() <= 0) || (!isAlive()))
        {
            return;
        }
        this.shouldDropItem = false;
        this.lastDropTime = System.currentTimeMillis();
    }

    public void spawnRevives(MapleMap map)
    {
        List<Integer> toSpawn = this.stats.getRevives();
        if ((toSpawn == null) || (getLinkCID() > 0))
        {
            return;
        }
        MapleMonster spongy = null;
        long spongyHp = 0L;
        Iterator localIterator;
        MapleMonster mons;
        MapleMonster monsterI;
        int i;
        switch (getId())
        {
            case 6160003:
            case 8820002:
            case 8820003:
            case 8820004:
            case 8820005:
            case 8820006:
            case 8820102:
            case 8820103:
            case 8820104:
            case 8820105:
            case 8820106:
            case 8820115:
            case 8820116:
            case 8820117:
            case 8820118:
            case 8820213:
            case 8820214:
            case 8820215:
            case 8820216:
            case 8820217:
            case 8820218:
            case 8820219:
            case 8820220:
            case 8820221:
            case 8820222:
            case 8820223:
            case 8820224:
            case 8820225:
            case 8820226:
            case 8820227:
            case 8840000:
            case 8850011:
                break;
            case 8810118:
            case 8810119:
            case 8810120:
            case 8810121:
                for (localIterator = toSpawn.iterator(); localIterator.hasNext(); )
                {
                    i = (Integer) localIterator.next();
                    MapleMonster mob = MapleLifeFactory.getMonster(i);
                    mob.setPosition(getTruePosition());
                    if (this.eventInstance != null)
                    {
                        this.eventInstance.registerMonster(mob);
                    }
                    if (dropsDisabled())
                    {
                        mob.disableDrops();
                    }
                    switch (mob.getId())
                    {
                        case 8810119:
                        case 8810120:
                        case 8810121:
                        case 8810122:
                            spongy = mob;
                    }

                }
                if ((spongy == null) || (map.getMonsterById(spongy.getId()) != null)) return;
                map.spawnMonster(spongy, -2);
                for (MapleMapObject mon : map.getAllMonstersThreadsafe())
                {
                    mons = (MapleMonster) mon;
                    if ((mons.getObjectId() != spongy.getObjectId()) && ((mons.getSponge() == this) || (mons.getLinkOid() == getObjectId()))) mons.setSponge(spongy);
                }
                break;


            case 8820300:
            case 8820301:
            case 8820302:
            case 8820303:


            case 8820304:
                MapleMonster linkMob = MapleLifeFactory.getMonster(getId() - 190);
                if (linkMob != null)
                {
                    toSpawn = linkMob.getStats().getRevives();
                }
            case 8820108:
            case 8820109:
                List<MapleMonster> cs_mobs = new ArrayList<>();
                for (localIterator = toSpawn.iterator(); localIterator.hasNext(); )
                {
                    i = (Integer) localIterator.next();
                    MapleMonster mob = MapleLifeFactory.getMonster(i);
                    mob.setPosition(getTruePosition());
                    if (this.eventInstance != null)
                    {
                        this.eventInstance.registerMonster(mob);
                    }
                    if (dropsDisabled())
                    {
                        mob.disableDrops();
                    }
                    switch (mob.getId())
                    {
                        case 8820109:
                        case 8820300:
                        case 8820301:
                        case 8820302:
                        case 8820303:
                        case 8820304:
                            spongy = mob;

                            break;
                        default:
                            if (mob.isFirstAttack())
                            {
                                spongyHp += mob.getMobMaxHp();
                            }
                            cs_mobs.add(mob);
                    }

                }

                if ((spongy == null) || (map.getMonsterById(spongy.getId()) != null)) return;
                if (spongyHp > 0L)
                {
                    spongy.setHp(spongyHp);
                    spongy.getStats().setHp(spongyHp);
                }

                map.spawnMonster(spongy, -2);
                for (localIterator = cs_mobs.iterator(); localIterator.hasNext(); )
                {
                    monsterI = (MapleMonster) localIterator.next();
                    map.spawnMonster(monsterI, -2);
                    monsterI.setSponge(spongy);
                }
                break;


            case 8810026:
            case 8810130:
            case 8820008:
            case 8820009:
            case 8820010:
            case 8820011:
            case 8820012:
            case 8820013:
                ArrayList mobs = new ArrayList<>();
                for (localIterator = toSpawn.iterator(); localIterator.hasNext(); )
                {
                    i = (Integer) localIterator.next();
                    MapleMonster mob = MapleLifeFactory.getMonster(i);
                    mob.setPosition(getTruePosition());
                    if (this.eventInstance != null)
                    {
                        this.eventInstance.registerMonster(mob);
                    }
                    if (dropsDisabled())
                    {
                        mob.disableDrops();
                    }
                    switch (mob.getId())
                    {
                        case 8810018:
                        case 8810118:
                        case 8820009:
                        case 8820010:
                        case 8820011:
                        case 8820012:
                        case 8820013:
                        case 8820014:
                            spongy = mob;

                            break;
                        default:
                            mobs.add(mob);
                    }

                }

                if ((spongy == null) || (map.getMonsterById(spongy.getId()) != null)) return;
                map.spawnMonster(spongy, -2);
                for (localIterator = mobs.iterator(); localIterator.hasNext(); )
                {
                    monsterI = (MapleMonster) localIterator.next();
                    map.spawnMonster(monsterI, -2);
                    monsterI.setSponge(spongy);
                }
                break;
            case 8820014:
            case 8820101:
            case 8820200:
            case 8820201:
            case 8820202:
            case 8820203:
            case 8820204:
            case 8820205:
            case 8820206:
            case 8820207:
            case 8820208:
            case 8820209:
            case 8820210:
            case 8820211:
                for (localIterator = toSpawn.iterator(); localIterator.hasNext(); )
                {
                    i = (Integer) localIterator.next();
                    MapleMonster mob = MapleLifeFactory.getMonster(i);
                    if (this.eventInstance != null)
                    {
                        this.eventInstance.registerMonster(mob);
                    }
                    mob.setPosition(getTruePosition());
                    if (dropsDisabled())
                    {
                        mob.disableDrops();
                    }
                    map.spawnMonster(mob, -2);
                }

                break;
        }
        for (localIterator = toSpawn.iterator(); localIterator.hasNext(); )
        {
            i = (Integer) localIterator.next();
            MapleMonster mob = MapleLifeFactory.getMonster(i);
            if (mob != null)
            {

                if (this.eventInstance != null)
                {
                    this.eventInstance.registerMonster(mob);
                }
                mob.setPosition(getTruePosition());
                if (dropsDisabled())
                {
                    mob.disableDrops();
                }
                map.spawnRevives(mob, getObjectId());
                if (mob.getId() == 9300216)
                {
                    map.broadcastMessage(tools.MaplePacketCreator.playSound("Dojang/clear"));
                    map.broadcastMessage(tools.MaplePacketCreator.showEffect("dojang/end/clear"));
                }
            }
        }
    }

    public MapleMonster getSponge()
    {
        return this.sponge.get();
    }

    public boolean isAlive()
    {
        return this.hp > 0L;
    }

    public MapleMonsterStats getStats()
    {
        return this.stats;
    }

    public int getLinkOid()
    {
        return this.linkoid;
    }

    public void cancelStatus(MonsterStatus stat)
    {
        if ((stat == MonsterStatus.空白BUFF) || (stat == MonsterStatus.召唤怪物))
        {
            return;
        }
        MonsterStatusEffect mse = this.mobEffects.get(stat);
        if ((mse == null) || (!isAlive()))
        {
            return;
        }
        if (mse.isReflect())
        {
            this.reflectpack = null;
        }
        mse.cancelPoisonSchedule(this);
        MapleCharacter con = getController();
        if (con != null)
        {
            this.map.broadcastMessage(con, MobPacket.cancelMonsterStatus(getObjectId(), stat), getTruePosition());
            con.getClient().getSession().write(MobPacket.cancelMonsterStatus(getObjectId(), stat));
        }
        else
        {
            this.map.broadcastMessage(MobPacket.cancelMonsterStatus(getObjectId(), stat), getTruePosition());
        }
        this.mobEffects.remove(stat);
    }

    public void cancelSingleStatus(MonsterStatusEffect stat)
    {
        if ((stat == null) || (stat.getStati() == MonsterStatus.空白BUFF) || (stat.getStati() == MonsterStatus.召唤怪物) || (!isAlive()))
        {
            return;
        }
        if ((stat.getStati() != MonsterStatus.中毒) && (stat.getStati() != MonsterStatus.烈焰喷射))
        {
            cancelStatus(stat.getStati());
            return;
        }
        this.poisonsLock.writeLock().lock();
        try
        {
            if (!this.poisons.contains(stat))
            {
                return;
            }
            this.poisons.remove(stat);
            if (stat.isReflect())
            {
                this.reflectpack = null;
            }
            stat.cancelPoisonSchedule(this);
            MapleCharacter con = getController();
            if (con != null)
            {
                this.map.broadcastMessage(con, MobPacket.cancelMonsterPoisonStatus(getObjectId(), stat), getTruePosition());
                con.getClient().getSession().write(MobPacket.cancelMonsterPoisonStatus(getObjectId(), stat));
            }
            else
            {
                this.map.broadcastMessage(MobPacket.cancelMonsterPoisonStatus(getObjectId(), stat), getTruePosition());
            }
        }
        finally
        {
            this.poisonsLock.writeLock().unlock();
        }
    }

    public void cancelDropItem()
    {
        this.lastDropTime = 0L;
    }

    public int getLinkCID()
    {
        return this.linkCID;
    }

    public boolean dropsDisabled()
    {
        return this.dropsDisabled;
    }

    public void disableDrops()
    {
        this.dropsDisabled = true;
    }

    public boolean isFirstAttack()
    {
        return this.stats.isFirstAttack();
    }

    public MapleCharacter getController()
    {
        return this.controller.get();
    }

    public void setController(MapleCharacter controller)
    {
        this.controller = new WeakReference(controller);
    }

    public void setLinkCID(int lc)
    {
        this.linkCID = lc;
        if (lc > 0)
        {
            this.mobEffects.put(MonsterStatus.心灵控制, new MonsterStatusEffect(MonsterStatus.心灵控制, 60000, 30001062, null, false));
        }
    }

    public void setLinkOid(int lo)
    {
        this.linkoid = lo;
    }

    public void setSponge(MapleMonster mob)
    {
        this.sponge = new WeakReference(mob);
        if (this.linkoid <= 0)
        {
            this.linkoid = mob.getObjectId();
        }
    }

    public byte getCarnivalTeam()
    {
        return this.carnivalTeam;
    }

    public void setCarnivalTeam(byte team)
    {
        this.carnivalTeam = team;
    }

    public void switchController(MapleCharacter newController, boolean immediateAggro)
    {
        MapleCharacter controllers = getController();
        if (controllers == newController) return;
        if (controllers != null)
        {
            controllers.stopControllingMonster(this);
            controllers.getClient().getSession().write(MobPacket.stopControllingMonster(getObjectId()));
            sendStatus(controllers.getClient());
        }
        newController.controlMonster(this, immediateAggro);
        setController(newController);
        if (immediateAggro)
        {
            setControllerHasAggro(true);
        }
    }

    public void sendStatus(MapleClient client)
    {
        map.killAllMonsters(true);
        map.broadcastMessage(MaplePacketCreator.serverNotice(5, "A player has moved too far from Shammos. Shammos is going back to the start."));
        for (MapleCharacter chr : map.getCharactersThreadsafe())
        {
            chr.changeMap(chr.getMap(), chr.getMap().getPortal(0));
        }
        //屏蔽地图动画
        // MapScriptMethods.startScript_FirstUser(c, "shammos_Fenter");
    }

    public void addListener(MonsterListener listener)
    {
        this.listener = listener;
    }

    public boolean isControllerHasAggro()
    {
        return this.controllerHasAggro;
    }

    public void setControllerHasAggro(boolean controllerHasAggro)
    {
        this.controllerHasAggro = controllerHasAggro;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(this.stats.getName());
        sb.append("(");
        sb.append(getId());
        sb.append(") 等级:");
        sb.append(this.stats.getLevel());
        if (this.ostats != null)
        {
            sb.append("→");
            sb.append(this.ostats.level);
        }
        sb.append(" 坐标(X:");
        sb.append(getTruePosition().x);
        sb.append("/Y:");
        sb.append(getTruePosition().y);
        sb.append(") 信息: ");
        sb.append(getHp());
        sb.append("/");
        sb.append(getMobMaxHp());
        sb.append("Hp, ");
        sb.append(getMp());
        sb.append("/");
        sb.append(getMobMaxMp());
        sb.append("Mp, oid: ");
        sb.append(getObjectId());
        sb.append("||仇恨目标: ");
        MapleCharacter chr = this.controller.get();
        sb.append(chr != null ? chr.getName() : "无");
        return sb.toString();
    }

    public int getMobMaxMp()
    {
        if (this.ostats != null)
        {
            return this.ostats.mp;
        }
        return this.stats.getMp();
    }

    public MapleMapObjectType getType()
    {
        return server.maps.MapleMapObjectType.MONSTER;
    }

    public void sendSpawnData(MapleClient client)
    {
        if (!isAlive())
        {
            return;
        }
        client.getSession().write(MobPacket.spawnMonster(this, (this.fake) && (this.linkCID <= 0) ? -4 : -1, 0));
        sendStatus(client);
        if ((this.map != null) && (!this.stats.isEscort()) && (client.getPlayer() != null) && (client.getPlayer().getTruePosition().distanceSq(getTruePosition()) <= GameConstants.maxViewRangeSq_Half()))
        {
            this.map.updateMonsterController(this);
        }
    }

    public void sendDestroyData(MapleClient client)
    {
        if ((this.stats.isEscort()) && (getEventInstance() != null) && (this.lastNode >= 0))
        {
            this.map.resetShammos(client);
        }
        else
        {
            client.getSession().write(MobPacket.killMonster(getObjectId(), 0));
            if ((getController() != null) && (client.getPlayer() != null) && (client.getPlayer().getId() == getController().getId()))
            {
                client.getPlayer().stopControllingMonster(this);
            }
        }
    }

    public EventInstanceManager getEventInstance()
    {
        return this.eventInstance;
    }

    public void setEventInstance(EventInstanceManager eventInstance)
    {
        this.eventInstance = eventInstance;
    }

    public int getStatusSourceID(MonsterStatus status)
    {
        if ((status == MonsterStatus.中毒) || (status == MonsterStatus.烈焰喷射))
        {
            this.poisonsLock.readLock().lock();
            try
            {
                for (MonsterStatusEffect ps : this.poisons)
                {
                    if (ps != null)
                    {
                        return ps.getSkill();
                    }
                }
                return -1;
            }
            finally
            {
                this.poisonsLock.readLock().unlock();
            }
        }
        MonsterStatusEffect effect = this.mobEffects.get(status);
        if (effect != null)
        {
            return effect.getSkill();
        }
        return -1;
    }

    public ElementalEffectiveness getEffectiveness(Element e)
    {
        if ((this.mobEffects.size() > 0) && (this.mobEffects.containsKey(MonsterStatus.巫毒)))
        {
            return ElementalEffectiveness.正常;
        }
        return this.stats.getEffectiveness(e);
    }

    public void applyStatus(MapleCharacter from, MonsterStatusEffect status, boolean poison, long duration, boolean checkboss, MapleStatEffect effect)
    {
        if ((!isAlive()) || (getLinkCID() > 0))
        {
            return;
        }
        Skill skilz = client.SkillFactory.getSkill(status.getSkill());
        if (skilz != null)
        {
            switch (this.stats.getEffectiveness(skilz.getElement()))
            {
                case 免疫:
                case 增强:
                    return;
                case 正常:
                case 虚弱:
                    break;
                default:
                    return;
            }

        }
        int statusSkill = status.getSkill();
        switch (statusSkill)
        {
            case 2111006:

            case 4110011:
            case 4120011:
            case 4210010:
            case 4220011:
            case 4320005:
            case 4340012:
            case 14110004:
                switch (this.stats.getEffectiveness(Element.POISON))
                {
                    case 免疫:
                    case 增强:
                        return;
                }
                break;

            case 2211006:
                switch (this.stats.getEffectiveness(Element.ICE))
                {
                    case 免疫:
                    case 增强:
                        return;
                }
                break;
        }
        MonsterStatus stat = status.getStati();
        if ((this.stats.isNoDoom()) && (stat == MonsterStatus.巫毒))
        {
            return;
        }
        if (this.stats.isBoss())
        {
            if ((stat == MonsterStatus.眩晕) || (stat == MonsterStatus.速度))
            {
                return;
            }
            if ((checkboss) && (stat != MonsterStatus.忍者伏击) && (stat != MonsterStatus.物攻) && (stat != MonsterStatus.中毒) && (stat != MonsterStatus.烈焰喷射) && (stat != MonsterStatus.恐慌) && (stat != MonsterStatus.魔击无效))
            {
                return;
            }

            if ((getId() == 8850011) && (stat == MonsterStatus.魔击无效))
            {
                return;
            }
        }
        if (((this.stats.isFriendly()) || (isFake())) && ((stat == MonsterStatus.眩晕) || (stat == MonsterStatus.速度) || (stat == MonsterStatus.中毒) || (stat == MonsterStatus.烈焰喷射)))
        {
            return;
        }

        if (((stat == MonsterStatus.烈焰喷射) || (stat == MonsterStatus.中毒)) && (effect == null))
        {
            return;
        }
        if (this.mobEffects.containsKey(stat))
        {
            cancelStatus(stat);
        }
        if ((stat == MonsterStatus.中毒) || (stat == MonsterStatus.烈焰喷射))
        {
            this.poisonsLock.readLock().lock();
            try
            {
                for (MonsterStatusEffect mse : this.poisons)
                {
                    if ((mse != null) && ((mse.getSkill() == effect.getSourceId()) || (mse.getSkill() == GameConstants.getLinkedAttackSkill(effect.getSourceId())) || (GameConstants.getLinkedAttackSkill(mse.getSkill()) == effect.getSourceId())))
                    {
                        return;
                    }
                }
            }
            finally
            {
                this.poisonsLock.readLock().unlock();
            }
        }
        if ((poison) && (getHp() > 1L) && (effect != null))
        {
            if (statusSkill == 2111003)
            {
                duration = effect.getDOTTime() * 1000;
            }
            else
            {
                duration = Math.max(duration, effect.getDOTTime() * 1000);
            }
        }

        duration += from.getStat().dotTime * 1000;

        if (duration >= 60000L)
        {
            duration = 10000L;
        }
        long aniTime = duration;
        status.setCancelTask(aniTime);
        if ((poison) && (getHp() > 1L))
        {
            status.setDotTime(duration);
            int poisonDot = from.getStat().dot;
            int damageIncrease = from.getStat().getDamageIncrease(effect.getSourceId());
            if (damageIncrease > effect.getDOT())
            {
                poisonDot += damageIncrease;
            }
            else
            {
                poisonDot += effect.getDOT();
            }
            if (from.isAdmin())
            {
                from.dropSpouseMessage(18, "开始处理中毒效果 - 技能ID: " + effect.getSourceId());
                from.dropSpouseMessage(18, "中毒伤害加成 - 技能: " + effect.getDOT() + " 被动: " + from.getStat().dot + " 被动加成: " + damageIncrease + " 最终加成: " + poisonDot);
            }
            status.setValue(status.getStati(), (int) (poisonDot * from.getStat().getCurrentMaxBaseDamage() / 100.0D));
            int poisonDamage = Integer.valueOf((int) (aniTime / 1000L * status.getX() / 2L));
            if (from.isAdmin())
            {
                from.dropSpouseMessage(18, "中毒伤害 - 中毒伤害: " + poisonDamage + " 持续时间: " + aniTime + " 持续掉血: " + status.getX());
            }
            status.setPoisonSchedule(poisonDamage, from);
            if (poisonDamage > 0)
            {
                if (poisonDamage >= this.hp)
                {
                    poisonDamage = (int) (this.hp - 1L);
                }
                damage(from, poisonDamage, false);
            }
        }
        else if ((statusSkill == 4111003) || (statusSkill == 14111001))
        {
            status.setValue(status.getStati(), (int) (getMobMaxHp() / 50.0D + 0.999D));
            status.setPoisonSchedule(Integer.valueOf(status.getX()), from);
        }
        else if (statusSkill == 4341003)
        {
            status.setPoisonSchedule(Integer.valueOf((int) (effect.getDamage() * from.getStat().getCurrentMaxBaseDamage() / 100.0D)), from);
        }
        MapleCharacter con = getController();
        if ((stat == MonsterStatus.中毒) || (stat == MonsterStatus.烈焰喷射))
        {
            this.poisonsLock.writeLock().lock();
            try
            {
                this.poisons.add(status);
                if (con != null)
                {
                    this.map.broadcastMessage(con, MobPacket.applyMonsterPoisonStatus(this, this.poisons), getTruePosition());
                    con.getClient().getSession().write(MobPacket.applyMonsterPoisonStatus(this, this.poisons));
                }
                else
                {
                    this.map.broadcastMessage(MobPacket.applyMonsterPoisonStatus(this, this.poisons), getTruePosition());
                }
            }
            finally
            {
                this.poisonsLock.writeLock().unlock();
            }
        }
        else
        {
            this.mobEffects.put(stat, status);
            if (con != null)
            {
                this.map.broadcastMessage(con, MobPacket.applyMonsterStatus(this, status), getTruePosition());
                con.getClient().getSession().write(MobPacket.applyMonsterStatus(this, status));
            }
            else
            {
                this.map.broadcastMessage(MobPacket.applyMonsterStatus(this, status), getTruePosition());
            }
        }
    }

    public boolean isFake()
    {
        return this.fake;
    }

    public void damage(MapleCharacter from, long damage, boolean updateAttackTime)
    {
        damage(from, damage, updateAttackTime, 0);
    }

    public void damage(MapleCharacter from, long damage, boolean updateAttackTime, int lastSkill)
    {
        if ((from == null) || (damage <= 0L) || (!isAlive())) return;
        AttackerEntry attacker;
        if (from.getParty() != null)
        {
            attacker = new PartyAttackerEntry(from.getParty().getId());
        }
        else
        {
            attacker = new SingleAttackerEntry(from);
        }
        boolean replaced = false;
        for (AttackerEntry aentry : getAttackers())
        {
            if ((aentry != null) && (aentry.equals(attacker)))
            {
                attacker = aentry;
                replaced = true;
                break;
            }
        }
        if (!replaced)
        {
            this.attackers.add(attacker);
        }
        long rDamage = Math.max(0L, Math.min(damage, this.hp));
        attacker.addDamage(from, rDamage, updateAttackTime);

        if (this.stats.getSelfD() != -1)
        {
            this.hp -= rDamage;
            if (this.hp > 0L)
            {
                if (this.hp < this.stats.getSelfDHp())
                {
                    this.map.killMonster(this, from, false, false, this.stats.getSelfD(), lastSkill);
                }
                else
                {
                    for (AttackerEntry mattacker : getAttackers())
                    {
                        for (AttackingMapleCharacter cattacker : mattacker.getAttackers())
                        {
                            if ((cattacker.getAttacker().getMap() == from.getMap()) && (cattacker.getLastAttackTime() >= System.currentTimeMillis() - 4000L))
                            {
                                cattacker.getAttacker().getClient().getSession().write(MobPacket.showMonsterHP(getObjectId(), getHPPercent()));
                            }
                        }
                    }
                }
            }
            else
            {
                this.map.killMonster(this, from, true, false, (byte) 1, lastSkill);
            }
        }
        else
        {
            if (this.sponge.get() != null)
            {
                if (this.sponge.get().hp > 0L)
                {

                    this.sponge.get().hp -= rDamage;
                    if (this.sponge.get().hp <= 0L)
                    {
                        this.map.broadcastMessage(MobPacket.showBossHP(this.sponge.get().getId(), -1L, this.sponge.get().getMobMaxHp()));
                        this.map.killMonster(this.sponge.get(), from, true, false, (byte) 1, lastSkill);
                    }
                    else
                    {
                        this.map.broadcastMessage(MobPacket.showBossHP(this.sponge.get()));
                    }
                }
            }
            if (this.hp > 0L)
            {
                this.hp -= rDamage;
                Object em;
                if (this.eventInstance != null)
                {
                    this.eventInstance.monsterDamaged(from, this, (int) rDamage);
                }
                else
                {
                    em = from.getEventInstance();
                    if (em != null)
                    {
                        ((EventInstanceManager) em).monsterDamaged(from, this, (int) rDamage);
                    }
                }
                if ((this.sponge.get() == null) && (this.hp > 0L))
                {
                    switch (this.stats.getHPDisplayType())
                    {
                        case 0:
                            this.map.broadcastMessage(MobPacket.showBossHP(this), getTruePosition());
                            break;
                        case 1:
                            this.map.broadcastMessage(from, MobPacket.damageFriendlyMob(this, damage, true), false);
                            break;
                        case 2:
                            this.map.broadcastMessage(MobPacket.showMonsterHP(getObjectId(), getHPPercent()));
                            from.mulung_EnergyModify(true);
                            break;
                        case 3:
                            for (em = getAttackers().iterator(); ((Iterator) em).hasNext(); )
                            {
                                AttackerEntry mattacker = (AttackerEntry) ((Iterator) em).next();
                                if (mattacker != null)
                                {
                                    for (AttackingMapleCharacter cattacker : mattacker.getAttackers())
                                    {
                                        if ((cattacker != null) && (cattacker.getAttacker().getMap() == from.getMap()) && (cattacker.getLastAttackTime() >= System.currentTimeMillis() - 4000L))
                                        {
                                            cattacker.getAttacker().getClient().getSession().write(MobPacket.showMonsterHP(getObjectId(), getHPPercent()));
                                        }
                                    }
                                }
                            }
                    }

                }

                if (this.hp <= 0L)
                {
                    if (this.stats.getHPDisplayType() == 0)
                    {
                        this.map.broadcastMessage(MobPacket.showBossHP(getId(), -1L, getMobMaxHp()), getTruePosition());
                    }
                    this.map.killMonster(this, from, true, false, (byte) 1, lastSkill);
                }
            }
        }
        startDropItemSchedule();
    }

    public int getHPPercent()
    {
        return (int) Math.ceil(this.hp * 100.0D / getMobMaxHp());
    }

    public void setFake(boolean fake)
    {
        this.fake = fake;
    }

    public void applyStatus(MonsterStatusEffect status)
    {
        if (this.mobEffects.containsKey(status.getStati()))
        {
            cancelStatus(status.getStati());
        }
        this.mobEffects.put(status.getStati(), status);
        this.map.broadcastMessage(MobPacket.applyMonsterStatus(this, status), getTruePosition());
    }

    public void dispelSkill(MobSkill skillId)
    {
        List<MonsterStatus> toCancel = new ArrayList<>();
        for (Map.Entry<MonsterStatus, MonsterStatusEffect> effects : mobEffects.entrySet())
        {
            MonsterStatusEffect mse = effects.getValue();
            if ((mse.getMobSkill() != null) && (mse.getMobSkill().getSkillId() == skillId.getSkillId()))
            {
                toCancel.add(effects.getKey());
            }
        }
        for (MonsterStatus stat : toCancel)
        {
            cancelStatus(stat);
        }
    }

    public void applyMonsterBuff(Map<MonsterStatus, Integer> effect, int skillId, long duration, MobSkill skill, List<Integer> reflection)
    {
        for (Map.Entry<MonsterStatus, Integer> monsterStatusIntegerEntry : effect.entrySet())
        {
            Map.Entry status = monsterStatusIntegerEntry;
            if (this.mobEffects.containsKey(status.getKey()))
            {
                cancelStatus((MonsterStatus) status.getKey());
            }
            MonsterStatusEffect effectz = new MonsterStatusEffect((MonsterStatus) status.getKey(), (Integer) status.getValue(), 0, skill, true, reflection.size() > 0);
            effectz.setCancelTask(duration);
            this.mobEffects.put((MonsterStatus) status.getKey(), effectz);
        }
        Map.Entry<MonsterStatus, Integer> status;
        MapleCharacter con = getController();
        if (reflection.size() > 0)
        {
            this.reflectpack = MobPacket.applyMonsterStatus(getObjectId(), effect, reflection, skill);
            if (con != null)
            {
                this.map.broadcastMessage(con, this.reflectpack, getTruePosition());
                con.getClient().getSession().write(this.reflectpack);
            }
            else
            {
                this.map.broadcastMessage(this.reflectpack, getTruePosition());
            }
        }
        else
        {
            for (Map.Entry<MonsterStatus, Integer> status1 : effect.entrySet())
            {
                if (con != null)
                {
                    this.map.broadcastMessage(con, MobPacket.applyMonsterStatus(getObjectId(), status1.getKey(), status1.getValue(), skill), getTruePosition());
                    con.getClient().getSession().write(MobPacket.applyMonsterStatus(getObjectId(), status1.getKey(), status1.getValue(), skill));
                }
                else
                {
                    this.map.broadcastMessage(MobPacket.applyMonsterStatus(getObjectId(), status1.getKey(), status1.getValue(), skill), getTruePosition());
                }
            }
        }
    }

    public void setTempEffectiveness(final Element e, long milli)
    {
        this.stats.setEffectiveness(e, ElementalEffectiveness.虚弱);
        server.Timer.EtcTimer.getInstance().schedule(new Runnable()
        {

            public void run()
            {
                MapleMonster.this.stats.removeEffectiveness(e);
            }
        }, milli);
    }

    public boolean isBuffed(MonsterStatus status)
    {
        if ((status == MonsterStatus.中毒) || (status == MonsterStatus.烈焰喷射))
        {
            return (this.poisons.size() > 0) || (this.mobEffects.containsKey(status));
        }
        return this.mobEffects.containsKey(status);
    }

    public int getStatiSize()
    {
        return this.mobEffects.size() + (this.poisons.size() > 0 ? 1 : 0);
    }

    public ArrayList<MonsterStatusEffect> getAllBuffs()
    {
        ArrayList<MonsterStatusEffect> ret = new ArrayList<>();
        for (MonsterStatusEffect e : this.mobEffects.values())
        {
            ret.add(e);
        }
        this.poisonsLock.readLock().lock();
        try
        {
            for (MonsterStatusEffect e : this.poisons)
            {
                ret.add(e);
            }
        }
        finally
        {
            this.poisonsLock.readLock().unlock();
        }
        return ret;
    }

    public List<Pair<Integer, Integer>> getSkills()
    {
        return this.stats.getSkills();
    }

    public boolean hasSkill(int skillId, int level)
    {
        return this.stats.hasSkill(skillId, level);
    }

    public long getLastSkillUsed(int skillId)
    {
        if (this.usedSkills.containsKey(skillId))
        {
            return this.usedSkills.get(skillId);
        }
        return 0L;
    }

    public void setLastSkillUsed(int skillId, long now, long cooltime)
    {
        switch (skillId)
        {
            case 140:
                this.usedSkills.put(skillId, now + cooltime * 2L);
                this.usedSkills.put(141, now);
                break;
            case 141:
                this.usedSkills.put(skillId, now + cooltime * 2L);
                this.usedSkills.put(140, now + cooltime);
                break;
            default:
                this.usedSkills.put(skillId, now + cooltime);
        }
    }

    public byte getNoSkills()
    {
        return this.stats.getNoSkills();
    }

    public int getBuffToGive()
    {
        return this.stats.getBuffToGive();
    }

    public void doPoison(MonsterStatusEffect status, WeakReference<MapleCharacter> weakChr)
    {
        if (((status.getStati() == MonsterStatus.烈焰喷射) || (status.getStati() == MonsterStatus.中毒)) && (this.poisons.size() <= 0))
        {
            return;
        }
        if ((status.getStati() != MonsterStatus.烈焰喷射) && (status.getStati() != MonsterStatus.中毒) && (!this.mobEffects.containsKey(status.getStati())))
        {
            return;
        }
        if (weakChr == null)
        {
            return;
        }
        long damage = status.getPoisonSchedule();
        boolean shadowWeb = (status.getSkill() == 4111003) || (status.getSkill() == 14111001);
        MapleCharacter chr = weakChr.get();
        boolean cancel = (damage <= 0L) || (chr == null) || (chr.getMapId() != this.map.getId());
        if (damage >= this.hp)
        {
            damage = this.hp - 1L;
            cancel = (!shadowWeb) || (cancel);
        }
        if (!cancel)
        {
            damage(chr, damage, false);
            if (shadowWeb)
            {
                this.map.broadcastMessage(MobPacket.damageMonster(getObjectId(), damage), getTruePosition());
            }
        }
    }

    public ConcurrentEnumMap<MonsterStatus, MonsterStatusEffect> getStati()
    {
        return this.mobEffects;
    }

    public void addEmpty()
    {
        for (MonsterStatus stat : MonsterStatus.values())
        {
            if (stat.isEmpty())
            {
                this.mobEffects.put(stat, new MonsterStatusEffect(stat, 0, 0, null, false));
            }
        }
    }

    public int getStolen()
    {
        return this.stolen;
    }

    public void setStolen(int s)
    {
        this.stolen = s;
    }

    public void handleSteal(MapleCharacter chr)
    {
        double showdown = 100.0D;
        MonsterStatusEffect mse = getBuff(MonsterStatus.挑衅);
        if (mse != null)
        {
            showdown += mse.getX();
        }
        Skill steal = client.SkillFactory.getSkill(4201004);
        int level = chr.getTotalSkillLevel(steal);
        int chServerrate = ChannelServer.getInstance(chr.getClient().getChannel()).getDropRate();
        if ((level > 0) && (!getStats().isBoss()) && (this.stolen == -1) && (steal.getEffect(level).makeChanceResult()))
        {
            MapleMonsterInformationProvider mi = MapleMonsterInformationProvider.getInstance();
            List<MonsterDropEntry> de = mi.retrieveDrop(getId());
            if (de == null)
            {
                this.stolen = 0;
                return;
            }
            List<MonsterDropEntry> dropEntry = new ArrayList(de);
            java.util.Collections.shuffle(dropEntry);

            for (MonsterDropEntry d : dropEntry)
            {
                if ((d.itemId > 0) && (d.questid == 0) && (d.itemId / 10000 != 238) && (server.Randomizer.nextInt(999999) < (int) (10 * d.chance * chServerrate * chr.getDropMod() * (chr.getStat().getDropBuff() / 100.0D) * (showdown / 100.0D))))
                {
                    client.inventory.Item idrop;
                    if (constants.ItemConstants.getInventoryType(d.itemId) == client.inventory.MapleInventoryType.EQUIP)
                    {
                        client.inventory.Equip eq = (client.inventory.Equip) MapleItemInformationProvider.getInstance().getEquipById(d.itemId);
                        idrop = MapleItemInformationProvider.getInstance().randomizeStats(eq);
                    }
                    else
                    {
                        idrop = new client.inventory.Item(d.itemId, (short) 0, (short) (d.Maximum != 1 ? server.Randomizer.nextInt(d.Maximum - d.Minimum) + d.Minimum : 1), (short) 0);
                    }
                    this.stolen = d.itemId;
                    this.map.spawnMobDrop(idrop, this.map.calcDropPos(getPosition(), getTruePosition()), this, chr, (byte) 0, 0);
                    break;
                }
            }
        }
        else
        {
            this.stolen = 0;
        }
    }

    public MonsterStatusEffect getBuff(MonsterStatus status)
    {
        return this.mobEffects.get(status);
    }

    public int getLastNode()
    {
        return this.lastNode;
    }

    public void setLastNode(int lastNode)
    {
        this.lastNode = lastNode;
    }

    public boolean shouldDrop(long now)
    {
        return (this.lastDropTime > 0L) && (this.lastDropTime + this.stats.getDropItemPeriod() * 1000 < now);
    }

    public void doDropItem(long now)
    {
        int itemId;
        switch (getId())
        {
            case 9300061:
                itemId = 4001101;
                break;
            default:
                cancelDropItem();
                return;
        }
        if ((isAlive()) && (this.map != null))
        {
            if (this.shouldDropItem)
            {
                this.map.spawnAutoDrop(itemId, getTruePosition());
            }
            else
            {
                this.shouldDropItem = true;
            }
        }
        this.lastDropTime = now;
    }

    public byte[] getNodePacket()
    {
        return this.nodepack;
    }

    public void setNodePacket(byte[] np)
    {
        this.nodepack = np;
    }

    public void registerKill(long next)
    {
        this.nextKill = (System.currentTimeMillis() + next);
    }

    public boolean shouldKill(long now)
    {
        return (this.nextKill > 0L) && (now > this.nextKill);
    }

    private interface AttackerEntry
    {
        List<MapleMonster.AttackingMapleCharacter> getAttackers();

        void addDamage(MapleCharacter paramMapleCharacter, long paramLong, boolean paramBoolean);

        long getDamage();

        boolean contains(MapleCharacter paramMapleCharacter);

        void killedMob(MapleMap paramMapleMap, int paramInt1, boolean paramBoolean, int paramInt2);
    }

    private static class AttackingMapleCharacter
    {
        private final MapleCharacter attacker;
        private long lastAttackTime;

        public AttackingMapleCharacter(MapleCharacter attacker, long lastAttackTime)
        {
            this.attacker = attacker;
            this.lastAttackTime = lastAttackTime;
        }

        public long getLastAttackTime()
        {
            return this.lastAttackTime;
        }

        public void setLastAttackTime(long lastAttackTime)
        {
            this.lastAttackTime = lastAttackTime;
        }

        public MapleCharacter getAttacker()
        {
            return this.attacker;
        }
    }

    private static final class OnePartyAttacker
    {
        public MapleParty lastKnownParty;
        public long damage;
        public long lastAttackTime;

        public OnePartyAttacker(MapleParty lastKnownParty, long damage)
        {
            this.lastKnownParty = lastKnownParty;
            this.damage = damage;
            this.lastAttackTime = System.currentTimeMillis();
        }
    }

    private final class SingleAttackerEntry implements MapleMonster.AttackerEntry
    {
        private final int chrid;
        private long damage = 0L;
        private long lastAttackTime;

        public SingleAttackerEntry(MapleCharacter from)
        {
            this.chrid = from.getId();
        }

        public List<MapleMonster.AttackingMapleCharacter> getAttackers()
        {
            MapleCharacter chr = MapleMonster.this.map.getCharacterById(this.chrid);
            if (chr != null)
            {
                return java.util.Collections.singletonList(new MapleMonster.AttackingMapleCharacter(chr, this.lastAttackTime));
            }
            return java.util.Collections.emptyList();
        }

        public void addDamage(MapleCharacter from, long damage, boolean updateAttackTime)
        {
            if (this.chrid == from.getId())
            {
                this.damage += damage;
                if (updateAttackTime)
                {
                    this.lastAttackTime = System.currentTimeMillis();
                }
            }
        }

        public long getDamage()
        {
            return this.damage;
        }

        public boolean contains(MapleCharacter chr)
        {
            return this.chrid == chr.getId();
        }

        public void killedMob(MapleMap map, int baseExp, boolean mostDamage, int lastSkill)
        {
            MapleCharacter chr = map.getCharacterById(this.chrid);
            if ((chr != null) && (chr.isAlive()))
            {
                MapleMonster.this.giveExpToCharacter(chr, baseExp, mostDamage, 1, lastSkill);
            }
        }

        public int hashCode()
        {
            return this.chrid;
        }

        public boolean equals(Object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            SingleAttackerEntry other = (SingleAttackerEntry) obj;
            return this.chrid == other.chrid;
        }
    }

    private class PartyAttackerEntry implements MapleMonster.AttackerEntry
    {
        private final Map<Integer, MapleMonster.OnePartyAttacker> attackers = new HashMap(6);
        private final int partyid;
        private long totDamage = 0L;

        public PartyAttackerEntry(int partyid)
        {
            this.partyid = partyid;
        }

        public List<MapleMonster.AttackingMapleCharacter> getAttackers()
        {
            List<MapleMonster.AttackingMapleCharacter> ret = new ArrayList(this.attackers.size());
            for (Map.Entry<Integer, MapleMonster.OnePartyAttacker> entry : this.attackers.entrySet())
            {
                MapleCharacter chr = MapleMonster.this.map.getCharacterById(entry.getKey());
                if (chr != null)
                {
                    ret.add(new MapleMonster.AttackingMapleCharacter(chr, entry.getValue().lastAttackTime));
                }
            }
            return ret;
        }

        public void addDamage(MapleCharacter from, long damage, boolean updateAttackTime)
        {
            MapleMonster.OnePartyAttacker oldPartyAttacker = this.attackers.get(from.getId());
            if (oldPartyAttacker != null)
            {
                oldPartyAttacker.damage += damage;
                oldPartyAttacker.lastKnownParty = from.getParty();
                if (updateAttackTime)
                {
                    oldPartyAttacker.lastAttackTime = System.currentTimeMillis();
                }


            }
            else
            {
                MapleMonster.OnePartyAttacker onePartyAttacker = new MapleMonster.OnePartyAttacker(from.getParty(), damage);
                this.attackers.put(from.getId(), onePartyAttacker);
                if (!updateAttackTime)
                {
                    onePartyAttacker.lastAttackTime = 0L;
                }
            }
            this.totDamage += damage;
        }

        public long getDamage()
        {
            return this.totDamage;
        }

        public boolean contains(MapleCharacter chr)
        {
            return this.attackers.containsKey(chr.getId());
        }

        public void killedMob(MapleMap map, int baseExp, boolean mostDamage, int lastSkill)
        {
            MapleCharacter highest = null;
            long highestDamage = 0L;


            Map<MapleCharacter, Integer> expMap = new HashMap(6);

            for (Map.Entry<MapleCharacter, OnePartyAttacker> mapleCharacterOnePartyAttackerEntry : resolveAttackers().entrySet())
            {
                Map.Entry attacker = mapleCharacterOnePartyAttackerEntry;
                MapleParty party = ((OnePartyAttacker) attacker.getValue()).lastKnownParty;
                double addedPartyLevel = 0.0D;
                List<MapleCharacter> expApplicable = new ArrayList<>();
                for (MaplePartyCharacter partychar : party.getMembers())
                {
                    if ((((MapleCharacter) attacker.getKey()).getLevel() - partychar.getLevel() <= 5) || (MapleMonster.this.stats.getLevel() - partychar.getLevel() <= 5))
                    {
                        MapleCharacter pchr = map.getCharacterById(partychar.getId());
                        if ((pchr != null) && (pchr.isAlive()) && (pchr.getMap() == map))
                        {
                            expApplicable.add(pchr);
                            addedPartyLevel += pchr.getLevel();
                        }
                    }
                }
                double expBonus = 1.0D;
                if (expApplicable.size() > 1)
                {
                    expBonus = 1.1D + 0.05D * expApplicable.size();
                    addedPartyLevel /= expApplicable.size();
                }
                long iDamage = ((OnePartyAttacker) attacker.getValue()).damage;
                if (iDamage > highestDamage)
                {
                    highest = (MapleCharacter) attacker.getKey();
                    highestDamage = iDamage;
                }
                double innerBaseExp = baseExp * (iDamage / this.totDamage);
                double expFraction = innerBaseExp * expBonus / (expApplicable.size() + 1);

                for (MapleCharacter expReceiver : expApplicable)
                {
                    Integer oexp = expMap.get(expReceiver);
                    int iexp;
                    if (oexp == null)
                    {
                        iexp = 0;
                    }
                    else
                    {
                        iexp = oexp;
                    }
                    double expWeight = expReceiver == attacker.getKey() ? 2.0D : 1.0D;
                    double levelMod = expReceiver.getLevel() / addedPartyLevel;
                    if ((levelMod > 1.0D) || (this.attackers.containsKey(expReceiver.getId())))
                    {
                        levelMod = 1.0D;
                    }
                    iexp += (int) Math.round(expFraction * expWeight * levelMod);
                    expMap.put(expReceiver, iexp);
                }
            }
            Map.Entry<MapleCharacter, MapleMonster.OnePartyAttacker> attacker;
            double addedPartyLevel;
            double expFraction;
            for (Map.Entry<MapleCharacter, Integer> expReceiver : expMap.entrySet())
            {
                MapleMonster.this.giveExpToCharacter(expReceiver.getKey(), expReceiver.getValue(), expReceiver.getKey() == highest, expMap.size(), lastSkill);
            }
        }

        private Map<MapleCharacter, MapleMonster.OnePartyAttacker> resolveAttackers()
        {
            Map<MapleCharacter, MapleMonster.OnePartyAttacker> ret = new HashMap(this.attackers.size());
            for (Map.Entry<Integer, MapleMonster.OnePartyAttacker> aentry : this.attackers.entrySet())
            {
                MapleCharacter chr = MapleMonster.this.map.getCharacterById(aentry.getKey());
                if (chr != null)
                {
                    ret.put(chr, aentry.getValue());
                }
            }
            return ret;
        }

        public int hashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + this.partyid;
            return result;
        }

        public boolean equals(Object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            PartyAttackerEntry other = (PartyAttackerEntry) obj;
            return this.partyid == other.partyid;
        }
    }
}