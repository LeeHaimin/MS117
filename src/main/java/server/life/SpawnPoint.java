package server.life;

import java.awt.Point;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import client.SkillFactory;
import client.status.MonsterStatus;
import client.status.MonsterStatusEffect;
import server.MapleCarnivalFactory;
import server.MapleCarnivalFactory.MCSkill;
import server.MapleStatEffect;
import server.maps.MapleMap;
import server.maps.MapleReactor;
import server.maps.MapleSummon;
import tools.MaplePacketCreator;

public class SpawnPoint extends Spawns
{
    private final AtomicInteger spawnedMonsters = new AtomicInteger(0);
    private final MapleMonsterStats monster;
    private final Point pos;
    private final int mobTime;
    private final int fh;
    private final int f;
    private final int id;
    private final String msg;
    private final byte carnivalTeam;
    private int carnival = -1;
    private int level = -1;
    private long nextPossibleSpawn;

    public SpawnPoint(MapleMonster monster, Point pos, int mobTime, byte carnivalTeam, String msg)
    {
        this.monster = monster.getStats();
        this.pos = pos;
        this.id = monster.getId();
        this.fh = monster.getFh();
        this.f = monster.getF();
        this.mobTime = (mobTime < 0 ? -1 : mobTime * 1000);
        this.carnivalTeam = carnivalTeam;
        this.msg = msg;
        this.nextPossibleSpawn = System.currentTimeMillis();
    }

    public void setCarnival(int c)
    {
        this.carnival = c;
    }

    public void setLevel(int c)
    {
        this.level = c;
    }

    public MapleMonsterStats getMonster()
    {
        return this.monster;
    }

    public byte getCarnivalTeam()
    {
        return this.carnivalTeam;
    }

    public boolean shouldSpawn(long time)
    {
        if (this.mobTime < 0)
        {
            return false;
        }


        if (((this.mobTime == 0) && (this.monster.isMobile())) || ((this.spawnedMonsters.get() > 0) || (this.spawnedMonsters.get() > 1)))
        {
            return false;
        }
        return this.nextPossibleSpawn <= time;
    }

    public int getCarnivalId()
    {
        return this.carnival;
    }

    public MapleMonster spawnMonster(MapleMap map)
    {
        MapleMonster mob = new MapleMonster(this.id, this.monster);
        mob.setPosition(this.pos);
        mob.setCy(this.pos.y);
        mob.setRx0(this.pos.x - 50);
        mob.setRx1(this.pos.x + 50);
        mob.setFh(this.fh);
        mob.setF(this.f);
        mob.setCarnivalTeam(this.carnivalTeam);
        if (this.level > -1)
        {
            mob.changeLevel(this.level);
        }
        this.spawnedMonsters.incrementAndGet();
        mob.addListener(new MonsterListener()
        {
            public void monsterKilled()
            {
                SpawnPoint.this.nextPossibleSpawn = System.currentTimeMillis();

                if (SpawnPoint.this.mobTime > 0)
                {
                    SpawnPoint.this.nextPossibleSpawn = (SpawnPoint.this.nextPossibleSpawn + SpawnPoint.this.mobTime);
                }
                SpawnPoint.this.spawnedMonsters.decrementAndGet();
            }
        });
        map.spawnMonster(mob, -2);
        if (this.carnivalTeam > -1)
        {
            for (MapleReactor r : map.getAllReactorsThreadsafe())
            {
                if ((r.getName().startsWith(String.valueOf(this.carnivalTeam))) && (r.getReactorId() == 9980000 + this.carnivalTeam) && (r.getState() < 5))
                {
                    int num = Integer.parseInt(r.getName().substring(1, 2));
                    MCSkill skil = MapleCarnivalFactory.getInstance().getGuardian(num);
                    if (skil != null) skil.getSkill().applyEffect(null, mob, false);
                }
            }
        }
        MapleCarnivalFactory.MCSkill skil;
        for (MapleSummon s : map.getAllSummonsThreadsafe())
        {
            if (s.getSkillId() == 35111005)
            {
                MapleStatEffect effect = SkillFactory.getSkill(s.getSkillId()).getEffect(s.getSkillLevel());
                for (Map.Entry<MonsterStatus, Integer> stat : effect.getMonsterStati().entrySet())
                {
                    mob.applyStatus(s.getOwner(), new MonsterStatusEffect(stat.getKey(), stat.getValue(), s.getSkillId(), null, false), false, effect.getDuration(), true, effect);
                }
                break;
            }
        }
        if (this.msg != null)
        {
            map.broadcastMessage(MaplePacketCreator.serverNotice(6, this.msg));
        }
        return mob;
    }

    public int getMobTime()
    {
        return this.mobTime;
    }

    public Point getPosition()
    {
        return this.pos;
    }

    public int getF()
    {
        return this.f;
    }

    public int getFh()
    {
        return this.fh;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\life\SpawnPoint.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */