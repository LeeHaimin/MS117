package server.life;

import constants.GameConstants;

public class ChangeableStats extends OverrideMonsterStats
{
    public final int watk;
    public final int matk;
    public final int acc;
    public final int eva;
    public final int PDRate;
    public final int MDRate;
    public final int pushed;
    public final int level;

    public ChangeableStats(MapleMonsterStats stats, OverrideMonsterStats ostats)
    {
        this.hp = ostats.getHp();
        this.exp = ostats.getExp();
        this.mp = ostats.getMp();
        this.watk = stats.getPhysicalAttack();
        this.matk = stats.getMagicAttack();
        this.acc = stats.getAcc();
        this.eva = stats.getEva();
        this.PDRate = stats.getPDRate();
        this.MDRate = stats.getMDRate();
        this.pushed = stats.getPushed();
        this.level = stats.getLevel();
    }

    public ChangeableStats(MapleMonsterStats stats, int newLevel, boolean pqMob)
    {
        double mod = newLevel / stats.getLevel();
        double hpRatio = stats.getHp() / stats.getExp();
        double pqMod = pqMob ? 2.5D : 1.0D;
        this.hp = Math.round((!stats.isBoss() ? GameConstants.getMonsterHP(newLevel) : stats.getHp() * mod) * pqMod);
        this.exp = ((int) Math.round(stats.getExp() * mod * pqMod * 0.8D));
        this.mp = ((int) Math.round(stats.getMp() * mod * pqMod));
        this.watk = ((int) Math.round(stats.getPhysicalAttack() * mod));
        this.matk = ((int) Math.round(stats.getMagicAttack() * mod));
        this.acc = Math.round(stats.getAcc() + Math.max(0, newLevel - stats.getLevel()) * 2);
        this.eva = Math.round(stats.getEva() + Math.max(0, newLevel - stats.getLevel()));
        this.PDRate = Math.min(stats.isBoss() ? 30 : 20, (int) Math.round(stats.getPDRate() * mod));
        this.MDRate = Math.min(stats.isBoss() ? 30 : 20, (int) Math.round(stats.getMDRate() * mod));
        this.pushed = ((int) Math.round(stats.getPushed() * mod));
        this.level = (newLevel > 250 ? 250 : newLevel);
    }

    public ChangeableStats(MapleMonsterStats stats, int newLevel, int multipler)
    {
        double base = newLevel / stats.getLevel() * multipler;
        this.hp = Math.round(stats.getHp() * base * (!stats.isBoss() ? 1.0D : 3.0D));
        this.mp = ((int) Math.round(stats.getMp() * base));
        this.exp = ((int) (stats.getExp() * base));
        this.watk = ((int) Math.round(stats.getPhysicalAttack() * base));
        this.matk = ((int) Math.round(stats.getMagicAttack() * base));
        this.acc = ((int) Math.round(stats.getAcc() + Math.max(0, newLevel - stats.getLevel()) * base));
        this.eva = ((int) Math.round(stats.getEva() + Math.max(0, newLevel - stats.getLevel()) * base));
        this.PDRate = Math.min(stats.isBoss() ? 30 : 20, (int) Math.round(stats.getPDRate() * base));
        this.MDRate = Math.min(stats.isBoss() ? 30 : 20, (int) Math.round(stats.getMDRate() * base));
        this.pushed = ((int) Math.round(stats.getPushed() * base));
        this.level = (newLevel > 250 ? 250 : newLevel);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\life\ChangeableStats.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */