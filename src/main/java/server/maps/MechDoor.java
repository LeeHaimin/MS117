package server.maps;

import java.awt.Point;

import client.MapleCharacter;
import client.MapleClient;
import tools.MaplePacketCreator;

public class MechDoor extends MapleMapObject
{
    private final int owner;
    private final int partyid;
    private final int id;

    public MechDoor(MapleCharacter owner, Point pos, int id)
    {
        this.owner = owner.getId();
        this.partyid = (owner.getParty() == null ? 0 : owner.getParty().getId());
        setPosition(pos);
        this.id = id;
    }

    public int getOwnerId()
    {
        return this.owner;
    }

    public int getPartyId()
    {
        return this.partyid;
    }

    public int getId()
    {
        return this.id;
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.DOOR;
    }

    public void sendSpawnData(MapleClient client)
    {
        client.getSession().write(MaplePacketCreator.spawnMechDoor(this, false));
    }

    public void sendDestroyData(MapleClient client)
    {
        client.getSession().write(MaplePacketCreator.removeMechDoor(this, false));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MechDoor.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */