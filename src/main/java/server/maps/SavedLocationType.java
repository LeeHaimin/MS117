package server.maps;

public enum SavedLocationType
{
    FREE_MARKET(0), MULUNG_TC(1), WORLDTOUR(2), FLORINA(3), FISHING(4), RICHIE(5), DONGDONGCHIANG(6), EVENT(7), AMORIA(8), CHRISTMAS(9), ARDENTMILL(10), TURNEGG(11), PVP(12), GUILD(13), FAMILY(14),
    MonsterPark(15), ROOT(16);

    private final int index;

    SavedLocationType(int index)
    {
        this.index = index;
    }

    public static SavedLocationType fromString(String Str)
    {
        return valueOf(Str);
    }

    public int getValue()
    {
        return this.index;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\SavedLocationType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */