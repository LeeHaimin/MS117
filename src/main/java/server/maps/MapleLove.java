package server.maps;

import java.awt.Point;

import client.MapleCharacter;
import client.MapleClient;
import tools.MaplePacketCreator;


public class MapleLove extends MapleMapObject
{
    private final Point pos;
    private final MapleCharacter owner;
    private final String text;
    private final int ft;
    private final int itemid;

    public MapleLove(MapleCharacter owner, Point pos, int ft, String text, int itemid)
    {
        this.owner = owner;
        this.pos = pos;
        this.text = text;
        this.ft = ft;
        this.itemid = itemid;
    }

    public Point getPosition()
    {
        return this.pos.getLocation();
    }

    public void setPosition(Point position)
    {
        throw new UnsupportedOperationException();
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.LOVE;
    }

    public void sendSpawnData(MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.spawnLove(getObjectId(), this.itemid, this.owner.getName(), this.text, this.pos, this.ft));
    }

    public void sendDestroyData(MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.removeLove(getObjectId(), this.itemid));
    }

    public MapleCharacter getOwner()
    {
        return this.owner;
    }

    public int getItemId()
    {
        return this.itemid;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MapleLove.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */