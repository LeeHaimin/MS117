package server.maps.pvp;

import java.awt.Point;
import java.awt.Rectangle;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.Skill;
import client.SkillFactory;
import handling.channel.handler.AttackInfo;
import handling.world.WorldBroadcastService;
import handling.world.WorldGuildService;
import server.MapleStatEffect;
import server.Randomizer;
import server.life.MapleLifeFactory;
import server.life.MapleMonster;
import server.maps.MapleMap;
import tools.MaplePacketCreator;


public class MaplePvp
{
    public static boolean inArea(MapleCharacter chr)
    {
        for (Rectangle rect : chr.getMap().getAreas())
        {
            if (rect.contains(chr.getTruePosition()))
            {
                return true;
            }
        }
        return false;
    }

    public static synchronized void doPvP(MapleCharacter player, MapleMap map, AttackInfo attack, MapleStatEffect effect)
    {
        PvpAttackInfo pvpAttack = parsePvpAttack(attack, player, effect);
        int mobCount = 0;
        for (MapleCharacter attacked : player.getMap().getCharactersIntersect(pvpAttack.box))
        {
            if ((attacked.getId() != player.getId()) && (attacked.isAlive()) && (!attacked.isHidden()) && (mobCount < pvpAttack.mobCount))
            {
                mobCount++;
                monsterBomb(player, attacked, map, pvpAttack);
            }
        }
    }

    private static PvpAttackInfo parsePvpAttack(AttackInfo attack, MapleCharacter player, MapleStatEffect effect)
    {
        PvpAttackInfo ret = new PvpAttackInfo();
        double maxdamage = player.getLevel() + 100.0D;
        int skillId = attack.skillId;
        ret.skillId = skillId;
        ret.critRate = 5;
        ret.ignoreDef = 0;
        ret.skillDamage = 100;
        ret.mobCount = 1;
        ret.attackCount = 1;
        int pvpRange = attack.isCloseRangeAttack ? 35 : 70;
        ret.facingLeft = (attack.direction < 0);
        if ((skillId != 0) && (effect != null))
        {
            ret.critRate += effect.getCritical();
            ret.ignoreDef += effect.getIgnoreMob();
            ret.skillDamage = (effect.getDamage() + player.getStat().getDamageIncrease(skillId));
            ret.mobCount = Math.max(1, effect.getMobCount(player));
            ret.attackCount = Math.max(effect.getBulletCount(player), effect.getAttackCount(player));
            ret.box = effect.calculateBoundingBox(player.getTruePosition(), ret.facingLeft, pvpRange);
        }
        else
        {
            ret.box = calculateBoundingBox(player.getTruePosition(), ret.facingLeft, pvpRange);
        }
        boolean mirror = (player.getBuffedValue(MapleBuffStat.影分身) != null) || (player.getBuffedIntValue(MapleBuffStat.月光转换) == 1);
        ret.attackCount *= (mirror ? 2 : 1);
        maxdamage *= ret.skillDamage / 100.0D;
        ret.maxDamage = (maxdamage * ret.attackCount);
        if (player.isShowPacket())
        {
            player.dropSpouseMessage(10,
                    "Pvp伤害解析 - 最大攻击: " + maxdamage + " 数量: " + ret.mobCount + " 次数: " + ret.attackCount + " 爆击: " + ret.critRate + " 无视: " + ret.ignoreDef + " 技能伤害: " + ret.skillDamage);
        }
        return ret;
    }

    private static void monsterBomb(MapleCharacter player, MapleCharacter attacked, MapleMap map, PvpAttackInfo attack)
    {
        if ((player == null) || (attacked == null) || (map == null))
        {
            return;
        }
        double maxDamage = attack.maxDamage;
        boolean isCritDamage = false;

        if (player.getLevel() > attacked.getLevel() + 10)
        {
            maxDamage *= 1.05D;
        }
        else if (player.getLevel() < attacked.getLevel() - 10)
        {
            maxDamage /= 1.05D;
        }
        else if (player.getLevel() > attacked.getLevel() + 20)
        {
            maxDamage *= 1.1D;
        }
        else if (player.getLevel() < attacked.getLevel() - 20)
        {
            maxDamage /= 1.1D;
        }
        else if (player.getLevel() > attacked.getLevel() + 30)
        {
            maxDamage *= 1.15D;
        }
        else if (player.getLevel() < attacked.getLevel() - 30)
        {
            maxDamage /= 1.15D;
        }

        if (Randomizer.nextInt(100) < attack.critRate)
        {
            maxDamage *= 1.5D;
            isCritDamage = true;
        }
        int attackedDamage = (int) Math.floor(Math.random() * ((int) maxDamage * 0.35D) + (int) maxDamage * 0.65D);
        int MAX_PVP_DAMAGE = (int) (player.getStat().getLimitBreak(player) / 100.0D);
        int MIN_PVP_DAMAGE = 100;
        if (attackedDamage > MAX_PVP_DAMAGE)
        {
            attackedDamage = MAX_PVP_DAMAGE;
        }
        if (attackedDamage < MIN_PVP_DAMAGE)
        {
            attackedDamage = MIN_PVP_DAMAGE;
        }
        int hploss = attackedDamage;
        int mploss = 0;
        if (attackedDamage > 0)
        {
            if (attacked.getBuffedValue(MapleBuffStat.魔法盾) != null)
            {
                mploss = (int) (attackedDamage * (attacked.getBuffedValue(MapleBuffStat.魔法盾).doubleValue() / 100.0D));
                hploss -= mploss;
                if (attacked.getBuffedValue(MapleBuffStat.终极无限) != null)
                {
                    mploss = 0;
                }
                else if (mploss > attacked.getStat().getMp())
                {
                    mploss = attacked.getStat().getMp();
                    hploss -= mploss;
                }
                attacked.addMPHP(-hploss, -mploss);
            }
            else if (attacked.getTotalSkillLevel(27000003) > 0)
            {
                Skill skill = SkillFactory.getSkill(27000003);
                MapleStatEffect effect = skill.getEffect(attacked.getTotalSkillLevel(27000003));
                mploss = (int) (attackedDamage * (effect.getX() / 100.0D));
                hploss -= mploss;
                if (mploss > attacked.getStat().getMp())
                {
                    mploss = attacked.getStat().getMp();
                    hploss -= mploss;
                }
                attacked.addMPHP(-hploss, -mploss);
            }
            else if (attacked.getStat().mesoGuardMeso > 0.0D)
            {
                hploss = (int) Math.ceil(attackedDamage * attacked.getStat().mesoGuard / 100.0D);
                int mesoloss = (int) (attackedDamage * (attacked.getStat().mesoGuardMeso / 100.0D));
                if (attacked.getMeso() < mesoloss)
                {
                    attacked.gainMeso(-attacked.getMeso(), false);
                    attacked.cancelBuffStats(MapleBuffStat.金钱护盾);
                }
                else
                {
                    attacked.gainMeso(-mesoloss, false);
                }
                attacked.addHP(-hploss);
            }
            else
            {
                attacked.addHP(-hploss);
            }
        }
        MapleMonster pvpMob = MapleLifeFactory.getMonster(9400711);
        map.spawnMonsterOnGroundBelow(pvpMob, attacked.getPosition());
        map.broadcastMessage(MaplePacketCreator.damagePlayer(attacked.getId(), 2, pvpMob.getId(), hploss));
        if (isCritDamage)
        {
            player.dropMessage(6, "你对玩家 " + attacked.getName() + " 造成了 " + hploss + " 点爆击伤害! 对方血量: " + attacked.getStat().getHp() + "/" + attacked.getStat().getCurrentMaxHp());
            attacked.dropMessage(6, "玩家 " + player.getName() + " 对你造成了 " + hploss + " 点爆击伤害!");
        }
        else
        {
            player.dropTopMsg("你对玩家 " + attacked.getName() + " 造成了 " + hploss + " 点伤害! 对方血量: " + attacked.getStat().getHp() + "/" + attacked.getStat().getCurrentMaxHp());
            attacked.dropTopMsg("玩家 " + player.getName() + " 对你造成了 " + hploss + " 点伤害!");
        }
        map.killMonster(pvpMob, player, false, false, (byte) 1);

        if ((attacked.getStat().getHp() <= 0) && (!attacked.isAlive()))
        {
            int expReward = attacked.getLevel() * 10 * (attacked.getLevel() / player.getLevel());
            int gpReward = (int) Math.floor(Math.random() * 10.0D + 10.0D);
            if (player.getPvpKills() * 0.25D >= player.getPvpDeaths())
            {
                expReward *= 2;
            }
            player.gainExp(expReward, true, false, true);
            if ((player.getGuildId() > 0) && (player.getGuildId() != attacked.getGuildId()))
            {
                WorldGuildService.getInstance().gainGP(player.getGuildId(), gpReward);
            }
            player.gainPvpKill();
            player.dropMessage(6, "你击败了玩家 " + attacked.getName() + "!! ");
            int pvpVictory = attacked.getPvpVictory();
            attacked.gainPvpDeath();
            attacked.dropMessage(6, player.getName() + " 将你击败!");
            byte[] packet = MaplePacketCreator.spouseMessage(10, "[Pvp] 玩家 " + player.getName() + " 终结了 " + attacked.getName() + " 的 " + pvpVictory + " 连斩。");
            if ((pvpVictory >= 5) && (pvpVictory < 10))
            {
                map.broadcastMessage(packet);
            }
            else if ((pvpVictory >= 10) && (pvpVictory < 20))
            {
                player.getClient().getChannelServer().broadcastMessage(packet);
            }
            else if (pvpVictory >= 20)
            {
                WorldBroadcastService.getInstance().broadcastMessage(packet);
            }
        }
    }

    private static Rectangle calculateBoundingBox(Point posFrom, boolean facingLeft, int range)
    {
        Point lt = new Point(-70, -30);
        Point rb = new Point(-10, 0);
        Point myrb;
        Point mylt;
        if (facingLeft)
        {
            mylt = new Point(lt.x + posFrom.x - range, lt.y + posFrom.y);
            myrb = new Point(rb.x + posFrom.x, rb.y + posFrom.y);
        }
        else
        {
            myrb = new Point(lt.x * -1 + posFrom.x + range, rb.y + posFrom.y);
            mylt = new Point(rb.x * -1 + posFrom.x, lt.y + posFrom.y);
        }
        return new Rectangle(mylt.x, mylt.y, myrb.x - mylt.x, myrb.y - mylt.y);
    }

    public static synchronized void doPartyPvP(MapleCharacter player, MapleMap map, AttackInfo attack, MapleStatEffect effect)
    {
        PvpAttackInfo pvpAttack = parsePvpAttack(attack, player, effect);
        int mobCount = 0;
        for (MapleCharacter attacked : player.getMap().getCharactersIntersect(pvpAttack.box))
        {
            if ((attacked.getId() != player.getId()) && (attacked.isAlive()) && (!attacked.isHidden()) && ((player.getParty() == null) || (player.getParty() != attacked.getParty())) && (mobCount < pvpAttack.mobCount))
            {
                mobCount++;
                monsterBomb(player, attacked, map, pvpAttack);
            }
        }
    }

    public static synchronized void doGuildPvP(MapleCharacter player, MapleMap map, AttackInfo attack, MapleStatEffect effect)
    {
        PvpAttackInfo pvpAttack = parsePvpAttack(attack, player, effect);
        int mobCount = 0;
        for (MapleCharacter attacked : player.getMap().getCharactersIntersect(pvpAttack.box))
        {
            if ((attacked.getId() != player.getId()) && (attacked.isAlive()) && (!attacked.isHidden()) && ((player.getGuildId() == 0) || (player.getGuildId() != attacked.getGuildId())) && (mobCount < pvpAttack.mobCount))
            {
                mobCount++;
                monsterBomb(player, attacked, map, pvpAttack);
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\pvp\MaplePvp.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */