package server.maps.pvp;

import java.awt.Rectangle;

public class PvpAttackInfo
{
    public int skillId;
    public int critRate;
    public int ignoreDef;
    public int skillDamage;
    public int mobCount;
    public int attackCount;
    public int pvpRange;
    public boolean facingLeft;
    public double maxDamage;
    public Rectangle box;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\pvp\PvpAttackInfo.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */