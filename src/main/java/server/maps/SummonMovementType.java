package server.maps;

public enum SummonMovementType
{
    不会移动(0), 跟随移动(1), 自由移动(2), 跟随并且随机移动打怪(4), CIRCLE_STATIONARY(5), 未知效果(7);

    private final int val;

    SummonMovementType(int val)
    {
        this.val = val;
    }

    public int getValue()
    {
        return this.val;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\SummonMovementType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */