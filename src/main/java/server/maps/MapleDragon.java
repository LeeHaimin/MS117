package server.maps;

import client.MapleCharacter;
import client.MapleClient;

public class MapleDragon extends AnimatedMapleMapObject
{
    private final int owner;
    private final int jobid;

    public MapleDragon(MapleCharacter owner)
    {
        this.owner = owner.getId();
        this.jobid = owner.getJob();
        if ((this.jobid < 2200) || (this.jobid > 2218))
        {
            throw new RuntimeException("试图生成1个龙龙的信息，但角色不是龙神职业.");
        }
        setPosition(owner.getTruePosition());
        setStance(4);
    }

    public int getOwner()
    {
        return this.owner;
    }

    public int getJobId()
    {
        return this.jobid;
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.SUMMON;
    }

    public void sendSpawnData(MapleClient client)
    {
        client.getSession().write(tools.MaplePacketCreator.spawnDragon(this));
    }

    public void sendDestroyData(MapleClient client)
    {
        client.getSession().write(tools.MaplePacketCreator.removeDragon(this.owner));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MapleDragon.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */