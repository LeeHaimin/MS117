package server.maps;

public abstract class AnimatedMapleMapObject extends MapleMapObject
{
    private int stance;

    public boolean isFacingLeft()
    {
        return getStance() % 2 != 0;
    }

    public int getStance()
    {
        return this.stance;
    }

    public void setStance(int stance)
    {
        this.stance = stance;
    }

    public int getFacingDirection()
    {
        return Math.abs(getStance() % 2);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\AnimatedMapleMapObject.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */