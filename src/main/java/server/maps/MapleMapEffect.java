package server.maps;

import client.MapleClient;
import tools.MaplePacketCreator;
import tools.packet.MTSCSPacket;

public class MapleMapEffect
{
    private String msg = "";
    private int itemId = 0;
    private int effectType = -1;
    private boolean active = true;
    private boolean jukebox = false;

    public MapleMapEffect(String msg, int itemId)
    {
        this.msg = msg;
        this.itemId = itemId;
        this.effectType = -1;
    }

    public MapleMapEffect(String msg, int itemId, int effectType)
    {
        this.msg = msg;
        this.itemId = itemId;
        this.effectType = effectType;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isJukebox()
    {
        return this.jukebox;
    }

    public void setJukebox(boolean actie)
    {
        this.jukebox = actie;
    }

    public byte[] makeDestroyData()
    {
        return this.jukebox ? MTSCSPacket.playCashSong(0, "") : MaplePacketCreator.removeMapEffect();
    }

    public void sendStartData(MapleClient c)
    {
        c.getSession().write(makeStartData());
    }

    public byte[] makeStartData()
    {
        return this.jukebox ? MTSCSPacket.playCashSong(this.itemId, this.msg) : MaplePacketCreator.startMapEffect(this.msg, this.itemId, this.effectType, this.active);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MapleMapEffect.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */