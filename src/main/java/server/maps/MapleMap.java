package server.maps;

import java.awt.Point;
import java.awt.Rectangle;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleClient;
import client.MonsterFamiliar;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePet;
import client.status.MonsterStatus;
import constants.GameConstants;
import constants.ItemConstants;
import constants.ServerConstants;
import handling.channel.ChannelServer;
import handling.world.WorldBroadcastService;
import handling.world.party.ExpeditionType;
import scripting.event.EventManager;
import server.MapleItemInformationProvider;
import server.MaplePortal;
import server.MapleStatEffect;
import server.Randomizer;
import server.SpeedRunner;
import server.Timer;
import server.events.MapleEvent;
import server.life.MapleLifeFactory;
import server.life.MapleMonster;
import server.life.MapleMonsterInformationProvider;
import server.life.MapleNPC;
import server.life.MonsterDropEntry;
import server.life.MonsterGlobalDropEntry;
import server.life.SpawnPoint;
import server.life.Spawns;
import server.squad.MapleSquad;
import server.squad.MapleSquadType;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.packet.InventoryPacket;
import tools.packet.MTSCSPacket;
import tools.packet.MobPacket;
import tools.packet.NPCPacket;
import tools.packet.PetPacket;
import tools.packet.UIPacket;

public final class MapleMap
{
    private final Map<MapleMapObjectType, LinkedHashMap<Integer, MapleMapObject>> mapobjects;
    private final Map<MapleMapObjectType, ReentrantReadWriteLock> mapobjectlocks;
    private final List<MapleCharacter> characters = new ArrayList<>();
    private final ReentrantReadWriteLock charactersLock = new ReentrantReadWriteLock();
    private final Lock runningOidLock = new ReentrantLock();
    private final List<Spawns> monsterSpawn = new ArrayList<>();
    private final AtomicInteger spawnedMonstersOnMap = new AtomicInteger(0);
    private final Map<Integer, MaplePortal> portals = new HashMap<>();
    private final float monsterRate;
    private final int channel;
    private final int mapid;
    private final List<Integer> dced = new ArrayList<>();
    private final Map<String, Integer> environment = new LinkedHashMap<>();
    private int runningOid = 500000;
    private MapleFootholdTree footholds = null;
    private float recoveryRate;
    private MapleMapEffect mapEffect;
    private short decHP = 0;
    private short createMobInterval = 9000;
    private short top = 0;
    private short bottom = 0;
    private short left = 0;
    private short right = 0;
    private int consumeItemCoolTime = 0;
    private int protectItem = 0;
    private int decHPInterval = 10000;
    private int returnMapId;
    private int timeLimit;
    private int fieldLimit;
    private int maxRegularSpawn = 0;
    private int fixedMob;
    private int forcedReturnMap = 999999999;
    private int instanceid = -1;
    private int lvForceMove = 0;
    private int lvLimit = 0;
    private int permanentWeather = 0;
    private int partyBonusRate = 0;
    private boolean town;
    private boolean clock;
    private boolean personalShop;
    private boolean everlast = false;
    private boolean dropsDisabled = false;
    private boolean gDropsDisabled = false;
    private boolean soaring = false;
    private boolean squadTimer = false;
    private boolean isSpawns = true;
    private boolean checkStates = true;
    private String mapName;
    private String streetName;
    private String onUserEnter;
    private String onFirstUserEnter;
    private String speedRunLeader = "";
    private List<Point> spawnPoints = new ArrayList<>();
    private ScheduledFuture<?> squadSchedule;
    private long speedRunStart = 0L;
    private long lastSpawnTime = 0L;
    private long lastHurtTime = 0L;
    private MapleNodes nodes;
    private MapleSquadType squad;
    private int fieldType;

    public MapleMap(int mapid, int channel, int returnMapId, float monsterRate)
    {
        this.mapid = mapid;
        this.channel = channel;
        this.returnMapId = returnMapId;
        if (this.returnMapId == 999999999)
        {
            this.returnMapId = mapid;
        }
        if (GameConstants.getPartyPlay(mapid) > 0)
        {
            this.monsterRate = ((monsterRate - 1.0F) * 2.5F + 1.0F);
        }
        else
        {
            this.monsterRate = monsterRate;
        }
        EnumMap<MapleMapObjectType, LinkedHashMap<Integer, MapleMapObject>> objsMap = new EnumMap(MapleMapObjectType.class);
        EnumMap<MapleMapObjectType, ReentrantReadWriteLock> objlockmap = new EnumMap(MapleMapObjectType.class);
        for (MapleMapObjectType type : MapleMapObjectType.values())
        {
            objsMap.put(type, new LinkedHashMap());
            objlockmap.put(type, new ReentrantReadWriteLock());
        }
        this.mapobjects = Collections.unmodifiableMap(objsMap);
        this.mapobjectlocks = Collections.unmodifiableMap(objlockmap);
    }

    public boolean getSpawns()
    {
        return this.isSpawns;
    }

    public void setSpawns(boolean fm)
    {
        this.isSpawns = fm;
    }

    public void setFixedMob(int fm)
    {
        this.fixedMob = fm;
    }

    public int getForceMove()
    {
        return this.lvForceMove;
    }

    public void setForceMove(int fm)
    {
        this.lvForceMove = fm;
    }

    public int getLevelLimit()
    {
        return this.lvLimit;
    }

    public void setLevelLimit(int fm)
    {
        this.lvLimit = fm;
    }

    public void setSoaring(boolean b)
    {
        this.soaring = b;
    }

    public boolean canSoar()
    {
        return this.soaring;
    }

    public void toggleDrops()
    {
        this.dropsDisabled = (!this.dropsDisabled);
    }

    public void setDrops(boolean b)
    {
        this.dropsDisabled = b;
    }

    public void toggleGDrops()
    {
        this.gDropsDisabled = (!this.gDropsDisabled);
    }

    public int getId()
    {
        return this.mapid;
    }

    public MapleMap getReturnMap()
    {
        return ChannelServer.getInstance(this.channel).getMapFactory().getMap(this.returnMapId);
    }

    public int getReturnMapId()
    {
        return this.returnMapId;
    }

    public void setReturnMapId(int rmi)
    {
        this.returnMapId = rmi;
    }

    public int getForcedReturnId()
    {
        return this.forcedReturnMap;
    }

    public float getRecoveryRate()
    {
        return this.recoveryRate;
    }

    public void setRecoveryRate(float recoveryRate)
    {
        this.recoveryRate = recoveryRate;
    }

    public int getFieldLimit()
    {
        return this.fieldLimit;
    }

    public void setFieldLimit(int fieldLimit)
    {
        this.fieldLimit = fieldLimit;
    }

    public void setCreateMobInterval(short createMobInterval)
    {
        this.createMobInterval = createMobInterval;
    }

    public void setTimeLimit(int timeLimit)
    {
        this.timeLimit = timeLimit;
    }

    public String getMapName()
    {
        return this.mapName;
    }

    public void setMapName(String mapName)
    {
        this.mapName = mapName;
    }

    public String getStreetName()
    {
        return this.streetName;
    }

    public void setStreetName(String streetName)
    {
        this.streetName = streetName;
    }

    public String getFirstUserEnter()
    {
        return this.onFirstUserEnter;
    }

    public void setFirstUserEnter(String onFirstUserEnter)
    {
        this.onFirstUserEnter = onFirstUserEnter;
    }

    public String getUserEnter()
    {
        return this.onUserEnter;
    }

    public void setUserEnter(String onUserEnter)
    {
        this.onUserEnter = onUserEnter;
    }

    public void setClock(boolean hasClock)
    {
        this.clock = hasClock;
    }

    public boolean allowPersonalShop()
    {
        return this.personalShop;
    }

    public void setPersonalShop(boolean personalShop)
    {
        this.personalShop = personalShop;
    }

    public boolean getEverlast()
    {
        return this.everlast;
    }

    public void setEverlast(boolean everlast)
    {
        this.everlast = everlast;
    }

    public int getHPDec()
    {
        return this.decHP;
    }

    public void setHPDec(int delta)
    {
        if ((delta > 0) || (this.mapid == 749040100))
        {
            this.lastHurtTime = System.currentTimeMillis();
        }
        this.decHP = ((short) delta);
    }

    public int getHPDecInterval()
    {
        return this.decHPInterval;
    }

    public void setHPDecInterval(int delta)
    {
        this.decHPInterval = delta;
    }

    public int getHPDecProtect()
    {
        return this.protectItem;
    }

    public void setHPDecProtect(int delta)
    {
        this.protectItem = delta;
    }

    public List<Point> getSpawnPoints()
    {
        return this.spawnPoints;
    }

    public void setSpawnPoints(List<Point> Points)
    {
        this.spawnPoints = Points;
    }

    public int getCurrentPartyId()
    {
        this.charactersLock.readLock().lock();
        try
        {

            for (MapleCharacter chr : this.characters)
            {
                if (chr.getParty() != null)
                {
                    return chr.getParty().getId();
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return -1;
    }

    private void dropFromMonster(MapleCharacter chr, MapleMonster mob, boolean instanced)
    {
        if ((mob == null) || (chr == null) || (ChannelServer.getInstance(this.channel) == null) || (this.dropsDisabled) || (mob.dropsDisabled()) || (chr.getPyramidSubway() != null))
        {
            return;
        }

        int maxSize = 200;
        if ((!instanced) && (maxSize >= 200) && ((mapobjects.get(MapleMapObjectType.ITEM)).size() >= maxSize))
        {
            removeDropsDelay();
            if (!chr.isAdmin())
            {
            }
        }


        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        byte droptype = (byte) (chr.getParty() != null ? 1 : mob.getStats().isFfaLoot() ? 2 : mob.getStats().isExplosiveReward() ? 3 : 0);
        int mobpos = mob.getTruePosition().x;
        int mesoServerRate = ChannelServer.getInstance(this.channel).getMesoRate();
        int dropServerRate = ChannelServer.getInstance(this.channel).getDropRate();
        int cashServerRate = ChannelServer.getInstance(this.channel).getCashRate();
        int globalServerRate = ChannelServer.getInstance(this.channel).getGlobalRate();

        byte d = 1;
        Point pos = new Point(0, mob.getTruePosition().y);
        double 挑衅加成 = 100.0D;
        client.status.MonsterStatusEffect mse = mob.getBuff(MonsterStatus.挑衅);
        if (mse != null)
        {
            挑衅加成 += mse.getX();
        }

        MapleMonsterInformationProvider mi = MapleMonsterInformationProvider.getInstance();
        List<MonsterDropEntry> derp = mi.retrieveDrop(mob.getId());
        if (derp == null)
        {
            return;
        }
        List<MonsterDropEntry> dropEntry = new ArrayList(derp);
        Collections.shuffle(dropEntry);

        boolean mesoDropped = false;
        for (MonsterDropEntry de : dropEntry)
        {
            if (de.itemId != mob.getStolen())
            {

                if (Randomizer.nextInt(999999) < (int) (de.chance * dropServerRate * chr.getDropMod() * (chr.getStat().getDropBuff() / 100.0D) * (挑衅加成 / 100.0D)))
                    if (((!mesoDropped) || (droptype == 3) || (de.itemId != 0)) &&


                            (de.itemId / 10000 != 238))
                    {

                        if (droptype == 3)
                        {
                            pos.x = (mobpos + (d % 2 == 0 ? 40 * (d + 1) / 2 : -(40 * (d / 2))));
                        }
                        else
                        {
                            pos.x = (mobpos + (d % 2 == 0 ? 25 * (d + 1) / 2 : -(25 * (d / 2))));
                        }
                        if (de.itemId == 0)
                        {
                            int mesos = Randomizer.nextInt(1 + Math.abs(de.Maximum - de.Minimum)) + de.Minimum;
                            if (mesos > 0)
                            {
                                spawnMobMesoDrop((int) (mesos * (chr.getStat().mesoBuff / 100.0D) * chr.getDropMod() * mesoServerRate), calcDropPos(pos, mob.getTruePosition()), mob, chr, false,
                                        droptype);
                                mesoDropped = true;
                            }
                        }
                        else
                        {
                            Item idrop;
                            if (ItemConstants.getInventoryType(de.itemId) == MapleInventoryType.EQUIP)
                            {
                                idrop = ii.randomizeStats((client.inventory.Equip) ii.getEquipById(de.itemId));
                            }
                            else
                            {
                                int range = Math.abs(de.Maximum - de.Minimum);
                                idrop = new Item(de.itemId, (short) 0, (short) (de.Maximum != 1 ? Randomizer.nextInt(range <= 0 ? 1 : range) + de.Minimum : 1), (short) 0);
                            }
                            idrop.setGMLog("怪物掉落: " + mob.getId() + " 地图: " + this.mapid + " 时间: " + tools.FileoutputUtil.CurrentReadable_Date());
                            if (ItemConstants.isNoticeItem(de.itemId))
                            {
                            }


                            spawnMobDrop(idrop, calcDropPos(pos, mob.getTruePosition()), mob, chr, droptype, de.questid);
                        }
                        d = (byte) (d + 1);
                    }
            }
        }
        List<MonsterGlobalDropEntry> globalEntry = new ArrayList(mi.getGlobalDrop());
        Collections.shuffle(globalEntry);
        int cashz = ((mob.getStats().isBoss()) && (mob.getStats().getHPDisplayType() == 0) ? 20 : 1) * cashServerRate;
        int cashModifier = (int) (mob.getStats().isBoss() ? mob.getStats().isPartyBonus() ? mob.getMobExp() / 1000 : 0 : mob.getMobExp() / 1000 + mob.getMobMaxHp() / 20000L);

        for (MonsterGlobalDropEntry de : globalEntry)
        {
            if (de.chance != 0)
            {

                if ((Randomizer.nextInt(999999) < de.chance * globalServerRate) && ((de.continent < 0) || ((de.continent < 10) && (this.mapid / 100000000 == de.continent)) || ((de.continent < 100) && (this.mapid / 10000000 == de.continent)) || ((de.continent < 1000) && (this.mapid / 1000000 == de.continent))))
                    if ((de.itemId == 0) && (cashServerRate != 0))
                    {
                        int giveCash = Randomizer.nextInt(cashz) + cashz + cashModifier;
                        if (giveCash > 0)
                        {
                            chr.modifyCSPoints(2, giveCash, true);
                        }
                    }
                    else if (!this.gDropsDisabled)
                    {
                        if (droptype == 3)
                        {
                            pos.x = (mobpos + (d % 2 == 0 ? 40 * (d + 1) / 2 : -(40 * (d / 2))));
                        }
                        else pos.x = (mobpos + (d % 2 == 0 ? 25 * (d + 1) / 2 : -(25 * (d / 2))));
                        Item idrop;
                        if (ItemConstants.getInventoryType(de.itemId) == MapleInventoryType.EQUIP)
                        {
                            idrop = ii.randomizeStats((client.inventory.Equip) ii.getEquipById(de.itemId));
                        }
                        else
                        {
                            idrop = new Item(de.itemId, (short) 0, (short) (de.Maximum != 1 ? Randomizer.nextInt(de.Maximum - de.Minimum) + de.Minimum : 1), (short) 0);
                        }
                        idrop.setGMLog("怪物掉落: " + mob.getId() + " 地图: " + this.mapid + " (Global) 时间: " + tools.FileoutputUtil.CurrentReadable_Date());


                        spawnMobDrop(idrop, calcDropPos(pos, mob.getTruePosition()), mob, chr, (!ItemConstants.isNoticeItem(de.itemId)) || (de.onlySelf) ? 0 : droptype, de.questid);
                        d = (byte) (d + 1);
                    }
            }
        }
    }

    public void removeMonster(MapleMonster monster)
    {
        if (monster == null)
        {
            return;
        }
        this.spawnedMonstersOnMap.decrementAndGet();
        broadcastMessage(MobPacket.killMonster(monster.getObjectId(), 0));
        removeMapObject(monster);
        monster.killed();
    }

    public void broadcastMessage(byte[] packet)
    {
        broadcastMessage(null, packet, Double.POSITIVE_INFINITY, null);
    }

    public void removeMapObject(MapleMapObject obj)
    {
        mapobjectlocks.get(obj.getType()).writeLock().lock();
        try
        {
            mapobjects.get(obj.getType()).remove(obj.getObjectId());
        }
        finally
        {
            mapobjectlocks.get(obj.getType()).writeLock().unlock();
        }
    }

    public void broadcastMessage(MapleCharacter source, byte[] packet, double rangeSq, Point rangedFrom)
    {
        this.charactersLock.readLock().lock();
        try
        {
            for (MapleCharacter chr : this.characters)
            {
                if (chr != source)
                {
                    if (rangeSq < Double.POSITIVE_INFINITY)
                    {
                        if (rangedFrom.distanceSq(chr.getTruePosition()) <= rangeSq)
                        {
                            chr.getClient().getSession().write(packet);
                        }
                    }
                    else
                    {
                        chr.getClient().getSession().write(packet);
                    }
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
    }

    public void killMonster(MapleMonster monster)
    {
        if (monster == null)
        {
            return;
        }
        this.spawnedMonstersOnMap.decrementAndGet();
        monster.setHp(0L);
        if (monster.getLinkCID() <= 0)
        {
            monster.spawnRevives(this);
        }
        broadcastMessage(MobPacket.killMonster(monster.getObjectId(), monster.getStats().getSelfD() < 0 ? 1 : monster.getStats().getSelfD()));
        removeMapObject(monster);
        monster.killed();
    }

    public void killMonster(MapleMonster monster, MapleCharacter chr, boolean withDrops, boolean second, byte animation)
    {
        killMonster(monster, chr, withDrops, second, animation, 0);
    }

    public void killMonster(final MapleMonster monster, final MapleCharacter chr, boolean withDrops, boolean second, byte animation, int lastSkill)
    {
        if (((monster.getId() == 8810122) || (monster.getId() == 8810018)) && (!second))
        {
            Timer.MapTimer.getInstance().schedule(new Runnable()
            {
                public void run()
                {
                    MapleMap.this.killMonster(monster, chr, true, true, (byte) 1);
                    MapleMap.this.killAllMonsters(true);
                }
            }, 3000L);


            return;
        }
        if (monster.getId() == 8820014)
        {
            killMonster(8820000);
        }
        else if (monster.getId() == 8820212)
        {
            killMonster(8820100);
        }
        else if (monster.getId() == 9300166)
        {
            animation = 2;
        }
        this.spawnedMonstersOnMap.decrementAndGet();
        removeMapObject(monster);
        monster.killed();
        MapleSquad sqd = getSquadByMap();
        boolean instanced = (sqd != null) || (monster.getEventInstance() != null) || (getEMByMap() != null);
        int dropOwner = monster.killBy(chr, lastSkill);
        if (animation >= 0)
        {
            broadcastMessage(MobPacket.killMonster(monster.getObjectId(), animation));
        }
        if (monster.getBuffToGive() > -1)
        {
            int buffid = monster.getBuffToGive();
            MapleStatEffect buff = MapleItemInformationProvider.getInstance().getItemEffect(buffid);
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter mc : this.characters)
                {
                    if (mc.isAlive())
                    {
                        buff.applyTo(mc);
                        switch (monster.getId())
                        {
                            case 8810018:
                            case 8810122:
                            case 8820001:
                            case 8820212:
                                mc.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(buffid, 13, mc.getLevel(), 1));
                                broadcastMessage(mc, MaplePacketCreator.showBuffeffect(mc.getId(), buffid, 13, mc.getLevel(), 1), false);
                        }
                    }
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
        }
        int mobid = monster.getId();
        ExpeditionType type = null;
        if ((mobid == 8810018) && (this.mapid == 240060200))
        {
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(6, "经过无数次的挑战，终于击破了暗黑龙王的远征队！你们才是龙之林的真正英雄~"));
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(16);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }

            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Horntail;
            }
            doShrine(true);
        }
        else if ((mobid == 8810122) && (this.mapid == 240060201))
        {
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(6, "经过无数次的挑战，终于击破了进阶暗黑龙王的远征队！你们才是龙之林的真正英雄~"));
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(24);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }

            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.ChaosHT;
            }
            doShrine(true);
        }
        else if ((mobid == 9400266) && (this.mapid == 802000111))
        {
            doShrine(true);
        }
        else if ((mobid == 9400265) && (this.mapid == 802000211))
        {
            doShrine(true);
        }
        else if ((mobid == 9400270) && (this.mapid == 802000411))
        {
            doShrine(true);
        }
        else if ((mobid == 9400273) && (this.mapid == 802000611))
        {
            doShrine(true);
        }
        else if ((mobid == 9400294) && (this.mapid == 802000711))
        {
            doShrine(true);
        }
        else if ((mobid == 9400296) && (this.mapid == 802000803))
        {
            doShrine(true);
        }
        else if ((mobid == 9400289) && (this.mapid == 802000821))
        {
            doShrine(true);
        }
        else if ((mobid == 8830000) && (this.mapid == 105100300))
        {
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Normal_Balrog;
            }
        }
        else if (((mobid == 9420544) || (mobid == 9420549)) && (this.mapid == 551030200) && (monster.getEventInstance() != null) && (monster.getEventInstance().getName().contains(getEMByMap().getName())))
        {
            doShrine(getAllReactor().isEmpty());
        }
        else if ((mobid == 8820001) && (this.mapid == 270050100))
        {
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(6, "凭借永不疲倦的热情打败品克缤的远征队啊！你们是真正的时间的胜者！"));
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(17);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Pink_Bean;
            }
            doShrine(true);
        }
        else if ((mobid == 8820212) && (this.mapid == 270051100))
        {
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(6, "凭借永不疲倦的热情打败混沌品克缤的远征队啊！你们是真正的时间的胜者！"));
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(59);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Chaos_Pink_Bean;
            }
            doShrine(true);
        }
        else if (((mobid == 8850011) && (this.mapid == 271040200)) || ((mobid == 8850012) && (this.mapid == 271040100)))
        {
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(6, "被黑魔法师黑化的希纳斯女皇终于被永不言败的远征队打倒! 混沌世界得以净化!"));
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(39);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Cygnus;
            }
            doShrine(true);
        }
        else if ((mobid == 8840000) && (this.mapid == 211070100))
        {
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(38);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Von_Leon;
            }
            doShrine(true);
        }
        else if ((mobid == 8800002) && ((this.mapid == 280030000) || (this.mapid == 280030100)))
        {
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(15);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }

            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Zakum;
            }
            doShrine(true);
        }
        else if ((mobid == 8800102) && (this.mapid == 280030001))
        {
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(23);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }

            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Chaos_Zakum;
            }
            doShrine(true);
        }
        else if ((mobid == 8870000) && (this.mapid == 262031300))
        {
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(55);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Hillah;
            }
            doShrine(true);
        }
        else if ((mobid == 8870100) && (this.mapid == 262031300))
        {
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(56);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Hillah;
            }
            doShrine(true);
        }
        else if ((mobid == 8860000) && (this.mapid == 272030400))
        {
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter c : this.characters)
                {
                    c.finishAchievement(58);
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
            if (this.speedRunStart > 0L)
            {
                type = ExpeditionType.Akyrum;
            }
            doShrine(true);
        }
        else if ((mobid >= 8800003) && (mobid <= 8800010))
        {
            boolean makeZakReal = true;
            Collection<MapleMonster> monsters = getAllMonstersThreadsafe();
            for (MapleMonster mons : monsters)
            {
                if ((mons.getId() >= 8800003) && (mons.getId() <= 8800010))
                {
                    makeZakReal = false;
                    break;
                }
            }
            if (makeZakReal)
            {
                for (MapleMapObject object : monsters)
                {
                    MapleMonster mons = (MapleMonster) object;
                    if (mons.getId() == 8800000)
                    {
                        Point pos = mons.getTruePosition();
                        killAllMonsters(true);
                        spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8800000), pos);
                        break;
                    }
                }
            }
        }
        else if ((mobid >= 8800103) && (mobid <= 8800110))
        {
            boolean makeZakReal = true;
            Collection<MapleMonster> monsters = getAllMonstersThreadsafe();
            for (MapleMonster mons : monsters)
            {
                if ((mons.getId() >= 8800103) && (mons.getId() <= 8800110))
                {
                    makeZakReal = false;
                    break;
                }
            }
            if (makeZakReal) for (MapleMonster mons : monsters)
                if (mons.getId() == 8800100)
                {
                    Point pos = mons.getTruePosition();
                    killAllMonsters(true);
                    spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8800100), pos);
                    break;
                }
        }
        else
        {
            boolean makeZakReal;
            if ((mobid >= 9400903) && (mobid <= 9400910))
            {
                makeZakReal = true;
                Collection<MapleMonster> monsters = getAllMonstersThreadsafe();
                for (MapleMonster mons : monsters)
                {
                    if ((mons.getId() >= 9400903) && (mons.getId() <= 9400910))
                    {
                        makeZakReal = false;
                        break;
                    }
                }
                if (makeZakReal)
                {
                    for (MapleMonster mons : monsters)
                    {
                        if (mons.getId() == 9400900)
                        {
                            Point pos = mons.getTruePosition();
                            killAllMonsters(true);
                            spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9400900), pos);
                            break;
                        }
                    }
                }
            }
            else if (mobid == 8820008)
            {
                for (MapleMapObject mmo : getAllMonstersThreadsafe())
                {
                    MapleMonster mons = (MapleMonster) mmo;
                    if (mons.getLinkOid() != monster.getObjectId())
                    {
                        killMonster(mons, chr, false, false, animation);
                    }
                }
            }
            else if ((mobid >= 8820010) && (mobid <= 8820014))
            {
                for (MapleMapObject mmo : getAllMonstersThreadsafe())
                {
                    MapleMonster mons = (MapleMonster) mmo;
                    if ((mons.getId() != 8820000) && (mons.getId() != 8820001) && (mons.getObjectId() != monster.getObjectId()) && (mons.isAlive()) && (mons.getLinkOid() == monster.getObjectId()))
                    {
                        killMonster(mons, chr, false, false, animation);
                    }
                }
            }
            else if (mobid == 8820108)
            {
                for (MapleMapObject mmo : getAllMonstersThreadsafe())
                {
                    MapleMonster mons = (MapleMonster) mmo;
                    if (mons.getLinkOid() != monster.getObjectId())
                    {
                        killMonster(mons, chr, false, false, animation);
                    }
                }
            }
            else if ((mobid >= 8820300) && (mobid <= 8820304))
            {
                for (MapleMapObject mmo : getAllMonstersThreadsafe())
                {
                    MapleMonster mons = (MapleMonster) mmo;
                    if ((mons.getId() != 8820100) && (mons.getId() != 8820212) && (mons.getObjectId() != monster.getObjectId()) && (mons.isAlive()) && (mons.getLinkOid() == monster.getObjectId()))
                    {
                        killMonster(mons, chr, false, false, animation);
                    }
                }
            }
            else if ((mobid / 100000 == 98) && (chr.getMapId() / 10000000 == 95) && (getAllMonstersThreadsafe().isEmpty()))
            {
                switch (chr.getMapId() % 1000 / 100)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        chr.getClient().getSession().write(UIPacket.MapEff("monsterPark/clear"));
                        break;
                    case 5:
                        if (chr.getMapId() / 1000000 == 952)
                        {
                            chr.getClient().getSession().write(UIPacket.MapEff("monsterPark/clearF"));
                        }
                        else
                        {
                            chr.getClient().getSession().write(UIPacket.MapEff("monsterPark/clear"));
                        }
                        break;
                    case 6:
                        chr.getClient().getSession().write(UIPacket.MapEff("monsterPark/clearF"));
                }
            }
            else if ((mobid / 100000 == 93) && (chr.getMapId() / 1000000 == 955) && (getAllMonstersThreadsafe().isEmpty()))
            {
                switch (chr.getMapId() % 1000 / 100)
                {
                    case 1:
                    case 2:
                        chr.getClient().getSession().write(MaplePacketCreator.showEffect("aswan/clear"));
                        chr.getClient().getSession().write(MaplePacketCreator.playSound("Party1/Clear"));
                        break;
                    case 3:
                        chr.getClient().getSession().write(MaplePacketCreator.showEffect("aswan/clearF"));
                        chr.getClient().getSession().write(MaplePacketCreator.playSound("Party1/Clear"));
                        chr.dropMessage(-1, "你已经通过了所有回合。请通过传送口移动到外部。");
                }
            }
        }
        if ((type != null) && (this.speedRunStart > 0L) && (this.speedRunLeader.length() > 0))
        {
            String name = "";
            if (type.name().equals("Normal_Balrog"))
            {
                name = "蝙蝠怪";
            }
            else if (type.name().equals("Zakum"))
            {
                name = "扎昆";
            }
            else if (type.name().equals("Horntail"))
            {
                name = "暗黑龙王";
            }
            else if (type.name().equals("Pink_Bean"))
            {
                name = "时间的宠儿－品克缤";
            }
            else if (type.name().equals("Chaos_Pink_Bean"))
            {
                name = "混沌品克缤";
            }
            else if (type.name().equals("Chaos_Zakum"))
            {
                name = "进阶扎昆";
            }
            else if (type.name().equals("ChaosHT"))
            {
                name = "进阶暗黑龙王";
            }
            else if (type.name().equals("Von_Leon"))
            {
                name = "班·雷昂";
            }
            else if (type.name().equals("Cygnus"))
            {
                name = "希纳斯女皇";
            }
            else if (type.name().equals("Akyrum"))
            {
                name = "阿卡伊勒";
            }
            else if (type.name().equals("Hillah"))
            {
                name = "希拉";
            }
            long endTime = System.currentTimeMillis();
            String time = tools.StringUtil.getReadableMillis(this.speedRunStart, endTime);
            broadcastMessage(MaplePacketCreator.serverNotice(5, this.speedRunLeader + "带领的远征队，耗时: " + time + " 击败了 " + name + "!"));
            getRankAndAdd(this.speedRunLeader, time, type, endTime - this.speedRunStart, sqd == null ? null : sqd.getMembers());
            endSpeedRun();
        }

        if ((withDrops) && (dropOwner != 1))
        {
            MapleCharacter drop;
            if (dropOwner <= 0)
            {
                drop = chr;
            }
            else
            {
                drop = getCharacterById(dropOwner);
                if (drop == null)
                {
                    drop = chr;
                }
            }
            dropFromMonster(drop, monster, instanced);
        }
    }

    public List<MapleReactor> getAllReactor()
    {
        return getAllReactorsThreadsafe();
    }

    public List<MapleSummon> getAllSummonsThreadsafe()
    {
        ArrayList<MapleSummon> ret = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.SUMMON).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.SUMMON)).values())
            {
                if ((mmo instanceof MapleSummon))
                {
                    ret.add((MapleSummon) mmo);
                }
            }
        }
        finally
        {
            this.mapobjectlocks.get(MapleMapObjectType.SUMMON).readLock().unlock();
        }
        return ret;
    }

    public List<MapleMapObject> getAllDoor()
    {
        return getAllDoorsThreadsafe();
    }

    public List<MapleMapObject> getAllDoorsThreadsafe()
    {
        ArrayList<MapleMapObject> ret = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.DOOR).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.DOOR)).values())
            {
                if ((mmo instanceof MapleDoor))
                {
                    ret.add(mmo);
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.DOOR)).readLock().unlock();
        }
        return ret;
    }

    public List<MapleMapObject> getAllMechDoorsThreadsafe()
    {
        ArrayList<MapleMapObject> ret = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.DOOR).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.DOOR)).values())
            {
                if ((mmo instanceof MechDoor))
                {
                    ret.add(mmo);
                }
            }
        }
        finally
        {
            this.mapobjectlocks.get(MapleMapObjectType.DOOR).readLock().unlock();
        }
        return ret;
    }

    public List<MapleMapObject> getAllMerchant()
    {
        return getAllHiredMerchantsThreadsafe();
    }

    public List<MapleMapObject> getAllHiredMerchantsThreadsafe()
    {
        ArrayList<MapleMapObject> ret = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.HIRED_MERCHANT).readLock().lock();
        try
        {
            ret.addAll((mapobjects.get(MapleMapObjectType.HIRED_MERCHANT)).values());
        }
        finally
        {
            this.mapobjectlocks.get(MapleMapObjectType.HIRED_MERCHANT).readLock().unlock();
        }
        return ret;
    }

    public List<MapleMonster> getAllMonster()
    {
        return getAllMonstersThreadsafe();
    }

    public List<MapleMonster> getAllMonstersThreadsafe()
    {
        ArrayList<MapleMonster> ret = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.MONSTER).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.MONSTER)).values())
            {
                ret.add((MapleMonster) mmo);
            }
        }
        finally
        {
            this.mapobjectlocks.get(MapleMapObjectType.MONSTER).readLock().unlock();
        }
        return ret;
    }

    public List<Integer> getAllUniqueMonsters()
    {
        ArrayList<Integer> ret = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.MONSTER).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.MONSTER)).values())
            {
                int theId = ((MapleMonster) mmo).getId();
                if (!ret.contains(theId))
                {
                    ret.add(theId);
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.MONSTER)).readLock().unlock();
        }
        return ret;
    }

    public void killAllMonsters(boolean animate)
    {
        for (MapleMapObject monstermo : getAllMonstersThreadsafe())
        {
            MapleMonster monster = (MapleMonster) monstermo;
            this.spawnedMonstersOnMap.decrementAndGet();
            monster.setHp(0L);
            broadcastMessage(MobPacket.killMonster(monster.getObjectId(), animate ? 1 : 0));
            removeMapObject(monster);
            monster.killed();
        }
    }

    public void killMonster(int monsId)
    {
        for (MapleMapObject mmo : getAllMonstersThreadsafe())
        {
            if (((MapleMonster) mmo).getId() == monsId)
            {
                this.spawnedMonstersOnMap.decrementAndGet();
                removeMapObject(mmo);
                broadcastMessage(MobPacket.killMonster(mmo.getObjectId(), 1));
                ((MapleMonster) mmo).killed();
                break;
            }
        }
    }

    private String MapDebug_Log()
    {
        StringBuilder sb = new StringBuilder("Defeat time : ");
        sb.append(tools.FileoutputUtil.CurrentReadable_Time());
        sb.append(" | Mapid : ").append(this.mapid);
        this.charactersLock.readLock().lock();
        try
        {
            sb.append(" Users [").append(this.characters.size()).append("] | ");
            for (MapleCharacter mc : this.characters)
            {
                sb.append(mc.getName()).append(", ");
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return sb.toString();
    }

    public void limitReactor(int rid, int num)
    {
        List<MapleReactor> toDestroy = new ArrayList<>();
        Map<Integer, Integer> contained = new LinkedHashMap<>();
        this.mapobjectlocks.get(MapleMapObjectType.REACTOR).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                MapleReactor mr = (MapleReactor) obj;
                if (contained.containsKey(mr.getReactorId()))
                {
                    if (contained.get(mr.getReactorId()) >= num)
                    {
                        toDestroy.add(mr);
                    }
                    else
                    {
                        contained.put(mr.getReactorId(), contained.get(mr.getReactorId()) + 1);
                    }
                }
                else
                {
                    contained.put(mr.getReactorId(), 1);
                }
            }
        }
        finally
        {
            this.mapobjectlocks.get(MapleMapObjectType.REACTOR).readLock().unlock();
        }
        for (MapleReactor mr : toDestroy)
        {
            destroyReactor(mr.getObjectId());
        }
    }

    public void destroyReactor(int oid)
    {
        final MapleReactor reactor = getReactorByOid(oid);
        if (reactor == null)
        {
            return;
        }
        broadcastMessage(MaplePacketCreator.destroyReactor(reactor));
        reactor.setAlive(false);
        removeMapObject(reactor);
        reactor.setTimerActive(false);

        if (reactor.getDelay() > 0)
        {
            Timer.MapTimer.getInstance().schedule(new Runnable()
            {
                public void run()
                {
                    MapleMap.this.respawnReactor(reactor);
                }
            }, reactor.getDelay());
        }
    }

    public MapleReactor getReactorByOid(int oid)
    {
        MapleMapObject mmo = getMapObject(oid, MapleMapObjectType.REACTOR);
        if (mmo == null)
        {
            return null;
        }
        return (MapleReactor) mmo;
    }

    private void respawnReactor(MapleReactor reactor)
    {
        if ((!isSecretMap()) && (reactor.getReactorId() >= 100000) && (reactor.getReactorId() <= 200011))
        {
            int newRid = (reactor.getReactorId() < 200000 ? 100000 : 200000) + Randomizer.nextInt(11);
            int prop = reactor.getReactorId() % 100;
            if ((Randomizer.nextInt(22) <= prop) && (newRid % 100 < 10))
            {
                newRid++;
            }
            if ((Randomizer.nextInt(110) <= prop) && (newRid % 100 < 11))
            {
                newRid++;
            }
            List<Point> toSpawnPos = new ArrayList(this.spawnPoints);
            for (MapleMapObject reactor1l : getAllReactorsThreadsafe())
            {
                MapleReactor reactor2l = (MapleReactor) reactor1l;
                if ((!toSpawnPos.isEmpty()))
                {
                    toSpawnPos.remove(reactor2l.getPosition());
                }
            }


            MapleReactor newReactor = new MapleReactor(MapleReactorFactory.getReactor(newRid), newRid);
            newReactor.setPosition(toSpawnPos.isEmpty() ? reactor.getPosition() : toSpawnPos.get(Randomizer.nextInt(toSpawnPos.size())));
            newReactor.setDelay(newRid % 100 == 11 ? 60000 : 5000);
            spawnReactor(newReactor);
        }
        else
        {
            reactor.setState((byte) 0);
            reactor.setAlive(true);
            spawnReactor(reactor);
        }
    }

    public MapleMapObject getMapObject(int oid, MapleMapObjectType type)
    {
        (mapobjectlocks.get(type)).readLock().lock();
        try
        {
            return (mapobjects.get(type)).get(oid);
        }
        finally
        {
            (mapobjectlocks.get(type)).readLock().unlock();
        }
    }

    public boolean isSecretMap()
    {
        switch (this.mapid)
        {
            case 910001001:
            case 910001002:
            case 910001003:
            case 910001004:
            case 910001005:
            case 910001006:
            case 910001007:
            case 910001008:
            case 910001009:
            case 910001010:
                return true;
        }
        return false;
    }

    public List<MapleReactor> getAllReactorsThreadsafe()
    {
        ArrayList<MapleReactor> ret = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.REACTOR).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                ret.add((MapleReactor) mmo);
            }
        }
        finally
        {
            this.mapobjectlocks.get(MapleMapObjectType.REACTOR).readLock().unlock();
        }
        return ret;
    }

    public void spawnReactor(final MapleReactor reactor)
    {
        reactor.setMap(this);
        spawnAndAddRangedMapObject(reactor, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(MaplePacketCreator.spawnReactor(reactor));
            }
        });
    }

    private void spawnAndAddRangedMapObject(MapleMapObject mapobject, DelayedPacketCreation packetbakery)
    {
        addMapObject(mapobject);

        this.charactersLock.readLock().lock();
        try
        {

            for (MapleCharacter chr : this.characters)
            {
                if ((mapobject.getType() == MapleMapObjectType.MIST) || (chr.getTruePosition().distanceSq(mapobject.getTruePosition()) <= GameConstants.maxViewRangeSq()))
                {
                    packetbakery.sendPackets(chr.getClient());
                    chr.addVisibleMapObject(mapobject);
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
    }

    public void addMapObject(MapleMapObject mapobject)
    {
        runningOidLock.lock();
        int newOid;
        try
        {
            newOid = ++runningOid;
        }
        finally
        {
            runningOidLock.unlock();
        }

        mapobject.setObjectId(newOid);

        mapobjectlocks.get(mapobject.getType()).writeLock().lock();
        try
        {
            mapobjects.get(mapobject.getType()).put(newOid, mapobject);
        }
        finally
        {
            mapobjectlocks.get(mapobject.getType()).writeLock().unlock();
        }
    }

    public void destroyReactors(int first, int last)
    {
        List<MapleReactor> toDestroy = new ArrayList<>();
        this.mapobjectlocks.get(MapleMapObjectType.REACTOR).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                MapleReactor mr = (MapleReactor) obj;
                if ((mr.getReactorId() >= first) && (mr.getReactorId() <= last))
                {
                    toDestroy.add(mr);
                }
            }
        }
        finally
        {
            this.mapobjectlocks.get(MapleMapObjectType.REACTOR).readLock().unlock();
        }
        for (MapleReactor mr : toDestroy)
        {
            destroyReactor(mr.getObjectId());
        }
    }

    public void reloadReactors()
    {
        List<MapleReactor> toSpawn = new ArrayList<>();
        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                MapleReactor reactor = (MapleReactor) obj;
                broadcastMessage(MaplePacketCreator.destroyReactor(reactor));
                reactor.setAlive(false);
                reactor.setTimerActive(false);
                toSpawn.add(reactor);
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
        for (MapleReactor r : toSpawn)
        {
            removeMapObject(r);
            if (!r.isCustom())
            {
                respawnReactor(r);
            }
        }
    }

    public void resetReactors()
    {
        setReactorState((byte) 0);
    }

    public void setReactorState(byte state)
    {
        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                ((MapleReactor) obj).forceHitReactor(state);
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
    }

    public void setReactorState()
    {
        setReactorState((byte) 1);
    }

    public void setReactorDelay(int state)
    {
        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                ((MapleReactor) obj).setDelay(state);
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
    }

    public void shuffleReactors()
    {
        shuffleReactors(0, 9999999);
    }

    public void shuffleReactors(int first, int last)
    {
        List<Point> points = new ArrayList<>();
        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                MapleReactor mr = (MapleReactor) obj;
                if ((mr.getReactorId() >= first) && (mr.getReactorId() <= last))
                {
                    points.add(mr.getPosition());
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
        Collections.shuffle(points);
        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                MapleReactor mr = (MapleReactor) obj;
                if ((mr.getReactorId() >= first) && (mr.getReactorId() <= last))
                {
                    mr.setPosition(points.remove(points.size() - 1));
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
    }

    public boolean containsNPC(int npcid)
    {
        (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().lock();
        try
        {
            Iterator<MapleMapObject> itr = (mapobjects.get(MapleMapObjectType.NPC)).values().iterator();
            MapleNPC n;
            while (itr.hasNext())
            {
                n = (MapleNPC) itr.next();
                if (n.getId() == npcid)
                {
                    return true;
                }
            }
            return false;
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().unlock();
        }
    }

    public MapleNPC getNPCById(int id)
    {
        (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().lock();
        try
        {
            Iterator<MapleMapObject> itr = (mapobjects.get(MapleMapObjectType.NPC)).values().iterator();
            MapleNPC n;
            while (itr.hasNext())
            {
                n = (MapleNPC) itr.next();
                if (n.getId() == id)
                {
                    return n;
                }
            }
            return null;
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().unlock();
        }
    }

    public MapleMonster getMonsterById(int id)
    {
        (mapobjectlocks.get(MapleMapObjectType.MONSTER)).readLock().lock();
        try
        {
            MapleMonster ret = null;
            Iterator<MapleMapObject> itr = (mapobjects.get(MapleMapObjectType.MONSTER)).values().iterator();
            MapleMonster n;
            while (itr.hasNext())
            {
                n = (MapleMonster) itr.next();
                if (n.getId() == id)
                {
                    ret = n;
                    break;
                }
            }
            return ret;
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.MONSTER)).readLock().unlock();
        }
    }

    public int countMonsterById(int id)
    {
        (mapobjectlocks.get(MapleMapObjectType.MONSTER)).readLock().lock();
        try
        {
            int ret = 0;
            Iterator<MapleMapObject> itr = (mapobjects.get(MapleMapObjectType.MONSTER)).values().iterator();
            MapleMonster n;
            while (itr.hasNext())
            {
                n = (MapleMonster) itr.next();
                if (n.getId() == id)
                {
                    ret++;
                }
            }
            return ret;
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.MONSTER)).readLock().unlock();
        }
    }

    public MapleReactor getReactorById(int id)
    {
        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            MapleReactor ret = null;
            Iterator<MapleMapObject> itr = (mapobjects.get(MapleMapObjectType.REACTOR)).values().iterator();
            MapleReactor n;
            while (itr.hasNext())
            {
                n = (MapleReactor) itr.next();
                if (n.getReactorId() == id)
                {
                    ret = n;
                    break;
                }
            }
            return ret;
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
    }

    public MapleMonster getMonsterByOid(int oid)
    {
        MapleMapObject mmo = getMapObject(oid, MapleMapObjectType.MONSTER);
        if (mmo == null)
        {
            return null;
        }
        return (MapleMonster) mmo;
    }

    public MapleSummon getSummonByOid(int oid)
    {
        MapleMapObject mmo = getMapObject(oid, MapleMapObjectType.SUMMON);
        if (mmo == null)
        {
            return null;
        }
        return (MapleSummon) mmo;
    }

    public MapleNPC getNPCByOid(int oid)
    {
        MapleMapObject mmo = getMapObject(oid, MapleMapObjectType.NPC);
        if (mmo == null)
        {
            return null;
        }
        return (MapleNPC) mmo;
    }

    public MonsterFamiliar getFamiliarByOid(int oid)
    {
        MapleMapObject mmo = getMapObject(oid, MapleMapObjectType.FAMILIAR);
        if (mmo == null)
        {
            return null;
        }
        return (MonsterFamiliar) mmo;
    }

    public MapleMist getMistByOid(int oid)
    {
        MapleMapObject mmo = getMapObject(oid, MapleMapObjectType.MIST);
        if (mmo == null)
        {
            return null;
        }
        return (MapleMist) mmo;
    }

    public void spawnNpc(int id, Point pos)
    {
        MapleNPC npc = MapleLifeFactory.getNPC(id);
        npc.setPosition(pos);
        npc.setCy(pos.y);
        npc.setRx0(pos.x + 50);
        npc.setRx1(pos.x - 50);
        npc.setFh(getFootholds().findBelow(pos).getId());
        npc.setCustom(true);
        addMapObject(npc);
        broadcastMessage(NPCPacket.spawnNPC(npc, true));
    }

    public MapleFootholdTree getFootholds()
    {
        return this.footholds;
    }

    public void setFootholds(MapleFootholdTree footholds)
    {
        this.footholds = footholds;
    }

    public void removeNpc(int npcid)
    {
        (mapobjectlocks.get(MapleMapObjectType.NPC)).writeLock().lock();
        try
        {
            Iterator<MapleMapObject> itr = (mapobjects.get(MapleMapObjectType.NPC)).values().iterator();
            while (itr.hasNext())
            {
                MapleNPC npc = (MapleNPC) itr.next();
                if ((npc.isCustom()) && ((npcid == -1) || (npc.getId() == npcid)))
                {
                    broadcastMessage(NPCPacket.removeNPCController(npc.getObjectId()));
                    broadcastMessage(NPCPacket.removeNPC(npc.getObjectId()));
                    itr.remove();
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.NPC)).writeLock().unlock();
        }
    }

    public void hideNpc(int npcid)
    {
        (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().lock();
        try
        {
            for (MapleMapObject mapleMapObject : (mapobjects.get(MapleMapObjectType.NPC)).values())
            {
                MapleNPC npc = (MapleNPC) mapleMapObject;
                if ((npcid == -1) || (npc.getId() == npcid))
                {
                    broadcastMessage(NPCPacket.removeNPCController(npc.getObjectId()));
                    broadcastMessage(NPCPacket.removeNPC(npc.getObjectId()));
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().unlock();
        }
    }

    public void spawnMonster_Pokemon(MapleMonster mob, Point pos, int spawnType)
    {
        Point spos = calcPointBelow(new Point(pos.x, pos.y - 1));
        mob.setPosition(spos);
        spawnMonster(mob, spawnType, true);
    }

    public Point calcPointBelow(Point initial)
    {
        MapleFoothold fh = this.footholds.findBelow(initial);
        if (fh == null)
        {
            return new Point(1, 1);
        }
        int dropY = fh.getY1();
        if ((!fh.isWall()) && (fh.getY1() != fh.getY2()))
        {
            double s1 = Math.abs(fh.getY2() - fh.getY1());
            double s2 = Math.abs(fh.getX2() - fh.getX1());
            double s5 = Math.cos(Math.atan(s2 / s1)) * (Math.abs(initial.x - fh.getX1()) / Math.cos(Math.atan(s1 / s2)));
            if (fh.getY2() < fh.getY1())
            {
                dropY = fh.getY1() - (int) s5;
            }
            else
            {
                dropY = fh.getY1() + (int) s5;
            }
        }
        return new Point(initial.x, dropY);
    }

    public void spawnMonster(final MapleMonster monster, final int spawnType, final boolean overwrite)
    {
        monster.setMap(this);
        checkRemoveAfter(monster);
        if (monster.getId() == 9300166)
        {
            Timer.MapTimer.getInstance().schedule(new Runnable()
            {

                public void run()
                {
                    MapleMap.this.broadcastMessage(MobPacket.killMonster(monster.getObjectId(), 2));
                }
            }, 3000L);
        }


        spawnAndAddRangedMapObject(monster, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(MobPacket.spawnMonster(monster, (monster.getStats().getSummonType() <= 1) || (monster.getStats().getSummonType() == 27) || (overwrite) ? spawnType :
                        monster.getStats().getSummonType(), 0));
            }
        });
        updateMonsterController(monster);
        this.spawnedMonstersOnMap.incrementAndGet();
    }

    private void checkRemoveAfter(MapleMonster monster)
    {
        int ra = monster.getStats().getRemoveAfter();
        if ((ra > 0) && (monster.getLinkCID() <= 0))
        {
            monster.registerKill(ra * 1000);
        }
    }

    public void updateMonsterController(MapleMonster monster)
    {
        if ((!monster.isAlive()) || (monster.getLinkCID() > 0) || (monster.getStats().isEscort()))
        {
            return;
        }
        if (monster.getController() != null)
        {
            if ((monster.getController().getMap() != this) || (monster.getController().getTruePosition().distanceSq(monster.getTruePosition()) > monster.getRange()))
            {
                monster.getController().stopControllingMonster(monster);
            }
            else
            {
                return;
            }
        }
        int mincontrolled = -1;
        MapleCharacter newController = null;

        this.charactersLock.readLock().lock();
        try
        {

            for (MapleCharacter chr : this.characters)
            {
                if ((!chr.isHidden()) && ((chr.getControlledSize() < mincontrolled) || (mincontrolled == -1)) && (chr.getTruePosition().distanceSq(monster.getTruePosition()) <= monster.getRange()))
                {
                    mincontrolled = chr.getControlledSize();
                    newController = chr;
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        if (newController != null)
        {
            if (monster.isFirstAttack())
            {
                newController.controlMonster(monster, true);
                monster.setControllerHasAggro(true);
            }
            else
            {
                newController.controlMonster(monster, false);
            }
        }
    }

    public int spawnMonsterWithEffectBelow(MapleMonster mob, Point pos, int effect)
    {
        Point spos = calcPointBelow(new Point(pos.x, pos.y - 1));
        return spawnMonsterWithEffect(mob, effect, spos);
    }

    public int spawnMonsterWithEffect(final MapleMonster monster, final int effect, Point pos)
    {
        try
        {
            monster.setMap(this);
            monster.setPosition(pos);
            spawnAndAddRangedMapObject(monster, new DelayedPacketCreation()
            {
                public void sendPackets(MapleClient c)
                {
                    c.getSession().write(MobPacket.spawnMonster(monster, effect, 0));
                }
            });
            updateMonsterController(monster);
            this.spawnedMonstersOnMap.incrementAndGet();
            return monster.getObjectId();
        }
        catch (Exception ignored)
        {
        }
        return -1;
    }

    public void spawnZakum(int x, int y)
    {
        Point pos = new Point(x, y);
        MapleMonster mainb = MapleLifeFactory.getMonster(8800000);
        Point spos = calcPointBelow(new Point(pos.x, pos.y - 1));
        mainb.setPosition(spos);
        mainb.setFake(true);

        spawnFakeMonster(mainb);
        int[] zakpart = {8800003, 8800004, 8800005, 8800006, 8800007, 8800008, 8800009, 8800010};
        for (int i : zakpart)
        {
            MapleMonster part = MapleLifeFactory.getMonster(i);
            part.setPosition(spos);
            spawnMonster(part, -2);
        }
        if (this.squadSchedule != null)
        {
            cancelSquadSchedule(false);
        }
    }

    public void spawnChaosZakum(int x, int y)
    {
        Point pos = new Point(x, y);
        MapleMonster mainb = MapleLifeFactory.getMonster(8800100);
        Point spos = calcPointBelow(new Point(pos.x, pos.y - 1));
        mainb.setPosition(spos);
        mainb.setFake(true);

        spawnFakeMonster(mainb);
        int[] zakpart = {8800103, 8800104, 8800105, 8800106, 8800107, 8800108, 8800109, 8800110};
        for (int i : zakpart)
        {
            MapleMonster part = MapleLifeFactory.getMonster(i);
            part.setPosition(spos);
            spawnMonster(part, -2);
        }
        if (this.squadSchedule != null)
        {
            cancelSquadSchedule(false);
        }
    }

    public void spawnPinkZakum(int x, int y)
    {
        Point pos = new Point(x, y);
        MapleMonster mainb = MapleLifeFactory.getMonster(9400900);
        Point spos = calcPointBelow(new Point(pos.x, pos.y - 1));
        mainb.setPosition(spos);
        mainb.setFake(true);

        spawnFakeMonster(mainb);
        int[] zakpart = {9400903, 9400904, 9400905, 9400906, 9400907, 9400908, 9400909, 9400910};
        for (int i : zakpart)
        {
            MapleMonster part = MapleLifeFactory.getMonster(i);
            part.setPosition(spos);
            spawnMonster(part, -2);
        }
        if (this.squadSchedule != null)
        {
            cancelSquadSchedule(false);
        }
    }

    public void spawnFakeMonsterOnGroundBelow(MapleMonster mob, Point pos)
    {
        Point spos = calcPointBelow(new Point(pos.x, pos.y - 1));
        spos.y -= 1;
        mob.setPosition(spos);
        spawnFakeMonster(mob);
    }

    public void spawnFakeMonster(final MapleMonster monster)
    {
        monster.setMap(this);
        monster.setFake(true);
        spawnAndAddRangedMapObject(monster, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(MobPacket.spawnMonster(monster, -4, 0));
            }
        });
        updateMonsterController(monster);
        this.spawnedMonstersOnMap.incrementAndGet();
    }

    public void spawnRevives(final MapleMonster monster, final int oid)
    {
        monster.setMap(this);
        checkRemoveAfter(monster);
        if (monster.getId() == 9300166)
        {
            Timer.MapTimer.getInstance().schedule(new Runnable()
            {

                public void run()
                {
                    MapleMap.this.broadcastMessage(MobPacket.killMonster(monster.getObjectId(), 2));
                }
            }, 3000L);
        }


        monster.setLinkOid(oid);
        spawnAndAddRangedMapObject(monster, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(MobPacket.spawnMonster(monster, monster.getStats().getSummonType() <= 1 ? -3 : monster.getStats().getSummonType(), oid));
            }
        });
        updateMonsterController(monster);
        this.spawnedMonstersOnMap.incrementAndGet();
    }

    public void spawnDoor(final MapleDoor door)
    {
        spawnAndAddRangedMapObject(door, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                door.sendSpawnData(c);
                c.getSession().write(MaplePacketCreator.enableActions());
            }
        });
    }

    public void spawnMechDoor(final MechDoor door)
    {
        spawnAndAddRangedMapObject(door, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(MaplePacketCreator.spawnMechDoor(door, true));
                c.getSession().write(MaplePacketCreator.enableActions());
            }
        });
    }

    public void spawnSummon(final MapleSummon summon)
    {
        summon.updateMap(this);
        spawnAndAddRangedMapObject(summon, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                if ((summon != null) && (c.getPlayer() != null) && ((!summon.isChangedMap()) || (summon.getOwnerId() == c.getPlayer().getId())))
                {
                    summon.sendSpawnData(c);
                }
            }
        });
    }

    public void spawnFamiliar(final MonsterFamiliar familiar)
    {
        spawnAndAddRangedMapObject(familiar, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                if ((familiar != null) && (c.getPlayer() != null))
                {
                    c.getSession().write(MaplePacketCreator.spawnFamiliar(familiar, true));
                }
            }
        });
    }

    public void spawnExtractor(final MapleExtractor ex)
    {
        spawnAndAddRangedMapObject(ex, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                ex.sendSpawnData(c);
            }
        });
    }

    public void spawnLove(final MapleLove love)
    {
        spawnAndAddRangedMapObject(love, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                love.sendSpawnData(c);
            }

        });
        Timer.MapTimer tMan = Timer.MapTimer.getInstance();
        tMan.schedule(new Runnable()
        {
            public void run()
            {
                MapleMap.this.broadcastMessage(MaplePacketCreator.removeLove(love.getObjectId(), love.getItemId()));
                MapleMap.this.removeMapObject(love);
            }
        }, 3600000L);
    }

    public void spawnMist(final MapleMist mist, final int duration, boolean fake)
    {
        spawnAndAddRangedMapObject(mist, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                mist.sendSpawnData(c);
            }

        });
        Timer.MapTimer tMan = Timer.MapTimer.getInstance();
        final ScheduledFuture<?> poisonSchedule;
        if ((mist.isPoisonMist()) && (!mist.isMobMist()))
        {
            final MapleCharacter owner = getCharacterById(mist.getOwnerId());
            final boolean pvp = owner != null && owner.inPVP();
            poisonSchedule = tMan.register(new Runnable()
            {
                public void run()
                {
                    for (MapleMapObject mo : MapleMap.this.getMapObjectsInRect(mist.getBox(), Collections.singletonList(pvp ? MapleMapObjectType.PLAYER : MapleMapObjectType.MONSTER)))
                        if ((pvp) && (mist.makeChanceResult()) && (!((MapleCharacter) mo).hasDOT()) && (((MapleCharacter) mo).getId() != mist.getOwnerId()))
                        {
                            ((MapleCharacter) mo).setDOT(mist.getSource().getDOT(), mist.getSourceSkill().getId(), mist.getSkillLevel());
                        }
                        else if ((!pvp) && (mist.makeChanceResult()) && (!((MapleMonster) mo).isBuffed(MonsterStatus.中毒)) && (owner != null))
                            ((MapleMonster) mo).applyStatus(owner, new client.status.MonsterStatusEffect(MonsterStatus.中毒, 1, mist.getSourceSkill().getId(), null, false), true, duration, true,
                                    mist.getSource());
                }
            }, 2000L, 2500L);
        }
        else
        {
            if (mist.isRecoverMist())
            {
                poisonSchedule = tMan.register(() -> {
                    for (MapleMapObject mo : MapleMap.this.getMapObjectsInRect(mist.getBox(), Collections.singletonList(MapleMapObjectType.PLAYER)))
                        if (mist.makeChanceResult())
                        {
                            MapleCharacter chr = (MapleCharacter) mo;
                            chr.addMP((int) (mist.getSource().getX() * (chr.getStat().getMaxMp() / 100.0D)));
                        }
                }, 2000L, 2500L);

            }
            else
            {

                poisonSchedule = null;
            }
        }
        mist.setPoisonSchedule(poisonSchedule);
        mist.setSchedule(tMan.schedule(() -> {
            MapleMap.this.removeMapObject(mist);
            if (poisonSchedule != null)
            {
                poisonSchedule.cancel(false);
            }
            MapleMap.this.broadcastMessage(MaplePacketCreator.removeMist(mist.getObjectId(), false));
        }, duration));
    }

    public MapleCharacter getCharacterById(int id)
    {
        this.charactersLock.readLock().lock();
        try
        {
            for (MapleCharacter mc : this.characters)
            {
                if (mc.getId() == id)
                {
                    return mc;
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return null;
    }

    public List<MapleMapObject> getMapObjectsInRect(Rectangle box, List<MapleMapObjectType> MapObject_types)
    {
        List<MapleMapObject> ret = new ArrayList<>();
        for (MapleMapObjectType type : MapObject_types)
        {
            (mapobjectlocks.get(type)).readLock().lock();
            try
            {
                for (MapleMapObject mmo : (mapobjects.get(type)).values())
                {
                    if (box.contains(mmo.getTruePosition()))
                    {
                        ret.add(mmo);
                    }
                }
            }
            finally
            {
                (mapobjectlocks.get(type)).readLock().unlock();
            }
        }
        return ret;
    }

    public void disappearingItemDrop(MapleMapObject dropper, MapleCharacter owner, Item item, Point pos)
    {
        Point droppos = calcDropPos(pos, pos);
        MapleMapItem drop = new MapleMapItem(item, droppos, dropper, owner, (byte) 1, false);
        broadcastMessage(InventoryPacket.dropItemFromMapObject(drop, dropper.getTruePosition(), droppos, (byte) 3), drop.getTruePosition());
    }

    public Point calcDropPos(Point initial, Point fallback)
    {
        Point ret = calcPointBelow(new Point(initial.x, initial.y - 50));
        if (ret == null)
        {
            return fallback;
        }
        return ret;
    }

    public void broadcastMessage(byte[] packet, Point rangedFrom)
    {
        broadcastMessage(null, packet, GameConstants.maxViewRangeSq(), rangedFrom);
    }

    public void spawnMesoDrop(int meso, Point position, final MapleMapObject dropper, MapleCharacter owner, boolean playerDrop, byte droptype)
    {
        final Point droppos = calcDropPos(position, position);
        final MapleMapItem mdrop = new MapleMapItem(meso, droppos, dropper, owner, droptype, playerDrop);

        spawnAndAddRangedMapObject(mdrop, c -> c.getSession().write(InventoryPacket.dropItemFromMapObject(mdrop, dropper.getTruePosition(), droppos, (byte) 1)));
        if (!this.everlast)
        {
            mdrop.registerExpire(120000L);
            if ((droptype == 0) || (droptype == 1))
            {
                mdrop.registerFFA(30000L);
            }
        }
    }

    public void spawnMobMesoDrop(int meso, final Point position, final MapleMapObject dropper, MapleCharacter owner, boolean playerDrop, byte droptype)
    {
        final MapleMapItem mdrop = new MapleMapItem(meso, position, dropper, owner, droptype, playerDrop);

        spawnAndAddRangedMapObject(mdrop, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(InventoryPacket.dropItemFromMapObject(mdrop, dropper.getTruePosition(), position, (byte) 1));
            }

        });
        mdrop.registerExpire(120000L);
        if ((droptype == 0) || (droptype == 1))
        {
            mdrop.registerFFA(30000L);
        }
    }

    public void spawnMobDrop(final Item idrop, final Point dropPos, final MapleMonster mob, MapleCharacter chr, byte droptype, final int questid)
    {
        final MapleMapItem mdrop = new MapleMapItem(idrop, dropPos, mob, chr, droptype, false, questid);

        spawnAndAddRangedMapObject(mdrop, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                if ((c != null) && (c.getPlayer() != null) && ((questid <= 0) || (c.getPlayer().getQuestStatus(questid) == 1)) && ((idrop.getItemId() / 10000 != 238) || (c.getPlayer().getMonsterBook().getLevelByCard(idrop.getItemId()) >= 2)) && (mob != null) && (dropPos != null))
                {
                    c.getSession().write(InventoryPacket.dropItemFromMapObject(mdrop, mob.getTruePosition(), dropPos, (byte) 1));
                }

            }

        });
        mdrop.registerExpire(120000L);
        if ((droptype == 0) || (droptype == 1))
        {
            mdrop.registerFFA(30000L);
        }
        activateItemReactors(mdrop, chr.getClient());
    }

    public void spawnRandDrop()
    {
        if ((this.mapid != 910000000) || (this.channel != 1))
        {
            return;
        }

        (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().lock();
        try
        {
            for (MapleMapObject o : (mapobjects.get(MapleMapObjectType.ITEM)).values())
            {
                if (((MapleMapItem) o).isRandDrop())
                {
                    return;
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().unlock();
        }
        Timer.MapTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                Point pos = new Point(Randomizer.nextInt(800) + 531, 64730);
                int theItem = Randomizer.nextInt(1000);
                int itemid;
                if (theItem < 950)
                {
                    itemid = GameConstants.normalDrops[Randomizer.nextInt(GameConstants.normalDrops.length)];
                }
                else
                {
                    if (theItem < 990)
                    {
                        itemid = GameConstants.rareDrops[Randomizer.nextInt(GameConstants.rareDrops.length)];
                    }
                    else itemid = GameConstants.superDrops[Randomizer.nextInt(GameConstants.superDrops.length)];
                }
                MapleMap.this.spawnAutoDrop(itemid, pos);
            }
        }, 20000L);
    }

    public void spawnAutoDrop(int itemid, final Point pos)
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        Item idrop;
        if (ItemConstants.getInventoryType(itemid) == MapleInventoryType.EQUIP)
        {
            idrop = ii.randomizeStats((client.inventory.Equip) ii.getEquipById(itemid));
        }
        else
        {
            idrop = new Item(itemid, (short) 0, (short) 1, (short) 0);
        }
        idrop.setGMLog("自动掉落 " + itemid + " 地图 " + this.mapid);
        final MapleMapItem mdrop = new MapleMapItem(pos, idrop);
        spawnAndAddRangedMapObject(mdrop, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(InventoryPacket.dropItemFromMapObject(mdrop, pos, pos, (byte) 1));
            }
        });
        broadcastMessage(InventoryPacket.dropItemFromMapObject(mdrop, pos, pos, (byte) 0));
        if (itemid / 10000 != 291)
        {
            mdrop.registerExpire(120000L);
        }
    }

    public void spawnItemDrop(final MapleMapObject dropper, MapleCharacter owner, Item item, Point pos, boolean ffaDrop, boolean playerDrop)
    {
        final Point droppos = calcDropPos(pos, pos);
        final MapleMapItem drop = new MapleMapItem(item, droppos, dropper, owner, (byte) 2, playerDrop);

        spawnAndAddRangedMapObject(drop, new DelayedPacketCreation()
        {
            public void sendPackets(MapleClient c)
            {
                c.getSession().write(InventoryPacket.dropItemFromMapObject(drop, dropper.getTruePosition(), droppos, (byte) 1));
            }
        });
        broadcastMessage(InventoryPacket.dropItemFromMapObject(drop, dropper.getTruePosition(), droppos, (byte) 0));

        if (!this.everlast)
        {
            drop.registerExpire(120000L);
            activateItemReactors(drop, owner.getClient());
        }
    }

    private void activateItemReactors(MapleMapItem drop, MapleClient c)
    {
        Item item = drop.getItem();

        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            for (MapleMapObject o : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                MapleReactor react = (MapleReactor) o;

                if ((react.getReactorType() == 100) && (item.getItemId() == GameConstants.getCustomReactItem(react.getReactorId(), react.getReactItem().getLeft())) && (react.getReactItem().getRight() == item.getQuantity()) && (react.getArea().contains(drop.getTruePosition())) && (!react.isTimerActive()))
                {
                    Timer.MapTimer.getInstance().schedule(new ActivateItemReactor(drop, react, c), 5000L);
                    react.setTimerActive(true);
                    break;
                }

            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
    }

    public int getItemsSize()
    {
        return (mapobjects.get(MapleMapObjectType.ITEM)).size();
    }

    public int getExtractorSize()
    {
        return (mapobjects.get(MapleMapObjectType.EXTRACTOR)).size();
    }

    public List<MapleMapItem> getAllItems()
    {
        return getAllItemsThreadsafe();
    }

    public List<MapleMapItem> getAllItemsThreadsafe()
    {
        ArrayList<MapleMapItem> ret = new ArrayList<>();
        (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.ITEM)).values())
            {
                ret.add((MapleMapItem) mmo);
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().unlock();
        }
        return ret;
    }

    public Point getPointOfItem(int itemid)
    {
        (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.ITEM)).values())
            {
                MapleMapItem mm = (MapleMapItem) mmo;
                if ((mm.getItem() != null) && (mm.getItem().getItemId() == itemid))
                {
                    return mm.getPosition();
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().unlock();
        }
        return null;
    }

    public List<MapleMist> getAllMistsThreadsafe()
    {
        ArrayList<MapleMist> ret = new ArrayList<>();
        (mapobjectlocks.get(MapleMapObjectType.MIST)).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.MIST)).values())
            {
                ret.add((MapleMist) mmo);
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.MIST)).readLock().unlock();
        }
        return ret;
    }

    public void returnEverLastItem(MapleCharacter chr)
    {
        for (MapleMapObject o : getAllItemsThreadsafe())
        {
            MapleMapItem item = (MapleMapItem) o;
            if (item.getOwner() == chr.getId())
            {
                item.setPickedUp(true);
                broadcastMessage(InventoryPacket.removeItemFromMap(item.getObjectId(), 2, chr.getId()), item.getTruePosition());
                if (item.getMeso() > 0)
                {
                    chr.gainMeso(item.getMeso(), false);
                }
                else
                {
                    server.MapleInventoryManipulator.addFromDrop(chr.getClient(), item.getItem(), false);
                }
                removeMapObject(item);
            }
        }
        spawnRandDrop();
    }

    public void talkMonster(String msg, int itemId, int objectid)
    {
        if (itemId > 0)
        {
            startMapEffect(msg, itemId, false);
        }
        broadcastMessage(MobPacket.talkMonster(objectid, itemId, msg));
        broadcastMessage(MobPacket.removeTalkMonster(objectid));
    }

    public void startMapEffect(String msg, int itemId)
    {
        startMapEffect(msg, itemId, false);
    }

    public void startMapEffect(String msg, int itemId, boolean jukebox)
    {
        if (this.mapEffect != null)
        {
            return;
        }
        this.mapEffect = new MapleMapEffect(msg, itemId);
        this.mapEffect.setJukebox(jukebox);
        broadcastMessage(this.mapEffect.makeStartData());
        Timer.MapTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                if (MapleMap.this.mapEffect != null)
                {
                    MapleMap.this.broadcastMessage(MapleMap.this.mapEffect.makeDestroyData());
                    MapleMap.this.mapEffect = null;
                }
            }
        }, jukebox ? 300000L : 15000L);
    }

    public void startPredictCardMapEffect(String msg, int itemId, int effectType)
    {
        startMapEffect(msg, itemId, 30, effectType);
    }

    public void startMapEffect(String msg, int itemId, int time, int effectType)
    {
        if (this.mapEffect != null)
        {
            return;
        }
        if (time <= 0)
        {
            time = 5;
        }
        this.mapEffect = new MapleMapEffect(msg, itemId, effectType);
        this.mapEffect.setJukebox(false);
        broadcastMessage(this.mapEffect.makeStartData());
        Timer.MapTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                if (MapleMap.this.mapEffect != null)
                {
                    MapleMap.this.broadcastMessage(MapleMap.this.mapEffect.makeDestroyData());
                    MapleMap.this.mapEffect = null;
                }
            }
        }, time * 1000);
    }

    public void startMapEffect(String msg, int itemId, int time)
    {
        startMapEffect(msg, itemId, time, -1);
    }

    public void startExtendedMapEffect(final String msg, final int itemId)
    {
        broadcastMessage(MaplePacketCreator.startMapEffect(msg, itemId, true));
        Timer.MapTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                MapleMap.this.broadcastMessage(MaplePacketCreator.removeMapEffect());
                MapleMap.this.broadcastMessage(MaplePacketCreator.startMapEffect(msg, itemId, false));
            }
        }, 60000L);
    }

    public void startSimpleMapEffect(String msg, int itemId)
    {
        broadcastMessage(MaplePacketCreator.startMapEffect(msg, itemId, true));
    }

    public void startJukebox(String msg, int itemId)
    {
        startMapEffect(msg, itemId, true);
    }

    public void addPlayer(MapleCharacter chr)
    {
        mapobjectlocks.get(MapleMapObjectType.PLAYER).writeLock().lock();
        try
        {
            mapobjects.get(MapleMapObjectType.PLAYER).put(chr.getObjectId(), chr);
        }
        finally
        {
            mapobjectlocks.get(MapleMapObjectType.PLAYER).writeLock().unlock();
        }

        charactersLock.writeLock().lock();
        try
        {
            characters.add(chr);
        }
        finally
        {
            charactersLock.writeLock().unlock();
        }
        boolean 进入地图开启显示数据 = false;
        if (mapid == 109080000 || mapid == 109080001 || mapid == 109080002 || mapid == 109080003 || mapid == 109080010 || mapid == 109080011 || mapid == 109080012)
        {
            chr.setCoconutTeam(getAndSwitchTeam() ? 0 : 1);
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据A");
            }
        }
        if (!chr.isHidden())
        {
            broadcastMessage(chr, MaplePacketCreator.spawnPlayerMapobject(chr), false);
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据B");
            }
            /*for (final MaplePet pet : chr.getPets()) {
                if (pet.getSummoned()) {
                    broadcastMessage(chr, PetPacket.showPet(chr, pet, false, false), false);
                    if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                        System.out.println("进入地图加载数据B+");
                    }
               }
            }*/
            if (chr.isGM() && speedRunStart > 0)
            {
                endSpeedRun();
                broadcastMessage(MaplePacketCreator.serverNotice(5, "The speed run has ended."));
                if (ServerConstants.封包显示 || 进入地图开启显示数据)
                {
                    System.out.println("进入地图加载数据C");
                }
            }
        }
//        if (!chr.isClone())
        if (true)
        {
            //屏蔽地图动画
            /*if (!onFirstUserEnter.equals("")) {
                if (getCharactersSize() == 1) {
                    MapScriptMethods.startScript_FirstUser(chr.getClient(), onFirstUserEnter);
                }
            }*/
            sendObjectPlacement(chr);

            chr.getClient().getSession().write(MaplePacketCreator.spawnPlayerMapobject(chr));
            //屏蔽地图动画
            /*if (!onUserEnter.equals("")) {
                MapScriptMethods.startScript_User(chr.getClient(), onUserEnter);
            }*/
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据D");
            }
            switch (mapid)
            {
                case 109030001:
                case 109040000:
                case 109060001:
                case 109080000:
                case 109080010:
                    chr.getClient().getSession().write(MaplePacketCreator.showEventInstructions());
                    break;
                /*                case 109080000: // coconut shit
                 case 109080001:
                 case 109080002:
                 case 109080003:
                 case 109080010:
                 case 109080011:
                 case 109080012:
                 chr.getClient().getSession().write(MaplePacketCreator.showEquipEffect(chr.getCoconutTeam()));
                 break;*/
                case 809000101:
                case 809000201:
                    chr.getClient().getSession().write(MaplePacketCreator.showEquipEffect());
                    break;
            }
        }
        for (final MaplePet pet : chr.getPets())
        {
            if (pet.getSummoned())
            {
                //chr.getClient().getSession().write(PetPacket.showPet(chr, pet, false, false));
                chr.getClient().getSession().write(PetPacket.updatePet(pet, chr.getInventory(MapleInventoryType.CASH).getItem((byte) pet.getInventoryPosition()), true));
                broadcastMessage(chr, PetPacket.showPet(chr, pet, false, false), false);
                if (ServerConstants.封包显示 || 进入地图开启显示数据)
                {
                    System.out.println("进入地图加载数据F");
                }
            }
        }
//        if (hasForcedEquip())
//        {
//            chr.getClient().getSession().write(MaplePacketCreator.showForcedEquip());
//        }
//        chr.getClient().getSession().write(MaplePacketCreator.removeTutorialStats());
//        if (chr.getMapId() >= 914000200 && chr.getMapId() <= 914000220)
//        {
//            chr.getClient().getSession().write(MaplePacketCreator.addTutorialStats());
//        }
//        if (chr.getMapId() >= 140090100 && chr.getMapId() <= 140090500 || chr.getJob() == 1000 && chr.getMapId() != 130030000)
//        {
//            chr.getClient().getSession().write(MaplePacketCreator.spawnTutorialSummon(1));
//        }
//        if (!onUserEnter.equals(""))
//        {
//            MapScriptMethods.startScript_User(chr.getClient(), onUserEnter);
//        }
//        if (!onFirstUserEnter.equals(""))
//        {
//            if (getCharacters().size() == 1)
//            {
//                MapScriptMethods.startScript_FirstUser(chr.getClient(), onFirstUserEnter);
//            }
//        }
//        final MapleStatEffect stat = chr.getStatForBuff(MapleBuffStat.召唤兽);
//        if (stat != null)
//        {
//            final MapleSummon summon = chr.getSummons().get(stat.getSourceId());
//            summon.setPosition(chr.getPosition());
//            try
//            {
//                summon.setFh(getFootholds().findBelow(chr.getPosition()).getId());
//            }
//            catch (NullPointerException e)
//            {
//                summon.setFh(0); //lol, it can be fixed by movement
//            }
//            this.spawnSummon(summon);
//            chr.addVisibleMapObject(summon);
//            if (ServerConstants.封包显示 || 进入地图开启显示数据)
//            {
//                System.out.println("进入地图加载数据H");
//            }
//        }
        if (chr.getChalkboard() != null)
        {
            chr.getClient().getSession().write(MTSCSPacket.useChalkboard(chr.getId(), chr.getChalkboard()));
        }
        if (timeLimit > 0 && getForcedReturnMap() != null)
        {
            chr.startMapTimeLimitTask(timeLimit, getForcedReturnMap());
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据I");
            }
        }
        if (getSquadBegin() != null && getSquadBegin().getTimeLeft() > 0 && getSquadBegin().getStatus() == 1)
        {
            chr.getClient().getSession().write(MaplePacketCreator.getClock((int) (getSquadBegin().getTimeLeft() / 1000)));
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据O");
            }
        }
        if (chr.getCarnivalParty() != null && chr.getEventInstance() != null)
        {
            chr.getEventInstance().onMapLoad(chr);
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据M");
            }
        }
        MapleEvent.mapLoad(chr, channel);
        if (chr.getEventInstance() != null && chr.getEventInstance().isTimerStarted())
        {
            chr.getClient().getSession().write(MaplePacketCreator.getClock((int) (chr.getEventInstance().getTimeLeft() / 1000)));
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据K");
            }
        }
        if (hasClock())
        {
            final Calendar cal = Calendar.getInstance();
            chr.getClient().getSession().write((MaplePacketCreator.getClockTime(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND))));
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据L");
            }
        }
        if (isTown())
        {
//            chr.cancelEffectFromBuffStat(MapleBuffStat.RAINING_MINES);
            chr.cancelEffectFromBuffStat(MapleBuffStat.DEFAULT_BUFFSTAT);
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据W-------------完");
            }
        }
        chr.getClient().getSession().write((MaplePacketCreator.boatPacket(1)));
//        if (hasBoat() == 2)
//        {
//            chr.getClient().getSession().write((MaplePacketCreator.boatPacket(true)));
//        }
//        else if (hasBoat() == 1 && (chr.getMapId() != 200090000 || chr.getMapId() != 200090010))
//        {
//            chr.getClient().getSession().write(MaplePacketCreator.boatPacket(false));
//        }
        if (chr.getParty() != null)
        {
            //chr.silentPartyUpdate();
            //chr.getClient().getSession().write(MaplePacketCreator.updateParty(chr.getClient().getChannel(), chr.getParty(), PartyOperation.SILENT_UPDATE, null));
            chr.updatePartyMemberHP();
            chr.receivePartyMemberHP();
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据G");
            }
        }
        /*if (mapEffect != null) {
            mapEffect.sendStartData(chr.getClient());
        }*/
       /* if (timeLimit > 0 && getForcedReturnMap() != null ) {
            chr.startMapTimeLimitTask(timeLimit, getForcedReturnMap());
            if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                System.out.println("进入地图加载数据I");
            }
        }
        if (chr.getBuffedValue(MapleBuffStat.MONSTER_RIDING) != null) {
            if (FieldLimitType.Mount.check(fieldLimit)) {
                chr.cancelBuffStats(MapleBuffStat.MONSTER_RIDING);
                if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                    System.out.println("进入地图加载数据J");
                }
            }
        }*/
        /*if (!chr.isClone()) {
            if (chr.getEventInstance() != null && chr.getEventInstance().isTimerStarted() ) {
                chr.getClient().getSession().write(MaplePacketCreator.getClock((int) (chr.getEventInstance().getTimeLeft() / 1000)));
                if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                    System.out.println("进入地图加载数据K");
                }
            }
            if (hasClock()) {
                final Calendar cal = Calendar.getInstance();
                chr.getClient().getSession().write((MaplePacketCreator.getClockTime(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND))));
                if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                    System.out.println("进入地图加载数据L");
                }
            }
            if (chr.getCarnivalParty() != null && chr.getEventInstance() != null) {
                chr.getEventInstance().onMapLoad(chr);
                if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                    System.out.println("进入地图加载数据M");
                }
            }
            MapleEvent.mapLoad(chr, channel);
            if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                System.out.println("进入地图加载数据N");
            }
            if (getSquadBegin() != null && getSquadBegin().getTimeLeft() > 0 && getSquadBegin().getStatus() == 1) {
                chr.getClient().getSession().write(MaplePacketCreator.getClock((int) (getSquadBegin().getTimeLeft() / 1000)));
                if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                    System.out.println("进入地图加载数据O");
                }
            }*/
            /*if (mapid / 1000 != 105100 && mapid / 100 != 8020003 && mapid / 100 != 8020008) { //no boss_balrog/2095/coreblaze/auf. but coreblaze/auf does AFTER
                final MapleSquad sqd = getSquadByMap(); //for all squads
                if (!squadTimer && sqd != null && chr.getName().equals(sqd.getLeaderName()) ) {
                    //leader? display
                    doShrine(false);
                    squadTimer = true;
                }
                if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                    System.out.println("进入地图加载数据P");
                }
            }*/
            /*if (getNumMonsters() > 0 && (mapid == 280030001 || mapid == 240060201 || mapid == 280030000 || mapid == 240060200 || mapid == 220080001 || mapid == 541020800 || mapid == 541010100)) {
                String music = "Bgm09/TimeAttack";
                switch (mapid) {
                    case 240060200:
                    case 240060201:
                        music = "Bgm14/HonTale";
                        break;
                    case 280030000:
                    case 280030001:
                        music = "Bgm06/FinalFight";
                        break;
                    case 200090000:
                    case 200090010:
                        music = "Bgm04/ArabPirate";
                        break;
                }
                chr.getClient().getSession().write(MaplePacketCreator.musicChange(music));
                if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                    System.out.println("进入地图加载数据Q");
                }
                //maybe timer too for zak/ht
            }*/
            /*for (final WeakReference<MapleCharacter> chrz : chr.getClones()) {
                if (chrz.get() != null) {
                    chrz.get().setPosition(new Point(chr.getPosition()));
                    chrz.get().setMap(this);
                    addPlayer(chrz.get());
                }
            if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                System.out.println("进入地图加载数据R");
            }
            }*/
        //  if (mapid == 914000000) {
        ///     chr.getClient().getSession().write(MaplePacketCreator.addTutorialStats());
        //   if (ServerConstants.封包显示 || 进入地图开启显示数据) {
        //       System.out.println("进入地图加载数据S");
        //  }
        // }
            /* else if (mapid == 105100300 && chr.getLevel() >= 91) {
                chr.getClient().getSession().write(MaplePacketCreator.temporaryStats_Balrog(chr));
            if (ServerConstants.封包显示 || 进入地图开启显示数据) {
                System.out.println("进入地图加载数据T");
            }
            } */
        //  else if (mapid == 140090000 || mapid == 105100301 || mapid == 105100401 || mapid == 105100100) {
        //  chr.getClient().getSession().write(MaplePacketCreator.temporaryStats_Reset());
        //   if (ServerConstants.封包显示 || 进入地图开启显示数据) {
        //      System.out.println("进入地图加载数据U");
        //  }
        // }
        //  }
        /*if (GameConstants.isEvan(chr.getJob()) && chr.getJob() >= 2200 && chr.getBuffedValue(MapleBuffStat.MONSTER_RIDING) == null) {
            if (chr.getDragon() == null) {
                chr.makeDragon();
            }
            spawnDragon(chr.getDragon());
            if (!chr.isClone()) {
                updateMapObjectVisibility(chr, chr.getDragon());
            }
        }*/
        // if ((mapid == 10000 && chr.getJob() == 0) || (mapid == 130030000 && chr.getJob() == 1000) || (mapid == 914000000 && chr.getJob() == 2000) || (mapid == 900010000 && chr.getJob() == 2001)) {
//            chr.getClient().getSession().write(MaplePacketCreator.startMapEffect("Welcome to " + chr.getClient().getChannelServer().getServerName() + "!", 5122000, true));
//            chr.dropMessage(1, "Welcome to " + chr.getClient().getChannelServer().getServerName() + ", " + chr.getName() + " ! \r\nUse @joyce to collect your Item Of Appreciation once you're
//            level 10! \r\nUse @help for commands. \r\nGood luck
// and have fun!");
        //   chr.dropMessage(1, "新手技能記得在一轉之前點完 十等之後可以去自由市場找禮物盒領東西");
//            chr.dropMessage(5, "Use @joyce to collect your Item Of Appreciation once you're level 10! Use @help for commands. Good luck and have fun!");
        //    }
        if (permanentWeather > 0)
        {
            chr.getClient().getSession().write(MaplePacketCreator.startMapEffect("", permanentWeather, false)); //snow, no msg
        }
        if (getPlatforms().size() > 0)
        {
            chr.getClient().getSession().write(MaplePacketCreator.getMovingPlatforms(this));
        }
        if (environment.size() > 0)
        {
            chr.getClient().getSession().write(MaplePacketCreator.getUpdateEnvironment(this));
        }
        if (isTown())
        {
            chr.cancelEffectFromBuffStat(MapleBuffStat.DEFAULT_BUFFSTAT);
            if (ServerConstants.封包显示 || 进入地图开启显示数据)
            {
                System.out.println("进入地图加载数据W-------------完");
            }
        }
    }

    public boolean getAndSwitchTeam()
    {
        return getCharactersSize() % 2 != 0;
    }

    public void broadcastMessage(MapleCharacter source, byte[] packet, boolean repeatToSource)
    {
        broadcastMessage(repeatToSource ? null : source, packet, Double.POSITIVE_INFINITY, source.getTruePosition());
    }

    public void endSpeedRun()
    {
        this.speedRunStart = 0L;
        this.speedRunLeader = "";
    }

    private void sendObjectPlacement(MapleCharacter chr)
    {
        if (chr == null)
        {
            return;
        }
        for (MapleMapObject o : getMapObjectsInRange(chr.getTruePosition(), chr.getRange(), GameConstants.rangedMapobjectTypes))
            if ((o.getType() != MapleMapObjectType.REACTOR) || (((MapleReactor) o).isAlive()))
            {


                o.sendSpawnData(chr.getClient());
                chr.addVisibleMapObject(o);
            }
    }

    public MapleMap getForcedReturnMap()
    {
        return ChannelServer.getInstance(this.channel).getMapFactory().getMap(this.forcedReturnMap);
    }

    public void setForcedReturnMap(int map)
    {
        this.forcedReturnMap = map;
    }

    public MapleSquad getSquadBegin()
    {
        if (this.squad != null)
        {
            return ChannelServer.getInstance(this.channel).getMapleSquad(this.squad);
        }
        return null;
    }

    public boolean hasClock()
    {
        return this.clock;
    }

    public boolean isTown()
    {
        return this.town;
    }

    public void setTown(boolean town)
    {
        this.town = town;
    }

    public List<MapleNodes.MaplePlatform> getPlatforms()
    {
        return this.nodes.getPlatforms();
    }

    public int getCharactersSize()
    {
        this.charactersLock.readLock().lock();
        try
        {
            return this.characters.size();
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
    }

    public List<MapleMapObject> getMapObjectsInRange(Point from, double rangeSq, List<MapleMapObjectType> MapObject_types)
    {
        List<MapleMapObject> ret = new ArrayList<>();
        for (MapleMapObjectType type : MapObject_types)
        {
            (mapobjectlocks.get(type)).readLock().lock();
            try
            {
                for (MapleMapObject mmo : (mapobjects.get(type)).values())
                {
                    if (from.distanceSq(mmo.getTruePosition()) <= rangeSq)
                    {
                        ret.add(mmo);
                    }
                }
            }
            finally
            {
                (mapobjectlocks.get(type)).readLock().unlock();
            }
        }
        return ret;
    }

    private boolean hasForcedEquip()
    {
        return fieldType == 81 || fieldType == 82;
    }

    public void setFieldType(int fieldType)
    {
        this.fieldType = fieldType;
    }

    public int getNumItems()
    {
        (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().lock();
        try
        {
            return (mapobjects.get(MapleMapObjectType.ITEM)).size();
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().unlock();
        }
    }

    public void doShrine(final boolean spawned)
    {
        if (this.squadSchedule != null)
        {
            cancelSquadSchedule(true);
        }
        MapleSquad sqd = getSquadByMap();
        if (sqd == null)
        {
            return;
        }
        final int mode = (this.mapid == 240060200) || (this.mapid == 240060201) ? 3 : this.mapid == 280030001 ? 2 : (this.mapid == 280030000) || (this.mapid == 280030100) ? 1 : 0;

        EventManager em = getEMByMap();
        if ((sqd != null) && (em != null) && (getCharactersSize() > 0))
        {
            final String leaderName = sqd.getLeaderName();
            final String state = em.getProperty("state");

            MapleMap returnMapa = getForcedReturnMap();
            if ((returnMapa == null) || (returnMapa.getId() == this.mapid))
            {
                returnMapa = getReturnMap();
            }
            if (((mode != 3)) ||


                    (spawned))
            {
                broadcastMessage(MaplePacketCreator.getClock(300));
            }
            final MapleMap returnMapz = returnMapa;
            Runnable run;
            if (!spawned)
            {
                final List<MapleMonster> monsterz = getAllMonstersThreadsafe();
                final List<Integer> monsteridz = new ArrayList<>();
                for (MapleMapObject m : monsterz)
                {
                    monsteridz.add(m.getObjectId());
                }
                run = new Runnable()
                {
                    public void run()
                    {
                        MapleSquad sqnow = MapleMap.this.getSquadByMap();
                        if ((MapleMap.this.getCharactersSize() > 0) && (MapleMap.this.getNumMonsters() == monsterz.size()) && (sqnow != null) && (sqnow.getStatus() == 2) && (sqnow.getLeaderName().equals(leaderName)) && (MapleMap.this.getEMByMap().getProperty("state").equals(state)))
                        {
                            boolean passed = monsterz.isEmpty();
                            for (MapleMonster mapleMonster : MapleMap.this.getAllMonstersThreadsafe())
                            {
                                MapleMapObject m = mapleMonster;
                                for (Integer integer : monsteridz)
                                {
                                    int i = integer;
                                    if (m.getObjectId() == i)
                                    {
                                        passed = true;
                                        break;
                                    }
                                }
                                if (passed) break;
                            }
                            MapleMapObject m;
                            if (passed)
                            {
                                byte[] packet;
                                if ((mode == 1) || (mode == 2))
                                {
                                    packet = MaplePacketCreator.showChaosZakumShrine(spawned, 0);
                                }
                                else
                                {
                                    packet = MaplePacketCreator.showHorntailShrine(spawned, 0);
                                }
                                for (MapleCharacter chr : MapleMap.this.getCharactersThreadsafe())
                                {
                                    chr.changeMap(returnMapz, returnMapz.getPortal(0));
                                }
                                MapleMap.this.checkStates("");
                                MapleMap.this.resetFully();
                            }
                        }
                    }
                };
            }
            else
            {
                run = new Runnable()
                {
                    public void run()
                    {
                        MapleSquad sqnow = MapleMap.this.getSquadByMap();

                        if ((MapleMap.this.getCharactersSize() > 0) && (sqnow != null) && (sqnow.getStatus() == 2) && (sqnow.getLeaderName().equals(leaderName)) && (MapleMap.this.getEMByMap().getProperty("state").equals(state)))
                        {
                            byte[] packet;
                            if ((mode == 1) || (mode == 2))
                            {
                                packet = MaplePacketCreator.showChaosZakumShrine(spawned, 0);
                            }
                            else
                            {
                                packet = MaplePacketCreator.showHorntailShrine(spawned, 0);
                            }
                            for (MapleCharacter chr : MapleMap.this.getCharactersThreadsafe())
                            {
                                chr.changeMap(returnMapz, returnMapz.getPortal(0));
                            }
                            MapleMap.this.checkStates("");
                            MapleMap.this.resetFully();
                        }
                    }
                };
            }
            this.squadSchedule = Timer.MapTimer.getInstance().schedule(run, 300000L);
        }
    }

    public EventManager getEMByMap()
    {
        String em;
        switch (this.mapid)
        {
            case 105100400:
                em = "BossBalrog_EASY";
                break;
            case 105100300:
                em = "BossBalrog_NORMAL";
                break;
            case 280030000:
            case 280030100:
                em = "ZakumBattle";
                break;
            case 240060200:
                em = "HorntailBattle";
                break;
            case 280030001:
                em = "ChaosZakum";
                break;
            case 240060201:
                em = "ChaosHorntail";
                break;
            case 270050100:
                em = "PinkBeanBattle";
                break;
            case 270051100:
                em = "ChaosPinkBean";
                break;
            case 802000111:
                em = "NamelessMagicMonster";
                break;
            case 802000211:
                em = "Vergamot";
                break;
            case 802000311:
                em = "2095_tokyo";
                break;
            case 802000411:
                em = "Dunas";
                break;
            case 802000611:
                em = "Nibergen";
                break;
            case 802000711:
                em = "Dunas2";
                break;
            case 802000801:
            case 802000802:
            case 802000803:
                em = "CoreBlaze";
                break;
            case 802000821:
            case 802000823:
                em = "Aufhaven";
                break;
            case 211070100:
            case 211070101:
            case 211070110:
                em = "VonLeonBattle";
                break;
            case 551030200:
                em = "ScarTarBattle";
                break;
            case 271040100:
                em = "CygnusBattle";
                break;
            case 689013000:
                em = "PinkZakum";
                break;
            case 262031300:
            case 262031310:
                em = "Hillah_170";
                break;
            case 272030400:
            case 272030420:
                em = "ArkariumBattle";
                break;
            default:
                return null;
        }
        return ChannelServer.getInstance(this.channel).getEventSM().getEventManager(em);
    }

    public void removePlayer(MapleCharacter chr)
    {
        //log.warn("[dc] [level2] Player {} leaves map {}", new Object[] { chr.getName(), mapid });

        if (everlast)
        {
            returnEverLastItem(chr);
        }

        charactersLock.writeLock().lock();
        try
        {
            characters.remove(chr);
        }
        finally
        {
            charactersLock.writeLock().unlock();
        }
        removeMapObject(chr);
        chr.checkFollow();
        broadcastMessage(MaplePacketCreator.removePlayerFromMap(chr.getId()));
//        if (!chr.isClone())
//        {
//            final List<MapleMonster> update = new ArrayList<>();
//            final Iterator<MapleMonster> controlled = chr.getControlled().iterator();
//
//            while (controlled.hasNext())
//            {
//                MapleMonster monster = controlled.next();
//                if (monster != null)
//                {
//                    monster.setController(null);
//                    monster.setControllerHasAggro(false);
//                    monster.setControllerKnowsAboutAggro(false);
//                    controlled.remove();
//                    update.add(monster);
//                }
//            }
//            for (MapleMonster mons : update)
//            {
//                updateMonsterController(mons);
//            }
//            chr.leaveMap(chr.getMap());
//            checkStates(chr.getName());
//            if (mapid == 109020001)
//            {
//                chr.canTalk(true);
//            }
//            for (final WeakReference<MapleCharacter> chrz : chr.getClones())
//            {
//                if (chrz.get() != null)
//                {
//                    removePlayer(chrz.get());
//                }
//            }
//        }
//        chr.cancelEffectFromBuffStat(MapleBuffStat.PUPPET);
//        chr.cancelEffectFromBuffStat(MapleBuffStat.REAPER);
//        boolean cancelSummons = false;
//        for (final MapleSummon summon : chr.getSummons().values())
//        {
//            if (summon.getMovementType() == SummonMovementType.STATIONARY || summon.getMovementType() == SummonMovementType.CIRCLE_STATIONARY || summon.getMovementType() == SummonMovementType
//            .WALK_STATIONARY)
//            {
//                cancelSummons = true;
//            }
//            else
//            {
//                summon.setChangedMap(true);
//                removeMapObject(summon);
//            }
//        }
//        if (cancelSummons)
//        {
        chr.cancelEffectFromBuffStat(MapleBuffStat.召唤兽);

//        }
        if (chr.getDragon() != null)
        {
            removeMapObject(chr.getDragon());
        }
    }

    public void broadcastMessage(MapleCharacter source, byte[] packet, Point rangedFrom)
    {
        broadcastMessage(source, packet, GameConstants.maxViewRangeSq(), rangedFrom);
    }

    public List<MaplePortal> getPortalsInRange(Point from, double rangeSq)
    {
        List<MaplePortal> ret = new ArrayList<>();
        for (MaplePortal type : this.portals.values())
        {
            if ((from.distanceSq(type.getPosition()) <= rangeSq) && (type.getTargetMapId() != this.mapid) && (type.getTargetMapId() != 999999999))
            {
                ret.add(type);
            }
        }
        return ret;
    }

    public List<MapleMapObject> getItemsInRange(Point from, double rangeSq)
    {
        return getMapObjectsInRange(from, rangeSq, java.util.Arrays.asList(MapleMapObjectType.ITEM));
    }

    public List<MapleMapObject> getMonstersInRange(Point from, double rangeSq)
    {
        return getMapObjectsInRange(from, rangeSq, java.util.Arrays.asList(MapleMapObjectType.MONSTER));
    }

    public List<MapleCharacter> getCharactersIntersect(Rectangle box)
    {
        List<MapleCharacter> ret = new ArrayList<>();
        this.charactersLock.readLock().lock();
        try
        {
            for (MapleCharacter chr : this.characters)
            {
                if (chr.getBounds().intersects(box))
                {
                    ret.add(chr);
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return ret;
    }

    public List<MapleCharacter> getPlayersInRectAndInList(Rectangle box, List<MapleCharacter> chrList)
    {
        List<MapleCharacter> character = new LinkedList<>();

        this.charactersLock.readLock().lock();
        try
        {

            for (MapleCharacter a : this.characters)
            {
                if ((chrList.contains(a)) && (box.contains(a.getTruePosition())))
                {
                    character.add(a);
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return character;
    }

    public void addPortal(MaplePortal myPortal)
    {
        this.portals.put(myPortal.getId(), myPortal);
    }

    public MaplePortal getPortal(String portalname)
    {
        for (MaplePortal port : this.portals.values())
        {
            if (port.getName().equals(portalname))
            {
                return port;
            }
        }
        return null;
    }

    public void resetPortals()
    {
        for (MaplePortal port : this.portals.values())
        {
            port.setPortalState(true);
        }
    }

    public int getNumSpawnPoints()
    {
        return this.monsterSpawn.size();
    }

    public void loadMonsterRate(boolean first)
    {
        int spawnSize = this.monsterSpawn.size();
        if ((spawnSize >= 20) || (this.partyBonusRate > 0))
        {
            this.maxRegularSpawn = Math.round(spawnSize / this.monsterRate);
        }
        else
        {
            this.maxRegularSpawn = ((int) Math.ceil(spawnSize * this.monsterRate));
        }
        if (this.fixedMob > 0)
        {
            this.maxRegularSpawn = this.fixedMob;
        }
        else if (this.maxRegularSpawn <= 2)
        {
            this.maxRegularSpawn = 2;
        }
        else if (this.maxRegularSpawn > spawnSize)
        {
            this.maxRegularSpawn = Math.max(10, spawnSize);
        }

        Collection<Spawns> newSpawn = new LinkedList<>();
        Collection<Spawns> newBossSpawn = new LinkedList<>();
        for (Spawns s : this.monsterSpawn)
        {
            if (s.getCarnivalTeam() < 2)
            {

                if (s.getMonster().isBoss())
                {
                    newBossSpawn.add(s);
                }
                else newSpawn.add(s);
            }
        }
        this.monsterSpawn.clear();
        this.monsterSpawn.addAll(newBossSpawn);
        this.monsterSpawn.addAll(newSpawn);

        if ((first) && (spawnSize > 0))
        {
            this.lastSpawnTime = System.currentTimeMillis();
            if (GameConstants.isForceRespawn(this.mapid))
            {
                this.createMobInterval = 15000;
            }
        }
    }

    public void addAreaMonsterSpawn(MapleMonster monster, Point pos1, Point pos2, Point pos3, int mobTime, String msg, boolean shouldSpawn, boolean sendWorldMsg)
    {
        pos1 = calcPointBelow(pos1);
        pos2 = calcPointBelow(pos2);
        pos3 = calcPointBelow(pos3);
        if (pos1 != null)
        {
            pos1.y -= 1;
        }
        if (pos2 != null)
        {
            pos2.y -= 1;
        }
        if (pos3 != null)
        {
            pos3.y -= 1;
        }
        if ((pos1 == null) && (pos2 == null) && (pos3 == null))
        {
            System.out.println("WARNING: mapid " + this.mapid + ", monster " + monster.getId() + " could not be spawned.");
            return;
        }
        if (pos1 != null)
        {
            if (pos2 == null)
            {
                pos2 = new Point(pos1);
            }
            if (pos3 == null)
            {
                pos3 = new Point(pos1);
            }
        }
        else if (pos2 != null)
        {
            if (pos1 == null)
            {
                pos1 = new Point(pos2);
            }
            if (pos3 == null)
            {
                pos3 = new Point(pos2);
            }
        }
        else if (pos3 != null)
        {
            if (pos1 == null)
            {
                pos1 = new Point(pos3);
            }
            if (pos2 == null)
            {
                pos2 = new Point(pos3);
            }
        }
        this.monsterSpawn.add(new server.life.SpawnPointAreaBoss(monster, pos1, pos2, pos3, mobTime, msg, shouldSpawn, sendWorldMsg));
    }

    public List<MapleCharacter> getCharacters()
    {
        return getCharactersThreadsafe();
    }

    public List<MapleCharacter> getCharactersThreadsafe()
    {
        List<MapleCharacter> chars = new ArrayList<>();
        this.charactersLock.readLock().lock();
        try
        {
            for (MapleCharacter mc : this.characters)
            {
                chars.add(mc);
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return chars;
    }

    public MapleCharacter getCharacterByName(String id)
    {
        this.charactersLock.readLock().lock();
        try
        {
            for (MapleCharacter mc : this.characters)
            {
                if (mc.getName().equalsIgnoreCase(id))
                {
                    return mc;
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return null;
    }

    public MapleCharacter getCharacterById_InMap(int id)
    {
        return getCharacterById(id);
    }

    public void moveMonster(MapleMonster monster, Point reportedPos)
    {
        monster.setPosition(reportedPos);

        this.charactersLock.readLock().lock();
        try
        {
            for (MapleCharacter mc : this.characters)
            {
                updateMapObjectVisibility(mc, monster);
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
    }

    public void updateMapObjectVisibility(MapleCharacter chr, MapleMapObject mo)
    {
        if (chr == null)
        {
            return;
        }
        if (!chr.isMapObjectVisible(mo))
        {
            if ((mo.getType() == MapleMapObjectType.MIST) || (mo.getType() == MapleMapObjectType.EXTRACTOR) || (mo.getType() == MapleMapObjectType.SUMMON) || (mo.getType() == MapleMapObjectType.FAMILIAR) || ((mo instanceof MechDoor)) || (mo.getTruePosition().distanceSq(chr.getTruePosition()) <= mo.getRange()))
            {
                chr.addVisibleMapObject(mo);
                mo.sendSpawnData(chr.getClient());
            }
        }
        else if ((!(mo instanceof MechDoor)) && (mo.getType() != MapleMapObjectType.MIST) && (mo.getType() != MapleMapObjectType.EXTRACTOR) && (mo.getType() != MapleMapObjectType.SUMMON) && (mo.getType() != MapleMapObjectType.FAMILIAR) && (mo.getTruePosition().distanceSq(chr.getTruePosition()) > mo.getRange()))
        {
            chr.removeVisibleMapObject(mo);
            mo.sendDestroyData(chr.getClient());
        }
        else if ((mo.getType() == MapleMapObjectType.MONSTER) && (chr.getTruePosition().distanceSq(mo.getTruePosition()) <= GameConstants.maxViewRangeSq_Half()))
        {
            updateMonsterController((MapleMonster) mo);
        }
    }

    public void movePlayer(MapleCharacter player, Point newPosition)
    {
        player.setPosition(newPosition);
        try
        {
            Collection<MapleMapObject> visibleObjects = player.getAndWriteLockVisibleMapObjects();
            ArrayList<MapleMapObject> copy = new ArrayList(visibleObjects);
            for (MapleMapObject mo : copy)
            {
                if ((mo != null) && (getMapObject(mo.getObjectId(), mo.getType()) == mo))
                {
                    updateMapObjectVisibility(player, mo);
                }
                else if (mo != null)
                {
                    visibleObjects.remove(mo);
                }
            }
            for (MapleMapObject mo : getMapObjectsInRange(player.getTruePosition(), player.getRange()))
                if ((mo != null) && (!visibleObjects.contains(mo)))
                {
                    mo.sendSpawnData(player.getClient());
                    visibleObjects.add(mo);
                }
        }
        finally
        {
            Collection<MapleMapObject> visibleObjects;
            MapleMapObject mo;
            player.unlockWriteVisibleMapObjects();
        }
    }

    public List<MapleMapObject> getMapObjectsInRange(Point from, double rangeSq)
    {
        List<MapleMapObject> ret = new ArrayList<>();
        for (MapleMapObjectType type : MapleMapObjectType.values())
        {
            (mapobjectlocks.get(type)).readLock().lock();
            try
            {
                for (MapleMapObject mmo : (mapobjects.get(type)).values())
                {
                    if (from.distanceSq(mmo.getTruePosition()) <= rangeSq)
                    {
                        ret.add(mmo);
                    }
                }
            }
            finally
            {
                (mapobjectlocks.get(type)).readLock().unlock();
            }
        }
        return ret;
    }

    public MaplePortal findClosestSpawnpoint(Point from)
    {
        MaplePortal closest = getPortal(0);
        double shortestDistance = Double.POSITIVE_INFINITY;
        for (MaplePortal portal : this.portals.values())
        {
            double distance = portal.getPosition().distanceSq(from);
            if ((portal.getType() >= 0) && (portal.getType() <= 2) && (distance < shortestDistance) && (portal.getTargetMapId() == 999999999))
            {
                closest = portal;
                shortestDistance = distance;
            }
        }
        return closest;
    }

    public MaplePortal getPortal(int portalid)
    {
        return this.portals.get(portalid);
    }

    public MaplePortal findClosestPortal(Point from)
    {
        MaplePortal closest = getPortal(0);
        double shortestDistance = Double.POSITIVE_INFINITY;
        for (MaplePortal portal : this.portals.values())
        {
            double distance = portal.getPosition().distanceSq(from);
            if (distance < shortestDistance)
            {
                closest = portal;
                shortestDistance = distance;
            }
        }
        return closest;
    }

    public String spawnDebug()
    {

        String sb =
                "Mobs in map : " + getMobsSize() + " spawnedMonstersOnMap: " + this.spawnedMonstersOnMap + " spawnpoints: " + this.monsterSpawn.size() + " maxRegularSpawn: " + this.maxRegularSpawn + " " + "actual monsters: " + getNumMonsters() + " monster rate: " + this.monsterRate + " fixed: " + this.fixedMob;
        return sb;
    }

    public int getMobsSize()
    {
        return (mapobjects.get(MapleMapObjectType.MONSTER)).size();
    }

    public int getNumMonsters()
    {
        (mapobjectlocks.get(MapleMapObjectType.MONSTER)).readLock().lock();
        try
        {
            return (mapobjects.get(MapleMapObjectType.MONSTER)).size();
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.MONSTER)).readLock().unlock();
        }
    }

    public int getMapObjectSize()
    {
        return this.mapobjects.size();
    }

    public Collection<MaplePortal> getPortals()
    {
        return Collections.unmodifiableCollection(this.portals.values());
    }

    public int getSpawnedMonstersOnMap()
    {
        return this.spawnedMonstersOnMap.get();
    }

    public void respawn(boolean force)
    {
        respawn(force, System.currentTimeMillis());
    }

    public void respawn(boolean force, long now)
    {
        this.lastSpawnTime = now;
        int numShouldSpawn;
        int spawned;
        Iterator localIterator;
        Spawns spawnPoint;
        if (force)
        {
            numShouldSpawn = this.monsterSpawn.size() - this.spawnedMonstersOnMap.get();

            if (numShouldSpawn > 0)
            {
                spawned = 0;

                for (localIterator = this.monsterSpawn.iterator(); localIterator.hasNext(); )
                {
                    spawnPoint = (Spawns) localIterator.next();
                    spawnPoint.spawnMonster(this);
                    spawned++;
                    if (spawned >= numShouldSpawn)
                    {
                        break;
                    }
                }
            }
        }
        else
        {
            numShouldSpawn = (GameConstants.isForceRespawn(this.mapid) ? this.monsterSpawn.size() : this.maxRegularSpawn) - this.spawnedMonstersOnMap.get();
            if (numShouldSpawn > 0)
            {
                spawned = 0;

                List<Spawns> randomSpawn = new ArrayList(this.monsterSpawn);
                Collections.shuffle(randomSpawn);

                for (Spawns sPoint : randomSpawn)
                {
                    if ((this.isSpawns) || (sPoint.getMobTime() <= 0))
                    {

                        if ((sPoint.shouldSpawn(this.lastSpawnTime)) || (GameConstants.isForceRespawn(this.mapid)) || ((this.monsterSpawn.size() < 10) && (this.maxRegularSpawn > this.monsterSpawn.size()) && (this.partyBonusRate > 0)))
                        {
                            sPoint.spawnMonster(this);
                            spawned++;
                        }
                        if (spawned >= numShouldSpawn)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    public String getSnowballPortal()
    {
        int[] teamss = new int[2];
        this.charactersLock.readLock().lock();
        try
        {
            for (MapleCharacter chr : this.characters)
            {
                if (chr.getTruePosition().y > -80)
                {
                    teamss[0] += 1;
                }
                else
                {
                    teamss[1] += 1;
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        if (teamss[0] > teamss[1])
        {
            return "st01";
        }
        return "st00";
    }

    public boolean isDisconnected(int id)
    {
        return this.dced.contains(id);
    }

    public void addDisconnected(int id)
    {
        this.dced.add(id);
    }

    public void resetDisconnected()
    {
        this.dced.clear();
    }

    public void startSpeedRun()
    {
        MapleSquad squads = getSquadByMap();
        if (squads != null)
        {
            this.charactersLock.readLock().lock();
            try
            {
                for (MapleCharacter chr : this.characters)
                {
                    if ((chr.getName().equals(squads.getLeaderName())) && (!chr.isIntern()))
                    {
                        startSpeedRun(chr.getName());
                        return;
                    }
                }
            }
            finally
            {
                this.charactersLock.readLock().unlock();
            }
        }
    }

    public MapleSquad getSquadByMap()
    {
        MapleSquadType zz;
        switch (this.mapid)
        {
            case 105100300:
            case 105100400:
                zz = MapleSquadType.bossbalrog;
                break;
            case 280030000:
            case 280030100:
                zz = MapleSquadType.zak;
                break;
            case 280030001:
                zz = MapleSquadType.chaoszak;
                break;
            case 240060200:
                zz = MapleSquadType.horntail;
                break;
            case 240060201:
                zz = MapleSquadType.chaosht;
                break;
            case 270050100:
                zz = MapleSquadType.pinkbean;
                break;
            case 270051100:
                zz = MapleSquadType.chaospb;
                break;
            case 802000111:
                zz = MapleSquadType.nmm_squad;
                break;
            case 802000211:
                zz = MapleSquadType.vergamot;
                break;
            case 802000311:
                zz = MapleSquadType.tokyo_2095;
                break;
            case 802000411:
                zz = MapleSquadType.dunas;
                break;
            case 802000611:
                zz = MapleSquadType.nibergen_squad;
                break;
            case 802000711:
                zz = MapleSquadType.dunas2;
                break;
            case 802000801:
            case 802000802:
            case 802000803:
                zz = MapleSquadType.core_blaze;
                break;
            case 802000821:
            case 802000823:
                zz = MapleSquadType.aufheben;
                break;
            case 211070100:
            case 211070101:
            case 211070110:
                zz = MapleSquadType.vonleon;
                break;
            case 551030200:
                zz = MapleSquadType.scartar;
                break;
            case 271040100:
                zz = MapleSquadType.cygnus;
                break;
            case 689013000:
                zz = MapleSquadType.pinkzak;
                break;
            case 262031300:
            case 262031310:
                zz = MapleSquadType.hillah;
                break;
            case 272030400:
            case 272030420:
                zz = MapleSquadType.arkarium;
                break;
            default:
                return null;
        }
        return ChannelServer.getInstance(this.channel).getMapleSquad(zz);
    }

    public void startSpeedRun(String leader)
    {
        this.speedRunStart = System.currentTimeMillis();
        this.speedRunLeader = leader;
    }

    public void getRankAndAdd(String leader, String time, ExpeditionType type, long timz, Collection<String> squad)
    {
        try
        {
            long lastTime = SpeedRunner.getSpeedRunData(type) == null ? 0L : SpeedRunner.getSpeedRunData(type).right;
            StringBuilder rett = new StringBuilder();
            if (squad != null)
            {
                for (String chr : squad)
                {
                    rett.append(chr);
                    rett.append(",");
                }
            }
            String z = rett.toString();
            if (squad != null)
            {
                z = z.substring(0, z.length() - 1);
            }
            java.sql.Connection con = database.DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO speedruns(`type`, `leader`, `timestring`, `time`, `members`) VALUES (?,?,?,?,?)");
            ps.setString(1, type.name());
            ps.setString(2, leader);
            ps.setString(3, time);
            ps.setLong(4, timz);
            ps.setString(5, z);
            ps.executeUpdate();
            ps.close();

            if (lastTime == 0L)
            {
                SpeedRunner.addSpeedRunData(type, SpeedRunner.addSpeedRunData(new StringBuilder(SpeedRunner.getPreamble(type)), new HashMap(), z, leader, 1, time), timz);
            }
            else
            {
                SpeedRunner.removeSpeedRunData(type);
                SpeedRunner.loadSpeedRunData(type);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public long getSpeedRunStart()
    {
        return this.speedRunStart;
    }

    public void disconnectAll()
    {
        for (MapleCharacter chr : getCharactersThreadsafe())
        {
            if (!chr.isGM())
            {
                chr.getClient().disconnect(true, false);
                chr.getClient().getSession().close(true);
            }
        }
    }

    public List<MapleNPC> getAllNPCs()
    {
        return getAllNPCsThreadsafe();
    }

    public List<MapleNPC> getAllNPCsThreadsafe()
    {
        ArrayList<MapleNPC> ret = new ArrayList<>();
        (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.NPC)).values())
            {
                ret.add((MapleNPC) mmo);
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.NPC)).readLock().unlock();
        }
        return ret;
    }

    public void resetNPCs()
    {
        removeNpc(-1);
    }

    public void resetPQ(int level)
    {
        resetFully();
        for (MapleMonster mons : getAllMonstersThreadsafe())
        {
            mons.changeLevel(level, true);
        }
        resetSpawnLevel(level);
    }

    public void resetSpawnLevel(int level)
    {
        for (Spawns spawn : this.monsterSpawn)
        {
            if ((spawn instanceof SpawnPoint))
            {
                ((SpawnPoint) spawn).setLevel(level);
            }
        }
    }

    public void resetFully()
    {
        resetFully(true);
    }

    public void resetFully(boolean respawn)
    {
        killAllMonsters(false);
        reloadReactors();
        removeDrops();
        resetNPCs();
        resetSpawns();
        resetDisconnected();
        endSpeedRun();
        cancelSquadSchedule(true);
        resetPortals();
        this.environment.clear();
        if (respawn)
        {
            respawn(true);
        }
    }

    public void cancelSquadSchedule(boolean interrupt)
    {
        this.squadTimer = false;
        this.checkStates = true;
        if (this.squadSchedule != null)
        {
            this.squadSchedule.cancel(interrupt);
            this.squadSchedule = null;
        }
    }

    public void removeDrops()
    {
        List<MapleMapItem> mapItems = getAllItemsThreadsafe();
        for (MapleMapItem mapItem : mapItems)
        {
            mapItem.expire(this);
        }
    }

    public void removeDropsDelay()
    {
        List<MapleMapItem> mapItems = getAllItemsThreadsafe();
        int delay = 0;
        int i = 0;
        for (MapleMapItem mapItem : mapItems)
        {
            i++;
            if (i < 50)
            {
                mapItem.expire(this);
            }
            else
            {
                delay++;
                if (mapItem.hasFFA())
                {
                    mapItem.registerFFA(delay * 20);
                }
                else
                {
                    mapItem.registerExpire(delay * 30);
                }
            }
        }
    }

    public void resetAllSpawnPoint(int mobid, int mobTime)
    {
        Collection<Spawns> AllSpawnPoints = new LinkedList(this.monsterSpawn);
        resetFully();
        this.monsterSpawn.clear();
        for (Spawns spawnPoint : AllSpawnPoints)
        {
            MapleMonster newMons = MapleLifeFactory.getMonster(mobid);
            newMons.setF(spawnPoint.getF());
            newMons.setFh(spawnPoint.getFh());
            newMons.setPosition(spawnPoint.getPosition());
            addMonsterSpawn(newMons, mobTime, (byte) -1, null);
        }
        loadMonsterRate(true);
    }

    public void resetSpawns()
    {
        boolean changed = false;
        Iterator<Spawns> AllSpawnPoints = this.monsterSpawn.iterator();
        while (AllSpawnPoints.hasNext())
        {
            if (AllSpawnPoints.next().getCarnivalId() > -1)
            {
                AllSpawnPoints.remove();
                changed = true;
            }
        }
        setSpawns(true);
        if (changed)
        {
            loadMonsterRate(true);
        }
    }

    public boolean makeCarnivalSpawn(int team, MapleMonster newMons, int num)
    {
        MapleNodes.MonsterPoint ret = null;
        for (MapleNodes.MonsterPoint mp : this.nodes.getMonsterPoints())
        {
            if ((mp.team == team) || (mp.team == -1))
            {
                Point newpos = calcPointBelow(new Point(mp.x, mp.y));
                newpos.y -= 1;
                boolean found = false;
                for (Spawns s : this.monsterSpawn)
                {
                    if ((s.getCarnivalId() > -1) && ((mp.team == -1) || (s.getCarnivalTeam() == mp.team)) && (s.getPosition().x == newpos.x) && (s.getPosition().y == newpos.y))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    ret = mp;
                    break;
                }
            }
        }
        if (ret != null)
        {
            newMons.setCy(ret.cy);
            newMons.setF(0);
            newMons.setFh(ret.fh);
            newMons.setRx0(ret.x + 50);
            newMons.setRx1(ret.x - 50);
            newMons.setPosition(new Point(ret.x, ret.y));
            newMons.setHide(false);
            SpawnPoint sp = addMonsterSpawn(newMons, 1, (byte) team, null);
            sp.setCarnival(num);
        }
        return ret != null;
    }

    public SpawnPoint addMonsterSpawn(MapleMonster monster, int mobTime, byte carnivalTeam, String msg)
    {
        Point newpos = calcPointBelow(monster.getPosition());
        newpos.y -= 1;
        SpawnPoint sp = new SpawnPoint(monster, newpos, mobTime, carnivalTeam, msg);
        if (carnivalTeam > -1)
        {
            this.monsterSpawn.add(0, sp);
        }
        else
        {
            this.monsterSpawn.add(sp);
        }
        return sp;
    }

    public boolean makeCarnivalReactor(int team, int num)
    {
        MapleReactor old = getReactorByName(team + "" + num);
        if ((old != null) && (old.getState() < 5))
        {
            return false;
        }
        Point guardz = null;
        List<MapleReactor> react = getAllReactorsThreadsafe();
        for (Pair<Point, Integer> guard : this.nodes.getGuardians())
            if ((guard.right == team) || (guard.right == -1))
            {
                boolean found = false;
                for (MapleReactor r : react)
                {
                    if ((r.getTruePosition().x == guard.left.x) && (r.getTruePosition().y == guard.left.y) && (r.getState() < 5))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    guardz = guard.left;
                    break;
                }
            }
        boolean found;
        server.MapleCarnivalFactory.MCSkill skil;
        if (guardz != null)
        {
            MapleReactor my = new MapleReactor(MapleReactorFactory.getReactor(9980000 + team), 9980000 + team);
            my.setState((byte) 1);
            my.setName(team + "" + num);

            spawnReactorOnGroundBelow(my, guardz);
            skil = server.MapleCarnivalFactory.getInstance().getGuardian(num);
            for (MapleMonster mons : getAllMonstersThreadsafe())
            {
                if (mons.getCarnivalTeam() == team)
                {
                    skil.getSkill().applyEffect(null, mons, false);
                }
            }
        }
        return guardz != null;
    }

    public MapleReactor getReactorByName(String name)
    {
        (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().lock();
        try
        {
            for (MapleMapObject obj : (mapobjects.get(MapleMapObjectType.REACTOR)).values())
            {
                MapleReactor mr = (MapleReactor) obj;
                if (mr.getName().equalsIgnoreCase(name))
                {
                    return mr;
                }
            }
            return null;
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.REACTOR)).readLock().unlock();
        }
    }

    public void spawnReactorOnGroundBelow(MapleReactor mob, Point pos)
    {
        mob.setPosition(pos);
        mob.setCustom(true);
        spawnReactor(mob);
    }

    public void blockAllPortal()
    {
        for (MaplePortal p : this.portals.values())
        {
            p.setPortalState(false);
        }
    }

    public void setSquad(MapleSquadType s)
    {
        this.squad = s;
    }

    public int getChannel()
    {
        return this.channel;
    }

    public int getConsumeItemCoolTime()
    {
        return this.consumeItemCoolTime;
    }

    public void setConsumeItemCoolTime(int ciit)
    {
        this.consumeItemCoolTime = ciit;
    }

    public int getPermanentWeather()
    {
        return this.permanentWeather;
    }

    public void setPermanentWeather(int pw)
    {
        this.permanentWeather = pw;
    }

    public void checkStates(String chr)
    {
        if (!this.checkStates)
        {
            return;
        }
        MapleSquad sqd = getSquadByMap();
        EventManager em = getEMByMap();
        int size = getCharactersSize();
        if ((sqd != null) && (sqd.getStatus() == 2))
        {
            sqd.removeMember(chr);
            if (em != null)
            {
                if (sqd.getLeaderName().equalsIgnoreCase(chr))
                {
                    em.setProperty("leader", "false");
                }
                if ((chr.equals("")) || (size == 0))
                {
                    em.setProperty("state", "0");
                    em.setProperty("leader", "true");
                    cancelSquadSchedule(!chr.equals(""));
                    sqd.clear();
                    sqd.copy();
                }
            }
        }
        if ((em != null) && (em.getProperty("state") != null) && ((sqd == null) || (sqd.getStatus() == 2)) && (size == 0))
        {
            em.setProperty("state", "0");
            if (em.getProperty("leader") != null)
            {
                em.setProperty("leader", "true");
            }
        }
        if ((this.speedRunStart > 0L) && (size == 0))
        {
            endSpeedRun();
        }
    }

    public void setCheckStates(boolean b)
    {
        this.checkStates = b;
    }

    public Collection<MapleNodes.MapleNodeInfo> getNodes()
    {
        return this.nodes.getNodes();
    }

    public void setNodes(MapleNodes mn)
    {
        this.nodes = mn;
    }

    public MapleNodes.MapleNodeInfo getNode(int index)
    {
        return this.nodes.getNode(index);
    }

    public boolean isLastNode(int index)
    {
        return this.nodes.isLastNode(index);
    }

    public List<Rectangle> getAreas()
    {
        return this.nodes.getAreas();
    }

    public void changeEnvironment(String ms, int type)
    {
        broadcastMessage(MaplePacketCreator.environmentChange(ms, type));
    }

    public void toggleEnvironment(String ms)
    {
        if (this.environment.containsKey(ms))
        {
            moveEnvironment(ms, this.environment.get(ms) == 1 ? 2 : 1);
        }
        else
        {
            moveEnvironment(ms, 1);
        }
    }

    public void moveEnvironment(String ms, int type)
    {
        broadcastMessage(MaplePacketCreator.environmentMove(ms, type));
        this.environment.put(ms, type);
    }

    public Map<String, Integer> getEnvironment()
    {
        return this.environment;
    }

    public int getNumPlayersInArea(int index)
    {
        return getNumPlayersInRect(getArea(index));
    }

    public int getNumPlayersInRect(Rectangle rect)
    {
        int ret = 0;
        this.charactersLock.readLock().lock();
        try
        {

            for (MapleCharacter character : this.characters)
            {
                if (rect.contains(character.getTruePosition()))
                {
                    ret++;
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
        return ret;
    }

    public Rectangle getArea(int index)
    {
        return this.nodes.getArea(index);
    }

    public int getNumPlayersItemsInArea(int index)
    {
        return getNumPlayersItemsInRect(getArea(index));
    }

    public int getNumPlayersItemsInRect(Rectangle rect)
    {
        int ret = getNumPlayersInRect(rect);
        (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().lock();
        try
        {
            for (MapleMapObject mmo : (mapobjects.get(MapleMapObjectType.ITEM)).values())
            {
                if (rect.contains(mmo.getTruePosition()))
                {
                    ret++;
                }
            }
        }
        finally
        {
            (mapobjectlocks.get(MapleMapObjectType.ITEM)).readLock().unlock();
        }
        return ret;
    }

    public void broadcastGMMessage(MapleCharacter source, byte[] packet, boolean repeatToSource)
    {
        broadcastGMMessage(repeatToSource ? null : source, packet);
    }

    private void broadcastGMMessage(MapleCharacter source, byte[] packet)
    {
        this.charactersLock.readLock().lock();
        try
        {
            if (source == null)
            {
                for (MapleCharacter chr : this.characters)
                {
                    if (chr.isStaff())
                    {
                        chr.getClient().getSession().write(packet);
                    }
                }
            }
            else
            {
                for (MapleCharacter chr : this.characters)
                {
                    if ((chr != source) && (chr.getGMLevel() >= source.getGMLevel()))
                    {
                        chr.getClient().getSession().write(packet);
                    }
                }
            }
        }
        finally
        {
            this.charactersLock.readLock().unlock();
        }
    }

    public List<Pair<Integer, Integer>> getMobsToSpawn()
    {
        return this.nodes.getMobsToSpawn();
    }

    public List<Integer> getSkillIds()
    {
        return this.nodes.getSkillIds();
    }

    public boolean canSpawn(long now)
    {
        return (this.lastSpawnTime > 0L) && (this.lastSpawnTime + this.createMobInterval < now);
    }

    public boolean canHurt(long now)
    {
        if ((this.lastHurtTime > 0L) && (this.lastHurtTime + this.decHPInterval < now))
        {
            this.lastHurtTime = now;
            return true;
        }
        return false;
    }

    public void resetShammos(final MapleClient c)
    {
        killAllMonsters(true);
        broadcastMessage(MaplePacketCreator.serverNotice(5, "A player has moved too far from Shammos. Shammos is going back to the start."));
        server.Timer.EtcTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                if (c.getPlayer() != null)
                {
                    c.getPlayer().changeMap(MapleMap.this, MapleMap.this.getPortal(0));
                    if (MapleMap.this.getCharactersThreadsafe().size() > 1) MapScriptMethods.startScript_FirstUser(c, "shammos_Fenter");
                }
            }
        }, 500L);
    }

    public int getInstanceId()
    {
        return this.instanceid;
    }

    public void setInstanceId(int ii)
    {
        this.instanceid = ii;
    }

    public int getPartyBonusRate()
    {
        return this.partyBonusRate;
    }

    public void setPartyBonusRate(int ii)
    {
        this.partyBonusRate = ii;
    }

    public short getTop()
    {
        return this.top;
    }

    public void setTop(int ii)
    {
        this.top = ((short) ii);
    }

    public short getBottom()
    {
        return this.bottom;
    }

    public void setBottom(int ii)
    {
        this.bottom = ((short) ii);
    }

    public short getLeft()
    {
        return this.left;
    }

    public void setLeft(int ii)
    {
        this.left = ((short) ii);
    }

    public short getRight()
    {
        return this.right;
    }

    public void setRight(int ii)
    {
        this.right = ((short) ii);
    }

    public List<Pair<Point, Integer>> getGuardians()
    {
        return this.nodes.getGuardians();
    }

    public MapleNodes.DirectionInfo getDirectionInfo(int i)
    {
        return this.nodes.getDirection(i);
    }

    public void AutoNx(int jsNx, boolean isAutoPoints)
    {
        if (this.mapid != 741000000)
        {
            return;
        }
        for (MapleCharacter chr : this.characters)
        {
            if (chr != null)
            {
                if (chr.getClient().getLastPing() <= 0L)
                {
                    chr.getClient().sendPing();
                }
                if (!isAutoPoints)
                {


                    int givNx = chr.getLevel() / 10 + jsNx;
                    chr.modifyCSPoints(2, givNx);
                    chr.dropMessage(5, "[提示] 在线额外奖励获得 [" + givNx + "] 点抵用券.");
                }
            }
        }
    }

    public void AutoGain(int jsexp, int expRate)
    {
        if (this.mapid != 741000000)
        {
            return;
        }
        for (MapleCharacter chr : this.characters)
        {
            if ((chr == null) || (chr.getLevel() >= 250))
            {
                return;
            }
            int givExp = jsexp * chr.getLevel() + expRate;
            givExp *= 3;
            chr.gainExp(givExp, true, false, true);
            chr.dropMessage(5, "[提示] 在线额外奖励获得 [" + givExp + "] 点经验.");
        }
    }

    public boolean isMarketMap()
    {
        return (this.mapid >= 910000000) && (this.mapid <= 910000022);
    }

    public boolean isPvpMaps()
    {
        return (isPvpMap()) || (isPartyPvpMap()) || (isGuildPvpMap());
    }

    public boolean isPvpMap()
    {
        return this.mapid == 701000201;
    }

    public boolean isPartyPvpMap()
    {
        return this.mapid == 701000202;
    }

    public boolean isGuildPvpMap()
    {
        return this.mapid == 701000203;
    }

    public boolean isBossMap()
    {
        switch (this.mapid)
        {
            case 105100300:
            case 105100400:
            case 211070100:
            case 211070101:
            case 211070110:
            case 220080001:
            case 240040700:
            case 240060200:
            case 240060201:
            case 262031300:
            case 262031310:
            case 270050100:
            case 271040100:
            case 271040200:
            case 272030400:
            case 272030420:
            case 280030000:
            case 280030001:
            case 280030100:
            case 300030310:
            case 551030200:
            case 802000111:
            case 802000211:
            case 802000311:
            case 802000411:
            case 802000611:
            case 802000711:
            case 802000801:
            case 802000802:
            case 802000803:
            case 802000821:
            case 802000823:
                return true;
        }
        return false;
    }

    public void checkMoveMonster(Point from, boolean fly, MapleCharacter chr)
    {
        if ((this.maxRegularSpawn <= 2) || (this.monsterSpawn.isEmpty()) || (this.monsterRate <= 1.0D) || (chr == null))
        {
            return;
        }
        int check = (int) ((fly ? 70 : 60) / 100.0D * this.maxRegularSpawn);

        if (getMonstersInRange(from, 5000.0D).size() >= check)
        {
            for (MapleMapObject obj : getMonstersInRange(from, Double.POSITIVE_INFINITY))
            {
                MapleMonster mob = (MapleMonster) obj;
                killMonster(mob, chr, false, false, (byte) 1);
            }
        }
    }

    public void showMonster(final String monsterID, int x, int y, int hp)
    {
        final Point pos = new Point(x, y);
        final MapleMonster monster = MapleLifeFactory.getMonster(Integer.parseInt(monsterID));
        spawnMonsterOnGroundBelow(monster, pos);
    }

    public void spawnMonsterOnGroundBelow(MapleMonster mob, Point pos)
    {
        spawnMonster_sSack(mob, pos, -2);
    }

    public void spawnMonster_sSack(MapleMonster mob, Point pos, int spawnType)
    {
        mob.setPosition(calcPointBelow(new Point(pos.x, pos.y - 1)));
        spawnMonster(mob, spawnType);
    }

    public void spawnMonster(MapleMonster monster, int spawnType)
    {
        spawnMonster(monster, spawnType, false);
    }


    private interface DelayedPacketCreation
    {
        void sendPackets(MapleClient paramMapleClient);
    }

    private class ActivateItemReactor implements Runnable
    {
        private final MapleMapItem mapitem;
        private final MapleReactor reactor;
        private final MapleClient c;

        public ActivateItemReactor(MapleMapItem mapitem, MapleReactor reactor, MapleClient c)
        {
            this.mapitem = mapitem;
            this.reactor = reactor;
            this.c = c;
        }

        public void run()
        {
            if ((this.mapitem != null) && (this.mapitem == MapleMap.this.getMapObject(this.mapitem.getObjectId(), this.mapitem.getType())) && (!this.mapitem.isPickedUp()))
            {
                this.mapitem.expire(MapleMap.this);
                this.reactor.hitReactor(this.c);
                this.reactor.setTimerActive(false);

                if (this.reactor.getDelay() > 0)
                {
                    Timer.MapTimer.getInstance().schedule(new Runnable()
                    {
                        public void run()
                        {
                            MapleMap.ActivateItemReactor.this.reactor.forceHitReactor((byte) 0);
                        }
                    }, this.reactor.getDelay());
                }
            }
            else
            {
                this.reactor.setTimerActive(false);
            }
        }
    }
}