package server.maps;

public class ReactorDropEntry
{
    public final int itemId;
    public final int chance;
    public final int questid;
    public int assignedRangeStart;
    public int assignedRangeLength;

    public ReactorDropEntry(int itemId, int chance, int questid)
    {
        this.itemId = itemId;
        this.chance = chance;
        this.questid = questid;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\ReactorDropEntry.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */