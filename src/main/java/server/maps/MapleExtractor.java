package server.maps;

import client.MapleClient;

public class MapleExtractor extends MapleMapObject
{
    public final int owner;
    public final int timeLeft;
    public final int itemId;
    public final int fee;
    public final long startTime;
    public final String ownerName;

    public MapleExtractor(client.MapleCharacter owner, int itemId, int fee, int timeLeft)
    {
        this.owner = owner.getId();
        this.itemId = itemId;
        this.fee = fee;
        this.ownerName = owner.getName();
        this.startTime = System.currentTimeMillis();
        this.timeLeft = timeLeft;
        setPosition(owner.getPosition());
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.EXTRACTOR;
    }

    public void sendSpawnData(MapleClient client)
    {
        client.getSession().write(tools.MaplePacketCreator.makeExtractor(this.owner, this.ownerName, getTruePosition(), getTimeLeft(), this.itemId, this.fee));
    }

    public int getTimeLeft()
    {
        return this.timeLeft;
    }

    public void sendDestroyData(MapleClient client)
    {
        client.getSession().write(tools.MaplePacketCreator.removeExtractor(this.owner));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MapleExtractor.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */