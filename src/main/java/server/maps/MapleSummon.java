package server.maps;

import java.awt.Point;

import client.MapleCharacter;
import client.MapleClient;
import client.SkillFactory;
import client.anticheat.CheatingOffense;
import server.MapleStatEffect;

public final class MapleSummon extends AnimatedMapleMapObject
{
    private final int ownerid;
    private final int skillLevel;
    private final int ownerLevel;
    private final int skillId;
    private final SummonMovementType movementType;
    private MapleMap map;
    private int hp = 1;
    private boolean changedMap = false;
    private int lastSummonTickCount;
    private byte Summon_tickResetCount;
    private long Server_ClientSummonTickDiff;
    private long lastAttackTime;

    public MapleSummon(MapleCharacter owner, MapleStatEffect effect, Point pos, SummonMovementType movementType)
    {
        this(owner, effect.getSourceId(), effect.getLevel(), pos, movementType);
    }

    public MapleSummon(MapleCharacter owner, int sourceid, int level, Point pos, SummonMovementType movementType)
    {
        this.map = owner.getMap();
        this.ownerid = owner.getId();
        this.ownerLevel = owner.getLevel();
        this.skillId = sourceid;
        this.skillLevel = level;
        this.movementType = movementType;
        setPosition(pos);

        if (!is替身术())
        {
            this.lastSummonTickCount = 0;
            this.Summon_tickResetCount = 0;
            this.Server_ClientSummonTickDiff = 0L;
            this.lastAttackTime = 0L;
        }
    }

    public boolean is替身术()
    {
        switch (this.skillId)
        {
            case 3221014:
            case 4341006:
            case 33111003:
                return true;
        }
        return is天使召唤兽();
    }

    public boolean is天使召唤兽()
    {
        return constants.GameConstants.is天使技能(this.skillId);
    }

    public void updateMap(MapleMap map)
    {
        this.map = map;
    }

    public MapleCharacter getOwner()
    {
        return this.map.getCharacterById(this.ownerid);
    }

    public int getOwnerId()
    {
        return this.ownerid;
    }

    public int getOwnerLevel()
    {
        return this.ownerLevel;
    }

    public int getSkillId()
    {
        return this.skillId;
    }

    public int getSkillLevel()
    {
        return this.skillLevel;
    }

    public int getSummonHp()
    {
        return this.hp;
    }

    public void setSummonHp(int hp)
    {
        this.hp = hp;
    }

    public void addSummonHp(int delta)
    {
        this.hp += delta;
    }

    public boolean isMultiAttack()
    {
        return (this.skillId == 35111002) || (this.skillId == 35121003) || ((this.skillId != 33101008) && (this.skillId < 35000000)) || (this.skillId == 35111001) || (this.skillId == 35111009) || (this.skillId == 35111010);
    }

    public boolean is神箭幻影()
    {
        return this.skillId == 3221014;
    }

    public boolean is灵魂助力()
    {
        return this.skillId == 1301013;
    }

    public boolean is傀儡召唤()
    {
        return this.skillId == 4341006;
    }

    public boolean is机械磁场()
    {
        return this.skillId == 35111002;
    }

    public boolean is战法重生()
    {
        return this.skillId == 32111006;
    }

    public boolean isMultiSummon()
    {
        return (this.skillId == 5211014) || (this.skillId == 32111006) || (this.skillId == 33101008);
    }

    public boolean isSummon()
    {
        return (is天使召唤兽()) || (SkillFactory.getSkill(this.skillId).isSummonSkill());
    }

    public SummonMovementType getMovementType()
    {
        return this.movementType;
    }

    public byte getAttackType()
    {
        if (is天使召唤兽())
        {
            return 2;
        }
        switch (this.skillId)
        {
            case 13111024:
            case 13120007:
            case 35111002:
            case 35111005:
            case 35121010:
                return 0;
            case 3221014:
                return 1;
            case 1301013:
                return 2;
            case 23111008:
            case 23111009:
            case 23111010:
            case 35111001:
            case 35111009:
            case 35111010:
                return 3;
            case 35121009:
                return 5;
            case 35121003:
                return 6;
            case 4111007:
            case 4211007:
            case 14111010:
                return 7;
            case 5210015:
            case 5210016:
            case 5210017:
            case 5210018:
                return 9;
        }
        return 1;
    }

    public byte getRemoveStatus()
    {
        if (is天使召唤兽())
        {
            return 10;
        }
        switch (this.skillId)
        {
            case 2111010:
            case 5321003:
            case 33101008:
            case 35111002:
            case 35111005:
            case 35111011:
            case 35121009:
            case 35121010:
            case 35121011:
                return 5;
            case 23111008:
            case 23111009:
            case 23111010:
            case 35111001:
            case 35111009:
            case 35111010:
            case 35121003:
                return 10;
        }
        return 0;
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.SUMMON;
    }

    public void sendSpawnData(MapleClient client)
    {
        client.getSession().write(tools.packet.SummonPacket.spawnSummon(this, true));
    }

    public void sendDestroyData(MapleClient client)
    {
        client.getSession().write(tools.packet.SummonPacket.removeSummon(this, false));
    }

    public void CheckSummonAttackFrequency(MapleCharacter chr, int tickcount)
    {
        int tickdifference = tickcount - this.lastSummonTickCount;
        if (tickdifference < SkillFactory.getSummonData(this.skillId).delay)
        {
            chr.getCheatTracker().registerOffense(CheatingOffense.FAST_SUMMON_ATTACK);
        }
        long STime_TC = System.currentTimeMillis() - tickcount;
        long S_C_Difference = this.Server_ClientSummonTickDiff - STime_TC;
        if (S_C_Difference > 500L)
        {
            chr.getCheatTracker().registerOffense(CheatingOffense.FAST_SUMMON_ATTACK);
        }
        this.Summon_tickResetCount = ((byte) (this.Summon_tickResetCount + 1));
        if (this.Summon_tickResetCount > 4)
        {
            this.Summon_tickResetCount = 0;
            this.Server_ClientSummonTickDiff = STime_TC;
        }
        this.lastSummonTickCount = tickcount;
    }

    public void CheckPVPSummonAttackFrequency(MapleCharacter chr)
    {
        long tickdifference = System.currentTimeMillis() - this.lastAttackTime;
        if (tickdifference < SkillFactory.getSummonData(this.skillId).delay)
        {
            chr.getCheatTracker().registerOffense(CheatingOffense.FAST_SUMMON_ATTACK);
        }
        this.lastAttackTime = System.currentTimeMillis();
    }

    public boolean isChangedMap()
    {
        return this.changedMap;
    }

    public void setChangedMap(boolean cm)
    {
        this.changedMap = cm;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MapleSummon.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */