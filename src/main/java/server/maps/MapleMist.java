package server.maps;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.concurrent.ScheduledFuture;

import client.MapleCharacter;
import client.MapleClient;
import client.Skill;
import client.SkillFactory;
import server.MapleStatEffect;
import server.life.MapleMonster;
import server.life.MobSkill;
import tools.MaplePacketCreator;


public class MapleMist extends MapleMapObject
{
    private final int ownerId;
    private final int skilllevel;
    private final boolean isMobMist;
    private final Rectangle mistPosition;
    private int healCount;
    private boolean isHolyFountain;
    private ScheduledFuture<?> poisonSchedule = null;
    private ScheduledFuture<?> schedule = null;
    private int mistType;
    private int skillDelay;
    private boolean isRecoverMist;
    private boolean isPoisonMist;
    private MobSkill skill;
    private MapleStatEffect source;

    public MapleMist(Rectangle mistPosition, MapleMonster mob, MobSkill skill)
    {
        this.mistPosition = mistPosition;
        this.ownerId = mob.getId();
        this.skill = skill;
        this.skilllevel = skill.getSkillLevel();
        this.isMobMist = true;
        this.isPoisonMist = true;
        this.isRecoverMist = false;
        this.mistType = 0;
        this.skillDelay = 0;
    }

    public MapleMist(Rectangle mistPosition, MapleCharacter owner, MapleStatEffect source)
    {
        this.mistPosition = mistPosition;
        this.ownerId = owner.getId();
        this.source = source;
        this.skillDelay = 10;
        this.isMobMist = false;
        this.isPoisonMist = false;
        this.isRecoverMist = false;
        this.healCount = 0;
        this.isHolyFountain = false;
        this.skilllevel = owner.getTotalSkillLevel(SkillFactory.getSkill(source.getSourceId()));
        switch (source.getSourceId())
        {
            case 2311011:
                this.mistType = 0;
                this.healCount = source.getY();
                this.isHolyFountain = true;
                break;
            case 4121015:
                this.mistType = 0;
                break;
            case 4221006:
                this.mistType = 3;
                this.skillDelay = 3;
                this.isPoisonMist = true;
                break;
            case 32121006:
                this.mistType = 3;
                break;
            case 1076:
            case 2111003:
            case 12111005:
            case 14111006:
                this.mistType = 0;
                this.isPoisonMist = true;
                break;
            case 22161003:
                this.mistType = 0;
                this.isRecoverMist = true;
        }
    }

    public MapleMist(Rectangle mistPosition, MapleCharacter owner)
    {
        this.mistPosition = mistPosition;
        this.ownerId = owner.getId();
        this.source = new MapleStatEffect();
        this.source.setSourceId(2111003);
        this.skilllevel = 30;
        this.mistType = 0;
        this.isMobMist = false;
        this.isPoisonMist = false;
        this.skillDelay = 10;
    }

    public Point getPosition()
    {
        return this.mistPosition.getLocation();
    }

    public void setPosition(Point position)
    {
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.MIST;
    }

    public void sendSpawnData(MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.spawnMist(this));
    }

    public void sendDestroyData(MapleClient c)
    {
        c.getSession().write(MaplePacketCreator.removeMist(getObjectId(), false));
    }

    public Skill getSourceSkill()
    {
        return SkillFactory.getSkill(this.source.getSourceId());
    }

    public ScheduledFuture<?> getSchedule()
    {
        return this.schedule;
    }

    public void setSchedule(ScheduledFuture<?> s)
    {
        this.schedule = s;
    }

    public ScheduledFuture<?> getPoisonSchedule()
    {
        return this.poisonSchedule;
    }

    public void setPoisonSchedule(ScheduledFuture<?> s)
    {
        this.poisonSchedule = s;
    }

    public boolean isMobMist()
    {
        return this.isMobMist;
    }

    public boolean isPoisonMist()
    {
        return this.isPoisonMist;
    }

    public boolean isRecoverMist()
    {
        return this.isRecoverMist;
    }

    public int getHealCount()
    {
        return isHolyFountain() ? this.healCount : 0;
    }

    public boolean isHolyFountain()
    {
        return this.isHolyFountain;
    }

    public void setHealCount(int count)
    {
        this.healCount = count;
    }

    public int getMistType()
    {
        return this.mistType;
    }

    public int getSkillDelay()
    {
        return this.skillDelay;
    }

    public int getSkillLevel()
    {
        return this.skilllevel;
    }

    public int getOwnerId()
    {
        return this.ownerId;
    }

    public MobSkill getMobSkill()
    {
        return this.skill;
    }

    public Rectangle getBox()
    {
        return this.mistPosition;
    }

    public MapleStatEffect getSource()
    {
        return this.source;
    }

    public byte[] fakeSpawnData(int level)
    {
        return MaplePacketCreator.spawnMist(this);
    }

    public boolean makeChanceResult()
    {
        return this.source.makeChanceResult();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MapleMist.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */