package server.maps;

import java.util.LinkedList;
import java.util.List;

import client.MapleCharacter;
import client.MapleClient;
import handling.world.WorldBroadcastService;
import server.Timer;
import tools.MaplePacketCreator;


public class MapleTVEffect
{
    private static boolean active;
    private final MapleCharacter user;
    private final int type;
    MapleClient c;
    private List<String> message = new LinkedList<>();
    private MapleCharacter partner = null;

    public MapleTVEffect(MapleCharacter User, MapleCharacter Partner, List<String> Msg, int Type)
    {
        this.message = Msg;
        this.user = User;
        this.type = Type;
        this.partner = Partner;
    }

    public static boolean isActive()
    {
        return active;
    }

    private void setActive(boolean set)
    {
        active = set;
    }

    public static int getDelayTime(int type)
    {
        switch (type)
        {
            case 0:
            case 3:
                return 15;
            case 1:
            case 4:
                return 30;
            case 2:
            case 5:
                return 60;
        }
        return 0;
    }

    public void stratMapleTV()
    {
        broadCastTV(true);
    }

    private void broadCastTV(boolean isActive)
    {
        setActive(isActive);
        if (isActive)
        {
            int delay = getDelayTime(this.type);
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.enableTV());
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.sendTV(this.user, this.message, this.type <= 2 ? this.type : this.type - 3, this.partner, delay));

            Timer.WorldTimer.getInstance().schedule(new Runnable()
            {

                public void run()
                {
                    MapleTVEffect.this.broadCastTV(false);
                }
            }, delay * 1000);
        }
        else
        {
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.removeTV());
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\maps\MapleTVEffect.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */