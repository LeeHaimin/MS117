package server.maps;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import constants.BattleConstants;
import constants.GameConstants;
import database.DatabaseConnection;
import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;
import server.MaplePortal;
import server.Randomizer;
import server.life.AbstractLoadedMapleLife;
import server.life.MapleLifeFactory;
import server.life.MapleMonster;
import server.life.MapleNPC;
import tools.Pair;

public class MapleMapFactory
{
    private final MapleDataProvider source = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/Map.wz"));
    private final MapleData nameData = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/String.wz")).getData("Map.img");
    private final HashMap<Integer, MapleMap> maps = new HashMap<>();
    private final HashMap<Integer, MapleMap> instanceMap = new HashMap<>();
    private final ReentrantLock lock = new ReentrantLock();
    private int channel;

    public MapleMapFactory(int channel)
    {
        this.channel = channel;
    }

    public MapleMap getMap(int mapid, boolean respawns, boolean npcs)
    {
        return getMap(mapid, respawns, npcs, true);
    }

    public MapleMap getMap(int mapid, boolean respawns, boolean npcs, boolean reactors)
    {
        Integer omapid = mapid;
        MapleMap map = this.maps.get(omapid);
        if (map == null)
        {
            this.lock.lock();
            try
            {
                map = this.maps.get(omapid);
                if (map != null)
                {
                    return map;
                }
                MapleData mapData = null;
                try
                {
                    mapData = this.source.getData(getMapName(mapid));
                }
                catch (Exception e)
                {
                    return null;
                }
                if (mapData == null)
                {
                    return null;
                }
                int linkMapId = -1;
                MapleData link = mapData.getChildByPath("info/link");
                if (link != null)
                {
                    linkMapId = MapleDataTool.getIntConvert("info/link", mapData);
                    mapData = this.source.getData(getMapName(linkMapId));
                }

                float monsterRate = 0.0F;
                if (respawns)
                {
                    MapleData mobRate = mapData.getChildByPath("info/mobRate");
                    if (mobRate != null)
                    {
                        monsterRate = (Float) mobRate.getData();
                    }
                }
                map = new MapleMap(mapid, this.channel, MapleDataTool.getInt("info/returnMap", mapData), monsterRate);

                loadPortals(map, mapData.getChildByPath("portal"));
                map.setTop(MapleDataTool.getInt(mapData.getChildByPath("info/VRTop"), 0));
                map.setLeft(MapleDataTool.getInt(mapData.getChildByPath("info/VRLeft"), 0));
                map.setBottom(MapleDataTool.getInt(mapData.getChildByPath("info/VRBottom"), 0));
                map.setRight(MapleDataTool.getInt(mapData.getChildByPath("info/VRRight"), 0));
                List<MapleFoothold> allFootholds = new LinkedList<>();
                Point lBound = new Point();
                Point uBound = new Point();


                for (MapleData footRoot : mapData.getChildByPath("foothold"))
                {
                    for (MapleData footCat : footRoot)
                        for (MapleData footHold : footCat)
                        {
                            MapleFoothold fh = new MapleFoothold(new Point(MapleDataTool.getInt(footHold.getChildByPath("x1"), 0), MapleDataTool.getInt(footHold.getChildByPath("y1"), 0)),
                                    new Point(MapleDataTool.getInt(footHold.getChildByPath("x2"), 0), MapleDataTool.getInt(footHold.getChildByPath("y2"), 0)), Integer.parseInt(footHold.getName()));
                            fh.setPrev((short) MapleDataTool.getInt(footHold.getChildByPath("prev"), 0));
                            fh.setNext((short) MapleDataTool.getInt(footHold.getChildByPath("next"), 0));

                            if (fh.getX1() < lBound.x)
                            {
                                lBound.x = fh.getX1();
                            }
                            if (fh.getX2() > uBound.x)
                            {
                                uBound.x = fh.getX2();
                            }
                            if (fh.getY1() < lBound.y)
                            {
                                lBound.y = fh.getY1();
                            }
                            if (fh.getY2() > uBound.y)
                            {
                                uBound.y = fh.getY2();
                            }
                            allFootholds.add(fh);
                        }
                }
                MapleData footRoot;
                MapleFootholdTree fTree = new MapleFootholdTree(lBound, uBound);
                for (MapleFoothold foothold : allFootholds)
                {
                    fTree.insert(foothold);
                }
                map.setFootholds(fTree);
                if (map.getTop() == 0)
                {
                    map.setTop(lBound.y);
                }
                if (map.getBottom() == 0)
                {
                    map.setBottom(uBound.y);
                }
                if (map.getLeft() == 0)
                {
                    map.setLeft(lBound.x);
                }
                if (map.getRight() == 0)
                {
                    map.setRight(uBound.x);
                }
                int bossid = -1;
                String msg = null;
                if (mapData.getChildByPath("info/timeMob") != null)
                {
                    bossid = MapleDataTool.getInt(mapData.getChildByPath("info/timeMob/id"), 0);
                    msg = MapleDataTool.getString(mapData.getChildByPath("info/timeMob/message"), null);
                }
                try
                {
                    PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM spawns WHERE mid = ?");
                    ps.setInt(1, omapid);
                    ResultSet rs = ps.executeQuery();
                    while (rs.next())
                    {
                        int sqlid = rs.getInt("idd");
                        int sqlf = rs.getInt("f");
                        boolean sqlhide = false;
                        String sqltype = rs.getString("type");
                        int sqlfh = rs.getInt("fh");
                        int sqlcy = rs.getInt("cy");
                        int sqlrx0 = rs.getInt("rx0");
                        int sqlrx1 = rs.getInt("rx1");
                        int sqlx = rs.getInt("x");
                        int sqly = rs.getInt("y");
                        int sqlmobTime = rs.getInt("mobtime");
                        AbstractLoadedMapleLife sqlmyLife = loadLife(sqlid, sqlf, sqlhide, sqlfh, sqlcy, sqlrx0, sqlrx1, sqlx, sqly, sqltype);
                        if (sqltype.equals("n"))
                        {
                            map.addMapObject(sqlmyLife);
                        }
                        else if (sqltype.equals("m"))
                        {
                            MapleMonster monster = (MapleMonster) sqlmyLife;
                            map.addMonsterSpawn(monster, sqlmobTime, (byte) -1, null);
                        }
                    }
                }
                catch (SQLException e)
                {
                    System.out.println("读取SQL刷Npc和刷新怪物出错.");
                }


                List<Point> herbRocks = new ArrayList<>();
                int lowestLevel = 200;
                int highestLevel = 0;


                for (MapleData life : mapData.getChildByPath("life"))
                {
                    String type = MapleDataTool.getString(life.getChildByPath("type"));
                    String limited = MapleDataTool.getString("limitedname", life, "");
                    if (((npcs) || (!type.equals("n"))) && (!limited.equals("Stage0")))
                    {
                        AbstractLoadedMapleLife myLife = loadLife(life, MapleDataTool.getString(life.getChildByPath("id")), type);

                        if (((myLife instanceof MapleMonster)) && (!BattleConstants.isBattleMap(mapid)) && (!GameConstants.isNoSpawn(mapid)))
                        {
                            MapleMonster mob = (MapleMonster) myLife;

                            herbRocks.add(map.addMonsterSpawn(mob, MapleDataTool.getInt("mobTime", life, 0), (byte) MapleDataTool.getInt("team", life, -1), mob.getId() == bossid ? msg : null).getPosition());
                            if ((mob.getStats().getLevel() > highestLevel) && (!mob.getStats().isBoss()))
                            {
                                highestLevel = mob.getStats().getLevel();
                            }
                            if ((mob.getStats().getLevel() < lowestLevel) && (!mob.getStats().isBoss()))
                            {
                                lowestLevel = mob.getStats().getLevel();
                            }
                        }
                        else if ((myLife instanceof MapleNPC))
                        {
                            map.addMapObject(myLife);
                        }
                    }
                }
                MapleData life;
                addAreaBossSpawn(map);
                map.setCreateMobInterval((short) MapleDataTool.getInt(mapData.getChildByPath("info/createMobInterval"), 9000));
                map.setFixedMob(MapleDataTool.getInt(mapData.getChildByPath("info/fixedMobCapacity"), 0));
                map.setPartyBonusRate(GameConstants.getPartyPlay(mapid, MapleDataTool.getInt(mapData.getChildByPath("info/partyBonusR"), 0)));
                map.loadMonsterRate(true);
                map.setNodes(loadNodes(mapid, mapData));
                map.setSpawnPoints(herbRocks);


                if ((reactors) && (mapData.getChildByPath("reactor") != null) && (!BattleConstants.isBattleMap(mapid)))
                {
                    for (MapleData reactor : mapData.getChildByPath("reactor"))
                    {
                        String id = MapleDataTool.getString(reactor.getChildByPath("id"));
                        if (id != null)
                        {
                            map.spawnReactor(loadReactor(reactor, id, (byte) MapleDataTool.getInt(reactor.getChildByPath("f"), 0)));
                        }
                    }
                }
                map.setFirstUserEnter(MapleDataTool.getString(mapData.getChildByPath("info/onFirstUserEnter"), ""));
                map.setUserEnter(mapid == 180000001 ? "jail" : MapleDataTool.getString(mapData.getChildByPath("info/onUserEnter"), ""));
                if ((reactors) && (herbRocks.size() > 0) && (highestLevel >= 30) && (map.getFirstUserEnter().equals("")) && (map.getUserEnter().equals("")))
                {
                    List<Integer> allowedSpawn = new ArrayList(24);
                    allowedSpawn.add(100011);
                    allowedSpawn.add(200011);
                    if (highestLevel >= 100)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            for (int x = 0; x < 4; x++)
                            {
                                allowedSpawn.add(100000 + i);
                                allowedSpawn.add(200000 + i);
                            }
                        }
                    }
                    else
                    {
                        for (int i = lowestLevel % 10 > highestLevel % 10 ? 0 : lowestLevel % 10; i < highestLevel % 10; i++)
                        {
                            for (int x = 0; x < 4; x++)
                            {
                                allowedSpawn.add(100000 + i);
                                allowedSpawn.add(200000 + i);
                            }
                        }
                    }
                    int numSpawn = Randomizer.nextInt(allowedSpawn.size()) / 6;
                    for (int i = 0; (i < numSpawn) && (!herbRocks.isEmpty()); i++)
                    {
                        int idd = allowedSpawn.get(Randomizer.nextInt(allowedSpawn.size()));
                        int theSpawn = Randomizer.nextInt(herbRocks.size());
                        MapleReactor myReactor = new MapleReactor(MapleReactorFactory.getReactor(idd), idd);
                        myReactor.setPosition(herbRocks.get(theSpawn));
                        myReactor.setDelay(idd % 100 == 11 ? 60000 : 5000);
                        map.spawnReactor(myReactor);
                        herbRocks.remove(theSpawn);
                    }
                }


                try
                {
                    map.setMapName(MapleDataTool.getString("mapName", this.nameData.getChildByPath(getMapStringName(linkMapId > 0 ? linkMapId : omapid)), ""));
                    map.setStreetName(MapleDataTool.getString("streetName", this.nameData.getChildByPath(getMapStringName(linkMapId > 0 ? linkMapId : omapid)), ""));
                }
                catch (Exception e)
                {
                    map.setMapName("");
                    map.setStreetName("");
                }
                if ((map.getMapName().length() <= 1) && (map.getStreetName().length() <= 1))
                {
                    tools.FileoutputUtil.log("地图名字错误.txt", "地图ID: " + mapid, true);
                }


                map.setClock(mapData.getChildByPath("clock") != null);
                map.setEverlast(MapleDataTool.getInt(mapData.getChildByPath("info/everlast"), 0) > 0);
                map.setTown(MapleDataTool.getInt(mapData.getChildByPath("info/town"), 0) > 0);
                map.setSoaring(MapleDataTool.getInt(mapData.getChildByPath("info/needSkillForFly"), 0) > 0);
                map.setPersonalShop(MapleDataTool.getInt(mapData.getChildByPath("info/personalShop"), 0) > 0);
                map.setForceMove(MapleDataTool.getInt(mapData.getChildByPath("info/lvForceMove"), 0));
                map.setHPDec(MapleDataTool.getInt(mapData.getChildByPath("info/decHP"), 0));
                map.setHPDecInterval(MapleDataTool.getInt(mapData.getChildByPath("info/decHPInterval"), 10000));
                map.setHPDecProtect(MapleDataTool.getInt(mapData.getChildByPath("info/protectItem"), 0));
                map.setForcedReturnMap(mapid == 0 ? 999999999 : MapleDataTool.getInt(mapData.getChildByPath("info/forcedReturn"), 999999999));
                map.setTimeLimit(MapleDataTool.getInt(mapData.getChildByPath("info/timeLimit"), -1));
                map.setFieldLimit(MapleDataTool.getInt(mapData.getChildByPath("info/fieldLimit"), 0));
                map.setFieldType(MapleDataTool.getInt(mapData.getChildByPath("info/fieldType"), 0));
                map.setRecoveryRate(MapleDataTool.getFloat(mapData.getChildByPath("info/recovery"), 1.0F));
                map.setFixedMob(MapleDataTool.getInt(mapData.getChildByPath("info/fixedMobCapacity"), 0));
                map.setPartyBonusRate(GameConstants.getPartyPlay(mapid, MapleDataTool.getInt(mapData.getChildByPath("info/partyBonusR"), 0)));
                map.setConsumeItemCoolTime(MapleDataTool.getInt(mapData.getChildByPath("info/consumeItemCoolTime"), 0));
                this.maps.put(omapid, map);
            }
            finally
            {
                this.lock.unlock();
            }
        }
        return map;
    }

    private String getMapName(int mapid)
    {
        String mapName = tools.StringUtil.getLeftPaddedStr(Integer.toString(mapid), '0', 9);
        String builder = "Map/Map" + mapid / 100000000 + "/" + mapName + ".img";
        mapName = builder;
        return mapName;
    }

    private void loadPortals(MapleMap map, MapleData port)
    {
        if (port == null)
        {
            return;
        }
        int nextDoorPortal = 128;
        for (MapleData portal : port.getChildren())
        {
            MaplePortal myPortal = new MaplePortal(MapleDataTool.getInt(portal.getChildByPath("pt")));
            myPortal.setName(MapleDataTool.getString(portal.getChildByPath("pn")));
            myPortal.setTarget(MapleDataTool.getString(portal.getChildByPath("tn")));
            myPortal.setTargetMapId(MapleDataTool.getInt(portal.getChildByPath("tm")));
            myPortal.setPosition(new Point(MapleDataTool.getInt(portal.getChildByPath("x")), MapleDataTool.getInt(portal.getChildByPath("y"))));
            String script = MapleDataTool.getString("script", portal, null);
            if ((script != null) && (script.equals("")))
            {
                script = null;
            }
            myPortal.setScriptName(script);

            if (myPortal.getType() == MaplePortal.DOOR_PORTAL)
            {
                myPortal.setId(nextDoorPortal);
                nextDoorPortal++;
            }
            else
            {
                myPortal.setId(Integer.parseInt(portal.getName()));
            }
            map.addPortal(myPortal);
        }
    }

    private AbstractLoadedMapleLife loadLife(int id, int f, boolean hide, int fh, int cy, int rx0, int rx1, int x, int y, String type)
    {
        AbstractLoadedMapleLife myLife = MapleLifeFactory.getLife(id, type);
        myLife.setCy(cy);
        myLife.setF(f);
        myLife.setFh(fh);
        myLife.setRx0(rx0);
        myLife.setRx1(rx1);
        myLife.setPosition(new Point(x, y));
        myLife.setHide(hide);
        return myLife;
    }

    private AbstractLoadedMapleLife loadLife(MapleData life, String id, String type)
    {
        AbstractLoadedMapleLife myLife = MapleLifeFactory.getLife(Integer.parseInt(id), type);
        if (myLife == null)
        {
            return null;
        }
        myLife.setCy(MapleDataTool.getInt(life.getChildByPath("cy")));
        MapleData dF = life.getChildByPath("f");
        if (dF != null)
        {
            myLife.setF(MapleDataTool.getInt(dF));
        }
        myLife.setFh(MapleDataTool.getInt(life.getChildByPath("fh")));
        myLife.setRx0(MapleDataTool.getInt(life.getChildByPath("rx0")));
        myLife.setRx1(MapleDataTool.getInt(life.getChildByPath("rx1")));
        myLife.setPosition(new Point(MapleDataTool.getIntConvert(life.getChildByPath("x")), MapleDataTool.getIntConvert(life.getChildByPath("y"))));

        if ((MapleDataTool.getInt("hide", life, 0) == 1) && ((myLife instanceof MapleNPC)))
        {
            myLife.setHide(true);
        }
        return myLife;
    }

    private void addAreaBossSpawn(MapleMap map)
    {
        int monsterid = -1;
        int mobtime = -1;
        String msg = null;
        boolean shouldSpawn = true;
        boolean sendWorldMsg = false;
        Point pos1 = null;
        Point pos2 = null;
        Point pos3 = null;

        switch (map.getId())
        {
            case 104010200:
                mobtime = 1200;
                monsterid = 2220000;
                msg = "天气凉快了就会出现红蜗牛王。";
                pos1 = new Point(189, 2);
                pos2 = new Point(478, 250);
                pos3 = new Point(611, 489);
                break;
            case 102020500:
                mobtime = 1200;
                monsterid = 3220000;
                msg = "树妖王出现了。";
                pos1 = new Point(1121, 2130);
                pos2 = new Point(483, 2171);
                pos3 = new Point(1474, 1706);
                break;
            case 100020101:
                mobtime = 1200;
                monsterid = 6130101;
                msg = "什么地方出现了巨大的蘑菇。";
                pos1 = new Point(65225, 201);
                pos2 = new Point(64633, 197);
                pos3 = new Point(64968, 196);
                break;
            case 100020301:
                mobtime = 1200;
                monsterid = 8220007;
                msg = "什么地方出现了巨大的蓝色蘑菇。";
                pos1 = new Point(65348, 64879);
                pos2 = new Point(625, 64876);
                pos3 = new Point(508, 64888);
                break;
            case 100020401:
                mobtime = 1200;
                monsterid = 6300005;
                msg = "什么地方出现了笼罩着阴暗气息的巨大蘑菇。";
                pos1 = new Point(65406, 64763);
                pos2 = new Point(504, 64776);
                pos3 = new Point(608, 64895);
                break;
            case 120030500:
                mobtime = 1200;
                monsterid = 5220001;
                msg = "从沙滩里慢慢的走出了一只巨居蟹。";
                pos1 = new Point(65181, 179);
                pos2 = new Point(64253, -113);
                pos3 = new Point(64965, 64943);
                break;
            case 250010304:
                mobtime = 2100;
                monsterid = 7220000;
                msg = "随着微弱的口哨声，肯德熊出现了。";
                pos1 = new Point(65326, 33);
                pos2 = new Point(65302, 393);
                pos3 = new Point(64882, 33);
                break;
            case 200010300:
                mobtime = 1200;
                monsterid = 8220000;
                msg = "艾利杰出现了。";
                pos1 = new Point(665, 83);
                pos2 = new Point(672, 65319);
                pos3 = new Point(-123, 65319);
                break;
            case 250010503:
                mobtime = 1800;
                monsterid = 7220002;
                msg = "周边的妖气慢慢浓厚，可以听到诡异的猫叫声。";
                pos1 = new Point(65233, 543);
                pos2 = new Point(227, 543);
                pos3 = new Point(719, 543);
                break;
            case 222010310:
                mobtime = 2700;
                monsterid = 7220001;
                msg = "在阴暗的月光中随着九尾狐的哭声，可以感受到它阴气。";
                pos1 = new Point(65367, 65389);
                pos2 = new Point(65019, 93);
                pos3 = new Point(247, 93);
                break;
            case 103030400:
                mobtime = 1800;
                monsterid = 6220000;
                msg = "从沼泽出现了巨大的多尔。";
                pos1 = new Point(64705, 109);
                pos2 = new Point(1525, -75);
                pos3 = new Point(65025, 107);
                break;
            case 101040300:
                mobtime = 1800;
                monsterid = 5220002;
                msg = "蓝雾慢慢散去，浮士德慢慢的显现了出来。";
                pos1 = new Point(600, 64936);
                pos2 = new Point(600, 64736);
                pos3 = new Point(600, 65236);
                break;
            case 220050200:
                mobtime = 1500;
                monsterid = 5220003;
                msg = "嘀嗒嘀嗒! 随着规则的指针声出现了提莫。";
                pos1 = new Point(65069, 1032);
                pos2 = new Point(532, 1032);
                pos3 = new Point(-47, 1032);
                break;
            case 221040301:
                mobtime = 2400;
                monsterid = 6220001;
                msg = "厚重的机器运作声，朱诺出现了!";
                pos1 = new Point(61402, 416);
                pos2 = new Point(61253, 776);
                pos3 = new Point(62244, 776);
                break;
            case 240040401:
                mobtime = 7200;
                monsterid = 8220003;
                msg = "大海兽出现了。";
                pos1 = new Point(-15, 2481);
                pos2 = new Point(127, 1634);
                pos3 = new Point(159, 1142);
                break;
            case 260010201:
                mobtime = 3600;
                monsterid = 3220001;
                msg = "从沙尘中可以看到大宇的身影。";
                pos1 = new Point(65321, 275);
                pos2 = new Point(298, 275);
                pos3 = new Point(592, 275);
                break;
            case 251010102:
                mobtime = 3600;
                monsterid = 5220004;
                msg = "大王蜈蚣出现了。";
                pos1 = new Point(-41, 124);
                pos2 = new Point(65363, 126);
                pos3 = new Point(79, 118);
                break;
            case 261030000:
                mobtime = 2700;
                monsterid = 8220002;
                msg = "吉米拉出现了。";
                pos1 = new Point(64442, 65131);
                pos2 = new Point(64764, -116);
                pos3 = new Point(-108, 181);
                break;
            case 230020100:
                mobtime = 2700;
                monsterid = 4220000;
                msg = "在海草中间，出现了奇怪的蛤蚌。";
                pos1 = new Point(65245, -20);
                pos2 = new Point(65264, 65036);
                pos3 = new Point(65074, 640);
                break;
            case 103020320:
                mobtime = 1800;
                monsterid = 5090000;
                msg = "在地铁的阴影中出现了什么东西。";
                pos1 = new Point(79, 174);
                pos2 = new Point(65313, 296);
                pos3 = new Point(80, 275);
                break;
            case 103020420:
                mobtime = 1800;
                monsterid = 5090000;
                msg = "在地铁的阴影中出现了什么东西。";
                pos1 = new Point(2241, 301);
                pos2 = new Point(1990, 301);
                pos3 = new Point(1684, 307);
                break;
            case 261020300:
                mobtime = 2700;
                monsterid = 7090000;
                msg = "自动警备系统出现了。";
                pos1 = new Point(312, 157);
                pos2 = new Point(539, 136);
                pos3 = new Point(760, 141);
                break;
            case 261020401:
                mobtime = 2700;
                monsterid = 8090000;
                msg = "迪特和罗伊出现了。";
                pos1 = new Point(65273, 155);
                pos2 = new Point(65100, 122);
                pos3 = new Point(22, 144);
                break;
            case 250020300:
                mobtime = 2700;
                monsterid = 5090001;
                msg = "仙人玩偶出现了。";
                pos1 = new Point(1208, 27);
                pos2 = new Point(1654, 40);
                pos3 = new Point(927, 65034);
                break;
            case 211050000:
                mobtime = 2700;
                monsterid = 6090001;
                msg = "被束缚在冰里的魔女睁开了眼睛。";
                pos1 = new Point(65303, 65105);
                pos2 = new Point(65166, 65110);
                pos3 = new Point(65010, 65116);
                break;
            case 261010003:
                mobtime = 2700;
                monsterid = 6090004;
                msg = "陆陆猫出现了。";
                pos1 = new Point(64675, 301);
                pos2 = new Point(64833, 301);
                pos3 = new Point(65110, 287);
                break;
            case 222010300:
                mobtime = 2700;
                monsterid = 6090003;
                msg = "书生鬼出现了。";
                pos1 = new Point(1300, 65136);
                pos2 = new Point(1100, -100);
                pos3 = new Point(1100, 100);
                break;
            case 251010101:
                mobtime = 2700;
                monsterid = 6090002;
                msg = "竹林里出现了一个来历不明的青竹武士，只要打碎小竹片，就可让青竹武士大发雷霆而葬失自制力，并将他打倒。";
                pos1 = new Point(-15, 65087);
                pos2 = new Point(-114, 65094);
                pos3 = new Point(65281, 65090);
                break;
            case 211041400:
                mobtime = 2700;
                monsterid = 6090000;
                msg = "黑山老妖出现了！";
                pos1 = new Point(1672, 82);
                pos2 = new Point(2071, 10);
                pos3 = new Point(1417, 57);
                break;
            case 105030500:
                mobtime = 2700;
                monsterid = 8130100;
                msg = "蝙蝠怪出现了。";
                pos1 = new Point(1275, 65137);
                pos2 = new Point(1254, 65124);
                pos3 = new Point(1058, 65109);
                break;
            case 105020400:
                mobtime = 2700;
                monsterid = 8220008;
                msg = "出现了一个奇怪的商店。";
                pos1 = new Point(65373, 82);
                pos2 = new Point(958, 107);
                pos3 = new Point(706, 65330);
                break;
            case 211040101:
                mobtime = 3600;
                monsterid = 8220001;
                msg = "驮狼雪人出现了。";
                pos1 = new Point(485, 244);
                pos2 = new Point(-60, 249);
                pos3 = new Point(208, 255);
                break;
            case 209000000:
                mobtime = 300;
                monsterid = 9500317;
                msg = "小雪人出现了。";
                pos1 = new Point(-115, 154);
                pos2 = new Point(-115, 154);
                pos3 = new Point(-115, 154);
                break;
            case 677000001:
                mobtime = 60;
                monsterid = 9400612;
                msg = "牛魔王出现了。";
                pos1 = new Point(99, 60);
                pos2 = new Point(99, 60);
                pos3 = new Point(99, 60);
                break;
            case 677000003:
                mobtime = 60;
                monsterid = 9400610;
                msg = "黑暗独角兽出现了。";
                pos1 = new Point(6, 35);
                pos2 = new Point(6, 35);
                pos3 = new Point(6, 35);
                break;
            case 677000005:
                mobtime = 60;
                monsterid = 9400609;
                msg = "印第安老斑鸠出现了。";
                pos1 = new Point(65259, 78);
                pos2 = new Point(547, 86);
                pos3 = new Point(65189, 80);
                break;
            case 677000007:
                mobtime = 60;
                monsterid = 9400611;
                msg = "雪之猫女出现了。";
                pos1 = new Point(117, 73);
                pos2 = new Point(117, 73);
                pos3 = new Point(117, 73);
                break;
            case 677000009:
                mobtime = 60;
                monsterid = 9400613;
                msg = "沃勒福出现了。";
                pos1 = new Point(85, 66);
                pos2 = new Point(85, 66);
                pos3 = new Point(85, 66);
                break;
            case 931000500:
            case 931000502:
                mobtime = 3600;
                monsterid = 9304005;
                msg = "美洲豹栖息地出现 剑齿豹 ，喜欢此坐骑的弩豹游侠职业可以前往抓捕。";
                pos1 = new Point(64664, 65204);
                pos2 = new Point(409, 64964);
                pos3 = new Point(65405, 0);
                shouldSpawn = false;
                sendWorldMsg = true;
                break;
            case 931000501:
            case 931000503:
                mobtime = 7200;
                monsterid = 9304006;
                msg = "美洲豹栖息地出现 雪豹 ，喜欢此坐骑的弩豹游侠职业可以前往抓捕。";
                pos1 = new Point(64664, 65204);
                pos2 = new Point(409, 64964);
                pos3 = new Point(65405, 0);
                shouldSpawn = false;
                sendWorldMsg = true;
        }

        if (monsterid > 0)
        {
            map.addAreaMonsterSpawn(MapleLifeFactory.getMonster(monsterid), pos1, pos2, pos3, mobtime, msg, shouldSpawn, sendWorldMsg);
        }
    }

    private MapleNodes loadNodes(int mapid, MapleData mapData)
    {
        MapleNodes nodeInfo = new MapleNodes(mapid);
        MapleData node;
        if (mapData.getChildByPath("nodeInfo") != null)
        {
            for (MapleData mapleData : mapData.getChildByPath("nodeInfo"))
            {
                node = mapleData;
                try
                {
                    if (node.getName().equals("start"))
                    {
                        nodeInfo.setNodeStart(MapleDataTool.getInt(node, 0));
                    }
                    else
                    {
                        List<Integer> edges = new ArrayList<>();
                        if (node.getChildByPath("edge") != null)
                        {
                            for (MapleData edge : node.getChildByPath("edge"))
                            {
                                edges.add(MapleDataTool.getInt(edge, -1));
                            }
                        }


                        MapleNodes.MapleNodeInfo mni = new MapleNodes.MapleNodeInfo(Integer.parseInt(node.getName()), MapleDataTool.getIntConvert("key", node, 0), MapleDataTool.getIntConvert("x",
                                node, 0), MapleDataTool.getIntConvert("y", node, 0), MapleDataTool.getIntConvert("attr", node, 0), edges);
                        nodeInfo.addNode(mni);
                    }
                }
                catch (NumberFormatException ignored)
                {
                }
            }
            nodeInfo.sortNodes();
        }
        int speed;
        for (int i = 1; i <= 7; i++)
        {
            if ((mapData.getChildByPath(String.valueOf(i)) != null) && (mapData.getChildByPath(i + "/obj") != null))
            {
                for (MapleData mapleData : mapData.getChildByPath(i + "/obj"))
                {
                    if ((mapleData.getChildByPath("SN_count") != null) && (mapleData.getChildByPath("speed") != null))
                    {
                        int sn_count = MapleDataTool.getIntConvert("SN_count", mapleData, 0);
                        String name = MapleDataTool.getString("name", mapleData, "");
                        speed = MapleDataTool.getIntConvert("speed", mapleData, 0);
                        if ((sn_count > 0) && (speed > 0) && (!name.equals("")))
                        {

                            List<Integer> SN = new ArrayList<>();
                            for (int x = 0; x < sn_count; x++)
                            {
                                SN.add(MapleDataTool.getIntConvert("SN" + x, mapleData, 0));
                            }


                            MapleNodes.MaplePlatform mni = new MapleNodes.MaplePlatform(name, MapleDataTool.getIntConvert("start", mapleData, 2), speed, MapleDataTool.getIntConvert("x1", mapleData,
                                    0), MapleDataTool.getIntConvert("y1", mapleData, 0), MapleDataTool.getIntConvert("x2", mapleData, 0), MapleDataTool.getIntConvert("y2", mapleData, 0),
                                    MapleDataTool.getIntConvert("r", mapleData, 0), SN);
                            nodeInfo.addPlatform(mni);
                        }
                    }
                    else if (mapleData.getChildByPath("tags") != null)
                    {
                        String name = MapleDataTool.getString("tags", mapleData, "");
                        nodeInfo.addFlag(new Pair(name, name.endsWith("3") ? 1 : 0));
                    }
                }
            }
        }

        if (mapData.getChildByPath("area") != null)
        {

            for (MapleData area : mapData.getChildByPath("area"))
            {
                int x1 = MapleDataTool.getInt(area.getChildByPath("x1"));
                int y1 = MapleDataTool.getInt(area.getChildByPath("y1"));
                int x2 = MapleDataTool.getInt(area.getChildByPath("x2"));
                int y2 = MapleDataTool.getInt(area.getChildByPath("y2"));
                Rectangle mapArea = new Rectangle(x1, y1, x2 - x1, y2 - y1);
                nodeInfo.addMapleArea(mapArea);
            }
        }
        int y1;
        Rectangle mapArea;
        if (mapData.getChildByPath("CaptureTheFlag") != null)
        {
            MapleData mc = mapData.getChildByPath("CaptureTheFlag");
            for (MapleData area : mc)
            {
                nodeInfo.addGuardianSpawn(new Point(MapleDataTool.getInt(area.getChildByPath("FlagPositionX")), MapleDataTool.getInt(area.getChildByPath("FlagPositionY"))),
                        area.getName().startsWith("Red") ? 0 : 1);
            }
        }
        if (mapData.getChildByPath("directionInfo") != null)
        {
            MapleData mc = mapData.getChildByPath("directionInfo");
            for (MapleData area : mc)
            {
                MapleNodes.DirectionInfo di = new MapleNodes.DirectionInfo(Integer.parseInt(area.getName()), MapleDataTool.getInt("x", area, 0), MapleDataTool.getInt("y", area, 0),
                        MapleDataTool.getInt("forcedInput", area, 0) > 0);
                if (area.getChildByPath("EventQ") != null)
                {
                    for (MapleData event : area.getChildByPath("EventQ"))
                    {
                        di.eventQ.add(MapleDataTool.getString(event));
                    }
                }
                else
                {
                    System.out.println("[loadNodes] 地图: " + mapid + " 没有找到EventQ.");
                }
                nodeInfo.addDirection(Integer.parseInt(area.getName()), di);
            }
        }
        if (mapData.getChildByPath("monsterCarnival") != null)
        {
            MapleData mc = mapData.getChildByPath("monsterCarnival");
            if (mc.getChildByPath("mobGenPos") != null)
            {
                for (MapleData area : mc.getChildByPath("mobGenPos"))
                {
                    nodeInfo.addMonsterPoint(MapleDataTool.getInt(area.getChildByPath("x")), MapleDataTool.getInt(area.getChildByPath("y")), MapleDataTool.getInt(area.getChildByPath("fh")),
                            MapleDataTool.getInt(area.getChildByPath("cy")), MapleDataTool.getInt("team", area, -1));
                }
            }
            if (mc.getChildByPath("mob") != null)
            {
                for (MapleData area : mc.getChildByPath("mob"))
                {
                    nodeInfo.addMobSpawn(MapleDataTool.getInt(area.getChildByPath("id")), MapleDataTool.getInt(area.getChildByPath("spendCP")));
                }
            }
            if (mc.getChildByPath("guardianGenPos") != null)
            {
                for (MapleData area : mc.getChildByPath("guardianGenPos"))
                {
                    nodeInfo.addGuardianSpawn(new Point(MapleDataTool.getInt(area.getChildByPath("x")), MapleDataTool.getInt(area.getChildByPath("y"))), MapleDataTool.getInt("team", area, -1));
                }
            }
            if (mc.getChildByPath("skill") != null)
            {
                for (MapleData area : mc.getChildByPath("skill"))
                {
                    nodeInfo.addSkillId(MapleDataTool.getInt(area));
                }
            }
        }
        return nodeInfo;
    }

    private MapleReactor loadReactor(MapleData reactor, String id, byte FacingDirection)
    {
        MapleReactor myReactor = new MapleReactor(MapleReactorFactory.getReactor(Integer.parseInt(id)), Integer.parseInt(id));
        myReactor.setFacingDirection(FacingDirection);
        myReactor.setPosition(new Point(MapleDataTool.getInt(reactor.getChildByPath("x")), MapleDataTool.getInt(reactor.getChildByPath("y"))));
        myReactor.setDelay(MapleDataTool.getInt(reactor.getChildByPath("reactorTime")) * 1000);
        myReactor.setName(MapleDataTool.getString(reactor.getChildByPath("name"), ""));
        return myReactor;
    }

    private String getMapStringName(int mapid)
    {
        StringBuilder builder = new StringBuilder();
        if (mapid < 100000000)
        {
            builder.append("maple");
        }
        else if ((mapid >= 100000000) && (mapid < 200000000))
        {
            builder.append("victoria");
        }
        else if ((mapid >= 200000000) && (mapid < 300000000))
        {
            builder.append("ossyria");
        }
        else if ((mapid >= 300000000) && (mapid < 400000000))
        {
            builder.append("3rd");
        }
        else if (((mapid >= 400000000) && (mapid < 410000000)) || ((mapid >= 940000000) && (mapid < 941000000)))
        {
            builder.append("grandis");
        }
        else if ((mapid >= 500000000) && (mapid < 510000000))
        {
            builder.append("thai");
        }
        else if ((mapid >= 510000000) && (mapid < 520000000))
        {
            builder.append("EU");
        }
        else if ((mapid >= 540000000) && (mapid <= 555000000))
        {
            builder.append("SG");
        }
        else if ((mapid >= 600000000) && (mapid <= 600020600))
        {
            builder.append("MasteriaGL");
        }
        else if ((mapid >= 677000000) && (mapid < 678000000))
        {
            builder.append("Episode1GL");
        }
        else if ((mapid >= 680000000) && (mapid < 681000000))
        {
            builder.append("global");
        }
        else if (((mapid >= 680100000) && (mapid < 681100000)) || ((mapid >= 682000000) && (mapid < 683000000)))
        {
            builder.append("HalloweenGL");
        }
        else if ((mapid >= 686000000) && (mapid < 687000000))
        {
            builder.append("event_6th");
        }
        else if ((mapid >= 689010000) && (mapid < 690000000))
        {
            builder.append("Pink ZakumGL");
        }
        else if ((mapid == 689000000) || (mapid == 689000010))
        {
            builder.append("CTF_GL");
        }
        else if (((mapid >= 690000000) && (mapid < 700000000)) || ((mapid >= 746000000) && (mapid < 746100000)))
        {
            builder.append("boost");
        }
        else if ((mapid >= 700000000) && (mapid < 742000000))
        {
            builder.append("chinese");
        }
        else if ((mapid >= 743000000) && (mapid < 744000000))
        {
            builder.append("taiwan");
        }
        else if ((mapid >= 744000000) && (mapid < 800000000))
        {
            builder.append("chinese");
        }
        else if ((mapid >= 860000000) && (mapid < 861000000))
        {
            builder.append("aquaroad");
        }
        else if ((mapid >= 863000000) && (mapid < 864000000))
        {
            builder.append("masteria");
        }
        else if ((mapid >= 800000000) && (mapid < 900000000))
        {
            builder.append("jp");
        }
        else
        {
            builder.append("etc");
        }
        builder.append("/");
        builder.append(mapid);
        return builder.toString();
    }

    public void removeInstanceMap(int instanceid)
    {
        if (isInstanceMapLoaded(instanceid))
        {
            getInstanceMap(instanceid).checkStates("");
            instanceMap.remove(instanceid);
        }
    }

    public boolean isInstanceMapLoaded(int instanceid)
    {
        return this.instanceMap.containsKey(instanceid);
    }

    public MapleMap getInstanceMap(int instanceid)
    {
        return this.instanceMap.get(instanceid);
    }

    public void removeMap(int instanceid)
    {
        if (isMapLoaded(instanceid))
        {
            getMap(instanceid).checkStates("");
            maps.remove(instanceid);
        }
    }

    public boolean isMapLoaded(int mapId)
    {
        return this.maps.containsKey(mapId);
    }

    public MapleMap getMap(int mapid)
    {
        return getMap(mapid, true, true, true);
    }

    public MapleMap CreateInstanceMap(int mapid, boolean respawns, boolean npcs, boolean reactors, int instanceid)
    {
        this.lock.lock();
        try
        {
            if (isInstanceMapLoaded(instanceid))
            {
                return getInstanceMap(instanceid);
            }
        }
        finally
        {
            this.lock.unlock();
        }
        MapleData mapData = null;
        try
        {
            mapData = this.source.getData(getMapName(mapid));
        }
        catch (Exception e)
        {
            return null;
        }
        if (mapData == null)
        {
            return null;
        }
        MapleData link = mapData.getChildByPath("info/link");
        if (link != null)
        {
            mapData = this.source.getData(getMapName(MapleDataTool.getIntConvert("info/link", mapData)));
        }

        float monsterRate = 0.0F;
        if (respawns)
        {
            MapleData mobRate = mapData.getChildByPath("info/mobRate");
            if (mobRate != null)
            {
                monsterRate = (Float) mobRate.getData();
            }
        }
        MapleMap map = new MapleMap(mapid, this.channel, MapleDataTool.getInt("info/returnMap", mapData), monsterRate);
        loadPortals(map, mapData.getChildByPath("portal"));
        map.setTop(MapleDataTool.getInt(mapData.getChildByPath("info/VRTop"), 0));
        map.setLeft(MapleDataTool.getInt(mapData.getChildByPath("info/VRLeft"), 0));
        map.setBottom(MapleDataTool.getInt(mapData.getChildByPath("info/VRBottom"), 0));
        map.setRight(MapleDataTool.getInt(mapData.getChildByPath("info/VRRight"), 0));
        List<MapleFoothold> allFootholds = new LinkedList<>();
        Point lBound = new Point();
        Point uBound = new Point();
        for (MapleData footRoot : mapData.getChildByPath("foothold"))
        {
            for (MapleData footCat : footRoot)
                for (MapleData footHold : footCat)
                {


                    MapleFoothold fh = new MapleFoothold(new Point(MapleDataTool.getInt(footHold.getChildByPath("x1")), MapleDataTool.getInt(footHold.getChildByPath("y1"))),
                            new Point(MapleDataTool.getInt(footHold.getChildByPath("x2")), MapleDataTool.getInt(footHold.getChildByPath("y2"))), Integer.parseInt(footHold.getName()));
                    fh.setPrev((short) MapleDataTool.getInt(footHold.getChildByPath("prev")));
                    fh.setNext((short) MapleDataTool.getInt(footHold.getChildByPath("next")));

                    if (fh.getX1() < lBound.x)
                    {
                        lBound.x = fh.getX1();
                    }
                    if (fh.getX2() > uBound.x)
                    {
                        uBound.x = fh.getX2();
                    }
                    if (fh.getY1() < lBound.y)
                    {
                        lBound.y = fh.getY1();
                    }
                    if (fh.getY2() > uBound.y)
                    {
                        uBound.y = fh.getY2();
                    }
                    allFootholds.add(fh);
                }
        }
        MapleData footRoot;
        MapleFootholdTree fTree = new MapleFootholdTree(lBound, uBound);
        for (MapleFoothold fh : allFootholds)
        {
            fTree.insert(fh);
        }
        map.setFootholds(fTree);
        if (map.getTop() == 0)
        {
            map.setTop(lBound.y);
        }
        if (map.getBottom() == 0)
        {
            map.setBottom(uBound.y);
        }
        if (map.getLeft() == 0)
        {
            map.setLeft(lBound.x);
        }
        if (map.getRight() == 0)
        {
            map.setRight(uBound.x);
        }
        int bossid = -1;
        String msg = null;
        if (mapData.getChildByPath("info/timeMob") != null)
        {
            bossid = MapleDataTool.getInt(mapData.getChildByPath("info/timeMob/id"), 0);
            msg = MapleDataTool.getString(mapData.getChildByPath("info/timeMob/message"), null);
        }
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM spawns WHERE mid = ?");
            ps.setInt(1, mapid);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                int sqlid = rs.getInt("idd");
                int sqlf = rs.getInt("f");
                boolean sqlhide = false;
                String sqltype = rs.getString("type");
                int sqlfh = rs.getInt("fh");
                int sqlcy = rs.getInt("cy");
                int sqlrx0 = rs.getInt("rx0");
                int sqlrx1 = rs.getInt("rx1");
                int sqlx = rs.getInt("x");
                int sqly = rs.getInt("y");
                int sqlmobTime = rs.getInt("mobtime");
                AbstractLoadedMapleLife sqlmyLife = loadLife(sqlid, sqlf, sqlhide, sqlfh, sqlcy, sqlrx0, sqlrx1, sqlx, sqly, sqltype);
                if (sqltype.equals("n"))
                {
                    map.addMapObject(sqlmyLife);
                }
                else if (sqltype.equals("m"))
                {
                    MapleMonster monster = (MapleMonster) sqlmyLife;
                    map.addMonsterSpawn(monster, sqlmobTime, (byte) -1, null);
                }
            }
        }
        catch (SQLException e)
        {
            System.out.println("读取SQL刷Npc和刷新怪物出错.");
        }


        for (MapleData life : mapData.getChildByPath("life"))
        {
            String type = MapleDataTool.getString(life.getChildByPath("type"));
            String limited = MapleDataTool.getString("limitedname", life, "");
            if (((npcs) || (!type.equals("n"))) && (limited.equals("")))
            {
                AbstractLoadedMapleLife myLife = loadLife(life, MapleDataTool.getString(life.getChildByPath("id")), type);
                if (((myLife instanceof MapleMonster)) && (!BattleConstants.isBattleMap(mapid)) && (!GameConstants.isNoSpawn(mapid)))
                {
                    MapleMonster mob = (MapleMonster) myLife;
                    map.addMonsterSpawn(mob, MapleDataTool.getInt("mobTime", life, 0), (byte) MapleDataTool.getInt("team", life, -1), mob.getId() == bossid ? msg : null);
                }
                else if ((myLife instanceof MapleNPC))
                {
                    map.addMapObject(myLife);
                }
            }
        }
        MapleData life;
        addAreaBossSpawn(map);
        map.setCreateMobInterval((short) MapleDataTool.getInt(mapData.getChildByPath("info/createMobInterval"), 9000));
        map.setFixedMob(MapleDataTool.getInt(mapData.getChildByPath("info/fixedMobCapacity"), 0));
        map.setPartyBonusRate(GameConstants.getPartyPlay(mapid, MapleDataTool.getInt(mapData.getChildByPath("info/partyBonusR"), 0)));
        map.loadMonsterRate(true);
        map.setNodes(loadNodes(mapid, mapData));


        if ((reactors) && (mapData.getChildByPath("reactor") != null) && (!BattleConstants.isBattleMap(mapid)))
        {
            for (MapleData reactor : mapData.getChildByPath("reactor"))
            {
                String id = MapleDataTool.getString(reactor.getChildByPath("id"));
                if (id != null)
                {
                    map.spawnReactor(loadReactor(reactor, id, (byte) MapleDataTool.getInt(reactor.getChildByPath("f"), 0)));
                }
            }
        }
        try
        {
            map.setMapName(MapleDataTool.getString("mapName", this.nameData.getChildByPath(getMapStringName(mapid)), ""));
            map.setStreetName(MapleDataTool.getString("streetName", this.nameData.getChildByPath(getMapStringName(mapid)), ""));
        }
        catch (Exception e)
        {
            map.setMapName("");
            map.setStreetName("");
        }
        map.setClock(MapleDataTool.getInt(mapData.getChildByPath("info/clock"), 0) > 0);
        map.setEverlast(MapleDataTool.getInt(mapData.getChildByPath("info/everlast"), 0) > 0);
        map.setTown(MapleDataTool.getInt(mapData.getChildByPath("info/town"), 0) > 0);
        map.setSoaring(MapleDataTool.getInt(mapData.getChildByPath("info/needSkillForFly"), 0) > 0);
        map.setForceMove(MapleDataTool.getInt(mapData.getChildByPath("info/lvForceMove"), 0));
        map.setHPDec(MapleDataTool.getInt(mapData.getChildByPath("info/decHP"), 0));
        map.setHPDecInterval(MapleDataTool.getInt(mapData.getChildByPath("info/decHPInterval"), 10000));
        map.setHPDecProtect(MapleDataTool.getInt(mapData.getChildByPath("info/protectItem"), 0));
        map.setForcedReturnMap(MapleDataTool.getInt(mapData.getChildByPath("info/forcedReturn"), 999999999));
        map.setTimeLimit(MapleDataTool.getInt(mapData.getChildByPath("info/timeLimit"), -1));
        map.setFieldLimit(MapleDataTool.getInt(mapData.getChildByPath("info/fieldLimit"), 0));
        map.setFieldType(MapleDataTool.getInt(mapData.getChildByPath("info/fieldType"), 0));
        map.setFirstUserEnter(MapleDataTool.getString(mapData.getChildByPath("info/onFirstUserEnter"), ""));
        map.setUserEnter(MapleDataTool.getString(mapData.getChildByPath("info/onUserEnter"), ""));
        map.setRecoveryRate(MapleDataTool.getFloat(mapData.getChildByPath("info/recovery"), 1.0F));
        map.setConsumeItemCoolTime(MapleDataTool.getInt(mapData.getChildByPath("info/consumeItemCoolTime"), 0));
        map.setInstanceId(instanceid);
        this.lock.lock();
        try
        {
            this.instanceMap.put(instanceid, map);
        }
        finally
        {
            this.lock.unlock();
        }
        return map;
    }

    public int getLoadedMaps()
    {
        return this.maps.size();
    }

    public void clearLoadedMap()
    {
        maps.clear();
    }

    public Collection<MapleMap> getAllLoadedMaps()
    {
        return instanceMap.values();
    }

    public java.util.Collection<MapleMap> getAllMaps()
    {
        return this.maps.values();
    }

    public void setChannel(int channel)
    {
        this.channel = channel;
    }
}
