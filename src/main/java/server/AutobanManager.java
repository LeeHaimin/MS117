package server;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.locks.ReentrantLock;

import client.MapleClient;
import handling.world.WorldBroadcastService;

public class AutobanManager implements Runnable
{
    private static final int AUTOBAN_POINTS = 5000;
    private static final AutobanManager instance = new AutobanManager();
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AutobanManager.class);
    private final Map<Integer, Integer> points = new HashMap<>();
    private final Map<Integer, List<String>> reasons = new HashMap<>();
    private final java.util.Set<ExpirationEntry> expirations = new TreeSet();
    private final ReentrantLock lock = new ReentrantLock(true);

    public static AutobanManager getInstance()
    {
        return instance;
    }

    public void autoban(MapleClient c, String reason)
    {
        if (c.getPlayer() == null)
        {
            return;
        }
        if (c.getPlayer().isGM())
        {
            c.getPlayer().dropMessage(5, "[警告] A/b 触发: " + reason);
        }
        else if (configs.ServerConfig.AUTO_BAN_ENABLE)
        {
            addPoints(c, AUTOBAN_POINTS, 0L, reason);
        }
        else
        {
            WorldBroadcastService.getInstance().broadcastGMMessage(tools.MaplePacketCreator.serverNotice(6,
                    "[GM Message] 玩家: " + c.getPlayer().getName() + " ID: " + c.getPlayer().getId() + " (等级 " + c.getPlayer().getLevel() + ") 游戏操作异常. (原因: " + reason + ")"));
        }
    }

    public void addPoints(MapleClient c, int points, long expiration, String reason)
    {
        this.lock.lock();
        try
        {
            int acc = c.getPlayer().getAccountID();

            if (this.points.containsKey(acc))
            {
                int SavedPoints = this.points.get(acc);
                if (SavedPoints >= AUTOBAN_POINTS)
                {
                    return;
                }
                this.points.put(acc, SavedPoints + points);
                List<String> reasonList = this.reasons.get(acc);
                reasonList.add(reason);
            }
            else
            {
                this.points.put(acc, points);
                List<String> reasonList = new LinkedList<>();
                reasonList.add(reason);
                this.reasons.put(acc, reasonList);
            }

            if (this.points.get(acc) >= AUTOBAN_POINTS)
            {
                log.info("[作弊] 玩家 " + c.getPlayer().getName() + " A/b 触发 " + reason);
                if (c.getPlayer().isGM())
                {
                    c.getPlayer().dropMessage(5, "[警告] A/b 触发 : " + reason);
                    return;
                }
                StringBuilder sb = new StringBuilder("A/b ");
                sb.append(c.getPlayer().getName());
                sb.append(" (IP ");
                sb.append(c.getSession().getRemoteAddress().toString());
                sb.append("): ");
                for (Object s : this.reasons.get(acc))
                {
                    sb.append(s);
                    sb.append(", ");
                }
                WorldBroadcastService.getInstance().broadcastMessage(tools.MaplePacketCreator.serverNotice(0, " <" + c.getPlayer().getName() + "> 被系统封号 (原因: " + reason + ")"));
                c.getPlayer().ban(sb.toString(), false, true, false);
            }
            else if (expiration > 0L)
            {
                this.expirations.add(new ExpirationEntry(System.currentTimeMillis() + expiration, acc, points));
            }
        }
        finally
        {
            this.lock.unlock();
        }
    }

    public void run()
    {
        long now = System.currentTimeMillis();
        for (ExpirationEntry e : this.expirations)
        {
            if (e.time <= now)
            {
                this.points.put(e.acc, this.points.get(e.acc).intValue() - e.points);
            }
            else
            {
                return;
            }
        }
    }

    private static class ExpirationEntry implements Comparable<ExpirationEntry>
    {
        public final long time;
        public final int acc;
        public final int points;

        public ExpirationEntry(long time, int acc, int points)
        {
            this.time = time;
            this.acc = acc;
            this.points = points;
        }

        public int compareTo(ExpirationEntry o)
        {
            return (int) (this.time - o.time);
        }

        public boolean equals(Object oth)
        {
            if (!(oth instanceof ExpirationEntry))
            {
                return false;
            }
            ExpirationEntry ee = (ExpirationEntry) oth;
            return (this.time == ee.time) && (this.points == ee.points) && (this.acc == ee.acc);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\AutobanManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */