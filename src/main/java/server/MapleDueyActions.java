package server;

import client.inventory.Item;

public class MapleDueyActions
{
    private String sender = null;
    private Item item = null;
    private int mesos = 0;
    private int quantity = 1;
    private long sentTime;
    private int packageId = 0;

    public MapleDueyActions(int pId, Item item)
    {
        this.item = item;
        this.quantity = item.getQuantity();
        this.packageId = pId;
    }

    public MapleDueyActions(int pId)
    {
        this.packageId = pId;
    }

    public String getSender()
    {
        return this.sender;
    }

    public void setSender(String name)
    {
        this.sender = name;
    }

    public Item getItem()
    {
        return this.item;
    }

    public int getMesos()
    {
        return this.mesos;
    }

    public void setMesos(int set)
    {
        this.mesos = set;
    }

    public int getQuantity()
    {
        return this.quantity;
    }

    public int getPackageId()
    {
        return this.packageId;
    }

    public long getSentTime()
    {
        return this.sentTime;
    }

    public void setSentTime(long sentTime)
    {
        this.sentTime = sentTime;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\MapleDueyActions.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */