package server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import client.inventory.Item;
import client.inventory.ItemLoader;
import client.inventory.MapleInventoryType;
import tools.Pair;

public class MTSStorage
{
    private static final long serialVersionUID = 231541893513228L;
    private static MTSStorage instance;
    private final Map<Integer, MTSCart> idToCart;
    private final AtomicInteger packageId;
    private final Map<Integer, MTSItemInfo> buyNow;
    private final ReentrantReadWriteLock mutex;
    private final ReentrantReadWriteLock cart_mutex;
    private long lastUpdate = System.currentTimeMillis();
    private boolean end = false;

    public MTSStorage()
    {
        this.idToCart = new LinkedHashMap<>();
        this.buyNow = new LinkedHashMap<>();
        this.packageId = new AtomicInteger(1);
        this.mutex = new ReentrantReadWriteLock();
        this.cart_mutex = new ReentrantReadWriteLock();
    }

    public static MTSStorage getInstance()
    {
        return instance;
    }

    public static void load()
    {
        if (instance == null)
        {
            instance = new MTSStorage();
            instance.loadBuyNow();
        }
    }

    private void loadBuyNow()
    {
        int lastPackage = 0;


        Connection con = database.DatabaseConnection.getConnection();
        try
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM mts_items WHERE tab = 1");
            ResultSet rs = ps.executeQuery();
            int charId;
            while (rs.next())
            {
                lastPackage = rs.getInt("id");
                charId = rs.getInt("characterid");
                if (!this.idToCart.containsKey(charId))
                {
                    this.idToCart.put(charId, new MTSCart(charId));
                }
                Map<Long, Pair<Item, MapleInventoryType>> items = ItemLoader.拍卖道具.loadItems(false, lastPackage);
                if ((items != null) && (items.size() > 0))
                {
                    for (Pair<Item, MapleInventoryType> i : items.values())
                    {
                        this.buyNow.put(lastPackage, new MTSItemInfo(rs.getInt("price"), i.getLeft(), rs.getString("seller"), lastPackage, charId, rs.getLong("expiration")));
                    }
                }
            }
            rs.close();
            ps.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        this.packageId.set(lastPackage);
    }

    public boolean check(int packageid)
    {
        return getSingleItem(packageid) != null;
    }

    public MTSItemInfo getSingleItem(int packageid)
    {
        this.mutex.readLock().lock();
        try
        {
            return this.buyNow.get(packageid);
        }
        finally
        {
            this.mutex.readLock().unlock();
        }
    }

    public boolean checkCart(int packageid, int charID)
    {
        MTSItemInfo item = getSingleItem(packageid);
        return (item != null) && (item.getCharacterId() != charID);
    }

    public void addToBuyNow(MTSCart cart, Item item, int price, int cid, String seller, long expiration)
    {
        this.mutex.writeLock().lock();
        int id;
        try
        {
            id = this.packageId.incrementAndGet();
            this.buyNow.put(id, new MTSItemInfo(price, item, seller, id, cid, expiration));
        }
        finally
        {
            this.mutex.writeLock().unlock();
        }
        cart.addToNotYetSold(id);
    }

    public boolean removeFromBuyNow(int id, int cidBought, boolean check)
    {
        Item item = null;
        this.mutex.writeLock().lock();
        MTSItemInfo r;
        try
        {
            if (this.buyNow.containsKey(id))
            {
                r = this.buyNow.get(id);
                if ((!check) || (r.getCharacterId() == cidBought))
                {
                    item = r.getItem();
                    this.buyNow.remove(id);
                }
            }
        }
        finally
        {
            this.mutex.writeLock().unlock();
        }
        if (item != null)
        {
            this.cart_mutex.readLock().lock();
            try
            {
                for (Object c : this.idToCart.entrySet())
                {
                    ((MTSCart) ((Map.Entry) c).getValue()).removeFromCart(id);
                    ((MTSCart) ((Map.Entry) c).getValue()).removeFromNotYetSold(id);
                    if ((Integer) ((Map.Entry) c).getKey() == cidBought)
                    {
                        ((MTSCart) ((Map.Entry) c).getValue()).addToInventory(item);
                    }
                }
            }
            finally
            {
                this.cart_mutex.readLock().unlock();
            }
        }
        return item != null;
    }

    public void checkExpirations()
    {
        if (System.currentTimeMillis() - this.lastUpdate > 3600000L)
        {
            saveBuyNow(false);
        }
    }

    public void saveBuyNow(boolean isShutDown)
    {
        if (this.end)
        {
            return;
        }
        this.end = isShutDown;
        if (isShutDown)
        {
            System.out.println("正在保存 MTS...");
        }
        Map<Integer, ArrayList<Item>> expire = new HashMap<>();
        List<Integer> toRemove = new ArrayList<>();
        long now = System.currentTimeMillis();
        Map<Integer, ArrayList<Pair<Item, MapleInventoryType>>> items = new HashMap<>();
        Connection con = database.DatabaseConnection.getConnection();
        this.mutex.writeLock().lock();
        int i;
        try
        {
            PreparedStatement ps = con.prepareStatement("DELETE FROM mts_items WHERE tab = 1");
            ps.execute();
            ps.close();
            ps = con.prepareStatement("INSERT INTO mts_items VALUES (?, ?, ?, ?, ?, ?)");
            for (MTSItemInfo m : this.buyNow.values())
            {
                if (now > m.getEndingDate())
                {
                    if (!expire.containsKey(m.getCharacterId()))
                    {
                        expire.put(m.getCharacterId(), new ArrayList());
                    }
                    expire.get(m.getCharacterId()).add(m.getItem());
                    toRemove.add(m.getId());
                    items.put(m.getId(), null);
                }
                else
                {
                    ps.setInt(1, m.getId());
                    ps.setByte(2, (byte) 1);
                    ps.setInt(3, m.getPrice());
                    ps.setInt(4, m.getCharacterId());
                    ps.setString(5, m.getSeller());
                    ps.setLong(6, m.getEndingDate());
                    ps.executeUpdate();
                    if (!items.containsKey(m.getId()))
                    {
                        items.put(m.getId(), new ArrayList());
                    }
                    items.get(m.getId()).add(new Pair(m.getItem(), constants.ItemConstants.getInventoryType(m.getItem().getItemId())));
                }
            }
            for (Integer integer : toRemove)
            {
                i = integer;
                this.buyNow.remove(i);
            }
            ps.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            this.mutex.writeLock().unlock();
        }
        if (isShutDown)
        {
            System.out.println("正在保存 MTS 道具信息...");
        }
        try
        {
            for (Object ite : items.entrySet())
            {
                ItemLoader.拍卖道具.saveItems((List) ((Map.Entry) ite).getValue(), (Integer) ((Map.Entry) ite).getKey());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        if (isShutDown)
        {
            System.out.println("正在保存 MTS carts...");
        }
        this.cart_mutex.writeLock().lock();
        try
        {
            for (Object c : this.idToCart.entrySet())
            {
                for (int id : toRemove)
                {
                    ((MTSCart) ((Map.Entry) c).getValue()).removeFromCart(id);
                    ((MTSCart) ((Map.Entry) c).getValue()).removeFromNotYetSold(id);
                }
                if (expire.containsKey(((Map.Entry) c).getKey()))
                {
                    for (Item item : expire.get(((Map.Entry) c).getKey()))
                    {
                        ((MTSCart) ((Map.Entry) c).getValue()).addToInventory(item);
                    }
                }
                ((MTSCart) ((Map.Entry) c).getValue()).save();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            this.cart_mutex.writeLock().unlock();
        }
        this.lastUpdate = System.currentTimeMillis();
    }

    public MTSCart getCart(int characterId)
    {
        MTSCart ret;
        cart_mutex.readLock().lock();
        try
        {
            ret = idToCart.get(characterId);
        }
        finally
        {
            cart_mutex.readLock().unlock();
        }
        if (ret == null)
        {
            cart_mutex.writeLock().lock();
            try
            {
                ret = new MTSCart(characterId);
                idToCart.put(characterId, ret);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
            finally
            {
                cart_mutex.writeLock().unlock();
            }
        }
        return ret;
    }

    public byte[] getCurrentMTS(MTSCart cart)
    {
        this.mutex.readLock().lock();
        try
        {
            return tools.packet.MTSCSPacket.sendMTS(getMultiItems(cart.getCurrentView(), cart.getPage()), cart.getTab(), cart.getType(), cart.getPage(), cart.getCurrentView().size());
        }
        finally
        {
            this.mutex.readLock().unlock();
        }
    }

    public List<MTSItemInfo> getMultiItems(List<Integer> items, int pageNumber)
    {
        List<MTSItemInfo> ret = new ArrayList<>();

        List<Integer> cartt = new ArrayList(items);
        if (pageNumber > cartt.size() / 16 + (cartt.size() % 16 > 0 ? 1 : 0))
        {
            pageNumber = 0;
        }
        int maxSize = Math.min(cartt.size(), pageNumber * 16 + 16);
        int minSize = Math.min(cartt.size(), pageNumber * 16);
        for (int i = minSize; i < maxSize; i++)
        {
            if (cartt.size() <= i) break;
            MTSItemInfo r = this.buyNow.get(cartt.get(i));
            if (r == null)
            {
                items.remove(i);
                cartt.remove(i);
            }
            else
            {
                ret.add(r);
            }
        }


        return ret;
    }

    public byte[] getCurrentNotYetSold(MTSCart cart)
    {
        this.mutex.readLock().lock();
        try
        {
            List<MTSItemInfo> nys = new ArrayList<>();

            List<Integer> nyss = new ArrayList(cart.getNotYetSold());
            for (Object localObject1 = nyss.iterator(); ((Iterator) localObject1).hasNext(); )
            {
                int i = (Integer) ((Iterator) localObject1).next();
                MTSItemInfo r = this.buyNow.get(i);
                if (r == null)
                {
                    cart.removeFromNotYetSold(i);
                }
                else
                {
                    nys.add(r);
                }
            }
            return tools.packet.MTSCSPacket.getNotYetSoldInv(nys);
        }
        finally
        {
            this.mutex.readLock().unlock();
        }
    }

    public byte[] getCurrentTransfer(MTSCart cart, boolean changed)
    {
        return tools.packet.MTSCSPacket.getTransferInventory(cart.getInventory(), changed);
    }

    public List<Integer> getBuyNow(int type)
    {
        this.mutex.readLock().lock();
        try
        {
            if (type == 0)
            {
                return new ArrayList(this.buyNow.keySet());
            }


            Object ret = new ArrayList(this.buyNow.values());
            List<Integer> rett = new ArrayList<>();

            for (int i = 0; i < ((List) ret).size(); i++)
            {
                MTSItemInfo r = (MTSItemInfo) ((List) ret).get(i);
                if ((r != null) && (constants.ItemConstants.getInventoryType(r.getItem().getItemId()).getType() == type))
                {
                    rett.add(r.getId());
                }
            }
            return rett;
        }
        finally
        {
            this.mutex.readLock().unlock();
        }
    }

    public List<Integer> getSearch(boolean item, String name, int type, int tab)
    {
        this.mutex.readLock().lock();
        try
        {
            MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
            if ((tab != 1) || (name.length() <= 0))
            {
                return new ArrayList();
            }
            name = name.toLowerCase();
            Object ret = new ArrayList(this.buyNow.values());
            List<Integer> rett = new ArrayList<>();

            for (int i = 0; i < ((List) ret).size(); i++)
            {
                MTSItemInfo r = (MTSItemInfo) ((List) ret).get(i);
                if ((r != null) && ((type == 0) || (constants.ItemConstants.getInventoryType(r.getItem().getItemId()).getType() == type)))
                {
                    String thename = item ? ii.getName(r.getItem().getItemId()) : r.getSeller();
                    if ((thename != null) && (thename.toLowerCase().contains(name)))
                    {
                        rett.add(r.getId());
                    }
                }
            }
            return rett;
        }
        finally
        {
            this.mutex.readLock().unlock();
        }
    }

    private List<MTSItemInfo> getCartItems(MTSCart cart)
    {
        return getMultiItems(cart.getCart(), cart.getPage());
    }

    public static class MTSItemInfo
    {
        private final int price;
        private final Item item;
        private final String seller;
        private final int id;
        private final int cid;
        private final long date;

        public MTSItemInfo(int price, Item item, String seller, int id, int cid, long date)
        {
            this.item = item;
            this.price = price;
            this.seller = seller;
            this.id = id;
            this.cid = cid;
            this.date = date;
        }

        public Item getItem()
        {
            return this.item;
        }

        public int getPrice()
        {
            return this.price;
        }

        public int getRealPrice()
        {
            return this.price + getTaxes();
        }

        public int getTaxes()
        {
            return 0 + this.price * 5 / 100;
        }

        public int getId()
        {
            return this.id;
        }

        public int getCharacterId()
        {
            return this.cid;
        }

        public long getEndingDate()
        {
            return this.date;
        }

        public String getSeller()
        {
            return this.seller;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\MTSStorage.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */