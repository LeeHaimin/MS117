package server;

import java.util.ArrayList;
import java.util.List;

import client.inventory.Item;

public class MerchItemPackage
{
    private long sentTime;
    private int mesos = 0;
    private List<Item> items = new ArrayList<>();

    public List<Item> getItems()
    {
        return this.items;
    }

    public void setItems(List<Item> items)
    {
        this.items = items;
    }

    public long getSentTime()
    {
        return this.sentTime;
    }

    public void setSentTime(long sentTime)
    {
        this.sentTime = sentTime;
    }

    public int getMesos()
    {
        return this.mesos;
    }

    public void setMesos(int set)
    {
        this.mesos = set;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\MerchItemPackage.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */