package server;

import java.awt.Point;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.ItemFlag;
import constants.ItemConstants;
import provider.MapleData;
import provider.MapleDataDirectoryEntry;
import provider.MapleDataEntry;
import provider.MapleDataProvider;
import provider.MapleDataTool;
import tools.Pair;
import tools.Triple;

public class MapleItemInformationProvider
{
    private static final MapleItemInformationProvider instance = new MapleItemInformationProvider();
    protected final MapleDataProvider chrData = provider.MapleDataProviderFactory.getDataProvider(new java.io.File(System.getProperty("wzpath") + "/Character.wz"));
    protected final MapleDataProvider etcData = provider.MapleDataProviderFactory.getDataProvider(new java.io.File(System.getProperty("wzpath") + "/Etc.wz"));
    protected final MapleDataProvider itemData = provider.MapleDataProviderFactory.getDataProvider(new java.io.File(System.getProperty("wzpath") + "/Item.wz"));
    protected final Map<Integer, ItemInformation> dataCache = new HashMap<>();
    protected final Map<String, List<Triple<String, Point, Point>>> afterImage = new HashMap<>();
    protected final Map<Integer, List<StructItemOption>> potentialCache = new HashMap<>();
    protected final Map<Integer, Map<Integer, StructItemOption>> socketCache = new HashMap<>();
    protected final Map<Integer, MapleStatEffect> itemEffects = new HashMap<>();
    protected final Map<Integer, MapleStatEffect> itemEffectsEx = new HashMap<>();
    protected final Map<Integer, Integer> mobIds = new HashMap<>();
    protected final Map<Integer, Pair<Integer, Integer>> potLife = new HashMap<>();
    protected final Map<Integer, StructFamiliar> familiars = new HashMap<>();
    protected final Map<Integer, StructFamiliar> familiars_Item = new HashMap<>();
    protected final Map<Integer, StructFamiliar> familiars_Mob = new HashMap<>();
    protected final Map<Integer, Integer> androidType = new HashMap<>();
    protected final Map<Integer, StructAndroid> androidInfo = new HashMap<>();
    protected final Map<Integer, Triple<Integer, List<Integer>, List<Integer>>> monsterBookSets = new HashMap<>();
    protected final Map<Integer, StructSetItem> SetItemInfo = new HashMap<>();
    protected final Map<Integer, Map<String, String>> getExpCardTimes = new HashMap<>();
    protected final Map<Integer, ScriptedItem> scriptedItemCache = new HashMap<>();
    protected final Map<Integer, Boolean> floatCashItem = new HashMap<>();
    protected final Map<Integer, Short> petFlagInfo = new HashMap<>();
    protected final Map<Integer, Integer> petSetItemID = new HashMap<>();
    protected final Map<Integer, Integer> successRates = new HashMap<>();
    protected final Map<Integer, Integer> forceUpgrade = new HashMap<>();
    protected final Map<Integer, Integer> ScrollLimitBreak = new HashMap<>();
    protected final Map<Integer, StructCrossHunterShop> crossHunterShop = new HashMap<>();
    protected final Map<Integer, Pair<Integer, Integer>> chairRecovery = new HashMap<>();
    protected final Map<Integer, Integer> exclusiveEquip = new HashMap<>();
    protected final Map<Integer, StructExclusiveEquip> exclusiveEquipInfo = new HashMap<>();
    protected final Map<Integer, Boolean> noCursedScroll = new HashMap<>();
    protected final Map<Integer, Boolean> noNegativeScroll = new HashMap<>();
    private ItemInformation tmpInfo = null;

    public static MapleItemInformationProvider getInstance()
    {
        return instance;
    }

    public void runEtc()
    {
        if ((!this.SetItemInfo.isEmpty()) || (!this.potentialCache.isEmpty()) || (!this.socketCache.isEmpty()))
        {
            return;
        }


        MapleData setsData = this.etcData.getData("SetItemInfo.img");


        for (MapleData dat : setsData)
        {
            StructSetItem SetItem = new StructSetItem();
            SetItem.setItemID = Integer.parseInt(dat.getName());
            SetItem.setItemName = MapleDataTool.getString("setItemName", dat, "");
            SetItem.completeCount = ((byte) MapleDataTool.getIntConvert("completeCount", dat, 0));
            for (MapleData level : dat.getChildByPath("ItemID"))
            {
                Iterator localIterator3;
                if (level.getType() != provider.MapleDataType.INT)
                {
                    for (localIterator3 = level.iterator(); localIterator3.hasNext(); )
                    {
                        MapleData leve = (MapleData) localIterator3.next();
                        if ((!leve.getName().equals("representName")) && (!leve.getName().equals("typeName")))
                        {
                            SetItem.itemIDs.add(MapleDataTool.getInt(leve));
                        }
                    }
                }
                else
                {
                    SetItem.itemIDs.add(MapleDataTool.getInt(level));
                }
            }
            for (MapleData level : dat.getChildByPath("Effect"))
            {
                StructSetItemStat SetItemStat = new StructSetItemStat();
                SetItemStat.incSTR = MapleDataTool.getIntConvert("incSTR", level, 0);
                SetItemStat.incDEX = MapleDataTool.getIntConvert("incDEX", level, 0);
                SetItemStat.incINT = MapleDataTool.getIntConvert("incINT", level, 0);
                SetItemStat.incLUK = MapleDataTool.getIntConvert("incLUK", level, 0);
                SetItemStat.incMHP = MapleDataTool.getIntConvert("incMHP", level, 0);
                SetItemStat.incMMP = MapleDataTool.getIntConvert("incMMP", level, 0);
                SetItemStat.incMHPr = MapleDataTool.getIntConvert("incMHPr", level, 0);
                SetItemStat.incMMPr = MapleDataTool.getIntConvert("incMMPr", level, 0);
                SetItemStat.incACC = MapleDataTool.getIntConvert("incACC", level, 0);
                SetItemStat.incEVA = MapleDataTool.getIntConvert("incEVA", level, 0);
                SetItemStat.incPDD = MapleDataTool.getIntConvert("incPDD", level, 0);
                SetItemStat.incMDD = MapleDataTool.getIntConvert("incMDD", level, 0);
                SetItemStat.incPAD = MapleDataTool.getIntConvert("incPAD", level, 0);
                SetItemStat.incMAD = MapleDataTool.getIntConvert("incMAD", level, 0);
                SetItemStat.incJump = MapleDataTool.getIntConvert("incJump", level, 0);
                SetItemStat.incSpeed = MapleDataTool.getIntConvert("incSpeed", level, 0);
                SetItemStat.incAllStat = MapleDataTool.getIntConvert("incAllStat", level, 0);
                SetItemStat.incPQEXPr = MapleDataTool.getIntConvert("incPQEXPr", level, 0);
                SetItemStat.incPVPDamage = MapleDataTool.getIntConvert("incPVPDamage", level, 0);
                SetItemStat.option1 = MapleDataTool.getIntConvert("Option/1/option", level, 0);
                SetItemStat.option2 = MapleDataTool.getIntConvert("Option/2/option", level, 0);
                SetItemStat.option1Level = MapleDataTool.getIntConvert("Option/1/level", level, 0);
                SetItemStat.option2Level = MapleDataTool.getIntConvert("Option/2/level", level, 0);
                SetItemStat.skillId = MapleDataTool.getIntConvert("activeSkill/0/id", level, 0);
                SetItemStat.skillLevel = MapleDataTool.getIntConvert("activeSkill/0/level", level, 0);
                SetItem.setItemStat.put(Integer.parseInt(level.getName()), SetItemStat);
            }
            this.SetItemInfo.put(SetItem.setItemID, SetItem);
        }

        MapleData potsData = this.itemData.getData("ItemOption.img");

        for (MapleData dat : potsData)
        {
            List items = new LinkedList<>();
            for (MapleData potLevel : dat.getChildByPath("level"))
            {
                StructItemOption item = new StructItemOption();
                item.opID = Integer.parseInt(dat.getName());
                item.optionType = MapleDataTool.getIntConvert("info/optionType", dat, 0);
                item.reqLevel = MapleDataTool.getIntConvert("info/reqLevel", dat, 0);
                item.opString = MapleDataTool.getString("info/string", dat, "");
                for (String i : StructItemOption.types)
                {
                    if (i.equals("face"))
                    {
                        item.face = MapleDataTool.getString("face", potLevel, "");
                    }
                    else
                    {
                        int level = MapleDataTool.getIntConvert(i, potLevel, 0);
                        if (level > 0)
                        {
                            item.data.put(i, level);
                        }
                    }
                }
                switch (item.opID)
                {
                    case 31001:
                    case 31002:
                    case 31003:
                    case 31004:
                        item.data.put("skillID", item.opID - 23001);
                        break;
                    case 41005:
                    case 41006:
                    case 41007:
                        item.data.put("skillID", item.opID - 33001);
                }

                items.add(item);
            }
            this.potentialCache.put(Integer.parseInt(dat.getName()), items);
        }

        Map<Integer, StructItemOption> gradeS = new HashMap<>();
        Map<Integer, StructItemOption> gradeA = new HashMap<>();
        Map<Integer, StructItemOption> gradeB = new HashMap<>();
        Map<Integer, StructItemOption> gradeC = new HashMap<>();
        Map<Integer, StructItemOption> gradeD = new HashMap<>();
        MapleData nebuliteData = this.itemData.getData("Install/0306.img");
        for (MapleData dat : nebuliteData)
        {
            StructItemOption item = new StructItemOption();
            item.opID = Integer.parseInt(dat.getName());
            item.optionType = MapleDataTool.getInt("optionType", dat.getChildByPath("socket"), 0);
            for (MapleData info : dat.getChildByPath("socket/option"))
            {
                String optionString = MapleDataTool.getString("optionString", info, "");
                int level = MapleDataTool.getInt("level", info, 0);
                if (level > 0)
                {
                    item.data.put(optionString, level);
                }
            }
            switch (item.opID)
            {
                case 3063370:
                    item.data.put("skillID", 8000);
                    break;
                case 3063380:
                    item.data.put("skillID", 8001);
                    break;
                case 3063390:
                    item.data.put("skillID", 8002);
                    break;
                case 3063400:
                    item.data.put("skillID", 8003);
                    break;
                case 3064470:
                    item.data.put("skillID", 8004);
                    break;
                case 3064480:
                    item.data.put("skillID", 8005);
                    break;
                case 3064490:
                    item.data.put("skillID", 8006);
            }

            switch (ItemConstants.getNebuliteGrade(item.opID))
            {
                case 4:
                    gradeS.put(Integer.parseInt(dat.getName()), item);
                    break;
                case 3:
                    ((Map) gradeA).put(Integer.parseInt(dat.getName()), item);
                    break;
                case 2:
                    gradeB.put(Integer.parseInt(dat.getName()), item);
                    break;
                case 1:
                    gradeC.put(Integer.parseInt(dat.getName()), item);
                    break;
                case 0:
                    ((Map) gradeD).put(Integer.parseInt(dat.getName()), item);
            }

        }
        this.socketCache.put(4, gradeS);
        this.socketCache.put(3, gradeA);
        this.socketCache.put(2, gradeB);
        this.socketCache.put(1, gradeC);
        this.socketCache.put(0, gradeD);


        MapleDataDirectoryEntry e = (MapleDataDirectoryEntry) this.etcData.getRoot().getEntry("Android");

        for (provider.MapleDataFileEntry mapleDataFileEntry : e.getFiles())
        {
            MapleDataEntry d = mapleDataFileEntry;
            MapleData iz = this.etcData.getData("Android/" + d.getName());
            StructAndroid android = new StructAndroid();
            int type = Integer.parseInt(d.getName().substring(0, 4));
            android.type = type;
            android.gender = MapleDataTool.getIntConvert("info/gender", iz, 0);
            for (MapleData ds : iz.getChildByPath("costume/skin"))
            {
                android.skin.add(MapleDataTool.getInt(ds, 2000));
            }
            for (MapleData ds : iz.getChildByPath("costume/hair"))
            {
                android.hair.add(MapleDataTool.getInt(ds, android.gender == 0 ? 20101 : 21101));
            }
            for (MapleData ds : iz.getChildByPath("costume/face"))
            {
                android.face.add(MapleDataTool.getInt(ds, android.gender == 0 ? 30110 : 31510));
            }
            this.androidInfo.put(type, android);
        }


        MapleData shopData = this.etcData.getData("CrossHunterChapter.img").getChildByPath("Shop");
        for (MapleData dat : shopData)
        {
            int key = Integer.parseInt(dat.getName());
            StructCrossHunterShop shop = new StructCrossHunterShop(MapleDataTool.getIntConvert("itemId", dat, 0), MapleDataTool.getIntConvert("tokenPrice", dat, -1), MapleDataTool.getIntConvert(
                    "potentialGrade", dat, 0));
            this.crossHunterShop.put(key, shop);
        }

        MapleData lifesData = this.etcData.getData("ItemPotLifeInfo.img");
        for (MapleData d : lifesData)
        {
            if ((d.getChildByPath("info") != null) && (MapleDataTool.getInt("type", d.getChildByPath("info"), 0) == 1))
            {
                this.potLife.put(MapleDataTool.getInt("counsumeItem", d.getChildByPath("info"), 0), new Pair(Integer.parseInt(d.getName()), d.getChildByPath("level").getChildren().size()));
            }
        }
        List<Triple<String, Point, Point>> thePointK = new ArrayList<>();
        List<Triple<String, Point, Point>> thePointA = new ArrayList<>();

        MapleDataDirectoryEntry a = (MapleDataDirectoryEntry) this.chrData.getRoot().getEntry("Afterimage");
        for (provider.MapleDataEntry b : a.getFiles())
        {
            MapleData iz = this.chrData.getData("Afterimage/" + b.getName());
            List thePoint = new ArrayList<>();
            Map<String, Pair<Point, Point>> dummy = new HashMap<>();
            for (MapleData i : iz)
            {
                for (MapleData xD : i)
                {
                    if ((!xD.getName().contains("prone")) && (!xD.getName().contains("double")) && (!xD.getName().contains("triple")) && (


                            ((!b.getName().contains("bow")) && (!b.getName().contains("Bow"))) || ((xD.getName().contains("shoot")) && (


                                    ((!b.getName().contains("gun")) && (!b.getName().contains("cannon"))) || (xD.getName().contains("shot"))))))
                    {

                        if (dummy.containsKey(xD.getName()))
                        {
                            if (xD.getChildByPath("lt") != null)
                            {
                                Point lt = (Point) xD.getChildByPath("lt").getData();
                                Point ourLt = dummy.get(xD.getName()).left;
                                if (lt.x < ourLt.x)
                                {
                                    ourLt.x = lt.x;
                                }
                                if (lt.y < ourLt.y)
                                {
                                    ourLt.y = lt.y;
                                }
                            }
                            if (xD.getChildByPath("rb") != null)
                            {
                                Point rb = (Point) xD.getChildByPath("rb").getData();
                                Point ourRb = dummy.get(xD.getName()).right;
                                if (rb.x > ourRb.x)
                                {
                                    ourRb.x = rb.x;
                                }
                                if (rb.y > ourRb.y)
                                {
                                    ourRb.y = rb.y;
                                }
                            }
                        }
                        else
                        {
                            Point lt = null;
                            Point rb = null;
                            if (xD.getChildByPath("lt") != null)
                            {
                                lt = (Point) xD.getChildByPath("lt").getData();
                            }
                            if (xD.getChildByPath("rb") != null)
                            {
                                rb = (Point) xD.getChildByPath("rb").getData();
                            }
                            dummy.put(xD.getName(), new Pair(lt, rb));
                        }
                    }
                }
            }
            for (Map.Entry<String, Pair<Point, Point>> stringPairEntry : dummy.entrySet())
            {
                Map.Entry ez = stringPairEntry;
                if ((((String) ez.getKey()).length() > 2) && (((String) ez.getKey()).substring(((String) ez.getKey()).length() - 2, ((String) ez.getKey()).length() - 1).equals("D")))
                {
                    thePointK.add(new Triple(ez.getKey(), ((Pair) ez.getValue()).left, ((Pair) ez.getValue()).right));
                }
                else if (((String) ez.getKey()).contains("PoleArm"))
                {
                    thePointA.add(new Triple(ez.getKey(), ((Pair) ez.getValue()).left, ((Pair) ez.getValue()).right));
                }
                else
                {
                    thePoint.add(new Triple(ez.getKey(), ((Pair) ez.getValue()).left, ((Pair) ez.getValue()).right));
                }
            }
            this.afterImage.put(b.getName().substring(0, b.getName().length() - 4), thePoint);
        }
        List<Triple<String, Point, Point>> thePoint;
        Map.Entry<String, Pair<Point, Point>> ez;
        this.afterImage.put("katara", thePointK);
        this.afterImage.put("aran", thePointA);

        MapleData exclusiveEquipData = this.etcData.getData("ExclusiveEquip.img");


        for (MapleData datt : exclusiveEquipData)
        {
            StructExclusiveEquip exclusive = new StructExclusiveEquip();
            int exId = Integer.parseInt(datt.getName());
            String msg = MapleDataTool.getString("msg", datt, "");
            msg = msg.replace("\\r\\n", "\r\n");
            msg = msg.replace("-------<", "---<");
            msg = msg.replace(">------", ">---");
            exclusive.id = exId;
            exclusive.msg = msg;
            for (MapleData llevel : datt.getChildByPath("item"))
            {
                int itemId = MapleDataTool.getInt(llevel);
                exclusive.itemIDs.add(itemId);
                this.exclusiveEquip.put(itemId, exId);
            }
            this.exclusiveEquipInfo.put(exId, exclusive);
        }

        StructExclusiveEquip exclusive = new StructExclusiveEquip();
        int exId = 100;
        exclusive.msg = "只能佩戴一个\r\n老公老婆的戒指。";
        for (int i = 1112446; i <= 1112495; i++)
        {
            exclusive.itemIDs.add(i);
            this.exclusiveEquip.put(i, exId);
        }
        this.exclusiveEquipInfo.put(exId, exclusive);

        exclusive = new StructExclusiveEquip();
        exId = 101;
        exclusive.msg = "只能佩戴一个\r\n不速之客的戒指。";
        for (int i = 1112435; i <= 1112439; i++)
        {
            exclusive.itemIDs.add(i);
            this.exclusiveEquip.put(i, exId);
        }
        this.exclusiveEquipInfo.put(exId, exclusive);

        exclusive = new StructExclusiveEquip();
        exId = 102;
        exclusive.msg = "只能佩戴一个\r\n十字旅团的戒指。";
        for (int i = 1112599; i <= 1112613; i++)
        {
            exclusive.itemIDs.add(i);
            this.exclusiveEquip.put(i, exId);
        }
        this.exclusiveEquipInfo.put(exId, exclusive);
    }

    public void runItems()
    {
        if (constants.GameConstants.GMS)
        {
            MapleData fData = this.etcData.getData("FamiliarInfo.img");
            for (MapleData dd : fData)
            {
                StructFamiliar f = new StructFamiliar();
                f.grade = 0;
                f.mob = MapleDataTool.getInt("mob", dd, 0);
                f.passive = MapleDataTool.getInt("passive", dd, 0);
                f.itemid = MapleDataTool.getInt("consume", dd, 0);
                f.familiar = Integer.parseInt(dd.getName());
                this.familiars.put(f.familiar, f);
                this.familiars_Item.put(f.itemid, f);
                this.familiars_Mob.put(f.mob, f);
            }
            MapleDataDirectoryEntry e = (MapleDataDirectoryEntry) this.chrData.getRoot().getEntry("Familiar");
            for (provider.MapleDataFileEntry mapleDataFileEntry : e.getFiles())
            {
                MapleDataEntry MDEd = mapleDataFileEntry;
                int id = Integer.parseInt(MDEd.getName().substring(0, MDEd.getName().length() - 4));
                if (this.familiars.containsKey(id))
                {
                    this.familiars.get(id).grade = ((byte) MapleDataTool.getInt("grade", this.chrData.getData("Familiar/" + MDEd.getName()).getChildByPath("info"), 0));
                }
            }

            MapleData mSetsData = this.etcData.getData("MonsterBookSet.img");
            for (MapleData d : mSetsData.getChildByPath("setList"))
            {
                if (MapleDataTool.getInt("deactivated", d, 0) <= 0)
                {

                    List<Integer> set = new ArrayList<>();
                    List<Integer> potential = new ArrayList(3);
                    for (MapleData ds : d.getChildByPath("stats/potential"))
                    {
                        if ((ds.getType() != provider.MapleDataType.STRING) && (MapleDataTool.getInt(ds, 0) > 0))
                        {
                            potential.add(MapleDataTool.getInt(ds, 0));
                            if (potential.size() >= 5)
                            {
                                break;
                            }
                        }
                    }
                    for (MapleData ds : d.getChildByPath("cardList"))
                    {
                        set.add(MapleDataTool.getInt(ds, 0));
                    }
                    this.monsterBookSets.put(Integer.parseInt(d.getName()), new Triple(MapleDataTool.getInt("setScore", d, 0), set, potential));
                }
            }
        }
        try
        {
            java.sql.Connection con = database.DatabaseConnectionWZ.getConnection();
            java.sql.PreparedStatement ps = con.prepareStatement("SELECT * FROM wz_itemdata");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                initItemInformation(rs);
            }
            rs.close();
            ps.close();


            ps = con.prepareStatement("SELECT * FROM wz_itemequipdata ORDER BY itemid");
            rs = ps.executeQuery();
            while (rs.next())
            {
                initItemEquipData(rs);
            }
            rs.close();
            ps.close();


            ps = con.prepareStatement("SELECT * FROM wz_itemadddata ORDER BY itemid");
            rs = ps.executeQuery();
            while (rs.next())
            {
                initItemAddData(rs);
            }
            rs.close();
            ps.close();


            ps = con.prepareStatement("SELECT * FROM wz_itemrewarddata ORDER BY itemid");
            rs = ps.executeQuery();
            while (rs.next())
            {
                initItemRewardData(rs);
            }
            rs.close();
            ps.close();


            for (Map.Entry<Integer, ItemInformation> entry : this.dataCache.entrySet())
            {
                if (ItemConstants.getInventoryType(entry.getKey()) == client.inventory.MapleInventoryType.EQUIP)
                {
                    finalizeEquipData(entry.getValue());
                }
            }
        }
        catch (java.sql.SQLException ex)
        {
            System.out.println("[ItemLoader] 加载装备数据出错." + ex);
        }
        System.out.println("共加载 " + this.dataCache.size() + " 个道具信息.");
    }

    public void initItemInformation(ResultSet sqlItemData) throws java.sql.SQLException
    {
        ItemInformation ret = new ItemInformation();
        int itemId = sqlItemData.getInt("itemid");
        ret.itemId = itemId;
        ret.slotMax = (ItemConstants.getSlotMax(itemId) > 0 ? ItemConstants.getSlotMax(itemId) : sqlItemData.getShort("slotMax"));
        ret.price = Double.parseDouble(sqlItemData.getString("price"));
        ret.wholePrice = sqlItemData.getInt("wholePrice");
        ret.stateChange = sqlItemData.getInt("stateChange");
        ret.name = sqlItemData.getString("name");
        ret.desc = sqlItemData.getString("desc");
        ret.msg = sqlItemData.getString("msg");

        ret.flag = sqlItemData.getInt("flags");

        ret.karmaEnabled = sqlItemData.getByte("karma");
        ret.meso = sqlItemData.getInt("meso");
        ret.monsterBook = sqlItemData.getInt("monsterBook");
        ret.itemMakeLevel = sqlItemData.getShort("itemMakeLevel");
        ret.questId = sqlItemData.getInt("questId");
        ret.create = sqlItemData.getInt("create");
        ret.replaceItem = sqlItemData.getInt("replaceId");
        ret.replaceMsg = sqlItemData.getString("replaceMsg");
        ret.afterImage = sqlItemData.getString("afterImage");
        ret.cardSet = 0;
        if ((ret.monsterBook > 0) && (itemId / 10000 == 238))
        {
            this.mobIds.put(ret.monsterBook, itemId);
            for (Map.Entry<Integer, Triple<Integer, List<Integer>, List<Integer>>> set : this.monsterBookSets.entrySet())
            {
                if (set.getValue().mid.contains(itemId))
                {
                    ret.cardSet = set.getKey();
                    break;
                }
            }
        }

        String scrollRq = sqlItemData.getString("scrollReqs");
        String s;
        if (scrollRq.length() > 0)
        {
            ret.scrollReqs = new ArrayList<>();
            String[] scroll = scrollRq.split(",");
            String[] arrayOfString1 = scroll;
            int i = arrayOfString1.length;
            for (String value : arrayOfString1)
            {
                s = value;
                if (s.length() > 1)
                {
                    ret.scrollReqs.add(Integer.parseInt(s));
                }
            }
        }

        String consumeItem = sqlItemData.getString("consumeItem");
        if (consumeItem.length() > 0)
        {
            ret.questItems = new ArrayList<>();
            int localObject = scrollRq.split(",").length;
            for (int k = 0; k < localObject; k++)
            {
                s = scrollRq.split(",")[k];
                if (s.length() > 1)
                {
                    ret.questItems.add(Integer.parseInt(s));
                }
            }
        }

        ret.totalprob = sqlItemData.getInt("totalprob");

        String incRq = sqlItemData.getString("incSkill");
        if (incRq.length() > 0)
        {
            ret.incSkill = new ArrayList<>();
            String[] scroll = incRq.split(",");
            for (String value : scroll)
            {
                s = value;
                if (s.length() > 1)
                {
                    ret.incSkill.add(Integer.parseInt(s));
                }
            }
        }
        this.dataCache.put(itemId, ret);
    }

    public void initItemEquipData(ResultSet sqlEquipData) throws java.sql.SQLException
    {
        int itemID = sqlEquipData.getInt("itemid");
        if ((this.tmpInfo == null) || (this.tmpInfo.itemId != itemID))
        {
            if (!this.dataCache.containsKey(itemID))
            {
                System.out.println("[initItemEquipData] Tried to load an item while this is not in the cache: " + itemID);
                return;
            }
            this.tmpInfo = this.dataCache.get(itemID);
        }

        if (this.tmpInfo.equipStats == null)
        {
            this.tmpInfo.equipStats = new HashMap<>();
        }

        int itemLevel = sqlEquipData.getInt("itemLevel");
        if (itemLevel == -1)
        {
            this.tmpInfo.equipStats.put(sqlEquipData.getString("key"), sqlEquipData.getInt("value"));
        }
        else
        {
            if (this.tmpInfo.equipIncs == null)
            {
                this.tmpInfo.equipIncs = new HashMap<>();
            }
            Map<String, Integer> toAdd = this.tmpInfo.equipIncs.get(itemLevel);
            if (toAdd == null)
            {
                toAdd = new HashMap<>();
                this.tmpInfo.equipIncs.put(itemLevel, toAdd);
            }
            toAdd.put(sqlEquipData.getString("key"), sqlEquipData.getInt("value"));
        }
    }

    public void initItemAddData(ResultSet sqlAddData) throws java.sql.SQLException
    {
        int itemID = sqlAddData.getInt("itemid");
        if ((this.tmpInfo == null) || (this.tmpInfo.itemId != itemID))
        {
            if (!this.dataCache.containsKey(itemID))
            {
                System.out.println("[initItemAddData] Tried to load an item while this is not in the cache: " + itemID);
                return;
            }
            this.tmpInfo = this.dataCache.get(itemID);
        }

        if (this.tmpInfo.equipAdditions == null)
        {
            this.tmpInfo.equipAdditions = new LinkedList<>();
        }
        this.tmpInfo.equipAdditions.add(new Triple(sqlAddData.getString("key"), sqlAddData.getString("subKey"), sqlAddData.getString("value")));
    }

    public void initItemRewardData(ResultSet sqlRewardData) throws java.sql.SQLException
    {
        int itemID = sqlRewardData.getInt("itemid");
        if ((this.tmpInfo == null) || (this.tmpInfo.itemId != itemID))
        {
            if (!this.dataCache.containsKey(itemID))
            {
                System.out.println("[initItemRewardData] Tried to load an item while this is not in the cache: " + itemID);
                return;
            }
            this.tmpInfo = this.dataCache.get(itemID);
        }

        if (this.tmpInfo.rewardItems == null)
        {
            this.tmpInfo.rewardItems = new ArrayList<>();
        }

        StructRewardItem add = new StructRewardItem();
        add.itemid = sqlRewardData.getInt("item");
        add.period = (add.itemid == 1122017 ? Math.max(sqlRewardData.getInt("period"), 7200) : sqlRewardData.getInt("period"));


        add.prob = (add.itemid == 2511117 ? 3 : sqlRewardData.getInt("prob"));
        add.quantity = sqlRewardData.getShort("quantity");
        add.worldmsg = (sqlRewardData.getString("worldMsg").length() <= 0 ? null : sqlRewardData.getString("worldMsg"));
        add.effect = sqlRewardData.getString("effect");


        this.tmpInfo.rewardItems.add(add);
    }

    public void finalizeEquipData(ItemInformation item)
    {
        int itemId = item.itemId;


        if (item.equipStats == null)
        {
            item.equipStats = new HashMap<>();
        }

        item.eq = new Equip(itemId, (short) 0, -1, (short) 0);
        short stats = ItemConstants.getStat(itemId, 0);
        if (stats > 0)
        {
            item.eq.setStr(stats);
            item.eq.setDex(stats);
            item.eq.setInt(stats);
            item.eq.setLuk(stats);
        }
        stats = ItemConstants.getATK(itemId, 0);
        if (stats > 0)
        {
            item.eq.setWatk(stats);
            item.eq.setMatk(stats);
        }
        stats = ItemConstants.getHpMp(itemId, 0);
        if (stats > 0)
        {
            item.eq.setHp(stats);
            item.eq.setMp(stats);
        }
        stats = ItemConstants.getDEF(itemId, 0);
        if (stats > 0)
        {
            item.eq.setWdef(stats);
            item.eq.setMdef(stats);
        }
        if (item.equipStats.size() > 0)
        {
            for (Map.Entry<String, Integer> stat : item.equipStats.entrySet())
            {
                String key = stat.getKey();
                if (key.equals("STR"))
                {
                    item.eq.setStr(ItemConstants.getStat(itemId, stat.getValue()));
                }
                else if (key.equals("DEX"))
                {
                    item.eq.setDex(ItemConstants.getStat(itemId, stat.getValue()));
                }
                else if (key.equals("INT"))
                {
                    item.eq.setInt(ItemConstants.getStat(itemId, stat.getValue()));
                }
                else if (key.equals("LUK"))
                {
                    item.eq.setLuk(ItemConstants.getStat(itemId, stat.getValue()));
                }
                else if (key.equals("PAD"))
                {
                    item.eq.setWatk(ItemConstants.getATK(itemId, stat.getValue()));
                }
                else if (key.equals("PDD"))
                {
                    item.eq.setWdef(ItemConstants.getDEF(itemId, stat.getValue()));
                }
                else if (key.equals("MAD"))
                {
                    item.eq.setMatk(ItemConstants.getATK(itemId, stat.getValue()));
                }
                else if (key.equals("MDD"))
                {
                    item.eq.setMdef(ItemConstants.getDEF(itemId, stat.getValue()));
                }
                else if (key.equals("ACC"))
                {
                    item.eq.setAcc((short) stat.getValue().intValue());
                }
                else if (key.equals("EVA"))
                {
                    item.eq.setAvoid((short) stat.getValue().intValue());
                }
                else if (key.equals("Speed"))
                {
                    item.eq.setSpeed((short) stat.getValue().intValue());
                }
                else if (key.equals("Jump"))
                {
                    item.eq.setJump((short) stat.getValue().intValue());
                }
                else if (key.equals("MHP"))
                {
                    item.eq.setHp(ItemConstants.getHpMp(itemId, stat.getValue()));
                }
                else if (key.equals("MMP"))
                {
                    item.eq.setMp(ItemConstants.getHpMp(itemId, stat.getValue()));
                }
                else if (key.equals("tuc"))
                {
                    item.eq.setUpgradeSlots(stat.getValue().byteValue());
                }
                else if (key.equals("Craft"))
                {
                    item.eq.setHands(stat.getValue().shortValue());
                }
                else if (key.equals("durability"))
                {
                    item.eq.setDurability(stat.getValue());
                }
                else if (key.equals("charmEXP"))
                {
                    item.eq.setCharmEXP(stat.getValue().shortValue());
                }
                else if (key.equals("PVPDamage"))
                {
                    item.eq.setPVPDamage(stat.getValue().shortValue());
                }
                else if (key.equals("bdR"))
                {
                    item.eq.setBossDamage(stat.getValue().shortValue());
                }
                else if (key.equals("imdR"))
                {
                    item.eq.setIgnorePDR(stat.getValue().shortValue());
                }
            }
            if ((item.equipStats.get("cash") != null) && (item.eq.getCharmEXP() <= 0))
            {
                short exp = 0;
                int identifier = itemId / 10000;
                if ((ItemConstants.isWeapon(itemId)) || (identifier == 106))
                {
                    exp = 60;
                }
                else if (identifier == 100)
                {
                    exp = 50;
                }
                else if ((ItemConstants.isAccessory(itemId)) || (identifier == 102) || (identifier == 108) || (identifier == 107))
                {
                    exp = 40;
                }
                else if ((identifier == 104) || (identifier == 105) || (identifier == 110))
                {
                    exp = 30;
                }
                item.eq.setCharmEXP(exp);
            }
        }
    }

    public Map<Integer, List<StructItemOption>> getAllPotentialInfo()
    {
        return this.potentialCache;
    }

    public Map<Integer, List<StructItemOption>> getPotentialInfos(int potId)
    {
        Map<Integer, List<StructItemOption>> ret = new HashMap<>();
        for (Map.Entry<Integer, List<StructItemOption>> pots : this.potentialCache.entrySet())
        {
            if (pots.getKey() >= potId)
            {
                ret.put(pots.getKey(), pots.getValue());
            }
        }
        return ret;
    }

    public String resolvePotentialId(int itemId, int potId)
    {
        int eqLevel = getReqLevel(itemId);

        List<StructItemOption> potInfo = getPotentialInfo(potId);
        int potLevel;
        if (eqLevel == 0)
        {
            potLevel = 1;
        }
        else
        {
            potLevel = (eqLevel + 1) / 10;
            potLevel++;
        }
        if (potId <= 0)
        {
            return "没有潜能属性";
        }
        StructItemOption itemOption = potInfo.get(potLevel - 1);
        String ret = itemOption.opString;
        for (int i = 0; i < itemOption.opString.length(); i++)
        {
            if (itemOption.opString.charAt(i) == '#')
            {
                int j = i + 2;
                while ((j < itemOption.opString.length()) && (itemOption.opString.substring(i + 1, j).matches("^(?!_)(?!.*?_$)[a-zA-Z0-9_一-龥]+$")))
                {
                    j++;
                }
                String curParam = itemOption.opString.substring(i, j);
                String curParamStripped;
                if ((j != itemOption.opString.length()) || (itemOption.opString.charAt(itemOption.opString.length() - 1) == '%'))
                {
                    curParamStripped = curParam.substring(1, curParam.length() - 1);
                }
                else
                {
                    curParamStripped = curParam.substring(1);
                }
                String paramValue = Integer.toString(itemOption.get(curParamStripped));
                if (curParam.charAt(curParam.length() - 1) == '%')
                {
                    paramValue = paramValue.concat("%");
                }
                ret = ret.replace(curParam, paramValue);
            }
        }
        return ret;
    }

    public int getReqLevel(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("reqLevel")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("reqLevel");
    }

    public List<StructItemOption> getPotentialInfo(int potId)
    {
        return this.potentialCache.get(potId);
    }

    public Map<String, Integer> getEquipStats(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.equipStats;
    }

    public ItemInformation getItemInformation(int itemId)
    {
        if (itemId <= 0)
        {
            return null;
        }
        return this.dataCache.get(itemId);
    }

    public StructItemOption getSocketInfo(int socketId)
    {
        int grade = ItemConstants.getNebuliteGrade(socketId);
        if (grade == -1)
        {
            return null;
        }
        return (StructItemOption) ((Map) this.socketCache.get(grade)).get(socketId);
    }

    public Map<Integer, StructItemOption> getAllSocketInfo(int grade)
    {
        return this.socketCache.get(grade);
    }

    public java.util.Collection<Integer> getMonsterBookList()
    {
        return this.mobIds.values();
    }

    public Map<Integer, Integer> getMonsterBook()
    {
        return this.mobIds;
    }

    public Pair<Integer, Integer> getPot(int f)
    {
        return this.potLife.get(f);
    }

    public StructFamiliar getFamiliar(int f)
    {
        return this.familiars.get(f);
    }

    public Map<Integer, StructFamiliar> getFamiliars()
    {
        return this.familiars;
    }

    public StructFamiliar getFamiliarByItem(int f)
    {
        return this.familiars_Item.get(f);
    }

    public StructFamiliar getFamiliarByMob(int f)
    {
        return this.familiars_Mob.get(f);
    }

    public java.util.Collection<ItemInformation> getAllItems()
    {
        return this.dataCache.values();
    }

    public StructAndroid getAndroidInfo(int i)
    {
        return this.androidInfo.get(i);
    }

    public Triple<Integer, List<Integer>, List<Integer>> getMonsterBookInfo(int i)
    {
        return this.monsterBookSets.get(i);
    }

    public Map<Integer, Triple<Integer, List<Integer>, List<Integer>>> getAllMonsterBookInfo()
    {
        return this.monsterBookSets;
    }

    public Integer getItemIdByMob(int mobId)
    {
        return this.mobIds.get(mobId);
    }

    public Integer getSetId(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.cardSet;
    }

    public short getSlotMax(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return 0;
        }
        return i.slotMax;
    }

    public int getWholePrice(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return 0;
        }
        return i.wholePrice;
    }

    public double getPrice(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return -1.0D;
        }
        return i.price;
    }

    public Equip levelUpEquip(Equip equip, Map<String, Integer> sta)
    {
        Equip nEquip = (Equip) equip.copy();
        try
        {
            for (Map.Entry<String, Integer> stat : sta.entrySet())
            {
                if (stat.getKey().equals("STRMin"))
                {
                    nEquip.setStr((short) (nEquip.getStr() + rand(stat.getValue(), sta.get("STRMax"))));
                }
                else if (stat.getKey().equals("DEXMin"))
                {
                    nEquip.setDex((short) (nEquip.getDex() + rand(stat.getValue(), sta.get("DEXMax"))));
                }
                else if (stat.getKey().equals("INTMin"))
                {
                    nEquip.setInt((short) (nEquip.getInt() + rand(stat.getValue(), sta.get("INTMax"))));
                }
                else if (stat.getKey().equals("LUKMin"))
                {
                    nEquip.setLuk((short) (nEquip.getLuk() + rand(stat.getValue(), sta.get("LUKMax"))));
                }
                else if (stat.getKey().equals("PADMin"))
                {
                    nEquip.setWatk((short) (nEquip.getWatk() + rand(stat.getValue(), sta.get("PADMax"))));
                }
                else if (stat.getKey().equals("PDDMin"))
                {
                    nEquip.setWdef((short) (nEquip.getWdef() + rand(stat.getValue(), sta.get("PDDMax"))));
                }
                else if (stat.getKey().equals("MADMin"))
                {
                    nEquip.setMatk((short) (nEquip.getMatk() + rand(stat.getValue(), sta.get("MADMax"))));
                }
                else if (stat.getKey().equals("MDDMin"))
                {
                    nEquip.setMdef((short) (nEquip.getMdef() + rand(stat.getValue(), sta.get("MDDMax"))));
                }
                else if (stat.getKey().equals("ACCMin"))
                {
                    nEquip.setAcc((short) (nEquip.getAcc() + rand(stat.getValue(), sta.get("ACCMax"))));
                }
                else if (stat.getKey().equals("EVAMin"))
                {
                    nEquip.setAvoid((short) (nEquip.getAvoid() + rand(stat.getValue(), sta.get("EVAMax"))));
                }
                else if (stat.getKey().equals("SpeedMin"))
                {
                    nEquip.setSpeed((short) (nEquip.getSpeed() + rand(stat.getValue(), sta.get("SpeedMax"))));
                }
                else if (stat.getKey().equals("JumpMin"))
                {
                    nEquip.setJump((short) (nEquip.getJump() + rand(stat.getValue(), sta.get("JumpMax"))));
                }
                else if (stat.getKey().equals("MHPMin"))
                {
                    nEquip.setHp((short) (nEquip.getHp() + rand(stat.getValue(), sta.get("MHPMax"))));
                }
                else if (stat.getKey().equals("MMPMin"))
                {
                    nEquip.setMp((short) (nEquip.getMp() + rand(stat.getValue(), sta.get("MMPMax"))));
                }
                else if (stat.getKey().equals("MaxHPMin"))
                {
                    nEquip.setHp((short) (nEquip.getHp() + rand(stat.getValue(), sta.get("MaxHPMax"))));
                }
                else if (stat.getKey().equals("MaxMPMin"))
                {
                    nEquip.setMp((short) (nEquip.getMp() + rand(stat.getValue(), sta.get("MaxMPMax"))));
                }
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        return nEquip;
    }

    protected int rand(int min, int max)
    {
        return Math.abs(Randomizer.rand(min, max));
    }

    public List<Triple<String, String, String>> getEquipAdditions(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.equipAdditions;
    }

    public String getEquipAddReqs(int itemId, String key, String sub)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        for (Triple<String, String, String> data : i.equipAdditions)
        {
            if ((data.getLeft().equals("key")) && (data.getMid().equals("con:" + sub)))
            {
                return data.getRight();
            }
        }
        return null;
    }

    public Map<Integer, Map<String, Integer>> getEquipIncrements(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.equipIncs;
    }

    public List<Integer> getEquipSkills(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.incSkill;
    }

    public boolean canEquip(Map<String, Integer> stats, int itemid, int level, int job, int fame, int str, int dex, int luk, int int_, int supremacy)
    {
        if (level + supremacy >= (stats.containsKey("reqLevel") ? stats.get("reqLevel") : 0)) if (str >= (stats.containsKey("reqSTR") ? stats.get("reqSTR") : 0))
            if (dex >= (stats.containsKey("reqDEX") ? stats.get("reqDEX") : 0))
                if (luk >= (stats.containsKey("reqLUK") ? stats.get("reqLUK") : 0)) if (int_ >= (stats.containsKey("reqINT") ? stats.get("reqINT") : 0))
                {
                    Integer fameReq = stats.get("reqPOP");
                    return (fameReq == null) || (fame >= fameReq);
                }
        if (level + supremacy >= (stats.containsKey("reqLevel") ? stats.get("reqLevel") : 0)) if (constants.GameConstants.is恶魔复仇者(job)) return true;
        if (level + supremacy >= (stats.containsKey("reqLevel") ? stats.get("reqLevel") : 0)) if (constants.GameConstants.is尖兵(job))
        {
            int jobtype = stats.containsKey("reqJob") ? stats.get("reqJob") : 0;
            return (jobtype == 0) || (jobtype == 8) || (jobtype == 16) || (jobtype == 24);
        }
        return false;
    }

    public int getReqJob(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("reqJob")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("reqJob");
    }

    public int getSlots(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("tuc")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("tuc");
    }

    public Integer getSetItemID(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("setItemID")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("setItemID");
    }

    public StructSetItem getSetItem(int setItemId)
    {
        return this.SetItemInfo.get(setItemId);
    }

    public List<Integer> getScrollReqs(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.scrollReqs;
    }

    public int getScrollSuccess(int itemId)
    {
        if ((itemId / 10000 != 204) || (getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("success")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("success");
    }

    public Item scrollEquipWithId(Item equip, Item scroll, boolean whiteScroll, MapleCharacter chr, int vegas)
    {
        if (equip.getType() == 1)
        {
            int scrollId = scroll.getItemId();
            if (ItemConstants.isEquipScroll(scrollId)) return scrollEnhance(equip, scroll, chr);
            if (ItemConstants.isPotentialScroll(scrollId)) return scrollPotential(equip, scroll, chr);
            if (ItemConstants.isPotentialAddScroll(scrollId)) return scrollPotentialAdd(equip, scroll, chr);
            if (ItemConstants.isLimitBreakScroll(scrollId)) return scrollLimitBreak(equip, scroll, chr);
            if (ItemConstants.isResetScroll(scrollId))
            {
                return scrollResetEquip(equip, scroll, chr);
            }
            Equip nEquip = (Equip) equip;
            Map<String, Integer> scrollStats = getEquipStats(scrollId);
            Map<String, Integer> equipStats = getEquipStats(equip.getItemId());

            int succ = (scrollStats == null) || (!scrollStats.containsKey("success")) ? 0 : ItemConstants.isTablet(scrollId) ? ItemConstants.getSuccessTablet(scrollId, nEquip.getLevel()) :
                    scrollStats.get("success");

            int curse = (scrollStats == null) || (!scrollStats.containsKey("cursed")) ? 0 : ItemConstants.isTablet(scrollId) ? ItemConstants.getCurseTablet(scrollId, nEquip.getLevel()) :
                    scrollStats.get("cursed");

            int craft = ItemConstants.isCleanSlate(scrollId) ? 0 : chr.getTrait(client.MapleTraitType.craft).getLevel() / 10;

            int lucksKey = ItemFlag.幸运卷轴.check(equip.getFlag()) ? 10 : 0;
            int success = succ + lucksKey + craft + getSuccessRates(scroll.getItemId());
            if (chr.isAdmin())
            {
                chr.dropSpouseMessage(11, "普通卷轴 - 默认几率: " + succ + "% 倾向加成: " + craft + "% 幸运状态加成: " + lucksKey + "% 最终概率: " + success + "% 失败消失几率: " + curse + "%");
            }
            if ((ItemFlag.幸运卷轴.check(equip.getFlag())) && (!ItemConstants.isSpecialScroll(scrollId)))
            {
                equip.setFlag((short) (equip.getFlag() - ItemFlag.幸运卷轴.getValue()));
            }
            if ((ItemConstants.isSpecialScroll(scrollId)) || (Randomizer.nextInt(100) <= success))
            {
                int stat;
                short flag;
                switch (scrollId)
                {
                    case 2049000:
                    case 2049001:
                    case 2049002:
                    case 2049003:
                    case 2049004:
                    case 2049005:
                        if ((equipStats.containsKey("tuc")) && (nEquip.getLevel() + nEquip.getUpgradeSlots() < equipStats.get("tuc") + nEquip.getViciousHammer()))
                        {
                            nEquip.setUpgradeSlots((byte) (nEquip.getUpgradeSlots() + 1));
                        }

                        break;
                    case 2049006:
                    case 2049007:
                    case 2049008:
                        if ((equipStats.containsKey("tuc")) && (nEquip.getLevel() + nEquip.getUpgradeSlots() < equipStats.get("tuc") + nEquip.getViciousHammer()))
                        {
                            nEquip.setUpgradeSlots((byte) (nEquip.getUpgradeSlots() + 2));
                        }

                        break;
                    case 2040727:
                        flag = nEquip.getFlag();
                        flag = (short) (flag | ItemFlag.鞋子防滑.getValue());
                        nEquip.setFlag(flag);
                        break;

                    case 2041058:
                        flag = nEquip.getFlag();
                        flag = (short) (flag | ItemFlag.披风防寒.getValue());
                        nEquip.setFlag(flag);
                        break;

                    case 2530000:
                    case 2530001:
                    case 5063100:
                        flag = nEquip.getFlag();
                        flag = (short) (flag | ItemFlag.幸运卷轴.getValue());
                        nEquip.setFlag(flag);
                        break;

                    case 2531000:
                    case 5064000:
                    case 5064003:
                        flag = nEquip.getFlag();
                        flag = (short) (flag | ItemFlag.装备防爆.getValue());
                        nEquip.setFlag(flag);
                        break;

                    case 5064100:
                    case 5068100:
                        flag = nEquip.getFlag();
                        flag = (short) (flag | ItemFlag.保护升级次数.getValue());
                        nEquip.setFlag(flag);
                        break;

                    case 5064300:
                    case 5068200:
                        flag = nEquip.getFlag();
                        flag = (short) (flag | ItemFlag.卷轴防护.getValue());
                        nEquip.setFlag(flag);
                        break;

                    default:
                        if (ItemConstants.isChaosScroll(scrollId))
                        {
                            stat = ItemConstants.getChaosNumber(scrollId);
                            int increase = Randomizer.nextBoolean() ? 1 : (ItemConstants.isChaosForGoodness(scrollId)) || (isNegativeScroll(scrollId)) ? 1 : -1;
                            if (nEquip.getStr() > 0)
                            {
                                nEquip.setStr((short) (nEquip.getStr() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getDex() > 0)
                            {
                                nEquip.setDex((short) (nEquip.getDex() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getInt() > 0)
                            {
                                nEquip.setInt((short) (nEquip.getInt() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getLuk() > 0)
                            {
                                nEquip.setLuk((short) (nEquip.getLuk() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getWatk() > 0)
                            {
                                nEquip.setWatk((short) (nEquip.getWatk() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getWdef() > 0)
                            {
                                nEquip.setWdef((short) (nEquip.getWdef() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getMatk() > 0)
                            {
                                nEquip.setMatk((short) (nEquip.getMatk() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getMdef() > 0)
                            {
                                nEquip.setMdef((short) (nEquip.getMdef() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getAcc() > 0)
                            {
                                nEquip.setAcc((short) (nEquip.getAcc() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getAvoid() > 0)
                            {
                                nEquip.setAvoid((short) (nEquip.getAvoid() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getSpeed() > 0)
                            {
                                nEquip.setSpeed((short) (nEquip.getSpeed() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getJump() > 0)
                            {
                                nEquip.setJump((short) (nEquip.getJump() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getHp() > 0)
                            {
                                nEquip.setHp((short) (nEquip.getHp() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                            if (nEquip.getMp() > 0)
                            {
                                nEquip.setMp((short) (nEquip.getMp() + (Randomizer.nextInt(stat) + 1) * increase));
                            }
                        }
                        else
                        {
                            for (Map.Entry<String, Integer> statt : scrollStats.entrySet())
                            {
                                String key = statt.getKey();
                                if (key.equals("STR"))
                                {
                                    nEquip.setStr((short) (nEquip.getStr() + statt.getValue()));
                                }
                                else if (key.equals("DEX"))
                                {
                                    nEquip.setDex((short) (nEquip.getDex() + statt.getValue()));
                                }
                                else if (key.equals("INT"))
                                {
                                    nEquip.setInt((short) (nEquip.getInt() + statt.getValue()));
                                }
                                else if (key.equals("LUK"))
                                {
                                    nEquip.setLuk((short) (nEquip.getLuk() + statt.getValue()));
                                }
                                else if (key.equals("PAD"))
                                {
                                    nEquip.setWatk((short) (nEquip.getWatk() + statt.getValue()));
                                }
                                else if (key.equals("PDD"))
                                {
                                    nEquip.setWdef((short) (nEquip.getWdef() + statt.getValue()));
                                }
                                else if (key.equals("MAD"))
                                {
                                    nEquip.setMatk((short) (nEquip.getMatk() + statt.getValue()));
                                }
                                else if (key.equals("MDD"))
                                {
                                    nEquip.setMdef((short) (nEquip.getMdef() + statt.getValue()));
                                }
                                else if (key.equals("ACC"))
                                {
                                    nEquip.setAcc((short) (nEquip.getAcc() + statt.getValue()));
                                }
                                else if (key.equals("EVA"))
                                {
                                    nEquip.setAvoid((short) (nEquip.getAvoid() + statt.getValue()));
                                }
                                else if (key.equals("Speed"))
                                {
                                    nEquip.setSpeed((short) (nEquip.getSpeed() + statt.getValue()));
                                }
                                else if (key.equals("Jump"))
                                {
                                    nEquip.setJump((short) (nEquip.getJump() + statt.getValue()));
                                }
                                else if (key.equals("MHP"))
                                {
                                    nEquip.setHp((short) (nEquip.getHp() + statt.getValue()));
                                }
                                else if (key.equals("MMP"))
                                {
                                    nEquip.setMp((short) (nEquip.getMp() + statt.getValue()));
                                }
                            }
                        }

                        break;
                }

                if ((!ItemConstants.isCleanSlate(scrollId)) && (!ItemConstants.isSpecialScroll(scrollId)))
                {
                    short oldFlag = nEquip.getFlag();
                    if (ItemFlag.保护升级次数.check(oldFlag))
                    {
                        nEquip.setFlag((short) (oldFlag - ItemFlag.保护升级次数.getValue()));
                    }
                    int scrollUseSlots = ItemConstants.isAzwanScroll(scrollId) ? getSlots(scrollId) : 1;
                    nEquip.setUpgradeSlots((byte) (nEquip.getUpgradeSlots() - scrollUseSlots));
                    nEquip.setLevel((byte) (nEquip.getLevel() + scrollUseSlots));
                }
            }
            else
            {
                if ((!whiteScroll) && (!ItemConstants.isCleanSlate(scrollId)) && (!ItemConstants.isSpecialScroll(scrollId)))
                {
                    short oldFlag = nEquip.getFlag();
                    if (ItemFlag.保护升级次数.check(oldFlag))
                    {
                        nEquip.setFlag((short) (oldFlag - ItemFlag.保护升级次数.getValue()));
                        chr.dropSpouseMessage(11, "由于卷轴的效果，升级次数没有减少。");
                    }
                    else
                    {
                        int scrollUseSlots = ItemConstants.isAzwanScroll(scrollId) ? getSlots(scrollId) : 1;
                        nEquip.setUpgradeSlots((byte) (nEquip.getUpgradeSlots() - scrollUseSlots));
                    }
                }
                if (Randomizer.nextInt(99) < curse)
                {
                    return null;
                }
            }
        }
        return equip;
    }

    public Item scrollEnhance(Item equip, Item scroll, MapleCharacter chr)
    {
        if (equip.getType() != 1)
        {
            return equip;
        }
        Equip nEquip = (Equip) equip;
        int scrollId = scroll.getItemId();
        Map<String, Integer> scrollStats = getEquipStats(scrollId);
        boolean noCursed = isNoCursedScroll(scrollId);
        int scrollForceUpgrade = getForceUpgrade(scrollId);
        int succ = (scrollStats == null) || (!scrollStats.containsKey("success")) ? 0 : scrollStats.get("success");
        int curse = (scrollStats == null) || (!scrollStats.containsKey("cursed")) ? 100 : noCursed ? 0 : scrollStats.get("cursed");
        int craft = chr.getTrait(client.MapleTraitType.craft).getLevel() / 10;
        if ((scrollForceUpgrade == 1) && (succ == 0))
        {
            succ = Math.max(((scroll.getItemId() == 2049301) || (scroll.getItemId() == 2049307) ? 80 : 100) - nEquip.getEnhance() * 10, 5);
        }
        int success = succ + craft;
        if (chr.isAdmin())
        {
            chr.dropSpouseMessage(11, "装备强化卷轴 - 默认几率: " + succ + "% 倾向加成: " + craft + "% 最终几率: " + success + "% 失败消失几率: " + curse + "% 卷轴是否失败不消失装备: " + noCursed);
        }
        if (Randomizer.nextInt(100) > success)
        {
            return Randomizer.nextInt(99) < curse ? null : nEquip;
        }
        int mixStats = isSuperiorEquip(nEquip.getItemId()) ? 3 : 0;
        int maxStats = isSuperiorEquip(nEquip.getItemId()) ? 8 : 5;
        for (int i = 0; i < scrollForceUpgrade; i++)
        {
            if ((nEquip.getStr() > 0) || (Randomizer.nextInt(50) == 1))
            {
                nEquip.setStr((short) (nEquip.getStr() + Randomizer.rand(mixStats, maxStats)));
            }
            if ((nEquip.getDex() > 0) || (Randomizer.nextInt(50) == 1))
            {
                nEquip.setDex((short) (nEquip.getDex() + Randomizer.rand(mixStats, maxStats)));
            }
            if ((nEquip.getInt() > 0) || (Randomizer.nextInt(50) == 1))
            {
                nEquip.setInt((short) (nEquip.getInt() + Randomizer.rand(mixStats, maxStats)));
            }
            if ((nEquip.getLuk() > 0) || (Randomizer.nextInt(50) == 1))
            {
                nEquip.setLuk((short) (nEquip.getLuk() + Randomizer.rand(mixStats, maxStats)));
            }
            if ((nEquip.getWatk() > 0) && (ItemConstants.isWeapon(nEquip.getItemId())))
            {
                if (nEquip.getWatk() < 150)
                {
                    nEquip.setWatk((short) (nEquip.getWatk() + 3));
                }
                else if (nEquip.getWatk() < 200)
                {
                    nEquip.setWatk((short) (nEquip.getWatk() + 4));
                }
                else if (nEquip.getWatk() < 250)
                {
                    nEquip.setWatk((short) (nEquip.getWatk() + 5));
                }
                else
                {
                    nEquip.setWatk((short) (nEquip.getWatk() + 5 + (Randomizer.nextBoolean() ? 1 : 0)));
                }
            }
            if ((nEquip.getWdef() > 0) || (Randomizer.nextInt(40) == 1))
            {
                nEquip.setWdef((short) (nEquip.getWdef() + Randomizer.nextInt(5)));
            }
            if ((nEquip.getMatk() > 0) && (ItemConstants.isWeapon(nEquip.getItemId())))
            {
                if (nEquip.getMatk() < 150)
                {
                    nEquip.setMatk((short) (nEquip.getMatk() + 3));
                }
                else if (nEquip.getMatk() < 200)
                {
                    nEquip.setMatk((short) (nEquip.getMatk() + 4));
                }
                else if (nEquip.getMatk() < 250)
                {
                    nEquip.setMatk((short) (nEquip.getMatk() + 5));
                }
                else
                {
                    nEquip.setMatk((short) (nEquip.getMatk() + 5 + (Randomizer.nextBoolean() ? 1 : 0)));
                }
            }
            if ((nEquip.getMdef() > 0) || (Randomizer.nextInt(40) == 1))
            {
                nEquip.setMdef((short) (nEquip.getMdef() + Randomizer.nextInt(5)));
            }
            if ((nEquip.getAcc() > 0) || (Randomizer.nextInt(20) == 1))
            {
                nEquip.setAcc((short) (nEquip.getAcc() + Randomizer.nextInt(5)));
            }
            if ((nEquip.getAvoid() > 0) || (Randomizer.nextInt(20) == 1))
            {
                nEquip.setAvoid((short) (nEquip.getAvoid() + Randomizer.nextInt(5)));
            }
            if ((nEquip.getSpeed() > 0) || (Randomizer.nextInt(10) == 1))
            {
                nEquip.setSpeed((short) (nEquip.getSpeed() + Randomizer.nextInt(5)));
            }
            if ((nEquip.getJump() > 0) || (Randomizer.nextInt(10) == 1))
            {
                nEquip.setJump((short) (nEquip.getJump() + Randomizer.nextInt(5)));
            }
            if ((nEquip.getHp() > 0) || (Randomizer.nextInt(5) == 1))
            {
                nEquip.setHp((short) (nEquip.getHp() + Randomizer.rand(mixStats, maxStats)));
            }
            if ((nEquip.getMp() > 0) || (Randomizer.nextInt(5) == 1))
            {
                nEquip.setMp((short) (nEquip.getMp() + Randomizer.rand(mixStats, maxStats)));
            }
            nEquip.setEnhance((byte) (nEquip.getEnhance() + 1));
        }
        return nEquip;
    }

    public Item scrollPotential(Item equip, Item scroll, MapleCharacter chr)
    {
        if (equip.getType() != 1)
        {
            return equip;
        }
        Equip nEquip = (Equip) equip;
        int scrollId = scroll.getItemId();
        boolean noCursed = isNoCursedScroll(scrollId);
        switch (scrollId)
        {
            case 2049400:
            case 2049401:
            case 2049402:
            case 2049404:
            case 2049405:
            case 2049406:
            case 2049407:
            case 2049408:
            case 2049412:
            case 2049414:
            case 2049415:
            case 2049416:
            case 2049417:
            case 2049418:
            case 2049419:
            case 2049420:
                if (nEquip.getState() == 0)
                {
                    int success = (scrollId == 2049401) || (scrollId == 2049408) || (scrollId == 2049416) ? 70 : (scrollId == 2049400) || (scrollId == 2049407) || (scrollId == 2049412) ? 90 : 100;
                    if (chr.isAdmin())
                    {
                        chr.dropSpouseMessage(11, "潜能附加卷轴 - 砸卷几率: " + success + "%");
                    }
                    if (Randomizer.nextInt(100) > success)
                    {
                        return noCursed ? nEquip : null;
                    }
                    nEquip.resetPotential();
                }
                break;

            case 2049700:
            case 2049701:
            case 2049702:
            case 2049703:
            case 2049704:
            case 2049705:
            case 2049709:
                if (nEquip.getState() < 18)
                {
                    int success = (scrollId == 2049705) || (scrollId == 2049709) ? 50 : scrollId == 2049704 ? 40 : scrollId == 2049701 ? 80 : 100;
                    if (chr.isAdmin())
                    {
                        chr.dropSpouseMessage(11, "A级潜能卷轴 - 砸卷几率: " + success + "%");
                    }
                    if (Randomizer.nextInt(100) <= success)
                    {
                        nEquip.renewPotential(2);
                    }
                    else if (((scrollId == 2049701) && (Randomizer.nextInt(99) < 20)) || ((scrollId == 2049709) && (Randomizer.nextInt(99) < 50))) return null;
                }
                break;

            case 2049750:
            case 2049751:
            case 2049752:
            case 2049756:
            case 2049757:
            case 2049758:
                if (nEquip.getState() < 19)
                {
                    int success = (scrollId == 2049757) || (scrollId == 2049758) ? 50 : (scrollId == 2049752) || (scrollId == 2049756) ? 30 : scrollId == 2049704 ? 60 : scrollId == 2049750 ? 80 : 30;
                    if (chr.isAdmin())
                    {
                        chr.dropSpouseMessage(11, "S级潜能卷轴 - 砸卷几率: " + success + "%");
                    }
                    if (Randomizer.nextInt(100) <= success)
                    {
                        nEquip.renewPotential(4);
                    }
                }
                break;
        }
        return nEquip;
    }

    public Item scrollPotentialAdd(Item equip, Item scroll, MapleCharacter chr)
    {
        if (equip.getType() != 1)
        {
            return equip;
        }
        Equip nEquip = (Equip) equip;
        int scrollId = scroll.getItemId();
        switch (scrollId)
        {
            case 2048305:
            case 2048306:
            case 2048307:
            case 2048308:
            case 2048309:
            case 2048310:
                if (nEquip.getAddState() == 0)
                {
                    int success = (scrollId == 2048309) || (scrollId == 2048310) ? 60 : scrollId == 2048308 ? 50 : scrollId == 2048305 ? 70 : 100;
                    if (chr.isAdmin())
                    {
                        chr.dropSpouseMessage(11, "附加潜能附加卷轴 - 砸卷几率: " + success + "%");
                    }
                    if (Randomizer.nextInt(100) <= success)
                    {
                        nEquip.setPotential4(-1);
                    }
                    else if (((scrollId == 2048308) && (Randomizer.nextInt(99) < 50)) || (scrollId == 2048305) || (scrollId == 2048310))
                    {
                        return null;
                    }
                }
                break;
        }
        return nEquip;
    }

    public Item scrollLimitBreak(Item equip, Item scroll, MapleCharacter chr)
    {
        if (equip.getType() != 1)
        {
            return equip;
        }
        Equip nEquip = (Equip) equip;
        int scrollId = scroll.getItemId();
        Map<String, Integer> scrollStats = getEquipStats(scrollId);
        int succe = (scrollStats == null) || (!scrollStats.containsKey("success")) ? 0 : scrollStats.get("success");
        int craft = chr.getTrait(client.MapleTraitType.craft).getLevel() / 10;
        int lucksKey = ItemFlag.幸运卷轴.check(equip.getFlag()) ? 10 : 0;
        if (ItemFlag.幸运卷轴.check(equip.getFlag()))
        {
            equip.setFlag((short) (equip.getFlag() - ItemFlag.幸运卷轴.getValue()));
        }
        int success = succe + craft + lucksKey;
        if (chr.isAdmin())
        {
            chr.dropSpouseMessage(11, "突破攻击上限之石 - 默认几率: " + succe + "% 倾向加成: " + craft + "% 幸运卷轴状态加成: " + lucksKey + "% 最终几率: " + success + "%");
        }
        if (Randomizer.nextInt(100) <= success)
        {
            int limitBreak = getScrollLimitBreak(scrollId) + nEquip.getLimitBreak();
            if ((ItemConstants.isWeapon(nEquip.getItemId())) && (limitBreak <= 5000000))
            {
                nEquip.setLimitBreak(limitBreak);
            }
        }
        return nEquip;
    }

    public Item scrollResetEquip(Item equip, Item scroll, MapleCharacter chr)
    {
        if (equip.getType() != 1)
        {
            return equip;
        }
        Equip nEquip = (Equip) equip;
        int scrollId = scroll.getItemId();
        Map<String, Integer> scrollStats = getEquipStats(scrollId);
        int succe = (scrollStats == null) || (!scrollStats.containsKey("success")) ? 0 : scrollStats.get("success");
        int curse = (scrollStats == null) || (!scrollStats.containsKey("cursed")) ? 0 : scrollStats.get("cursed");
        int craft = chr.getTrait(client.MapleTraitType.craft).getLevel() / 10;
        int lucksKey = ItemFlag.幸运卷轴.check(equip.getFlag()) ? 10 : 0;
        if (ItemFlag.幸运卷轴.check(equip.getFlag()))
        {
            equip.setFlag((short) (equip.getFlag() - ItemFlag.幸运卷轴.getValue()));
        }
        int success = succe + craft + lucksKey;
        if (chr.isAdmin())
        {
            chr.dropSpouseMessage(11, "还原卷轴 - 默认几率: " + succe + "% 倾向加成: " + craft + "% 幸运卷轴状态加成: " + lucksKey + "% 最终几率: " + success + "% 失败消失几率: " + curse + "%");
        }
        if (Randomizer.nextInt(100) <= success) return resetEquipStats(nEquip);
        if (Randomizer.nextInt(99) < curse)
        {
            return null;
        }
        return nEquip;
    }

    public Equip resetEquipStats(Equip oldEquip)
    {
        Equip newEquip = (Equip) getEquipById(oldEquip.getItemId());

        newEquip.setState(oldEquip.getState());
        newEquip.setStateMsg(oldEquip.getStateMsg());
        newEquip.setPotential1(oldEquip.getPotential1());
        newEquip.setPotential2(oldEquip.getPotential2());
        newEquip.setPotential3(oldEquip.getPotential3());
        newEquip.setPotential4(oldEquip.getPotential4());
        newEquip.setPotential5(oldEquip.getPotential5());
        newEquip.setPotential6(oldEquip.getPotential6());
        newEquip.setSocket1(oldEquip.getSocket1());
        newEquip.setSocket2(oldEquip.getSocket2());
        newEquip.setSocket3(oldEquip.getSocket3());
        newEquip.setItemSkin(oldEquip.getItemSkin());

        newEquip.setPosition(oldEquip.getPosition());
        newEquip.setQuantity(oldEquip.getQuantity());
        newEquip.setFlag(oldEquip.getFlag());
        oldEquip.setOwner(oldEquip.getOwner());
        newEquip.setGMLog(oldEquip.getGMLog());
        newEquip.setExpiration(oldEquip.getExpiration());
        newEquip.setUniqueId(oldEquip.getUniqueId());
        newEquip.setEquipOnlyId(oldEquip.getEquipOnlyId());
        return newEquip;
    }

    public Equip randomizeStats(Equip equip)
    {
        equip.setStr(getRandStat(equip.getStr(), 5));
        equip.setDex(getRandStat(equip.getDex(), 5));
        equip.setInt(getRandStat(equip.getInt(), 5));
        equip.setLuk(getRandStat(equip.getLuk(), 5));
        equip.setMatk(getRandStat(equip.getMatk(), 5));
        equip.setWatk(getRandStat(equip.getWatk(), 5));
        equip.setAcc(getRandStat(equip.getAcc(), 5));
        equip.setAvoid(getRandStat(equip.getAvoid(), 5));
        equip.setJump(getRandStat(equip.getJump(), 5));
        equip.setHands(getRandStat(equip.getHands(), 5));
        equip.setSpeed(getRandStat(equip.getSpeed(), 5));
        equip.setWdef(getRandStat(equip.getWdef(), 10));
        equip.setMdef(getRandStat(equip.getMdef(), 10));
        equip.setHp(getRandStat(equip.getHp(), 10));
        equip.setMp(getRandStat(equip.getMp(), 10));
        return equip;
    }

    protected short getRandStat(short defaultValue, int maxRange)
    {
        if (defaultValue == 0)
        {
            return 0;
        }

        int lMaxRange = (int) Math.min(Math.ceil(defaultValue * 0.1D), maxRange);
        return (short) (defaultValue - lMaxRange + Randomizer.nextInt(lMaxRange * 2 + 1));
    }

    public Equip randomizeStats_Above(Equip equip)
    {
        equip.setStr(getRandStatAbove(equip.getStr(), 5));
        equip.setDex(getRandStatAbove(equip.getDex(), 5));
        equip.setInt(getRandStatAbove(equip.getInt(), 5));
        equip.setLuk(getRandStatAbove(equip.getLuk(), 5));
        equip.setMatk(getRandStatAbove(equip.getMatk(), 5));
        equip.setWatk(getRandStatAbove(equip.getWatk(), 5));
        equip.setAcc(getRandStatAbove(equip.getAcc(), 5));
        equip.setAvoid(getRandStatAbove(equip.getAvoid(), 5));
        equip.setJump(getRandStatAbove(equip.getJump(), 5));
        equip.setHands(getRandStatAbove(equip.getHands(), 5));
        equip.setSpeed(getRandStatAbove(equip.getSpeed(), 5));
        equip.setWdef(getRandStatAbove(equip.getWdef(), 10));
        equip.setMdef(getRandStatAbove(equip.getMdef(), 10));
        equip.setHp(getRandStatAbove(equip.getHp(), 10));
        equip.setMp(getRandStatAbove(equip.getMp(), 10));
        return equip;
    }

    protected short getRandStatAbove(short defaultValue, int maxRange)
    {
        if (defaultValue <= 0)
        {
            return 0;
        }
        int lMaxRange = (int) Math.min(Math.ceil(defaultValue * 0.1D), maxRange);
        return (short) (defaultValue + Randomizer.nextInt(lMaxRange + 1));
    }

    public Equip fuse(Equip equip1, Equip equip2)
    {
        if (equip1.getItemId() != equip2.getItemId())
        {
            return equip1;
        }
        Equip equip = (Equip) getEquipById(equip1.getItemId());
        equip.setStr(getRandStatFusion(equip.getStr(), equip1.getStr(), equip2.getStr()));
        equip.setDex(getRandStatFusion(equip.getDex(), equip1.getDex(), equip2.getDex()));
        equip.setInt(getRandStatFusion(equip.getInt(), equip1.getInt(), equip2.getInt()));
        equip.setLuk(getRandStatFusion(equip.getLuk(), equip1.getLuk(), equip2.getLuk()));
        equip.setMatk(getRandStatFusion(equip.getMatk(), equip1.getMatk(), equip2.getMatk()));
        equip.setWatk(getRandStatFusion(equip.getWatk(), equip1.getWatk(), equip2.getWatk()));
        equip.setAcc(getRandStatFusion(equip.getAcc(), equip1.getAcc(), equip2.getAcc()));
        equip.setAvoid(getRandStatFusion(equip.getAvoid(), equip1.getAvoid(), equip2.getAvoid()));
        equip.setJump(getRandStatFusion(equip.getJump(), equip1.getJump(), equip2.getJump()));
        equip.setHands(getRandStatFusion(equip.getHands(), equip1.getHands(), equip2.getHands()));
        equip.setSpeed(getRandStatFusion(equip.getSpeed(), equip1.getSpeed(), equip2.getSpeed()));
        equip.setWdef(getRandStatFusion(equip.getWdef(), equip1.getWdef(), equip2.getWdef()));
        equip.setMdef(getRandStatFusion(equip.getMdef(), equip1.getMdef(), equip2.getMdef()));
        equip.setHp(getRandStatFusion(equip.getHp(), equip1.getHp(), equip2.getHp()));
        equip.setMp(getRandStatFusion(equip.getMp(), equip1.getMp(), equip2.getMp()));
        return equip;
    }

    public Item getEquipById(int equipId)
    {
        return getEquipById(equipId, -1);
    }

    protected short getRandStatFusion(short defaultValue, int value1, int value2)
    {
        if (defaultValue == 0)
        {
            return 0;
        }
        int range = (value1 + value2) / 2 - defaultValue;
        int rand = Randomizer.nextInt(Math.abs(range) + 1);
        return (short) (defaultValue + (range < 0 ? -rand : rand));
    }

    public Item getEquipById(int equipId, int ringId)
    {
        ItemInformation i = getItemInformation(equipId);
        if (i == null)
        {
            return new Equip(equipId, (short) 0, ringId, (short) 0);
        }
        Item eq = i.eq.copy();
        eq.setUniqueId(ringId);
        return eq;
    }

    public Equip randomize休彼德蔓徽章(Equip equip)
    {
        int stats = get休彼德蔓徽章点数(equip.getItemId());
        if (stats > 0)
        {
            int prob = equip.getItemId() - 1182000;
            if (Randomizer.nextInt(15) <= prob)
            {
                equip.setStr((short) Randomizer.nextInt(stats + prob));
            }
            if (Randomizer.nextInt(15) <= prob)
            {
                equip.setDex((short) Randomizer.nextInt(stats + prob));
            }
            if (Randomizer.nextInt(15) <= prob)
            {
                equip.setInt((short) Randomizer.nextInt(stats + prob));
            }
            if (Randomizer.nextInt(15) <= prob)
            {
                equip.setLuk((short) Randomizer.nextInt(stats + prob));
            }
            if (Randomizer.nextInt(30) <= prob)
            {
                equip.setWatk((short) Randomizer.nextInt(stats));
            }
            if (Randomizer.nextInt(10) <= prob)
            {
                equip.setWdef((short) Randomizer.nextInt(stats * 8));
            }
            if (Randomizer.nextInt(30) <= prob)
            {
                equip.setMatk((short) Randomizer.nextInt(stats));
            }
            if (Randomizer.nextInt(10) <= prob)
            {
                equip.setMdef((short) Randomizer.nextInt(stats * 8));
            }
            if (Randomizer.nextInt(8) <= prob)
            {
                equip.setAcc((short) Randomizer.nextInt(stats * 5));
            }
            if (Randomizer.nextInt(8) <= prob)
            {
                equip.setAvoid((short) Randomizer.nextInt(stats * 5));
            }
            if (Randomizer.nextInt(10) <= prob)
            {
                equip.setSpeed((short) Randomizer.nextInt(stats));
            }
            if (Randomizer.nextInt(10) <= prob)
            {
                equip.setJump((short) Randomizer.nextInt(stats));
            }
            if (Randomizer.nextInt(8) <= prob)
            {
                equip.setHp((short) Randomizer.nextInt(stats * 10));
            }
            if (Randomizer.nextInt(8) <= prob)
            {
                equip.setMp((short) Randomizer.nextInt(stats * 10));
            }
        }
        return equip;
    }

    public int get休彼德蔓徽章点数(int itemId)
    {
        switch (itemId)
        {
            case 1182000:
                return 3;
            case 1182001:
                return 5;
            case 1182002:
                return 7;
            case 1182003:
                return 9;
            case 1182004:
                return 13;
            case 1182005:
                return 16;
        }
        return 0;
    }

    public int getTotalStat(Equip equip)
    {
        return equip.getStr() + equip.getDex() + equip.getInt() + equip.getLuk() + equip.getMatk() + equip.getWatk() + equip.getAcc() + equip.getAvoid() + equip.getJump() + equip.getHands() + equip.getSpeed() + equip.getHp() + equip.getMp() + equip.getWdef() + equip.getMdef();
    }

    public Equip setPotentialState(Equip equip, int state)
    {
        if (equip.getState() == 0)
        {
            if (state == 1)
            {
                equip.setPotential1(-17);
            }
            else if (state == 2)
            {
                equip.setPotential1(-18);
            }
            else if (state == 3)
            {
                equip.setPotential1(-19);
            }
            else if (state == 4)
            {
                equip.setPotential1(-20);
            }
            else
            {
                equip.setPotential1(-17);
            }
        }
        return equip;
    }

    public MapleStatEffect getItemEffect(int itemId)
    {
        MapleStatEffect ret = this.itemEffects.get(itemId);
        if (ret == null)
        {
            MapleData item = getItemData(itemId);
            if ((item == null) || (item.getChildByPath("spec") == null))
            {
                return null;
            }
            ret = MapleStatEffect.loadItemEffectFromData(item.getChildByPath("spec"), itemId);
            this.itemEffects.put(itemId, ret);
        }
        return ret;
    }

    protected MapleData getItemData(int itemId)
    {
        MapleData ret = null;
        String idStr = "0" + itemId;
        MapleDataDirectoryEntry root = this.itemData.getRoot();
        for (MapleDataDirectoryEntry topDir : root.getSubdirectories())
        {
            for (provider.MapleDataFileEntry iFile : topDir.getFiles())
            {
                if (iFile.getName().equals(idStr.substring(0, 4) + ".img"))
                {
                    ret = this.itemData.getData(topDir.getName() + "/" + iFile.getName());
                    if (ret == null)
                    {
                        return null;
                    }
                    ret = ret.getChildByPath(idStr);
                    return ret;
                }
                if (iFile.getName().equals(idStr.substring(1) + ".img"))
                {
                    ret = this.itemData.getData(topDir.getName() + "/" + iFile.getName());
                    return ret;
                }
            }
        }
        MapleDataDirectoryEntry topDir;
        root = this.chrData.getRoot();
        for (MapleDataDirectoryEntry mapleDataDirectoryEntry : root.getSubdirectories())
        {
            topDir = mapleDataDirectoryEntry;
            for (provider.MapleDataFileEntry iFile : topDir.getFiles())
                if (iFile.getName().equals(idStr + ".img"))
                {
                    ret = this.chrData.getData(topDir.getName() + "/" + iFile.getName());
                    return ret;
                }
        }
        return ret;
    }

    public MapleStatEffect getItemEffectEX(int itemId)
    {
        MapleStatEffect ret = this.itemEffectsEx.get(itemId);
        if (ret == null)
        {
            MapleData item = getItemData(itemId);
            if ((item == null) || (item.getChildByPath("specEx") == null))
            {
                return null;
            }
            ret = MapleStatEffect.loadItemEffectFromData(item.getChildByPath("specEx"), itemId);
            this.itemEffectsEx.put(itemId, ret);
        }
        return ret;
    }

    public int getCreateId(int id)
    {
        ItemInformation i = getItemInformation(id);
        if (i == null)
        {
            return 0;
        }
        return i.create;
    }

    public int getCardMobId(int id)
    {
        ItemInformation i = getItemInformation(id);
        if (i == null)
        {
            return 0;
        }
        return i.monsterBook;
    }

    public int getBagType(int id)
    {
        ItemInformation i = getItemInformation(id);
        if (i == null)
        {
            return 0;
        }
        return i.flag & 0xF;
    }

    public int getWatkForProjectile(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if ((i == null) || (i.equipStats == null) || (i.equipStats.get("PAD") == null))
        {
            return 0;
        }
        return i.equipStats.get("PAD");
    }

    public boolean canScroll(int scrollid, int itemid)
    {
        return (scrollid / 100 % 100 == itemid / 10000 % 100) || ((itemid >= 1672000) && (itemid <= 1672010));
    }

    public String getName(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.name;
    }

    public String getDesc(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.desc;
    }

    public String getMsg(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.msg;
    }

    public short getItemMakeLevel(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return 0;
        }
        return i.itemMakeLevel;
    }

    public boolean cantSell(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x10) != 0;
    }

    public boolean isLogoutExpire(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x20) != 0;
    }

    public boolean isPickupBlocked(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x40) != 0;
    }

    public boolean isPickupRestricted(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (((i.flag & 0x80) != 0) || (ItemConstants.isPickupRestricted(itemId))) && (itemId != 4001168);
    }

    public boolean isAccountShared(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x100) != 0;
    }

    public boolean isQuestItem(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return ((i.flag & 0x200) != 0) && (itemId / 10000 != 301);
    }

    public boolean isDropRestricted(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return ((i.flag & 0x200) != 0) || ((i.flag & 0x400) != 0) || (ItemConstants.isDropRestricted(itemId));
    }

    public boolean isShareTagEnabled(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x800) != 0;
    }

    public boolean isMobHP(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x1000) != 0;
    }

    public boolean isActivatedSocketItem(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x2000) != 0;
    }

    public boolean isSuperiorEquip(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x4000) != 0;
    }

    public boolean isOnlyEquip(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return (i.flag & 0x8000) != 0;
    }

    public int getStateChangeItem(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return 0;
        }
        return i.stateChange;
    }

    public int getMeso(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return 0;
        }
        return i.meso;
    }

    public boolean isKarmaEnabled(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return i.karmaEnabled == 1;
    }

    public boolean isPKarmaEnabled(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return false;
        }
        return i.karmaEnabled == 2;
    }

    public Pair<Integer, List<StructRewardItem>> getRewardItem(int itemid)
    {
        ItemInformation i = getItemInformation(itemid);
        if (i == null)
        {
            return null;
        }
        return new Pair(i.totalprob, i.rewardItems);
    }

    public Pair<Integer, List<Integer>> questItemInfo(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return new Pair(i.questId, i.questItems);
    }

    public Pair<Integer, String> replaceItemInfo(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return new Pair(i.replaceItem, i.replaceMsg);
    }

    public List<Triple<String, Point, Point>> getAfterImage(String after)
    {
        return this.afterImage.get(after);
    }

    public String getAfterImage(int itemId)
    {
        ItemInformation i = getItemInformation(itemId);
        if (i == null)
        {
            return null;
        }
        return i.afterImage;
    }

    public boolean itemExists(int itemId)
    {
        if (ItemConstants.getInventoryType(itemId) == client.inventory.MapleInventoryType.UNDEFINED)
        {
            return false;
        }
        return getItemInformation(itemId) != null;
    }

    public boolean isCash(int itemId)
    {
        if (getEquipStats(itemId) == null)
        {
            return ItemConstants.getInventoryType(itemId) == client.inventory.MapleInventoryType.CASH;
        }
        return (ItemConstants.getInventoryType(itemId) == client.inventory.MapleInventoryType.CASH) || (getEquipStats(itemId).get("cash") != null);
    }

    public boolean isExpOrDropCardTime(int itemId)
    {
        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("Asia/ShangHai"));
        String day = MapleDayInt.getDayInt(cal.get(7));
        Map<String, String> times;
        if (this.getExpCardTimes.containsKey(itemId))
        {
            times = this.getExpCardTimes.get(itemId);
        }
        else
        {
            List<MapleData> data = getItemData(itemId).getChildByPath("info").getChildByPath("time").getChildren();
            Map<String, String> hours = new HashMap<>();
            for (MapleData childdata : data)
            {
                String[] time = MapleDataTool.getString(childdata).split(":");
                hours.put(time[0], time[1]);
            }
            times = hours;
            this.getExpCardTimes.put(itemId, hours);
            cal.get(7);
        }
        if (times.containsKey(day))
        {
            String[] hourspan = times.get(day).split("-");
            int starthour = Integer.parseInt(hourspan[0]);
            int endhour = Integer.parseInt(hourspan[1]);

            return (cal.get(11) >= starthour) && (cal.get(11) <= endhour);
        }
        return false;
    }

    public ScriptedItem getScriptedItemInfo(int itemId)
    {
        if (this.scriptedItemCache.containsKey(itemId))
        {
            return this.scriptedItemCache.get(itemId);
        }
        if (itemId / 10000 != 243)
        {
            return null;
        }
        ScriptedItem script = new ScriptedItem(MapleDataTool.getInt("spec/npc", getItemData(itemId), 0), MapleDataTool.getString("spec/script", getItemData(itemId), ""),
                MapleDataTool.getInt("spec" + "/runOnPickup", getItemData(itemId), 0) == 1);
        this.scriptedItemCache.put(itemId, script);
        return this.scriptedItemCache.get(itemId);
    }

    public StructCrossHunterShop getCrossHunterShop(int key)
    {
        if (this.crossHunterShop.containsKey(key))
        {
            return this.crossHunterShop.get(key);
        }
        return null;
    }

    public boolean isFloatCashItem(int itemId)
    {
        if (this.floatCashItem.containsKey(itemId))
        {
            return this.floatCashItem.get(itemId);
        }
        if (itemId / 10000 != 512)
        {
            return false;
        }
        boolean floatType = MapleDataTool.getIntConvert("info/floatType", getItemData(itemId), 0) > 0;
        this.floatCashItem.put(itemId, floatType);
        return floatType;
    }

    public short getPetFlagInfo(int itemId)
    {
        if (this.petFlagInfo.containsKey(itemId))
        {
            return this.petFlagInfo.get(itemId);
        }
        short flag = 0;
        if (itemId / 10000 != 500)
        {
            return flag;
        }
        MapleData item = getItemData(itemId);
        if (item == null)
        {
            return flag;
        }
        if (MapleDataTool.getIntConvert("info/pickupItem", item, 0) > 0)
        {
            flag = (short) (flag | 0x1);
        }
        if (MapleDataTool.getIntConvert("info/longRange", item, 0) > 0)
        {
            flag = (short) (flag | 0x2);
        }
        if (MapleDataTool.getIntConvert("info/pickupAll", item, 0) > 0)
        {
            flag = (short) (flag | 0x4);
        }
        if (MapleDataTool.getIntConvert("info/sweepForDrop", item, 0) > 0)
        {
            flag = (short) (flag | 0x10);
        }
        if (MapleDataTool.getIntConvert("info/consumeHP", item, 0) > 0)
        {
            flag = (short) (flag | 0x20);
        }
        if (MapleDataTool.getIntConvert("info/consumeMP", item, 0) > 0)
        {
            flag = (short) (flag | 0x40);
        }
        if (MapleDataTool.getIntConvert("info/autoBuff", item, 0) > 0)
        {
            flag = (short) (flag | 0x200);
        }
        this.petFlagInfo.put(itemId, flag);
        return flag;
    }

    public int getPetSetItemID(int itemId)
    {
        if (this.petSetItemID.containsKey(itemId))
        {
            return this.petSetItemID.get(itemId);
        }
        int ret = -1;
        if (itemId / 10000 != 500)
        {
            return ret;
        }
        ret = MapleDataTool.getIntConvert("info/setItemID", getItemData(itemId), 0);
        this.petSetItemID.put(itemId, ret);
        return ret;
    }

    public int getItemIncMHPr(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("MHPr")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("MHPr");
    }

    public int getItemIncMMPr(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("MMPr")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("MMPr");
    }

    public int getSuccessRates(int itemId)
    {
        if (this.successRates.containsKey(itemId))
        {
            return this.successRates.get(itemId);
        }
        int success = 0;
        if (itemId / 10000 != 204)
        {
            return success;
        }
        success = MapleDataTool.getIntConvert("info/successRates/0", getItemData(itemId), 0);
        this.successRates.put(itemId, success);
        return success;
    }

    public int getForceUpgrade(int itemId)
    {
        if (this.forceUpgrade.containsKey(itemId))
        {
            return this.forceUpgrade.get(itemId);
        }
        int upgrade = 0;
        if (itemId / 100 != 20493)
        {
            return upgrade;
        }
        upgrade = MapleDataTool.getIntConvert("info/forceUpgrade", getItemData(itemId), 1);
        this.forceUpgrade.put(itemId, upgrade);
        return upgrade;
    }

    public Pair<Integer, Integer> getChairRecovery(int itemId)
    {
        if (itemId / 10000 != 301)
        {
            return null;
        }
        if (this.chairRecovery.containsKey(itemId))
        {
            return this.chairRecovery.get(itemId);
        }
        int recoveryHP = MapleDataTool.getIntConvert("info/recoveryHP", getItemData(itemId), 0);
        int recoveryMP = MapleDataTool.getIntConvert("info/recoveryMP", getItemData(itemId), 0);
        Pair<Integer, Integer> ret = new Pair(recoveryHP, recoveryMP);
        this.chairRecovery.put(itemId, ret);
        return ret;
    }

    public int getLimitBreak(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("limitBreak")))
        {
            return 999999;
        }
        return getEquipStats(itemId).get("limitBreak");
    }

    public int getBossDamageRate(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("bdR")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("bdR");
    }

    public int getIgnoreMobDmageRate(int itemId)
    {
        if ((getEquipStats(itemId) == null) || (!getEquipStats(itemId).containsKey("imdR")))
        {
            return 0;
        }
        return getEquipStats(itemId).get("imdR");
    }

    public int getAndroidType(int itemId)
    {
        if (this.androidType.containsKey(itemId))
        {
            return this.androidType.get(itemId);
        }
        int type = 0;
        if (itemId / 10000 != 166)
        {
            return type;
        }
        type = MapleDataTool.getIntConvert("info/android", getItemData(itemId), 1);
        this.androidType.put(itemId, type);
        return type;
    }

    public int getScrollLimitBreak(int itemId)
    {
        if (this.ScrollLimitBreak.containsKey(itemId))
        {
            return this.ScrollLimitBreak.get(itemId);
        }
        int upgrade = 0;
        if (itemId / 100 != 26140)
        {
            return upgrade;
        }
        upgrade = MapleDataTool.getIntConvert("info/incALB", getItemData(itemId), 0);
        this.forceUpgrade.put(itemId, upgrade);
        return upgrade;
    }

    public boolean isNoCursedScroll(int itemId)
    {
        if (this.noCursedScroll.containsKey(itemId))
        {
            return this.noCursedScroll.get(itemId);
        }
        if (itemId / 10000 != 204)
        {
            return false;
        }
        boolean noCursed = MapleDataTool.getIntConvert("info/noCursed", getItemData(itemId), 0) > 0;
        this.noCursedScroll.put(itemId, noCursed);
        return noCursed;
    }

    public boolean isNegativeScroll(int itemId)
    {
        if (this.noNegativeScroll.containsKey(itemId))
        {
            return this.noNegativeScroll.get(itemId);
        }
        if (itemId / 10000 != 204)
        {
            return false;
        }
        boolean noNegative = MapleDataTool.getIntConvert("info/noNegative", getItemData(itemId), 0) > 0;
        this.noNegativeScroll.put(itemId, noNegative);
        return noNegative;
    }

    public boolean isExclusiveEquip(int itemId)
    {
        return this.exclusiveEquip.containsKey(itemId);
    }

    public StructExclusiveEquip getExclusiveEquipInfo(int itemId)
    {
        if (this.exclusiveEquip.containsKey(itemId))
        {
            int exclusiveId = this.exclusiveEquip.get(itemId);
            if (this.exclusiveEquipInfo.containsKey(exclusiveId))
            {
                return this.exclusiveEquipInfo.get(exclusiveId);
            }
        }
        return null;
    }

    public static class MapleDayInt
    {
        public static String getDayInt(int day)
        {
            if (day == 1) return "SUN";
            if (day == 2) return "MON";
            if (day == 3) return "TUE";
            if (day == 4) return "WED";
            if (day == 5) return "THU";
            if (day == 6) return "FRI";
            if (day == 7)
            {
                return "SAT";
            }
            return null;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\MapleItemInformationProvider.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */