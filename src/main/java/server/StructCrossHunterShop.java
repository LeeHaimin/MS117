package server;


public class StructCrossHunterShop
{
    private final int itemId;


    private final int tokenPrice;

    private final int potentialGrade;


    public StructCrossHunterShop(int itemId, int tokenPrice, int potentialGrade)
    {
        this.itemId = itemId;
        this.tokenPrice = tokenPrice;
        this.potentialGrade = potentialGrade;
    }

    public int getItemId()
    {
        return this.itemId;
    }

    public int getTokenPrice()
    {
        return this.tokenPrice;
    }

    public int getPotentialGrade()
    {
        return this.potentialGrade;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\StructCrossHunterShop.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */