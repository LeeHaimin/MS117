package server;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleClient;
import client.inventory.Item;
import client.inventory.ItemFlag;
import client.inventory.ItemLoader;
import client.inventory.MapleInventoryType;
import constants.ItemConstants;
import tools.Pair;
import tools.packet.NPCPacket;

public class MapleStorage implements java.io.Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private final int storageId;
    private final int accountId;
    private final List<Item> items;
    private final Map<MapleInventoryType, List<Item>> typeItems = new EnumMap(MapleInventoryType.class);
    private int meso;
    private byte slots;
    private int storageNpcId;
    private boolean changed = false;

    private MapleStorage(int storageId, byte slots, int meso, int accountId)
    {
        this.storageId = storageId;
        this.slots = slots;
        this.meso = meso;
        this.accountId = accountId;
        this.items = new LinkedList<>();
        if (this.slots > 96)
        {
            this.slots = 96;
            this.changed = true;
        }
    }

    public static MapleStorage loadOrCreateFromDB(int accountId)
    {
        MapleStorage ret = null;
        try
        {
            PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("SELECT * FROM storages WHERE accountid = ?");
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            MapleItemInformationProvider ii;
            if (rs.next())
            {
                int storeId = rs.getInt("storageid");
                ret = new MapleStorage(storeId, rs.getByte("slots"), rs.getInt("meso"), accountId);
                rs.close();
                ps.close();
                ii = MapleItemInformationProvider.getInstance();
                for (Pair<Item, MapleInventoryType> mit : ItemLoader.仓库道具.loadItems(false, accountId).values())
                {
                    Item item = mit.getLeft();
                    if ((item.getItemId() / 1000000 == 1) && (ii.isDropRestricted(item.getItemId())) && (!ItemFlag.KARMA_EQ.check(item.getFlag())))
                    {
                        item.addFlag((short) ItemFlag.KARMA_EQ.getValue());
                    }
                    ret.items.add(item);
                }
            }
            else
            {
                int storeId = create(accountId);
                ret = new MapleStorage(storeId, (byte) 4, 0, accountId);
                rs.close();
                ps.close();
            }
        }
        catch (SQLException ex)
        {
            System.err.println("Error loading storage" + ex);
        }
        return ret;
    }

    public static int create(int accountId) throws SQLException
    {
        PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("INSERT INTO storages (accountid, slots, meso) VALUES (?, ?, ?)", 1);
        ps.setInt(1, accountId);
        ps.setInt(2, 4);
        ps.setInt(3, 0);
        ps.executeUpdate();


        ResultSet rs = ps.getGeneratedKeys();
        if (rs.next())
        {
            int storageid = rs.getInt(1);
            ps.close();
            rs.close();
            return storageid;
        }
        ps.close();
        rs.close();
        throw new database.DatabaseException("Inserting char failed.");
    }

    public void saveToDB()
    {
        if (!this.changed)
        {
            return;
        }
        try
        {
            PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("UPDATE storages SET slots = ?, meso = ? WHERE storageid = ?");
            ps.setInt(1, this.slots);
            ps.setInt(2, this.meso);
            ps.setInt(3, this.storageId);
            ps.executeUpdate();
            ps.close();

            List<Pair<Item, MapleInventoryType>> itemsWithType = new ArrayList<>();
            for (Item item : this.items)
            {
                itemsWithType.add(new Pair(item, ItemConstants.getInventoryType(item.getItemId())));
            }
            ItemLoader.仓库道具.saveItems(itemsWithType, this.accountId);
            this.changed = false;
        }
        catch (SQLException ex)
        {
            System.err.println("Error saving storage" + ex);
        }
    }


    public Item getItem(byte slot)
    {
        if ((slot >= this.items.size()) || (slot < 0))
        {
            return null;
        }
        return this.items.get(slot);
    }


    public Item takeOut(byte slot)
    {
        this.changed = true;
        Item ret = this.items.remove(slot);
        MapleInventoryType type = ItemConstants.getInventoryType(ret.getItemId());
        this.typeItems.put(type, new ArrayList(filterItems(type)));
        return ret;
    }

    private List<Item> filterItems(MapleInventoryType type)
    {
        List<Item> ret = new LinkedList<>();
        for (Item item : this.items)
        {
            if (ItemConstants.getInventoryType(item.getItemId()) == type)
            {
                ret.add(item);
            }
        }
        return ret;
    }

    public void store(Item item)
    {
        this.changed = true;
        this.items.add(item);
        MapleInventoryType type = ItemConstants.getInventoryType(item.getItemId());
        this.typeItems.put(type, new ArrayList(filterItems(type)));
    }

    public void arrange()
    {
        this.items.sort((Comparator) (o1, o2) -> {
            {
                if (((Item) o1).getItemId() < ((Item) o2).getItemId()) return -1;
                if (((Item) o1).getItemId() == ((Item) o2).getItemId())
                {
                    return 0;
                }
                return 1;
            }
        });

        for (MapleInventoryType type : MapleInventoryType.values())
        {
            this.typeItems.put(type, new ArrayList(this.items));
        }
    }

    public List<Item> getItems()
    {
        return java.util.Collections.unmodifiableList(this.items);
    }

    public byte getSlot(MapleInventoryType type, byte slot)
    {
        byte ret = 0;
        List<Item> it = this.typeItems.get(type);
        if ((it == null) || (slot >= it.size()) || (slot < 0))
        {
            return -1;
        }
        for (Item item : this.items)
        {
            if (item == it.get(slot))
            {
                return ret;
            }
            ret = (byte) (ret + 1);
        }
        return -1;
    }

    public void sendStorage(MapleClient c, int npcId)
    {
        this.storageNpcId = npcId;
        java.util.Collections.sort(this.items, new Comparator()
        {
            @Override
            public int compare(Object o1, Object o2)
            {
                if (ItemConstants.getInventoryType(((Item) o1).getItemId()).getType() < ItemConstants.getInventoryType(((Item) o2).getItemId()).getType()) return -1;
                if (ItemConstants.getInventoryType(((Item) o1).getItemId()) == ItemConstants.getInventoryType(((Item) o2).getItemId()))
                {
                    return 0;
                }
                return 1;
            }

            public int compare(Item o1, Item o2)
            {
                if (ItemConstants.getInventoryType(o1.getItemId()).getType() < ItemConstants.getInventoryType(o2.getItemId()).getType()) return -1;
                if (ItemConstants.getInventoryType(o1.getItemId()) == ItemConstants.getInventoryType(o2.getItemId()))
                {
                    return 0;
                }
                return 1;
            }
        });

        for (MapleInventoryType type : MapleInventoryType.values())
        {
            this.typeItems.put(type, new ArrayList(this.items));
        }
        c.getSession().write(NPCPacket.getStorage(npcId, this.slots, this.items, this.meso));
    }

    public void update(MapleClient c)
    {
        c.getSession().write(NPCPacket.arrangeStorage(this.slots, this.items, true));
    }

    public void sendStored(MapleClient c, MapleInventoryType type)
    {
        c.getSession().write(NPCPacket.storeStorage(this.slots, type, this.typeItems.get(type)));
    }

    public void sendTakenOut(MapleClient c, MapleInventoryType type)
    {
        c.getSession().write(NPCPacket.takeOutStorage(this.slots, type, this.typeItems.get(type)));
    }

    public int getMeso()
    {
        return this.meso;
    }

    public void setMeso(int meso)
    {
        if (meso < 0)
        {
            return;
        }
        this.changed = true;
        this.meso = meso;
    }

    public Item findById(int itemId)
    {
        for (Item item : this.items)
        {
            if (item.getItemId() == itemId)
            {
                return item;
            }
        }
        return null;
    }

    public void sendMeso(MapleClient c)
    {
        c.getSession().write(NPCPacket.mesoStorage(this.slots, this.meso));
    }

    public boolean isFull()
    {
        return this.items.size() >= this.slots;
    }

    public int getSlots()
    {
        return this.slots;
    }

    public void setSlots(byte set)
    {
        this.changed = true;
        this.slots = set;
    }

    public void increaseSlots(byte gain)
    {
        this.changed = true;
        this.slots = ((byte) (this.slots + gain));
    }

    public int getNpcId()
    {
        return this.storageNpcId;
    }

    public void close()
    {
        this.typeItems.clear();
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\MapleStorage.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */