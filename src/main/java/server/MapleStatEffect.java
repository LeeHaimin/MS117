package server;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleDisease;
import client.MapleTraitType;
import client.PlayerStats;
import client.Skill;
import client.SkillFactory;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import client.status.MonsterStatus;
import constants.GameConstants;
import provider.MapleData;
import provider.MapleDataTool;
import server.life.MapleMonster;
import server.maps.MapleMap;
import server.maps.MapleMapObject;
import server.maps.MapleMist;
import server.maps.MapleSummon;
import tools.CaltechEval;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.Triple;
import tools.packet.BuffPacket;

public class MapleStatEffect implements java.io.Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MapleStatEffect.class);
    private Map<MapleStatInfo, Integer> info;
    private Map<client.MapleTraitType, Integer> traits;
    private boolean overTime;
    private boolean skill;
    private boolean partyBuff = true;
    private boolean notRemoved;
    private boolean repeatEffect;
    private ArrayList<Pair<MapleBuffStat, Integer>> statups;
    private ArrayList<Pair<Integer, Integer>> availableMap;
    private EnumMap<MonsterStatus, Integer> monsterStatus;
    private Point lt;
    private Point rb;
    private byte level;
    private List<MapleDisease> cureDebuffs;
    private List<Integer> petsCanConsume;
    private List<Integer> familiars;
    private List<Integer> randomPickup;
    private List<Triple<Integer, Integer, Integer>> rewardItem;
    private byte slotCount;
    private byte slotPerLine;
    private byte expR;
    private byte familiarTarget;
    private byte recipeUseCount;
    private byte recipeValidDay;
    private byte reqSkillLevel;
    private byte effectedOnAlly;
    private byte effectedOnEnemy;
    private byte type;
    private byte preventslip;
    private byte immortal;
    private byte bs;
    private short ignoreMob;
    private short mesoR;
    private short thaw;
    private short fatigueChange;
    private short lifeId;
    private short imhp;
    private short immp;
    private short inflation;
    private short useLevel;
    private short indiePdd;
    private short indieMdd;
    private short incPVPdamage;
    private short mobSkill;
    private short mobSkillLevel;
    private double hpR;
    private double mpR;
    private int sourceid;
    private int recipe;
    private int moveTo;
    private int moneyCon;
    private int morphId = 0;
    private int expinc;
    private int exp;
    private int consumeOnPickup;
    private int charColor;
    private int interval;
    private int rewardMeso;
    private int totalprob;
    private int cosmetic;
    private int expBuff;
    private int itemup;
    private int mesoup;
    private int cashup;
    private int berserk;
    private int illusion;
    private int booster;
    private int berserk2;
    private int cp;
    private int nuffSkill;

    public static MapleStatEffect loadSkillEffectFromData(MapleData source, int skillid, boolean overtime, int level, String variables, boolean notRemoved)
    {
        return loadFromData(source, skillid, true, overtime, level, variables, notRemoved);
    }

    private static MapleStatEffect loadFromData(MapleData source, int sourceid, boolean skill, boolean overTime, int level, String variables, boolean notRemoved)
    {
        MapleStatEffect ret = new MapleStatEffect();
        ret.sourceid = sourceid;
        ret.skill = skill;
        ret.level = ((byte) level);
        if (source == null)
        {
            return ret;
        }
        ret.info = new EnumMap(MapleStatInfo.class);
        for (MapleStatInfo i : MapleStatInfo.values())
        {
            if (i.isSpecial())
            {
                ret.info.put(i, parseEval(i.name().substring(0, i.name().length() - 1), source, i.getDefault(), variables, level));
            }
            else
            {
//                System.out.println("转换:"+i.name());
                ret.info.put(i, parseEval(i.name(), source, i.getDefault(), variables, level));
            }
        }
        ret.hpR = (parseEval("hpR", source, 0, variables, level) / 100.0D);
        ret.mpR = (parseEval("mpR", source, 0, variables, level) / 100.0D);
        ret.ignoreMob = ((short) parseEval("ignoreMobpdpR", source, 0, variables, level));
        ret.thaw = ((short) parseEval("thaw", source, 0, variables, level));
        ret.interval = parseEval("interval", source, 0, variables, level);
        ret.expinc = parseEval("expinc", source, 0, variables, level);
        ret.exp = parseEval("exp", source, 0, variables, level);
        ret.morphId = parseEval("morph", source, 0, variables, level);
        ret.cp = parseEval("cp", source, 0, variables, level);
        ret.cosmetic = parseEval("cosmetic", source, 0, variables, level);
        ret.slotCount = ((byte) parseEval("slotCount", source, 0, variables, level));
        ret.slotPerLine = ((byte) parseEval("slotPerLine", source, 0, variables, level));
        ret.preventslip = ((byte) parseEval("preventslip", source, 0, variables, level));
        ret.useLevel = ((short) parseEval("useLevel", source, 0, variables, level));
        ret.nuffSkill = parseEval("nuffSkill", source, 0, variables, level);
        ret.familiarTarget = ((byte) (parseEval("familiarPassiveSkillTarget", source, 0, variables, level) + 1));
        ret.immortal = ((byte) parseEval("immortal", source, 0, variables, level));
        ret.type = ((byte) parseEval("type", source, 0, variables, level));
        ret.bs = ((byte) parseEval("bs", source, 0, variables, level));
        ret.indiePdd = ((short) parseEval("indiePdd", source, 0, variables, level));
        ret.indieMdd = ((short) parseEval("indieMdd", source, 0, variables, level));
        ret.expBuff = parseEval("expBuff", source, 0, variables, level);
        ret.cashup = parseEval("cashBuff", source, 0, variables, level);
        ret.itemup = parseEval("itemupbyitem", source, 0, variables, level);
        ret.mesoup = parseEval("mesoupbyitem", source, 0, variables, level);
        ret.berserk = parseEval("berserk", source, 0, variables, level);
        ret.berserk2 = parseEval("berserk2", source, 0, variables, level);
        ret.booster = parseEval("booster", source, 0, variables, level);
        ret.lifeId = ((short) parseEval("lifeId", source, 0, variables, level));
        ret.inflation = ((short) parseEval("inflation", source, 0, variables, level));
        ret.imhp = ((short) parseEval("imhp", source, 0, variables, level));
        ret.immp = ((short) parseEval("immp", source, 0, variables, level));
        ret.illusion = parseEval("illusion", source, 0, variables, level);
        ret.consumeOnPickup = parseEval("consumeOnPickup", source, 0, variables, level);
        if ((ret.consumeOnPickup == 1) && (parseEval("party", source, 0, variables, level) > 0))
        {
            ret.consumeOnPickup = 2;
        }

        ret.recipe = parseEval("recipe", source, 0, variables, level);
        ret.recipeUseCount = ((byte) parseEval("recipeUseCount", source, 0, variables, level));
        ret.recipeValidDay = ((byte) parseEval("recipeValidDay", source, 0, variables, level));
        ret.reqSkillLevel = ((byte) parseEval("reqSkillLevel", source, 0, variables, level));
        ret.effectedOnAlly = ((byte) parseEval("effectedOnAlly", source, 0, variables, level));
        ret.effectedOnEnemy = ((byte) parseEval("effectedOnEnemy", source, 0, variables, level));
        ret.incPVPdamage = ((short) parseEval("incPVPDamage", source, 0, variables, level));
        ret.moneyCon = parseEval("moneyCon", source, 0, variables, level);
        ret.moveTo = parseEval("moveTo", source, -1, variables, level);
        ret.repeatEffect = ret.is战法灵气();

        ret.charColor = 0;
        String cColor = MapleDataTool.getString("charColor", source, null);
        if (cColor != null)
        {
            ret.charColor |= Integer.parseInt("0x" + cColor.substring(0, 2));
            ret.charColor |= Integer.parseInt("0x" + cColor.substring(2, 4) + "00");
            ret.charColor |= Integer.parseInt("0x" + cColor.substring(4, 6) + "0000");
            ret.charColor |= Integer.parseInt("0x" + cColor.substring(6, 8) + "000000");
        }
        ret.traits = new EnumMap(client.MapleTraitType.class);


        for (MapleTraitType info : client.MapleTraitType.values())
        {
            int expz = parseEval(info.name() + "EXP", source, 0, variables, level);
            if (expz != 0)
            {
                ret.traits.put(info, expz);
            }
        }
        List<MapleDisease> cure = new ArrayList(5);
        if (parseEval("poison", source, 0, variables, level) > 0)
        {
            cure.add(MapleDisease.中毒);
        }
        if (parseEval("seal", source, 0, variables, level) > 0)
        {
            cure.add(MapleDisease.封印);
        }
        if (parseEval("darkness", source, 0, variables, level) > 0)
        {
            cure.add(MapleDisease.黑暗);
        }
        if (parseEval("weakness", source, 0, variables, level) > 0)
        {
            cure.add(MapleDisease.虚弱);
        }
        if (parseEval("curse", source, 0, variables, level) > 0)
        {
            cure.add(MapleDisease.诅咒);
        }
        ret.cureDebuffs = cure;
        ret.petsCanConsume = new ArrayList<>();
        for (int i = 0; ; i++)
        {
            int dd = parseEval(String.valueOf(i), source, 0, variables, level);
            if (dd <= 0) break;
            ret.petsCanConsume.add(dd);
        }


        MapleData mdd = source.getChildByPath("0");
        if ((mdd != null) && (mdd.getChildren().size() > 0))
        {
            ret.mobSkill = ((short) parseEval("mobSkill", mdd, 0, variables, level));
            ret.mobSkillLevel = ((short) parseEval("level", mdd, 0, variables, level));
        }
        else
        {
            ret.mobSkill = 0;
            ret.mobSkillLevel = 0;
        }
        MapleData pd = source.getChildByPath("randomPickup");
        if (pd != null)
        {
            ret.randomPickup = new ArrayList<>();
            for (MapleData p : pd)
            {
                ret.randomPickup.add(MapleDataTool.getInt(p));
            }
        }
        MapleData ltd = source.getChildByPath("lt");
        if (ltd != null)
        {
            ret.lt = ((Point) ltd.getData());
            ret.rb = ((Point) source.getChildByPath("rb").getData());
        }
        MapleData ltc = source.getChildByPath("con");
        if (ltc != null)
        {
            ret.availableMap = new ArrayList<>();
            for (MapleData ltb : ltc)
            {
                ret.availableMap.add(new Pair(MapleDataTool.getInt("sMap", ltb, 0), MapleDataTool.getInt("eMap", ltb, 999999999)));
            }
        }
        MapleData ltb = source.getChildByPath("familiar");
        if (ltb != null)
        {
            ret.fatigueChange = ((short) (parseEval("incFatigue", ltb, 0, variables, level) - parseEval("decFatigue", ltb, 0, variables, level)));
            ret.familiarTarget = ((byte) parseEval("target", ltb, 0, variables, level));
            MapleData lta = ltb.getChildByPath("targetList");
            if (lta != null)
            {
                ret.familiars = new ArrayList<>();
                for (MapleData ltz : lta)
                {
                    ret.familiars.add(MapleDataTool.getInt(ltz, 0));
                }
            }
        }
        else
        {
            ret.fatigueChange = 0;
        }
        int totalprob = 0;
        MapleData lta = source.getChildByPath("reward");
        if (lta != null)
        {
            ret.rewardMeso = parseEval("meso", lta, 0, variables, level);
            MapleData ltz = lta.getChildByPath("case");
            if (ltz != null)
            {
                ret.rewardItem = new ArrayList<>();
                for (MapleData lty : ltz)
                {
                    ret.rewardItem.add(new Triple(MapleDataTool.getInt("id", lty, 0), MapleDataTool.getInt("count", lty, 0), MapleDataTool.getInt("prop", lty, 0)));
                    totalprob += MapleDataTool.getInt("prob", lty, 0);
                }
            }
        }
        else
        {
            ret.rewardMeso = 0;
        }
        ret.totalprob = totalprob;

        if (ret.skill)
        {
            int priceUnit = ret.info.get(MapleStatInfo.priceUnit);
            if (priceUnit > 0)
            {
                int price = ret.info.get(MapleStatInfo.price);
                int extendPrice = ret.info.get(MapleStatInfo.extendPrice);
                ret.info.put(MapleStatInfo.price, price * priceUnit);
                ret.info.put(MapleStatInfo.extendPrice, extendPrice * priceUnit);
            }
            switch (sourceid)
            {
                case 1100002:
                case 1120013:
                case 1200002:
                case 1300002:
                case 2111007:
                case 2211007:
                case 2311007:
                case 3100001:
                case 3120008:
                case 3200001:
                case 12111007:
                case 21100010:
                case 21120012:
                case 22150004:
                case 22161005:
                case 22181004:
                case 23100006:
                case 23120012:
                case 32111010:
                case 33100009:
                case 33120011:
                    ret.info.put(MapleStatInfo.mobCount, 6);
                    break;
                case 24100003:
                case 24120002:
                case 35111004:
                case 35121005:
                case 35121013:
                    ret.info.put(MapleStatInfo.attackCount, 6);
                    ret.info.put(MapleStatInfo.bulletCount, 6);
                    break;
                case 31220007:
                    ret.info.put(MapleStatInfo.attackCount, 2);
                    break;
                case 27101100:
                case 36001005:
                    ret.info.put(MapleStatInfo.attackCount, 4);
                    break;
                case 61101002:
                case 61110211:
                    ret.info.put(MapleStatInfo.attackCount, 3);
                    break;
                case 61120007:
                case 61121217:
                    ret.info.put(MapleStatInfo.attackCount, 5);
            }

            if (GameConstants.isNoDelaySkill(sourceid))
            {
                ret.info.put(MapleStatInfo.mobCount, 6);
            }
        }
        if ((!ret.skill) && (ret.info.get(MapleStatInfo.time) > -1))
        {
            ret.overTime = true;
        }
        else
        {
            ret.info.put(MapleStatInfo.time, ret.info.get(MapleStatInfo.time).intValue() * 1000);
            ret.info.put(MapleStatInfo.subTime, ret.info.get(MapleStatInfo.subTime).intValue() * 1000);
            ret.overTime = ((overTime) || (ret.isMorph()) || (ret.is天使技能()) || (ret.getSummonMovementType() != null));
            ret.notRemoved = notRemoved;
        }
        ret.monsterStatus = new EnumMap(MonsterStatus.class);
        ret.statups = new ArrayList<>();
        if ((ret.overTime) && (ret.getSummonMovementType() == null) && (!ret.is能量获得()))
        {
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.物理攻击, ret.info.get(MapleStatInfo.pad));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.物理防御, ret.info.get(MapleStatInfo.pdd));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.魔法攻击, ret.info.get(MapleStatInfo.mad));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.魔法防御, ret.info.get(MapleStatInfo.mdd));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.命中率, ret.info.get(MapleStatInfo.acc));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.回避率, ret.info.get(MapleStatInfo.eva));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.移动速度, (sourceid == 32120001) || (sourceid == 32101003) ? ret.info.get(MapleStatInfo.x) : ret.info.get(MapleStatInfo.speed));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.跳跃力, ret.info.get(MapleStatInfo.jump));
            if (sourceid != 22131001)
            {
                addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.MAXHP, ret.info.get(MapleStatInfo.mhpR));
            }
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.MAXMP, ret.info.get(MapleStatInfo.mmpR));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.攻击加速, ret.booster);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.HP_LOSS_GUARD, (int) ret.thaw);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.EXPRATE, ret.expBuff);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.ACASH_RATE, ret.cashup);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.DROP_RATE, constants.ItemConstants.getModifier(ret.sourceid, ret.itemup));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.MESO_RATE, constants.ItemConstants.getModifier(ret.sourceid, ret.mesoup));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.狂暴战魂, ret.berserk2);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.ILLUSION, ret.illusion);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.天使状态, ret.berserk);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.增强_MAXHP, ret.info.get(MapleStatInfo.emhp));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.增强_MAXMP, ret.info.get(MapleStatInfo.emmp));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.增强_物理攻击, ret.info.get(MapleStatInfo.epad));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.增强_魔法攻击, ret.info.get(MapleStatInfo.emad));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.增强_物理防御, ret.info.get(MapleStatInfo.epdd));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.增强_魔法防御, ret.info.get(MapleStatInfo.emdd));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.GIANT_POTION, (int) ret.inflation);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.力量, ret.info.get(MapleStatInfo.str));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.敏捷, ret.info.get(MapleStatInfo.dex));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.智力, ret.info.get(MapleStatInfo.int_));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.运气, ret.info.get(MapleStatInfo.luk));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.力量, ret.info.get(MapleStatInfo.indieSTR));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.敏捷, ret.info.get(MapleStatInfo.indieDEX));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.智力, ret.info.get(MapleStatInfo.indieINT));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.运气, ret.info.get(MapleStatInfo.indieLUK));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieMad, ret.info.get(MapleStatInfo.indieMad));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieMaxHp, (int) ret.imhp);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieMaxMp, (int) ret.immp);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieMaxHp, ret.info.get(MapleStatInfo.indieMhp));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieMaxMp, ret.info.get(MapleStatInfo.indieMmp));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.PVP_DAMAGE, (int) ret.incPVPdamage);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieJump, ret.info.get(MapleStatInfo.indieJump));
            if ((sourceid != 35001002) && (sourceid != 35120000))
            {
                addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieSpeed, ret.info.get(MapleStatInfo.indieSpeed));
            }
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieAcc, ret.info.get(MapleStatInfo.indieAcc));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieEva, ret.info.get(MapleStatInfo.indieEva));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.indieAllStat, ret.info.get(MapleStatInfo.indieAllStat));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.PVP_ATTACK, ret.info.get(MapleStatInfo.PVPdamage));
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.INVINCIBILITY, (int) ret.immortal);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.NO_SLIP, (int) ret.preventslip);
            addBuffStatPairToListIfNotZero(ret.statups, MapleBuffStat.FAMILIAR_SHADOW, ret.charColor > 0 ? 1 : 0);
        }
        if (ret.skill)
        {
            switch (sourceid)
            {
                case 1001003:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.物理防御增加, ret.info.get(MapleStatInfo.indiePdd)));
                    break;
                case 1311015:
                    ret.statups.add(new Pair(MapleBuffStat.交叉锁链, ret.info.get(MapleStatInfo.x)));
                    break;
                case 1310016:
                    ret.statups.add(new Pair(MapleBuffStat.暴击概率, ret.info.get(MapleStatInfo.indieCr)));
                    break;
                case 4200013:
                    ret.statups.add(new Pair(MapleBuffStat.暴击概率, ret.info.get(MapleStatInfo.x)));
                    break;
                case 1321015:
                    ret.hpR = (ret.info.get(MapleStatInfo.y) / 100.0D);
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, ret.info.get(MapleStatInfo.ignoreMobpdpR)));
                    ret.statups.add(new Pair(MapleBuffStat.BOSS伤害增加, ret.info.get(MapleStatInfo.indieBDR)));
                    break;
                case 2001002:
                case 12001001:
                case 22111001:
                    ret.statups.add(new Pair(MapleBuffStat.魔法盾, ret.info.get(MapleStatInfo.x)));
                    break;
                case 2111011:
                case 2211012:
                case 2301003:
                    ret.statups.add(new Pair(MapleBuffStat.神之保护, ret.info.get(MapleStatInfo.x)));
                    break;
                case 35001002:
                case 35120000:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.骑兽技能, 1932016));
                    ret.statups.add(new Pair(MapleBuffStat.indieSpeed, ret.info.get(MapleStatInfo.indieSpeed)));
                    break;
                case 9001004:
                case 9101004:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.隐身术, ret.info.get(MapleStatInfo.x)));
                    break;
                case 4330001:
                    ret.statups.add(new Pair(MapleBuffStat.隐身术, (int) ret.level));
                    ret.statups.add(new Pair(MapleBuffStat.移动速度, 1));
                    break;
                case 4001003:
                case 14001003:
                    ret.statups.add(new Pair(MapleBuffStat.隐身术, ret.info.get(MapleStatInfo.x)));
                    break;
                case 4211003:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.敛财术, ret.info.get(MapleStatInfo.x)));
                    break;
                case 4201011:
                    ret.statups.add(new Pair(MapleBuffStat.金钱护盾, ret.info.get(MapleStatInfo.x)));
                    break;
                case 4111002:
                case 4211008:
                case 4331002:
                case 14111000:
                case 36111006:
                    if (sourceid == 36111006)
                    {
                        ret.info.put(MapleStatInfo.time, 180000);
                    }
                    ret.statups.add(new Pair(MapleBuffStat.影分身, ret.info.get(MapleStatInfo.x)));
                    break;
                case 4221013:
                    ret.statups.add(new Pair(MapleBuffStat.击杀点数, 0));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.x)));
                    break;
                case 22161004:
                    ret.statups.add(new Pair(MapleBuffStat.玛瑙的保佑, ret.info.get(MapleStatInfo.x)));
                    break;
                case 2311002:
                case 3101004:
                case 3201004:
                case 5201008:
                case 35101005:
                    ret.statups.add(new Pair(MapleBuffStat.无形箭弩, ret.info.get(MapleStatInfo.x)));
                    break;
                case 33101003:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.无形箭弩, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    break;
                case 2120012:
                case 2220013:
                case 2320012:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.x)));
                    break;
                case 2120010:
                case 2220010:
                case 2320011:
                    ret.info.put(MapleStatInfo.time, 5000);
                    ret.statups.add(new Pair(MapleBuffStat.神秘瞄准术, ret.info.get(MapleStatInfo.x)));
                    break;
                case 1201011:
                case 1201012:
                case 1211008:
                case 1221004:
                case 21101006:
                    ret.statups.add(new Pair(MapleBuffStat.属性攻击, ret.info.get(MapleStatInfo.x)));
                    break;
                case 2111008:
                case 2211008:
                case 12101005:
                case 22131002:
                    ret.statups.add(new Pair(MapleBuffStat.自然力重置, ret.info.get(MapleStatInfo.x)));
                    break;
                case 3110012:
                    ret.statups.add(new Pair(MapleBuffStat.集中精力, ret.info.get(MapleStatInfo.x)));
                    break;
                case 3111011:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.极限射箭, 1));
                    break;
                case 3211012:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.极限射箭, ret.info.get(MapleStatInfo.x)));
                    break;
                case 3121016:
                    ret.statups.add(new Pair(MapleBuffStat.进阶箭筒, 0));
                    break;
                case 3210013:
                    ret.statups.add(new Pair(MapleBuffStat.伤害置换, 0));
                    break;
                case 5100015:
                case 5110014:
                case 5120018:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.能量获得, ret.info.get(MapleStatInfo.x)));
                    break;
                case 1210016:
                    ret.statups.add(new Pair(MapleBuffStat.祝福护甲, ret.info.get(MapleStatInfo.x).intValue() + 1));
                    break;
                case 1211011:
                    ret.statups.add(new Pair(MapleBuffStat.战斗命令, ret.info.get(MapleStatInfo.x)));
                    break;
                case 1211010:
                    ret.hpR = (ret.info.get(MapleStatInfo.x) / 100.0D);
                    ret.statups.add(new Pair(MapleBuffStat.元气恢复, ret.info.get(MapleStatInfo.y)));
                    break;
                case 1211014:
                    ret.statups.clear();
                    break;
                case 1221015:
                    ret.statups.add(new Pair(MapleBuffStat.自然力重置, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 1101004:
                case 1201004:
                case 1301004:
                case 2101008:
                case 2201010:
                case 2301008:
                case 3101002:
                case 3201002:
                case 4101003:
                case 4201002:
                case 4311009:
                case 5101006:
                case 5201003:
                case 5301002:
                case 5701005:
                case 11101024:
                case 12101004:
                case 13101023:
                case 14101002:
                case 15101022:
                case 22141002:
                case 23101002:
                case 24101005:
                case 27101004:
                case 31001001:
                case 31201002:
                case 32101005:
                case 33101012:
                case 35101006:
                case 36101004:
                case 51101003:
                    ret.statups.add(new Pair(MapleBuffStat.攻击加速, ret.info.get(MapleStatInfo.x)));
                    break;
                case 21001003:
                    ret.statups.add(new Pair(MapleBuffStat.攻击加速, -ret.info.get(MapleStatInfo.y).intValue()));
                    break;
                case 5111007:
                case 5120012:
                case 5211007:
                case 5220014:
                case 5311005:
                case 5320007:
                case 5711011:
                case 5720005:
                case 35111013:
                case 35120014:
                    ret.statups.add(new Pair(MapleBuffStat.幸运骰子, 0));
                    break;
                case 5311004:
                    ret.statups.add(new Pair(MapleBuffStat.随机橡木桶, Randomizer.nextInt(3) + 1));
                    break;
                case 5120011:
                case 5220012:
                case 5720012:
                case 51111003:
                    ret.statups.add(new Pair(MapleBuffStat.反制攻击, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 5121015:
                    ret.statups.add(new Pair(MapleBuffStat.反制攻击, ret.info.get(MapleStatInfo.x)));
                    break;
                case 5121009:
                case 15121005:
                    ret.statups.add(new Pair(MapleBuffStat.极速领域, ret.info.get(MapleStatInfo.x)));
                    break;
                case 5001005:
                    ret.statups.add(new Pair(MapleBuffStat.疾驰速度, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.疾驰跳跃, ret.info.get(MapleStatInfo.y)));
                    break;
                case 1301007:
                case 9001008:
                case 9101008:
                    ret.statups.add(new Pair(MapleBuffStat.MAXHP, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.MAXMP, ret.info.get(MapleStatInfo.x)));
                    break;
                case 1101013:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.斗气集中, 1));
                    break;
                case 21121007:
                    ret.statups.add(new Pair(MapleBuffStat.战神之盾, ret.info.get(MapleStatInfo.x)));
                    break;
                case 5221015:
                case 5721003:
                case 22151002:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.导航辅助, ret.info.get(MapleStatInfo.x)));
                    break;
                case 4341007:
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                    break;
                case 21111009:
                    ret.hpR = (-ret.info.get(MapleStatInfo.x) / 100.0D);
                    break;
                case 4341002:
                    ret.info.put(MapleStatInfo.time, 60000);
                    ret.hpR = (-ret.info.get(MapleStatInfo.x) / 100.0D);
                    ret.statups.add(new Pair(MapleBuffStat.终极斩, ret.info.get(MapleStatInfo.y)));
                    break;
                case 2111007:
                case 2211007:
                case 2311007:
                case 22161005:
                case 32111010:
                    ret.info.put(MapleStatInfo.mpCon, ret.info.get(MapleStatInfo.y));
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.快速移动精通, ret.info.get(MapleStatInfo.x)));
                    ret.monsterStatus.put(MonsterStatus.眩晕, 1);
                    break;
                case 1121000:
                case 1221000:
                case 1321000:
                case 2121000:
                case 2221000:
                case 2321000:
                case 3121000:
                case 3221000:
                case 4121000:
                case 4221000:
                case 4341000:
                case 5121000:
                case 5221000:
                case 5321005:
                case 5721000:
                case 11121000:
                case 13121000:
                case 15121000:
                case 21121000:
                case 22171000:
                case 23121005:
                case 24121008:
                case 27121009:
                case 31121004:
                case 31221008:
                case 32121007:
                case 33121007:
                case 35121007:
                case 36121008:
                case 51121005:
                case 61121014:
                case 65121009:
                    ret.statups.add(new Pair(MapleBuffStat.冒险岛勇士, ret.info.get(MapleStatInfo.x)));
                    break;
                case 3121002:
                case 3221002:
                case 13121005:
                case 33121004:
                    ret.statups.add(new Pair(MapleBuffStat.火眼晶晶, (ret.info.get(MapleStatInfo.x).intValue() << 8) + ret.info.get(MapleStatInfo.criticaldamageMax).intValue()));
                    break;
                case 22151003:
                    ret.statups.add(new Pair(MapleBuffStat.抗魔领域, ret.info.get(MapleStatInfo.x)));
                    break;
                case 2000007:
                case 12000006:
                case 22000002:
                case 32000012:
                    ret.statups.add(new Pair(MapleBuffStat.精灵弱化, ret.info.get(MapleStatInfo.x)));
                    break;
                case 21101003:
                    ret.statups.add(new Pair(MapleBuffStat.战神抗压, ret.info.get(MapleStatInfo.x)));
                    break;
                case 21000000:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.矛连击强化, 10));
                    break;
                case 5220019:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 120000);
                    ret.statups.add(new Pair(MapleBuffStat.精神连接, 10));
                    break;
                case 23101003:
                    ret.statups.add(new Pair(MapleBuffStat.精神连接, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.爆击提升, ret.info.get(MapleStatInfo.x)));
                    break;
                case 21101005:
                case 31121002:
                case 32101004:
                    ret.statups.add(new Pair(MapleBuffStat.连环吸血, ret.info.get(MapleStatInfo.x)));
                    break;
                case 21111001:
                    ret.statups.add(new Pair(MapleBuffStat.战神威势, ret.info.get(MapleStatInfo.x)));
                    break;
                case 23121004:
                    ret.statups.add(new Pair(MapleBuffStat.反制攻击, ret.info.get(MapleStatInfo.damR)));
                    break;
                case 1121016:
                case 1221014:
                case 1321014:
                case 51111005:
                    ret.monsterStatus.put(MonsterStatus.魔击无效, 1);
                    break;
                case 5111010:
                    ret.statups.add(new Pair(MapleBuffStat.伤害吸收, ret.info.get(MapleStatInfo.x)));
                    break;
                case 23111005:
                    ret.statups.add(new Pair(MapleBuffStat.异常抗性, ret.info.get(MapleStatInfo.y)));
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.y)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害吸收, ret.info.get(MapleStatInfo.x)));
                    break;
                case 22131001:
                    ret.statups.add(new Pair(MapleBuffStat.魔法屏障, ret.info.get(MapleStatInfo.x)));
                    break;
                case 22181003:
                    ret.statups.add(new Pair(MapleBuffStat.灵魂之石, 1));
                    break;
                case 32121003:
                    ret.statups.add(new Pair(MapleBuffStat.幻灵飓风, ret.info.get(MapleStatInfo.x)));
                    break;
                case 2311009:
                    ret.statups.add(new Pair(MapleBuffStat.神圣魔法盾, ret.info.get(MapleStatInfo.x)));
                    ret.info.put(MapleStatInfo.cooltime, ret.info.get(MapleStatInfo.y));
                    ret.hpR = (ret.info.get(MapleStatInfo.z) / 100.0D);
                    break;
                case 32111005:
                    ret.info.put(MapleStatInfo.time, 60000);
                    ret.statups.add(new Pair(MapleBuffStat.幻灵霸体, (int) ret.level));
                    break;
                case 1211013:
                    ret.monsterStatus.put(MonsterStatus.物攻, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.物防, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.恐慌, ret.info.get(MapleStatInfo.z));
                    break;
                case 1111008:
                case 2221006:
                case 2311004:
                case 3101005:
                case 4201004:
                case 4221007:
                case 4321006:
                case 5111002:
                case 5301001:
                case 5310008:
                case 5311001:
                case 5311002:
                case 9001020:
                case 9101020:
                case 21111014:
                case 22131000:
                case 22141001:
                case 22151001:
                case 22181001:
                case 31101002:
                case 31111001:
                case 31221003:
                case 32101001:
                case 32121004:
                case 33101001:
                case 33101002:
                case 33111002:
                case 33121002:
                case 35101003:
                case 35111015:
                case 51111007:
                case 51121008:
                case 61101101:
                case 61111101:

                case 2121005:
                case 3111005:
                case 3211005:
                case 23111008:
                case 23111009:
                case 23111010:
                case 33101011:
                case 35111002:
                case 27101101:
                    ret.monsterStatus.put(MonsterStatus.眩晕, 1);
                    break;
                case 1111003:
                case 4321002:
                case 90001004:
                    ret.monsterStatus.put(MonsterStatus.恐慌, ret.info.get(MapleStatInfo.x));
                    break;
                case 4121017:
                    ret.monsterStatus.put(MonsterStatus.标飞挑衅, ret.info.get(MapleStatInfo.x));
                    break;
                case 33121005:
                    ret.monsterStatus.put(MonsterStatus.挑衅, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.魔防, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.物防, ret.info.get(MapleStatInfo.x));
                    break;
                case 31121003:
                    ret.monsterStatus.put(MonsterStatus.挑衅, ret.info.get(MapleStatInfo.w));
                    ret.monsterStatus.put(MonsterStatus.魔防, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.物防, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.魔攻, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.物攻, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.命中, ret.info.get(MapleStatInfo.x));
                    break;
                case 2221011:
                    ret.monsterStatus.put(MonsterStatus.魔击无效, 1);
                    ret.monsterStatus.put(MonsterStatus.眩晕, 1);
                    ret.monsterStatus.put(MonsterStatus.封印, 1);
                    ret.info.put(MapleStatInfo.time, ret.info.get(MapleStatInfo.time).intValue() * 1);
                    break;
                case 23121002:
                    ret.monsterStatus.put(MonsterStatus.物防, -ret.info.get(MapleStatInfo.x).intValue());
                    break;
                case 2121006:
                case 2201008:
                case 2211010:
                case 2221007:
                case 21120006:
                case 22121000:
                case 2211002:
                    ret.monsterStatus.put(MonsterStatus.结冰, 1);
                    ret.info.put(MapleStatInfo.time, ret.info.get(MapleStatInfo.time).intValue() * 2);
                    break;
                case 12101001:
                case 90001002:
                case 61111002:
                    ret.monsterStatus.put(MonsterStatus.速度, ret.info.get(MapleStatInfo.x));
                    break;
                case 5011002:
                    ret.monsterStatus.put(MonsterStatus.速度, ret.info.get(MapleStatInfo.z));
                    break;
                case 1121010:
                case 51121006:
                    ret.statups.add(new Pair(MapleBuffStat.葵花宝典, ret.info.get(MapleStatInfo.x).intValue() * 100 + ret.info.get(MapleStatInfo.mobCount).intValue()));
                    break;
                case 22161002:
                case 23111002:
                    ret.monsterStatus.put(MonsterStatus.鬼刻符, ret.info.get(MapleStatInfo.x));
                    break;
                case 90001003:
                    ret.monsterStatus.put(MonsterStatus.中毒, 1);
                    break;
                case 2311012:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.抵抗之魔法盾, 1));
                    break;
                case 2311003:
                case 9001002:
                case 9101002:
                    ret.statups.add(new Pair(MapleBuffStat.神圣祈祷, ret.info.get(MapleStatInfo.x)));
                    break;
                case 80001034:
                case 80001035:
                case 80001036:
                    ret.statups.add(new Pair(MapleBuffStat.神圣拯救者的祝福, 1));
                    break;
                case 12111002:
                case 90001005:
                    ret.monsterStatus.put(MonsterStatus.封印, 1);
                    break;
                case 4111003:
                case 14111001:
                    ret.monsterStatus.put(MonsterStatus.影网, 1);
                    break;
                case 4111009:
                case 14111007:
                    ret.statups.add(new Pair(MapleBuffStat.暗器伤人, 0));
                    break;
                case 2121004:
                case 2221004:
                case 2321004:
                    ret.hpR = (ret.info.get(MapleStatInfo.y) / 100.0D);
                    ret.mpR = (ret.info.get(MapleStatInfo.y) / 100.0D);
                    ret.statups.add(new Pair(MapleBuffStat.终极无限, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                    break;
                case 22181004:
                    ret.statups.add(new Pair(MapleBuffStat.玛瑙的意志, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                    break;
                case 5321010:
                case 32111014:
                case 51121004:
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                    break;
                case 2121002:
                case 2221002:
                case 2321002:
                    ret.statups.add(new Pair(MapleBuffStat.魔法反击, 1));
                    break;
                case 2321005:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.进阶祝福, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.indieMaxHp, ret.info.get(MapleStatInfo.indieMhp)));
                    ret.statups.add(new Pair(MapleBuffStat.indieMaxMp, ret.info.get(MapleStatInfo.indieMmp)));
                    break;
                case 3121007:
                case 3221006:
                    ret.statups.add(new Pair(MapleBuffStat.敏捷提升, ret.info.get(MapleStatInfo.x)));
                    ret.monsterStatus.put(MonsterStatus.速度, ret.info.get(MapleStatInfo.x));
                    break;
                case 33111004:
                    ret.statups.add(new Pair(MapleBuffStat.致盲, ret.info.get(MapleStatInfo.x)));
                    ret.monsterStatus.put(MonsterStatus.命中, ret.info.get(MapleStatInfo.x));
                    break;
                case 33111007:
                    ret.statups.add(new Pair(MapleBuffStat.移动速度, ret.info.get(MapleStatInfo.z)));
                    ret.statups.add(new Pair(MapleBuffStat.ATTACK_BUFF, ret.info.get(MapleStatInfo.y)));
                    ret.statups.add(new Pair(MapleBuffStat.暴走形态, ret.info.get(MapleStatInfo.x)));
                    break;
                case 33101004:
                    ret.statups.add(new Pair(MapleBuffStat.地雷, ret.info.get(MapleStatInfo.x)));
                    break;
                case 33101005:
                    ret.statups.add(new Pair(MapleBuffStat.DAMAGE_BUFF, ret.info.get(MapleStatInfo.z)));
                    ret.statups.add(new Pair(MapleBuffStat.呼啸_爆击概率, ret.info.get(MapleStatInfo.y)));
                    ret.statups.add(new Pair(MapleBuffStat.呼啸_MaxMp增加, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.呼啸_伤害减少, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.呼啸_回避概率, ret.info.get(MapleStatInfo.x)));
                    break;
                case 33121013:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.indieAllStat, ret.info.get(MapleStatInfo.indieAllStat)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 2301004:
                case 9001003:
                case 9101003:
                    ret.statups.add(new Pair(MapleBuffStat.牧师祝福, (int) ret.level));
                    break;
                case 32120000:
                    ret.info.put(MapleStatInfo.dot, ret.info.get(MapleStatInfo.damage));
                    ret.info.put(MapleStatInfo.dotTime, 3);
                case 32001003:
                case 32110007:
                    ret.info.put(MapleStatInfo.time, 60000);
                    ret.statups.add(new Pair(MapleBuffStat.黑暗灵气, ret.info.get(MapleStatInfo.x)));
                    break;
                case 32110000:
                case 32110008:
                case 32111012:
                    ret.info.put(MapleStatInfo.time, 60000);
                    ret.statups.add(new Pair(MapleBuffStat.蓝色灵气, (int) ret.level));
                    break;
                case 32120001:
                    ret.monsterStatus.put(MonsterStatus.速度, ret.info.get(MapleStatInfo.speed));
                case 32101003:
                case 32110009:
                    ret.info.put(MapleStatInfo.time, 60000);
                    ret.statups.add(new Pair(MapleBuffStat.黄色灵气, (int) ret.level));
                    break;
                case 32121010:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.葵花宝典, ret.info.get(MapleStatInfo.x).intValue() * 100 + ret.info.get(MapleStatInfo.mobCount).intValue()));
                    ret.statups.add(new Pair(MapleBuffStat.爆击概率增加, ret.info.get(MapleStatInfo.z)));
                    ret.statups.add(new Pair(MapleBuffStat.最小爆击伤害, ret.info.get(MapleStatInfo.y)));
                    break;
                case 35101007:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.完美机甲, ret.info.get(MapleStatInfo.x)));
                    break;
                case 31101003:
                    ret.statups.add(new Pair(MapleBuffStat.伤害反击, ret.info.get(MapleStatInfo.y)));
                    break;
                case 31111004:
                case 51111004:
                    ret.statups.add(new Pair(MapleBuffStat.异常抗性, ret.info.get(MapleStatInfo.y)));
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.z)));
                    ret.statups.add(new Pair(MapleBuffStat.黑暗忍耐, ret.info.get(MapleStatInfo.x)));
                    break;
                case 31121005:
                    ret.statups.add(new Pair(MapleBuffStat.黑暗变形, ret.info.get(MapleStatInfo.damR)));
                    ret.statups.add(new Pair(MapleBuffStat.反制攻击, (int) ret.level));
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxHp, ret.info.get(MapleStatInfo.indieMhpR)));
                    break;
                case 31121007:
                    ret.statups.add(new Pair(MapleBuffStat.无限精气, 1));
                    break;
                case 35121006:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.卫星防护_PROC, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.卫星防护_吸收, ret.info.get(MapleStatInfo.y)));
                    break;
                case 20021110:
                case 20031203:
                case 80001040:
                    ret.moveTo = ret.info.get(MapleStatInfo.x);
                    break;
                case 80001089:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.飞行骑乘, 1));
                    break;
                case 35001001:
                case 35101009:
                    ret.info.put(MapleStatInfo.time, 1000);
                    ret.statups.add(new Pair(MapleBuffStat.金属机甲, level));
                    break;
                case 35111004:
                case 35121013:
                    ret.info.put(MapleStatInfo.time, 10000);
                    ret.statups.add(new Pair(MapleBuffStat.金属机甲, level));
                    break;
                case 35121005:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.金属机甲, level));
                    break;
                case 35111016:
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 10001075:
                    ret.statups.add(new Pair(MapleBuffStat.英雄回声, ret.info.get(MapleStatInfo.x)));
                    break;
                case 5221018:
                case 5721009:
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.反制攻击, ret.info.get(MapleStatInfo.damR)));
                    ret.statups.add(new Pair(MapleBuffStat.异常抗性, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.x)));
                    break;
                case 20031209:
                case 20031210:
                    ret.statups.add(new Pair(MapleBuffStat.卡牌审判, 0));
                    break;
                case 20031205:
                    ret.statups.add(new Pair(MapleBuffStat.幻影屏障, ret.info.get(MapleStatInfo.x)));
                    break;
                case 24111003:
                    ret.statups.add(new Pair(MapleBuffStat.异常抗性, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxHp, ret.info.get(MapleStatInfo.indieMhpR)));
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxMp, ret.info.get(MapleStatInfo.indieMmpR)));
                    break;
                case 24121004:
                    ret.statups.add(new Pair(MapleBuffStat.反制攻击, ret.info.get(MapleStatInfo.damR)));
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, ret.info.get(MapleStatInfo.damR)));
                    break;
                case 24111002:
                    ret.info.put(MapleStatInfo.time, 900000);
                    ret.statups.add(new Pair(MapleBuffStat.神秘运气, ret.info.get(MapleStatInfo.x)));
                    break;
                case 50001214:
                case 80001140:
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                    break;
                case 20040216:
                case 20040217:
                    ret.info.put(MapleStatInfo.time, 200000000);
                    ret.statups.add(new Pair(MapleBuffStat.光暗转换, 1));
                    break;
                case 20040219:
                    ret.statups.add(new Pair(MapleBuffStat.光暗转换, 2));
                    break;
                case 27001004:
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxMp, ret.info.get(MapleStatInfo.indieMmpR)));
                    break;
                case 27100003:
                    ret.statups.add(new Pair(MapleBuffStat.黑暗祝福, 1));
                    break;
                case 27110007:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.生命潮汐, 2));
                    break;
                case 27111004:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.抵抗之魔法盾, 3));
                    break;
                case 27111005:
                    ret.statups.add(new Pair(MapleBuffStat.增强_物理防御, (int) ret.indiePdd));
                    ret.statups.add(new Pair(MapleBuffStat.增强_魔法防御, (int) ret.indieMdd));
                    ret.statups.add(new Pair(MapleBuffStat.呼啸_伤害减少, ret.info.get(MapleStatInfo.prop)));
                    break;
                case 27121006:
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.自然力重置, ret.info.get(MapleStatInfo.x)));
                    break;
                case 27121005:
                    ret.statups.add(new Pair(MapleBuffStat.黑暗高潮, 1));
                    break;
                case 60001216:
                case 60001217:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.模式转换, 0));
                    break;
                case 61101004:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.攻击加速, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    break;
                case 61111003:
                    ret.statups.add(new Pair(MapleBuffStat.异常抗性, ret.info.get(MapleStatInfo.terR)));
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.asrR)));
                    break;
                case 61111004:
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 61121009:
                    ret.statups.add(new Pair(MapleBuffStat.强健护甲, ret.info.get(MapleStatInfo.x)));
                    break;
                case 61101002:
                case 61120007:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.剑刃之壁, (int) ret.level));
                    break;
                case 65101002:
                    ret.statups.add(new Pair(MapleBuffStat.伤害置换, ret.info.get(MapleStatInfo.y).intValue() * 1000));
                    break;
                case 65111004:
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                    break;
                case 65121004:
                    ret.statups.add(new Pair(MapleBuffStat.灵魂凝视, ret.info.get(MapleStatInfo.x)));
                    break;
                case 30010242:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.生命潮汐, 3));
                    break;
                case 31011001:
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxHp, ret.info.get(MapleStatInfo.indieMhpR)));
                    break;
                case 31211003:
                    ret.statups.add(new Pair(MapleBuffStat.异常抗性, ret.info.get(MapleStatInfo.y)));
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.z)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害吸收, ret.info.get(MapleStatInfo.x)));
                    break;
                case 31211004:
                    ret.statups.add(new Pair(MapleBuffStat.恶魔恢复, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxHp, ret.info.get(MapleStatInfo.indieMhpR)));
                    break;
                case 31221004:
                    ret.statups.add(new Pair(MapleBuffStat.攻击速度提升, ret.info.get(MapleStatInfo.indieBooster)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 30020232:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.尖兵电力, 1));
                    break;
                case 30021237:
                    ret.info.put(MapleStatInfo.time, 30000);
                    ret.statups.add(new Pair(MapleBuffStat.尖兵飞行, 1));
                    break;
                case 36001002:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    break;
                case 36101002:
                    ret.statups.add(new Pair(MapleBuffStat.爆击提升, ret.info.get(MapleStatInfo.x)));
                    break;
                case 36101003:
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxHp, ret.info.get(MapleStatInfo.indieMhpR)));
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxMp, ret.info.get(MapleStatInfo.indieMmpR)));
                    break;
                case 36121003:
                    ret.statups.add(new Pair(MapleBuffStat.BOSS伤害, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 36121004:
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, ret.info.get(MapleStatInfo.y)));
                    break;
                case 11001021:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.命中增加, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.灵魂融入, (int) ret.level));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    break;
                case 11001022:
                    ret.statups.add(new Pair(MapleBuffStat.元素灵魂, (int) ret.level));
                    break;
                case 11111024:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.物理防御增加, ret.info.get(MapleStatInfo.indiePdd)));
                    ret.statups.add(new Pair(MapleBuffStat.魔法防御增加, ret.info.get(MapleStatInfo.indieMdd)));
                    ret.statups.add(new Pair(MapleBuffStat.indieMaxHp, ret.info.get(MapleStatInfo.indieMhp)));
                    break;
                case 11121006:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                    ret.statups.add(new Pair(MapleBuffStat.indieAllStat, ret.info.get(MapleStatInfo.indieAllStat)));
                    ret.statups.add(new Pair(MapleBuffStat.暴击概率, ret.info.get(MapleStatInfo.indieCr)));
                    break;

                case 11101022:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.月光转换, 1));
                    ret.statups.add(new Pair(MapleBuffStat.暴击概率, ret.info.get(MapleStatInfo.indieCr)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害减少, ret.info.get(MapleStatInfo.y)));
                    break;
                case 5321054:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.月光转换, 1));
                    ret.statups.add(new Pair(MapleBuffStat.伤害减少, ret.info.get(MapleStatInfo.y)));
                    break;
                case 11111022:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.月光转换, 2));
                    ret.statups.add(new Pair(MapleBuffStat.攻击速度提升, ret.info.get(MapleStatInfo.indieBooster)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 13001022:
                    ret.statups.add(new Pair(MapleBuffStat.元素属性, 1));
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    break;
                case 13101024:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.无形箭弩, 1));
                    ret.statups.add(new Pair(MapleBuffStat.爆击提升, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    break;
                case 13111023:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.元素灵魂, (int) ret.level));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    ret.statups.add(new Pair(MapleBuffStat.indieMaxHp, ret.info.get(MapleStatInfo.indieMhp)));
                    ret.statups.add(new Pair(MapleBuffStat.攻击速度提升, ret.info.get(MapleStatInfo.indieBooster)));
                    break;
                case 13120008:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, ret.info.get(MapleStatInfo.ignoreMobpdpR)));
                    ret.statups.add(new Pair(MapleBuffStat.元素灵魂, (int) ret.level));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    ret.statups.add(new Pair(MapleBuffStat.indieMaxHp, ret.info.get(MapleStatInfo.indieMhp)));
                    ret.statups.add(new Pair(MapleBuffStat.攻击速度提升, ret.info.get(MapleStatInfo.indieBooster)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    ret.statups.add(new Pair(MapleBuffStat.状态异常抗性, ret.info.get(MapleStatInfo.indieAsrR)));
                    ret.statups.add(new Pair(MapleBuffStat.所有属性抗性, ret.info.get(MapleStatInfo.indieTerR)));
                    ret.statups.add(new Pair(MapleBuffStat.暴击概率, ret.info.get(MapleStatInfo.indieCr)));
                    break;
                case 13121004:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.敏捷提升, ret.info.get(MapleStatInfo.x)));
                    ret.statups.add(new Pair(MapleBuffStat.命中增加, ret.info.get(MapleStatInfo.y)));
                    ret.statups.add(new Pair(MapleBuffStat.回避增加, ret.info.get(MapleStatInfo.prop)));
                    ret.statups.add(new Pair(MapleBuffStat.百分比MaxHp, ret.info.get(MapleStatInfo.indieMhpR)));
                    break;
                case 15001022:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.元素属性, 1));
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, 5));
                    break;
                case 15111023:
                    ret.statups.add(new Pair(MapleBuffStat.异常抗性, ret.info.get(MapleStatInfo.asrR)));
                    ret.statups.add(new Pair(MapleBuffStat.属性抗性, ret.info.get(MapleStatInfo.terR)));
                    break;
                case 15111024:
                    ret.statups.add(new Pair(MapleBuffStat.伤害吸收, ret.info.get(MapleStatInfo.y)));
                    break;
                case 15121004:
                    ret.statups.add(new Pair(MapleBuffStat.影分身, ret.info.get(MapleStatInfo.x)));
                    break;
                case 15111022:
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.y)));
                    break;
                case 2221005:
                    ret.monsterStatus.put(MonsterStatus.结冰, 1);
                    break;
                case 35111005:
                    ret.monsterStatus.put(MonsterStatus.速度, ret.info.get(MapleStatInfo.x));
                    ret.monsterStatus.put(MonsterStatus.物防, ret.info.get(MapleStatInfo.y));
                    break;
                case 32111006:
                    ret.statups.add(new Pair(MapleBuffStat.幻灵重生, 1));
                    break;
                case 35121003:
                case 35111001:
                case 35111009:
                case 35111010:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    break;
                case 1301013:
                    ret.statups.add(new Pair(MapleBuffStat.灵魂助力, (int) ret.level));
                    break;
                case 35121010:
                    ret.info.put(MapleStatInfo.time, 60000);
                    ret.statups.add(new Pair(MapleBuffStat.DAMAGE_BUFF, ret.info.get(MapleStatInfo.x)));
                    break;
                case 100001268:
                    ret.statups.add(new Pair(MapleBuffStat.冒险岛勇士, ret.info.get(MapleStatInfo.x)));
                    break;
                case 100001263:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.圣洁之力, 1));
                    ret.statups.add(new Pair(MapleBuffStat.indiePad, ret.info.get(MapleStatInfo.indiePad)));
                    ret.statups.add(new Pair(MapleBuffStat.indieMad, ret.info.get(MapleStatInfo.indieMad)));
                    ret.statups.add(new Pair(MapleBuffStat.物理防御增加, ret.info.get(MapleStatInfo.indiePdd)));
                    ret.statups.add(new Pair(MapleBuffStat.魔法防御增加, ret.info.get(MapleStatInfo.indieMdd)));
                    ret.statups.add(new Pair(MapleBuffStat.状态异常抗性, ret.info.get(MapleStatInfo.indieTerR)));
                    ret.statups.add(new Pair(MapleBuffStat.所有属性抗性, ret.info.get(MapleStatInfo.indieAsrR)));
                    break;
                case 100001264:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.神圣迅捷, 1));
                    ret.statups.add(new Pair(MapleBuffStat.indieAcc, ret.info.get(MapleStatInfo.indieAcc)));
                    ret.statups.add(new Pair(MapleBuffStat.indieEva, ret.info.get(MapleStatInfo.indieEva)));
                    ret.statups.add(new Pair(MapleBuffStat.indieJump, ret.info.get(MapleStatInfo.indieJump)));
                    ret.statups.add(new Pair(MapleBuffStat.indieSpeed, ret.info.get(MapleStatInfo.indieSpeed)));
                    ret.statups.add(new Pair(MapleBuffStat.攻击速度提升, ret.info.get(MapleStatInfo.indieBooster)));
                    break;


                case 1121053:
                case 1221053:
                case 1321053:
                case 2121053:
                case 2221053:
                case 2321053:
                case 3121053:
                case 3221053:
                case 4121053:
                case 4221053:
                case 4341053:
                case 5121053:
                case 5221053:
                case 5321053:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害上限, ret.info.get(MapleStatInfo.indieMaxDamageOver)));
                    break;
                case 2321054:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, ret.info.get(MapleStatInfo.ignoreMobpdpR)));
                    ret.statups.add(new Pair(MapleBuffStat.天使复仇, ret.info.get(MapleStatInfo.mobCount)));
                    ret.statups.add(new Pair(MapleBuffStat.indieMad, ret.info.get(MapleStatInfo.indieMad)));
                    ret.statups.add(new Pair(MapleBuffStat.攻击速度提升, ret.info.get(MapleStatInfo.indieBooster)));
                    ret.statups.add(new Pair(MapleBuffStat.伤害上限, ret.info.get(MapleStatInfo.indieMaxDamageOver)));
                    break;
                case 3221054:
                    ret.statups.clear();
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.百分比无视防御, ret.info.get(MapleStatInfo.ignoreMobpdpR)));


                    break;


                case 110001501:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.模式变更, 1));
                    break;
                case 110001502:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.模式变更, 2));
                    break;
                case 110001503:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.模式变更, 3));
                    break;
                case 110001504:
                    ret.info.put(MapleStatInfo.time, 2100000000);
                    ret.statups.add(new Pair(MapleBuffStat.模式变更, 4));
                    break;
                case 110001511:
                    ret.statups.add(new Pair(MapleBuffStat.冒险岛勇士, ret.info.get(MapleStatInfo.x)));
                    break;
            }


            if (GameConstants.is新手职业(sourceid / 10000))
            {
                switch (sourceid % 10000)
                {
                    case 1087:


                    case 1179:


                    case 1085:
                    case 1090:
                        break;


                    case 93:
                        ret.statups.add(new Pair(MapleBuffStat.潜力解放, 1));
                        break;
                    case 99:
                    case 104:
                        ret.monsterStatus.put(MonsterStatus.结冰, 1);
                        ret.info.put(MapleStatInfo.time, ret.info.get(MapleStatInfo.time).intValue() * 2);
                        break;
                    case 103:
                        ret.monsterStatus.put(MonsterStatus.眩晕, 1);
                        break;
                    case 1001:
                        if (ret.is潜入())
                        {
                            ret.statups.add(new Pair(MapleBuffStat.潜入, ret.info.get(MapleStatInfo.x)));
                        }
                        else
                        {
                            ret.statups.add(new Pair(MapleBuffStat.恢复效果, ret.info.get(MapleStatInfo.x)));
                        }
                        break;
                    case 1005:
                        ret.statups.add(new Pair(MapleBuffStat.英雄回声, ret.info.get(MapleStatInfo.x)));
                        break;
                    case 1010:
                        ret.statups.add(new Pair(MapleBuffStat.金刚霸体, 1));
                        break;
                    case 1011:
                        ret.statups.add(new Pair(MapleBuffStat.狂暴战魂, ret.info.get(MapleStatInfo.x)));
                        break;
                    case 1105:
                        ret.info.put(MapleStatInfo.time, 2100000000);
                        ret.statups.add(new Pair(MapleBuffStat.ICE_SKILL, 1));
                        break;
                    case 1026:
                    case 1142:
                        ret.info.put(MapleStatInfo.time, 2100000000);
                        ret.statups.add(new Pair(MapleBuffStat.飞行骑乘, 1));
                        break;
                    case 8001:
                        ret.statups.add(new Pair(MapleBuffStat.无形箭弩, ret.info.get(MapleStatInfo.x)));
                        break;
                    case 8002:
                        ret.statups.add(new Pair(MapleBuffStat.火眼晶晶,
                                (ret.info.get(MapleStatInfo.x).intValue() << 8) + ret.info.get(MapleStatInfo.y).intValue() + ret.info.get(MapleStatInfo.criticaldamageMax).intValue()));
                        break;
                    case 8003:
                        ret.statups.add(new Pair(MapleBuffStat.MAXHP, ret.info.get(MapleStatInfo.x)));
                        ret.statups.add(new Pair(MapleBuffStat.MAXMP, ret.info.get(MapleStatInfo.x)));
                        break;
                    case 8004:
                        ret.statups.add(new Pair(MapleBuffStat.战斗命令, ret.info.get(MapleStatInfo.x)));
                        break;
                    case 8005:
                        ret.statups.clear();
                        ret.statups.add(new Pair(MapleBuffStat.进阶祝福, ret.info.get(MapleStatInfo.x)));
                        ret.statups.add(new Pair(MapleBuffStat.indieMaxHp, ret.info.get(MapleStatInfo.indieMhp)));
                        ret.statups.add(new Pair(MapleBuffStat.indieMaxMp, ret.info.get(MapleStatInfo.indieMmp)));
                        break;
                    case 8006:
                        ret.statups.add(new Pair(MapleBuffStat.极速领域, ret.info.get(MapleStatInfo.x)));
                }
            }
        }
        else
        {
            switch (sourceid)
            {
                case 2022746:
                case 2022747:
                case 2022823:
                    ret.statups.clear();
                    ret.statups.add(new Pair(MapleBuffStat.天使状态, 1));
                    int value = sourceid == 2022823 ? 12 : sourceid == 2022747 ? 10 : sourceid == 2022746 ? 5 : 0;
                    if (value > 0)
                    {
                        ret.statups.add(new Pair(MapleBuffStat.indiePad, value));
                        ret.statups.add(new Pair(MapleBuffStat.indieMad, value));
                    }
                    break;
            }
        }
        if (ret.isPoison())
        {
            ret.monsterStatus.put(MonsterStatus.中毒, 1);
        }
        if (ret.getSummonMovementType() != null)
        {
            ret.statups.add(new Pair(MapleBuffStat.召唤兽, 1));
        }
        if (ret.isMorph())
        {
            ret.statups.add(new Pair(MapleBuffStat.变身术, ret.getMorph()));
            if (ret.is狂龙变形())
            {
                ret.statups.add(new Pair(MapleBuffStat.稳如泰山, ret.info.get(MapleStatInfo.prop)));
                ret.statups.add(new Pair(MapleBuffStat.爆击提升, ret.info.get(MapleStatInfo.cr)));
                ret.statups.add(new Pair(MapleBuffStat.攻击速度提升, ret.info.get(MapleStatInfo.indieBooster)));
                ret.statups.add(new Pair(MapleBuffStat.伤害增加, ret.info.get(MapleStatInfo.indieDamR)));
            }
        }
        ret.statups.trimToSize();
        return ret;
    }

    private static int parseEval(String path, MapleData source, int def, String variables, int level)
    {
        if (variables == null)
        {
            return MapleDataTool.getIntConvert(path, source, def);
        }
        MapleData dd = source.getChildByPath(path);
        if (dd == null)
        {
            return def;
        }
        if (dd.getType() != provider.MapleDataType.STRING)
        {
            return MapleDataTool.getIntConvert(path, source, def);
        }
        String dddd = MapleDataTool.getString(dd).replace(variables, String.valueOf(level));
        if (dddd.substring(0, 1).equals("-"))
        {
            if ((dddd.substring(1, 2).equals("u")) || (dddd.substring(1, 2).equals("d")))
            {
                dddd = "n(" + dddd.substring(1) + ")";
            }
            else
            {
                dddd = "n" + dddd.substring(1);
            }
        }
        else if (dddd.substring(0, 1).equals("="))
        {
            dddd = dddd.substring(1);
        }
        return (int) new CaltechEval(dddd).evaluate();
    }

    public boolean is战法灵气()
    {
        return (this.skill) && ((this.sourceid == 32001003) || (this.sourceid == 32101003) || (this.sourceid == 32111012) || (this.sourceid == 32110000) || (this.sourceid == 32120000) || (this.sourceid == 32120001));
    }

    public boolean isMorph()
    {
        return this.morphId > 0;
    }

    public boolean is天使技能()
    {
        return GameConstants.is天使技能(this.sourceid);
    }

    public server.maps.SummonMovementType getSummonMovementType()
    {
        if (!this.skill)
        {
            return null;
        }
        switch (this.sourceid)
        {
            case 3221014:
            case 4111007:
            case 4211007:
            case 4341006:
            case 5211014:
            case 5320011:
            case 5321003:
            case 5321004:
            case 5321052:
            case 5711001:
            case 13111024:
            case 13120007:
            case 14111010:
            case 33101008:
            case 33111003:
            case 35111002:
            case 35111005:
            case 35111011:
            case 35121003:
            case 35121009:
            case 35121010:
            case 61111002:
                return server.maps.SummonMovementType.不会移动;
            case 3111005:
            case 3211005:
            case 23111008:
            case 23111009:
            case 23111010:
            case 33101011:
                return server.maps.SummonMovementType.跟随并且随机移动打怪;
            case 32111006:
            case 35121011:
                return server.maps.SummonMovementType.自由移动;
            case 1301013:
            case 2111010:
            case 2121005:
            case 2211011:
            case 2221005:
            case 2321003:
            case 12001004:
            case 12111004:
            case 14001005:
            case 35111001:
            case 35111009:
            case 35111010:
                return server.maps.SummonMovementType.跟随移动;
            case 5210015:
            case 5210016:
            case 5210017:
            case 5210018:
                return server.maps.SummonMovementType.未知效果;
        }
        if (is天使技能())
        {
            return server.maps.SummonMovementType.跟随移动;
        }
        return null;
    }

    public boolean is能量获得()
    {
        return (this.skill) && ((this.sourceid == 5100015) || (this.sourceid == 5110014) || (this.sourceid == 5120018));
    }

    private static void addBuffStatPairToListIfNotZero(List<Pair<MapleBuffStat, Integer>> list, MapleBuffStat buffstat, Integer val)
    {
        if (val != 0)
        {
            list.add(new Pair(buffstat, val));
        }
    }

    public boolean is潜入()
    {
        return (this.skill) && ((this.sourceid == 20021001) || (this.sourceid == 20031001) || (this.sourceid == 30001001) || (this.sourceid == 30011001) || (this.sourceid == 30021001) || (this.sourceid == 60001001) || (this.sourceid == 60011001));
    }

    public boolean isPoison()
    {
        return (this.info.get(MapleStatInfo.dot) > 0) && (this.info.get(MapleStatInfo.dotTime) > 0);
    }

    public int getMorph()
    {
        return this.morphId;
    }

    public boolean is狂龙变形()
    {
        return (this.skill) && ((this.sourceid == 61111008) || (this.sourceid == 61120008));
    }

    public static MapleStatEffect loadItemEffectFromData(MapleData source, int itemid)
    {
        return loadFromData(source, itemid, false, false, 1, null, false);
    }

    public static int parseMountInfo(MapleCharacter player, int skillid)
    {
        if ((skillid == 80001000) || (GameConstants.is骑兽技能(skillid)))
        {
            if ((player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -123) != null) && (player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -124) != null))
            {
                return player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -123).getItemId();
            }
            return parseMountInfo_Pure(player, skillid);
        }
        return GameConstants.getMountItem(skillid, player);
    }

    public static int parseMountInfo_Pure(MapleCharacter player, int skillid)
    {
        if ((skillid == 80001000) || (GameConstants.is骑兽技能(skillid)))
        {
            if ((player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -18) != null) && (player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -19) != null))
            {
                return player.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -18).getItemId();
            }
            return 0;
        }
        return GameConstants.getMountItem(skillid, player);
    }

    private static int makeHealHP(double rate, double stat, double lowerfactor, double upperfactor)
    {
        return (int) (Math.random() * ((int) (stat * upperfactor * rate) - (int) (stat * lowerfactor * rate) + 1) + (int) (stat * lowerfactor * rate));
    }

    public void applyPassive(MapleCharacter applyto, server.maps.MapleMapObject obj)
    {
        if (makeChanceResult())
        {
            switch (this.sourceid)
            {
                case 2100000:
                case 2200000:
                case 2300000:
                    if ((obj == null) || (obj.getType() != server.maps.MapleMapObjectType.MONSTER))
                    {
                        return;
                    }
                    MapleMonster mob = (MapleMonster) obj;
                    if (!mob.getStats().isBoss())
                    {
                        int absorbMp = Math.min((int) (mob.getMobMaxMp() * (getX() / 100.0D)), mob.getMp());
                        if (absorbMp > 0)
                        {
                            mob.setMp(mob.getMp() - absorbMp);
                            applyto.getStat().setMp(applyto.getStat().getMp() + absorbMp, applyto);
                            applyto.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(this.sourceid, 1, applyto.getLevel(), this.level));
                            applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showBuffeffect(applyto.getId(), this.sourceid, 1, applyto.getLevel(), this.level), false);
                        }
                    }

                    break;
            }

        }
    }

    public boolean makeChanceResult()
    {
        return (this.info.get(MapleStatInfo.prop) >= 100) || (Randomizer.nextInt(100) < this.info.get(MapleStatInfo.prop));
    }

    public int getX()
    {
        return this.info.get(MapleStatInfo.x);
    }

    public boolean applyTo(MapleCharacter chr)
    {
        return applyTo(chr, chr, true, null, getDuration(chr), false);
    }

    public boolean applyTo(MapleCharacter chr, boolean passive)
    {
        return applyTo(chr, chr, true, null, getDuration(chr), passive);
    }

    public boolean applyTo(MapleCharacter chr, Point pos)
    {
        return applyTo(chr, chr, true, pos, getDuration(chr), false);
    }

    public boolean applyTo(MapleCharacter applyfrom, MapleCharacter applyto, boolean primary, Point pos, int newDuration)
    {
        return applyTo(applyfrom, applyto, primary, pos, newDuration, false);
    }

    public boolean applyTo(MapleCharacter applyfrom, MapleCharacter applyto, boolean primary, Point pos, int newDuration, boolean passive)
    {
        if ((!applyfrom.isAdmin()) && (applyfrom.getMap().isMarketMap()))
        {
            applyfrom.getClient().getSession().write(MaplePacketCreator.enableActions());
            return false;
        }
        if ((is群体治愈()) && ((applyfrom.getMapId() == 749040100) || (applyto.getMapId() == 749040100) || (applyfrom.getMap().isPvpMaps())))
        {
            applyfrom.getClient().getSession().write(MaplePacketCreator.enableActions());
            return false;
        }
        if (((isSoaring_Mount()) && (applyfrom.getBuffedValue(MapleBuffStat.骑兽技能) == null)) || ((isSoaring_Normal()) && (!applyfrom.getMap().canSoar())))
        {
            applyfrom.getClient().getSession().write(MaplePacketCreator.enableActions());
            return false;
        }
        if ((this.sourceid == 4341006) && (applyfrom.getBuffedValue(MapleBuffStat.影分身) == null))
        {
            applyfrom.getClient().getSession().write(MaplePacketCreator.enableActions());
            return false;
        }
        if ((this.sourceid == 33101008) && ((applyfrom.getBuffedValue(MapleBuffStat.地雷) == null) || (applyfrom.getBuffedValue(MapleBuffStat.召唤兽) != null) || (!applyfrom.canSummon())))
        {
            applyfrom.getClient().getSession().write(MaplePacketCreator.enableActions());
            return false;
        }
        if ((this.sourceid == 33101004) && (applyfrom.getMap().isTown()))
        {
            applyfrom.dropMessage(5, "无法在村庄中使用的技能。");
            applyfrom.getClient().getSession().write(MaplePacketCreator.enableActions());
            return false;
        }
        int hpchange = calcHPChange(applyfrom, primary);
        int mpchange = calcMPChange(applyfrom, primary);
        PlayerStats stat = applyto.getStat();
        if (primary)
        {
            if ((this.info.get(MapleStatInfo.itemConNo) != 0) && (!applyto.inPVP()))
            {
                if (!applyto.haveItem(this.info.get(MapleStatInfo.itemCon), this.info.get(MapleStatInfo.itemConNo), false, true))
                {
                    applyto.getClient().getSession().write(MaplePacketCreator.enableActions());
                    return false;
                }
                MapleInventoryManipulator.removeById(applyto.getClient(), constants.ItemConstants.getInventoryType(this.info.get(MapleStatInfo.itemCon)), this.info.get(MapleStatInfo.itemCon),
                        this.info.get(MapleStatInfo.itemConNo), false, true);
            }
        }
        else if ((!primary) && (is复活术()))
        {
            hpchange = stat.getMaxHp();
            applyto.setStance(0);
        }
        if ((is净化()) && (makeChanceResult()))
        {
            applyto.dispelDebuffs();
        }
        else if (is勇士的意志())
        {
            applyto.dispelDebuffs();
        }
        else if (this.cureDebuffs.size() > 0)
        {
            for (MapleDisease debuff : this.cureDebuffs)
            {
                applyfrom.dispelDebuff(debuff);
            }
        }
        else if (is负荷释放())
        {
            if (applyto.get超越数值() < 20)
            {
                applyfrom.dropMessage(5, "该技能处于未激活状态。");
                return false;
            }
            applyto.cancelEffectFromBuffStat(MapleBuffStat.恶魔超越);
            hpchange = stat.getMaxHp() / 100 * getX();
        }
        else if (is额外供给())
        {
            applyto.addPowerCount(this.info.get(MapleStatInfo.x));
        }
        else if (is龙之献祭())
        {
            applyto.dispelSkill(1301013);
            if (applyto.skillisCooling(1321013))
            {
                applyto.removeCooldown(1321013);
                applyto.getClient().getSession().write(MaplePacketCreator.skillCooldown(1321013, 0));
            }
        }
        Object hpmpupdate = new EnumMap(client.MapleStat.class);
        if (hpchange != 0)
        {
            if ((hpchange < 0) && (-hpchange > stat.getHp()) && (!applyto.hasDisease(MapleDisease.ZOMBIFY)))
            {
                applyto.getClient().getSession().write(MaplePacketCreator.enableActions());
                return false;
            }
            stat.setHp(stat.getHp() + hpchange, applyto);
        }
        if (mpchange != 0)
        {
            if ((mpchange < 0) && (-mpchange > stat.getMp()))
            {
                applyto.getClient().getSession().write(MaplePacketCreator.enableActions());
                return false;
            }
            stat.setMp(stat.getMp() + mpchange, applyto);
            ((Map) hpmpupdate).put(client.MapleStat.MP, (long) stat.getMp());
        }
        ((Map) hpmpupdate).put(client.MapleStat.HP, (long) stat.getHp());
        applyto.getClient().getSession().write(MaplePacketCreator.updatePlayerStats((Map) hpmpupdate, true, applyto));

        int powerchange = calcPowerChange(applyfrom);
        if (powerchange != 0)
        {
            if ((powerchange < 0) && (-powerchange > applyfrom.getSpecialStat().getPowerCount()))
            {
                applyfrom.dropMessage(5, "施展技能所需的支援能量不足。");
                return false;
            }
            applyto.addPowerCount(powerchange);
        }

        int combochange = calcAranComboChange(applyfrom);
        if (combochange != 0)
        {
            if ((combochange < 0) && (-combochange > applyfrom.getAranCombo()))
            {
                applyfrom.dropMessage(5, "使用技能所需的连击数不足。");
                return false;
            }
            applyto.gainAranCombo(combochange, true);
        }
        int types;
        if (this.expinc != 0)
        {
            applyto.gainExp(this.expinc, true, true, false);
            applyto.getClient().getSession().write(MaplePacketCreator.showSpecialEffect(21));
        }
        else
        {
            int mobid;
            if (this.sourceid / 10000 == 238)
            {
                if (GameConstants.GMS)
                {
                    MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
                    mobid = ii.getCardMobId(this.sourceid);
                    if (mobid > 0)
                    {
                        boolean done = applyto.getMonsterBook().monsterCaught(applyto.getClient(), mobid, server.life.MapleLifeFactory.getMonsterStats(mobid).getName());
                        applyto.getClient().getSession().write(MaplePacketCreator.getCard(done ? this.sourceid : 0, 1));
                    }
                }
            }
            else if (isReturnScroll())
            {
                applyReturnScroll(applyto);
            }
            else if ((this.useLevel > 0) && (!this.skill))
            {
                applyto.setExtractor(new server.maps.MapleExtractor(applyto, this.sourceid, this.useLevel * 50, 1440));
                applyto.getMap().spawnExtractor(applyto.getExtractor());
            }
            else
            {
                if (is迷雾爆发())
                {
                    int i = this.info.get(MapleStatInfo.y);
                    for (MapleMist mist : applyto.getMap().getAllMistsThreadsafe())
                    {
                        if ((mist.getOwnerId() == applyto.getId()) && (mist.getSourceSkill().getId() == 2111003))
                        {
                            if (mist.getSchedule() != null)
                            {
                                mist.getSchedule().cancel(false);
                                mist.setSchedule(null);
                            }
                            if (mist.getPoisonSchedule() != null)
                            {
                                mist.getPoisonSchedule().cancel(false);
                                mist.setPoisonSchedule(null);
                            }
                            applyto.getMap().broadcastMessage(MaplePacketCreator.removeMist(mist.getObjectId(), true));
                            applyto.getMap().removeMapObject(mist);
                            i--;
                            if (i <= 0)
                            {
                                break;
                            }
                        }
                    }
                }
                else if (this.cosmetic > 0)
                {
                    if (this.cosmetic >= 30000)
                    {
                        applyto.setHair(this.cosmetic);
                        applyto.updateSingleStat(client.MapleStat.发型, this.cosmetic);
                    }
                    else if (this.cosmetic >= 20000)
                    {
                        applyto.setFace(this.cosmetic);
                        applyto.updateSingleStat(client.MapleStat.脸型, this.cosmetic);
                    }
                    else if (this.cosmetic < 100)
                    {
                        applyto.setSkinColor((byte) this.cosmetic);
                        applyto.updateSingleStat(client.MapleStat.皮肤, this.cosmetic);
                    }
                    applyto.equipChanged();
                }
                else if (this.bs > 0)
                {
                    if (!applyto.inPVP())
                    {
                        return false;
                    }
                    int xx = Integer.parseInt(applyto.getEventInstance().getProperty(String.valueOf(applyto.getId())));
                    applyto.getEventInstance().setProperty(String.valueOf(applyto.getId()), String.valueOf(xx + this.bs));
                    applyto.getClient().getSession().write(MaplePacketCreator.getPVPScore(xx + this.bs, false));
                }
                else if (this.info.get(MapleStatInfo.iceGageCon) > 0)
                {
                    if (!applyto.inPVP())
                    {
                        return false;
                    }
                    int x = Integer.parseInt(applyto.getEventInstance().getProperty("icegage"));
                    if (x < this.info.get(MapleStatInfo.iceGageCon))
                    {
                        return false;
                    }
                    applyto.getEventInstance().setProperty("icegage", String.valueOf(x - this.info.get(MapleStatInfo.iceGageCon)));
                    applyto.getClient().getSession().write(MaplePacketCreator.getPVPIceGage(x - this.info.get(MapleStatInfo.iceGageCon)));
                    applyto.applyIceGage(x - this.info.get(MapleStatInfo.iceGageCon));
                }
                else if (this.recipe > 0)
                {
                    if ((applyto.getSkillLevel(this.recipe) > 0) || (applyto.getProfessionLevel(this.recipe / 10000 * 10000) < this.reqSkillLevel))
                    {
                        return false;
                    }
                    applyto.changeSingleSkillLevel(SkillFactory.getCraft(this.recipe), Integer.MAX_VALUE, this.recipeUseCount, this.recipeValidDay > 0 ?
                            System.currentTimeMillis() + this.recipeValidDay * 24L * 60L * 60L * 1000L : -1L);
                }
                else if (is斗气重生())
                {
                    int addCombo = this.info.get(MapleStatInfo.y);
                    if (applyto.getTotalSkillLevel(21120045) > 0)
                    {
                        addCombo *= 2;
                    }
                    applyto.gainAranCombo(addCombo, false);
                    applyto.setLastComboTime(System.currentTimeMillis());
                    applyto.getClient().getSession().write(MaplePacketCreator.rechargeCombo(applyto.getAranCombo()));
                    SkillFactory.getSkill(21000000).getEffect(10).applyComboBuff(applyto, applyto.getAranCombo());
                }
                else if (is飞龙传动())
                {
                    MaplePortal portal = applyto.getMap().getPortal(Randomizer.nextInt(applyto.getMap().getPortals().size()));
                    if (portal != null)
                    {
                        applyto.getClient().getSession().write(MaplePacketCreator.dragonBlink(portal.getId()));
                        applyto.getMap().movePlayer(applyto, portal.getPosition());
                        applyto.checkFollow();
                    }
                }
                else if (is卡牌审判())
                {
                    if (applyto.getCardStack() < applyto.getCarteByJob())
                    {
                        applyfrom.dropMessage(5, "必须等卡片值充满后，才能使用技能。");
                        return false;
                    }
                    applyto.setCardStack(0);
                }
                else if (is狂龙变形())
                {
                    if (applyto.getMorphCount() < 1000)
                    {
                        applyfrom.dropMessage(5, "变形值不足，无法使用该技能。");
                        return false;
                    }
                    applyto.setMorphCount(0);
                }
                else if (is暗器伤人())
                {
                    MapleInventory use = applyto.getInventory(MapleInventoryType.USE);
                    boolean itemz = false;
                    int bulletConsume = this.info.get(MapleStatInfo.bulletConsume);
                    for (int i = 0; i < use.getSlotLimit(); i++)
                    {
                        Item item = use.getItem((byte) i);
                        if ((item != null) && (constants.ItemConstants.is飞镖道具(item.getItemId())) && (item.getQuantity() >= bulletConsume))
                        {
                            MapleInventoryManipulator.removeFromSlot(applyto.getClient(), MapleInventoryType.USE, (short) i, (short) bulletConsume, false, true);
                            itemz = true;
                            break;
                        }
                    }

                    if (!itemz) return false;
                }
                else
                {
                    MapleInventory use;
                    int bulletConsume;
                    if (is无限子弹())
                    {
                        use = applyto.getInventory(MapleInventoryType.USE);
                        boolean itemz = false;
                        bulletConsume = this.info.get(MapleStatInfo.bulletConsume);
                        for (int i = 0; i < use.getSlotLimit(); i++)
                        {
                            Item item = use.getItem((byte) i);
                            if ((item != null) && (constants.ItemConstants.is子弹道具(item.getItemId())) && (item.getQuantity() >= bulletConsume))
                            {
                                MapleInventoryManipulator.removeFromSlot(applyto.getClient(), MapleInventoryType.USE, (short) i, (short) bulletConsume, false, true);
                                itemz = true;
                                break;
                            }
                        }

                        if (!itemz)
                        {
                            return false;
                        }
                    }
                    else if ((this.cp != 0) && (applyto.getCarnivalParty() != null))
                    {
                        applyto.getCarnivalParty().addCP(applyto, this.cp);
                        applyto.CPUpdate(false, applyto.getAvailableCP(), applyto.getTotalCP(), 0);
                        for (MapleCharacter chr : applyto.getMap().getCharactersThreadsafe())
                            chr.CPUpdate(true, applyto.getCarnivalParty().getAvailableCP(), applyto.getCarnivalParty().getTotalCP(), applyto.getCarnivalParty().getTeam());
                    }
                    else
                    {
                        MapleCarnivalFactory.MCSkill skil;
                        MapleDisease dis;
                        if ((this.nuffSkill != 0) && (applyto.getParty() != null))
                        {
                            skil = MapleCarnivalFactory.getInstance().getSkill(this.nuffSkill);
                            if (skil != null)
                            {
                                dis = skil.getDisease();
                                for (MapleCharacter chr : applyto.getMap().getCharactersThreadsafe())
                                {
                                    if (((applyto.getParty() == null) || (chr.getParty() == null) || (chr.getParty().getId() != applyto.getParty().getId())) && ((skil.targetsAll) || (Randomizer.nextBoolean())))
                                    {
                                        if (dis == null)
                                        {
                                            chr.dispel();
                                        }
                                        else if (skil.getSkill() == null)
                                        {
                                            chr.giveDebuff(dis, 1, 30000L, dis.getDisease(), 1);
                                        }
                                        else
                                        {
                                            chr.giveDebuff(dis, skil.getSkill());
                                        }
                                        if (!skil.targetsAll) break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (((this.effectedOnEnemy > 0) || (this.effectedOnAlly > 0)) && (primary) && (applyto.inPVP()))
                            {
                                types = Integer.parseInt(applyto.getEventInstance().getProperty("type"));
                                if ((types > 0) || (this.effectedOnEnemy > 0))
                                {
                                    for (MapleCharacter chr : applyto.getMap().getCharactersThreadsafe())
                                    {
                                        if ((chr.getId() != applyto.getId()) && (this.effectedOnAlly > 0 ? chr.getTeam() != applyto.getTeam() : (chr.getTeam() != applyto.getTeam()) || (types == 0)))
                                        {
                                            applyTo(applyto, chr, false, pos, newDuration);
                                        }
                                    }
                                }
                            }
                            else if ((this.mobSkill > 0) && (this.mobSkillLevel > 0) && (primary) && (applyto.inPVP()))
                            {
                                if (this.effectedOnEnemy > 0)
                                {
                                    types = Integer.parseInt(applyto.getEventInstance().getProperty("type"));
                                    for (MapleCharacter chr : applyto.getMap().getCharactersThreadsafe())
                                    {
                                        if ((chr.getId() != applyto.getId()) && ((chr.getTeam() != applyto.getTeam()) || (types == 0)))
                                        {
                                            chr.disease(this.mobSkill, this.mobSkillLevel);
                                        }
                                    }
                                }
                                else if ((this.sourceid == 2910000) || (this.sourceid == 2910001))
                                {
                                    applyto.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(this.sourceid, 13, applyto.getLevel(), this.level));
                                    applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showBuffeffect(applyto.getId(), this.sourceid, 13, applyto.getLevel(), this.level), false);
                                    applyto.getClient().getSession().write(MaplePacketCreator.showOwnCraftingEffect("UI/UIWindow2.img/CTF/Effect", 0, 0));
                                    applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showCraftingEffect(applyto.getId(), "UI/UIWindow2.img/CTF/Effect", 0, 0), false);
                                    if (applyto.getTeam() == this.sourceid - 2910000)
                                    {
                                        if (this.sourceid == 2910000)
                                        {
                                            applyto.getEventInstance().broadcastPlayerMsg(-7, "The Red Team's flag has been restored.");
                                        }
                                        else
                                        {
                                            applyto.getEventInstance().broadcastPlayerMsg(-7, "The Blue Team's flag has been restored.");
                                        }
                                        applyto.getMap().spawnAutoDrop(this.sourceid, applyto.getMap().getGuardians().get(this.sourceid - 2910000).left);
                                    }
                                    else
                                    {
                                        applyto.disease(this.mobSkill, this.mobSkillLevel);
                                        if (this.sourceid == 2910000)
                                        {
                                            applyto.getEventInstance().setProperty("redflag", String.valueOf(applyto.getId()));
                                            applyto.getEventInstance().broadcastPlayerMsg(-7, "The Red Team's flag has been captured!");
                                            applyto.getClient().getSession().write(MaplePacketCreator.showOwnCraftingEffect("UI/UIWindow2.img/CTF/Tail/Red", 600000, 0));
                                            applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showCraftingEffect(applyto.getId(), "UI/UIWindow2.img/CTF/Tail/Red", 600000, 0), false);
                                        }
                                        else
                                        {
                                            applyto.getEventInstance().setProperty("blueflag", String.valueOf(applyto.getId()));
                                            applyto.getEventInstance().broadcastPlayerMsg(-7, "The Blue Team's flag has been captured!");
                                            applyto.getClient().getSession().write(MaplePacketCreator.showOwnCraftingEffect("UI/UIWindow2.img/CTF/Tail/Blue", 600000, 0));
                                            applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showCraftingEffect(applyto.getId(), "UI/UIWindow2.img/CTF/Tail/Blue", 600000, 0), false);
                                        }
                                    }
                                }
                                else
                                {
                                    applyto.disease(this.mobSkill, this.mobSkillLevel);
                                }
                            }
                            else if ((this.randomPickup != null) && (this.randomPickup.size() > 0))
                            {
                                MapleItemInformationProvider.getInstance().getItemEffect(this.randomPickup.get(Randomizer.nextInt(this.randomPickup.size()))).applyTo(applyto);
                            }
                        }
                    }
                }
            }
        }
        for (Map.Entry<client.MapleTraitType, Integer> traitType : this.traits.entrySet())
            applyto.getTrait(traitType.getKey()).addExp(traitType.getValue(), applyto);
        int newId;
        if (is机械传送门())
        {
            newId = 0;
            boolean applyBuff = false;
            server.maps.MechDoor remove;
            if (applyto.getMechDoors().size() >= 2)
            {
                remove = applyto.getMechDoors().remove(0);
                newId = remove.getId();
                applyto.getMap().broadcastMessage(MaplePacketCreator.removeMechDoor(remove, true));
                applyto.getMap().removeMapObject(remove);
            }
            else
            {
                for (server.maps.MechDoor d : applyto.getMechDoors())
                {
                    if (d.getId() == newId)
                    {
                        applyBuff = true;
                        newId = 1;
                        break;
                    }
                }
            }
            server.maps.MechDoor door = new server.maps.MechDoor(applyto, new Point(pos == null ? applyto.getTruePosition() : pos), newId);
            applyto.getMap().spawnMechDoor(door);
            applyto.addMechDoor(door);
            applyto.getClient().getSession().write(MaplePacketCreator.mechPortal(door.getTruePosition()));
            if (!applyBuff)
            {
                return true;
            }
        }
        if ((primary) && (this.availableMap != null))
        {
            for (Pair<Integer, Integer> e : this.availableMap)
            {
                if ((applyto.getMapId() < e.left) || (applyto.getMapId() > e.right))
                {
                    applyto.getClient().getSession().write(MaplePacketCreator.enableActions());
                    return true;
                }
            }
        }
        if ((this.overTime) && (!is能量获得()))
        {
            if ((getSummonMovementType() != null) || ((this.sourceid == 32111006) && (applyfrom.getBuffedValue(MapleBuffStat.幻灵重生) != null)))
            {
                applySummonEffect(applyfrom, primary, pos, newDuration);
            }
            else
            {
                if (applyfrom.isShowPacket())
                {
                    applyfrom.dropSpouseMessage(10, "开始 => applyBuffEffect ID: " + this.sourceid + " 持续时间: " + newDuration);
                }
                applyBuffEffect(applyfrom, applyto, primary, newDuration, passive);
            }
        }
        if (this.skill)
        {
            removeMonsterBuff(applyfrom);
        }
        if (primary)
        {
            if (((this.overTime) || (is群体治愈())) && (!is能量获得()))
            {
                applyBuff(applyfrom, newDuration);
            }
            if (isMonsterBuff()) applyMonsterBuff(applyfrom);
        }
        Rectangle bounds;
        if (is时空门())
        {
            server.maps.MapleDoor door = new server.maps.MapleDoor(applyto, new Point(pos == null ? applyto.getTruePosition() : pos), this.sourceid);
            if (door.getTownPortal() != null)
            {
                applyto.getMap().spawnDoor(door);
                applyto.addDoor(door);
                server.maps.MapleDoor townDoor = new server.maps.MapleDoor(door);
                applyto.addDoor(townDoor);
                door.getTown().spawnDoor(townDoor);
                if (applyto.getParty() != null)
                {
                    applyto.silentPartyUpdate();
                }
            }
            else
            {
                applyto.dropMessage(5, "You may not spawn a door because all doors in the town are taken.");
            }
        }
        else if (isMist())
        {
            bounds = calculateBoundingBox(pos != null ? pos : applyfrom.getPosition(), applyfrom.isFacingLeft());
            MapleMist mist = new MapleMist(bounds, applyfrom, this);
            if (getCooldown(applyfrom) > 0)
            {
                applyfrom.getClient().getSession().write(MaplePacketCreator.skillCooldown(this.sourceid, getCooldown(applyfrom)));
                applyfrom.addCooldown(this.sourceid, System.currentTimeMillis(), getCooldown(applyfrom) * 1000);
            }
            applyfrom.getMap().spawnMist(mist, getDuration(), false);
        }
        else if (is伺机待发())
        {
            for (client.MapleCoolDownValueHolder i : applyto.getCooldowns())
            {
                if (i.skillId != 5121010)
                {
                    applyto.removeCooldown(i.skillId);
                    applyto.getClient().getSession().write(MaplePacketCreator.skillCooldown(i.skillId, 0));
                }
            }
        }
        if ((this.fatigueChange != 0) && (applyto.getSummonedFamiliar() != null) && ((this.familiars == null) || (this.familiars.contains(applyto.getSummonedFamiliar().getFamiliar()))))
        {
            applyto.getSummonedFamiliar().addFatigue(applyto, this.fatigueChange);
        }


        if (this.rewardMeso != 0)
        {
            applyto.gainMeso(this.rewardMeso, false);
        }


        if ((this.rewardItem != null) && (this.totalprob > 0))
        {
            for (Triple<Integer, Integer, Integer> reward : this.rewardItem)
            {
                if ((MapleInventoryManipulator.checkSpace(applyto.getClient(), reward.left, reward.mid, "")) && (reward.right > 0) && (Randomizer.nextInt(this.totalprob) < reward.right))
                {
                    if (constants.ItemConstants.getInventoryType(reward.left) == MapleInventoryType.EQUIP)
                    {
                        Item item = MapleItemInformationProvider.getInstance().getEquipById(reward.left);
                        item.setGMLog("Reward item (effect): " + this.sourceid + " on " + tools.FileoutputUtil.CurrentReadable_Date());
                        MapleInventoryManipulator.addbyItem(applyto.getClient(), item);
                    }
                    else
                    {
                        MapleInventoryManipulator.addById(applyto.getClient(), reward.left, reward.mid.shortValue(),
                                "Reward item (effect): " + this.sourceid + " " + "on " + tools.FileoutputUtil.CurrentReadable_Date());
                    }
                }
            }
        }
        if ((this.familiarTarget == 2) && (applyfrom.getParty() != null) && (primary))
        {
            for (handling.world.party.MaplePartyCharacter mpc : applyfrom.getParty().getMembers())
            {
                if ((mpc.getId() != applyfrom.getId()) && (mpc.getChannel() == applyfrom.getClient().getChannel()) && (mpc.getMapid() == applyfrom.getMapId()) && (mpc.isOnline()))
                {
                    MapleCharacter player = applyfrom.getMap().getCharacterById(mpc.getId());
                    if (player != null)
                    {
                        applyTo(applyfrom, player, false, null, newDuration);
                    }
                }
            }
        }
        else if ((this.familiarTarget == 3) && (primary))
        {
            for (MapleCharacter player : applyfrom.getMap().getCharactersThreadsafe())
            {
                if (player.getId() != applyfrom.getId())
                {
                    applyTo(applyfrom, player, false, null, newDuration);
                }
            }
        }

        if ((applyfrom.getJob() == 2711) || (applyfrom.getJob() == 2712))
        {
            applyfrom.check生命潮汐();
        }
        return true;
    }

    public boolean applySummonEffect(MapleCharacter applyto, boolean primary, Point pos, int newDuration)
    {
        server.maps.SummonMovementType summonMovementType = getSummonMovementType();
        if ((summonMovementType == null) || ((this.sourceid == 32111006) && (applyto.getBuffedValue(MapleBuffStat.幻灵重生) == null)))
        {
            return false;
        }
        byte[] buff = null;
        int summonSkillId = this.sourceid;
        int localDuration = newDuration;
        List<Pair<MapleBuffStat, Integer>> localstatups = this.statups;
        if (applyto.isShowPacket())
        {
            applyto.dropSpouseMessage(10, "开始召唤召唤兽 - 召唤兽技能: " + summonSkillId + " 持续时间: " + newDuration);
        }

        if (this.sourceid != 35111002)
        {
            applyto.cancelEffect(this, true, -1L, localstatups);
        }
        if (is集合船员())
        {
            int skilllevel = applyto.getTotalSkillLevel(5220019);
            if (skilllevel > 0)
            {
                SkillFactory.getSkill(5220019).getEffect(skilllevel).applyBuffEffect(applyto, applyto, primary, newDuration);
            }
        }

        MapleSummon tosummon = new MapleSummon(applyto, summonSkillId, getLevel(), new Point(pos == null ? applyto.getTruePosition() : pos), summonMovementType);
        if (!tosummon.is替身术())
        {
            applyto.getCheatTracker().resetSummonAttack();
        }
        applyto.getMap().spawnSummon(tosummon);
        applyto.addSummon(tosummon);
        if (this.info.get(MapleStatInfo.hcSummonHp) > 0)
        {
            tosummon.setSummonHp(this.info.get(MapleStatInfo.hcSummonHp));
        }
        else if (this.sourceid == 3221014)
        {
            tosummon.setSummonHp(this.info.get(MapleStatInfo.x));
        }

        if (this.sourceid == 13120007)
        {
            applyto.dispelSkill(13111024);
        }
        else if (this.sourceid == 4341006)
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.影分身);
        }
        else if (this.sourceid == 1301013)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.灵魂助力, (int) this.level));
            buff = BuffPacket.giveBuff(this.sourceid, localDuration, stat, this, applyto);
        }

        long startTime = System.currentTimeMillis();
        if (localDuration > 0)
        {
            CancelEffectAction cancelAction = new CancelEffectAction(applyto, this, startTime, localstatups);
            java.util.concurrent.ScheduledFuture<?> schedule = Timer.BuffTimer.getInstance().schedule(cancelAction, localDuration);
            applyto.registerEffect(this, startTime, schedule, localstatups, false, localDuration, applyto.getId());
        }

        int cooldown = getCooldown(applyto);
        if (cooldown > 0)
        {
            if (this.sourceid == 35111002)
            {
                List<Integer> count = new ArrayList<>();
                List<MapleSummon> summons = applyto.getSummonsReadLock();
                try
                {
                    for (MapleSummon summon : summons)
                    {
                        if (summon.getSkillId() == this.sourceid)
                        {
                            count.add(summon.getObjectId());
                        }
                    }
                }
                finally
                {
                    applyto.unlockSummonsReadLock();
                }
                if (count.size() == 3)
                {
                    applyto.getClient().getSession().write(MaplePacketCreator.skillCooldown(this.sourceid, cooldown));
                    applyto.addCooldown(this.sourceid, startTime, cooldown * 1000);
                    applyto.getMap().broadcastMessage(MaplePacketCreator.teslaTriangle(applyto.getId(), count.get(0), count.get(1), count.get(2)));
                }
            }
            else
            {
                applyto.getClient().getSession().write(MaplePacketCreator.skillCooldown(this.sourceid, cooldown));
                applyto.addCooldown(this.sourceid, startTime, cooldown * 1000);
            }
        }
        if (buff != null)
        {
            applyto.getClient().getSession().write(buff);
        }
        return true;
    }

    public boolean applyReturnScroll(MapleCharacter applyto)
    {
        if (this.moveTo != -1)
        {
            if ((this.sourceid != 2031010) || (this.sourceid != 2030021))
            {
                MapleMap target = null;
                boolean nearest = false;
                if (this.moveTo == 999999999)
                {
                    nearest = true;
                    if (applyto.getMap().getReturnMapId() != 999999999)
                    {
                        target = applyto.getMap().getReturnMap();
                    }
                }
                else
                {
                    target = handling.channel.ChannelServer.getInstance(applyto.getClient().getChannel()).getMapFactory().getMap(this.moveTo);
                    if ((target.getId() == 931050500) && (target != applyto.getMap()))
                    {
                        applyto.changeMap(target, target.getPortal(0));
                        return true;
                    }
                    int targetMapId = target.getId() / 10000000;
                    int charMapId = applyto.getMapId() / 10000000;
                    if ((targetMapId != 60) && (charMapId != 61) && (targetMapId != 21) && (charMapId != 20) && (targetMapId != 12) && (charMapId != 10) && (targetMapId != 10) && (charMapId != 12) && (targetMapId != charMapId))
                    {
                        log.info("玩家 " + applyto.getName() + " 尝试回到一个非法的位置 (" + applyto.getMapId() + "->" + target.getId() + ")");
                        return false;
                    }
                }


                if ((target == applyto.getMap()) || ((nearest) && (applyto.getMap().isTown())))
                {
                    return false;
                }
                applyto.changeMap(target, target.getPortal(0));
                return true;
            }
        }
        return false;
    }

    private boolean is灵魂之石()
    {
        return (this.skill) && (this.sourceid == 22181003);
    }

    public boolean is寒冰灵气()
    {
        return (this.skill) && (this.sourceid == 2221054);
    }

    private void applyBuff(MapleCharacter applyfrom, int newDuration)
    {
        if (is灵魂之石())
        {
            if (applyfrom.getParty() != null)
            {
                int membrs = 0;
                for (MapleCharacter chr : applyfrom.getMap().getCharactersThreadsafe())
                {
                    if ((chr.getParty() != null) && (chr.getParty().getId() == applyfrom.getParty().getId()) && (chr.isAlive()))
                    {
                        membrs++;
                    }
                }
                List<MapleCharacter> awarded = new ArrayList<>();
                while (awarded.size() < Math.min(membrs, this.info.get(MapleStatInfo.y)))
                {
                    for (MapleCharacter chr : applyfrom.getMap().getCharactersThreadsafe())
                    {
                        if ((chr != null) && (chr.isAlive()) && (chr.getParty() != null) && (chr.getParty().getId() == applyfrom.getParty().getId()) && (!awarded.contains(chr)) && (Randomizer.nextInt(this.info.get(MapleStatInfo.y)) == 0))
                        {
                            awarded.add(chr);
                        }
                    }
                }
                for (MapleCharacter chr : awarded)
                {
                    applyTo(applyfrom, chr, false, null, newDuration);
                    chr.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(this.sourceid, 2, applyfrom.getLevel(), this.level));
                    chr.getMap().broadcastMessage(chr, MaplePacketCreator.showBuffeffect(chr.getId(), this.sourceid, 2, applyfrom.getLevel(), this.level), false);
                }
            }
        }
        else if ((isPartyBuff()) && ((applyfrom.getParty() != null) || (isGmBuff()) || (applyfrom.inPVP())))
        {
            Rectangle bounds = calculateBoundingBox(applyfrom.getTruePosition(), applyfrom.isFacingLeft());
            List<MapleMapObject> affecteds = applyfrom.getMap().getMapObjectsInRect(bounds, java.util.Arrays.asList(server.maps.MapleMapObjectType.PLAYER));
            for (server.maps.MapleMapObject affectedmo : affecteds)
            {
                MapleCharacter affected = (MapleCharacter) affectedmo;
                if ((affected.getId() != applyfrom.getId()) && ((isGmBuff()) || ((applyfrom.inPVP()) && (affected.getTeam() == applyfrom.getTeam()) && (Integer.parseInt(applyfrom.getEventInstance().getProperty("type")) != 0)) || ((applyfrom.getParty() != null) && (affected.getParty() != null) && (applyfrom.getParty().getId() == affected.getParty().getId()))))
                {
                    if (((is复活术()) && (!affected.isAlive())) || ((!is复活术()) && (affected.isAlive())))
                    {
                        applyTo(applyfrom, affected, false, null, newDuration);
                        affected.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(this.sourceid, 2, applyfrom.getLevel(), this.level));
                        affected.getMap().broadcastMessage(affected, MaplePacketCreator.showBuffeffect(affected.getId(), this.sourceid, 2, applyfrom.getLevel(), this.level), false);
                    }
                    if (is伺机待发())
                    {
                        for (client.MapleCoolDownValueHolder i : affected.getCooldowns())
                            if (i.skillId != 5121010)
                            {
                                affected.removeCooldown(i.skillId);
                                affected.getClient().getSession().write(MaplePacketCreator.skillCooldown(i.skillId, 0));
                            }
                    }
                }
            }
        }
    }

    private void removeMonsterBuff(MapleCharacter applyfrom)
    {
        List<MonsterStatus> cancel = new ArrayList<>();
        switch (this.sourceid)
        {
            case 1121016:
            case 1221014:
            case 1321014:
            case 51111005:
                cancel.add(MonsterStatus.物防提升);
                cancel.add(MonsterStatus.魔防提升);
                cancel.add(MonsterStatus.物攻提升);
                cancel.add(MonsterStatus.魔攻提升);
                break;
            default:
                return;
        }
        Rectangle bounds = calculateBoundingBox(applyfrom.getTruePosition(), applyfrom.isFacingLeft());
        List<server.maps.MapleMapObject> affected = applyfrom.getMap().getMapObjectsInRect(bounds, java.util.Arrays.asList(server.maps.MapleMapObjectType.MONSTER));
        int i = 0;
        for (server.maps.MapleMapObject mo : affected)
        {
            if (makeChanceResult())
            {
                for (MonsterStatus stat : cancel)
                {
                    ((MapleMonster) mo).cancelStatus(stat);
                }
            }
            i++;
            if (i >= this.info.get(MapleStatInfo.mobCount))
            {
                break;
            }
        }
    }

    public void applyMonsterBuff(MapleCharacter applyfrom)
    {
        Rectangle bounds = calculateBoundingBox(applyfrom.getTruePosition(), applyfrom.isFacingLeft());
        boolean pvp = applyfrom.inPVP();
        server.maps.MapleMapObjectType types = pvp ? server.maps.MapleMapObjectType.PLAYER : server.maps.MapleMapObjectType.MONSTER;
        List<server.maps.MapleMapObject> affected = this.sourceid == 35111005 ? applyfrom.getMap().getMapObjectsInRange(applyfrom.getTruePosition(), Double.POSITIVE_INFINITY,
                java.util.Arrays.asList(types)) : applyfrom.getMap().getMapObjectsInRect(bounds, java.util.Arrays.asList(types));
        int i = 0;
        for (server.maps.MapleMapObject mo : affected)
        {
            if (makeChanceResult())
            {
                for (Map.Entry<MonsterStatus, Integer> stat : getMonsterStati().entrySet())
                {
                    if (pvp)
                    {
                        MapleCharacter chr = (MapleCharacter) mo;
                        MapleDisease d = MonsterStatus.getLinkedDisease(stat.getKey());
                        if (d != null)
                        {
                            chr.giveDebuff(d, stat.getValue(), getDuration(), d.getDisease(), 1);
                        }
                    }
                    else
                    {
                        MapleMonster mons = (MapleMonster) mo;
                        if ((this.sourceid == 35111005) && (mons.getStats().isBoss()))
                        {
                            break;
                        }
                        mons.applyStatus(applyfrom, new client.status.MonsterStatusEffect(stat.getKey(), stat.getValue(), this.sourceid, null, false), isPoison(), isSubTime(this.sourceid) ?
                                getSubTime() : getDuration(), true, this);
                    }
                }
                if ((pvp) && (this.skill))
                {
                    MapleCharacter chr = (MapleCharacter) mo;
                    handleExtraPVP(applyfrom, chr);
                }
            }
            i++;
            if ((i >= this.info.get(MapleStatInfo.mobCount)) && (this.sourceid != 35111005))
            {
                break;
            }
        }
    }

    public boolean isSubTime(int source)
    {
        switch (source)
        {
            case 1211013:
            case 23111008:
            case 23111009:
            case 23111010:
            case 31101003:
            case 31121003:
            case 31121005:
            case 61111002:
                return true;
        }
        return false;
    }

    public void handleExtraPVP(MapleCharacter applyfrom, MapleCharacter chr)
    {
        if ((this.sourceid == 1211013) || ((GameConstants.is新手职业(this.sourceid / 10000)) && (this.sourceid % 10000 == 104)))
        {
            long starttime = System.currentTimeMillis();
            int localsourceid = this.sourceid;
            List<Pair<MapleBuffStat, Integer>> localstatups;
            if (this.sourceid == 1211013)
            {
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.压制术, (int) this.level));
            }
            else
            {
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.变身术, this.info.get(MapleStatInfo.x)));
            }
            chr.getClient().getSession().write(BuffPacket.giveBuff(localsourceid, getDuration(), localstatups, this, chr));
            chr.registerEffect(this, starttime, Timer.BuffTimer.getInstance().schedule(new CancelEffectAction(chr, this, starttime, localstatups), isSubTime(this.sourceid) ? getSubTime() :
                    getDuration()), localstatups, false, getDuration(), applyfrom.getId());
        }
    }

    public Rectangle calculateBoundingBox(Point posFrom, boolean facingLeft)
    {
        return calculateBoundingBox(posFrom, facingLeft, this.lt, this.rb, this.info.get(MapleStatInfo.range));
    }

    public Rectangle calculateBoundingBox(Point posFrom, boolean facingLeft, int addedRange)
    {
        return calculateBoundingBox(posFrom, facingLeft, this.lt, this.rb, this.info.get(MapleStatInfo.range) + addedRange);
    }

    public static Rectangle calculateBoundingBox(Point posFrom, boolean facingLeft, Point lt, Point rb, int range)
    {
        if ((lt == null) || (rb == null)) return new java.awt.Rectangle((facingLeft ? 65336 - range : 0) + posFrom.x, -100 - range + posFrom.y, 200 + range, 100 + range);
        Point myrb;
        Point mylt;
        if (facingLeft)
        {
            mylt = new Point(lt.x + posFrom.x - range, lt.y + posFrom.y);
            myrb = new Point(rb.x + posFrom.x, rb.y + posFrom.y);
        }
        else
        {
            myrb = new Point(lt.x * -1 + posFrom.x + range, rb.y + posFrom.y);
            mylt = new Point(rb.x * -1 + posFrom.x, lt.y + posFrom.y);
        }
        return new java.awt.Rectangle(mylt.x, mylt.y, myrb.x - mylt.x, myrb.y - mylt.y);
    }

    public double getMaxDistanceSq()
    {
        int maxX = Math.max(Math.abs(this.lt == null ? 0 : this.lt.x), Math.abs(this.rb == null ? 0 : this.rb.x));
        int maxY = Math.max(Math.abs(this.lt == null ? 0 : this.lt.y), Math.abs(this.rb == null ? 0 : this.rb.y));
        return maxX * maxX + maxY * maxY;
    }

    public void silentApplyBuff(MapleCharacter chr, long starttime, int localDuration, List<Pair<MapleBuffStat, Integer>> statup, int chrId)
    {
        int maskedDuration = 0;
        int newDuration = (int) (starttime + localDuration - System.currentTimeMillis());
        if (is终极无限())
        {
            maskedDuration = alchemistModifyVal(chr, 4000, false);
        }
        java.util.concurrent.ScheduledFuture<?> schedule = Timer.BuffTimer.getInstance().schedule(new CancelEffectAction(chr, this, starttime, statup), maskedDuration > 0 ? maskedDuration :
                newDuration);
        chr.registerEffect(this, starttime, schedule, statup, true, localDuration, chrId);
        server.maps.SummonMovementType summonMovementType = getSummonMovementType();
        if (summonMovementType != null)
        {
            MapleSummon summon = new MapleSummon(chr, this, chr.getTruePosition(), summonMovementType);
            if (!summon.is替身术())
            {
                chr.getCheatTracker().resetSummonAttack();
                chr.getMap().spawnSummon(summon);
                chr.addSummon(summon);
                summon.addSummonHp(this.info.get(MapleStatInfo.x).shortValue());
                if (is灵魂助力())
                {
                    summon.addSummonHp(1);
                }
            }
        }
    }

    public void applyComboBuff(MapleCharacter applyto, int combo)
    {
        int combocount = Math.min(combo, 9999);
        int localDuration = this.info.get(MapleStatInfo.time);
        List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.矛连击强化, combocount));
        applyto.getClient().getSession().write(BuffPacket.giveBuff(this.sourceid, localDuration, stat, this, applyto));
        long starttime = System.currentTimeMillis();
        applyto.cancelEffect(this, true, -1L, stat);


        applyto.registerEffect(this, starttime, null, applyto.getId());
    }

    public void applyEnergyBuff(MapleCharacter applyto)
    {
        long startTime = System.currentTimeMillis();
        int localDuration = this.info.get(MapleStatInfo.time);
        int energy = this.info.get(MapleStatInfo.x);
        List<Pair<MapleBuffStat, Integer>> localstatups = Collections.singletonList(new Pair(MapleBuffStat.能量获得, energy));
        applyto.cancelEffect(this, true, -1L, localstatups);
        CancelEffectAction cancelAction = new CancelEffectAction(applyto, this, startTime, localstatups);
        java.util.concurrent.ScheduledFuture<?> schedule = Timer.BuffTimer.getInstance().schedule(cancelAction, localDuration);
        applyto.registerEffect(this, startTime, schedule, localstatups, false, localDuration, applyto.getId());

        applyto.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(this.sourceid, 2, applyto.getLevel(), this.level));
        applyto.getClient().getSession().write(BuffPacket.giveEnergyCharge(energy, this.sourceid, false, false));

        applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showBuffeffect(applyto.getId(), this.sourceid, 2, applyto.getLevel(), this.level), false);
        applyto.getMap().broadcastMessage(applyto, BuffPacket.showEnergyCharge(applyto.getId(), energy, this.sourceid, false, false), false);
    }

    public void apply超越状态(MapleCharacter applyto)
    {
        List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.恶魔超越, 1));
        applyto.getClient().getSession().write(BuffPacket.give恶魔复仇者超越(1, 30010230));
        long starttime = System.currentTimeMillis();
        applyto.cancelEffect(this, true, -1L, stat);
        CancelEffectAction cancelAction = new CancelEffectAction(applyto, this, starttime, stat);
        java.util.concurrent.ScheduledFuture<?> schedule = Timer.BuffTimer.getInstance().schedule(cancelAction, starttime + 2100000000L - System.currentTimeMillis());
        applyto.registerEffect(this, starttime, schedule, stat, false, 2100000000, applyto.getId());
    }

    private void applyBuffEffect(MapleCharacter applyfrom, MapleCharacter applyto, boolean primary, int newDuration)
    {
        applyBuffEffect(applyfrom, applyto, primary, newDuration, false);
    }

    private void applyBuffEffect(MapleCharacter applyfrom, MapleCharacter applyto, boolean primary, int newDuration, boolean passive)
    {
        if ((!applyto.isAdmin()) && (applyto.getMap().isMarketMap()))
        {
            applyto.getClient().getSession().write(MaplePacketCreator.enableActions());
            return;
        }
        int localDuration = newDuration;
        if (primary)
        {
            localDuration = is战法灵气() ? 180000 : Math.max(newDuration, alchemistModifyVal(applyfrom, localDuration, false));
        }
        List<Pair<MapleBuffStat, Integer>> localstatups = this.statups;
        List<Pair<MapleBuffStat, Integer>> maskedStatups = null;
        boolean normal = true;
        boolean showEffect = primary;
        int maskedDuration = 0;
        byte[] buff = null;
        byte[] foreignbuff = null;
        if (is幸运骰子())
        {
            int dice = Randomizer.nextInt(6) + 1;
            applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showDiceEffect(applyto.getId(), this.sourceid, dice, -1, this.level), false);
            applyto.getClient().getSession().write(MaplePacketCreator.showOwnDiceEffect(this.sourceid, dice, -1, this.level));
            if (dice <= 1)
            {
                applyto.dropMessage(-10, "幸运骰子技能失败。");
                return;
            }
            localstatups = Collections.singletonList(new Pair(MapleBuffStat.幸运骰子, dice));
            applyto.dropMessage(-10, "幸运骰子技能发动了[" + dice + "]号效果。");
            applyto.getClient().getSession().write(BuffPacket.giveDice(dice, this.sourceid, localDuration, localstatups));
            normal = false;
            showEffect = false;
        }
        else if (is双幸运骰子())
        {
            int dice = Randomizer.nextInt(6) + 1;
            int dice2 = makeChanceResult() ? Randomizer.nextInt(6) + 1 : 0;
            applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showDiceEffect(applyto.getId(), this.sourceid, dice, dice2 > 0 ? -1 : 0, this.level), false);
            applyto.getClient().getSession().write(MaplePacketCreator.showOwnDiceEffect(this.sourceid, dice, dice2 > 0 ? -1 : 0, this.level));
            if ((dice <= 1) && (dice2 <= 1))
            {
                applyto.dropMessage(-10, "双幸运骰子技能失败。");
                return;
            }
            int buffid = dice2 <= 1 ? dice : dice <= 1 ? dice2 : dice == dice2 ? dice * 100 : dice * 10 + dice2;
            if (buffid >= 100)
            {
                applyto.dropMessage(-10, "双幸运骰子技能发动了[" + buffid / 100 + "]号效果。");
            }
            else if (buffid >= 10)
            {
                applyto.dropMessage(-10, "双幸运骰子技能发动了[" + buffid / 10 + "]号效果。");
                applyto.dropMessage(-10, "双幸运骰子技能发动了[" + buffid % 10 + "]号效果。");
            }
            else
            {
                applyto.dropMessage(-10, "双幸运骰子技能发动了[" + buffid + "]号效果。");
            }
            localstatups = Collections.singletonList(new Pair(MapleBuffStat.幸运骰子, buffid));
            applyto.getClient().getSession().write(BuffPacket.giveDice(buffid, this.sourceid, localDuration, localstatups));
            normal = false;
            showEffect = false;
        }
        else if (is卡牌审判())
        {
            int dice = Randomizer.nextInt(this.sourceid == 20031209 ? 2 : 4) + 1;
            int theStat = this.info.get(MapleStatInfo.v);
            switch (dice)
            {
                case 1:
                    theStat = this.info.get(MapleStatInfo.v);
                    break;
                case 2:
                    theStat = this.info.get(MapleStatInfo.w);
                    break;
                case 3:
                    theStat = this.info.get(MapleStatInfo.x) * 100 + this.info.get(MapleStatInfo.y);
                    break;
                case 4:
                    theStat = this.info.get(MapleStatInfo.s);
                    break;
                case 5:
                    theStat = this.info.get(MapleStatInfo.z);
            }

            applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showDiceEffect(applyto.getId(), this.sourceid, dice, -1, this.level), false);
            applyto.getClient().getSession().write(MaplePacketCreator.showOwnDiceEffect(this.sourceid, dice, -1, this.level));
            localstatups = Collections.singletonList(new Pair(MapleBuffStat.卡牌审判, dice));
            applyto.getClient().getSession().write(BuffPacket.give卡牌审判(this.sourceid, localDuration, localstatups, theStat));
            normal = false;
            showEffect = false;
        }
        else if (is极速领域())
        {
            buff = BuffPacket.givePirateBuff(this.statups, localDuration / 1000, this.sourceid);
        }
        else if (is极限射箭())
        {
            if (applyto.getBuffedValue(MapleBuffStat.极限射箭) != null)
            {
                applyto.cancelEffectFromBuffStat(MapleBuffStat.极限射箭);
            }
        }
        else if (is疾驰())
        {
            buff = BuffPacket.givePirateBuff(this.statups, localDuration / 1000, this.sourceid);
            foreignbuff = BuffPacket.giveForeignDash(this.statups, localDuration / 1000, applyto.getId(), this.sourceid);
        }
        else if (is导航辅助())
        {
            if (applyto.getFirstLinkMid() > 0)
            {
                applyto.cancelEffectFromBuffStat(MapleBuffStat.导航辅助);
                buff = BuffPacket.give导航辅助(this.sourceid, applyto.getFirstLinkMid(), 1);
            }

        }
        else if (is神秘瞄准术())
        {
            if ((applyto.getFirstLinkMid() > 0) && (!applyto.getAllLinkMid().isEmpty()))
            {
                buff = BuffPacket.give神秘瞄准术(applyto.getAllLinkMid().size() * this.info.get(MapleStatInfo.x), this.sourceid, localDuration);
            }

        }
        else if (is神圣之火())
        {
            localstatups = new ArrayList<>();
            int addHp = applyfrom.getTotalSkillLevel(1320045) > 0 ? 20 : 0;
            int addMp = applyfrom.getTotalSkillLevel(1320044) > 0 ? 20 : 0;
            localstatups.add(new Pair(MapleBuffStat.MAXHP, this.info.get(MapleStatInfo.x).intValue() + addHp));
            localstatups.add(new Pair(MapleBuffStat.MAXMP, this.info.get(MapleStatInfo.x).intValue() + addMp));
        }
        else if (is潜入())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.潜入, 0));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is隐身术())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.隐身术, 0));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 2311009)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.神圣魔法盾, 0));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 23111005)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.伤害吸收, this.info.get(MapleStatInfo.x)));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 23101003)
        {
            List<Pair<MapleBuffStat, Integer>> stat = new ArrayList<>();
            stat.add(new Pair(MapleBuffStat.精神连接, this.info.get(MapleStatInfo.x)));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 1211008)
        {
            if ((applyto.getBuffedValue(MapleBuffStat.属性攻击) != null) && (applyto.getBuffSource(MapleBuffStat.属性攻击) != this.sourceid))
            {
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.雷鸣冲击, 1));
            }
            buff = BuffPacket.giveBuff(this.sourceid, localDuration, localstatups, this, applyto);
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.属性攻击, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is元气恢复())
        {
            int healRate = applyto.getBuffedIntValue(MapleBuffStat.元气恢复) + 5;
            localstatups = Collections.singletonList(new Pair(MapleBuffStat.元气恢复, healRate));
        }
        else if (this.sourceid == 35111004)
        {
            if ((applyto.getBuffedValue(MapleBuffStat.金属机甲) != null) && (applyto.getBuffSource(MapleBuffStat.金属机甲) == 35121005))
            {
                SkillFactory.getSkill(35121013).getEffect(this.level).applyBuffEffect(applyfrom, applyto, primary, newDuration);
                return;
            }
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.金属机甲, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if ((this.sourceid == 35001001) || (this.sourceid == 35101009) || (this.sourceid == 35121013) || (this.sourceid == 35121005))
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.金属机甲, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 1210016)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.祝福护甲, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is斗气集中())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.斗气集中, 0));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if ((this.sourceid == 3101004) || (this.sourceid == 3201004))
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.无形箭弩, 0));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 2321005)
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.牧师祝福);
        }
        else if (is影分身())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.影分身, this.info.get(MapleStatInfo.x)));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is侠盗本能())
        {
            int killSpree = Math.min(applyto.getBuffedIntValue(MapleBuffStat.击杀点数), 5);
            if (passive)
            {
                if (killSpree >= 5)
                {
                    return;
                }
                killSpree++;
                if (applyto.isShowPacket())
                {
                    applyto.dropSpouseMessage(10, "当前击杀点数: " + killSpree);
                }
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.击杀点数, killSpree));
                foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), localstatups, this);
            }
            else
            {
                applyto.cancelEffectFromBuffStat(MapleBuffStat.击杀点数);
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.indiePad, this.info.get(MapleStatInfo.x).intValue() + this.info.get(MapleStatInfo.kp).intValue() * killSpree));
            }
        }
        else if (this.sourceid == 15001022)
        {
            if (passive)
            {
                localDuration = 30000;
                int raidenCount = applyto.getStat().raidenCount;
                int count = Math.min(applyto.getBuffedIntValue(MapleBuffStat.百分比无视防御) / 5, raidenCount);
                if ((count < raidenCount) && (raidenCount > 0))
                {
                    count++;
                }
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.百分比无视防御, count * 5));
            }
            else
            {
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.元素属性, 1));
            }
        }
        else if (this.sourceid == 15111022)
        {
            int count = Math.min(applyto.getBuffedIntValue(MapleBuffStat.百分比无视防御) / 5, applyto.getStat().raidenCount);
            int skillLevel = applyto.getTotalSkillLevel(15120003);
            if (count < (skillLevel > 0 ? 2 : 3))
            {
                applyto.dropMessage(5, "雷电增益不足，无法使用技能。");
                return;
            }
            int value = this.info.get(MapleStatInfo.y);
            if (skillLevel > 0)
            {
                MapleStatEffect effect = SkillFactory.getSkill(15120003).getEffect(skillLevel);
                value = effect.getY();
                localDuration = effect.getDuration(applyto);
            }
            applyto.cancelEffectFromBuffStat(MapleBuffStat.百分比无视防御);
            localstatups = Collections.singletonList(new Pair(MapleBuffStat.伤害增加, value * count));
        }
        else if (is属性攻击())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.属性攻击, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is终极无限())
        {
            maskedDuration = alchemistModifyVal(applyfrom, 4000, false);
        }
        else if (is尖兵支援())
        {
            maskedDuration = 4000;
        }
        else if (is光暗转换())
        {
            buff = BuffPacket.giveLuminousState(this.sourceid, localDuration, applyfrom);
        }
        else if (is黑暗高潮())
        {
            buff = BuffPacket.giveDarkCrescendo(this.sourceid, localDuration, 1);
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.黑暗高潮, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is黑暗祝福())
        {
            localDuration = 2100000000;
            applyto.getClient().getSession().write(MaplePacketCreator.showBlessOfDarkness(this.sourceid));
        }
        else if (is圣洁之力())
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.神圣迅捷);
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.圣洁之力, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is神圣迅捷())
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.圣洁之力);
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.神圣迅捷, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 1121010)
        {
            applyto.handleOrbconsume(10);
        }
        else if ((this.sourceid == 61101002) || (this.sourceid == 61120007))
        {
            if ((this.sourceid == 61101002) && (applyfrom.getTotalSkillLevel(61120007) > 0))
            {
                SkillFactory.getSkill(61120007).getEffect(applyfrom.getTotalSkillLevel(61120007)).applyBuffEffect(applyfrom, applyto, primary, newDuration);
                return;
            }
            Item weapon = applyto.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -11);
            if (weapon != null)
            {
                client.inventory.Equip skin = (client.inventory.Equip) weapon;
                int itemId = skin.getItemSkin() % 10000 > 0 ? skin.getItemSkin() : weapon.getItemId();
                List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.剑刃之壁, (int) this.level));
                buff = BuffPacket.give剑刃之壁(this.sourceid, localDuration, localstatups, itemId);
                foreignbuff = BuffPacket.show剑刃之壁(applyto.getId(), this.sourceid, stat, itemId);
            }
            else
            {
                applyto.dropMessage(5, "佩戴的武器无法使用此技能。");
                return;
            }
        }
        else if ((this.sourceid == 35001002) || (this.sourceid == 35120000))
        {
            if ((this.sourceid == 35001002) && (applyfrom.getTotalSkillLevel(35120000) > 0))
            {
                localstatups = new ArrayList(SkillFactory.getSkill(35120000).getEffect(applyfrom.getTotalSkillLevel(35120000)).statups);
            }
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.骑兽技能, 0));
            foreignbuff = BuffPacket.showMonsterRiding(applyto.getId(), stat, 35001002, 1932016);
        }
        else if (this.sourceid == 32111005)
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.幻灵霸体);

            Pair<MapleBuffStat, Integer> statt;
            if (applyfrom.getStatForBuff(MapleBuffStat.黑暗灵气) != null)
            {
                int sourcez = 32001003;
                statt = new Pair(MapleBuffStat.黑暗灵气, this.level + 10 + applyto.getTotalSkillLevel(32001003));
            }
            else
            {
                if (applyfrom.getStatForBuff(MapleBuffStat.黄色灵气) != null)
                {
                    int sourcez = 32101003;
                    statt = new Pair(MapleBuffStat.黄色灵气, applyto.getTotalSkillLevel(32101003));
                }
                else
                {
                    if (applyfrom.getStatForBuff(MapleBuffStat.蓝色灵气) != null)
                    {
                        int sourcez = 32111012;
                        localDuration = 10000;
                        statt = new Pair(MapleBuffStat.蓝色灵气, applyto.getTotalSkillLevel(32111012));
                    }
                    else
                    {
                        return;
                    }
                }
            }
            int sourcez = 0;
            localstatups = new ArrayList<>();
            localstatups.add(new Pair(MapleBuffStat.幻灵霸体, (int) this.level));
            applyto.getClient().getSession().write(BuffPacket.giveBuff(this.sourceid, localDuration, localstatups, this, applyto));
            localstatups.add(statt);
            applyto.cancelEffectFromBuffStat(MapleBuffStat.蓝色灵气, applyfrom.getId());
            applyto.cancelEffectFromBuffStat(MapleBuffStat.黄色灵气, applyfrom.getId());
            applyto.cancelEffectFromBuffStat(MapleBuffStat.黑暗灵气, applyfrom.getId());
            applyto.getClient().getSession().write(BuffPacket.giveBuff(sourcez, localDuration, Collections.singletonList(statt), this, applyto));
            normal = false;
        }
        else if ((this.sourceid == 32001003) || (this.sourceid == 32110007))
        {
            if ((this.sourceid == 32001003) && (applyfrom.getTotalSkillLevel(32120000) > 0))
            {
                SkillFactory.getSkill(32120000).getEffect(applyfrom.getTotalSkillLevel(32120000)).applyBuffEffect(applyfrom, applyto, primary, newDuration);
                return;
            }
            applyto.cancelEffectFromBuffStat(MapleBuffStat.黑暗灵气);
            applyto.cancelEffectFromBuffStat(MapleBuffStat.幻灵霸体);
            List<Pair<MapleBuffStat, Integer>> statt = Collections.singletonList(new Pair(this.sourceid == 32110007 ? MapleBuffStat.幻灵霸体 : MapleBuffStat.黑暗灵气, (int) this.level));
            applyto.getClient().getSession().write(BuffPacket.giveBuff(this.sourceid, localDuration, statt, this, applyto));
            statt = Collections.singletonList(new Pair(MapleBuffStat.黑暗灵气, this.info.get(MapleStatInfo.x)));
            applyto.getClient().getSession().write(BuffPacket.giveBuff(this.sourceid, localDuration, statt, this, applyto));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), statt, this);
            normal = false;
        }
        else if (this.sourceid == 32120000)
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.黑暗灵气);
            applyto.cancelEffectFromBuffStat(MapleBuffStat.幻灵霸体);
            List<Pair<MapleBuffStat, Integer>> statt = Collections.singletonList(new Pair(MapleBuffStat.黑暗灵气, this.info.get(MapleStatInfo.x)));
            buff = BuffPacket.giveBuff(this.sourceid, localDuration, statt, this, applyto);
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), statt, this);
        }
        else if ((this.sourceid == 32111012) || (this.sourceid == 32110008) || (this.sourceid == 32110000))
        {
            if (this.sourceid == 32110008)
            {
                localDuration = 10000;
            }
            if ((this.sourceid == 32111012) && (applyfrom.getTotalSkillLevel(32110000) > 0))
            {
                SkillFactory.getSkill(32110000).getEffect(applyfrom.getTotalSkillLevel(32110000)).applyBuffEffect(applyfrom, applyto, primary, newDuration);
                return;
            }
            applyto.cancelEffectFromBuffStat(MapleBuffStat.蓝色灵气);
            applyto.cancelEffectFromBuffStat(MapleBuffStat.幻灵霸体);
            List<Pair<MapleBuffStat, Integer>> statt = Collections.singletonList(new Pair(MapleBuffStat.蓝色灵气, (int) this.level));
            buff = BuffPacket.giveBuff(this.sourceid, localDuration, statt, this, applyto);
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), statt, this);
        }
        else if ((this.sourceid == 32101003) || (this.sourceid == 32110009) || (this.sourceid == 32120001))
        {
            if ((this.sourceid == 32101003) && (applyfrom.getTotalSkillLevel(32120001) > 0))
            {
                SkillFactory.getSkill(32120001).getEffect(applyfrom.getTotalSkillLevel(32120001)).applyBuffEffect(applyfrom, applyto, primary, newDuration);
                return;
            }
            applyto.cancelEffectFromBuffStat(MapleBuffStat.黄色灵气);
            applyto.cancelEffectFromBuffStat(MapleBuffStat.幻灵霸体);
            List<Pair<MapleBuffStat, Integer>> statt = Collections.singletonList(new Pair(MapleBuffStat.黄色灵气, (int) this.level));
            buff = BuffPacket.giveBuff(this.sourceid, localDuration, statt, this, applyto);
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), statt, this);
        }
        else if (isMorph())
        {
            if (is冰骑士())
            {
                List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.冰骑士, 2));
                buff = BuffPacket.giveBuff(0, localDuration, stat, this, applyto);
            }
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.变身术, getMorph(applyto)));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (isInflation())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.GIANT_POTION, (int) this.inflation));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.charColor > 0)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.FAMILIAR_SHADOW, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is骑兽技能())
        {
            localDuration = 2100000000;
            localstatups = new ArrayList(this.statups);
            int mountid = parseMountInfo(applyto, this.sourceid);
            int mountid2 = parseMountInfo_Pure(applyto, this.sourceid);
            if ((mountid != 0) && (mountid2 != 0))
            {
                localstatups.add(new Pair(MapleBuffStat.骑兽技能, mountid2));
                List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.骑兽技能, 0));
                applyto.cancelEffectFromBuffStat(MapleBuffStat.战神抗压);
                applyto.cancelEffectFromBuffStat(MapleBuffStat.伤害反击);
                applyto.cancelEffectFromBuffStat(MapleBuffStat.魔法反击);
                foreignbuff = BuffPacket.showMonsterRiding(applyto.getId(), stat, mountid, this.sourceid);
            }
            else
            {
                if (applyto.isAdmin())
                {
                    applyto.dropSpouseMessage(10, "骑宠BUFF " + this.sourceid + " 错误，未找到这个骑宠的外形ID。");
                }
                return;
            }
        }
        else if (isSoaring())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.飞行骑乘, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.berserk > 0)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.天使状态, 0));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if ((is狂暴战魂()) || (this.berserk2 > 0))
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.狂暴战魂, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is金刚霸体())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.金刚霸体, 1));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is天使物品())
        {
            if (applyto.getStat().equippedSummon <= 0)
            {
                localstatups = Collections.singletonList(new Pair(MapleBuffStat.天使状态, 1));
            }
        }
        else if (this.sourceid == 11001021)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.灵魂融入, (int) this.level));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (this.sourceid == 11001022)
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.元素灵魂, (int) this.level));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is月光转换())
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.月光转换);
            int skillLevel = applyto.getTotalSkillLevel(11120009);
            if (skillLevel > 0)
            {
                if (this.sourceid == 11101022)
                {
                    MapleStatEffect effect = SkillFactory.getSkill(11120009).getEffect(skillLevel);
                    if (effect != null)
                    {
                        localstatups = new ArrayList<>();
                        localstatups.add(new Pair(MapleBuffStat.月光转换, 1));
                        localstatups.add(new Pair(MapleBuffStat.暴击概率, effect.getIndieCr()));
                        localstatups.add(new Pair(MapleBuffStat.伤害减少, effect.getY()));
                    }
                }
                else if (this.sourceid == 11111022)
                {
                    MapleStatEffect effect = SkillFactory.getSkill(11120010).getEffect(skillLevel);
                    if (effect != null)
                    {
                        localstatups = new ArrayList<>();
                        localstatups.add(new Pair(MapleBuffStat.月光转换, 2));
                        localstatups.add(new Pair(MapleBuffStat.攻击速度提升, effect.getIndieBooster()));
                        localstatups.add(new Pair(MapleBuffStat.伤害增加, effect.getIndieDamR()));
                    }
                }
            }

            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.月光转换, this.sourceid == 11101022 ? 1 : 2));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (is信天翁新())
        {
            List<Pair<MapleBuffStat, Integer>> stat = Collections.singletonList(new Pair(MapleBuffStat.元素灵魂, (int) this.level));
            foreignbuff = BuffPacket.giveForeignBuff(applyto.getId(), stat, this);
        }
        else if (isSummonSkill())
        {
            localstatups = new ArrayList(this.statups);
            localstatups.remove(new Pair(MapleBuffStat.召唤兽, 1));
            normal = localstatups.size() > 0;
        }
        else if (this.sourceid == 110001500)
        {
            applyto.cancelEffectFromBuffStat(MapleBuffStat.模式变更);
        }


        if ((!is骑兽技能()) && (!is机械传送门()))
        {
            applyto.cancelEffect(this, true, -1L, localstatups);
        }

        long startTime = System.currentTimeMillis();
        if (localDuration > 0)
        {
            CancelEffectAction cancelAction = new CancelEffectAction(applyto, this, startTime, localstatups);
            java.util.concurrent.ScheduledFuture<?> schedule = Timer.BuffTimer.getInstance().schedule(cancelAction, maskedDuration > 0 ? maskedDuration : localDuration);
            applyto.registerEffect(this, startTime, schedule, localstatups, false, localDuration, applyfrom.getId());
        }

        int cooldown = getCooldown(applyto);
        if ((cooldown > 0) && (!applyto.skillisCooling(this.sourceid)))
        {
            applyto.getClient().getSession().write(MaplePacketCreator.skillCooldown(this.sourceid, cooldown));
            applyto.addCooldown(this.sourceid, startTime, cooldown * 1000);
        }

        if (isMechPassive())
        {
            applyto.getClient().getSession().write(MaplePacketCreator.showOwnBuffEffect(this.sourceid - 1000, 1, applyto.getLevel(), this.level, (byte) 1));
        }

        if ((showEffect) && (!applyto.isHidden()))
        {
            applyto.getMap().broadcastMessage(applyto, MaplePacketCreator.showBuffeffect(applyto.getId(), this.sourceid, 1, applyto.getLevel(), this.level), false);
        }

        if (buff != null)
        {
            applyto.getClient().getSession().write(buff);
        }
        else if ((normal) && (localstatups.size() > 0))
        {
            applyto.getClient().getSession().write(BuffPacket.giveBuff(this.skill ? this.sourceid : -this.sourceid, localDuration, maskedStatups == null ? localstatups : maskedStatups, this,
                    applyto));
        }
        if ((foreignbuff != null) && (!applyto.isHidden()))
        {
            applyto.getMap().broadcastMessage(foreignbuff);
        }
    }

    private int getHpMpChange(MapleCharacter applyfrom, boolean hpchange)
    {
        int change = 0;
        if ((this.hpR != 0.0D) || (this.mpR != 0.0D))
        {
            double healHpRate = this.hpR;
            if (is元气恢复())
            {
                healHpRate -= applyfrom.getBuffedIntValue(MapleBuffStat.元气恢复) / 100.0D;
                if (healHpRate <= 0.0D)
                {
                    return 0;
                }
            }
            if (applyfrom.isShowPacket())
            {
                applyfrom.dropMessage(-5, "HpMpChange => 默认: " + this.hpR + " - " + healHpRate);
            }
            int maxChange = (hpchange ? healHpRate : this.mpR) < 1.0D ? Math.min(49999, (int) Math.floor(99999.0D * (hpchange ? healHpRate : this.mpR))) : 99999;
            int current = hpchange ? applyfrom.getStat().getCurrentMaxHp() : applyfrom.getStat().getCurrentMaxMp(applyfrom.getJob());
            change = (int) (current * (hpchange ? healHpRate : this.mpR)) > maxChange ? maxChange : (int) (current * (hpchange ? healHpRate : this.mpR));
        }
        return change;
    }

    private int calcHPChange(MapleCharacter applyfrom, boolean primary)
    {
        int hpchange = 0;
        if (this.info.get(MapleStatInfo.hp) != 0)
        {
            if (!this.skill)
            {
                if (primary)
                {
                    hpchange += alchemistModifyVal(applyfrom, this.info.get(MapleStatInfo.hp), true);
                }
                else
                {
                    hpchange += this.info.get(MapleStatInfo.hp);
                }
                if (applyfrom.hasDisease(MapleDisease.ZOMBIFY))
                {
                    hpchange /= 2;
                }
            }
            else
            {
                hpchange += makeHealHP(this.info.get(MapleStatInfo.hp) / 100.0D, applyfrom.getStat().getTotalMagic(), 3.0D, 5.0D);
                if (applyfrom.hasDisease(MapleDisease.ZOMBIFY))
                {
                    hpchange = -hpchange;
                }
            }
        }
        if (this.hpR != 0.0D)
        {
            hpchange += getHpMpChange(applyfrom, true) / (applyfrom.hasDisease(MapleDisease.ZOMBIFY) ? 2 : 1);
        }

        if ((primary) && (this.info.get(MapleStatInfo.hpCon) != 0))
        {
            hpchange -= this.info.get(MapleStatInfo.hpCon);
        }


        if ((is斗气重生()) && (applyfrom.getTotalSkillLevel(21120043) > 0))
        {
            hpchange = 0;
        }
        return hpchange;
    }

    private int calcMPChange(MapleCharacter applyfrom, boolean primary)
    {
        int mpchange = 0;
        if (this.info.get(MapleStatInfo.mp) != 0)
        {
            if (primary)
            {
                mpchange += alchemistModifyVal(applyfrom, this.info.get(MapleStatInfo.mp), true);
            }
            else
            {
                mpchange += this.info.get(MapleStatInfo.mp);
            }
        }
        if (this.mpR != 0.0D)
        {
            mpchange += getHpMpChange(applyfrom, false);
        }
        if (GameConstants.is恶魔猎手(applyfrom.getJob()))
        {
            mpchange = 0;
        }
        if (primary)
        {
            if ((this.info.get(MapleStatInfo.mpCon) != 0) && (!GameConstants.is恶魔猎手(applyfrom.getJob())))
            {
                boolean free = false;
                if ((applyfrom.getJob() == 411) || (applyfrom.getJob() == 412))
                {
                    Skill expert = SkillFactory.getSkill(4110012);
                    if (applyfrom.getTotalSkillLevel(expert) > 0)
                    {
                        MapleStatEffect eff = expert.getEffect(applyfrom.getTotalSkillLevel(expert));
                        if (eff.makeChanceResult())
                        {
                            free = true;
                        }
                    }
                }
                if (applyfrom.getBuffedValue(MapleBuffStat.终极无限) != null)
                {
                    mpchange = 0;
                }
                else if (!free)
                {
                    mpchange =
                            (int) (mpchange - (this.info.get(MapleStatInfo.mpCon) - this.info.get(MapleStatInfo.mpCon) * applyfrom.getStat().mpconReduce / 100) * (applyfrom.getStat().mpconPercent / 100.0D));
                }
            }
            else if ((this.info.get(MapleStatInfo.forceCon) != 0) && (GameConstants.is恶魔猎手(applyfrom.getJob())))
            {
                if (applyfrom.getBuffedValue(MapleBuffStat.无限精气) != null)
                {
                    mpchange = 0;
                }
                else
                {
                    mpchange -= this.info.get(MapleStatInfo.forceCon);
                }
            }
        }
        if ((is斗气重生()) && (applyfrom.getTotalSkillLevel(21120043) > 0))
        {
            mpchange = 0;
        }
        return mpchange;
    }

    public int alchemistModifyVal(MapleCharacter chr, int val, boolean withX)
    {
        if (!this.skill)
        {
            return val * (withX ? chr.getStat().RecoveryUP : chr.getStat().BuffUP) / 100;
        }
        return val * (withX ? chr.getStat().RecoveryUP : chr.getStat().BuffUP_Skill + (getSummonMovementType() == null ? 0 : chr.getStat().BuffUP_Summon)) / 100;
    }

    private int calcPowerChange(MapleCharacter applyfrom)
    {
        int powerchange = 0;
        if (!GameConstants.is尖兵(applyfrom.getJob()))
        {
            return powerchange;
        }
        if (this.info.get(MapleStatInfo.powerCon) != 0)
        {
            powerchange -= this.info.get(MapleStatInfo.powerCon);
        }
        return powerchange;
    }

    private int calcAranComboChange(MapleCharacter applyfrom)
    {
        int combochange = 0;
        if (this.info.get(MapleStatInfo.aranComboCon) != 0)
        {
            combochange -= this.info.get(MapleStatInfo.aranComboCon);
        }
        return combochange;
    }

    public boolean isGmBuff()
    {
        switch (this.sourceid)
        {
            case 9001000:
            case 9001001:
            case 9001002:
            case 9001003:
            case 9001005:
            case 9001008:
            case 9101000:
            case 9101001:
            case 9101002:
            case 9101003:
            case 9101005:
            case 9101008:
            case 10001075:
                return true;
        }
        return (GameConstants.is新手职业(this.sourceid / 10000)) && (this.sourceid % 10000 == 1005);
    }

    public boolean isInflation()
    {
        return this.inflation > 0;
    }

    public int getInflation()
    {
        return this.inflation;
    }

    private boolean isMonsterBuff()
    {
        switch (this.sourceid)
        {


            case 1121016:
            case 1211013:
            case 1221014:
            case 1321014:
            case 2121006:
            case 2201008:
            case 2211010:
            case 2221011:
            case 4111003:
            case 4321002:
            case 5011002:
            case 12101001:
            case 12111002:
            case 14111001:
            case 21120006:
            case 22121000:
            case 22151001:
            case 22161002:
            case 32120000:
            case 32120001:
            case 35111005:
            case 51111005:
            case 90001002:
            case 90001003:
            case 90001004:
            case 90001005:
            case 90001006:
                return this.skill;
        }

        return false;
    }

    private boolean isPartyBuff()
    {
        if ((this.lt == null) || (this.rb == null) || (!this.partyBuff))
        {
            return is灵魂之石();
        }
        switch (this.sourceid)
        {
            case 1201011:
            case 1201012:
            case 1211008:
            case 1221004:
            case 4341002:
            case 12101005:
            case 35121005:
            case 61121009:
            case 110001500:
            case 110001501:
            case 110001502:
            case 110001503:
            case 110001504:
                return false;
        }
        return !GameConstants.isNoDelaySkill(this.sourceid);
    }

    public void setPartyBuff(boolean pb)
    {
        this.partyBuff = pb;
    }

    public boolean is神圣之火()
    {
        return (this.skill) && (this.sourceid == 1301007);
    }

    public boolean is神秘瞄准术()
    {
        return (this.skill) && ((this.sourceid == 2320011) || (this.sourceid == 2220010) || (this.sourceid == 2120010));
    }

    public boolean is群体治愈()
    {
        return (this.skill) && ((this.sourceid == 2301002) || (this.sourceid == 9101000) || (this.sourceid == 9001000));
    }

    public boolean is复活术()
    {
        return (this.skill) && ((this.sourceid == 9001005) || (this.sourceid == 9101005) || (this.sourceid == 2321006));
    }

    public boolean is伺机待发()
    {
        return (this.skill) && (this.sourceid == 5121010);
    }

    public boolean is黑暗灵气()
    {
        return (this.skill) && (this.sourceid == 32001003);
    }

    public boolean is黄色灵气()
    {
        return (this.skill) && (this.sourceid == 32101003);
    }

    public boolean is蓝色灵气()
    {
        return (this.skill) && (this.sourceid == 32111012);
    }

    public boolean is尖兵支援()
    {
        return (this.skill) && (this.sourceid == 30020232);
    }

    public boolean is月光转换()
    {
        return (this.skill) && ((this.sourceid == 11101022) || (this.sourceid == 11111022));
    }

    public boolean is信天翁新()
    {
        return (this.skill) && ((this.sourceid == 13111023) || (this.sourceid == 13120008));
    }

    public boolean is圣洁之力()
    {
        return (this.skill) && (this.sourceid == 100001263);
    }

    public boolean is神圣迅捷()
    {
        return (this.skill) && (this.sourceid == 100001264);
    }

    public int getHp()
    {
        return this.info.get(MapleStatInfo.hp);
    }

    public int getMp()
    {
        return this.info.get(MapleStatInfo.mp);
    }

    public int getDOTStack()
    {
        return this.info.get(MapleStatInfo.dotSuperpos);
    }

    public double getHpR()
    {
        return this.hpR;
    }

    public double getMpR()
    {
        return this.mpR;
    }

    public int getMastery()
    {
        return this.info.get(MapleStatInfo.mastery);
    }

    public int getWatk()
    {
        return this.info.get(MapleStatInfo.pad);
    }

    public int getMatk()
    {
        return this.info.get(MapleStatInfo.mad);
    }

    public int getWdef()
    {
        return this.info.get(MapleStatInfo.pdd);
    }

    public int getMdef()
    {
        return this.info.get(MapleStatInfo.mdd);
    }

    public int getAcc()
    {
        return this.info.get(MapleStatInfo.acc);
    }

    public int getAvoid()
    {
        return this.info.get(MapleStatInfo.eva);
    }

    public int getSpeed()
    {
        return this.info.get(MapleStatInfo.speed);
    }

    public int getJump()
    {
        return this.info.get(MapleStatInfo.jump);
    }

    public int getSpeedMax()
    {
        return this.info.get(MapleStatInfo.speedMax);
    }

    public int getPassiveSpeed()
    {
        return this.info.get(MapleStatInfo.psdSpeed);
    }

    public int getPassiveJump()
    {
        return this.info.get(MapleStatInfo.psdJump);
    }

    public int getDuration()
    {
        return this.info.get(MapleStatInfo.time);
    }

    public void setDuration(int d)
    {
        this.info.put(MapleStatInfo.time, d);
    }

    public int getDuration(MapleCharacter applyfrom)
    {
        int time = this.skill ? applyfrom.getStat().getDuration(this.sourceid) : 0;
        return this.info.get(MapleStatInfo.time) + time;
    }

    public int getSubTime()
    {
        return this.info.get(MapleStatInfo.subTime);
    }

    public boolean isOverTime()
    {
        return this.overTime;
    }

    public boolean isNotRemoved()
    {
        return this.notRemoved;
    }

    public boolean isRepeatEffect()
    {
        return this.repeatEffect;
    }

    public List<Pair<MapleBuffStat, Integer>> getStatups()
    {
        return this.statups;
    }

    public boolean sameSource(MapleStatEffect effect)
    {
        return (effect != null) && (this.sourceid == effect.sourceid) && (this.skill == effect.skill);
    }

    public int getT()
    {
        return this.info.get(MapleStatInfo.t);
    }

    public int getU()
    {
        return this.info.get(MapleStatInfo.u);
    }

    public int getV()
    {
        return this.info.get(MapleStatInfo.v);
    }

    public int getW()
    {
        return this.info.get(MapleStatInfo.w);
    }

    public int getY()
    {
        return this.info.get(MapleStatInfo.y);
    }

    public int getZ()
    {
        return this.info.get(MapleStatInfo.z);
    }

    public int getDamage()
    {
        return this.info.get(MapleStatInfo.damage);
    }

    public int getMagicDamage()
    {
        return this.info.get(MapleStatInfo.madR);
    }

    public int getPVPDamage()
    {
        return this.info.get(MapleStatInfo.PVPdamage);
    }

    public int getAttackCount(MapleCharacter applyfrom)
    {
        int addcount = (applyfrom.getSkillLevel(3220015) > 0) && (getAttackCount() >= 2) ? 1 : 0;
        return this.info.get(MapleStatInfo.attackCount) + applyfrom.getStat().getAttackCount(this.sourceid) + addcount;
    }

    public int getAttackCount()
    {
        return this.info.get(MapleStatInfo.attackCount);
    }

    public int getBulletCount(MapleCharacter applyfrom)
    {
        int addcount = (applyfrom.getSkillLevel(3220015) > 0) && (getBulletCount() >= 2) ? 1 : 0;
        return this.info.get(MapleStatInfo.bulletCount) + applyfrom.getStat().getAttackCount(this.sourceid) + addcount;
    }

    public int getBulletCount()
    {
        return this.info.get(MapleStatInfo.bulletCount);
    }

    public int getBulletConsume()
    {
        return this.info.get(MapleStatInfo.bulletConsume);
    }

    public int getMobCount()
    {
        return this.info.get(MapleStatInfo.mobCount);
    }

    public int getMobCount(MapleCharacter applyfrom)
    {
        return this.info.get(MapleStatInfo.mobCount) + applyfrom.getStat().getMobCount(this.sourceid);
    }

    public int getMoneyCon()
    {
        return this.moneyCon;
    }

    public int getCooltimeReduceR()
    {
        return this.info.get(MapleStatInfo.coolTimeR);
    }

    public int getMesoAcquisition()
    {
        return this.info.get(MapleStatInfo.mesoR);
    }

    public int getCooldown(MapleCharacter applyfrom)
    {
        if ((is神枪降临()) && (applyfrom.hasBuffSkill(1321015)))
        {
            return 0;
        }
        if (this.info.get(MapleStatInfo.cooltime) >= 5)
        {
            int cooldownX = (int) (this.info.get(MapleStatInfo.cooltime) * (applyfrom.getStat().getCoolTimeR() / 100.0D));
            int coolTimeR = (int) (this.info.get(MapleStatInfo.cooltime) * (applyfrom.getStat().getReduceCooltimeRate(this.sourceid) / 100.0D));
            if (applyfrom.isShowPacket())
            {
                applyfrom.dropMessage(-5,
                        "技能冷却时间 => 默认: " + this.info.get(MapleStatInfo.cooltime) + " [减少百分比: " + applyfrom.getStat().getCoolTimeR() + "% - " + cooldownX + "] [减少时间: " + applyfrom.getStat().getReduceCooltime() + "] [超级技能减少百分比: " + applyfrom.getStat().getReduceCooltimeRate(this.sourceid) + "% 减少时间: " + coolTimeR + "]");
            }
            return Math.max(0, this.info.get(MapleStatInfo.cooltime) - applyfrom.getStat().getReduceCooltime() - (cooldownX > 5 ? 5 : cooldownX) - coolTimeR);
        }
        return this.info.get(MapleStatInfo.cooltime);
    }

    public Map<MonsterStatus, Integer> getMonsterStati()
    {
        return this.monsterStatus;
    }

    public int getBerserk()
    {
        return this.berserk;
    }

    public boolean is神枪降临()
    {
        return (this.skill) && (this.sourceid == 1321013);
    }

    public boolean is隐藏术()
    {
        return (this.skill) && ((this.sourceid == 9001004) || (this.sourceid == 9101004));
    }

    public boolean is隐身术()
    {
        return (this.skill) && ((this.sourceid == 9001004) || (this.sourceid == 9101004) || (this.sourceid == 4001003) || (this.sourceid == 14001003) || (this.sourceid == 4330001));
    }

    public boolean is龙之力()
    {
        return (this.skill) && (this.sourceid == 1311008);
    }

    public boolean is龙之献祭()
    {
        return (this.skill) && (this.sourceid == 1321015);
    }

    public boolean is元气恢复()
    {
        return (this.skill) && (this.sourceid == 1211010);
    }

    public boolean is团队治疗()
    {
        return (this.skill) && ((this.sourceid == 1001) || (this.sourceid == 10001001) || (this.sourceid == 20001001) || (this.sourceid == 20011001) || (this.sourceid == 35121005));
    }

    public boolean is重生契约()
    {
        return (this.skill) && (this.sourceid == 1320016);
    }

    public boolean is灵魂助力()
    {
        return (this.skill) && (this.sourceid == 1301013);
    }

    public boolean is刀飞炼狱()
    {
        return (this.skill) && (this.sourceid == 4211002);
    }

    public boolean is极限射箭()
    {
        return (this.skill) && ((this.sourceid == 3111011) || (this.sourceid == 3211012));
    }

    public boolean is终极无限()
    {
        return (this.skill) && ((this.sourceid == 2121004) || (this.sourceid == 2221004) || (this.sourceid == 2321004));
    }

    public boolean is骑兽技能_()
    {
        return (this.skill) && ((GameConstants.is骑兽技能(this.sourceid)) || (this.sourceid == 80001000));
    }

    public boolean is骑兽技能()
    {
        return (this.skill) && ((is骑兽技能_()) || (GameConstants.getMountItem(this.sourceid, null) != 0)) && (!is机械骑兽());
    }

    public boolean is机械骑兽()
    {
        return (this.skill) && ((this.sourceid == 35001002) || (this.sourceid == 35120000));
    }

    public boolean is时空门()
    {
        return (this.skill) && ((this.sourceid == 2311002) || (this.sourceid % 10000 == 8001));
    }

    public boolean is金钱护盾()
    {
        return (this.skill) && (this.sourceid == 4201011);
    }

    public boolean is金钱炸弹()
    {
        return (this.skill) && (this.sourceid == 4211006);
    }

    public boolean is机械传送门()
    {
        return (this.skill) && (this.sourceid == 35101005);
    }

    public boolean is斗气重生()
    {
        return (this.skill) && (this.sourceid == 21111009);
    }

    public boolean is飞龙传动()
    {
        return (this.skill) && (this.sourceid == 22141004);
    }

    public boolean is火焰咆哮()
    {
        return (this.skill) && (this.sourceid == 23111004);
    }

    public boolean is影子闪避()
    {
        return (this.skill) && (this.sourceid == 4330009);
    }

    public boolean is卡牌审判()
    {
        return (this.skill) && ((this.sourceid == 20031209) || (this.sourceid == 20031210));
    }

    public boolean is黑暗祝福()
    {
        return (this.skill) && (this.sourceid == 27100003);
    }

    public boolean is黑暗高潮()
    {
        return (this.skill) && (this.sourceid == 27121005);
    }

    public boolean is光暗转换()
    {
        return (this.skill) && ((this.sourceid == 20040216) || (this.sourceid == 20040217) || (this.sourceid == 20040219));
    }

    public boolean isCharge()
    {
        switch (this.sourceid)
        {
            case 1211008:
            case 12101005:
            case 21101006:
                return this.skill;
        }
        return false;
    }

    private boolean isMist()
    {
        switch (this.sourceid)
        {
            case 1076:
            case 2111003:
            case 2311011:
            case 4121015:
            case 4221006:
            case 12111005:
            case 14111006:
            case 22161003:
            case 32121006:
                return true;
        }
        return false;
    }

    private boolean is暗器伤人()
    {
        return (this.skill) && ((this.sourceid == 4111009) || (this.sourceid == 14111007));
    }

    private boolean is无限子弹()
    {
        return (this.skill) && (this.sourceid == 5201008);
    }

    private boolean is净化()
    {
        return (this.skill) && ((this.sourceid == 2311001) || (this.sourceid == 9001000) || (this.sourceid == 9101000));
    }

    private boolean is勇士的意志()
    {
        switch (this.sourceid)
        {
            case 1121011:
            case 1221012:
            case 1321010:
            case 2121008:
            case 2221008:
            case 2321009:
            case 3121009:
            case 3221008:
            case 4121009:
            case 4221008:
            case 4341008:
            case 5121008:
            case 5221010:
            case 5321006:
            case 21121008:
            case 22171004:
            case 23121008:
            case 24121009:
            case 27121010:
            case 32121008:
            case 33121008:
            case 35121008:
            case 36121009:
            case 61121015:
            case 65121010:
            case 110001512:
                return this.skill;
        }
        return false;
    }

    public boolean is矛连击强化()
    {
        return (this.skill) && (this.sourceid == 21000000);
    }

    public boolean is侠盗本能()
    {
        return (this.skill) && (this.sourceid == 4221013);
    }

    public boolean is斗气集中()
    {
        switch (this.sourceid)
        {
            case 1101013:
                return this.skill;
        }
        return false;
    }

    public boolean is负荷释放()
    {
        return (this.skill) && (this.sourceid == 31011001);
    }

    public boolean is恶魔恢复()
    {
        return (this.skill) && (this.sourceid == 31211004);
    }

    public boolean is额外供给()
    {
        return (this.skill) && (this.sourceid == 36111008);
    }

    public boolean is金刚霸体()
    {
        return (this.skill) && (GameConstants.is新手职业(this.sourceid / 10000)) && (this.sourceid % 10000 == 1010);
    }

    public boolean is祝福护甲()
    {
        switch (this.sourceid)
        {
            case 1210016:
                return this.skill;
        }
        return false;
    }

    public boolean is狂暴战魂()
    {
        return (this.skill) && (GameConstants.is新手职业(this.sourceid / 10000)) && (this.sourceid % 10000 == 1011);
    }

    public int getMorph(MapleCharacter chr)
    {
        int morph = getMorph();
        switch (morph)
        {
            case 1000:
            case 1001:
            case 1003:
                return morph + (chr.getGender() == 1 ? 100 : 0);
        }
        return morph;
    }

    public byte getLevel()
    {
        return this.level;
    }

    public boolean isSummonSkill()
    {
        Skill summon = SkillFactory.getSkill(this.sourceid);
        if ((!this.skill) || (summon == null))
        {
            return false;
        }
        return summon.isSummonSkill();
    }

    public boolean is集合船员()
    {
        switch (this.sourceid)
        {
            case 5210015:
            case 5210016:
            case 5210017:
            case 5210018:
                return this.skill;
        }
        return false;
    }

    public boolean is船员统帅()
    {
        return (this.skill) && (this.sourceid == 5220019);
    }

    public boolean isSkill()
    {
        return this.skill;
    }

    public int getSourceId()
    {
        return this.sourceid;
    }

    public void setSourceId(int newid)
    {
        this.sourceid = newid;
    }

    public boolean is冰骑士()
    {
        return (this.skill) && (GameConstants.is新手职业(this.sourceid / 10000)) && (this.sourceid % 10000 == 1105);
    }

    public boolean isSoaring()
    {
        return (isSoaring_Normal()) || (isSoaring_Mount());
    }

    public boolean isSoaring_Normal()
    {
        return (this.skill) && (GameConstants.is新手职业(this.sourceid / 10000)) && (this.sourceid % 10000 == 1026);
    }

    public boolean isSoaring_Mount()
    {
        return (this.skill) && (((GameConstants.is新手职业(this.sourceid / 10000)) && (this.sourceid % 10000 == 1142)) || (this.sourceid == 80001089));
    }

    public boolean is迷雾爆发()
    {
        return (this.skill) && (this.sourceid == 2121003);
    }

    public boolean is影分身()
    {
        switch (this.sourceid)
        {
            case 4111002:
            case 4211008:
            case 4331002:
            case 14111000:
            case 36111006:
                return this.skill;
        }
        return false;
    }

    public int getShadowDamage()
    {
        switch (this.sourceid)
        {
            case 4111002:
            case 4211008:
            case 4331002:
            case 14111000:
                return this.info.get(MapleStatInfo.x);
            case 36111006:
                return this.info.get(MapleStatInfo.y);
        }
        return this.info.get(MapleStatInfo.x);
    }

    public boolean isMechPassive()
    {
        switch (this.sourceid)
        {
            case 35121013:
                return true;
        }
        return false;
    }

    public boolean is天使物品()
    {
        return (!this.skill) && ((this.sourceid == 2022746) || (this.sourceid == 2022747) || (this.sourceid == 2022823));
    }

    private boolean is极速领域()
    {
        return (this.skill) && ((this.sourceid == 5121009) || (this.sourceid == 15121005) || (this.sourceid % 10000 == 8006));
    }

    private boolean is幸运骰子()
    {
        switch (this.sourceid)
        {
            case 5111007:
            case 5211007:
            case 5311005:
            case 5711011:
            case 35111013:
                return true;
        }
        return false;
    }

    private boolean is双幸运骰子()
    {
        switch (this.sourceid)
        {
            case 5120012:
            case 5220014:
            case 5320007:
            case 5720005:
            case 35120014:
                return true;
        }
        return false;
    }

    private boolean is疾驰()
    {
        return (this.skill) && (this.sourceid == 5001005);
    }

    private boolean is属性攻击()
    {
        switch (this.sourceid)
        {
            case 1201011:
            case 1201012:
            case 1221004:
            case 21101006:
                return true;
        }
        return false;
    }

    private boolean is导航辅助()
    {
        switch (this.sourceid)
        {
            case 5221015:
            case 5721003:
            case 22151002:
                return true;
        }
        return false;
    }

    public boolean makeChanceResult(MapleCharacter applyfrom)
    {
        int prop = this.info.get(MapleStatInfo.prop);
        return (prop >= 100) || (Randomizer.nextInt(100) < prop);
    }

    public int getProb()
    {
        return this.info.get(MapleStatInfo.prop);
    }


    public short getIgnoreMob()
    {
        return this.ignoreMob;
    }


    public int getEnhancedHP()
    {
        return this.info.get(MapleStatInfo.emhp);
    }


    public int getEnhancedMP()
    {
        return this.info.get(MapleStatInfo.emmp);
    }


    public int getEnhancedWatk()
    {
        return this.info.get(MapleStatInfo.epad);
    }


    public int getEnhancedMatk()
    {
        return this.info.get(MapleStatInfo.emad);
    }


    public int getEnhancedWdef()
    {
        return this.info.get(MapleStatInfo.pdd);
    }


    public int getEnhancedMdef()
    {
        return this.info.get(MapleStatInfo.emdd);
    }

    public int getDOT()
    {
        return this.info.get(MapleStatInfo.dot);
    }

    public int getDOTTime()
    {
        return this.info.get(MapleStatInfo.dotTime);
    }


    public int getCritical()
    {
        return this.info.get(MapleStatInfo.cr);
    }


    public int getCriticalMax()
    {
        return this.info.get(MapleStatInfo.criticaldamageMax);
    }


    public int getCriticalMin()
    {
        return this.info.get(MapleStatInfo.criticaldamageMin);
    }


    public int getArRate()
    {
        return this.info.get(MapleStatInfo.ar);
    }

    public int getASRRate()
    {
        return this.info.get(MapleStatInfo.asrR);
    }

    public int getTERRate()
    {
        return this.info.get(MapleStatInfo.terR);
    }


    public int getDAMRate()
    {
        return this.info.get(MapleStatInfo.damR);
    }


    public int getMdRate()
    {
        return this.info.get(MapleStatInfo.mdR);
    }


    public int getPercentDamageRate()
    {
        return this.info.get(MapleStatInfo.pdR);
    }


    public short getMesoRate()
    {
        return this.mesoR;
    }

    public int getEXP()
    {
        return this.exp;
    }


    public int getWdefToMdef()
    {
        return this.info.get(MapleStatInfo.pdd2mdd);
    }


    public int getMdefToWdef()
    {
        return this.info.get(MapleStatInfo.mdd2pdd);
    }


    public int getAvoidToHp()
    {
        return this.info.get(MapleStatInfo.eva2hp);
    }


    public int getAccToMp()
    {
        return this.info.get(MapleStatInfo.acc2mp);
    }


    public int getStrToDex()
    {
        return this.info.get(MapleStatInfo.str2dex);
    }


    public int getDexToStr()
    {
        return this.info.get(MapleStatInfo.dex2str);
    }


    public int getIntToLuk()
    {
        return this.info.get(MapleStatInfo.int2luk);
    }


    public int getLukToDex()
    {
        return this.info.get(MapleStatInfo.luk2dex);
    }


    public int getHpToDamageX()
    {
        return this.info.get(MapleStatInfo.mhp2damX);
    }


    public int getMpToDamageX()
    {
        return this.info.get(MapleStatInfo.mmp2damX);
    }


    public int getLevelToMaxHp()
    {
        return this.info.get(MapleStatInfo.lv2mhp);
    }


    public int getLevelToMaxMp()
    {
        return this.info.get(MapleStatInfo.lv2mmp);
    }


    public int getLevelToDamageX()
    {
        return this.info.get(MapleStatInfo.lv2damX);
    }


    public int getLevelToWatk()
    {
        return this.info.get(MapleStatInfo.lv2pad);
    }


    public int getLevelToMatk()
    {
        return this.info.get(MapleStatInfo.lv2mad);
    }


    public int getLevelToWatkX()
    {
        return this.info.get(MapleStatInfo.lv2pdX);
    }


    public int getLevelToMatkX()
    {
        return this.info.get(MapleStatInfo.lv2mdX);
    }

    public int getEXPLossRate()
    {
        return this.info.get(MapleStatInfo.expLossReduceR);
    }

    public int getBuffTimeRate()
    {
        return this.info.get(MapleStatInfo.bufftimeR);
    }

    public int getSuddenDeathR()
    {
        return this.info.get(MapleStatInfo.suddenDeathR);
    }

    public int getSummonTimeInc()
    {
        return this.info.get(MapleStatInfo.summonTimeR);
    }

    public int getMPConsumeEff()
    {
        return this.info.get(MapleStatInfo.mpConEff);
    }


    public int getAttackX()
    {
        return this.info.get(MapleStatInfo.padX);
    }


    public int getMagicX()
    {
        return this.info.get(MapleStatInfo.madX);
    }


    public int getPercentHP()
    {
        return this.info.get(MapleStatInfo.mhpR);
    }


    public int getPercentMP()
    {
        return this.info.get(MapleStatInfo.mmpR);
    }


    public int getIgnoreMobDamR()
    {
        return this.info.get(MapleStatInfo.ignoreMobDamR);
    }

    public int getConsume()
    {
        return this.consumeOnPickup;
    }

    public int getSelfDestruction()
    {
        return this.info.get(MapleStatInfo.selfDestruction);
    }

    public int getCharColor()
    {
        return this.charColor;
    }

    public List<Integer> getPetsCanConsume()
    {
        return this.petsCanConsume;
    }

    public boolean isReturnScroll()
    {
        return (this.skill) && ((this.sourceid == 20031203) || (this.sourceid == 80001040) || (this.sourceid == 20021110));
    }


    public boolean isMechChange()
    {
        switch (this.sourceid)
        {
            case 35001001:
            case 35101009:
            case 35111004:
            case 35121005:
            case 35121013:
                return this.skill;
        }
        return false;
    }

    public int getRange()
    {
        return this.info.get(MapleStatInfo.range);
    }


    public int getER()
    {
        return this.info.get(MapleStatInfo.er);
    }

    public int getPrice()
    {
        return this.info.get(MapleStatInfo.price);
    }

    public int getExtendPrice()
    {
        return this.info.get(MapleStatInfo.extendPrice);
    }

    public int getPeriod()
    {
        return this.info.get(MapleStatInfo.period);
    }

    public int getReqGuildLevel()
    {
        return this.info.get(MapleStatInfo.reqGuildLevel);
    }

    public byte getEXPRate()
    {
        return this.expR;
    }

    public short getLifeID()
    {
        return this.lifeId;
    }

    public short getUseLevel()
    {
        return this.useLevel;
    }


    public byte getSlotCount()
    {
        return this.slotCount;
    }

    public byte getSlotPerLine()
    {
        return this.slotPerLine;
    }


    public int getStr()
    {
        return this.info.get(MapleStatInfo.str);
    }

    public int getStrX()
    {
        return this.info.get(MapleStatInfo.strX);
    }

    public int getStrFX()
    {
        return this.info.get(MapleStatInfo.strFX);
    }

    public int getStrRate()
    {
        return this.info.get(MapleStatInfo.strR);
    }


    public int getDex()
    {
        return this.info.get(MapleStatInfo.dex);
    }

    public int getDexX()
    {
        return this.info.get(MapleStatInfo.dexX);
    }

    public int getDexFX()
    {
        return this.info.get(MapleStatInfo.dexFX);
    }

    public int getDexRate()
    {
        return this.info.get(MapleStatInfo.dexR);
    }


    public int getInt()
    {
        return this.info.get(MapleStatInfo.int_);
    }

    public int getIntX()
    {
        return this.info.get(MapleStatInfo.intX);
    }

    public int getIntFX()
    {
        return this.info.get(MapleStatInfo.intFX);
    }

    public int getIntRate()
    {
        return this.info.get(MapleStatInfo.intR);
    }


    public int getLuk()
    {
        return this.info.get(MapleStatInfo.luk);
    }

    public int getLukX()
    {
        return this.info.get(MapleStatInfo.lukX);
    }

    public int getLukFX()
    {
        return this.info.get(MapleStatInfo.lukFX);
    }

    public int getLukRate()
    {
        return this.info.get(MapleStatInfo.lukR);
    }


    public int getMaxHpX()
    {
        return this.info.get(MapleStatInfo.mhpX);
    }


    public int getMaxMpX()
    {
        return this.info.get(MapleStatInfo.mmpX);
    }


    public int getAccX()
    {
        return this.info.get(MapleStatInfo.accX);
    }


    public int getPercentAcc()
    {
        return this.info.get(MapleStatInfo.accR);
    }


    public int getAvoidX()
    {
        return this.info.get(MapleStatInfo.evaX);
    }


    public int getPercentAvoid()
    {
        return this.info.get(MapleStatInfo.evaR);
    }


    public int getWdefX()
    {
        return this.info.get(MapleStatInfo.pddX);
    }


    public int getMdefX()
    {
        return this.info.get(MapleStatInfo.mddX);
    }


    public int getIndieMHp()
    {
        return this.info.get(MapleStatInfo.indieMhp);
    }


    public int getIndieMMp()
    {
        return this.info.get(MapleStatInfo.indieMmp);
    }


    public int getIndieMhpR()
    {
        return this.info.get(MapleStatInfo.indieMhpR);
    }


    public int getIndieMmpR()
    {
        return this.info.get(MapleStatInfo.indieMmpR);
    }


    public int getIndieAllStat()
    {
        return this.info.get(MapleStatInfo.indieAllStat);
    }


    public int getIndieCr()
    {
        return this.info.get(MapleStatInfo.indieCr);
    }

    public short getIndiePdd()
    {
        return this.indiePdd;
    }

    public short getIndieMdd()
    {
        return this.indieMdd;
    }


    public int getIndieDamR()
    {
        return this.info.get(MapleStatInfo.indieDamR);
    }


    public int getIndieBooster()
    {
        return this.info.get(MapleStatInfo.indieBooster);
    }

    public byte getType()
    {
        return this.type;
    }


    public int getBossDamage()
    {
        return this.info.get(MapleStatInfo.bdR);
    }


    public int getMobCountDamage()
    {
        return this.info.get(MapleStatInfo.mobCountDamR);
    }

    public int getInterval()
    {
        return this.interval;
    }

    public ArrayList<Pair<Integer, Integer>> getAvailableMaps()
    {
        return this.availableMap;
    }


    public int getWDEFRate()
    {
        return this.info.get(MapleStatInfo.pddR);
    }


    public int getMDEFRate()
    {
        return this.info.get(MapleStatInfo.mddR);
    }


    public int getKillSpree()
    {
        return this.info.get(MapleStatInfo.kp);
    }


    public int getMaxDamageOver()
    {
        return this.info.get(MapleStatInfo.MDamageOver);
    }


    public int getIndieMaxDamageOver()
    {
        return this.info.get(MapleStatInfo.indieMaxDamageOver);
    }


    public int getCostMpRate()
    {
        return this.info.get(MapleStatInfo.costmpR);
    }


    public int getMPConReduce()
    {
        return this.info.get(MapleStatInfo.mpConReduce);
    }


    public int getIndieMaxDF()
    {
        return this.info.get(MapleStatInfo.MDF);
    }


    public int getTargetPlus()
    {
        return this.info.get(MapleStatInfo.targetPlus);
    }


    public int getForceCon()
    {
        return this.info.get(MapleStatInfo.forceCon);
    }


    public static class CancelEffectAction implements Runnable
    {
        private final MapleStatEffect effect;
        private final java.lang.ref.WeakReference<MapleCharacter> target;
        private final long startTime;
        private final List<Pair<MapleBuffStat, Integer>> statup;

        public CancelEffectAction(MapleCharacter target, MapleStatEffect effect, long startTime, List<Pair<MapleBuffStat, Integer>> statup)
        {
            this.effect = effect;
            this.target = new java.lang.ref.WeakReference(target);
            this.startTime = startTime;
            this.statup = statup;
        }

        public void run()
        {
            MapleCharacter realTarget = this.target.get();
            if (realTarget != null)
            {
                realTarget.cancelEffect(this.effect, false, this.startTime, this.statup);
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\MapleStatEffect.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */