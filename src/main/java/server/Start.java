package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import GUI.KinMS;
import client.MapleCharacter;
import client.PlayMSEvent;
import client.SkillFactory;
import client.inventory.MapleInventoryIdentifier;
import commons.services.LoggingService;
import commons.utils.MapleInfos;
import constants.ServerConstants;
import database.DatabaseConnection;
import handling.Auction.AuctionServer;
import handling.cashshop.CashShopServer;
import handling.channel.ChannelServer;
import handling.channel.MapleGuildRanking;
import handling.login.LoginInformationProvider;
import handling.login.LoginServer;
import handling.world.World;
import handling.world.WorldRespawnService;
import handling.world.family.MapleFamily;
import handling.world.guild.MapleGuild;
import handling.world.messenger.MessengerRankingWorker;
import server.cashshop.CashItemFactory;
import server.events.MapleOxQuizFactory;
import server.life.MapleLifeFactory;
import server.life.MapleMonsterInformationProvider;
import server.life.MobSkillFactory;
import server.life.PlayerNPC;
import server.quest.MapleQuest;
import tools.FileoutputUtil;
import tools.MapleLog;
import tools.StringUtil;


public class Start
{
    public static final Start instance = new Start();
    private static final int maxUsers = 0;
    private static final int srvPort = 6350;
    private static ServerSocket srvSocket = null;
    private int rankTime;
    private boolean ivCheck = false;

    public static void main(String[] args) throws InterruptedException
    {
//        Console.setColor(15);
//
//
//        String[] macs = {"00-00-00-00-00-00"};
//
//
//        String localMac = MacAddressTool.getMacAddress(true);
//
//
//        if (localMac != null)
//        {
//            for (int i = 0; i < macs.length; i++)
//            {
//                if (!macs[i].equals(localMac))
//                {
//        instance.run();
        KinMS.main(new String[]{});
//        for (final RecvPacketOpcode recv : RecvPacketOpcode.values())
//        {
//            System.out.println(recv.name());
//        }
//                    break;
//                }
//            }
//        }
//        else
//        {
//            System.exit(0);
//        }
    }

    protected static void checkSingleInstance()
    {
        try
        {
            srvSocket = new ServerSocket(srvPort);
        }
        catch (IOException ex)
        {
            if (ex.getMessage().contains("Address already in use: JVM_Bind"))
            {
                System.out.println("在一台主机上同时只能启动一个进程(Only one instance allowed)。");
            }
            System.exit(0);
        }
    }

    public void run() throws InterruptedException
    {
//        Console.clear();
//        Console.setTitle("HuaiMS_V117");

        LoggingService.init();
        MapleInfos.printAllInfos();
        this.rankTime = Integer.parseInt(ServerProperties.getProperty("world.rankTime", "120"));
        this.ivCheck = Boolean.parseBoolean(ServerProperties.getProperty("world.ivCheck", "false"));
        if ((Boolean.parseBoolean(ServerProperties.getProperty("world.admin"))) || (ServerConstants.Use_Localhost))
        {
            ServerConstants.Use_Fixed_IV = false;
            System.out.println("[!!! 已开启只能管理员登录模式 !!!]");
        }
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("UPDATE `accounts` SET `loggedin` = 0, `check` = 0");
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException ex)
        {
            throw new RuntimeException("[EXCEPTION] Please check if the SQL server is active.");
        }

        printSection("时钟线程");
        Timer.WorldTimer.getInstance().start();
        Timer.EtcTimer.getInstance().start();
        Timer.MapTimer.getInstance().start();
        Timer.CloneTimer.getInstance().start();
        Timer.EventTimer.getInstance().start();
        Timer.BuffTimer.getInstance().start();
        Timer.PingTimer.getInstance().start();
        System.out.println("时钟线程加载完成...");


        printSection("世界服务器");
        World.init();
        System.out.println("世界服务器加载完成...");

        printSection("加载家族");
        MapleGuildRanking.getInstance().load();
        MapleGuild.loadAll();
        System.out.println("家族信息加载完成...");

        printSection("加载学院");
        MapleFamily.loadAll();
        System.out.println("学院信息加载完成...");

        printSection("加载任务");
        long startQuestTime = System.currentTimeMillis();
        MapleLifeFactory.loadQuestCounts();
        MapleQuest.initQuests();
        System.out.println("任务信息加载完成 耗时: " + (System.currentTimeMillis() - startQuestTime) / 1000L + " 秒..");

        printSection("加载道具");
        long startItemsTime = System.currentTimeMillis();
        MapleItemInformationProvider.getInstance().runEtc();
        MapleItemInformationProvider.getInstance().runItems();
        System.out.println("道具信息加载完成 耗时: " + (System.currentTimeMillis() - startItemsTime) / 1000L + " 秒..");

        printSection("加载爆率");
        long startDropTime = System.currentTimeMillis();
        MapleMonsterInformationProvider.getInstance().load();
        MapleMonsterInformationProvider.getInstance().addExtra();

        System.out.println("爆率信息加载完成 耗时: " + (System.currentTimeMillis() - startDropTime) / 1000L + " 秒..");

        printSection("加载技能");
        long startSkillsTime = System.currentTimeMillis();
        SkillFactory.loadAllSkills();
        System.out.println("技能数据信息加载完成 耗时: " + (System.currentTimeMillis() - startSkillsTime) / 1000L + " 秒..");

        printSection("BasicLoader");
        long startBasicTime = System.currentTimeMillis();
        LoginInformationProvider.getInstance();
        RandomRewards.load();
        MapleOxQuizFactory.getInstance();
        MapleCarnivalFactory.getInstance();
        CharacterCardFactory.getInstance().initialize();
        MobSkillFactory.getInstance();
        SpeedRunner.loadSpeedRuns();
        MTSStorage.load();
        PredictCardFactory.getInstance().initialize();
        System.out.println("BasicLoader加载完成 耗时: " + (System.currentTimeMillis() - startBasicTime) / 1000L + " 秒..");

        printSection("MIILoader");
        long startMIITime = System.currentTimeMillis();
        MapleInventoryIdentifier.getInstance();
        System.out.println("MIILoader加载完成 耗时: " + (System.currentTimeMillis() - startMIITime) / 1000L + " 秒..");

        printSection("加载商城道具");
        long startCashItemTime = System.currentTimeMillis();
        CashItemFactory.getInstance().initialize();
        System.out.println("商城道具加载完成 耗时: " + (System.currentTimeMillis() - startCashItemTime) / 1000L + " 秒..");

        printSection("登录服务器");
        LoginServer.run_startup_configurations();

        printSection("频道服务器");
        ChannelServer.startChannel_Main();

        printSection("商城服务器");
        CashShopServer.run_startup_configurations();

        printSection("拍卖服务器");
        AuctionServer.run_startup_configurations();

        Timer.CheatTimer.getInstance().register(AutobanManager.getInstance(), 60000L);
        Runtime.getRuntime().addShutdownHook(new Thread(new Shutdown()));
        printSection("刷怪线程");
        WorldRespawnService.getInstance();
        if (Boolean.parseBoolean(ServerProperties.getProperty("world.RandDrop")))
        {
            ChannelServer.getInstance(1).getMapFactory().getMap(910000000).spawnRandDrop();
        }
        ShutdownServer.registerMBean();
        ServerConstants.registerMBean();
        PlayerNPC.loadAll();
        printSection("启动完毕---江浩生日反编译版");
        LoginServer.setOn();
        System.out.println("[服务端已启动完毕，耗时 " + (System.currentTimeMillis() - startQuestTime) / 1000L + " 秒]");

        if (this.rankTime > 0)
        {
            printSection("刷新排名");
            RankingWorker.start();
        }

        if (Boolean.parseBoolean(ServerProperties.getProperty("world.AccCheck", "false")))
        {
            printSection("启动检测");
            startCheck();
        }
        printSection("在线统计");
        在线统计(Integer.parseInt(ServerProperties.getProperty("world.showUserCountTime", "30")));

        printSection("定时活动");
        PlayMSEvent.start();

        MessengerRankingWorker.getInstance();

        if (Boolean.parseBoolean(ServerProperties.getProperty("world.checkCopyItem", "false")))
        {
            checkCopyItemFromSql();
        }
    }

    public static void printSection(String s)
    {
        s = "-[ " + s + " ]";
        while (s.getBytes().length < 79)
        {
            s = "=" + s;
        }
        MapleLog.getInstance().logWrite(14, s);
    }

    public static void startCheck()
    {
        System.out.println("服务端启用检测.30秒检测一次角色是否与登录器断开连接.");
        Timer.WorldTimer.getInstance().register(new Runnable()
        {
            public void run()
            {
                for (ChannelServer cserv_ : ChannelServer.getAllInstances())
                    for (MapleCharacter chr : cserv_.getPlayerStorage().getAllCharacters())
                        if (chr != null) chr.startCheck();
            }
        }, 30000L);
    }

    public static void 在线统计(int time)
    {
        System.out.println("服务端启用在线统计." + time + "分钟统计一次在线的人数信息.");
        Timer.WorldTimer.getInstance().register(() -> {
            Map<Integer, Integer> connected = World.getConnected();
            StringBuilder conStr = new StringBuilder(FileoutputUtil.CurrentReadable_Time() + " 在线人数: ");
            for (int i : connected.keySet())
            {
                if (i == 0)
                {
                    int users = connected.get(i);
                    conStr.append(StringUtil.getRightPaddedStr(String.valueOf(users), ' ', 3));
                    if (users > Start.maxUsers)
                    {
//                            Start.access$002(users);
                    }
                    conStr.append(" 最高在线: ");
                    conStr.append(Start.maxUsers);
                    break;
                }
            }
            System.out.println(conStr.toString());
            if (Start.maxUsers > 0) FileoutputUtil.log("在线统计.txt", conStr.toString(), true);
        }, 60000 * time);
    }

    protected static void checkCopyItemFromSql()
    {
        List<Integer> equipOnlyIds = new ArrayList<>();
        Map<Integer, Integer> checkItems = new HashMap<>();
        try
        {
            Connection con = DatabaseConnection.getConnection();


            PreparedStatement ps = con.prepareStatement("SELECT * FROM inventoryitems WHERE equipOnlyId > 0");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                int itemId = rs.getInt("itemId");
                int equipOnlyId = rs.getInt("equipOnlyId");
                if (equipOnlyId > 0)
                {
                    if (checkItems.containsKey(equipOnlyId))
                    {
                        if (checkItems.get(equipOnlyId) == itemId)
                        {
                            equipOnlyIds.add(equipOnlyId);
                        }
                    }
                    else
                    {
                        checkItems.put(equipOnlyId, itemId);
                    }
                }
            }
            rs.close();
            ps.close();

            Collections.sort(equipOnlyIds);
            for (Integer equipOnlyId : equipOnlyIds)
            {
                int i = equipOnlyId;
                ps = con.prepareStatement("DELETE FROM inventoryitems WHERE equipOnlyId = ?");
                ps.setInt(1, i);
                ps.executeUpdate();
                ps.close();
                System.out.println("发现复制装备 该装备的唯一ID: " + i + " 已进行删除处理..");
                FileoutputUtil.log("装备复制.txt", "发现复制装备 该装备的唯一ID: " + i + " 已进行删除处理..", true);
            }
        }
        catch (SQLException ex)
        {
            Connection con;
            PreparedStatement ps;
            int itemId;
            System.out.println("[EXCEPTION] 清理复制装备出现错误." + ex);
        }
    }

    public int getRankTime()
    {
        return this.rankTime;
    }

    public void setRankTime(int rankTime)
    {
        this.rankTime = rankTime;
    }

    public boolean isIvCheck()
    {
        return this.ivCheck;
    }

    public static class Shutdown implements Runnable
    {
        public void run()
        {
            ShutdownServer.getInstance().run();
            ShutdownServer.getInstance().run();
        }
    }
}