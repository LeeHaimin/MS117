package server;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import client.MapleDisease;
import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;
import server.life.MobSkill;
import server.life.MobSkillFactory;

public class MapleCarnivalFactory
{
    private static final MapleCarnivalFactory instance = new MapleCarnivalFactory();
    private final Map<Integer, MCSkill> skills = new HashMap<>();
    private final Map<Integer, MCSkill> guardians = new HashMap<>();
    private final MapleDataProvider dataRoot = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/Skill.wz"));

    public MapleCarnivalFactory()
    {
        initialize();
    }

    private void initialize()
    {
        if (!this.skills.isEmpty())
        {
            return;
        }
        for (MapleData z : this.dataRoot.getData("MCSkill.img"))
        {
            this.skills.put(Integer.parseInt(z.getName()), new MCSkill(MapleDataTool.getInt("spendCP", z, 0), MapleDataTool.getInt("mobSkillID", z, 0), MapleDataTool.getInt("level", z, 0),
                    MapleDataTool.getInt("target", z, 1) > 1));
        }
        for (MapleData z : this.dataRoot.getData("MCGuardian.img"))
        {
            this.guardians.put(Integer.parseInt(z.getName()), new MCSkill(MapleDataTool.getInt("spendCP", z, 0), MapleDataTool.getInt("mobSkillID", z, 0), MapleDataTool.getInt("level", z, 0), true));
        }
    }

    public static MapleCarnivalFactory getInstance()
    {
        return instance;
    }

    public MCSkill getSkill(int id)
    {
        return this.skills.get(id);
    }


    public MCSkill getGuardian(int id)
    {
        return this.guardians.get(id);
    }

    public static class MCSkill
    {
        public final int cpLoss;
        public final int skillid;
        public final int level;
        public final boolean targetsAll;

        public MCSkill(int _cpLoss, int _skillid, int _level, boolean _targetsAll)
        {
            this.cpLoss = _cpLoss;
            this.skillid = _skillid;
            this.level = _level;
            this.targetsAll = _targetsAll;
        }

        public MobSkill getSkill()
        {
            return MobSkillFactory.getInstance().getMobSkill(this.skillid, 1);
        }

        public MapleDisease getDisease()
        {
            if (this.skillid <= 0)
            {
                return MapleDisease.getRandom();
            }
            return MapleDisease.getBySkill(this.skillid);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\MapleCarnivalFactory.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */