package server;

import java.util.List;
import java.util.Map;

import client.inventory.Equip;
import tools.Triple;

public class ItemInformation
{
    public List<Integer> scrollReqs = null;
    public List<Integer> questItems = null;
    public List<Integer> incSkill = null;

    public Equip eq = null;

    public double price = 0.0D;


    public List<StructRewardItem> rewardItems = null;
    public List<Triple<String, String, String>> equipAdditions = null;
    public Map<Integer, Map<String, Integer>> equipIncs = null;
    public short slotMax;
    public short itemMakeLevel;
    public Map<String, Integer> equipStats;
    public int itemId;
    public int wholePrice;
    public int monsterBook;
    public int stateChange;
    public int meso;
    public int questId;
    public int totalprob;
    public int replaceItem;
    public int mob;
    public int cardSet;
    public int create;
    public int flag;
    public String name;
    public String desc;
    public String msg;
    public String replaceMsg;
    public String afterImage;
    public byte karmaEnabled;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\ItemInformation.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */