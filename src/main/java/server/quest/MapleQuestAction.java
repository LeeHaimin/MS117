package server.quest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleQuestStatus;
import client.MapleTraitType;
import client.Skill;
import client.inventory.MapleInventoryType;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.Triple;

public class MapleQuestAction implements java.io.Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private final MapleQuestActionType type;
    private final MapleQuest quest;
    private final List<Integer> applicableJobs = new ArrayList<>();
    private int intStore = 0;
    private List<QuestItem> items = null;
    private List<Triple<Integer, Integer, Integer>> skill = null;
    private List<Pair<Integer, Integer>> state = null;


    public MapleQuestAction(MapleQuestActionType type, ResultSet rse, MapleQuest quest, PreparedStatement pss, PreparedStatement psq, PreparedStatement psi) throws java.sql.SQLException
    {
        this.type = type;
        this.quest = quest;

        this.intStore = rse.getInt("intStore");
        String[] jobs = rse.getString("applicableJobs").split(", ");
        if ((jobs.length <= 0) && (rse.getString("applicableJobs").length() > 0))
        {
            this.applicableJobs.add(Integer.parseInt(rse.getString("applicableJobs")));
        }
        for (String j : jobs)
        {
            if (j.length() > 0)
            {
                this.applicableJobs.add(Integer.parseInt(j));
            }
        }
        ResultSet rs;
        switch (type)
        {
            case item:
                this.items = new ArrayList<>();
                psi.setInt(1, rse.getInt("uniqueid"));
                rs = psi.executeQuery();
                while (rs.next())
                {
                    this.items.add(new QuestItem(rs.getInt("itemid"), rs.getInt("count"), rs.getInt("period"), rs.getInt("gender"), rs.getInt("job"), rs.getInt("jobEx"), rs.getInt("prop")));
                }
                rs.close();
                break;
            case quest:
                this.state = new ArrayList<>();
                psq.setInt(1, rse.getInt("uniqueid"));
                rs = psq.executeQuery();
                while (rs.next())
                {
                    this.state.add(new Pair(rs.getInt("quest"), rs.getInt("state")));
                }
                rs.close();
                break;
            case skill:
                this.skill = new ArrayList<>();
                pss.setInt(1, rse.getInt("uniqueid"));
                rs = pss.executeQuery();
                while (rs.next())
                {
                    this.skill.add(new Triple(rs.getInt("skillid"), rs.getInt("skillLevel"), rs.getInt("masterLevel")));
                }
                rs.close();
        }
    }

    public boolean RestoreLostItem(MapleCharacter chr, int itemid)
    {
        if (this.type == MapleQuestActionType.item)
        {
            for (QuestItem item : this.items)
            {
                if (item.itemid == itemid)
                {
                    if (!chr.haveItem(item.itemid, item.count, true, false))
                    {
                        MapleInventoryManipulator.addById(chr.getClient(), item.itemid, (short) item.count,
                                "Obtained from quest (Restored) " + this.quest.getId() + " on " + tools.FileoutputUtil.CurrentReadable_Date());
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public void runStart(MapleCharacter chr, Integer extSelection)
    {
        int i;
        int selection;
        int extNum;
        Pair<Integer, Integer> q;
        int masterLevel;
        MapleQuestStatus status;
        switch (this.type)
        {
            case exp:
                status = chr.getQuest(this.quest);
                if (status.getForfeited() <= 0)
                {

                    chr.gainExp(this.intStore * constants.GameConstants.getExpRate_Quest(chr.getLevel()) * chr.getStat().questBonus * (chr.getTrait(MapleTraitType.sense).getLevel() * 3 / 10 + 100) / 100, true, true, true);
                }
                break;

            case item:
                Map<Integer, Integer> props = new HashMap<>();
                for (QuestItem item : this.items)
                {
                    if ((item.prop > 0) && (canGetItem(item, chr)))
                    {
                        for (i = 0; i < item.prop; i++)
                        {
                            props.put(props.size(), item.itemid);
                        }
                    }
                }
                selection = 0;
                extNum = 0;
                if (props.size() > 0)
                {
                    selection = props.get(server.Randomizer.nextInt(props.size()));
                }
                for (QuestItem item : this.items)
                    if (canGetItem(item, chr))
                    {

                        int id = item.itemid;
                        if ((item.prop == -2) || (item.prop == -1 ? (extSelection == null) && (extSelection == extNum++) :


                                id == selection))
                        {


                            short count = (short) item.count;
                            if (count < 0)
                            {
                                try
                                {
                                    MapleInventoryManipulator.removeById(chr.getClient(), constants.ItemConstants.getInventoryType(id), id, count * -1, true, false);
                                }
                                catch (client.inventory.InventoryException ie)
                                {
                                    System.err.println("[h4x] Completing a quest without meeting the requirements" + ie);
                                }
                                chr.getClient().getSession().write(MaplePacketCreator.getShowItemGain(id, count, true));
                            }
                            else
                            {
                                int period = item.period / 1440;
                                String name = MapleItemInformationProvider.getInstance().getName(id);
                                if ((id / 10000 == 114) && (name != null) && (name.length() > 0))
                                {
                                    String msg = "恭喜您获得勋章 <" + name + ">";
                                    chr.dropMessage(-1, msg);
                                    chr.dropMessage(5, msg);
                                }
                                MapleInventoryManipulator.addById(chr.getClient(), id, count, "", null, period, "任务获得 " + this.quest.getId() + " 时间: " + tools.FileoutputUtil.CurrentReadable_Date());
                                chr.getClient().getSession().write(MaplePacketCreator.getShowItemGain(id, count, true));
                            }
                        }
                    }
                break;
            case nextQuest:
                status = chr.getQuest(this.quest);
                if (status.getForfeited() <= 0)
                {

                    chr.getClient().getSession().write(MaplePacketCreator.updateQuestFinish(this.quest.getId(), status.getNpc(), this.intStore));
                }
                break;
            case money:
                status = chr.getQuest(this.quest);
                if (status.getForfeited() <= 0)
                {

                    chr.gainMeso(this.intStore, true, true);
                }
                break;
            case quest:
                for (Pair<Integer, Integer> integerIntegerPair : this.state)
                {
                    q = integerIntegerPair;
                    chr.updateQuest(new MapleQuestStatus(MapleQuest.getInstance(q.left), q.right));
                }
                break;
            case skill:
                Map<Skill, client.SkillEntry> list = new HashMap<>();
                for (Triple<Integer, Integer, Integer> skills : this.skill)
                {
                    int skillid = skills.left;
                    int skillLevel = skills.mid;
                    masterLevel = skills.right;
                    Skill skillObject = client.SkillFactory.getSkill(skillid);
                    boolean found = false;
                    for (Integer job : this.applicableJobs)
                    {
                        int applicableJob = job;
                        if (chr.getJob() == applicableJob)
                        {
                            found = true;
                            break;
                        }
                    }
                    if ((skillObject.isBeginnerSkill()) || (found))
                    {
                        list.put(skillObject, new client.SkillEntry((byte) Math.max(skillLevel, chr.getSkillLevel(skillObject)), (byte) Math.max(masterLevel, chr.getMasterLevel(skillObject)),
                                client.SkillFactory.getDefaultSExpiry(skillObject)));
                    }
                }
                chr.changeSkillsLevel(list);
                break;
            case pop:
                status = chr.getQuest(this.quest);
                if (status.getForfeited() <= 0)
                {

                    int fameGain = this.intStore;
                    chr.addFame(fameGain);
                    chr.updateSingleStat(client.MapleStat.人气, chr.getFame());
                    chr.getClient().getSession().write(MaplePacketCreator.getShowFameGain(fameGain));
                }
                break;
            case buffItemID:
                status = chr.getQuest(this.quest);
                if (status.getForfeited() <= 0)
                {

                    int tobuff = this.intStore;
                    if (tobuff > 0)
                    {

                        MapleItemInformationProvider.getInstance().getItemEffect(tobuff).applyTo(chr);
                    }
                }
                break;

            case infoNumber:
                break;
            case sp:
                status = chr.getQuest(this.quest);
                if (status.getForfeited() <= 0)
                {
                    int sp_val = this.intStore;
                    if (this.applicableJobs.size() > 0)
                    {
                        int finalJob = 0;
                        for (int job_val : this.applicableJobs)
                        {
                            if ((chr.getJob() >= job_val) && (job_val > finalJob))
                            {
                                finalJob = job_val;
                            }
                        }
                        if (finalJob == 0)
                        {
                            chr.gainSP(sp_val);
                        }
                        else
                        {
                            chr.gainSP(sp_val, constants.GameConstants.getSkillBookByJob(finalJob));
                        }
                    }
                    else
                    {
                        chr.gainSP(sp_val);
                    }
                }
                break;

            case charmEXP:
            case charismaEXP:
            case craftEXP:
            case insightEXP:
            case senseEXP:
            case willEXP:
                status = chr.getQuest(this.quest);
                if (status.getForfeited() <= 0)
                {

                    chr.getTrait(MapleTraitType.getByQuestName(this.type.name())).addExp(this.intStore, chr);
                }
                break;
        }

    }

    private static boolean canGetItem(QuestItem item, MapleCharacter chr)
    {
        if ((item.gender != 2) && (item.gender >= 0) && (item.gender != chr.getGender()))
        {
            return false;
        }
        if (item.job > 0)
        {
            int codec;
            List<Integer> code = getJobBy5ByteEncoding(item.job);
            boolean jobFound = false;
            for (Integer integer : code)
            {
                codec = integer;
                if (codec / 100 == chr.getJob() / 100)
                {
                    jobFound = true;
                    break;
                }
            }
            if ((!jobFound) && (item.jobEx > 0))
            {
                List<Integer> codeEx = getJobBySimpleEncoding(item.jobEx);
                for (Integer o : codeEx)
                {
                    codec = o;
                    if (codec / 100 % 10 == chr.getJob() / 100 % 10)
                    {
                        jobFound = true;
                        break;
                    }
                }
            }
            return jobFound;
        }
        return true;
    }

    private static List<Integer> getJobBy5ByteEncoding(int encoded)
    {
        List<Integer> ret = new ArrayList<>();
        if ((encoded & 0x1) != 0)
        {
            ret.add(0);
        }
        if ((encoded & 0x2) != 0)
        {
            ret.add(100);
        }
        if ((encoded & 0x4) != 0)
        {
            ret.add(200);
        }
        if ((encoded & 0x8) != 0)
        {
            ret.add(300);
        }
        if ((encoded & 0x10) != 0)
        {
            ret.add(400);
        }
        if ((encoded & 0x20) != 0)
        {
            ret.add(500);
        }
        if ((encoded & 0x400) != 0)
        {
            ret.add(1000);
        }
        if ((encoded & 0x800) != 0)
        {
            ret.add(1100);
        }
        if ((encoded & 0x1000) != 0)
        {
            ret.add(1200);
        }
        if ((encoded & 0x2000) != 0)
        {
            ret.add(1300);
        }
        if ((encoded & 0x4000) != 0)
        {
            ret.add(1400);
        }
        if ((encoded & 0x8000) != 0)
        {
            ret.add(1500);
        }
        if ((encoded & 0x20000) != 0)
        {
            ret.add(2001);
            ret.add(2200);
        }
        if ((encoded & 0x100000) != 0)
        {
            ret.add(2000);
            ret.add(2001);
        }
        if ((encoded & 0x200000) != 0)
        {
            ret.add(2100);
        }
        if ((encoded & 0x400000) != 0)
        {
            ret.add(2001);
            ret.add(2200);
        }
        if ((encoded & 0x40000000) != 0)
        {
            ret.add(3000);
            ret.add(3200);
            ret.add(3300);
            ret.add(3500);
        }
        return ret;
    }

    private static List<Integer> getJobBySimpleEncoding(int encoded)
    {
        List<Integer> ret = new ArrayList<>();
        if ((encoded & 0x1) != 0)
        {
            ret.add(200);
        }
        if ((encoded & 0x2) != 0)
        {
            ret.add(300);
        }
        if ((encoded & 0x4) != 0)
        {
            ret.add(400);
        }
        if ((encoded & 0x8) != 0)
        {
            ret.add(500);
        }
        return ret;
    }

    public boolean checkEnd(MapleCharacter chr, Integer extSelection)
    {
        switch (this.type)
        {
            case item:
                Map<Integer, Integer> props = new HashMap<>();

                for (QuestItem item : this.items)
                {
                    if ((item.prop > 0) && (canGetItem(item, chr)))
                    {
                        for (int i = 0; i < item.prop; i++)
                        {
                            props.put(props.size(), item.itemid);
                        }
                    }
                }
                int selection = 0;
                int extNum = 0;
                if (props.size() > 0)
                {
                    selection = props.get(server.Randomizer.nextInt(props.size()));
                }
                byte eq = 0;
                byte use = 0;
                byte setup = 0;
                byte etc = 0;
                byte cash = 0;

                for (QuestItem item : this.items)
                    if (canGetItem(item, chr))
                    {

                        int id = item.itemid;
                        if ((item.prop == -2) || (item.prop == -1 ? (extSelection == null) && (extSelection == extNum++) :


                                id == selection))
                        {


                            short count = (short) item.count;
                            if (count < 0)
                            {
                                if (!chr.haveItem(id, count, false, true))
                                {
                                    chr.dropMessage(1, "您的任务道具不够，还不能完成任务.");
                                    return false;
                                }
                            }
                            else
                            {
                                if ((MapleItemInformationProvider.getInstance().isPickupRestricted(id)) && (chr.haveItem(id, 1, true, false)))
                                {
                                    chr.dropMessage(1, "You have this item already: " + MapleItemInformationProvider.getInstance().getName(id));
                                    return false;
                                }
                                switch (constants.ItemConstants.getInventoryType(id))
                                {
                                    case EQUIP:
                                        eq = (byte) (eq + 1);
                                        break;
                                    case USE:
                                        use = (byte) (use + 1);
                                        break;
                                    case SETUP:
                                        setup = (byte) (setup + 1);
                                        break;
                                    case ETC:
                                        etc = (byte) (etc + 1);
                                        break;
                                    case CASH:
                                        cash = (byte) (cash + 1);
                                }
                            }
                        }
                    }
                if (chr.getInventory(MapleInventoryType.EQUIP).getNumFreeSlot() < eq)
                {
                    chr.dropMessage(1, "装备栏空间不足.");
                    return false;
                }
                if (chr.getInventory(MapleInventoryType.USE).getNumFreeSlot() < use)
                {
                    chr.dropMessage(1, "消耗栏空间不足.");
                    return false;
                }
                if (chr.getInventory(MapleInventoryType.SETUP).getNumFreeSlot() < setup)
                {
                    chr.dropMessage(1, "设置栏空间不足.");
                    return false;
                }
                if (chr.getInventory(MapleInventoryType.ETC).getNumFreeSlot() < etc)
                {
                    chr.dropMessage(1, "其他栏空间不足.");
                    return false;
                }
                if (chr.getInventory(MapleInventoryType.CASH).getNumFreeSlot() < cash)
                {
                    chr.dropMessage(1, "特殊栏空间不足.");
                    return false;
                }
                return true;

            case money:
                int meso = this.intStore;
                if (chr.getMeso() + meso < 0)
                {
                    chr.dropMessage(1, "携带金币数量已达限制.");
                    return false;
                }
                if ((meso < 0) && (chr.getMeso() < Math.abs(meso)))
                {
                    chr.dropMessage(1, "金币不足.");
                    return false;
                }
                return true;
        }

        return true;
    }

    public void runEnd(MapleCharacter chr, Integer extSelection)
    {
        Map<Integer, Integer> props;
        int i;
        int selection;
        int extNum;
        String name;
        Object q;
        int skillid;
        switch (this.type)
        {
            case exp:
                chr.gainExp(this.intStore * constants.GameConstants.getExpRate_Quest(chr.getLevel()) * chr.getStat().questBonus * (chr.getTrait(MapleTraitType.sense).getLevel() * 3 / 10 + 100) / 100, true, true, true);
                break;


            case item:
                props = new HashMap<>();
                for (QuestItem item : this.items)
                {
                    if ((item.prop > 0) && (canGetItem(item, chr)))
                    {
                        for (i = 0; i < item.prop; i++)
                        {
                            props.put(props.size(), item.itemid);
                        }
                    }
                }
                selection = 0;
                extNum = 0;
                if (props.size() > 0)
                {
                    selection = props.get(server.Randomizer.nextInt(props.size()));
                }
                for (QuestItem item : this.items)
                    if (canGetItem(item, chr))
                    {

                        int id = item.itemid;
                        if ((item.prop == -2) || (item.prop == -1 ? (extSelection == null) && (extSelection == extNum++) :


                                id == selection))
                        {


                            short count = (short) item.count;
                            if (count < 0)
                            {
                                MapleInventoryManipulator.removeById(chr.getClient(), constants.ItemConstants.getInventoryType(id), id, count * -1, true, false);
                                chr.getClient().getSession().write(MaplePacketCreator.getShowItemGain(id, count, true));
                            }
                            else
                            {
                                int period = item.period / 1440;
                                name = MapleItemInformationProvider.getInstance().getName(id);
                                if ((id / 10000 == 114) && (name != null) && (name.length() > 0))
                                {
                                    String msg = "你获得了勋章 <" + name + ">";
                                    chr.dropMessage(-1, msg);
                                    chr.dropMessage(5, msg);
                                }
                                MapleInventoryManipulator.addById(chr.getClient(), id, count, "", null, period, "任务获得 " + this.quest.getId() + " 时间: " + tools.FileoutputUtil.CurrentReadable_Date());
                                chr.getClient().getSession().write(MaplePacketCreator.getShowItemGain(id, count, true));
                            }
                        }
                    }
                break;

            case nextQuest:
                chr.getClient().getSession().write(MaplePacketCreator.updateQuestFinish(this.quest.getId(), chr.getQuest(this.quest).getNpc(), this.intStore));
                break;

            case money:
                chr.gainMeso(this.intStore, true, true);
                break;

            case quest:
                for (Pair<Integer, Integer> integerIntegerPair : this.state)
                {
                    q = integerIntegerPair;
                    chr.updateQuest(new MapleQuestStatus(MapleQuest.getInstance((Integer) ((Pair) q).left), (Integer) ((Pair) q).right));
                }
                break;

            case skill:
                Map<Skill, client.SkillEntry> list = new HashMap<>();
                for (q = this.skill.iterator(); ((Iterator) q).hasNext(); )
                {
                    Triple<Integer, Integer, Integer> skills = (Triple) ((Iterator) q).next();
                    skillid = skills.left;
                    int skillLevel = skills.mid;
                    int masterLevel = skills.right;
                    Skill skillObject = client.SkillFactory.getSkill(skillid);
                    boolean found = false;
                    for (int applicableJob : this.applicableJobs)
                    {
                        if (chr.getJob() == applicableJob)
                        {
                            found = true;
                            break;
                        }
                    }
                    if ((skillObject.isBeginnerSkill()) || (found))
                    {
                        list.put(skillObject, new client.SkillEntry((byte) Math.max(skillLevel, chr.getSkillLevel(skillObject)), (byte) Math.max(masterLevel, chr.getMasterLevel(skillObject)),
                                client.SkillFactory.getDefaultSExpiry(skillObject)));
                    }
                }
                chr.changeSkillsLevel(list);
                break;
            case pop:
                int fameGain = this.intStore;
                chr.addFame(fameGain);
                chr.updateSingleStat(client.MapleStat.人气, chr.getFame());
                chr.getClient().getSession().write(MaplePacketCreator.getShowFameGain(fameGain));
                break;

            case buffItemID:
                int tobuff = this.intStore;
                if (tobuff > 0)
                {

                    MapleItemInformationProvider.getInstance().getItemEffect(tobuff).applyTo(chr);
                }
                break;


            case infoNumber:
                break;


            case sp:
                int sp_val = this.intStore;
                if (this.applicableJobs.size() > 0)
                {
                    int finalJob = 0;
                    for (int job_val : this.applicableJobs)
                    {
                        if ((chr.getJob() >= job_val) && (job_val > finalJob))
                        {
                            finalJob = job_val;
                        }
                    }
                    if (finalJob == 0)
                    {
                        chr.gainSP(sp_val);
                    }
                    else
                    {
                        chr.gainSP(sp_val, constants.GameConstants.getSkillBookByJob(finalJob));
                    }
                }
                else
                {
                    chr.gainSP(sp_val);
                }
                break;

            case charmEXP:
            case charismaEXP:
            case craftEXP:
            case insightEXP:
            case senseEXP:
            case willEXP:
                chr.getTrait(MapleTraitType.getByQuestName(this.type.name())).addExp(this.intStore, chr);
                break;
        }

    }

    public MapleQuestActionType getType()
    {
        return this.type;
    }

    public String toString()
    {
        return this.type.toString();
    }

    public List<Triple<Integer, Integer, Integer>> getSkills()
    {
        return this.skill;
    }


    public List<QuestItem> getItems()
    {
        return this.items;
    }

    public static class QuestItem
    {
        public final int itemid;
        public final int count;
        public final int period;
        public final int gender;
        public final int job;
        public final int jobEx;
        public final int prop;

        public QuestItem(int itemid, int count, int period, int gender, int job, int jobEx, int prop)
        {
            if (server.RandomRewards.getTenPercent().contains(itemid))
            {
                count += server.Randomizer.nextInt(3);
            }
            this.itemid = itemid;
            this.count = count;
            this.period = period;
            this.gender = gender;
            this.job = job;
            this.jobEx = jobEx;
            this.prop = prop;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\quest\MapleQuestAction.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */