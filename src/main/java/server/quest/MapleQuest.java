package server.quest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.MapleCharacter;
import client.MapleQuestStatus;
import constants.GameConstants;
import tools.MaplePacketCreator;
import tools.Pair;

public class MapleQuest implements java.io.Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private static final Map<Integer, MapleQuest> quests = new LinkedHashMap<>();
    protected final int id;
    protected final List<MapleQuestRequirement> startReqs = new LinkedList<>();
    protected final List<MapleQuestRequirement> completeReqs = new LinkedList<>();
    protected final List<MapleQuestAction> startActs = new LinkedList<>();
    protected final List<MapleQuestAction> completeActs = new LinkedList<>();
    protected final Map<String, List<Pair<String, Pair<String, Integer>>>> partyQuestInfo = new LinkedHashMap<>();
    protected final Map<Integer, Integer> relevantMobs = new LinkedHashMap<>();
    protected final Map<Integer, Integer> questItems = new LinkedHashMap<>();
    protected String name = "";
    private boolean autoStart = false;
    private boolean autoPreComplete = false;
    private boolean repeatable = false;
    private boolean customend = false;
    private boolean blocked = false;
    private boolean autoAccept = false;
    private boolean autoComplete = false;
    private boolean scriptedStart = false;
    private int viewMedalItem = 0;
    private int selectedSkillID = 0;

    protected MapleQuest(int id)
    {
        this.id = id;
    }

    public static void initQuests()
    {
        try
        {
            Connection con = database.DatabaseConnectionWZ.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM wz_questdata");
            PreparedStatement psr = con.prepareStatement("SELECT * FROM wz_questreqdata WHERE questid = ?");
            PreparedStatement psa = con.prepareStatement("SELECT * FROM wz_questactdata WHERE questid = ?");
            PreparedStatement pss = con.prepareStatement("SELECT * FROM wz_questactskilldata WHERE uniqueid = ?");
            PreparedStatement psq = con.prepareStatement("SELECT * FROM wz_questactquestdata WHERE uniqueid = ?");
            PreparedStatement psi = con.prepareStatement("SELECT * FROM wz_questactitemdata WHERE uniqueid = ?");
            PreparedStatement psp = con.prepareStatement("SELECT * FROM wz_questpartydata WHERE questid = ?");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                quests.put(rs.getInt("questid"), loadQuest(rs, psr, psa, pss, psq, psi, psp));
            }
            rs.close();
            ps.close();
            psr.close();
            psa.close();
            pss.close();
            psq.close();
            psi.close();
            psp.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println("共加载 " + quests.size() + " 个任务信息.");
    }

    private static MapleQuest loadQuest(ResultSet rs, PreparedStatement psr, PreparedStatement psa, PreparedStatement pss, PreparedStatement psq, PreparedStatement psi, PreparedStatement psp) throws java.sql.SQLException
    {
        MapleQuest ret = new MapleQuest(rs.getInt("questid"));
        ret.name = rs.getString("name");
        ret.autoStart = (rs.getInt("autoStart") > 0);
        ret.autoPreComplete = (rs.getInt("autoPreComplete") > 0);
        ret.autoAccept = (rs.getInt("autoAccept") > 0);
        ret.autoComplete = (rs.getInt("autoComplete") > 0);
        ret.viewMedalItem = rs.getInt("viewMedalItem");
        ret.selectedSkillID = rs.getInt("selectedSkillID");
        ret.blocked = (rs.getInt("blocked") > 0);

        psr.setInt(1, ret.id);
        ResultSet rse = psr.executeQuery();
        while (rse.next())
        {
            MapleQuestRequirementType type = MapleQuestRequirementType.getByWZName(rse.getString("name"));
            MapleQuestRequirement req = new MapleQuestRequirement(ret, type, rse);
            if (type.equals(MapleQuestRequirementType.interval))
            {
                ret.repeatable = true;
            }
            else if (type.equals(MapleQuestRequirementType.normalAutoStart))
            {
                ret.repeatable = true;
                ret.autoStart = true;
            }
            else if (type.equals(MapleQuestRequirementType.startscript))
            {
                ret.scriptedStart = true;
            }
            else if (type.equals(MapleQuestRequirementType.endscript))
            {
                ret.customend = true;
            }
            else if (type.equals(MapleQuestRequirementType.mob))
            {
                for (Pair<Integer, Integer> mob : req.getDataStore())
                {
                    ret.relevantMobs.put(mob.left, mob.right);
                }
            }
            else if (type.equals(MapleQuestRequirementType.item))
            {
                for (Pair<Integer, Integer> it : req.getDataStore())
                {
                    ret.questItems.put(it.left, it.right);
                }
            }
            if (rse.getInt("type") == 0)
            {
                ret.startReqs.add(req);
            }
            else
            {
                ret.completeReqs.add(req);
            }
        }
        rse.close();

        psa.setInt(1, ret.id);
        rse = psa.executeQuery();
        while (rse.next())
        {
            MapleQuestActionType ty = MapleQuestActionType.getByWZName(rse.getString("name"));
            if (rse.getInt("type") == 0)
            {
                if ((ty != MapleQuestActionType.item) || (ret.id != 7103))
                {

                    ret.startActs.add(new MapleQuestAction(ty, rse, ret, pss, psq, psi));
                }
            }
            else if ((ty != MapleQuestActionType.item) || (ret.id != 7102))
            {

                ret.completeActs.add(new MapleQuestAction(ty, rse, ret, pss, psq, psi));
            }
        }
        rse.close();

        psp.setInt(1, ret.id);
        rse = psp.executeQuery();
        while (rse.next())
        {
            if (!ret.partyQuestInfo.containsKey(rse.getString("rank")))
            {
                ret.partyQuestInfo.put(rse.getString("rank"), new ArrayList());
            }
            ret.partyQuestInfo.get(rse.getString("rank")).add(new Pair(rse.getString("mode"), new Pair(rse.getString("property"), rse.getInt("value"))));
        }
        rse.close();
        return ret;
    }

    public static MapleQuest getInstance(int id)
    {
        MapleQuest ret = quests.get(id);
        if (ret == null)
        {
            ret = new MapleQuest(id);
            quests.put(id, ret);
        }
        return ret;
    }

    public static java.util.Collection<MapleQuest> getAllInstances()
    {
        return quests.values();
    }

    public List<Pair<String, Pair<String, Integer>>> getInfoByRank(String rank)
    {
        return this.partyQuestInfo.get(rank);
    }

    public boolean isPartyQuest()
    {
        return this.partyQuestInfo.size() > 0;
    }

    public int getSkillID()
    {
        return this.selectedSkillID;
    }

    public String getName()
    {
        return this.name;
    }

    public List<MapleQuestAction> getCompleteActs()
    {
        return this.completeActs;
    }

    public boolean canComplete(MapleCharacter chr, Integer npcid)
    {
        if (chr.getQuest(this).getStatus() != 1)
        {
            return false;
        }
        if ((this.blocked) && (!chr.isGM()))
        {
            return false;
        }
        if ((this.autoComplete) && (npcid != null) && (this.viewMedalItem <= 0))
        {
            forceComplete(chr, npcid);
            return false;
        }
        for (MapleQuestRequirement r : this.completeReqs)
        {
            if (!r.check(chr, npcid))
            {
                return false;
            }
        }
        return true;
    }

    public void RestoreLostItem(MapleCharacter chr, int itemid)
    {
        if ((this.blocked) && (!chr.isGM()))
        {
            return;
        }
        for (MapleQuestAction a : this.startActs)
        {
            if (a.RestoreLostItem(chr, itemid))
            {
                break;
            }
        }
    }

    public void start(MapleCharacter chr, int npc)
    {
        if (chr.isShowPacket())
        {
            chr.dropMessage(6, "开始任务 start: " + npc + " autoStart：" + this.autoStart + " checkNPCOnMap: " + checkNPCOnMap(chr, npc) + " canStart: " + canStart(chr, npc));
        }
        if (((this.autoStart) || (checkNPCOnMap(chr, npc))) && (canStart(chr, npc)))
        {
            for (MapleQuestAction a : this.startActs)
            {
                if (!a.checkEnd(chr, null))
                {
                    if (chr.isShowPacket())
                    {
                        chr.dropMessage(6, "开始任务 checkEnd 错误...");
                    }
                    return;
                }
            }
            for (MapleQuestAction a : this.startActs)
            {
                a.runStart(chr, null);
            }
            if (!this.customend)
            {
                forceStart(chr, npc, null);
            }
            else
            {
                scripting.quest.QuestScriptManager.getInstance().endQuest(chr.getClient(), npc, getId(), true);
            }
        }
    }

    private boolean checkNPCOnMap(MapleCharacter player, int npcId)
    {
        return ((GameConstants.is龙神(player.getJob())) && (npcId == 1013000)) || ((GameConstants.is恶魔猎手(player.getJob())) && (npcId == 0)) || ((GameConstants.is双弩精灵(player.getJob())) && (npcId == 0)) || (npcId == 2151009) || (npcId == 3000018) || (npcId == 9010000) || ((npcId >= 2161000) && (npcId <= 2161011)) || (npcId == 9000040) || (npcId == 9000066) || (npcId == 2010010) || (npcId == 1032204) || (npcId == 0) || (npcId == 2182001) || (


                (player.getMap() != null) && (player.getMap().containsNPC(npcId)));
    }

    public boolean canStart(MapleCharacter chr, Integer npcid)
    {
        if ((chr.getQuest(this).getStatus() != 0) && ((chr.getQuest(this).getStatus() != 2) || (!this.repeatable)))
        {
            if (chr.isShowPacket())
            {
                chr.dropMessage(6,
                        "开始任务 canStart: " + (chr.getQuest(this).getStatus() != 0) + " - " + ((chr.getQuest(this).getStatus() != 2) || (!this.repeatable)) + " repeatable: " + this.repeatable);
            }
            return false;
        }
        if ((this.blocked) && (!chr.isGM()))
        {
            if (chr.isShowPacket())
            {
                chr.dropMessage(6, "开始任务 canStart - blocked " + this.blocked);
            }
            return false;
        }


        for (MapleQuestRequirement r : this.startReqs)
        {
            if ((r.getType() == MapleQuestRequirementType.dayByDay) && (npcid != null))
            {
                forceComplete(chr, npcid);
                return false;
            }
            if (!r.check(chr, npcid))
            {
                if (chr.isShowPacket())
                {
                    chr.dropMessage(6, "开始任务 canStart - check " + (!r.check(chr, npcid)));
                }
                return false;
            }
        }
        return true;
    }

    public void forceStart(MapleCharacter chr, int npc, String customData)
    {
        if (chr.isShowPacket())
        {
            chr.dropSpouseMessage(20, "[Start] 开始任务 任务ID： " + getId() + " 任务Npc: " + npc);
        }
        MapleQuestStatus newStatus = new MapleQuestStatus(this, (byte) 1, npc);
        newStatus.setForfeited(chr.getQuest(this).getForfeited());
        newStatus.setCompletionTime(chr.getQuest(this).getCompletionTime());
        newStatus.setCustomData(customData);
        chr.updateQuest(newStatus);
    }

    public int getId()
    {
        return this.id;
    }

    public void forceComplete(MapleCharacter chr, int npc)
    {
        if (chr.isShowPacket())
        {
            chr.dropSpouseMessage(20, "[Complete] 完成任务 任务ID： " + getId() + " 任务Npc: " + npc);
        }
        MapleQuestStatus newStatus = new MapleQuestStatus(this, (byte) 2, npc);
        newStatus.setForfeited(chr.getQuest(this).getForfeited());
        chr.updateQuest(newStatus);
    }

    public void complete(MapleCharacter chr, int npc)
    {
        complete(chr, npc, null);
    }

    public void complete(MapleCharacter chr, int npc, Integer selection)
    {
        if ((chr.getMap() != null) && ((this.autoPreComplete) || (checkNPCOnMap(chr, npc))) && (canComplete(chr, npc)))
        {
            for (MapleQuestAction a : this.completeActs)
            {
                if (!a.checkEnd(chr, selection))
                {
                    return;
                }
            }
            forceComplete(chr, npc);
            for (MapleQuestAction a : this.completeActs)
            {
                a.runEnd(chr, selection);
            }
            chr.getClient().getSession().write(MaplePacketCreator.showSpecialEffect(13));
            chr.getMap().broadcastMessage(chr, MaplePacketCreator.showSpecialEffect(chr.getId(), 13), false);
        }
    }

    public void forfeit(MapleCharacter chr)
    {
        if (chr.getQuest(this).getStatus() != 1)
        {
            return;
        }
        MapleQuestStatus oldStatus = chr.getQuest(this);
        MapleQuestStatus newStatus = new MapleQuestStatus(this, 0);
        newStatus.setForfeited(oldStatus.getForfeited() + 1);
        newStatus.setCompletionTime(oldStatus.getCompletionTime());
        chr.updateQuest(newStatus);
    }

    public Map<Integer, Integer> getRelevantMobs()
    {
        return this.relevantMobs;
    }

    public int getMedalItem()
    {
        return this.viewMedalItem;
    }

    public boolean isBlocked()
    {
        return this.blocked;
    }

    public int getAmountofItems(int itemId)
    {
        return this.questItems.get(itemId) != null ? this.questItems.get(itemId) : 0;
    }

    public boolean hasStartScript()
    {
        return this.scriptedStart;
    }

    public boolean hasEndScript()
    {
        return this.customend;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\quest\MapleQuest.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */