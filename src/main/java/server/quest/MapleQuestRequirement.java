package server.quest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import client.MapleCharacter;
import client.MapleQuestStatus;
import client.MapleTraitType;
import client.Skill;
import client.SkillFactory;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.inventory.MaplePet;
import constants.ItemConstants;
import tools.Pair;

public class MapleQuestRequirement implements java.io.Serializable
{
    private static final long serialVersionUID = 9179541993413738569L;
    private final MapleQuest quest;
    private final MapleQuestRequirementType type;
    private int intStore;
    private String stringStore;
    private List<Pair<Integer, Integer>> dataStore;

    public MapleQuestRequirement(MapleQuest quest, MapleQuestRequirementType type, ResultSet rse) throws SQLException
    {
        this.type = type;
        this.quest = quest;

        switch (type)
        {
            case pet:
            case mbcard:
            case mob:
            case item:
            case quest:
            case skill:
            case job:
                this.dataStore = new LinkedList<>();
                String[] first = rse.getString("intStoresFirst").split(", ");
                String[] second = rse.getString("intStoresSecond").split(", ");
                if ((first.length <= 0) && (rse.getString("intStoresFirst").length() > 0))
                {
                    this.dataStore.add(new Pair(Integer.parseInt(rse.getString("intStoresFirst")), Integer.parseInt(rse.getString("intStoresSecond"))));
                }
                for (int i = 0; i < first.length; i++)
                {
                    if ((first[i].length() > 0) && (second[i].length() > 0))
                    {
                        this.dataStore.add(new Pair(Integer.parseInt(first[i]), Integer.parseInt(second[i])));
                    }
                }
                break;

            case partyQuest_S:
            case dayByDay:
            case normalAutoStart:
            case subJobFlags:
            case fieldEnter:
            case pettamenessmin:
            case npc:
            case questComplete:
            case pop:
            case interval:
            case mbmin:
            case lvmax:
            case lvmin:
                this.intStore = Integer.parseInt(rse.getString("stringStore"));
                break;

            case end:
                this.stringStore = rse.getString("stringStore");
        }
    }

    public boolean check(MapleCharacter chr, Integer npcid)
    {
        int state;
        int count;
        int killReq;
        switch (this.type)
        {
            case job:
                for (Pair<Integer, Integer> a : this.dataStore)
                {
                    if ((a.getRight() == chr.getJob()) || (chr.isGM()))
                    {
                        return true;
                    }
                }
                return false;
            case skill:
                for (Pair<Integer, Integer> a : this.dataStore)
                {
                    boolean acquire = a.getRight() > 0;
                    int skill = a.getLeft();
                    Skill skil = SkillFactory.getSkill(skill);
                    if (acquire)
                    {
                        if (skil.isFourthJob())
                        {
                            if (chr.getMasterLevel(skil) == 0) return false;
                        }
                        else
                        {
                            if (skil.isGuildSkill())
                            {
                                return (chr.getGuild() != null) && (chr.getGuild().getLevel() >= 1) && (chr.getGuild().hasSkill(skill));
                            }

                            if (chr.getSkillLevel(skil) == 0)
                            {
                                return false;
                            }
                        }
                    }
                    else if ((chr.getSkillLevel(skil) > 0) || (chr.getMasterLevel(skil) > 0))
                    {
                        return false;
                    }
                }

                return true;

            case quest:
                for (Pair<Integer, Integer> a : this.dataStore)
                {
                    MapleQuestStatus q = chr.getQuest(MapleQuest.getInstance(a.getLeft()));
                    state = a.getRight();
                    if (state != 0)
                    {
                        if ((q != null) || (state != 0))
                        {

                            if ((q == null) || (q.getStatus() != state)) return false;
                        }
                    }
                }
                return true;


            case item:
                for (Pair<Integer, Integer> a : this.dataStore)
                {
                    int itemId = a.getLeft();
                    short quantity = 0;
                    MapleInventoryType iType = ItemConstants.getInventoryType(itemId);
                    for (Item item : chr.getInventory(iType).listById(itemId))
                    {
                        quantity = (short) (quantity + item.getQuantity());
                    }
                    count = a.getRight();
                    if ((quantity < count) || ((count <= 0) && (quantity > 0)))
                    {
                        return false;
                    }
                }
                return true;
            case lvmin:
                return chr.getLevel() >= this.intStore;
            case lvmax:
                return chr.getLevel() <= this.intStore;
            case end:
                String timeStr = this.stringStore;
                if ((timeStr == null) || (timeStr.length() <= 0))
                {
                    return true;
                }
                Calendar cal = Calendar.getInstance();
                cal.set(Integer.parseInt(timeStr.substring(0, 4)), Integer.parseInt(timeStr.substring(4, 6)), Integer.parseInt(timeStr.substring(6, 8)), Integer.parseInt(timeStr.substring(8, 10)), 0);
                return cal.getTimeInMillis() >= System.currentTimeMillis();
            case mob:
                for (Pair<Integer, Integer> a : this.dataStore)
                {
                    int mobId = a.getLeft();
                    killReq = a.getRight();
                    if (chr.getQuest(this.quest).getMobKills(mobId) < killReq)
                    {
                        return false;
                    }
                }
                return true;
            case npc:
                return (npcid == null) || (npcid == this.intStore);
            case fieldEnter:
                if (this.intStore > 0)
                {
                    return this.intStore == chr.getMapId();
                }
                return true;
            case mbmin:
                return chr.getMonsterBook().getSeen() >= this.intStore;
            case mbcard:
                for (Pair<Integer, Integer> a : this.dataStore)
                {
                    int cardId = a.getLeft();
                    killReq = a.getRight();
                    if (chr.getMonsterBook().getLevelByCard(cardId) < killReq)
                    {
                        return false;
                    }
                }
                return true;
            case pop:
                return chr.getFame() >= this.intStore;
            case questComplete:
                return chr.getNumQuest() >= this.intStore;
            case interval:
                return (chr.getQuest(this.quest).getStatus() != 2) || (chr.getQuest(this.quest).getCompletionTime() <= System.currentTimeMillis() - this.intStore * 60 * 1000L);
            case pet:
                for (Pair<Integer, Integer> a : this.dataStore)
                {
                    if (chr.getPetByItemId(a.getRight()) != -1)
                    {
                        return true;
                    }
                }
                return false;
            case pettamenessmin:
                MaplePet[] pet = chr.getSpawnPets();
                for (int i = 0; i < 3; i++)
                {
                    if ((pet[i] != null) && (pet[i].getSummoned()) && (pet[i].getCloseness() >= this.intStore))
                    {
                        return true;
                    }
                }
                return false;
            case partyQuest_S:
                int[] partyQuests = {1200, 1201, 1202, 1203, 1204, 1205, 1206, 1300, 1301, 1302};
                int sRankings = 0;
                for (int i : partyQuests)
                {
                    String rank = chr.getOneInfo(i, "rank");
                    if ((rank != null) && (rank.equals("S")))
                    {
                        sRankings++;
                    }
                }
                return sRankings >= 5;
            case subJobFlags:
                return chr.getSubcategory() == this.intStore / 2;
            case craftMin:
            case willMin:
            case charismaMin:
            case insightMin:
            case charmMin:
            case senseMin:
                return chr.getTrait(MapleTraitType.getByQuestName(this.type.name())).getLevel() >= this.intStore;
        }
        return true;
    }

    public MapleQuestRequirementType getType()
    {
        return this.type;
    }

    public String toString()
    {
        return this.type.toString();
    }

    public List<Pair<Integer, Integer>> getDataStore()
    {
        return this.dataStore;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\quest\MapleQuestRequirement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */