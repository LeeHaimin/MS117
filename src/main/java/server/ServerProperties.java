package server;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import database.DatabaseConnection;
import tools.PropertyTool;


public class ServerProperties
{
    private static final Properties server = new Properties();
    private static final Properties settings = new Properties();
    private static final Map<String, Boolean> blockedOpcodes = new HashMap<>();
    private static final boolean show = true;
    public static int maxHp;
    public static int maxMp;
    public static int maxLevel;
    public static int maxCygnusLevel;
    public static long maxMeso;
    private static PropertyTool propTool = new PropertyTool(new Properties());
    private static boolean showPacket;
    private static boolean blockDefault;

    static
    {
        String toLoad = "config/world.properties";
        loadProperties(toLoad);
        try
        {
            PreparedStatement ps = DatabaseConnection.getConnection().prepareStatement("SELECT * FROM auth_server_channel_ip");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                server.put(rs.getString("name") + rs.getInt("channelid"), rs.getString("value"));
            }
            rs.close();
            ps.close();
        }
        catch (SQLException ex)
        {
            System.exit(0);
        }
        maxHp = Integer.parseInt(getProperty("world.maxHp", "99999"));
        maxMp = Integer.parseInt(getProperty("world.maxMp", "99999"));
        maxMeso = Long.parseLong(getProperty("world.maxMeso", "2147483647"));
        maxLevel = Integer.parseInt(getProperty("world.maxLevel", "200"));
        maxCygnusLevel = Integer.parseInt(getProperty("world.maxCygnusLevel", "120"));
        if (isLoadShow())
        {
            loadSettings();
        }
    }

    public static void loadProperties(String name)
    {
        try
        {
            FileReader fr = new FileReader(name);
            server.load(fr);
            fr.close();
        }
        catch (IOException ex)
        {
            System.out.println("加载 " + name + " 配置出错 " + ex);
        }
    }

    public static void loadSettings()
    {
        try
        {
            FileInputStream fis = new FileInputStream("config/settings.properties");
            settings.load(fis);
            fis.close();
        }
        catch (IOException ex)
        {
            System.out.println("加载 settings.properties 配置出错" + ex);
        }
        propTool = new PropertyTool(settings);
        showPacket = propTool.getSettingInt("ShowPacket", 1) > 0;
        blockDefault = propTool.getSettingInt("BlockDefault", 0) > 0;
        blockedOpcodes.clear();
        for (Map.Entry<Object, Object> entry : settings.entrySet())
        {
            String property = (String) entry.getKey();
            if ((property.startsWith("S_")) || (property.startsWith("R_")))
            {
                blockedOpcodes.put(property, propTool.getSettingInt(property, 0) > 0);
            }
        }
    }

    public static boolean isLoadShow()
    {
        return show;
    }

    public static boolean ShowPacket()
    {
        return (showPacket) && (show);
    }

    public static boolean SendPacket(String op, String pHeaderStr)
    {
        if (op.equals("UNKNOWN"))
        {
            return blockedOpcodes.containsKey("S_" + pHeaderStr) ? blockedOpcodes.get("S_" + pHeaderStr) : blockDefault;
        }
        return blockedOpcodes.containsKey("S_" + op) ? blockedOpcodes.get("S_" + op) : blockDefault;
    }

    public static boolean RecvPacket(String op, String pHeaderStr)
    {
        if (op.equals("UNKNOWN"))
        {
            return blockedOpcodes.containsKey("R_" + pHeaderStr) ? blockedOpcodes.get("R_" + pHeaderStr) : blockDefault;
        }
        return blockedOpcodes.containsKey("R_" + op) ? blockedOpcodes.get("R_" + op) : blockDefault;
    }

    public static String getProperty(String name)
    {
        if (server.containsKey(name))
        {
            return server.getProperty(name);
        }
        System.out.println("Error finding the properties for: " + name + ".");
        return null;
    }

    public static void setProperty(String prop, String newInf)
    {
        server.setProperty(prop, newInf);
    }

    public static String getProperty(String name, String def)
    {
        return server.getProperty(name, def);
    }

    public static int getMaxHp()
    {
        if ((maxHp < 99999) || (maxHp > 500000))
        {
            maxHp = 99999;
        }
        return maxHp;
    }


    public static int getMaxMp()
    {
        if ((maxMp < 99999) || (maxMp > 500000))
        {
            maxMp = 99999;
        }
        return maxMp;
    }


    public static long getMaxMeso()
    {
        if (maxMeso < 2147483647L)
        {
            maxMeso = 2147483647L;
        }
        return maxMeso;
    }


    public static int getMaxLevel()
    {
        if ((maxLevel < 200) || (maxLevel > 250))
        {
            maxLevel = 200;
        }
        return maxLevel;
    }


    public static int getMaxCygnusLevel()
    {
        if ((maxCygnusLevel < 120) || (maxCygnusLevel > 250))
        {
            maxCygnusLevel = 120;
        }
        return maxCygnusLevel;
    }
}