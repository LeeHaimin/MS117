package server;

public class StructSetItemStat
{
    public int incSTR;
    public int incDEX;
    public int incINT;
    public int incLUK;
    public int incMHP;
    public int incMMP;
    public int incMHPr;
    public int incMMPr;
    public int incACC;
    public int incEVA;
    public int incPDD;
    public int incMDD;
    public int incPAD;
    public int incMAD;
    public int incJump;
    public int incSpeed;
    public int incAllStat;
    public int incPQEXPr;
    public int incPVPDamage;
    public int option1;
    public int option1Level;
    public int option2;
    public int option2Level;
    public int skillId;
    public int skillLevel;
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\StructSetItemStat.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */