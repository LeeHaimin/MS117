package server;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class StructSetItem
{
    public final Map<Integer, StructSetItemStat> setItemStat = new LinkedHashMap<>();
    public final List<Integer> itemIDs = new ArrayList<>();
    public int setItemID;
    public byte completeCount;
    public String setItemName;

    public Map<Integer, StructSetItemStat> getSetItemStats()
    {
        return new LinkedHashMap(this.setItemStat);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\StructSetItem.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */