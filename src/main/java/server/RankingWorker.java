package server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import tools.StringUtil;

public class RankingWorker
{
    private static final Map<Integer, List<RankingInformation>> rankings = new HashMap<>();
    private static final Map<String, Integer> jobCommands = new HashMap<>();
    private static final List<PokemonInformation> pokemon = new ArrayList<>();
    private static final List<PokedexInformation> pokemon_seen = new ArrayList<>();
    private static final List<PokebattleInformation> pokemon_ratio = new ArrayList<>();
    private static final List<Integer> itemSearch = new ArrayList<>();

    public static Integer getJobCommand(String job)
    {
        return jobCommands.get(job);
    }

    public static Map<String, Integer> getJobCommands()
    {
        return jobCommands;
    }

    public static List<RankingInformation> getRankingInfo(int job)
    {
        return rankings.get(job);
    }

    public static List<PokemonInformation> getPokemonInfo()
    {
        return pokemon;
    }

    public static List<PokedexInformation> getPokemonCaught()
    {
        return pokemon_seen;
    }

    public static List<PokebattleInformation> getPokemonRatio()
    {
        return pokemon_ratio;
    }

    public static List<Integer> getItemSearch()
    {
        return itemSearch;
    }

    public static void start()
    {
        System.out.println("系统自动更新玩家排名功能已启动...");
        System.out.println("更新间隔时间为: " + Start.instance.getRankTime() + " 分钟1次。");
        Timer.WorldTimer.getInstance().register(new Runnable()
        {
            public void run()
            {
                RankingWorker.jobCommands.clear();
                RankingWorker.rankings.clear();
                RankingWorker.pokemon.clear();
                RankingWorker.pokemon_seen.clear();
                RankingWorker.pokemon_ratio.clear();
                RankingWorker.itemSearch.clear();
                RankingWorker.updateRank();
            }
        }, 60000 * Start.instance.getRankTime());
    }

    public static void updateRank()
    {
        System.out.println("开始更新玩家排名...");
        long startTime = System.currentTimeMillis();
        loadJobCommands();
        Connection con = database.DatabaseConnection.getConnection();
        try
        {
            con.setAutoCommit(false);
            updateRanking(con);
            updatePokemon(con);
            updatePokemonRatio(con);
            updatePokemonCaught(con);
            updateItemSearch(con);
            con.commit();
            con.setAutoCommit(true);
        }
        catch (Exception ex)
        {
            try
            {
                con.rollback();
                con.setAutoCommit(true);
                tools.FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", ex);
                System.err.println("更新玩家排名出错");
            }
            catch (java.sql.SQLException ex2)
            {
                tools.FileoutputUtil.outputFileError("log\\Script\\Script_Except.log", ex2);
                System.err.println("Could not rollback unfinished ranking transaction");
            }
        }
        System.out.println("玩家排名更新完成 耗时: " + (System.currentTimeMillis() - startTime) / 1000L + " 秒..");
    }

    public static void loadJobCommands()
    {
        jobCommands.put("所有", -1);
        jobCommands.put("新手", 0);
        jobCommands.put("战士", 1);
        jobCommands.put("魔法师", 2);
        jobCommands.put("弓箭手", 3);
        jobCommands.put("飞侠", 4);
        jobCommands.put("海盗", 5);
        jobCommands.put("初心者", 10);
        jobCommands.put("魂骑士", 11);
        jobCommands.put("炎术士", 12);
        jobCommands.put("风灵使者", 13);
        jobCommands.put("夜行者", 14);
        jobCommands.put("奇袭者", 15);
        jobCommands.put("英雄", 20);
        jobCommands.put("战神", 21);
        jobCommands.put("龙神", 22);
        jobCommands.put("双弩精灵", 23);
        jobCommands.put("幻影神偷", 24);
        jobCommands.put("夜光法师", 27);
        jobCommands.put("反抗者", 30);
        jobCommands.put("恶魔猎手", 31);
        jobCommands.put("幻灵斗师", 32);
        jobCommands.put("弩豹游侠", 33);
        jobCommands.put("机械师", 35);
        jobCommands.put("米哈尔", 50);
    }

    private static void updateRanking(Connection con) throws Exception
    {

        String sb =
                "SELECT c.id, c.job, c.exp, c.level, c.name, c.jobRank, c.rank, c.fame" + " FROM characters AS c LEFT JOIN accounts AS a ON c.accountid = a.id WHERE c.gm = 0 AND a.banned = 0 " +
                        "AND c.level >= 160" + " ORDER BY c.level DESC , c.exp DESC , c.fame DESC , c.rank ASC";
        PreparedStatement charSelect = con.prepareStatement(sb);
        ResultSet rs = charSelect.executeQuery();
        PreparedStatement ps = con.prepareStatement("UPDATE characters SET jobRank = ?, jobRankMove = ?, rank = ?, rankMove = ? WHERE id = ?");
        int rank = 0;
        Map<Integer, Integer> rankMap = new LinkedHashMap<>();
        for (Integer integer : jobCommands.values())
        {
            int i = integer;
            rankMap.put(i, 0);
            rankings.put(i, new ArrayList());
        }
        while (rs.next())
        {
            int job = rs.getInt("job");
            if (rankMap.containsKey(job / 100))
            {

                int jobRank = rankMap.get(job / 100) + 1;
                rankMap.put(job / 100, jobRank);
                rank++;
                rankings.get(-1).add(new RankingInformation(rs.getString("name"), job, rs.getInt("level"), rs.getLong("exp"), rank, rs.getInt("fame")));
                rankings.get(job / 100).add(new RankingInformation(rs.getString("name"), job, rs.getInt("level"), rs.getLong("exp"), jobRank, rs.getInt("fame")));
                ps.setInt(1, jobRank);
                ps.setInt(2, rs.getInt("jobRank") - jobRank);
                ps.setInt(3, rank);
                ps.setInt(4, rs.getInt("rank") - rank);
                ps.setInt(5, rs.getInt("id"));
                ps.addBatch();
            }
        }
        ps.executeBatch();
        rs.close();
        charSelect.close();
        ps.close();
    }

    private static void updatePokemon(Connection con) throws Exception
    {

        String sb = "SELECT count(distinct m.id) AS mc, c.name, c.totalWins, c.totalLosses " + " FROM characters AS c LEFT JOIN accounts AS a ON c.accountid = a.id" + " RIGHT JOIN monsterbook AS m "
                + "ON m.charid = a.id WHERE c.gm = 0 AND a.banned = 0" + " ORDER BY c.totalWins DESC, c.totalLosses DESC, mc DESC LIMIT 50";
        PreparedStatement charSelect = con.prepareStatement(sb);
        ResultSet rs = charSelect.executeQuery();
        int rank = 0;
        while (rs.next())
        {
            rank++;
            pokemon.add(new PokemonInformation(rs.getString("name"), rs.getInt("totalWins"), rs.getInt("totalLosses"), rs.getInt("mc"), rank));
        }
        rs.close();
        charSelect.close();
    }

    private static void updatePokemonRatio(Connection con) throws Exception
    {

        String sb = "SELECT (c.totalWins / c.totalLosses) AS mc, c.name, c.totalWins, c.totalLosses " + " FROM characters AS c LEFT JOIN accounts AS a ON c.accountid = a.id" + " WHERE c.gm = 0 AND "
                + "a.banned = 0 AND c.totalWins > 10 AND c.totalLosses > 0" + " ORDER BY mc DESC, c.totalWins DESC, c.totalLosses ASC LIMIT 50";
        PreparedStatement charSelect = con.prepareStatement(sb);
        ResultSet rs = charSelect.executeQuery();
        int rank = 0;
        while (rs.next())
        {
            rank++;
            pokemon_ratio.add(new PokebattleInformation(rs.getString("name"), rs.getInt("totalWins"), rs.getInt("totalLosses"), rs.getDouble("mc"), rank));
        }
        rs.close();
        charSelect.close();
    }

    private static void updatePokemonCaught(Connection con) throws Exception
    {

        String sb = "SELECT count(distinct m.id) AS mc, c.name " + " FROM characters AS c LEFT JOIN accounts AS a ON c.accountid = a.id" + " RIGHT JOIN monsterbook AS m ON m.charid = a.id WHERE c" +
                ".gm = 0 AND a.banned = 0" + " ORDER BY mc DESC LIMIT 50";
        PreparedStatement charSelect = con.prepareStatement(sb);
        ResultSet rs = charSelect.executeQuery();
        int rank = 0;
        while (rs.next())
        {
            rank++;
            pokemon_seen.add(new PokedexInformation(rs.getString("name"), rs.getInt("mc"), rank));
        }
        rs.close();
        charSelect.close();
    }

    private static void updateItemSearch(Connection con) throws Exception
    {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        PreparedStatement ps = con.prepareStatement("SELECT itemid, count FROM itemsearch WHERE count > 0 ORDER BY count DESC LIMIT 10");
        ResultSet rs = ps.executeQuery();
        itemSearch.clear();
        while (rs.next())
        {
            int itemId = rs.getInt("itemid");


            if ((!itemSearch.contains(itemId)) && (ii.itemExists(itemId)))
            {

                itemSearch.add(itemId);
            }
        }
        rs.close();
        ps.close();
    }

    public static void printSection(String s)
    {
        s = "-[ " + s + " ]";
        while (s.getBytes().length < 79)
        {
            s = "=" + s;
        }
        System.out.println(s);
    }

    public static class RankingInformation
    {
        public final String toString;
        public final int rank;

        public RankingInformation(String name, int job, int level, long exp, int rank, int fame)
        {
            this.rank = rank;


            String builder =
                    "排名 " + StringUtil.getRightPaddedStr(String.valueOf(rank), ' ', 3) + " : " + StringUtil.getRightPaddedStr(name, ' ', 13) + " 等级: " + StringUtil.getRightPaddedStr(String.valueOf(level), ' ', 3) + " 职业: " + StringUtil.getRightPaddedStr(MapleCarnivalChallenge.getJobNameById(job), ' ', 10) + "\r\n";
            this.toString = builder;
        }

        public String toString()
        {
            return this.toString;
        }
    }

    public static class PokemonInformation
    {
        public final String toString;

        public PokemonInformation(String name, int totalWins, int totalLosses, int caught, int rank)
        {
            String builder = "排名 " + rank + " : #e" + name + "#n - #r胜利: " + totalWins + "#b 失败: " + totalLosses + "#k Caught:" + caught + "\r\n";
            this.toString = builder;
        }

        public String toString()
        {
            return this.toString;
        }
    }

    public static class PokedexInformation
    {
        public final String toString;

        public PokedexInformation(String name, int caught, int rank)
        {
            String builder = "排名 " + rank + " : #e" + name + "#n - #rCaught: " + caught + "\r\n";
            this.toString = builder;
        }

        public String toString()
        {
            return this.toString;
        }
    }

    public static class PokebattleInformation
    {
        public final String toString;

        public PokebattleInformation(String name, int totalWins, int totalLosses, double caught, int rank)
        {
            String builder = "Rank " + rank + " : #e" + name + "#n - #rRatio: " + caught + "\r\n";
            this.toString = builder;
        }

        public String toString()
        {
            return this.toString;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\RankingWorker.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */