package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class JumpDownMovement extends AbstractLifeMovement
{
    private java.awt.Point pixelsPerSecond;
    private java.awt.Point offset;
    private int unk;
    private int fh;

    public JumpDownMovement(int type, java.awt.Point position, int duration, int newstate)
    {
        super(type, position, duration, newstate);
    }

    public java.awt.Point getPixelsPerSecond()
    {
        return this.pixelsPerSecond;
    }

    public void setPixelsPerSecond(java.awt.Point wobble)
    {
        this.pixelsPerSecond = wobble;
    }

    public java.awt.Point getOffset()
    {
        return this.offset;
    }

    public void setOffset(java.awt.Point wobble)
    {
        this.offset = wobble;
    }

    public int getUnk()
    {
        return this.unk;
    }

    public void setUnk(int unk)
    {
        this.unk = unk;
    }

    public int getFH()
    {
        return this.fh;
    }

    public void setFH(int fh)
    {
        this.fh = fh;
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(getType());
        lew.writePos(getPosition());
        lew.writePos(this.pixelsPerSecond);
        lew.writeShort(this.unk);
        lew.writeShort(this.fh);
        lew.writePos(this.offset);
        lew.write(getNewstate());
        lew.writeShort(getDuration());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\JumpDownMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */