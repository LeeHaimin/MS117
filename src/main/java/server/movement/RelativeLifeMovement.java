package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class RelativeLifeMovement extends AbstractLifeMovement
{
    public RelativeLifeMovement(int type, java.awt.Point position, int duration, int newstate)
    {
        super(type, position, duration, newstate);
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(getType());
        lew.writeShort(getPosition().x);
        lew.writeShort(getPosition().y);
        lew.write(getNewstate());
        lew.writeShort(getDuration());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\RelativeLifeMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */