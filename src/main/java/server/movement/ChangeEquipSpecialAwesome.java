package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class ChangeEquipSpecialAwesome implements LifeMovementFragment
{
    private final int type;
    private final int wui;

    public ChangeEquipSpecialAwesome(int type, int wui)
    {
        this.type = type;
        this.wui = wui;
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(this.type);
        lew.write(this.wui);
    }

    public java.awt.Point getPosition()
    {
        return new java.awt.Point(0, 0);
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\ChangeEquipSpecialAwesome.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */