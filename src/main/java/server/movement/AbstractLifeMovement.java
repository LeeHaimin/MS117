package server.movement;

import java.awt.Point;

public abstract class AbstractLifeMovement implements LifeMovement
{
    private final Point position;
    private final int duration;
    private final int newstate;
    private final int type;

    public AbstractLifeMovement(int type, Point position, int duration, int newstate)
    {
        this.type = type;
        this.position = position;
        this.duration = duration;
        this.newstate = newstate;
    }

    public int getNewstate()
    {
        return this.newstate;
    }

    public int getDuration()
    {
        return this.duration;
    }

    public int getType()
    {
        return this.type;
    }

    public Point getPosition()
    {
        return this.position;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\AbstractLifeMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */