package server.movement;

public interface LifeMovement extends LifeMovementFragment
{
    int getNewstate();

    int getDuration();

    int getType();
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\LifeMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */