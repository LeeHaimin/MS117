package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class TeleportMovement extends AbsoluteLifeMovement
{
    public TeleportMovement(int type, java.awt.Point position, int newstate)
    {
        super(type, position, 0, newstate);
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(getType());
        lew.writeShort(getPosition().x);
        lew.writeShort(getPosition().y);
        lew.writeShort(getPixelsPerSecond().x);
        lew.writeShort(getPixelsPerSecond().y);
        lew.write(getNewstate());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\TeleportMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */