package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class UnknownMovement extends AbstractLifeMovement
{
    private java.awt.Point pixelsPerSecond;
    private int unk;
    private int fh;

    public UnknownMovement(int type, java.awt.Point position, int duration, int newstate)
    {
        super(type, position, duration, newstate);
    }

    public java.awt.Point getPixelsPerSecond()
    {
        return this.pixelsPerSecond;
    }

    public void setPixelsPerSecond(java.awt.Point wobble)
    {
        this.pixelsPerSecond = wobble;
    }

    public int getUnk()
    {
        return this.unk;
    }

    public void setUnk(int unk)
    {
        this.unk = unk;
    }

    public int getFH()
    {
        return this.fh;
    }

    public void setFH(int fh)
    {
        this.fh = fh;
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(getType());
        lew.writeShort(this.unk);
        lew.writeShort(getPosition().x);
        lew.writeShort(getPosition().y);
        lew.writeShort(this.pixelsPerSecond.x);
        lew.writeShort(this.pixelsPerSecond.y);
        lew.writeShort(this.fh);
        lew.write(getNewstate());
        lew.writeShort(getDuration());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\UnknownMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */