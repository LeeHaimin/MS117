package server.movement;

import java.awt.Point;

import tools.data.output.MaplePacketLittleEndianWriter;

public interface LifeMovementFragment
{
    void serialize(MaplePacketLittleEndianWriter paramMaplePacketLittleEndianWriter);

    Point getPosition();
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\LifeMovementFragment.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */