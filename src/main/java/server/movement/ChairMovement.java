package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class ChairMovement extends AbstractLifeMovement
{
    private int newfh;

    public ChairMovement(int type, java.awt.Point position, int duration, int newstate)
    {
        super(type, position, duration, newstate);
    }

    public int getNewFH()
    {
        return this.newfh;
    }

    public void setNewFH(int fh)
    {
        this.newfh = fh;
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(getType());
        lew.writeShort(getPosition().x);
        lew.writeShort(getPosition().y);
        lew.writeShort(this.newfh);
        lew.write(getNewstate());
        lew.writeShort(getDuration());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\ChairMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */