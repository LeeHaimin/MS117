package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class BounceMovement extends AbstractLifeMovement
{
    private int unk;
    private int fh;

    public BounceMovement(int type, java.awt.Point position, int duration, int newstate)
    {
        super(type, position, duration, newstate);
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(getType());
        lew.writePos(getPosition());
        lew.writeShort(getUnk());
        lew.writeShort(getFH());
        lew.write(getNewstate());
        lew.writeShort(getDuration());
    }

    public int getUnk()
    {
        return this.unk;
    }

    public void setUnk(int unk)
    {
        this.unk = unk;
    }

    public int getFH()
    {
        return this.fh;
    }

    public void setFH(int fh)
    {
        this.fh = fh;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\BounceMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */