package server.movement;

import tools.data.output.MaplePacketLittleEndianWriter;

public class AranMovement extends AbstractLifeMovement
{
    public AranMovement(int type, java.awt.Point position, int duration, int newstate)
    {
        super(type, position, duration, newstate);
    }

    public void serialize(MaplePacketLittleEndianWriter lew)
    {
        lew.write(getType());
        lew.write(getNewstate());
        lew.writeShort(getDuration());
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\movement\AranMovement.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */