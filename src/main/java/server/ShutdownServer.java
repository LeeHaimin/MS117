package server;

import org.apache.log4j.Logger;

import java.lang.management.ManagementFactory;
import java.sql.SQLException;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import database.DatabaseConnection;
import handling.Auction.AuctionServer;
import handling.cashshop.CashShopServer;
import handling.channel.ChannelServer;
import handling.login.LoginServer;
import handling.world.WorldAllianceService;
import handling.world.WorldBroadcastService;
import handling.world.WorldFamilyService;
import handling.world.WorldGuildService;
import tools.MaplePacketCreator;


public class ShutdownServer implements ShutdownServerMBean
{
    private static final Logger log = Logger.getLogger(ShutdownServer.class);
    public static ShutdownServer instance;
    public int mode = 0;

    public static void registerMBean()
    {
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        try
        {
            instance = new ShutdownServer();
            mBeanServer.registerMBean(instance, new ObjectName("server:type=ShutdownServer"));
        }
        catch (Exception e)
        {
            System.out.println("Error registering Shutdown MBean");
        }
    }

    public static ShutdownServer getInstance()
    {
        return instance;
    }

    public void shutdown()
    {
        run();
    }

    public void run()
    {
        ChannelServer cs;
        if (this.mode == 0)
        {
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(0, " 游戏服务器将关闭维护，请玩家安全下线..."));
            for (ChannelServer channelServer : ChannelServer.getAllInstances())
            {
                cs = channelServer;
                cs.setShutdown();
                cs.setServerMessage("游戏服务器将关闭维护，请玩家安全下线...");
                cs.closeAllMerchants();
            }
            WorldGuildService.getInstance().save();
            WorldAllianceService.getInstance().save();
            WorldFamilyService.getInstance().save();
            System.out.println("服务端关闭事件 1 已完成.");
            this.mode += 1;
        }
        else if (this.mode == 1)
        {
            this.mode += 1;
            System.out.println("服务端关闭事件 2 开始...");
            WorldBroadcastService.getInstance().broadcastMessage(MaplePacketCreator.serverNotice(0, " 游戏服务器将关闭维护，请玩家安全下线..."));
            Integer[] chs = ChannelServer.getAllInstance().toArray(new Integer[0]);
            for (int i : chs)
            {
                try
                {
                    ChannelServer ser = ChannelServer.getInstance(i);
                    synchronized (this)
                    {
                        ser.shutdown();
                    }
                }
                catch (Exception e)
                {
                    log.error("关闭服务端错误 - 3" + e);
                }
            }
            LoginServer.shutdown();
            CashShopServer.shutdown();
            AuctionServer.shutdown();
            System.out.println("正在关闭时钟线程...");
            Timer.WorldTimer.getInstance().stop();
            Timer.MapTimer.getInstance().stop();
            Timer.BuffTimer.getInstance().stop();
            Timer.CloneTimer.getInstance().stop();
            Timer.EventTimer.getInstance().stop();
            Timer.EtcTimer.getInstance().stop();
            Timer.PingTimer.getInstance().stop();
            System.out.println("正在关闭数据库连接...");
            try
            {
                DatabaseConnection.closeAll();
            }
            catch (SQLException e)
            {
                log.error("关闭数据库连接错误" + e);
            }
            System.out.println("服务端关闭事件 2 已完成.服务端关闭完成...");
            try
            {
                Thread.sleep(5000L);
            }
            catch (Exception ignored)
            {
            }

            System.exit(0);
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\ShutdownServer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */