package server.events;

import client.MapleCharacter;
import tools.MaplePacketCreator;

public class MapleSurvival extends MapleEvent
{
    protected final long time = 360000L;
    protected long timeStarted = 0L;
    protected java.util.concurrent.ScheduledFuture<?> olaSchedule;

    public MapleSurvival(int channel, MapleEventType type)
    {
        super(channel, type);
    }

    public void finished(MapleCharacter chr)
    {
        givePrize(chr);
        chr.finishAchievement(25);
    }

    public void onMapLoad(MapleCharacter chr)
    {
        super.onMapLoad(chr);
        if (isTimerStarted())
        {
            chr.getClient().getSession().write(MaplePacketCreator.getClock((int) (getTimeLeft() / 1000L)));
        }
    }

    public void startEvent()
    {
        unreset();
        super.reset();
        broadcast(MaplePacketCreator.getClock((int) (this.time / 1000L)));
        this.timeStarted = System.currentTimeMillis();

        this.olaSchedule = server.Timer.EventTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                for (int i = 0; i < MapleSurvival.this.type.mapids.length; i++)
                {
                    for (MapleCharacter chr : MapleSurvival.this.getMap(i).getCharactersThreadsafe())
                    {
                        MapleSurvival.this.warpBack(chr);
                    }
                    MapleSurvival.this.unreset();
                }
            }
        }, this.time);


        broadcast(MaplePacketCreator.serverNotice(0, "The portal has now opened. Press the up arrow key at the portal to enter."));
        broadcast(MaplePacketCreator.serverNotice(0, "Fall down once, and never get back up again! Get to the top without falling down!"));
    }

    public void resetSchedule()
    {
        this.timeStarted = 0L;
        if (this.olaSchedule != null)
        {
            this.olaSchedule.cancel(false);
        }
        this.olaSchedule = null;
    }

    public void reset()
    {
        super.reset();
        resetSchedule();
        getMap(0).getPortal("join00").setPortalState(false);
    }

    public void unreset()
    {
        super.unreset();
        resetSchedule();
        getMap(0).getPortal("join00").setPortalState(true);
    }

    public boolean isTimerStarted()
    {
        return this.timeStarted > 0L;
    }

    public long getTimeLeft()
    {
        return this.time - (System.currentTimeMillis() - this.timeStarted);
    }

    public long getTime()
    {
        return this.time;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\events\MapleSurvival.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */