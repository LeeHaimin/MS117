package server.events;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import database.DatabaseConnectionWZ;
import server.Randomizer;
import tools.Pair;

public class MapleOxQuizFactory
{
    private static final MapleOxQuizFactory instance = new MapleOxQuizFactory();
    private final Map<Pair<Integer, Integer>, MapleOxQuizEntry> questionCache = new HashMap<>();

    public MapleOxQuizFactory()
    {
        initialize();
    }

    private void initialize()
    {
        try
        {
            Connection con = DatabaseConnectionWZ.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM wz_oxdata");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                this.questionCache.put(new Pair(rs.getInt("questionset"), rs.getInt("questionid")), get(rs));
            }
            rs.close();
            ps.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private MapleOxQuizEntry get(ResultSet rs) throws SQLException
    {
        return new MapleOxQuizEntry(rs.getString("question"), rs.getString("display"), getAnswerByText(rs.getString("answer")), rs.getInt("questionset"), rs.getInt("questionid"));
    }

    private int getAnswerByText(String text)
    {
        if (text.equalsIgnoreCase("x")) return 0;
        if (text.equalsIgnoreCase("o"))
        {
            return 1;
        }
        return -1;
    }

    public static MapleOxQuizFactory getInstance()
    {
        return instance;
    }

    public Map.Entry<Pair<Integer, Integer>, MapleOxQuizEntry> grabRandomQuestion()
    {
        int size = this.questionCache.size();
        for (; ; )
        {
            for (Map.Entry<Pair<Integer, Integer>, MapleOxQuizEntry> oxquiz : this.questionCache.entrySet())
            {
                if (Randomizer.nextInt(size) == 0)
                {
                    return oxquiz;
                }
            }
        }
    }

    public static class MapleOxQuizEntry
    {
        private final String question;
        private final String answerText;
        private final int answer;
        private final int questionset;
        private final int questionid;

        public MapleOxQuizEntry(String question, String answerText, int answer, int questionset, int questionid)
        {
            this.question = question;
            this.answerText = answerText;
            this.answer = answer;
            this.questionset = questionset;
            this.questionid = questionid;
        }

        public String getQuestion()
        {
            return this.question;
        }

        public String getAnswerText()
        {
            return this.answerText;
        }

        public int getAnswer()
        {
            return this.answer;
        }

        public int getQuestionSet()
        {
            return this.questionset;
        }

        public int getQuestionId()
        {
            return this.questionid;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\events\MapleOxQuizFactory.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */