package server.shop;

import client.inventory.Item;


public class MapleShopItem
{
    private final short buyable;
    private final int itemId;
    private final int price;
    private final int reqItem;
    private final int reqItemQ;
    private final int period;
    private final int state;
    private final int rank;
    private final Item rebuy;

    public MapleShopItem(Item rebuy, int price, short buyable)
    {
        this.buyable = buyable;
        this.itemId = rebuy.getItemId();
        this.price = price;
        this.reqItem = 0;
        this.reqItemQ = 0;
        this.period = 0;
        this.state = 0;
        this.rank = 0;
        this.rebuy = rebuy;
    }


    public MapleShopItem(short buyable, int itemId, int price, int reqItem, int reqItemQ, int period, int state, int rank)
    {
        this.buyable = buyable;
        this.itemId = itemId;
        this.price = price;
        this.reqItem = reqItem;
        this.reqItemQ = reqItemQ;
        this.period = period;
        this.state = state;
        this.rank = rank;
        this.rebuy = null;
    }

    public short getBuyable()
    {
        return this.buyable;
    }

    public int getItemId()
    {
        return this.itemId;
    }

    public int getPrice()
    {
        return this.price;
    }

    public int getReqItem()
    {
        return this.reqItem;
    }

    public int getReqItemQ()
    {
        return this.reqItemQ;
    }

    public int getRank()
    {
        return this.rank;
    }

    public int getPeriod()
    {
        return this.period;
    }

    public int getState()
    {
        return this.state;
    }

    public Item getRebuy()
    {
        return this.rebuy;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shop\MapleShopItem.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */