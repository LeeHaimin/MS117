package server.shop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import client.MapleClient;
import client.inventory.Item;
import client.inventory.MapleInventoryIdentifier;
import client.inventory.MapleInventoryType;
import constants.ItemConstants;
import server.AutobanManager;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import tools.FileoutputUtil;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.packet.NPCPacket;

public class MapleShop
{
    private static final Set<Integer> blockedItems = new LinkedHashSet<>();
    private static final Set<Integer> rechargeableItems = new LinkedHashSet<>();

    static
    {
        rechargeableItems.add(2070000);
        rechargeableItems.add(2070001);
        rechargeableItems.add(2070002);
        rechargeableItems.add(2070003);
        rechargeableItems.add(2070004);
        rechargeableItems.add(2070005);
        rechargeableItems.add(2070006);
        rechargeableItems.add(2070007);
        rechargeableItems.add(2070008);
        rechargeableItems.add(2070009);
        rechargeableItems.add(2070010);
        rechargeableItems.add(2070011);
        rechargeableItems.add(2070012);
        rechargeableItems.add(2070013);
        rechargeableItems.add(2070015);
        rechargeableItems.add(2070016);
        rechargeableItems.add(2070017);
        rechargeableItems.add(2070019);
        rechargeableItems.add(2070020);
        rechargeableItems.add(2070021);
        rechargeableItems.add(2070023);
        rechargeableItems.add(2070024);

        rechargeableItems.add(2330000);
        rechargeableItems.add(2330001);
        rechargeableItems.add(2330002);
        rechargeableItems.add(2330003);
        rechargeableItems.add(2330004);
        rechargeableItems.add(2330005);
        rechargeableItems.add(2330006);
        rechargeableItems.add(2330007);
        rechargeableItems.add(2330008);

        blockedItems.add(4170023);
        blockedItems.add(4170024);
        blockedItems.add(4170025);
        blockedItems.add(4170028);
        blockedItems.add(4170029);
        blockedItems.add(4170031);
        blockedItems.add(4170032);
        blockedItems.add(4170033);
    }

    private final List<Pair<Integer, String>> ranks = new ArrayList<>();
    private int id;
    private int npcId;
    private int shopItemId;
    private List<MapleShopItem> items;


    private MapleShop(int id, int npcId)
    {
        this.id = id;
        this.npcId = npcId;
        this.shopItemId = 0;
        this.items = new ArrayList<>();
    }

    public static MapleShop createFromDB(int id, boolean isShopId)
    {
        MapleShop ret = null;

        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        try
        {
            Connection con = database.DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement(isShopId ? "SELECT * FROM shops WHERE shopid = ?" : "SELECT * FROM shops WHERE npcid = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                int shopId = rs.getInt("shopid");
                ret = new MapleShop(shopId, rs.getInt("npcid"));
                rs.close();
                ps.close();
            }
            else
            {
                rs.close();
                ps.close();
                return null;
            }
            int shopId = 0;
            ps = con.prepareStatement("SELECT * FROM shopitems WHERE shopid = ? ORDER BY position ASC");
            ps.setInt(1, shopId);
            rs = ps.executeQuery();
            List<Integer> recharges = new ArrayList(rechargeableItems);
            while (rs.next()) if ((ii.itemExists(rs.getInt("itemid"))) && (!blockedItems.contains(rs.getInt("itemid"))))
            {

                if ((ItemConstants.is飞镖道具(rs.getInt("itemid"))) || (ItemConstants.is子弹道具(rs.getInt("itemid"))))
                {
                    MapleShopItem starItem = new MapleShopItem((short) 1, rs.getInt("itemid"), rs.getInt("price"), rs.getInt("reqitem"), rs.getInt("reqitemq"), rs.getInt("period"), rs.getInt("state"
                    ), rs.getInt("rank"));
                    ret.addItem(starItem);
                    if (rechargeableItems.contains(starItem.getItemId()))
                    {
                        recharges.remove(Integer.valueOf(starItem.getItemId()));
                    }
                }
                else
                {
                    ret.addItem(new MapleShopItem((short) 1000, rs.getInt("itemid"), rs.getInt("price"), rs.getInt("reqitem"), rs.getInt("reqitemq"), rs.getInt("period"), rs.getInt("state"),
                            rs.getInt("rank")));
                }
            }
            Integer localInteger;
            for (Iterator<Integer> starItem = recharges.iterator(); starItem.hasNext(); localInteger = starItem.next())
            {
            }


            rs.close();
            ps.close();

            ps = con.prepareStatement("SELECT * FROM shopranks WHERE shopid = ? ORDER BY rank ASC");
            ps.setInt(1, shopId);
            rs = ps.executeQuery();
            while (rs.next())
            {
                if (ii.itemExists(rs.getInt("itemid")))
                {

                    ret.ranks.add(new Pair(rs.getInt("itemid"), rs.getString("name")));
                }
            }
            rs.close();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("Could not load shop");
        }
        return ret;
    }

    public void addItem(MapleShopItem item)
    {
        this.items.add(item);
    }

    public void sendShop(MapleClient c)
    {
        c.getPlayer().setShop(this);
        c.getSession().write(NPCPacket.getNPCShop(getNpcId(), this, c));
    }

    public int getNpcId()
    {
        return this.npcId;
    }

    public void sendShop(MapleClient c, int customNpc)
    {
        c.getPlayer().setShop(this);
        c.getSession().write(NPCPacket.getNPCShop(customNpc, this, c));
    }

    public void sendItemShop(MapleClient c, int itemId)
    {
        this.shopItemId = itemId;
        c.getPlayer().setShop(this);
        c.getSession().write(NPCPacket.getNPCShop(getNpcId(), this, c));
    }

    public void buy(MapleClient c, int itemId, short quantity, short position)
    {
        if ((c.getPlayer() == null) || (c.getPlayer().getMap() == null))
        {
            return;
        }
        if (quantity <= 0)
        {
            AutobanManager.getInstance().addPoints(c, 1000, 0L, "购买道具数量: " + quantity + " 道具: " + itemId);
            return;
        }
        if (itemId == 4000463)
        {
            AutobanManager.getInstance().addPoints(c, 1000, 0L, "商店非法购买道具: " + itemId + " 数量: " + quantity);
            return;
        }
        if ((itemId / 10000 == 190) && (!constants.GameConstants.isMountItemAvailable(itemId, c.getPlayer().getJob())))
        {
            c.getPlayer().dropMessage(1, "您无法够买这个道具。");
            c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.购买道具完成, this, c, -1));
            return;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        MapleShopItem shopItem = findBySlotAndId(c, itemId, position);

        if ((shopItem != null) && (position >= this.items.size()))
        {
            if (c.getPlayer().getRebuy().isEmpty())
            {
                c.getSession().write(MaplePacketCreator.enableActions());
                return;
            }
            int index = position - this.items.size();

            if (shopItem.getRebuy() != null)
            {
                int price = (int) Math.max(Math.ceil(shopItem.getPrice() * (ItemConstants.isRechargable(itemId) ? 1 : shopItem.getBuyable())), 0.0D);
                if ((price >= 0) && (c.getPlayer().getMeso() >= price))
                {
                    if (MapleInventoryManipulator.checkSpace(c, itemId, quantity, ""))
                    {
                        c.getPlayer().gainMeso(-price, false);
                        MapleInventoryManipulator.addbyItem(c, shopItem.getRebuy());
                        c.getPlayer().getRebuy().remove(index);
                        c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.购买道具完成, this, c, index));
                    }
                    else
                    {
                        c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.背包空间不够, this, c, -1));
                    }
                }
                else
                {
                    c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.购买道具完成, this, c, -1));
                }
            }
            else
            {
                c.getPlayer().dropMessage(1, "购买回购栏道具出现错误.");
                c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.购买道具完成, this, c, -1));
            }
        }
        else if ((shopItem != null) && (shopItem.getPrice() > 0) && (shopItem.getReqItem() == 0))
        {
            if (shopItem.getRank() >= 0)
            {
                boolean passed = true;
                int y = 0;
                for (Pair<Integer, String> i : getRanks())
                {
                    if ((c.getPlayer().haveItem(i.left, 1, true, true)) && (shopItem.getRank() >= y))
                    {
                        passed = true;
                        break;
                    }
                    y++;
                }
                if (!passed)
                {
                    c.getPlayer().dropMessage(1, "You need a higher rank.");
                    c.getSession().write(MaplePacketCreator.enableActions());
                    return;
                }
            }
            int price = ItemConstants.isRechargable(itemId) ? shopItem.getPrice() : shopItem.getPrice() * quantity;
            if ((price >= 0) && (c.getPlayer().getMeso() >= price))
            {
                if (MapleInventoryManipulator.checkSpace(c, itemId, quantity, ""))
                {
                    c.getPlayer().gainMeso(-price, false);
                    if (ItemConstants.isPet(itemId))
                    {
                        MapleInventoryManipulator.addById(c, itemId, quantity, "", client.inventory.MaplePet.createPet(itemId, MapleInventoryIdentifier.getInstance()), -1L,
                                "Bought from shop " + this.id + ", " + this.npcId + " on " + FileoutputUtil.CurrentReadable_Date());
                    }
                    else if (!ItemConstants.isRechargable(itemId))
                    {
                        int state = shopItem.getState();
                        long period = shopItem.getPeriod();
                        MapleInventoryManipulator.addById(c, itemId, quantity, period, state, "商店购买 " + this.id + ", " + this.npcId + " 时间 " + FileoutputUtil.CurrentReadable_Date());
                    }
                    else
                    {
                        quantity = ii.getSlotMax(shopItem.getItemId());
                        MapleInventoryManipulator.addById(c, itemId, quantity, "商店购买 " + this.id + ", " + this.npcId + " 时间 " + FileoutputUtil.CurrentReadable_Date());
                    }
                }
                else
                {
                    c.getPlayer().dropMessage(1, "您的背包是满的，请整理下背包。");
                }
                c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.购买道具完成, this, c, -1));
            }
        }
        else if ((shopItem != null) && (shopItem.getReqItem() > 0) && (shopItem.getReqItemQ() > 0) && (c.getPlayer().haveItem(shopItem.getReqItem(), shopItem.getReqItemQ() * quantity, false, true)))
        {
            if (MapleInventoryManipulator.checkSpace(c, itemId, quantity, ""))
            {
                MapleInventoryManipulator.removeById(c, ItemConstants.getInventoryType(shopItem.getReqItem()), shopItem.getReqItem(), shopItem.getReqItemQ() * quantity, false, false);
                if (ItemConstants.isPet(itemId))
                {
                    MapleInventoryManipulator.addById(c, itemId, quantity, "", client.inventory.MaplePet.createPet(itemId, MapleInventoryIdentifier.getInstance()), -1L,
                            "商店购买 " + this.id + ", " + this.npcId + " 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                else if (!ItemConstants.isRechargable(itemId))
                {
                    int state = shopItem.getState();
                    long period = shopItem.getPeriod();
                    MapleInventoryManipulator.addById(c, itemId, quantity, period, state, "商店购买 " + this.id + ", " + this.npcId + " 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
                else
                {
                    quantity = ii.getSlotMax(shopItem.getItemId());
                    MapleInventoryManipulator.addById(c, itemId, quantity, "商店购买 " + this.id + ", " + this.npcId + " 时间 " + FileoutputUtil.CurrentReadable_Date());
                }
            }
            else
            {
                c.getPlayer().dropMessage(1, "您的背包是满的，请整理下背包。");
            }
            c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.购买道具完成, this, c, -1));
        }
    }

    protected MapleShopItem findBySlotAndId(MapleClient c, int itemId, int pos)
    {
        MapleShopItem shopItem = getItems(c).get(pos);

        if ((shopItem != null) && (shopItem.getItemId() == itemId))
        {
            return shopItem;
        }
        return null;
    }

    public List<Pair<Integer, String>> getRanks()
    {
        return this.ranks;
    }

    public List<MapleShopItem> getItems(MapleClient c)
    {
        List<MapleShopItem> itemsPlusRebuy = new ArrayList(this.items);
        itemsPlusRebuy.addAll(c.getPlayer().getRebuy());
        return itemsPlusRebuy;
    }

    public void sell(MapleClient c, MapleInventoryType type, byte slot, short quantity)
    {
        if ((quantity == 65535) || (quantity == 0))
        {
            quantity = 1;
        }
        Item item = c.getPlayer().getInventory(type).getItem(slot);
        if (item == null)
        {
            return;
        }
        if ((ItemConstants.is飞镖道具(item.getItemId())) || (ItemConstants.is子弹道具(item.getItemId())))
        {
            quantity = item.getQuantity();
        }
        if (item.getItemId() == 4000463)
        {
            c.getPlayer().dropMessage(1, "该道具无法卖出.");
            c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.卖出道具完成, this, c, -1));
            return;
        }
        if (quantity < 0)
        {
            AutobanManager.getInstance().addPoints(c, 1000, 0L, "卖出道具 " + quantity + " " + item.getItemId() + " (" + type.name() + "/" + slot + ")");
            return;
        }
        short iQuant = item.getQuantity();
        if (iQuant == 65535)
        {
            iQuant = 1;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        if ((ii.cantSell(item.getItemId())) || (ItemConstants.isPet(item.getItemId())))
        {
            return;
        }
        if ((quantity <= iQuant) && (iQuant > 0))
        {
            if (c.getPlayer().isAdmin())
            {
                if (item.getQuantity() == quantity)
                {
                    c.getPlayer().getRebuy().add(new MapleShopItem(item.copy(), (int) ii.getPrice(item.getItemId()), item.getQuantity()));
                }
                else
                {
                    c.getPlayer().getRebuy().add(new MapleShopItem(item.copyWithQuantity(quantity), (int) ii.getPrice(item.getItemId()), quantity));
                }
            }
            MapleInventoryManipulator.removeFromSlot(c, type, slot, quantity, false);
            double price;
            if ((ItemConstants.is飞镖道具(item.getItemId())) || (ItemConstants.is子弹道具(item.getItemId())))
            {
                price = ii.getWholePrice(item.getItemId()) / ii.getSlotMax(item.getItemId());
            }
            else
            {
                price = ii.getPrice(item.getItemId());
            }
            int recvMesos = (int) Math.max(Math.ceil(price * quantity), 0.0D);
            if ((price != -1.0D) && (recvMesos > 0))
            {
                c.getPlayer().gainMeso(recvMesos, false);
            }
            c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.卖出道具完成, this, c, -1));
        }
    }

    public void recharge(MapleClient c, byte slot)
    {
        Item item = c.getPlayer().getInventory(MapleInventoryType.USE).getItem(slot);
        if ((item == null) || ((!ItemConstants.is飞镖道具(item.getItemId())) && (!ItemConstants.is子弹道具(item.getItemId()))))
        {
            return;
        }
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        short slotMax = ii.getSlotMax(item.getItemId());
        int skill = constants.GameConstants.getMasterySkill(c.getPlayer().getJob());
        if (skill != 0)
        {
            slotMax = (short) (slotMax + c.getPlayer().getTotalSkillLevel(client.SkillFactory.getSkill(skill)) * 10);
        }
        if (item.getQuantity() < slotMax)
        {
            int price = (int) Math.round(ii.getPrice(item.getItemId()) * (slotMax - item.getQuantity()));
            if (c.getPlayer().getMeso() >= price)
            {
                item.setQuantity(slotMax);
                c.getSession().write(tools.packet.InventoryPacket.modifyInventory(false, java.util.Collections.singletonList(new client.inventory.ModifyInventory(1, item))));
                c.getPlayer().gainMeso(-price, false, false);
                c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.充值飞镖完成, this, c, -1));
            }
            else
            {
                c.getSession().write(NPCPacket.confirmShopTransaction(MapleShopResponse.充值金币不够, this, c, -1));
            }
        }
    }

    public int getId()
    {
        return this.id;
    }

    public int getShopItemId()
    {
        return this.shopItemId;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shop\MapleShop.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */