package server.shop;


public enum MapleShopResponse
{
    购买道具完成(0), 背包空间不够(5), 卖出道具完成(6), 充值飞镖完成(12), 充值金币不够(14), 购买回购出错(27);

    private final int value;

    MapleShopResponse(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shop\MapleShopResponse.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */