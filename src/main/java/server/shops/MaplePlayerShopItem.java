package server.shops;

import client.inventory.Item;

public class MaplePlayerShopItem
{
    public final Item item;
    public final int price;
    public short bundles;

    public MaplePlayerShopItem(Item item, short bundles, int price)
    {
        this.item = item;
        this.bundles = bundles;
        this.price = price;
    }

    public Item getItem()
    {
        return this.item;
    }

    public short getBundles()
    {
        return this.bundles;
    }

    public int getPrice()
    {
        return this.price;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shops\MaplePlayerShopItem.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */