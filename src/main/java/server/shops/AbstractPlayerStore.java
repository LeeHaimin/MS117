package server.shops;

import org.apache.log4j.Logger;

import java.lang.ref.WeakReference;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.ItemLoader;
import client.inventory.MapleInventoryType;
import constants.ItemConstants;
import handling.channel.ChannelServer;
import handling.world.WorldFindService;
import server.maps.MapleMap;
import server.maps.MapleMapObjectType;
import tools.Pair;
import tools.packet.PlayerShopPacket;

public abstract class AbstractPlayerStore extends server.maps.MapleMapObject implements IMaplePlayerShop
{
    private static final Logger log = Logger.getLogger(AbstractPlayerStore.class);
    protected final String ownerName;
    protected final String pass;
    protected final int ownerId;
    protected final int owneraccount;
    protected final int itemId;
    protected final int channel;
    protected final int map;
    protected final AtomicInteger meso = new AtomicInteger(0);
    protected final WeakReference<MapleCharacter>[] chrs;
    protected final Map<String, VisitorInfo> visitorsList = new HashMap<>();
    protected final List<BoughtItem> bought = new LinkedList<>();
    protected final List<MaplePlayerShopItem> items = new LinkedList<>();
    private final List<Pair<String, Byte>> messages = new LinkedList<>();
    protected boolean open = false;
    protected boolean available = false;
    protected String des;

    public AbstractPlayerStore(MapleCharacter owner, int itemId, String desc, String pass, int slots)
    {
        setPosition(owner.getTruePosition());
        this.ownerName = owner.getName();
        this.ownerId = owner.getId();
        this.owneraccount = owner.getAccountID();
        this.itemId = itemId;
        this.des = desc;
        this.pass = pass;
        this.map = owner.getMapId();
        this.channel = owner.getClient().getChannel();
        this.chrs = new WeakReference[slots];
        for (int i = 0; i < this.chrs.length; i++)
        {
            this.chrs[i] = new WeakReference(null);
        }
        this.visitorsList.clear();
    }

    public void broadcastToVisitors(byte[] packet, boolean owner)
    {
        for (WeakReference<MapleCharacter> chr : this.chrs)
        {
            if ((chr != null) && (chr.get() != null))
            {
                chr.get().getClient().getSession().write(packet);
            }
        }
        if ((getShopType() != 1) && (owner) && (getMCOwner() != null))
        {
            getMCOwner().getClient().getSession().write(packet);
        }
    }

    public void broadcastToVisitors(byte[] packet, int exception)
    {
        for (WeakReference<MapleCharacter> chr : this.chrs)
        {
            if ((chr != null) && (chr.get() != null) && (getVisitorSlot(chr.get()) != exception))
            {
                chr.get().getClient().getSession().write(packet);
            }
        }
        if ((getShopType() != 1) && (getMCOwner() != null) && (exception != this.ownerId))
        {
            getMCOwner().getClient().getSession().write(packet);
        }
    }

    public MapleCharacter getVisitor(int num)
    {
        return this.chrs[num].get();
    }

    public boolean isInVisitorsList(String visitorName)
    {
        return this.visitorsList.containsKey(visitorName);
    }

    public void updateVisitorsList(MapleCharacter visitor, boolean leave)
    {
        if ((visitor != null) && (!isOwner(visitor)) && (!visitor.isGM()))
        {
            if (this.visitorsList.containsKey(visitor.getName()))
            {
                if (leave)
                {
                    this.visitorsList.get(visitor.getName()).updateInTime();
                }
                else
                {
                    this.visitorsList.get(visitor.getName()).updateStartTime();
                }
            }
            else
            {
                this.visitorsList.put(visitor.getName(), new VisitorInfo());
            }
        }
    }

    public void removeVisitorsList(String visitorName)
    {
        this.visitorsList.remove(visitorName);
    }

    public String getOwnerName()
    {
        return this.ownerName;
    }

    public String getDescription()
    {
        if (this.des == null)
        {
            return "";
        }
        return this.des;
    }

    public void setDescription(String desc)
    {
        if (this.des.equalsIgnoreCase(desc))
        {
            return;
        }
        this.des = desc;
        if ((isAvailable()) && (getShopType() == 1))
        {
            getMap().broadcastMessage(PlayerShopPacket.updateHiredMerchant((HiredMerchant) this, false));
        }
    }

    public List<Pair<Byte, MapleCharacter>> getVisitors()
    {
        List<Pair<Byte, MapleCharacter>> chrz = new LinkedList<>();
        for (byte i = 0; i < this.chrs.length; i = (byte) (i + 1))
        {
            if ((this.chrs[i] != null) && (this.chrs[i].get() != null))
            {
                chrz.add(new Pair((byte) (i + 1), this.chrs[i].get()));
            }
        }
        return chrz;
    }

    public List<MaplePlayerShopItem> getItems()
    {
        return this.items;
    }

    public boolean isOpen()
    {
        return this.open;
    }

    public void setOpen(boolean open)
    {
        this.open = open;
    }

    public boolean saveItems()
    {
        if (getShopType() != 1)
        {
            return false;
        }
        Connection con = database.DatabaseConnection.getConnection();
        try
        {
            PreparedStatement ps = con.prepareStatement("DELETE FROM hiredmerch WHERE characterid = ?");
            ps.setInt(1, this.ownerId);
            ps.executeUpdate();
            ps.close();
            ps = con.prepareStatement("INSERT INTO hiredmerch (characterid, accountid, Mesos, map, channel, time) VALUES (?, ?, ?, ?, ?, ?)", 1);
            ps.setInt(1, this.ownerId);
            ps.setInt(2, this.owneraccount);
            ps.setInt(3, this.meso.get());
            ps.setInt(4, this.map);
            ps.setInt(5, this.channel);
            ps.setLong(6, System.currentTimeMillis());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (!rs.next())
            {
                rs.close();
                ps.close();
                log.info("[SaveItems] 保存雇佣商店信息出错 - 1");
                throw new RuntimeException("保存雇佣商店信息出错.");
            }
            rs.close();
            ps.close();

            List<Pair<Item, MapleInventoryType>> itemsWithType = new ArrayList<>();

            for (MaplePlayerShopItem pItems : this.items)
                if ((pItems.item != null) && (pItems.bundles > 0) && (


                        (pItems.item.getQuantity() > 0) || (ItemConstants.isRechargable(pItems.item.getItemId()))))
                {

                    Item item = pItems.item.copy();
                    item.setQuantity((short) (item.getQuantity() * pItems.bundles));
                    itemsWithType.add(new Pair(item, ItemConstants.getInventoryType(item.getItemId())));
                }
            ItemLoader.雇佣道具.saveItems(itemsWithType, this.ownerId);
            return true;
        }
        catch (SQLException se)
        {
            log.info("[SaveItems] 保存雇佣商店信息出错 - 2 " + se);
        }
        return false;
    }

    public boolean removeItem(int item)
    {
        return false;
    }

    public boolean isOwner(MapleCharacter chr)
    {
        return (chr.getId() == this.ownerId) && (chr.getName().equals(this.ownerName));
    }

    public byte getVisitorSlot(MapleCharacter visitor)
    {
        for (byte i = 0; i < this.chrs.length; i = (byte) (i + 1))
        {
            if ((this.chrs[i] != null) && (this.chrs[i].get() != null) && (this.chrs[i].get().getId() == visitor.getId()))
            {
                return (byte) (i + 1);
            }
        }
        if (visitor.getId() == this.ownerId)
        {
            return 0;
        }
        return -1;
    }

    public byte getFreeSlot()
    {
        for (byte i = 0; i < this.chrs.length; i = (byte) (i + 1))
        {
            if ((this.chrs[i] == null) || (this.chrs[i].get() == null))
            {
                return (byte) (i + 1);
            }
        }
        return -1;
    }

    public int getItemId()
    {
        return this.itemId;
    }

    public int getMeso()
    {
        return this.meso.get();
    }

    public void setMeso(int meso)
    {
        this.meso.set(meso);
    }

    public int getOwnerId()
    {
        return this.ownerId;
    }

    public int getOwnerAccId()
    {
        return this.owneraccount;
    }

    public void addItem(MaplePlayerShopItem item)
    {
        this.items.add(item);
    }

    public void removeFromSlot(int slot)
    {
        this.items.remove(slot);
    }

    public void broadcastToVisitors(byte[] packet)
    {
        broadcastToVisitors(packet, true);
    }

    public void addVisitor(MapleCharacter visitor)
    {
        int i = getFreeSlot();
        if (i > 0)
        {
            if (getShopType() >= 3)
            {
                broadcastToVisitors(PlayerShopPacket.getMiniGameNewVisitor(visitor, i, (MapleMiniGame) this));
            }
            else
            {
                broadcastToVisitors(PlayerShopPacket.shopVisitorAdd(visitor, i));
            }
            this.chrs[(i - 1)] = new WeakReference(visitor);
            updateVisitorsList(visitor, false);
            if (i == 6)
            {
                update();
            }
        }
    }

    public void removeVisitor(MapleCharacter visitor)
    {
        byte slot = getVisitorSlot(visitor);
        boolean shouldUpdate = getFreeSlot() == -1;
        if (slot > 0)
        {
            broadcastToVisitors(PlayerShopPacket.shopVisitorLeave(slot), slot);
            this.chrs[(slot - 1)] = new WeakReference(null);
            if (shouldUpdate)
            {
                update();
            }
            updateVisitorsList(visitor, true);
        }
    }

    public void removeAllVisitors(int error, int type)
    {
        for (int i = 0; i < this.chrs.length; i++)
        {
            MapleCharacter visitor = getVisitor(i);
            if (visitor != null)
            {
                if (type != -1)
                {
                    visitor.getClient().getSession().write(PlayerShopPacket.shopErrorMessage(error, i + 1));
                }
                broadcastToVisitors(PlayerShopPacket.shopVisitorLeave(getVisitorSlot(visitor)), getVisitorSlot(visitor));
                visitor.setPlayerShop(null);
                this.chrs[i] = new WeakReference(null);
                updateVisitorsList(visitor, true);
            }
        }
        update();
    }

    public String getPassword()
    {
        if (this.pass == null)
        {
            return "";
        }
        return this.pass;
    }

    public int getMaxSize()
    {
        return this.chrs.length + 1;
    }

    public int getSize()
    {
        return getFreeSlot() == -1 ? getMaxSize() : getFreeSlot();
    }

    public int getGameType()
    {
        if (getShopType() == 1) return 6;
        if (getShopType() == 2) return 5;
        if (getShopType() == 3) return 1;
        if (getShopType() == 4)
        {
            return 2;
        }
        return 0;
    }

    public void update()
    {
        if (isAvailable())
        {
            if (getShopType() == 1)
            {
                getMap().broadcastMessage(PlayerShopPacket.updateHiredMerchant((HiredMerchant) this));
            }
            else if (getMCOwner() != null)
            {
                getMap().broadcastMessage(PlayerShopPacket.sendPlayerShopBox(getMCOwner()));
            }
        }
    }

    public boolean isAvailable()
    {
        return this.available;
    }

    public void setAvailable(boolean b)
    {
        this.available = b;
    }

    public List<BoughtItem> getBoughtItems()
    {
        return this.bought;
    }

    public List<Pair<String, Byte>> getMessages()
    {
        return this.messages;
    }

    public int getMapId()
    {
        return this.map;
    }

    public int getChannel()
    {
        return this.channel;
    }

    public MapleMap getMap()
    {
        return ChannelServer.getInstance(this.channel).getMapFactory().getMap(this.map);
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.SHOP;
    }

    public void sendSpawnData(MapleClient client)
    {
    }

    public void sendDestroyData(MapleClient client)
    {
    }

    public MapleCharacter getMCOwnerWorld()
    {
        int ourChannel = WorldFindService.getInstance().findChannel(this.ownerId);
        if (ourChannel <= 0)
        {
            return null;
        }
        return ChannelServer.getInstance(ourChannel).getPlayerStorage().getCharacterById(this.ownerId);
    }

    public MapleCharacter getMCOwnerChannel()
    {
        return ChannelServer.getInstance(this.channel).getPlayerStorage().getCharacterById(this.ownerId);
    }

    public MapleCharacter getMCOwner()
    {
        return getMap().getCharacterById(this.ownerId);
    }

    public static final class BoughtItem
    {
        public final int id;
        public final int quantity;
        public final int totalPrice;
        public final String buyer;

        public BoughtItem(int id, int quantity, int totalPrice, String buyer)
        {
            this.id = id;
            this.quantity = quantity;
            this.totalPrice = totalPrice;
            this.buyer = buyer;
        }
    }

    public static final class VisitorInfo
    {
        public int inTime;
        public long startTime;

        public VisitorInfo()
        {
            this.inTime = 0;
            this.startTime = System.currentTimeMillis();
        }

        public void updateInTime()
        {
            int time = (int) (System.currentTimeMillis() - this.startTime);
            if (time > 0)
            {
                this.inTime += time;
            }
        }

        public int getInTime()
        {
            return this.inTime;
        }

        public void updateStartTime()
        {
            this.startTime = System.currentTimeMillis();
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shops\AbstractPlayerStore.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */