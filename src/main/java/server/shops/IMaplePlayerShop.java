package server.shops;

import java.util.List;

import client.MapleCharacter;
import client.MapleClient;
import tools.Pair;

public interface IMaplePlayerShop
{
    byte HIRED_MERCHANT = 1;
    byte PLAYER_SHOP = 2;
    byte OMOK = 3;
    byte MATCH_CARD = 4;

    String getOwnerName();

    String getDescription();

    void setDescription(String paramString);

    List<Pair<Byte, MapleCharacter>> getVisitors();

    List<MaplePlayerShopItem> getItems();

    boolean isOpen();

    void setOpen(boolean paramBoolean);

    boolean saveItems();

    boolean removeItem(int paramInt);

    boolean isOwner(MapleCharacter paramMapleCharacter);

    byte getShopType();

    byte getVisitorSlot(MapleCharacter paramMapleCharacter);

    byte getFreeSlot();

    int getItemId();

    int getMeso();

    void setMeso(int paramInt);

    int getOwnerId();

    int getOwnerAccId();

    void addItem(MaplePlayerShopItem paramMaplePlayerShopItem);

    void removeFromSlot(int paramInt);

    void broadcastToVisitors(byte[] paramArrayOfByte);

    void addVisitor(MapleCharacter paramMapleCharacter);

    void removeVisitor(MapleCharacter paramMapleCharacter);

    void removeAllVisitors(int paramInt1, int paramInt2);

    void buy(MapleClient paramMapleClient, int paramInt, short paramShort);

    void closeShop(boolean paramBoolean1, boolean paramBoolean2);

    String getPassword();

    int getMaxSize();

    int getSize();

    int getGameType();

    void update();

    boolean isAvailable();

    void setAvailable(boolean paramBoolean);

    List<AbstractPlayerStore.BoughtItem> getBoughtItems();

    List<Pair<String, Byte>> getMessages();

    int getMapId();

    int getChannel();
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shops\IMaplePlayerShop.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */