package server.shops;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.ItemFlag;
import handling.channel.ChannelServer;
import server.MapleInventoryManipulator;
import server.MapleItemInformationProvider;
import server.Timer;
import server.maps.MapleMapObjectType;
import tools.MaplePacketCreator;
import tools.packet.PlayerShopPacket;

public class HiredMerchant extends AbstractPlayerStore
{
    private static final Logger log = Logger.getLogger(HiredMerchant.class);
    private final List<String> blacklist;
    private final long start;
    public ScheduledFuture<?> schedule;
    private int storeid;
    private long lastChangeNameTime = 0L;

    public HiredMerchant(MapleCharacter owner, int itemId, String desc)
    {
        super(owner, itemId, desc, "", 6);
        this.start = System.currentTimeMillis();
        this.blacklist = new LinkedList<>();
        this.schedule = Timer.EtcTimer.getInstance().schedule(new Runnable()
        {
            public void run()
            {
                if ((HiredMerchant.this.getMCOwner() != null) && (HiredMerchant.this.getMCOwner().getPlayerShop() == HiredMerchant.this))
                {
                    HiredMerchant.this.getMCOwner().setPlayerShop(null);
                }
                HiredMerchant.this.removeAllVisitors(-1, -1);
                HiredMerchant.this.closeShop(true, true);
            }
        }, 86400000L);
    }


    public byte getShopType()
    {
        return 1;
    }

    public void buy(MapleClient c, int item, short quantity)
    {
        MaplePlayerShopItem pItem = this.items.get(item);
        Item shopItem = pItem.item;
        Item newItem = shopItem.copy();
        short perbundle = newItem.getQuantity();
        int theQuantity = pItem.price * quantity;
        newItem.setQuantity((short) (quantity * perbundle));

        short flag = newItem.getFlag();

        if (ItemFlag.KARMA_EQ.check(flag))
        {
            newItem.setFlag((short) (flag - ItemFlag.KARMA_EQ.getValue()));
        }
        else if (ItemFlag.KARMA_USE.check(flag))
        {
            newItem.setFlag((short) (flag - ItemFlag.KARMA_USE.getValue()));
        }

        if (MapleInventoryManipulator.checkSpace(c, newItem.getItemId(), newItem.getQuantity(), newItem.getOwner()))
        {
            int gainmeso = getMeso() + theQuantity - constants.GameConstants.EntrustedStoreTax(theQuantity);
            if (gainmeso > 0)
            {
                setMeso(gainmeso);
                MaplePlayerShopItem tmp167_165 = pItem;
                tmp167_165.bundles = ((short) (tmp167_165.bundles - quantity));
                MapleInventoryManipulator.addFromDrop(c, newItem, false);
                this.bought.add(new AbstractPlayerStore.BoughtItem(newItem.getItemId(), quantity, theQuantity, c.getPlayer().getName()));
                c.getPlayer().gainMeso(-theQuantity, false);
                saveItems();
                MapleCharacter chr = getMCOwnerWorld();
                String itemText =
                        MapleItemInformationProvider.getInstance().getName(newItem.getItemId()) + " (" + perbundle + ") x " + quantity + " 已经被卖出。 剩余数量: " + pItem.bundles + " 购买者: " + c.getPlayer().getName();
                if (chr != null)
                {
                    chr.dropMessage(-5, "您雇佣商店里面的道具: " + itemText);
                }
                log.info("[雇佣] " + (chr != null ? chr.getName() : getOwnerName()) + " 雇佣商店卖出: " + newItem.getItemId() + " - " + itemText + " 价格: " + theQuantity);
                tools.FileoutputUtil.hiredMerchLog(chr != null ? chr.getName() : getOwnerName(), "雇佣商店卖出: " + newItem.getItemId() + " - " + itemText + " 价格: " + theQuantity);
            }
            else
            {
                c.getPlayer().dropMessage(1, "金币不足.");
                c.getSession().write(MaplePacketCreator.enableActions());
            }
        }
        else
        {
            c.getPlayer().dropMessage(1, "背包已满.");
            c.getSession().write(MaplePacketCreator.enableActions());
        }
    }

    public void closeShop(boolean saveItems, boolean remove)
    {
        if (this.schedule != null)
        {
            this.schedule.cancel(false);
        }
        if (saveItems)
        {
            saveItems();
            this.items.clear();
        }
        if (remove)
        {
            ChannelServer.getInstance(this.channel).removeMerchant(this);
            getMap().broadcastMessage(PlayerShopPacket.destroyHiredMerchant(getOwnerId()));
        }
        getMap().removeMapObject(this);
        this.schedule = null;
    }

    public void setStoreid(int storeid)
    {
        this.storeid = storeid;
    }

    public List<MaplePlayerShopItem> searchItem(int itemSearch)
    {
        List<MaplePlayerShopItem> itemz = new LinkedList<>();
        for (MaplePlayerShopItem item : this.items)
        {
            if ((item.item.getItemId() == itemSearch) && (item.bundles > 0))
            {
                itemz.add(item);
            }
        }
        return itemz;
    }

    public int getTimeLeft()
    {
        return (int) (System.currentTimeMillis() - this.start);
    }

    public int getTimeLeft(boolean first)
    {
        if (first)
        {
            return (int) this.start;
        }
        return 86400 - (int) (System.currentTimeMillis() - this.start) / 1000;
    }

    public int getStoreId()
    {
        return this.storeid;
    }


    public boolean canChangeName()
    {
        if (this.lastChangeNameTime + 60000L > System.currentTimeMillis())
        {
            return false;
        }
        this.lastChangeNameTime = System.currentTimeMillis();
        return true;
    }

    public int getChangeNameTimeLeft()
    {
        int time = 60 - (int) (System.currentTimeMillis() - this.lastChangeNameTime) / 1000;
        return time > 0 ? time : 1;
    }

    public MapleMapObjectType getType()
    {
        return MapleMapObjectType.HIRED_MERCHANT;
    }

    public void sendSpawnData(MapleClient client)
    {
        if (isAvailable())
        {
            client.getSession().write(PlayerShopPacket.spawnHiredMerchant(this));
        }
    }

    public void sendDestroyData(MapleClient client)
    {
        if (isAvailable())
        {
            client.getSession().write(PlayerShopPacket.destroyHiredMerchant(getOwnerId()));
        }
    }

    public boolean isInBlackList(String bl)
    {
        return this.blacklist.contains(bl);
    }

    public void addBlackList(String bl)
    {
        this.blacklist.add(bl);
    }

    public void removeBlackList(String bl)
    {
        this.blacklist.remove(bl);
    }

    public void sendBlackList(MapleClient c)
    {
        c.getSession().write(PlayerShopPacket.MerchantBlackListView(this.blacklist));
    }

    public void sendVisitor(MapleClient c)
    {
        c.getSession().write(PlayerShopPacket.MerchantVisitorView(this.visitorsList));
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shops\HiredMerchant.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */