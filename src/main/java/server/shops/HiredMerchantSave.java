package server.shops;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HiredMerchantSave
{
    public static final int NumSavingThreads = 5;
    private static final TimingThread[] Threads = new TimingThread[5];
    private static final AtomicInteger Distribute = new AtomicInteger(0);

    static
    {
        for (int i = 0; i < Threads.length; i++)
            Threads[i] = new TimingThread(new HiredMerchantSaveRunnable());
    }

    public static void QueueShopForSave(HiredMerchant hm)
    {
        int Current = Distribute.getAndIncrement() % 5;
        Threads[Current].getRunnable().Queue(hm);
    }

    public static void Execute(Object ToNotify)
    {
        for (TimingThread timingThread : Threads)
        {
            timingThread.getRunnable().SetToNotify(ToNotify);
        }
        for (TimingThread thread : Threads)
        {
            thread.start();
        }
    }

    private static class HiredMerchantSaveRunnable implements Runnable
    {
        private static final AtomicInteger RunningThreadID = new AtomicInteger(0);
        private final int ThreadID = RunningThreadID.incrementAndGet();
        private final ArrayBlockingQueue<HiredMerchant> Queue = new ArrayBlockingQueue(500);
        private long TimeTaken = 0L;
        private int ShopsSaved = 0;
        private Object ToNotify;

        public void run()
        {
            try
            {
                while (!this.Queue.isEmpty())
                {
                    HiredMerchant next = this.Queue.take();
                    long Start = System.currentTimeMillis();
                    if ((next.getMCOwner() != null) && (next.getMCOwner().getPlayerShop() == next))
                    {
                        next.getMCOwner().setPlayerShop(null);
                    }
                    next.closeShop(true, false);
                    this.TimeTaken += System.currentTimeMillis() - Start;
                    this.ShopsSaved += 1;
                }
                System.out.println("[保存雇佣商店数据 线程 " + this.ThreadID + "] 共保存: " + this.ShopsSaved + " | 耗时: " + this.TimeTaken + " 毫秒.");
                synchronized (this.ToNotify)
                {
                    this.ToNotify.notify();
                }
            }
            catch (InterruptedException ex)
            {
                Logger.getLogger(HiredMerchantSave.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void Queue(HiredMerchant hm)
        {
            this.Queue.add(hm);
        }

        private void SetToNotify(Object o)
        {
            if (this.ToNotify == null)
            {
                this.ToNotify = o;
            }
        }
    }

    private static class TimingThread extends Thread
    {
        private final HiredMerchantSave.HiredMerchantSaveRunnable ext;

        public TimingThread(HiredMerchantSave.HiredMerchantSaveRunnable r)
        {
            super();
            this.ext = r;
        }

        public HiredMerchantSave.HiredMerchantSaveRunnable getRunnable()
        {
            return this.ext;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\shops\HiredMerchantSave.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */