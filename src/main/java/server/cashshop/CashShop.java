package server.cashshop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import client.MapleClient;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.ItemLoader;
import client.inventory.MapleInventoryType;
import constants.ItemConstants;
import server.MapleItemInformationProvider;
import tools.Pair;

public class CashShop implements java.io.Serializable
{
    private static final long serialVersionUID = 231541893513373579L;
    private final int accountId;
    private final int characterId;
    private final ItemLoader factory = ItemLoader.现金道具;
    private final List<Item> inventory = new ArrayList<>();
    private final List<Integer> uniqueids = new ArrayList<>();

    public CashShop(int accountId, int characterId, int jobType) throws SQLException
    {
        this.accountId = accountId;
        this.characterId = characterId;
        for (Pair<Item, MapleInventoryType> item : this.factory.loadItems(false, accountId).values())
        {
            this.inventory.add(item.getLeft());
        }
    }

    public int getItemsSize()
    {
        return this.inventory.size();
    }

    public List<Item> getInventory()
    {
        return this.inventory;
    }

    public Item findByCashId(int cashId)
    {
        for (Item item : this.inventory)
        {
            if (item.getUniqueId() == cashId)
            {
                return item;
            }
        }
        return null;
    }

    public void checkExpire(MapleClient c)
    {
        List<Item> toberemove = new ArrayList<>();
        for (Item item : this.inventory)
        {
            if ((item != null) && (!ItemConstants.isPet(item.getItemId())) && (item.getExpiration() > 0L) && (item.getExpiration() < System.currentTimeMillis()))
            {
                toberemove.add(item);
            }
        }
        if (toberemove.size() > 0)
        {
            for (Item item : toberemove)
            {
                removeFromInventory(item);
                c.getSession().write(tools.packet.MTSCSPacket.cashItemExpired(item.getUniqueId()));
            }
            toberemove.clear();
        }
    }

    public void removeFromInventory(Item item)
    {
        this.inventory.remove(item);
    }

    public Item toItem(CashItemInfo cItem)
    {
        return toItem(cItem, server.MapleInventoryManipulator.getUniqueId(cItem.getId(), null), "");
    }

    public Item toItem(CashItemInfo cItem, int uniqueid, String gift)
    {
        if (uniqueid <= 0)
        {
            uniqueid = client.inventory.MapleInventoryIdentifier.getInstance();
        }
        long period = cItem.getPeriod();
        Item ret = null;
        if (ItemConstants.getInventoryType(cItem.getId()) == MapleInventoryType.EQUIP)
        {
            Equip eq = (Equip) MapleItemInformationProvider.getInstance().getEquipById(cItem.getId(), uniqueid);
            if (period > 0L)
            {
                eq.setExpiration(System.currentTimeMillis() + period * 24L * 60L * 60L * 1000L);
            }
            eq.setGMLog("商城购买 " + cItem.getSN() + " 时间 " + tools.FileoutputUtil.CurrentReadable_Date());
            eq.setGiftFrom(gift);
            if ((ItemConstants.isEffectRing(cItem.getId())) && (uniqueid > 0))
            {
                client.inventory.MapleRing ring = client.inventory.MapleRing.loadFromDb(uniqueid);
                if (ring != null)
                {
                    eq.setRing(ring);
                }
            }
            ret = eq.copy();
        }
        else
        {
            Item item = new Item(cItem.getId(), (short) 0, (short) cItem.getCount(), (short) 0, uniqueid);
            if (ItemConstants.isPet(cItem.getId()))
            {
                period = 90L;
                item.setExpiration(System.currentTimeMillis() + period * 24L * 60L * 60L * 1000L);
            }
            else if ((cItem.getId() == 5211047) || (cItem.getId() == 5360014))
            {
                item.setExpiration(System.currentTimeMillis() + 10800000L);
            }
            else if (cItem.getId() == 5211060)
            {
                item.setExpiration(System.currentTimeMillis() + 7200000L);
            }
            else if (period > 0L)
            {
                item.setExpiration(System.currentTimeMillis() + period * 24L * 60L * 60L * 1000L);
            }
            else
            {
                item.setExpiration(-1L);
            }
            item.setGMLog("商城购买 " + cItem.getSN() + " 时间 " + tools.FileoutputUtil.CurrentReadable_Date());
            item.setGiftFrom(gift);
            if (ItemConstants.isPet(cItem.getId()))
            {
                client.inventory.MaplePet pet = client.inventory.MaplePet.createPet(cItem.getId(), uniqueid);
                if (pet != null)
                {
                    item.setPet(pet);
                }
            }
            ret = item.copy();
        }
        return ret;
    }

    public Item toItem(CashItemInfo cItem, int uniqueid)
    {
        return toItem(cItem, uniqueid, "");
    }

    public void gift(int recipient, String from, String message, int sn)
    {
        gift(recipient, from, message, sn, 0);
    }

    public void gift(int recipient, String from, String message, int sn, int uniqueid)
    {
        try
        {
            PreparedStatement ps = database.DatabaseConnection.getConnection().prepareStatement("INSERT INTO `gifts` VALUES (DEFAULT, ?, ?, ?, ?, ?)");
            ps.setInt(1, recipient);
            ps.setString(2, from);
            ps.setString(3, message);
            ps.setInt(4, sn);
            ps.setInt(5, uniqueid);
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
    }

    public List<Pair<Item, String>> loadGifts()
    {
        List<Pair<Item, String>> gifts = new ArrayList<>();
        Connection con = database.DatabaseConnection.getConnection();
        try
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM `gifts` WHERE `recipient` = ?");
            ps.setInt(1, this.characterId);
            ResultSet rs = ps.executeQuery();

            while (rs.next())
            {
                CashItemInfo cItem = CashItemFactory.getInstance().getItem(rs.getInt("sn"));
                if (cItem != null)
                {

                    Item item = toItem(cItem, rs.getInt("uniqueid"), rs.getString("from"));
                    gifts.add(new Pair(item, rs.getString("message")));
                    this.uniqueids.add(item.getUniqueId());
                    List<Integer> packages = CashItemFactory.getInstance().getPackageItems(cItem.getId());
                    Iterator localIterator;
                    if ((packages != null) && (packages.size() > 0))
                    {
                        for (localIterator = packages.iterator(); localIterator.hasNext(); )
                        {
                            int packageItem = (Integer) localIterator.next();
                            CashItemInfo pack = CashItemFactory.getInstance().getSimpleItem(packageItem);
                            if (pack != null)
                            {
                                addToInventory(toItem(pack, rs.getString("from")));
                            }
                        }
                    }
                    else
                    {
                        addToInventory(item);
                    }
                }
            }
            rs.close();
            ps.close();
            ps = con.prepareStatement("DELETE FROM `gifts` WHERE `recipient` = ?");
            ps.setInt(1, this.characterId);
            ps.executeUpdate();
            ps.close();
            save();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        return gifts;
    }

    public void addToInventory(Item item)
    {
        this.inventory.add(item);
    }

    public Item toItem(CashItemInfo cItem, String gift)
    {
        return toItem(cItem, server.MapleInventoryManipulator.getUniqueId(cItem.getId(), null), gift);
    }

    public void save() throws SQLException
    {
        List<Pair<Item, MapleInventoryType>> itemsWithType = new ArrayList<>();

        for (Item item : this.inventory)
        {
            itemsWithType.add(new Pair(item, ItemConstants.getInventoryType(item.getItemId())));
        }

        this.factory.saveItems(itemsWithType, this.accountId);
    }

    public boolean canSendNote(int uniqueid)
    {
        return this.uniqueids.contains(uniqueid);
    }

    public void sendedNote(int uniqueid)
    {
        for (int i = 0; i < this.uniqueids.size(); i++)
        {
            if (this.uniqueids.get(i) == uniqueid)
            {
                this.uniqueids.remove(i);
            }
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\cashshop\CashShop.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */