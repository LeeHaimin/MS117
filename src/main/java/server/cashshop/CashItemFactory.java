package server.cashshop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import provider.MapleData;
import provider.MapleDataTool;

public class CashItemFactory
{
    private static final CashItemFactory instance = new CashItemFactory();
    private static final int[] bestItems = {30200045, 50000080, 30200066, 50400016, 30100092};
    private final Map<Integer, CashItemInfo> itemStats = new HashMap<>();
    private final Map<Integer, Integer> idLookup = new HashMap<>();
    private final Map<Integer, CashItemInfo> oldItemStats = new HashMap<>();
    private final Map<Integer, Integer> oldIdLookup = new HashMap<>();
    private final Map<Integer, List<Integer>> itemPackage = new HashMap<>();
    private final Map<Integer, List<Integer>> openBox = new HashMap<>();
    private final provider.MapleDataProvider data = provider.MapleDataProviderFactory.getDataProvider(new java.io.File(System.getProperty("wzpath") + "/Etc.wz"));
    private final MapleData commodities = this.data.getData("Commodity.img");
    private final Map<Integer, Boolean> blockCashItemId = new HashMap<>();
    private final Map<Integer, Boolean> blockCashSnId = new HashMap<>();
    private final List<Integer> blockRefundableItemId = new LinkedList<>();

    public static CashItemFactory getInstance()
    {
        return instance;
    }

    public void initialize()
    {
        this.blockRefundableItemId.clear();
        int onSaleSize = 0;
        Map<Integer, Integer> fixId = new HashMap<>();

        for (MapleData field : this.commodities.getChildren())
        {
            int SN = MapleDataTool.getIntConvert("SN", field, 0);
            int itemId = MapleDataTool.getIntConvert("ItemId", field, 0);
            int count = MapleDataTool.getIntConvert("Count", field, 1);
            int price = MapleDataTool.getIntConvert("Price", field, 0);
            int originalPrice = MapleDataTool.getIntConvert("originalPrice", field, 0);
            int period = MapleDataTool.getIntConvert("Period", field, 0);
            int gender = MapleDataTool.getIntConvert("Gender", field, 2);
            boolean onSale = (MapleDataTool.getIntConvert("OnSale", field, 0) > 0) || (isOnSalePackage(SN));
            boolean bonus = MapleDataTool.getIntConvert("Bonus", field, 0) >= 0;
            boolean refundable = MapleDataTool.getIntConvert("Refundable", field, 0) == 0;
            boolean discount = MapleDataTool.getIntConvert("discount", field, 0) >= 0;
            if (onSale)
            {
                onSaleSize++;
            }

            CashItemInfo stats = new CashItemInfo(itemId, count, price, originalPrice, SN, period, gender, onSale, bonus, refundable, discount);
            if (SN > 0)
            {
                this.itemStats.put(SN, stats);
                if (this.idLookup.containsKey(itemId))
                {
                    fixId.put(SN, itemId);
                    this.blockRefundableItemId.add(itemId);
                }
                this.idLookup.put(itemId, SN);
            }
        }
        System.out.println("共加载 " + this.itemStats.size() + " 个商城道具 有 " + onSaleSize + " 个道具处于出售状态...");
        System.out.println("其中有 " + fixId.size() + " 重复价格的道具和 " + this.blockRefundableItemId.size() + " 个禁止换购的道具.");

        MapleData packageData = this.data.getData("CashPackage.img");

        for (MapleData root : packageData.getChildren())
        {
            if (root.getChildByPath("SN") != null)
            {

                List<Integer> packageItems = new ArrayList<>();
                for (MapleData dat : root.getChildByPath("SN").getChildren())
                {
                    packageItems.add(MapleDataTool.getIntConvert(dat));
                }
                this.itemPackage.put(Integer.parseInt(root.getName()), packageItems);
            }
        }
        MapleData dat;
        System.out.println("共加载 " + this.itemPackage.size() + " 个商城礼包...");

        onSaleSize = 0;
        provider.MapleDataDirectoryEntry root = this.data.getRoot();
        for (provider.MapleDataEntry topData : root.getFiles())
        {
            if (topData.getName().startsWith("OldCommodity"))
            {
                MapleData Commodity = this.data.getData(topData.getName());
                for (MapleData field : Commodity.getChildren())
                {
                    int SN = MapleDataTool.getIntConvert("SN", field, 0);
                    int itemId = MapleDataTool.getIntConvert("ItemId", field, 0);
                    int count = MapleDataTool.getIntConvert("Count", field, 1);
                    int price = MapleDataTool.getIntConvert("Price", field, 0);
                    int originalPrice = MapleDataTool.getIntConvert("originalPrice", field, 0);
                    int period = MapleDataTool.getIntConvert("Period", field, 0);
                    int gender = MapleDataTool.getIntConvert("Gender", field, 2);
                    boolean onSale = (MapleDataTool.getIntConvert("OnSale", field, 0) > 0) || (isOnSalePackage(SN));
                    boolean bonus = MapleDataTool.getIntConvert("Bonus", field, 0) >= 0;
                    boolean refundable = MapleDataTool.getIntConvert("Refundable", field, 0) == 0;
                    boolean discount = MapleDataTool.getIntConvert("discount", field, 0) >= 0;
                    if (onSale)
                    {
                        onSaleSize++;
                    }
                    CashItemInfo stats = new CashItemInfo(itemId, count, price, originalPrice, SN, period, gender, onSale, bonus, refundable, discount);
                    if (SN > 0)
                    {
                        this.oldItemStats.put(SN, stats);
                        this.oldIdLookup.put(itemId, SN);
                    }
                }
            }
        }
        System.out.println("共加载 " + this.oldItemStats.size() + " 个老的商城道具 有 " + onSaleSize + " 个道具处于出售状态...");
        loadBlockedCash();
        loadRandomItemInfo();
    }

    public boolean isOnSalePackage(int snId)
    {
        return (snId >= 170200002) && (snId <= 170200013);
    }

    public void loadBlockedCash()
    {
        this.blockCashItemId.clear();
        MapleData root = this.data.getData("BlockCash.img");
        for (MapleData dat : root.getChildByPath("ItemId").getChildren())
        {
            int itemId = Integer.parseInt(dat.getName());
            boolean block = MapleDataTool.getIntConvert("Block", dat, 0) >= 0;
            if (this.blockCashItemId.containsKey(itemId))
            {
                System.out.println("发现重复禁止道具信息: " + itemId);
            }
            else this.blockCashItemId.put(itemId, block);
        }
        System.out.println("共加载 " + this.blockCashItemId.size() + " 个商城禁止购买的道具ID信息...");
        this.blockCashSnId.clear();
        for (MapleData dat : root.getChildByPath("SNId").getChildren())
        {
            int packageId = Integer.parseInt(dat.getName());
            boolean block = MapleDataTool.getIntConvert("Block", dat, 0) >= 0;
            if (this.blockCashSnId.containsKey(packageId))
            {
                System.out.println("发现重复禁止SN信息: " + packageId);
            }
            else this.blockCashSnId.put(packageId, block);
        }
        System.out.println("共加载 " + this.blockCashSnId.size() + " 个商城禁止购买的道具SN信息...");
    }

    public void loadRandomItemInfo()
    {
        this.openBox.clear();
        List<Integer> boxItems = new LinkedList<>();
        boxItems.add(50400438);
        boxItems.add(50400439);
        boxItems.add(50400440);
        boxItems.add(50400441);
        boxItems.add(50400442);
        boxItems.add(50400443);
        boxItems.add(50400444);
        boxItems.add(50400445);
        this.openBox.put(5533027, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000485);
        boxItems.add(20000486);
        boxItems.add(20000487);
        boxItems.add(20000488);
        boxItems.add(20000489);
        boxItems.add(20000490);
        boxItems.add(20000491);
        this.openBox.put(5533003, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000687);
        boxItems.add(20000688);
        boxItems.add(20000689);
        boxItems.add(20000690);
        boxItems.add(20000691);
        this.openBox.put(5533011, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(50500061);
        boxItems.add(50100026);
        boxItems.add(50100027);
        boxItems.add(50100028);
        boxItems.add(50500046);
        this.openBox.put(5533019, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20800316);
        boxItems.add(20800317);
        boxItems.add(20800318);
        boxItems.add(20800319);
        boxItems.add(20800320);
        this.openBox.put(5533004, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(21100152);
        boxItems.add(21100153);
        boxItems.add(21100154);
        boxItems.add(21100155);
        this.openBox.put(5533012, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000849);
        boxItems.add(20000850);
        boxItems.add(20000851);
        boxItems.add(20000852);
        boxItems.add(20000853);
        boxItems.add(20000854);
        boxItems.add(20400302);
        boxItems.add(20400303);
        boxItems.add(20400304);
        this.openBox.put(5533013, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000547);
        boxItems.add(20000533);
        boxItems.add(20000391);
        boxItems.add(20000550);
        boxItems.add(20000476);
        this.openBox.put(5533006, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000462);
        boxItems.add(20000463);
        boxItems.add(20000464);
        boxItems.add(20000465);
        boxItems.add(20000466);
        boxItems.add(20000467);
        boxItems.add(20000468);
        boxItems.add(20000469);
        this.openBox.put(5533014, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(140200224);
        boxItems.add(140200218);
        boxItems.add(140200225);
        boxItems.add(140200226);
        boxItems.add(140200227);
        boxItems.add(140200228);
        this.openBox.put(5533007, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000625);
        boxItems.add(20000626);
        boxItems.add(20000627);
        boxItems.add(20000628);
        boxItems.add(20000629);
        this.openBox.put(5533023, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000621);
        boxItems.add(20000622);
        boxItems.add(20000623);
        boxItems.add(20000624);
        this.openBox.put(5533024, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000740);
        boxItems.add(20000741);
        boxItems.add(20000742);
        boxItems.add(20000743);
        boxItems.add(20000744);
        boxItems.add(20000745);
        boxItems.add(20000746);
        boxItems.add(20000747);
        this.openBox.put(5533000, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(140800248);
        boxItems.add(140800249);
        boxItems.add(140800250);
        boxItems.add(140800251);
        boxItems.add(140800252);
        this.openBox.put(5533032, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(21100149);
        boxItems.add(21100150);
        boxItems.add(21100151);
        this.openBox.put(5533008, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20800259);
        boxItems.add(20800260);
        boxItems.add(20800263);
        boxItems.add(20800264);
        boxItems.add(20800265);
        boxItems.add(20800267);
        this.openBox.put(5533001, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(130000498);
        boxItems.add(130000499);
        boxItems.add(130000391);
        boxItems.add(130000500);
        boxItems.add(130000390);
        this.openBox.put(5533025, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(140100764);
        boxItems.add(140100765);
        boxItems.add(140100766);
        boxItems.add(140100767);
        boxItems.add(140100768);
        boxItems.add(140100769);
        boxItems.add(140100770);
        boxItems.add(140100771);
        boxItems.add(140100772);
        boxItems.add(140100773);
        boxItems.add(140100774);
        boxItems.add(140100775);
        boxItems.add(140100776);
        boxItems.add(140100777);
        this.openBox.put(5533033, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20000543);
        boxItems.add(20000544);
        boxItems.add(20000545);
        boxItems.add(20000546);
        boxItems.add(20000547);
        this.openBox.put(5533009, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(10002766);
        boxItems.add(10002767);
        boxItems.add(10002768);
        boxItems.add(10002769);
        this.openBox.put(5533017, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(140100547);
        boxItems.add(140100548);
        boxItems.add(140100549);
        boxItems.add(140100550);
        boxItems.add(140100551);
        boxItems.add(140100552);
        this.openBox.put(5533026, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(20800297);
        boxItems.add(20800298);
        boxItems.add(20800299);
        boxItems.add(20800300);
        boxItems.add(20800301);
        this.openBox.put(5533002, boxItems);

        boxItems = new LinkedList<>();
        boxItems.add(50500061);
        boxItems.add(50100026);
        boxItems.add(50100027);
        boxItems.add(50100028);
        boxItems.add(50500046);
        this.openBox.put(5533018, boxItems);

        System.out.println("共加载 " + this.openBox.size() + " 个商城随机箱子的信息...");
    }

    public Map<Integer, Boolean> getBlockedCashItem()
    {
        return this.blockCashItemId;
    }

    public boolean isBlockedCashItemId(int itemId)
    {
        return this.blockCashItemId.containsKey(itemId);
    }

    public Map<Integer, Boolean> getBlockCashSn()
    {
        return this.blockCashSnId;
    }

    public boolean isBlockCashSnId(int itemId)
    {
        return this.blockCashSnId.containsKey(itemId);
    }

    public CashItemInfo getSimpleItem(int sn)
    {
        return this.itemStats.get(sn);
    }

    public boolean isBlockRefundableItemId(int itemId)
    {
        return this.blockRefundableItemId.contains(itemId);
    }

    public CashItemInfo getItem(int sn)
    {
        return getItem(sn, true);
    }

    public CashItemInfo getItem(int sn, boolean checkSale)
    {
        CashItemInfo stats = this.itemStats.get(sn);

        if (stats == null)
        {
            return null;
        }
        return (checkSale) && (!stats.onSale()) ? null : stats;
    }

    public List<Integer> getPackageItems(int itemId)
    {
        return this.itemPackage.get(itemId);
    }


    public Map<Integer, List<Integer>> getRandomItemInfo()
    {
        return this.openBox;
    }

    public boolean hasRandomItem(int itemId)
    {
        return this.openBox.containsKey(itemId);
    }

    public List<Integer> getRandomItem(int itemId)
    {
        return this.openBox.get(itemId);
    }

    public int[] getBestItems()
    {
        return bestItems;
    }

    public int getLinkItemId(int itemId)
    {
        switch (itemId)
        {
            case 5000029:
            case 5000030:
            case 5000032:
            case 5000033:
            case 5000035:
                return 5000028;
            case 5000048:
            case 5000049:
            case 5000050:
            case 5000051:
            case 5000052:
                return 5000047;
        }
        return itemId;
    }

    public int getSnFromId(int itemId)
    {
        if (this.idLookup.containsKey(itemId))
        {
            return this.idLookup.get(itemId);
        }
        return 0;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\cashshop\CashItemFactory.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */