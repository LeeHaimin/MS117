package server;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import client.CardData;
import constants.GameConstants;
import database.DatabaseConnection;
import provider.MapleData;
import provider.MapleDataProvider;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;
import tools.Pair;
import tools.Triple;


public class CharacterCardFactory
{
    private static final CharacterCardFactory instance = new CharacterCardFactory();
    protected final MapleDataProvider etcData = MapleDataProviderFactory.getDataProvider(new File(System.getProperty("wzpath") + "/Etc.wz"));
    protected final Map<Integer, Integer> cardEffects = new HashMap<>();
    protected final Map<Integer, List<Integer>> uniqueEffects = new HashMap<>();

    public static CharacterCardFactory getInstance()
    {
        return instance;
    }

    public void initialize()
    {
        MapleData data = this.etcData.getData("CharacterCard.img");
        for (MapleData Card : data.getChildByPath("Card"))
        {
            int skillId = MapleDataTool.getIntConvert("skillID", Card, 0);
            if (skillId > 0)
            {
                this.cardEffects.put(Integer.parseInt(Card.getName()), skillId);
            }
        }
        for (MapleData Deck : data.getChildByPath("Deck"))
        {
            boolean uniqueEffect = MapleDataTool.getIntConvert("uniqueEffect", Deck, 0) > 0;
            int skillId = MapleDataTool.getIntConvert("skillID", Deck, 0);
            if (uniqueEffect)
            {
                List<Integer> ids = new ArrayList<>();
                for (MapleData reqCardID : Deck.getChildByPath("reqCardID"))
                {
                    ids.add(MapleDataTool.getIntConvert(reqCardID));
                }
                if ((skillId > 0) && (!ids.isEmpty()))
                {
                    this.uniqueEffects.put(skillId, ids);
                }
            }
        }
    }

    public Triple<Integer, Integer, Integer> getCardSkill(int job, int level)
    {
        int skillid = this.cardEffects.get(job / 10);
        if (skillid <= 0)
        {
            return null;
        }

        return new Triple(skillid - 71000000, skillid, GameConstants.getCardSkillLevel(level));
    }

    public List<Integer> getUniqueSkills(List<Integer> special)
    {
        List<Integer> ret = new LinkedList<>();
        for (Map.Entry<Integer, List<Integer>> m : this.uniqueEffects.entrySet())
        {
            if ((m.getValue().contains(special.get(0))) && (m.getValue().contains(special.get(1))) && (m.getValue().contains(special.get(2))))
            {
                ret.add(m.getKey());
            }
        }
        return ret;
    }

    public boolean isUniqueEffects(int skillId)
    {
        return this.uniqueEffects.containsKey(skillId);
    }

    public int getRankSkill(int level)
    {
        return GameConstants.getCardSkillLevel(level) + 71001099;
    }

    public Map<Integer, CardData> loadCharacterCards(int accId, int serverId)
    {
        Map<Integer, CardData> cards = new LinkedHashMap<>();
        Map<Integer, Pair<Short, Short>> inf = loadCharactersInfo(accId, serverId);
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM `character_cards` WHERE `accid` = ?");
            ps.setInt(1, accId);
            ResultSet rs = ps.executeQuery();
            int deck1 = 0;
            int deck2 = 3;
            int deck3 = 6;
            while (rs.next())
            {
                int chrId = rs.getInt("characterid");
                Pair<Short, Short> x = inf.get(chrId);
                if ((x != null) &&


                        (canHaveCard(x.getLeft(), x.getRight())))
                {

                    int position = rs.getInt("position");
                    if (position < 4)
                    {
                        deck1++;
                        cards.put(deck1, new CardData(chrId, x.getLeft(), x.getRight()));
                    }
                    else if ((position > 3) && (position < 7))
                    {
                        deck2++;
                        cards.put(deck2, new CardData(chrId, x.getLeft(), x.getRight()));
                    }
                    else
                    {
                        deck3++;
                        cards.put(deck3, new CardData(chrId, x.getLeft(), x.getRight()));
                    }
                }
            }
            rs.close();
            ps.close();
        }
        catch (SQLException sqlE)
        {
            System.out.println("Failed to load character cards. Reason: " + sqlE.toString());
        }
        for (int i = 1; i <= 9; i++)
        {
            if (cards.get(i) == null)
            {
                cards.put(i, new CardData(0, (short) 0, (short) 0));
            }
        }
        return cards;
    }

    public Map<Integer, Pair<Short, Short>> loadCharactersInfo(int accId, int serverId)
    {
        Map<Integer, Pair<Short, Short>> chars = new LinkedHashMap<>();
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT id, level, job FROM characters WHERE accountid = ? AND world = ?");
            ps.setInt(1, accId);
            ps.setInt(2, serverId);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                chars.put(rs.getInt("id"), new Pair(rs.getShort("level"), rs.getShort("job")));
            }
            rs.close();
            ps.close();
        }
        catch (SQLException e)
        {
            System.err.println("error loading characters info. reason: " + e.toString());
        }
        return chars;
    }

    public boolean canHaveCard(int level, int job)
    {
        if (level < 30)
        {
            return false;
        }
        return this.cardEffects.get(job / 10) != null;
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\CharacterCardFactory.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */