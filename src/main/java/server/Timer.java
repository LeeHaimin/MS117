package server;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Timer
{
    private static final AtomicInteger threadNumber = new AtomicInteger(1);
    protected String file;
    protected String name;
    private ScheduledThreadPoolExecutor ses;

    public void start()
    {
        if ((this.ses != null) && (!this.ses.isShutdown()) && (!this.ses.isTerminated()))
        {
            return;
        }
        this.file = ("Log_" + this.name + "_Except.rtf");
        this.ses = new ScheduledThreadPoolExecutor(5, new RejectedThreadFactory());
        this.ses.setKeepAliveTime(10L, java.util.concurrent.TimeUnit.MINUTES);
        this.ses.allowCoreThreadTimeOut(true);
        this.ses.setMaximumPoolSize(8);
        this.ses.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
    }

    public ScheduledThreadPoolExecutor getSES()
    {
        return this.ses;
    }

    public void stop()
    {
        if (this.ses != null)
        {
            this.ses.shutdown();
        }
    }

    public java.util.concurrent.ScheduledFuture<?> register(Runnable command, long period, long initialDelay)
    {
        if (this.ses == null)
        {
            return null;
        }


        return this.ses.scheduleAtFixedRate(new LoggingSaveRunnable(command, this.file), initialDelay, period, java.util.concurrent.TimeUnit.MILLISECONDS);
    }

    public java.util.concurrent.ScheduledFuture<?> register(Runnable command, long period)
    {
        if (this.ses == null)
        {
            return null;
        }
        return this.ses.scheduleAtFixedRate(new LoggingSaveRunnable(command, this.file), 0L, period, java.util.concurrent.TimeUnit.MILLISECONDS);
    }

    public java.util.concurrent.ScheduledFuture<?> scheduleAtTimestamp(Runnable command, long timestamp)
    {
        return schedule(command, timestamp - System.currentTimeMillis());
    }

    public java.util.concurrent.ScheduledFuture<?> schedule(Runnable command, long delay)
    {
        if (this.ses == null)
        {
            return null;
        }


        return this.ses.schedule(new LoggingSaveRunnable(command, this.file), delay, java.util.concurrent.TimeUnit.MILLISECONDS);
    }

    public static class WorldTimer extends Timer
    {
        private static final WorldTimer instance = new WorldTimer();

        private WorldTimer()
        {
            this.name = "Worldtimer";
        }

        public static WorldTimer getInstance()
        {
            return instance;
        }
    }

    public static class MapTimer extends Timer
    {
        private static final MapTimer instance = new MapTimer();

        private MapTimer()
        {
            this.name = "Maptimer";
        }

        public static MapTimer getInstance()
        {
            return instance;
        }
    }

    public static class BuffTimer extends Timer
    {
        private static final BuffTimer instance = new BuffTimer();

        private BuffTimer()
        {
            this.name = "Bufftimer";
        }

        public static BuffTimer getInstance()
        {
            return instance;
        }
    }

    public static class EventTimer extends Timer
    {
        private static final EventTimer instance = new EventTimer();

        private EventTimer()
        {
            this.name = "Eventtimer";
        }

        public static EventTimer getInstance()
        {
            return instance;
        }
    }

    public static class CloneTimer extends Timer
    {
        private static final CloneTimer instance = new CloneTimer();

        private CloneTimer()
        {
            this.name = "Clonetimer";
        }

        public static CloneTimer getInstance()
        {
            return instance;
        }
    }

    public static class EtcTimer extends Timer
    {
        private static final EtcTimer instance = new EtcTimer();

        private EtcTimer()
        {
            this.name = "Etctimer";
        }

        public static EtcTimer getInstance()
        {
            return instance;
        }
    }

    public static class CheatTimer extends Timer
    {
        private static final CheatTimer instance = new CheatTimer();

        private CheatTimer()
        {
            this.name = "Cheattimer";
        }

        public static CheatTimer getInstance()
        {
            return instance;
        }
    }

    public static class PingTimer extends Timer
    {
        private static final PingTimer instance = new PingTimer();

        private PingTimer()
        {
            this.name = "Pingtimer";
        }

        public static PingTimer getInstance()
        {
            return instance;
        }
    }

    private static class LoggingSaveRunnable implements Runnable
    {
        final Runnable command;
        final String file;

        public LoggingSaveRunnable(Runnable r, String file)
        {
            this.command = r;
            this.file = file;
        }

        public void run()
        {
            try
            {
                this.command.run();
            }
            catch (Throwable t)
            {
                tools.FileoutputUtil.outputFileError(this.file, t);
            }
        }
    }

    private class RejectedThreadFactory implements java.util.concurrent.ThreadFactory
    {
        private final AtomicInteger threadNumber2 = new AtomicInteger(1);
        private final String tname;

        public RejectedThreadFactory()
        {
            this.tname = (Timer.this.name + Randomizer.nextInt());
        }

        public Thread newThread(Runnable r)
        {
            Thread t = new Thread(r);
            t.setName(this.tname + "-W-" + Timer.threadNumber.getAndIncrement() + "-" + this.threadNumber2.getAndIncrement());
            return t;
        }
    }
}


/* Location:              C:\Users\李海民\Desktop\HuaiMS_V117.jar!\server\Timer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */